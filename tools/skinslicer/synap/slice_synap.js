var SKIN_TAG = "GREENGEM";

print("Slicing image " + file_in);

print("Skin " + SKIN_TAG);

function slice_windows() {
    print("Create windows resources");

    slice9IntoDir (file_in, "window/slct",  "./window/slct/",   "",   [ 40, 142], [9, 144, 11], [22, 135, 12]);
    slice9IntoDir (file_in, "window/dslct", "./window/dslct/",  "",   [219, 142], [9, 144, 11], [22, 135, 12]);
    slice9IntoDir (file_in, "window/drag",  "./window/drag/",   "",   [398, 142], [9, 144, 11], [22, 135, 12]);
    slice9IntoDir (file_in, "window/dsbl",  "./window/dsbl/",   "",   [577, 142], [9, 144, 11], [22, 135, 12]);
}

function slice_button() {
    print("Create button resources");

    slice3IntoDir (file_in, "button",  "./button/up_",          "",   [ 43, 319], [11, 78, 10], [22]);
    slice3IntoDir (file_in, "button",  "./button/mo_",          "",   [144, 319], [11, 78, 10], [22]);
    slice3IntoDir (file_in, "button",  "./button/dn_",          "",   [245, 319], [11, 78, 10], [22]);
    slice3IntoDir (file_in, "button",  "./button/dsbl_",        "",   [346, 319], [11, 78, 10], [22]);
    slice3IntoDir (file_in, "button",  "./button/emph_",        "",   [447, 319], [11, 78, 10], [22]);

    //File.Copy("3x4patch-template.html", "./button/index.html", true);    
}

function slice_cbrb() {
    print("Create checkboxes and radio buttons");
    sliceMNIntoDir(file_in, "cbrb",        "./cbrb/",    "",
        [938, 314], [17,17, 17, 17], [17, 17, 17, 17], 4, 4,
        ["cb_off_slct", "cb_off_dslct", "cb_off_dsbl", "cb_off_rsv", "cb_on_slct", "cb_on_dslct",  "cb_on_dsbl",  "cb_on_rsv",
        "rb_off_slct", "rb_off_dslct", "rb_off_dsbl", "rb_off_rsv", "rb_on_slct",  "rb_on_dslct",  "rb_on_dsbl",  "rb_on_rsv"]);
}

function slice_scrollbars() {
    print("Create scrollbars");

    var scrollbar_slice_names: Array = new Array("arrow_lft", "fill_lft", "grip_lft", "grip_lft_fill", "grip_hmid", "grip_rgt_fill", "grip_rgt", "fill_rgt", "arrow_rgt");        
    sliceMNIntoDir(file_in, "scrollbar/horiz/up",    "./scrollbar/horiz/up/",     "",   [251, 439], [17, 14, 5, 54, 11, 54, 5, 6, 17], [18], 9, 1, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/horiz/dn",    "./scrollbar/horiz/dn/",     "",   [251, 458], [17, 14, 5, 54, 11, 54, 5, 6, 17], [18], 9, 1, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/horiz/mo",    "./scrollbar/horiz/mo/",     "",   [251, 477], [17, 14, 5, 54, 11, 54, 5, 6, 17], [18], 9, 1, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/horiz/dsbl",  "./scrollbar/horiz/dsbl/",   "",   [251, 496], [17, 14, 5, 54, 11, 54, 5, 6, 17], [18], 9, 1, scrollbar_slice_names);
    prepareLZXButtonResources("scrollbar/horiz/",    SKIN_TAG, "", scrollbar_slice_names);

    scrollbar_slice_names = new Array("arrow_top", "fill_top", "grip_top", "grip_top_fill", "grip_mid", "grip_bot_fill", "grip_bot", "fill_bot", "arrow_bot");          
    sliceMNIntoDir(file_in, "scrollbar/vert/up",     "./scrollbar/vert/up/",     "",    [172, 439], [17], [17, 51, 5, 54, 11, 50, 3, 6, 17], 1, 9, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/vert/dn",     "./scrollbar/vert/dn/",     "",    [190, 439], [17], [17, 51, 5, 54, 11, 50, 3, 6, 17], 1, 9, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/vert/mo",     "./scrollbar/vert/mo/",     "",    [208, 439], [17], [17, 51, 5, 54, 11, 50, 3, 6, 17], 1, 9, scrollbar_slice_names);
    sliceMNIntoDir(file_in, "scrollbar/vert/dsbl",   "./scrollbar/vert/dsbl/",   "",    [226, 439], [17], [17, 51, 5, 54, 11, 50, 3, 6, 17], 1, 9, scrollbar_slice_names);
    prepareLZXButtonResources("scrollbar/vert/",     SKIN_TAG, "", scrollbar_slice_names);
}

function slice_tabs() {
    setgdxy(0, 0);
    
    print("Create tabs");
    var tab_slice_names: Array = ["slct_lft", "slct_mid", "slct_rt", "dslct_lft", "dslct_mid", "dslct_rt", "mo_lft", "mo_mid", "mo_rt", "disable_lft", "disable_mid", "disable_rt"];        
    sliceMNIntoDir(file_in,
        "tabs", "./tabs/tab_", "",
        [ 46, 685],                             // x, y
        [5,67,6,  5,67,6,  5,67,6,  5,67,7],    // widths
        [21],                                   // height
        tab_slice_names.length,                 // 12 columns
        1,                                      // 1 row
        tab_slice_names);

    print("Create tab scrollers");    
    var button_names: Array = [
        "scroll_lft_up", "scroll_rgt_up", "scroll_mnu_up",
        "scroll_lft_dn", "scroll_rgt_dn", "scroll_mnu_dn",
        "scroll_lft_mo", "scroll_rgt_mo", "scroll_mnu_mo"];
    sliceMNIntoDir(file_in,
        "tabs/", "./tabs/", "",
        [374, 685],
        [17, 17, 17],
        [21, 21, 21],
        3,
        3,
        button_names);
}

function slice_combobox() {
    print("Create combobox");
    slice3IntoDir(file_in, "combobox/up",  "./combobox/up/",  "", [40, 800], [3, 156, 18], [22]);
    slice3IntoDir(file_in, "combobox/dn",  "./combobox/dn/",  "", [40, 823], [3, 156, 18], [22]);
    slice3IntoDir(file_in, "combobox/mo",  "./combobox/mo/",  "", [40, 846], [3, 156, 18], [22]);
    slice3IntoDir(file_in, "combobox/dsbl","./combobox/dsbl/","", [40, 869], [3, 156, 18], [22]);
}

function slice_buttons() {        
    print("Create OTHER buttons");

    var button_names: Array = [
        "up_sq",   "left_sq",  "right_sq", "down_sq",
        "max",     "min",      "max2",     "min2",     "close",
        "plus",    "plus2",    "minus",    "minus2",   "thumb",
        "thumb2",  "question", "find",     "prev",     "next",
        "rsv3",    "gripper",  "empty",    "minus_sq",
        "plus_sq", "rsv4",     "nextff",   "props"];

    var button_sizes = memset_new(18, button_names.length);

    setgdxy(1, 1);  // Grid dx / dy step 1, 1
    sliceMNIntoDir(file_in, "buttons/up",   "./buttons/up/",   "", [40, 60],  button_sizes, [17], button_names.length, 1, button_names);
    sliceMNIntoDir(file_in, "buttons/dn",   "./buttons/dn/",   "", [40, 78],  button_sizes, [17], button_names.length, 1, button_names);
    sliceMNIntoDir(file_in, "buttons/mo",   "./buttons/mo/",   "", [40, 96],  button_sizes, [17], button_names.length, 1, button_names);
    sliceMNIntoDir(file_in, "buttons/dsbl", "./buttons/dsbl/", "", [40, 114], button_sizes, [17], button_names.length, 1, button_names);
    prepareLZXButtonResources("buttons", SKIN_TAG, "", button_names);
}

function slice_icons() {
    print("Create legend buttons");
    // sliceMNIntoDir(file_in, "tabs/button", "./tabs/button/", "",   [516, 60], [18], [17,17,17,17], 1, 4, ["next_up", "next_dn", "next_mo", "next_dsbl"] );
    var button_names: Array = ["tick", "legend", "filter", "up_arrow", "dv", "refresh_sm", "eye", "circle"];
    var button_sizes = memset_new(16, button_names.length);
    setgdxy(3, 2);
    sliceMNIntoDir(file_in, "icons/", "./icons/", "", [42, 41], button_sizes, [16], button_names.length, 1, button_names);
}

function slice_menu() {
    print("Create menu");

    setgdxy(0, 0);  // Grid dx / dy step 1, 1
    
    slice3IntoDir (file_in, "menu",   "./menu/up_",          "",   [441, 686], [3, 50, 19], [21]);
    slice3IntoDir (file_in, "menu",   "./menu/mo_",          "",   [514, 686], [3, 50, 19], [21]);
    slice3IntoDir (file_in, "menu",   "./menu/dn_",          "",   [587, 686], [3, 50, 19], [21]);
    slice3IntoDir (file_in, "menu",   "./menu/dsbl_",        "",   [660, 686], [3, 50, 19], [21]);    
    //File.Copy("3x4patch-template.html", "./menu/index.html", true);

    crop(file_in, "50x21+444+686", "menu/bar.png"); 
}

function slice_meter() {
    Directory.CreateDirectory("meter");
    crop(file_in, "154x19+40+907", "meter/slider.png");
    crop(file_in, "17x19+195+908", "meter/arrow.png");
}

slice_combobox();
slice_windows();
slice_button();
slice_cbrb();
slice_scrollbars();
slice_tabs();
slice_menu();
slice_icons();
slice_buttons();
slice_meter();    
// throw "Application stopped";
