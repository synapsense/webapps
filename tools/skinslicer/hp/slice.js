import System;
import System.IO;
import System.Shell;

function print(text) {
    Console.WriteLine(text);
};

// convert input.png -channel Alpha -evaluate Divide 2 output.png

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GLOBALS
var gdx = 0;
var gdy = 0;

function setgdxy(x, y,) {
    gdx = x;
    gdy = y;
}

function crop(fin, param, fout) {
    var im = new ActiveXObject("ImageMagickObject.MagickImage.1");

    // -page  option
    // -debug option

    var result = im.Convert(fin, '-crop', param+"!", fout); // , '-debug', 'Blob');
    
    print(fin + " ==> " + fout + " [" + param + "] " + result );
}

function crop9patch(filein,  pfx, sfx, xy0, w, h) {
    crop3Npatch(filein,      pfx, sfx, xy0, w, h, 3, null);
}

function crop3patch(filein,  pfx, sfx, xy0, w, h) {
    crop3Npatch(filein,      pfx, sfx, xy0, w, h, 1, null);
}

function crop3Npatch(filein, pfx, sfx, xy0, w, h, N, names_ext) {
    
    var names;
    
    if (N==1) {
        names = new Array("lft", "mid", "rt");     
    } else
    if (N==2) {
        names = new Array(
            "top_lft", "top_mid", "top_rt",
            "bot_lft", "bot_mid", "bot_rt");
    } else
    if (N==3) {
        names = new Array(
            "top_lft", "top_mid", "top_rt",
            "mid_lft", "mid_mid", "mid_rt",
            "bot_lft", "bot_mid", "bot_rt");
    } else {
        names = names_ext;
    }
    
    var x   = xy0[0];
    var y   = xy0[1];
    var dx  = 0;
    var dy  = 0;
    var i, j;

    for (j=0; j<N; j++) {
        dy = h[j];
        x = xy0[0];
        for (i=0; i<3; i++) {
            dx = w[i];
            var rect = dx + "x" + dy + "+" + x + "+" + y;
            var fileout = pfx + names[(j*3)+i] + sfx + ".png";
            crop(filein, rect, fileout);
            x+=dx;                              
        }
        y+=dy;
    }
}

function cropMNpatch(filein, pfx, sfx, xy0, w, h, M, N, names) {
    if ((M==3)&&(N==1)) {
        names = new Array("lft", "mid", "rt");     
    } else
    if ((M==1)&&(N==3)) {
        names = new Array("top", "mid", "bot");         
    }
    
    var x   = xy0[0] + gdx;
    var y   = xy0[1] + gdy;
    var dx  = 0;
    var dy  = 0;
    var i, j;

    for (j=0; j<N; j++) {
        dy = h[j];
        x = xy0[0];
        for (i=0; i<M; i++) {
            dx = w[i];
            var rect = dx + "x" + dy + "+" + x + "+" + y;
            var fileout = pfx + names[(j*M)+i] + sfx + ".png";
            crop(filein, rect, fileout);
            x+= dx + gdx;                              
        }
        y+=dy + gdy;
    }    
}

function slice9IntoDir(fname, dirname, pfx, sfx, xy0, w, h) {
    Directory.CreateDirectory(dirname);
    crop9patch(fname+"[0]", pfx, sfx, xy0, w, h);
    File.Copy("9patch-template.html", dirname + "/9patch.html", true);
}

function slice3IntoDir(fname, dirname, pfx, sfx, xy0, w, h) {
    Directory.CreateDirectory(dirname);
    crop3patch(fname+"[0]", pfx, sfx, xy0, w, h);
}

function sliceMNIntoDir(fname, dirname, pfx, sfx, xy0, w, h, M, N, names) {
    Directory.CreateDirectory(dirname);
    cropMNpatch(fname+"[0]", pfx, sfx, xy0, w, h, M, N, names);
}  

function readall(filename) {
    return File.ReadAllText(filename);
}

function prepareLZXButtonResources(dirname, skinname, pfx, names) {
    var fso, f1;
    
    fso = new ActiveXObject("Scripting.FileSystemObject");
    f1 = fso.CreateTextFile(dirname + "/resources.lzx", true);

    f1.WriteLine('<?xml version="1.0" encoding="UTF-8"?>');
    f1.WriteLine('<library>');
    f1.WriteLine('');
        
    var i;
    for (i=0; i<names.length; i++) {
        f1.WriteLine('    <resource name="' + skinname      + '_' + names[i] + '_rsc">');
        f1.WriteLine('        <frame src="' + pfx + 'up/'   + names[i]+'.png" />');
        f1.WriteLine('        <frame src="' + pfx + 'mo/'   + names[i]+'.png" />');
        f1.WriteLine('        <frame src="' + pfx + 'dn/'   + names[i]+'.png" />');
        f1.WriteLine('        <frame src="' + pfx + 'dsbl/' + names[i]+'.png" />');
        f1.WriteLine('    </resource>');
        f1.WriteLine('');
    }
    f1.WriteLine('</library>');
    
    f1.close();
}

function memset(b, c, n) {
    var i;
    for (i=0; i<n; i++) {
        b[i] = c;
    }
    return b;
}

function memset_new(c, n) {
    var b = new Array();
    return memset(b, c, n);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 * Get the source file name
 */
var CmdArgs = Environment.GetCommandLineArgs();

if (CmdArgs.length < 3) {
    print("Usage: " + Path.GetFileName(CmdArgs[0]) + " <input.png> <input.js>");
    Environment.Exit(255);
}

var file_in   = CmdArgs[1];

var script_in = CmdArgs[2];

eval(readall(script_in), "unsafe");
