/**
 * 
 */
package com.synapsense.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVStrategy;

/**
 * @author pahunov
 * 
 */
public class ResourceBundles2Csv {

	private static final String OUTPUT_FILE_NAME = "resources.csv";
	private static final String DEFAULT_LOCALE = "en_US";
	private static final String INPUT_FILE_NAME = "strings";
	private static final String EXTENSION = ".properties";
	private static final String INPUT_ENCODING = "UTF-8";
	private static final String OUTPUT_ENCODING = "UTF-8";

	private static final String HELP = "Converts strings_{locale}.properties into csv\n"
	        + "\t-h\t\t\tHelp\n"
	        + "\t-reverse\t\tConverts csv to strings_{locale}.properties\n"
	        + "\t-dir=path\t\tIf '-reverse' then it is path where to get properties to merge with, otherwise path were to get properties to convert.\n"
	        + "\t-delta\t\tText is added to csv only if translations to any language are missing. Works only if there is no '-reverse'\n";

	private static String separator = ",";

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> argList = Arrays.asList(args);

		if (argList.contains("-h")) {
			System.out.println(HELP);
			return;
		}

		String s = getArg("-separator=", argList);
		if (s != null) {
			if (!s.equals(",") && s.equals(";")) {
				System.out.println("Ignoring wrong separator: " + s);
			} else {
				separator = s;
			}
		}

		if (argList.contains("-reverse")) {
			csv2resourceBundles(argList);
		} else {
			resourceBundles2Csv(argList);
		}

		System.out.println("DONE!!!");
	}

	private static void resourceBundles2Csv(List<String> argList) {

		try {
			FileOutputStream fos = new FileOutputStream(OUTPUT_FILE_NAME);
			Writer writer = new OutputStreamWriter(fos, OUTPUT_ENCODING);

			// Write UTF-8 BOM
			if ("UTF-8".equalsIgnoreCase(OUTPUT_ENCODING)) {
				fos.write(0xEF);
				fos.write(0xBB);
				fos.write(0xBF);
			}

			CSVPrinter csvPrinter = new CSVPrinter(writer);
			CSVStrategy csvStrategy = new CSVStrategy(separator.charAt(0), '"', '#');
			csvPrinter.setStrategy(csvStrategy);

			String dir = getArg("-dir=", argList);
			if (dir == null) {
				dir = new File(".").getCanonicalPath();
			}

			String defaultResourcesFileName = dir + "/" + INPUT_FILE_NAME + "_" + DEFAULT_LOCALE + EXTENSION;
			Properties defaultResource = new Properties();
			defaultResource.load(new InputStreamReader(new FileInputStream(defaultResourcesFileName), INPUT_ENCODING));

			List<Properties> resources = new ArrayList<Properties>();

			// header
			csvPrinter.print("Id");
			csvPrinter.print(DEFAULT_LOCALE);

			for (String locale : listLocales(dir)) {
				if (locale.equals(DEFAULT_LOCALE)) {
					continue;
				}

				// header
				csvPrinter.print(locale);

				String resourcesFileName = dir + "/" + INPUT_FILE_NAME + "_" + locale + EXTENSION;
				Properties resource = new Properties();
				resource.load(new InputStreamReader(new FileInputStream(resourcesFileName), INPUT_ENCODING));
				resources.add(resource);

			}

			csvPrinter.println();

			List<String> keys = new LinkedList<String>(defaultResource.stringPropertyNames());
			Collections.sort(keys);

			for (String key : keys) {

				List<String> line = new ArrayList<String>();

				line.add(key);
				line.add(defaultResource.getProperty(key));

				for (Properties resource : resources) {
					line.add(resource.getProperty(key, ""));
				}

				if (line.contains("") || !argList.contains("-delta")) {
					for (String l : line) {
						csvPrinter.print(l);
					}
					csvPrinter.println();
				}
			}

			writer.flush();
			writer.close();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static void csv2resourceBundles(List<String> argList) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(OUTPUT_FILE_NAME),
			        OUTPUT_ENCODING));

			CSVStrategy csvStrategy = new CSVStrategy(separator.charAt(0), '"', '#');
			CSVParser csvParser = new CSVParser(reader, csvStrategy);

			String[][] values = csvParser.getAllValues();
			String[] headers = values[0];

			Map<String, Properties> translations = new HashMap<String, Properties>();
			List<String> locales = new ArrayList<String>();

			String dir = getArg("-dir=", argList);

			for (int i = 1; i < headers.length; i++) {
				String locale = headers[i];
				Properties p = new SortedProperties();
				if (dir != null) {
					String fileName = dir + "/" + INPUT_FILE_NAME + "_" + locale + EXTENSION;
					File file = new File(fileName);
					if (file.exists()) {
						Reader r = new InputStreamReader(new FileInputStream(file), INPUT_ENCODING);
						p.load(r);
						r.close();
					}
				}
				translations.put(locale, p);
				locales.add(locale);
			}

			for (int i = 1; i < values.length; i++) {
				String[] strings = values[i];
				String id = strings[0];
				for (int j = 0; j < locales.size(); j++) {
					if (strings.length > j + 1) {
						String translation = strings[j + 1];
						if (translation != null && !translation.isEmpty()) {
							translations.get(locales.get(j)).setProperty(id, translation);
						}
					}
				}
			}

			for (Map.Entry<String, Properties> entry : translations.entrySet()) {
				Properties properties = entry.getValue();
				String fileName = INPUT_FILE_NAME + "_" + entry.getKey() + EXTENSION;
				PrintWriter writer = new PrintWriter(fileName, INPUT_ENCODING);
				properties.store(writer, null);
				writer.flush();
				writer.close();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String[] listLocales(String dirName) {
		File dir = new File(dirName);
		List<String> locales = new ArrayList<String>();

		String[] fileNames = dir.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(INPUT_FILE_NAME) && name.endsWith(EXTENSION);
			}
		});

		for (String fileName : fileNames) {
			int beginIndex = INPUT_FILE_NAME.length() + 1;
			int endIndex = fileName.length() - EXTENSION.length();
			if (beginIndex < endIndex) {
				String localeId = fileName.substring(beginIndex, endIndex);
				locales.add(localeId);
			}
		}

		return locales.toArray(new String[locales.size()]);
	}

	private static String getArg(String prefix, List<String> argList) {
		for (String arg : argList) {
			if (arg.startsWith(prefix)) {
				return arg.substring(prefix.length());
			}
		}
		return null;
	}

}

class SortedProperties extends Properties {

	private static final long serialVersionUID = 8402876826957055423L;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Enumeration keys() {
		Enumeration keysEnum = super.keys();
		Vector<String> keyList = new Vector<String>();
		while (keysEnum.hasMoreElements()) {
			keyList.add((String) keysEnum.nextElement());
		}
		Collections.sort(keyList);
		return keyList.elements();
	}

}
