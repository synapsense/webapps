package com.synapsense.jskinslicer;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import javax.imageio.ImageIO;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ImageSlicer {
	BufferedImage image;
	Document doc;
	XmlHelper xml;
	String outrootdir;
	
	public ImageSlicer(String xmlFileName) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException, JSkinSlicerException {
		try {
			doc = XmlHelper.loadXmlDocunment(xmlFileName);
		} catch(IOException e) {
			throw new JSkinSlicerException("Failed to load input xml " + new File(xmlFileName).getCanonicalPath(), e);
		}		
   		xml = new XmlHelper(doc);
   		String imageFile = xml.querySingleNodeValue("slicer/@srcimg");
   		image = loadImage(imageFile);
   		outrootdir = xml.querySingleNodeValue("slicer/@outrootdir");
   		System.out.println("Xml: " + new File(xmlFileName).getCanonicalPath());
   		System.out.println("Source image: " + new File(imageFile).getCanonicalPath());
   		System.out.println("Output dir: " + new File(outrootdir).getCanonicalPath());
	}
	
	public void Execute() throws XPathExpressionException, ClassNotFoundException, IOException, SecurityException, NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException, InvocationTargetException {
		NodeList commands = xml.queryNodeList("slicer/command");
		JSkinSlicerCommands executer = new JSkinSlicerCommands(image, outrootdir, xml);
		Rectangle rect = new Rectangle(0, 0, image.getWidth(), image.getHeight());
		for(int i = 0; i < commands.getLength(); i++) {
			Node command = commands.item(i);
			
			executer.Execute(command, new CommandData(rect, new File(outrootdir), null));
		}
	}
		
	protected BufferedImage loadImage(String imageFileName) throws JSkinSlicerException, IOException {		
		try {
			return ImageIO.read(new File(imageFileName));
		} catch(Exception e) {
			File file = new File(imageFileName);
			throw new JSkinSlicerException("Failed to load input image " + file.getCanonicalPath(), e);
		}
	}
}
