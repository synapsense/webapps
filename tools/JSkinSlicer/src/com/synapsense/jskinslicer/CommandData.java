package com.synapsense.jskinslicer;

import java.awt.Rectangle;
import java.io.File;

import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Node;

public class CommandData {
	XmlHelper xml = new XmlHelper(null);
	public Rectangle rect;
	public File dir;
	public String file;
	
	public CommandData(Rectangle rect, File dir, String file) {
		this.rect = rect;
		this.dir = dir;
		this.file = file;
	}
	
	public CommandData(Node command, CommandData commandData) throws XPathExpressionException {
		rect = new Rectangle(commandData.rect);
		rect.x += queryNodeIntValue(command, "@x", 0);
		rect.y += queryNodeIntValue(command, "@y", 0);
		rect.width = queryNodeIntValue(command, "@w", rect.width);
		rect.height = queryNodeIntValue(command, "@h", rect.height);
		dir = commandData.dir;
		file = commandData.file;
		
		String dirStr = xml.querySingleNodeValue(command, "@dir");
		if(dirStr != null) {
			dir = dir == null ? new File(dirStr) : new File(dir, dirStr);
		}
		
		String fileStr = xml.querySingleNodeValue(command, "@file");
		if(fileStr != null) {
			file = file == null ? fileStr : file + fileStr;
		}
	}
	
	public File getPath() {
		return file == null ? new File(dir.getPath()) : new File(dir, file); 
	}
	
	private int queryNodeIntValue(Node command, String xpath, int defaultValue) throws XPathExpressionException {
		String strValue = xml.querySingleNodeValue(command, xpath);
		if(strValue == null) {
			return defaultValue;
		}
		try {
			return Integer.parseInt(strValue);
		} catch(NumberFormatException e) {
			return defaultValue;
		}
	}
}
