package com.synapsense.jskinslicer;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JSkinSlicerCommands {
	XmlHelper xml;
	String outrootdir;
	BufferedImage image;
	
	public JSkinSlicerCommands(BufferedImage image, String outrootdir, XmlHelper xml) {
		this.image = image;
		this.outrootdir = outrootdir;
		this.xml = xml;
	}
	
	public void Execute(Node command, CommandData commandData) throws XPathExpressionException {
		String name = null;;
		try {
			name = xml.querySingleNodeValue(command, "@name");
			name = name == null ? "undefined" : name.toLowerCase();
			
			CommandData commandDataDerived = new CommandData(command, commandData);
			//printExecutingMessage(command);
			if(name.equals("rect")) {
				Rect(commandDataDerived);
			} else if(name.equals("selectdir")) {
				SelectDir(command, commandDataDerived);
			} else if(name.equals("iteratedir")) {
				IterateDir(command, commandDataDerived);
			} else if(name.equals("iteratefile")) {
				IterateFile(command, commandDataDerived);
			} else {
				System.out.println("Command not supported: " + name);
			}
		} catch(Exception e) {
			System.out.println("Command exception: " + name + " " + e.getMessage());
		}
	}
	
	protected void SelectDir(Node command, CommandData commandData) throws XPathExpressionException, IOException {
		//System.out.println(commandData.getPath().getCanonicalPath());
		NodeList commands = xml.queryNodeList(command, "command");
		for(int i = 0; i < commands.getLength(); i++) {
			Node subcommand = commands.item(i);			
			this.Execute(subcommand, commandData);
		}
	}
	
	protected void IterateDir(Node command, CommandData commandData) throws XPathExpressionException, IOException {
		//System.out.println(commandData.getPath().getCanonicalPath());
		String [] dirs = xml.querySingleNodeValue(command, "@dirs").split(",");
		String xsstr = xml.querySingleNodeValue(command, "@xs");
		String [] xs = null;
		if(xsstr != null) {
			xs = xsstr.split(",");
		}
		String ysstr = xml.querySingleNodeValue(command, "@ys");
		String [] ys = null;
		if(ysstr != null) {
			ys = ysstr.split(",");
		}
		String wsstr = xml.querySingleNodeValue(command, "@ws");
		String [] ws = null;
		if(wsstr != null) {
			ws = wsstr.split(",");
		}
		String hsstr = xml.querySingleNodeValue(command, "@hs");
		String [] hs = null;
		if(hsstr != null) {
			hs = hsstr.split(",");
		}
		
		for(int i = 0; i < dirs.length; i++) {
			CommandData commandDataDerived = new CommandData(command, commandData);
			commandDataDerived.dir = new File(commandData.dir, dirs[i]); 
			commandDataDerived.rect.x += xs == null ? 0 : Integer.parseInt(xs[i]); 
			commandDataDerived.rect.y += ys == null ? 0 : Integer.parseInt(ys[i]); 
			commandDataDerived.rect.width = ws == null ? commandDataDerived.rect.width : Integer.parseInt(ws[i]); 
			commandDataDerived.rect.height = hs == null ? commandDataDerived.rect.height : Integer.parseInt(hs[i]); 
			NodeList commands = xml.queryNodeList(command, "command");
			for(int c = 0; c < commands.getLength(); c++) {
				Node subcommand = commands.item(c);			
				this.Execute(subcommand, commandDataDerived);
			}
		}		
	}
	
	protected void IterateFile(Node command, CommandData commandData) throws XPathExpressionException, IOException {
		//System.out.println(commandData.getPath().getCanonicalPath());
		String [] files = xml.querySingleNodeValue(command, "@files").split(",");
		String xsstr = xml.querySingleNodeValue(command, "@xs");
		String [] xs = null;
		if(xsstr != null) {
			xs = xsstr.split(",");
		}
		String ysstr = xml.querySingleNodeValue(command, "@ys");
		String [] ys = null;
		if(ysstr != null) {
			ys = ysstr.split(",");
		}
		String wsstr = xml.querySingleNodeValue(command, "@ws");
		String [] ws = null;
		if(wsstr != null) {
			ws = wsstr.split(",");
		}
		String hsstr = xml.querySingleNodeValue(command, "@hs");
		String [] hs = null;
		if(hsstr != null) {
			hs = hsstr.split(",");
		}
		
		for(int i = 0; i < files.length; i++) {
			CommandData commandDataDerived = new CommandData(command, commandData);
			commandDataDerived.file = commandData.file == null ? files[i] : commandData.file + files[i]; 
			commandDataDerived.rect.x += xs == null ? 0 : Integer.parseInt(xs[i]); 
			commandDataDerived.rect.y += ys == null ? 0 : Integer.parseInt(ys[i]); 
			commandDataDerived.rect.width = ws == null ? commandDataDerived.rect.width : Integer.parseInt(ws[i]); 
			commandDataDerived.rect.height = hs == null ? commandDataDerived.rect.height : Integer.parseInt(hs[i]); 
			NodeList commands = xml.queryNodeList(command, "command");
			for(int c = 0; c < commands.getLength(); c++) {
				Node subcommand = commands.item(c);			
				this.Execute(subcommand, commandDataDerived);
			}
		}		
	}
	
	protected void Rect(CommandData commandData) throws IOException {
		if(commandData.rect.width <= 0 || commandData.rect.height <= 0) {
			System.out.println("!!! " + commandData.getPath().getCanonicalPath() + " " + commandData.rect);
			return;
		}
		BufferedImage rect = image.getSubimage(commandData.rect.x, commandData.rect.y, commandData.rect.width, commandData.rect.height);
		if(commandData.file != null) {
			String imageFormatName = getFileExtension(commandData.file);
			File path = new File(commandData.getPath().getCanonicalPath());
			path.mkdirs();
			ImageIO.write(rect, imageFormatName, path);
			System.out.println(commandData.getPath().getCanonicalPath() + " " + commandData.rect);
		} else {
			System.out.println("commandData.file == null");
		}
	}
	
	protected File getSavePath(String outdir, String filename) {
		File pathOutDir = new File(outrootdir, outdir);
		File path = new File(pathOutDir, filename);
		path.mkdirs();
		return path;
	}
	
	protected String getFileExtension(String filename) {
		return filename.substring(filename.lastIndexOf('.') + 1, filename.length());
	}
	
	protected void printExecutingMessage(Node command) throws XPathExpressionException {
		NamedNodeMap attrs = command.getAttributes();
		String message = "Executing command: " + xml.querySingleNodeValue(command, "@name") + " (";
		String prefix = "";
		for(int i = 0; i < attrs.getLength(); i++) {
			Node attr = attrs.item(i);
			String attrName = attr.getNodeName();
			String attrValue = attr.getNodeValue();
			if(!attrName.equalsIgnoreCase("name")) {
				message += prefix + attrName + "=" + attrValue;
				prefix = " ";
			}
		}
		message += ")";
		System.out.println();
		System.out.println(message);
	}
}
