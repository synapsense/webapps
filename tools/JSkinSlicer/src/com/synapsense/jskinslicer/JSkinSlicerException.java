package com.synapsense.jskinslicer;

public class JSkinSlicerException extends Exception {
	private static final long serialVersionUID = 3585296672382331368L;
	public JSkinSlicerException(String message) {
		super(message);
	}
	public JSkinSlicerException(String message, Throwable cause) {
		super(message, cause);
	}
}
