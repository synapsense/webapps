package com.synapsense.jskinslicer;

public class JSkinSlicer {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			checkArgs(args);
			ImageSlicer slicer = new ImageSlicer(args[0]);
			slicer.Execute();			
		} catch(JSkinSlicerException e) {
			System.err.println();
			System.err.println(e);
		} catch(Exception e) {
			System.err.println();
			System.err.println("Unhandled exception:");
			System.err.println(e);
		}
	}
	
	protected static void checkArgs(String [] args) throws JSkinSlicerException {
		if(args.length != 1) {
			throw new JSkinSlicerException("Invalid arguments");
		}
	}
}
