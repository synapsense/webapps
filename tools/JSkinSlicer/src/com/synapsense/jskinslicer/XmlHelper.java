package com.synapsense.jskinslicer;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlHelper {
	Document doc;
	XPathFactory xpathFactory;
	XPath xpathOpj;
	
	public XmlHelper(Document doc) {
		this.doc = doc;
   		xpathFactory = XPathFactory.newInstance();
   		xpathOpj = xpathFactory.newXPath();
	}
		
	public static Document loadXmlDocunment(String xmlFileName) throws ParserConfigurationException, SAXException, IOException {		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = factory.newDocumentBuilder();
		return docBuilder.parse(new File(xmlFileName));
	}
	
	public String [] queryNodeListValues(String xpath) throws XPathExpressionException {
   		return queryNodeListValues(doc, xpath);
   	}

	public String [] queryNodeListValues(Node node, String xpath) throws XPathExpressionException {
		ArrayList<String> valueList = new ArrayList<String>();
   		NodeList nodes = queryNodeList(node, xpath);
   		for(int i = 0; i < nodes.getLength(); i++) {
   			Node nodeitem = nodes.item(i);
   			valueList.add(nodeitem.getNodeValue());
   		}
   		return valueList.toArray(new String[valueList.size()]);
   	}
	
	public String querySingleNodeValue(String xpath) throws XPathExpressionException {
   		return querySingleNodeValue(doc, xpath);
   	}

	public String querySingleNodeValue(Node node, String xpath) throws XPathExpressionException {
		String value = null;
   		Node nodeitem = querySingleNode(node, xpath);
   		if(nodeitem != null) {
   			value = nodeitem.getNodeValue();
   		}
   		return value;
   	}
		
	public Node querySingleNode(String xpath) throws XPathExpressionException {
   		return querySingleNode(doc, xpath);
   	}
	
	public Node querySingleNode(Node node, String xpath) throws XPathExpressionException {
   		Node nodeitem = null;
   		XPathExpression expr = xpathOpj.compile(xpath);
   		nodeitem = (Node)expr.evaluate(node, XPathConstants.NODE);
   		return nodeitem;
   	}
   	
	public NodeList queryNodeList(String xpath) throws XPathExpressionException {
   		return queryNodeList(doc, xpath);
   	}
	
	public NodeList queryNodeList(Node node, String xpath) throws XPathExpressionException {
   		NodeList nodes = null;
   		XPathExpression expr = xpathOpj.compile(xpath);
   		nodes = (NodeList)expr.evaluate(node, XPathConstants.NODESET);
   		return nodes;
   	}
}