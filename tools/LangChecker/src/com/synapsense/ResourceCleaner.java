package com.synapsense;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;


public class ResourceCleaner {

	
	final static private String FILE_NAME = "resources.txt";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {

			Map<String, Properties> bundles= getBundles();
			
	        BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME));
	        String line;
	        while ((line = reader.readLine()) != null) {
	        	line = line.trim();
	        	if (!line.isEmpty()) {
	        		for (Properties properties : bundles.values()) {
	        			properties.remove(line);
	        		}
	        	}
	        }
	        reader.close();
	        
	        for (Map.Entry<String, Properties> entry : bundles.entrySet()) {
	        	Properties properties = entry.getValue();
				FileOutputStream fos = new FileOutputStream(new File(entry.getKey() + "_"));
				OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
				properties.store(osw, null);

				osw.close();
				fos.close();
	        }
	        
	        
        } catch (Exception e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }

	}

	private static Map<String, Properties>  getBundles() throws UnsupportedEncodingException, FileNotFoundException, IOException {
        File f = new File(".");
		String[] propFiles = f.list(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				String f = new File(name).getName();
				return f.endsWith(".properties");
			}

		});
        
		Map<String, Properties> bundles = new HashMap<String, Properties>(propFiles.length); 
		
		for (String propFile: propFiles) {
			SortedProperties properties = new SortedProperties();
			properties.load(new InputStreamReader(new FileInputStream(propFile), "UTF-8"));
			bundles.put(propFile, properties);
		}
		
		return bundles;
	}
	
	static class SortedProperties extends Properties {
        private static final long serialVersionUID = 9126878878618065532L;

		@SuppressWarnings({ "rawtypes", "unchecked" })
        public Enumeration keys() {
		     Enumeration<Object> keysEnum = super.keys();
		     Vector<String> keyList = new Vector<String>();
		     while(keysEnum.hasMoreElements()){
		       keyList.add((String)keysEnum.nextElement());
		     }
		     Collections.sort(keyList);
		     return keyList.elements();
		  }
		  
		}

}
