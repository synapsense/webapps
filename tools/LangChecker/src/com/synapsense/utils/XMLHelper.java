package com.synapsense.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class XMLHelper {

	/**
	 * Retrieves the string version of an XML document
	 * 
	 * @param xmlDoc
	 * @return
	 */
	public static String getDocumentXml(Document xmlDoc) {
		// set up a transformer
		try {
			TransformerFactory transfac = TransformerFactory.newInstance();
			Transformer trans = transfac.newTransformer();
			trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			trans.setOutputProperty(OutputKeys.INDENT, "yes");

			// create string from xml tree
			StringWriter sw = new StringWriter();
			// sw.write("<foo>bar</foo>");
			StreamResult result = new StreamResult(sw);
			DOMSource source = new DOMSource(xmlDoc);

			if (source == null || result == null || trans == null) {
				System.err.println("PROBLEM FOOL...");
			}
			trans.transform(source, result);
			String xmlString = sw.toString();

			return xmlString;
		} catch (TransformerConfigurationException tce) {
			System.err.println("getDocumentXml()" + tce);
			return null;
		} catch (TransformerException te) {
			System.err.println("getDocumentXml()" + te);
			return null;
		}
	}

	public static String retrieveNewFile(Document document) {

		DOMImplementation domImplementation = document.getImplementation();
		if (domImplementation.hasFeature("LS", "3.0")
				&& domImplementation.hasFeature("Core", "2.0")) {
			DOMImplementationLS domImplementationLS = (DOMImplementationLS) domImplementation
					.getFeature("LS", "3.0");
			LSSerializer lsSerializer = domImplementationLS
					.createLSSerializer();
			DOMConfiguration domConfiguration = lsSerializer.getDomConfig();

			if (domConfiguration.canSetParameter("format-pretty-print",
					Boolean.TRUE)) {
				lsSerializer.getDomConfig().setParameter("format-pretty-print",
						Boolean.TRUE);
			} else {
				System.err
						.println("DOMConfiguration 'format-pretty-print' parameter isn't settable.");
			}
			LSOutput lsOutput = domImplementationLS.createLSOutput();
			lsOutput.setEncoding("UTF-8");
			Writer stringWriter = new StringWriter();
			lsOutput.setCharacterStream(stringWriter);
			lsSerializer.write(document, lsOutput);
			return stringWriter.toString();

		} else {
			throw new RuntimeException(
					"DOM 3.0 LS and/or DOM 2.0 Core not supported.");
		}
	}

	public static void addNode(Element root, String xPath, String textValue)
			throws AddNodeException {
		int index = xPath.indexOf("/");
		String fNodeName = index == -1 ? xPath : xPath.substring(0, index);
		Set<Node> nodeList = findNode(root,fNodeName);
		Element nextRoot = null;
		boolean newNode = false;
		if (nodeList.size() == 1) {
			nextRoot = (Element) nodeList.iterator().next();
		} else if (nodeList.size() == 0) {
			newNode = true;
			nextRoot = root.getOwnerDocument().createElement(fNodeName);
			root.appendChild(nextRoot);
		} else {
			throw new AddNodeException(
					"There is more than one node: " + fNodeName + " in " + root);
		}
		if (index == -1) {
			if (newNode) {
				nextRoot.appendChild(root.getOwnerDocument().createTextNode(
						textValue));
			}
			return;
		} else {
			addNode(nextRoot, xPath.substring(index + 1), textValue);
		}
	}
	
	private static Set<Node> findNode (Node node,String tagName) {
		NodeList children = node.getChildNodes();
		Set<Node> set = new LinkedHashSet<Node>();
		for (int i=0;i<children.getLength();i++) {
			Node child = children.item(i);
			if (child.getNodeName().equals(tagName)) {
				set.add(child);
			}
		}
		return set;
	}

	public static Document loadXml(String fileAbsolutePath) throws IOException {
		Document xmlDoc = null;
		try {
			DocumentBuilderFactory factory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			xmlDoc = builder.parse(new FileInputStream(fileAbsolutePath));
		} catch (ParserConfigurationException pcE) {
			System.err.println(pcE.getMessage());
		} catch (SAXException sE) {
			System.err.println(sE.getMessage());
		}
		return xmlDoc;
	}

	public static class AddNodeException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 2321356633194966486L;

		public AddNodeException(String message, Throwable cause) {
			super(message, cause);
		}

		public AddNodeException(String message) {
			super(message);
		}
	}
}
