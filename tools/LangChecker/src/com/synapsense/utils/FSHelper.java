package com.synapsense.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.Set;

public class FSHelper {
	public static void writeFile (File file,char[] data,WriteMode mode,Charset charset) 
		throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(createOStream(file.getCanonicalPath(), mode),charset));
		writer.write(data);
		writer.close();
	}

	public static void writeFile (File file,String data,WriteMode mode) 
		throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(createOStream(file.getCanonicalPath(), mode)));
		writer.write(data);
		writer.close();
	}

	private static OutputStream createOStream(String fileName, WriteMode mode)
			throws IOException {
		File file = new File(fileName);
		switch (mode) {
		case APPEND:
			if (!file.exists())
				throw new IOException("The file " + file.getAbsolutePath()
						+ " does not exist");
			break;
		case REWRITE:
			file.delete();
			break;
		case CHANGE:
			break;
		default:
			throw new IllegalStateException(
					"Forgot to update the switch statement");
		}
		return new FileOutputStream(file, mode.equals(WriteMode.APPEND));
	}
	
	public static String asString (File file) 
		throws IOException {
		StringBuilder bld = new StringBuilder();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		short bufferSize = 100;
		char[] data = new char[bufferSize];
		int charsRead = 0;
		while((charsRead = reader.read(data,0,bufferSize)) != -1) {
			bld.append(data,0,charsRead);			
		}
		reader.close();
		return bld.toString();
	}
	
	public static void searchFiles (File root,String[] fileExtensionsToSearch,Set<File> filesBuffer) {
		if (!root.exists()) {
			return;
		}
		if (root.isFile()) {
			for (String ext:fileExtensionsToSearch) {
				if (root.getAbsolutePath().endsWith(ext)) {
					filesBuffer.add(root);
					break;
				}
			}
			return;
		}
		for (File subfile:root.listFiles()) {
			searchFiles(subfile,fileExtensionsToSearch,filesBuffer);
		}
		return;
	}
	
	public static enum WriteMode {
		APPEND,
		REWRITE,
		CHANGE
	};
}