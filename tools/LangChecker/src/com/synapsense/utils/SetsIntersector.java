package com.synapsense.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This class computes intersection of two sets - A^B
 * @author anechaev
 *
 * @param <T>
 */
public class SetsIntersector<T> {
	public static class Difference<T> extends Pair<Collection<T>, Collection<T>> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7194341934697626904L;

		public Difference(Collection<T> first, Collection<T> second) {
			super(first, second);
		}
		
		public Collection<T> getFMinusS() {
			return first;
		}
		
		public Collection<T> getSMinusF() {
			return second;
		}
	}
	
	public static class AllValues<T> extends Pair<Collection<T>, Difference<T>> {
		/**
		 * 
		 */
		private static final long serialVersionUID = 7957530715339197372L;

		public AllValues(Collection<T> first, Difference<T> second) {
			super(first, second);
		}
		
		public Collection<T> getIntersection() {
			return first;
		}
		
		public Difference<T> getDifference() {
			return second;
		}
	}
	
	/**
	 * Computes an intersection of two sets: first^second
	 * @param first
	 * @param second
	 * @param comparator
	 * @return first^second
	 */
	public Collection<T> intersect(Collection<T> first, Collection<T> second, Comparator<T> comparator) {
		return doAllJob(first, second, comparator).getIntersection();
	}
	
	/**
	 * Computes the difference between two sets : [first - second, second - first] 
	 * (what to remove from the first set and what to add to the first set to compute first ^ second).
	 * @param first - first set
	 * @param second - second set
	 * @param comparator - Comparator implementation to compare elements of sets 
	 * @return a pair containing [first - second, second - first]
	 */
	public Difference<T> computeDifference(Collection<T> first, Collection<T> second, Comparator<T> comparator) {
		return doAllJob(first, second, comparator).getDifference();
	}
	
	/**
	 * Computes both intersection and difference of two sets
	 * @param first
	 * @param second
	 * @param comparator
	 * @return [first ^ second, [first - second], [second - first]]
	 */
	public AllValues<T> doAllJob(Collection<T> first, Collection<T> second, Comparator<T> comparator) {
		AllValues<T> result = new AllValues<T>(new ArrayList<T>(), new Difference<T>(new ArrayList<T>(), new ArrayList<T>()));

		//algo: sort'em all then merge
		//the algo complexity looks like nlogn + mlogm + n + m (n - power of first set, m - power of second one);
		//for unsorted merging the compexity is about n*m;
		//for the equal powered sets first algo's complexity is nlogn, and second one has pow(n,2) complexity;
		
		List<T> f = new ArrayList<T>(first);
		List<T> s = new ArrayList<T>(second);
		Collections.sort(f, comparator);
		Collections.sort(s, comparator);
		
		int fi = 0, si = 0;
		while(fi < f.size() && si < s.size()) {
			int comp = comparator.compare(f.get(fi), s.get(si));
			if(comp < 0) {
				result.getSecond().getFirst().add(f.get(fi));
				fi++;
				continue;
			}
			if(comp == 0) {
				result.getFirst().add(f.get(fi));
				fi++;
				si++;
				continue;
			}
			result.getSecond().getSecond().add(s.get(si));
			si++;
		}
		for(; fi < f.size(); fi++) {
			result.getSecond().getFirst().add(f.get(fi));
		}
		for(; si < s.size(); si++) {
			result.getSecond().getSecond().add(s.get(si));
		}
		return result;
	}
}
