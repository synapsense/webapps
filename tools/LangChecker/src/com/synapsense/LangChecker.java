package com.synapsense;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.utils.FSHelper;
import com.synapsense.utils.Pair;
import com.synapsense.utils.SetsIntersector;
import com.synapsense.utils.XMLHelper;
import com.synapsense.utils.FSHelper.WriteMode;
import com.synapsense.utils.SetsIntersector.Difference;
import com.synapsense.utils.XMLHelper.AddNodeException;

public class LangChecker {
	public static final String F_S = System
			.getProperty("file.separator");
	public static final String USER_DIR = System.getProperty("user.dir");

	public static final String DIFF_PROJECT_PATH_DEFAULT = USER_DIR;
	public static final String[] DIFF_LZX_HOME_DEFAULT = {"./"};
	public static final String[] DIFF_FILE_EXTENSION_DEFAULT = {".lzx",".xml",".java"};
	public static final String DIFF_LOCALE_DEFAULT = "en_US";
	public static final String DIFF_RES_FILE_PATH_DEFAULT = "resources/strings_en_US.properties";
	public static final String DIFF_LZX_COMPONENTS_PATH_DEFAULT = "../lzx";
	
	public static final String CONVERT_PROJECT_PATH_DEFAULT = USER_DIR;
	public static final String[] CONVERT_LZX_HOME_DEFAULT = {"./"};
	public static final String[] CONVERT_FILE_EXTENSION_DEFAULT = {".lzx",".xml"};
	public static final String[] CONVERT_LOCALES_DEFAULT = {"En"};
	public static final String CONVERT_LANG_XML_PATH_DEFAULT = "resources/LangData.xml";
	
	public static final String COMPARE_PROJECT_PATH_DEFAULT = USER_DIR;
	public static final String COMPARE_LANG_XML_PATH_DEFAULT = "resources/LangData.xml";
	
	public static final String EQUATE_PROJECT_PATH_DEFAULT = USER_DIR;
	public static final String EQUATE_LANG_XML_PATH_DEFAULT = "resources/LangData.xml";

	public static final String ERR_EMPTY_OPTION = "Option can not be empty";
	public static final String ERR_INCORRECT_PROJECT_PATH = "Incorrect project directory";
	public static final String MESS_HELP = "For more information use LangChecker -help";
	public static final String MESS_APP_HELP[] = {"Commands:",
		"diff Find difference between LangData.xml and *.lzx files.",
		"-help Help."};
	public static final String MESS_DIFF_HELP[] = {"[options]",
		"-path  Absolute Project path. Default is directory from call LangChecker.",
		"-laszlohome||-lh Relative path to find project's lzx files. \n Exemple ./;.lzx;../somedirectory",
		"-locale||-loc ID of locale in LangData.xml.",
		"-resfile||-rf Relative path to find resource file file.",
		"-lzxcomp Relative path to global Synapsence LZX components.",
		"-help Help."};
	public static final String MESS_CONVERT_HELP[] = {"[options]",
		"-path  Absolute Project path. Default is directory from call LangChecker.",
		"-laszlohome||-lh Relative path to find project's lzx files. \n Exemple ./;.lzx;../somedirectory",
		"-fileextension||-fe Array of file's extension to search. Example .lzx;.xml or .lzx",
		"-locale||-loc ID of locale in LangData.xml.",
		"-langxml||-lxml Relative path to find LangData.xml file.",
		"-help Help."};
	public static final String MESS_COMPARE_HELP[] = {"[options]",
		"-path  Absolute Project path. Default is directory from call LangChecker. Optional.",
		"-langxml||-lxml Relative path to find LangData.xml file. Optional.",
		"-whichlocale||-fl Locale name that will be evaluated. Required.",
		"-withlocale||-sl Locale name that will be used as template. Required."};
	public static final String MESS_EQUATE_HELP[] = {
		"-path  Absolute Project path. Default is directory from call LangChecker. Optional.",
		"-langxml||-lxml Relative path to find LangData.xml file. Optional.",
		"-whichlocale||-fl Locale name that will be completed. Required.",
		"-withlocale||-sl Locale name that will be used as template. Required."
	};

	private static final Pattern[] patternsToSearch = {
		Pattern.compile("[^a-zA-Z]prompt\\s*[=|:]\\s*['|\"](.*?)['|\"]"),
		Pattern.compile(" mappedname\\s*[=|:]\\s*['|\"](.*?)['|\"]"),
		Pattern.compile(" shortname\\s*[=|:]\\s*['|\"](.*?)['|\"]"),
		Pattern.compile(" long\\s*[=|:]\\s*['|\"](.*?)['|\"]"),
		Pattern.compile("LangManager\\.getLocaleString\\s*\\(\\s*['|\"](.+?)['|\"]\\s*\\)"),
		Pattern.compile("LangManager\\.getFormattedLocaleString\\s*\\(\\s*['|\"](.+?)['|\"]\\s*\\,"),
		Pattern.compile("['|\"](ESConfigurator[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](button[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](columnname[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](context_menu[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](cron[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](dc_properties[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](dataview[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](dataviewlinker[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](enhalerting[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](groups[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](menu[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](message[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](object_details[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](statusbar[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](support[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](tab[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](tabular_templates[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](text[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](title[/|_][\\w|\\-|/]+)"),
		Pattern.compile("['|\"](tooltip[/|_][\\w|\\-|/]+)"),
	};

	public static void main(String[] args) throws IOException {
		System.exit(execute(args));
	}

	public static int execute(String[] args) throws IOException {
		if (args.length == 0) {
			System.err.println(MESS_HELP);
			return 1;
		}
		Iterator<String> argslist = Arrays.asList(args).iterator();
		String operation = argslist.next();
		if (operation.equals("diff")) {
			String pathOption = DIFF_PROJECT_PATH_DEFAULT;
			String[] lzxHomeOption = DIFF_LZX_HOME_DEFAULT;
			String[] fileExtensionsOption = DIFF_FILE_EXTENSION_DEFAULT;
			String localeOption = DIFF_LOCALE_DEFAULT;
			String resFilePathOption = DIFF_RES_FILE_PATH_DEFAULT;
			String lzxComponentsPathOption = DIFF_LZX_COMPONENTS_PATH_DEFAULT;
			while (argslist.hasNext()) {
				String option = argslist.next();
				if (option.equals("-path")) {
					if (argslist.hasNext()) {
						pathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-laszlohome") || option.equals("-lh")) {
					if (argslist.hasNext()) {
						Set<String> tempPaths = new LinkedHashSet<String>(Arrays.asList(argslist.next().split(";")));
						lzxHomeOption = tempPaths.toArray(new String[tempPaths.size()]);
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-fileextension") || option.equals("-fe")) {
					if (argslist.hasNext()) {
						Set<String> tempExts = new LinkedHashSet<String>(Arrays.asList(argslist.next().split(";")));
						fileExtensionsOption = tempExts.toArray(new String[tempExts.size()]);
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-locale") || option.equals("-loc")) {
					if (argslist.hasNext()) {
						localeOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-resfile") || option.equals("-rf")) {
					if (argslist.hasNext()) {
						resFilePathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-lzxcomp")) {
					if (argslist.hasNext()) {
						lzxComponentsPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-help")) {
					for (String str:MESS_DIFF_HELP) {
						System.out.println(str);
					}
					return 0;
				}
			}
			if (!checkProjectHome(pathOption, resFilePathOption, lzxHomeOption)) {
				return makeError(ERR_INCORRECT_PROJECT_PATH+": " + pathOption, 1);
			}
			System.out.println("Finding difference:");
			Difference<String> diff = findDifference(pathOption,
					resFilePathOption,localeOption,
					lzxHomeOption,
					lzxComponentsPathOption,
					fileExtensionsOption);
			System.out.println("Unused data in LangData.xml:");
			for (String str:diff.getFMinusS()) {
				System.out.println(str);
			}
			System.out.println("======================================");
			System.out.println("There is no text for path in LangData.xml:");
			for (String str:diff.getSMinusF()) {
				System.out.println(str);
			}
		} else if (operation.equals("convert")) {
			String pathOption = CONVERT_PROJECT_PATH_DEFAULT;
			String[] lzxHomeOption = CONVERT_LZX_HOME_DEFAULT;
			String[] fileExtensionsOption = CONVERT_FILE_EXTENSION_DEFAULT;
			String[] localesOption = CONVERT_LOCALES_DEFAULT;
			String langXMLPathOption = CONVERT_LANG_XML_PATH_DEFAULT;
			while (argslist.hasNext()) {
				String option = argslist.next();
				if (option.equals("-path")) {
					if (argslist.hasNext()) {
						pathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-laszlohome") || option.equals("-lh")) {
					if (argslist.hasNext()) {
						Set<String> tempPaths = new LinkedHashSet<String>(Arrays.asList(argslist.next().split(";")));
						lzxHomeOption = tempPaths.toArray(new String[tempPaths.size()]);
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-fileextension") || option.equals("-fe")) {
					if (argslist.hasNext()) {
						Set<String> tempExts = new LinkedHashSet<String>(Arrays.asList(argslist.next().split(";")));
						fileExtensionsOption = tempExts.toArray(new String[tempExts.size()]);
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-locale") || option.equals("-loc")) {
					if (argslist.hasNext()) {
						Set<String> tempLocales = new LinkedHashSet<String>(Arrays.asList(argslist.next().split(";")));
						localesOption = tempLocales.toArray(new String[tempLocales.size()]);
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-langxml") || option.equals("-lxml")) {
					if (argslist.hasNext()) {
						langXMLPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-help")) {
					for (String str:MESS_CONVERT_HELP) {
						System.out.println(str);
					}
					return 0;
				}
			}
			return makeConvertion(pathOption, 
					langXMLPathOption, localesOption, 
					lzxHomeOption, fileExtensionsOption);
		} else if (operation.equals("compare")) {
			String projectPathOption = COMPARE_PROJECT_PATH_DEFAULT;
			String langDataPathOption = COMPARE_LANG_XML_PATH_DEFAULT;
			String whichLocaleOption = null;
			String withLocaleOption = null;
			while (argslist.hasNext()) {
				String option = argslist.next();
				if (option.equals("-path")) {
					if (argslist.hasNext()) {
						projectPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-langxml") || option.equals("-lxml")) {
					if (argslist.hasNext()) {
						langDataPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-whichlocale") || option.equals("-fl")) {
					if (argslist.hasNext()) {
						whichLocaleOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-withlocale") || option.equals("-sl")) {
					if (argslist.hasNext()) {
						withLocaleOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-help")) {
					for (String str:MESS_COMPARE_HELP) {
						System.out.println(str);
					}
					return 0;
				}
			}
			if (whichLocaleOption == null) {
				return makeError("Option -whichlocale||-fl can not be absent. Use -help.", 1);
			} else if (withLocaleOption == null) {
				return makeError("Option -withlocale||-sl can not be absent. Use -help.", 1);
			}
			return makeComparation(projectPathOption, 
					langDataPathOption, 
					whichLocaleOption, withLocaleOption);
		} else if (operation.equals("equate")) {
			String projectPathOption = EQUATE_PROJECT_PATH_DEFAULT;
			String langDataPathOption = EQUATE_LANG_XML_PATH_DEFAULT;
			String whichLocaleOption = null;
			String withLocaleOption = null;
			while (argslist.hasNext()) {
				String option = argslist.next();
				if (option.equals("-path")) {
					if (argslist.hasNext()) {
						projectPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-langxml") || option.equals("-lxml")) {
					if (argslist.hasNext()) {
						langDataPathOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-whichlocale") || option.equals("-fl")) {
					if (argslist.hasNext()) {
						whichLocaleOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-withlocale") || option.equals("-sl")) {
					if (argslist.hasNext()) {
						withLocaleOption = argslist.next();
					} else {
						return makeError(ERR_EMPTY_OPTION,1);
					}
				} else if (option.equals("-help")) {
					for (String str:MESS_EQUATE_HELP) {
						System.out.println(str);
					}
					return 0;
				}
			}
			if (whichLocaleOption == null) {
				return makeError("Option -whichlocale||-fl can not be absent. Use -help.", 1);
			} else if (withLocaleOption == null) {
				return makeError("Option -withlocale||-sl can not be absent. Use -help.", 1);
			}
			return makeEquate(projectPathOption, 
					langDataPathOption, 
					whichLocaleOption, withLocaleOption);
		} else if (operation.equals("-help")){
			for (String str:MESS_APP_HELP) {
				System.err.println(str);
			}
		} else {
			System.err.println(MESS_HELP);
			return 1;
		}
		return 0;
	}
	
	private static int makeEquate (String projectPath,
			String langDataPath,
			String firstLocale,String secondLocale) 
		throws IOException {
		System.out.println("Start equate.");
		try {
			Map<String,String> diff = doComparation(projectPath,langDataPath,
					firstLocale,secondLocale);
			complementLangDataXML(diff,projectPath+F_S+langDataPath,firstLocale);
			System.out.println("Finish equate.");
			return 0;
		} catch (CompareException e) {
			System.err.println(e.getMessage());
		}
		return 1;
	}
	
	private static int makeComparation (String projectPath,
			String langDataPath,
			String firstLocale,String secondLocale) 
		throws IOException {
		System.out.println("Start compare.");
		try {
			Map<String,String> diff = doComparation(projectPath,langDataPath,
					firstLocale,secondLocale);
			
			System.out.println("In locale " + firstLocale + " no data that are in " + secondLocale);
			for (Entry<String,String> entry:diff.entrySet()) {
				System.out.println("Node: " + entry.getKey() + "->" + entry.getValue());
			}
			System.out.println("Finish compare.");
			return 0;
		} catch (CompareException e) {
			System.err.println(e.getMessage());
		}
		return 1;
	}
		
	private static Map<String,String> doComparation (String projectPath,
			String langDataPath,
			String firstLocale,String secondLocale) 
		throws IOException,CompareException {
		try {
			Map<String,String> fMap = getLangDataMap(projectPath+F_S+langDataPath,firstLocale);
			Map<String,String> sMap = getLangDataMap(projectPath+F_S+langDataPath,secondLocale);
			SetsIntersector<String> comparator = new SetsIntersector<String>();
			Difference<String> diff = comparator.computeDifference(fMap.keySet(), sMap.keySet(), new StringComparator());
			Map<String,String> result = new LinkedHashMap<String, String>();
			for (String key:diff.getSMinusF()) {
				result.put(key, sMap.get(key));
			}
			return result;
		} catch (GetLangDataMapException e) {
			throw new CompareException("Unable to complete compare operation: " + e);
		}
	}
	
	private static int makeError (String message,int returnCode) {
		System.err.println(message);
		return returnCode;
	}
	
	private static int makeConvertion (String projectPath,
			String langXMLPath,String[] localesForAdd,
			String[] lzxHomePaths,String[] fileExtensions) 
		throws IOException {
		System.out.println("Starting conversion");
		try {
			Set<File> lzxFiles = new LinkedHashSet<File>();
			for (String lzxHPath:lzxHomePaths) {
				FSHelper.searchFiles(new File(projectPath+F_S+lzxHPath), fileExtensions, lzxFiles);
			}
			for (String locale:localesForAdd) {
				Map<String, String> langDataMap = getLangDataMap(projectPath+F_S+langXMLPath,locale);
				Map<String,String> difference = executeFileConversion(lzxFiles, langDataMap);
				complementLangDataXML(difference,projectPath+F_S+langXMLPath,locale);
			}
		} catch (GetLangDataMapException e) {
			System.err.println("Unable to complete: " + e);
			return 1;
		}
		System.out.println("Conversion is finished");
		return 0;
	}
	
	private static Map<String,String> executeFileConversion (Set<File> lzxFiles,Map<String, String> langDataMap) 
		throws IOException {
		Map<String,String> difference = new LinkedHashMap<String, String>();
		Pattern patterns[] = patternsToSearch;
		for (File file:lzxFiles) {
			String data = FSHelper.asString(file);
			boolean needRewrite = false;
			for (Pattern pattern : patterns) {
				Matcher matcher = pattern.matcher(data);
				while(matcher.find()) {
					String value = matcher.group(1);
					if (!value.trim().isEmpty()) {
						try {
							Pair<String, String> pathVsText = convertUsageToRealLangDataPath(value);
							if (!langDataMap.containsKey(pathVsText.getFirst())) {
								difference.put(pathVsText.getFirst(), pathVsText.getSecond());
								//Replace
								data = data.substring(0, matcher.start(1)) + 
										pathVsText.getFirst() + data.substring(matcher.end(1),data.length());
								needRewrite = true;
							}
						} catch (ConvertUsageToRealLangDataPathException e) {
							System.err.println(e);
						}
					}
				}
			}
			if (needRewrite) {
				FSHelper.writeFile(file, data, WriteMode.CHANGE);
			}
		}
		return difference;
	}
	
	private static void complementLangDataXML (Map<String,String> difference,String langDataXMLPath,String locale) 
		throws IOException {
		Document document = XMLHelper.loadXml(langDataXMLPath);
		if (document == null) {
			System.err.println("Could not load file: " + langDataXMLPath);
		}
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expres = xPath.compile("locales/locale[@id='" + locale + "']");
			Node localeNode = (Node)expres.evaluate(document,XPathConstants.NODE);
			for (Entry<String, String> entry:difference.entrySet()) {
				expres = xPath.compile(entry.getKey());
				Node node = (Node)expres.evaluate(localeNode,XPathConstants.NODE);
				if (node == null) {
					try {
						XMLHelper.addNode((Element)localeNode,entry.getKey(),entry.getValue());
					} catch (AddNodeException e) {
						System.err.println("Could not add node: " + e);
					}
				}
			}
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			//Magic chars
			os.write(0xEF);
			os.write(0xBB);
			os.write(0xBF);
			os.write(XMLHelper.retrieveNewFile(document).getBytes("UTF-8"));
			byte[] buffer = os.toByteArray();
			os.close();
			FSHelper.writeFile(new File(langDataXMLPath), new String(buffer,"UTF-8").toCharArray(),WriteMode.CHANGE,Charset.forName("UTF-8"));
		} catch (XPathExpressionException e) {
			System.err.println("Could not complete: " + e);
		}
	}
	
	private static Pair<String, String> convertUsageToRealLangDataPath (String langManagerUsage) 
		throws ConvertUsageToRealLangDataPathException {
		int index = langManagerUsage.lastIndexOf("/");
		String textToConversion = langManagerUsage.substring(index == -1?0:index+1,langManagerUsage.length());
		Matcher matcher = Pattern.compile("[a-zA-z0-9_]+").matcher(textToConversion);
		if (matcher.find()) {
			if (matcher.group().equals(textToConversion)) {
				String realText = textToConversion.replaceAll("_", " ");
				return new Pair<String, String>(langManagerUsage, realText);
			} else {
				throw new ConvertUsageToRealLangDataPathException("Unsupported path: " + textToConversion);
			}
		} else {
			throw new ConvertUsageToRealLangDataPathException("Unsupported path: " + textToConversion);
		}
	}
	
	private static Difference<String> findDifference(String projectPath,
			String resFilePath,String locale,
			String[] lzxHomePaths,
			String lzxComponentsPath,
			String[] fileExtensions)
		throws IOException {
		try {
			Map<String, String> mapA = getLangDataMap(projectPath+F_S+resFilePath);//,locale);
			Collection<String> collB = new LinkedHashSet<String>();
			for(String lzxHPath:lzxHomePaths) {
				collB.addAll(convertToPathUsage(getLangManagerUsage(projectPath+F_S+lzxHPath,fileExtensions)));
			}
			collB.addAll(convertToPathUsage(getLangManagerUsage(projectPath+F_S+lzxComponentsPath,fileExtensions)));
			SetsIntersector<String> setsIntersector = new SetsIntersector<String>();
			Difference<String> diff = setsIntersector.computeDifference(mapA.keySet(), collB, new StringComparator());
			return diff;
		} catch (GetLangDataMapException e) {
			System.err.println("Could not complete." + e);
		}
		return null;
	}
	
	private static Map<String,Set<String>> getLangManagerUsage (String lzxHomePath,String[] fileExtensionsToSearch) 
		throws IOException {
		File lzxHomeDir = new File(lzxHomePath);
		Set<File> files = new LinkedHashSet<File>();
		FSHelper.searchFiles(lzxHomeDir,fileExtensionsToSearch,files);
		Map<String,Set<String>> resultMap = calculateLangManagerUsage(files);
		return resultMap;
	}
	
	private static Collection<String> convertToPathUsage (Map<String,Set<String>> langManagerUsage) {
		Set<String> result = new LinkedHashSet<String>();
		for (Set<String> set:langManagerUsage.values()) {
			result.addAll(set);
		}
		return result;
	}
	
	private static Map<String,Set<String>> calculateLangManagerUsage (Collection<File> lzxFiles) 
		throws IOException {

		Pattern patterns[] = patternsToSearch;
		Map<String,Set<String>> resultMap = new LinkedHashMap<String, Set<String>>();
		
		for (File lzxFile:lzxFiles) {
			Set<String> currentSet = new LinkedHashSet<String>();
			resultMap.put(lzxFile.getCanonicalPath(), currentSet);
			String data = FSHelper.asString(lzxFile);
			for (Pattern pattern : patterns) {
				Matcher matcher1 = pattern.matcher(data);
				while(matcher1.find()) {
					String value = preprocessPromptId(matcher1.group(1));
					if (!value.trim().isEmpty()) {
						currentSet.add(value);
					}
				}
			}
		}
		return resultMap;
	}

	//from strings_en_US.properties
	private static Map<String,String> getLangDataMap (String resFilePath) 
		throws GetLangDataMapException,IOException {
		Properties props = new Properties();
		props.load(new InputStreamReader(new FileInputStream(resFilePath), "utf-8"));
		Map<String,String> langDataMap = new LinkedHashMap<String, String>();
		
		for (Object propName :  props.keySet()) {
			langDataMap.put((String) propName, (String) props.get(propName));
		}
		return langDataMap;
	}

	//from LangData.xml
	private static Map<String,String> getLangDataMap (String filePath,String langId) 
		throws GetLangDataMapException,IOException {
		Document document = XMLHelper.loadXml(filePath);
		if (document == null) {
			throw new GetLangDataMapException("Could not load LangData.xml file");
		}
		XPath xPath = XPathFactory.newInstance().newXPath();
		try {
			XPathExpression expres = xPath.compile("locales/locale[@id='" + langId + "']");
			Node localeNode = (Node)expres.evaluate(document,XPathConstants.NODE);
			Map<String,String> langDataMap = new LinkedHashMap<String, String>(); 
			pullMapFromNode(localeNode,langDataMap,"");
			return langDataMap;
		} catch (XPathExpressionException e) {
			throw new GetLangDataMapException("Could not create map for " + filePath + " file:",e);
		}
	}
	
	private static void pullMapFromNode (Node node,Map<String,String> buffer,String currentpath) {
		if (node.getNodeType() == Node.TEXT_NODE) {
			String value = node.getNodeValue();
			//Should we add strings are consisted of whitespace, new line symbols and etc
			//or not
			//For example we want the same offset for something and use for it node:
			//<whitespace>    </whitespace>
			//Laszlo showing this nodes.
			//if (!value.trim().isEmpty()) {
			//Unsure the parent of this node contain only one child type of TEXT_NODE
			//Avoid adding of ("/n   "). For example:
			//<somenode>
			//<text>text</text>
			//</somenode>
				if (node.getParentNode().getChildNodes().getLength() == 1) {
					buffer.put(currentpath, value);
				}
			//}
			return;
		}
		NodeList children = node.getChildNodes();
		int childsCount = children.getLength();
		if (childsCount == 0) {
			//empty node(<somenode></somenode>)
			if (!currentpath.isEmpty()) {
				buffer.put(currentpath, "");
			}
			return;
		}
		for(int i=0;i<childsCount;i++) {
			Node child = children.item(i);
			if (child.getNodeType() == Node.ELEMENT_NODE) {
				pullMapFromNode(child,buffer,currentpath.isEmpty()?child.getNodeName():currentpath + "/" + child.getNodeName());
			} else if (child.getNodeType() == Node.TEXT_NODE && childsCount == 1) {
				pullMapFromNode(child,buffer,currentpath.isEmpty()?node.getNodeName():currentpath);
			}
		}
		return;
	}

	private static boolean checkProjectHome(String projectPath,String resFilePath,String[] lzxHomePaths) {
		if (projectPath == null || projectPath.isEmpty()) {
			return false;
		}
		File projectDir = new File(projectPath);
		if (!projectDir.isDirectory()) {
			return false;
		}
		File resFile = new File(projectDir.getAbsolutePath()
				+ F_S + resFilePath);
		if (!resFile.canRead() || !resFile.isFile()) {
			return false;
		}
		for (String lzxHPath:lzxHomePaths) {
			File lzxHomeDir = new File(projectDir.getAbsolutePath()
					+ F_S + lzxHPath);
			if (!lzxHomeDir.canRead() || !lzxHomeDir.isDirectory()) {
				return false;
			}
		}
		return true;
	}

	public static class GetLangDataMapException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -1632495540658390214L;

		public GetLangDataMapException(String message, Throwable cause) {
			super(message, cause);
		}
		public GetLangDataMapException(String message) {
			super(message);
		}
	}
	
	public static class ConvertUsageToRealLangDataPathException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = -4595061250752518545L;
		public ConvertUsageToRealLangDataPathException(String message, Throwable cause) {
			super(message, cause);
		}
		public ConvertUsageToRealLangDataPathException(String message) {
			super(message);
		}
	}
	
	public static class CompareException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 6744062342939710593L;
		public CompareException(String message, Throwable cause) {
			super(message, cause);
		}
		public CompareException(String message) {
			super(message);
		}
	}
	
	public static class StringComparator implements Comparator<String> {
		@Override
		public int compare(String o1, String o2) {
			return o1.compareTo(o2);
		}
	}
	
	private static String preprocessPromptId(String promptId) {
		
		if (promptId.equals("text/crah/auto_mode_params/max_uncontrollable")) {
			System.out.println("text/crah/auto_mode_params/max_uncontrollable");		}
		
		return promptId.replaceAll("/", "_");
	}
}