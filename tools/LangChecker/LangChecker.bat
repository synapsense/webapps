@echo OFF
echo Start
if not exist %~dp0bin (mkdir %~dp0bin)
javac -sourcepath %~dp0/src -d %~dp0/bin %~dp0/src/com/synapsense/LangChecker.java
java -classpath %~dp0/bin com/synapsense/LangChecker %*
echo Finish