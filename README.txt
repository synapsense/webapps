A. openlaszlo folder contains unpacked openlaszlo-4.7.0-servlet.war.
  
  List of changes:
  1. lps/components/lz/textlistitem.lzx line 42: "if (this._title)" was added to avoid sfw10 runtime errors
  2. lps/components/base/basetree.lzx line 139: "sv.length > 0" condition was added to "if" to avoid sfw10 runtime errors
  3. webdl/webdl.lzx file was added to allow compilation of webdl in openlaszlo web context.
  4. webconsole/synapsoft.lzx file was added to allow compilation of synapsoft app in openlaszlo web context.
  5. lps/components/base/basewindow.lzx line 349: Temporary fix for http://jira.openlaszlo.org/jira/browse/LPP-8951
  6. lps/components/base/basescrollbar.lzx line 590: Temporary fix for http://jira.openlaszlo.org/jira/browse/LPP-8963
  7. lps/components/base/baselist.lzx line 190: Temporary fix for http://jira.openlaszlo.org/jira/browse/LPP-9062 - NPE in baselist._dokeydown()
  8. lps/components/lz/edittext.lzx line 13: Add dummy-handler for 'onmousedown' event via tag (NOTE: when we do the same via LzDelegate that has no effect)
     lps/components/base/basecombobox.lzx line 199: Add dummy-handler for 'onmousedown' event via tag (NOTE: when we do the same via LzDelegate that has no effect)
     Filed bug on openlaszlo is http://jira.openlaszlo.org/jira/browse/LPP-8076
  9. lps/includes/lfc/LFCdhtml.js
     lps/includes/lfc/LFCdhtml-debug.js
     Fixes for http://jira.openlaszlo.org/jira/browse/LPP-9573 and http://jira.openlaszlo.org/jira/browse/LPP-9573, comment out some warnings
  10. lps/components/base/baseforitem.lzx line 133: Temporary fix for http://jira.openlaszlo.org/jira/browse/LPP-8826
  11. openlaszlo/lps/components/base/componentmanager.lzx line 77: Need to send onblur event to focus component because some components updated their value on this event
  12. In openlaszlo source code in lps-4.7.0-src/lps-4.7.0/WEB-INF/lps/lfc/kernel/swf9/LzTextSprite.as html property default value was set to false. LFC10.swc, LFC10-debug.swc and LFC10-backtrace.swc flash libraries were recompiled with this change and put into repository (Bug 5814)
  13. lps/components/base/basegrid.lzx line 141: "isinited" condition was changed to "inited" (_getReplicator() returned "undefined")
  14. lps/components/extensions/drawview.lzx line 1211: Add an additional check for limit width and height because in Flash Player 10, the maximum size for a BitmapData object is 8,191 pixels in width or height.

B. webconsole folder contains synapsoft application
