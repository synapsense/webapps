LPS 4.7.0 Core README

Here's what you'll find:

    LICENSE.txt            - The End User License Agreement for the LPS.
    README.txt             - This file.
    3rdPartyCredits        - Acknowledgements for 3rd party contributions 
                             to the LPS.
    lps-4.7.0        - Expanded directory containing the 
                             LPS 4.7.0 web app.
    release-notes.html     - Release notes.

Deploy the lps-4.7.0 web application and surf to 

       http://yourhost:yourport/lps-4.7.0/index.html

where yourhost:yourport is the host and port where your servlet
container is running.  This should bring up Laszlo Quick Index page
with pointers to tutorials, examples, sample applications and more.

For detailed installation instructions, please see 
http://www.laszlosystems.com/developers/learn/documentation/ 

--

OpenLaszlo 4.7.0 Production branches/4.7@15482 (15483) 2010-01-16T11:49:52Z

* T_LZ_COPYRIGHT_BEGIN ******************************************************
* Copyright 2001-2006, 2009 Laszlo Systems, Inc.  All Rights Reserved.            *
* Use is subject to license terms.                                          *
* T_LZ_COPYRIGHT_END ********************************************************
