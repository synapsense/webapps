package com.synapsense.auditor;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.Properties;

import javax.management.JMX;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRulesEngineManagement;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngineManagement;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.service.nsvc.NotificationService;

import com.synapsense.util.JNDINames;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

public class AuditorXMLViewer extends BaseHttpServlet {
    private static final long serialVersionUID = 1L;
    private static final Log LOG = LogFactory.getLog(AuditorXMLViewer.class);
    private Map<String, String> dispCompNames = new HashMap<String, String>();
    private Properties environment = new Properties();

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        dispCompNames.put("RmiServiceAuditor", "Device Manager");
        dispCompNames.put("LiveImagingAuditor", "LiveImaganing Server");
        dispCompNames.put("MySQLAuditor", "Database");
        
        final ServletContext cont = config.getServletContext();
        final String INITIAL_CONTEXT_FACTORY = cont.getInitParameter("INITIAL_CONTEXT_FACTORY");
        final String URL_PKG_PREFIXES = cont.getInitParameter("URL_PKG_PREFIXES");
        final String PROVIDER_URL = cont.getInitParameter("PROVIDER_URL");
        environment.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
        environment.put(Context.URL_PKG_PREFIXES, URL_PKG_PREFIXES);
        environment.put(Context.PROVIDER_URL, PROVIDER_URL);
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        sendResponse(response, XMLHelper.getDocumentXml(getAuditorXML(request)));
    }

    private Document getAuditorXML(HttpServletRequest request) {
        AuditorSvc auditorSvc = null;
        MBeanServerConnection mbeanSrv = null;
        AlertService ALERTING_SERVICE = null;
        Environment ENV_SERVICE = null;
        try {
            InitialContext ctx = new InitialContext(environment);
            auditorSvc = ServerLoginModule.getProxy(ctx,
                    "SynapServer/AuditorSvcImpl/remote", AuditorSvc.class);
            mbeanSrv = (MBeanServerConnection) ctx.lookup("jmx/invoker/RMIAdaptor");
            ALERTING_SERVICE = ServerLoginModule.getProxy(ctx,
                    JNDINames.ALERTING_SERVICE_JNDI_NAME, AlertService.class);
            ENV_SERVICE = ServerLoginModule.getProxy(ctx,
                    JNDINames.ENVIRONMENT, Environment.class);
        } catch (NamingException e) {
            LOG.error(e.getLocalizedMessage(),e);
            return null;
        }
        Map<String, Auditor> auditors = auditorSvc.getAuditors();
        String drulesStatus = "N/A";
        String creStatus = "N/A";
        String alertingStatus = "N/A";
        String environmentStatus = "N/A";
        String notificationStatus = "N/A";

        Collection<Alert> alerts = ALERTING_SERVICE.getActiveAlerts(
                TOFactory.EMPTY_TO, true);
        String[] objTypes = null;
        objTypes = ENV_SERVICE.getObjectTypes();
        Map<String, Integer> objTypeCount = new HashMap<String, Integer>();
        if (objTypes != null && objTypes.length != 0) {
            for (String typeName : objTypes) {
                Collection<TO<?>> objIds = ENV_SERVICE
                        .getObjectsByType(typeName);
                objTypeCount.put(typeName, objIds.size());
            }

        }

        try {
            drulesStatus = (String) mbeanSrv
                    .getAttribute(
                            new ObjectName(
                                    "jboss.j2ee:ear=SynapServer.ear,jar=DRulesEngine.jar,name=DRulesEngineService,service=EJB3"),
                            "StateString");
            creStatus = (String) mbeanSrv
                    .getAttribute(
                            new ObjectName(
                                    "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=RulesEngineService,service=EJB3"),
                            "StateString");
            alertingStatus = (String) mbeanSrv
                    .getAttribute(
                            new ObjectName(
                                    "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=AlertService,service=EJB3"),
                            "StateString");
            environmentStatus = (String) mbeanSrv
                    .getAttribute(
                            new ObjectName(
                                    "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=Environment,service=EJB3"),
                            "StateString");
            notificationStatus = (String) mbeanSrv
                    .getAttribute(
                            new ObjectName(
                                    "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=NotificationService,service=EJB3"),
                            "StateString");

        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        String dreDetails = "";
        if (drulesStatus.equalsIgnoreCase("Started")) {
            try {
                DRulesEngineManagement druleEngManagment = JMX
                        .newMBeanProxy(
                                mbeanSrv,
                                new ObjectName(
                                        "jboss.j2ee:ear=SynapServer.ear,jar=DRulesEngine.jar,name=DRulesEngineService,service=EJB3,type=ManagementInterface"),
                                DRulesEngineManagement.class);
                String[][] ruleEnum = druleEngManagment.enumerateRules();
                dreDetails = ruleEnum.length + " rules loaded";

            } catch (Exception e) {
                drulesStatus = "Not started";
                dreDetails = e.toString();
                LOG.error(e.getLocalizedMessage(), e);
            }
        }

        String creDetails = "";
        if (creStatus.equalsIgnoreCase("Started")) {
            try {
                RulesEngineManagement rulesEngineManagement = JMX
                        .newMBeanProxy(
                                mbeanSrv,
                                new ObjectName(
                                        "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=RulesEngineService,service=EJB3,type=ManagementInterface"),
                                RulesEngineManagement.class);

                Long CRElastTime = rulesEngineManagement
                        .getLastPropertyCalculationTime();

                if (CRElastTime != null) {
                    Date CRElastDate = new Date(CRElastTime);
                    SimpleDateFormat ft = new SimpleDateFormat(
                            " dd.MM.yyyy 'at' HH:mm");
                    creDetails = "Last Task finished at "
                            + ft.format(CRElastDate);
                }

            } catch (Exception e) {
                creStatus = "Not started";
                creDetails = e.toString();
                LOG.error(e.getLocalizedMessage(), e);
            }
        }

        String ntfDetails = "";
        if (notificationStatus.equals("Started")) {
            try {
                NotificationService nsvcManagement = JMX
                        .newMBeanProxy(
                                mbeanSrv,
                                new ObjectName(
                                        "jboss.j2ee:ear=SynapServer.ear,jar=SynapEnvCore.jar,name=NotificationServiceImpl,service=EJB3,type=ManagementInterface"),
                                NotificationService.class);
                ntfDetails = nsvcManagement.toString();
            } catch (Exception e) {
                notificationStatus = "Not started";
                ntfDetails = e.toString();
                LOG.error(e.getLocalizedMessage(), e);
            }
        }

        Document xmlDoc = null;
        try {
            xmlDoc = XMLHelper.createNewXmlDocument("xml");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        Element root = xmlDoc.getDocumentElement();
        Element componentList = newElement(root, "componentList", null);

        for (Auditor auditor : auditors.values()) {
            String fullName = auditor.getName();
            String shortName = fullName
                    .substring(fullName.lastIndexOf(".") + 1);
            Element component = newElement(componentList, "component", null);
            if (dispCompNames.containsKey(shortName)) {
                newElement(component, "name", dispCompNames.get(shortName));
            } else {
                newElement(component, "name", shortName);
            }

            if (auditor.getLastStatus().equals("OK")) {
                newElement(component, "status", "Operational");
                newElement(component, "details", "");
            } else {
                newElement(component, "status", "Not operational");
                newElement(component, "details", auditor.getLastStatus());
            }
        }

        // CRE
        Element componentCRE = newElement(componentList, "component", null);
        newElement(componentCRE, "name", "CRE");
        newElement(componentCRE, "status", creStatus);
        newElement(componentCRE, "details", creDetails);

        // DRE
        Element componentDRE = newElement(componentList, "component", null);
        newElement(componentDRE, "name", "DRE");
        newElement(componentDRE, "status", drulesStatus);
        newElement(componentDRE, "details", dreDetails);

        // Alerting
        Element componentAlert = newElement(componentList, "component", null);
        newElement(componentAlert, "name", "Alerting");
        newElement(componentAlert, "status", alertingStatus);
        newElement(componentAlert, "details", alerts.size() + " active alerts");

        // Environment
        Element componentEnv = newElement(componentList, "component", null);
        newElement(componentEnv, "name", "Environment");
        newElement(componentEnv, "status", environmentStatus);
        StringBuffer sb = new StringBuffer();
        for (String typeName : objTypes) {
            sb.append("\t\t\t\t[" + typeName + "]: "
                    + objTypeCount.get(typeName) + "\n");
        }
        newElement(componentEnv, "details", sb.toString());

        // NotificationSVC
        Element componentNotif = newElement(componentList, "component", null);
        newElement(componentNotif, "name", "NotificationSVC");
        newElement(componentNotif, "status", notificationStatus);
        newElement(componentNotif, "details", ntfDetails);
        return xmlDoc;
    }

    private Element newElement(Node parent, String name, String value) {
        Document document = parent.getOwnerDocument();
        Element newElement = document.createElement(name);

        if (parent != null) {
            parent.appendChild(newElement);
        }

        if (value != null) {
            newElement.appendChild(document.createTextNode(value));
        }

        return newElement;
    }

}
