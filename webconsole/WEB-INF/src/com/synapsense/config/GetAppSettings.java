package com.synapsense.config;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

public class GetAppSettings extends BaseHttpServlet {	
    private static final long serialVersionUID = 1L;
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
        try {
            String settingsStr = request.getParameter("settings");
            
            String[] settings = settingsStr.split(",");
            
            Document doc = XMLHelper.createNewXmlDocument("settings");
            Node root = doc.getDocumentElement();
            
            for (String setting : settings) {
                String value = System.getProperty("com.synapsense." + setting);
                
                if (value != null && !value.isEmpty()) {
                    Element e = doc.createElement("setting");
                    root.appendChild(e);
                    e.setAttribute("name", setting);
                    e.setAttribute("value", value);
                }
            }
            sendResponse(response, doc);
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }

     }
 
}
