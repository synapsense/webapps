package com.synapsense.config;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.config.metrics.MetricsConfig;
import com.synapsense.config.objectdetails.Block;
import com.synapsense.config.objectdetails.ObjectDetailsConfig;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.symbolvalues.SymbolValue;
import com.synapsense.utilities.symbolvalues.SymbolValues;

/**
 * @author pahunov
 *
 * Utility class to obtain settings from the uiconfig.xml
 */
public class UIConfig {
    static Log log = LogFactory.getLog(UIConfig.class);
    private static HashMap <String,String> userRoleMapping = new HashMap<String, String>();
    private static MetricsConfig metricsConfig = new MetricsConfig();
    private static HashMap <Integer, SymbolValues> classId_symbolValues = new HashMap<Integer, SymbolValues>();
    private static HashMap <Integer, ArrayList<Integer>> dataClassId_layerValues = new HashMap<Integer, ArrayList<Integer>>();
    private static ObjectDetailsConfig objDetailsConf = new ObjectDetailsConfig();
    private static List<String> alertableObjectTypes = new ArrayList<String>();
    private static List<String> dataAnalysisExcludedTypes = new ArrayList<String>();
    private static List<String> dataViewTypes = new ArrayList<String>();
    
    private UIConfig() {}
    
    public static void init() {
        //Get XML-configuration document
        File confFile = new File(CommonFunctions.getStandardConfigFileName());      
        Document confDoc = null;
        
        try{
            confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
            XPathFactory  factoryX = XPathFactory.newInstance();
            XPath xPath = factoryX.newXPath();
            
            //Set userRoleMapping
            //Compile xPath expressions
            XPathExpression xPathRole = xPath.compile("config/userrolesmapping/*");
            XPathExpression xPathName = xPath.compile("@name");
            XPathExpression xPathMappedName = xPath.compile("@prompt");
            
            NodeList role = (NodeList) xPathRole.evaluate(confDoc, XPathConstants.NODESET);
            
            for (int i=0;i<role.getLength();i++) {
                String name = xPathName.evaluate(role.item(i));
                String mappedName =  xPathMappedName.evaluate(role.item(i));
                if(mappedName != null){
                    userRoleMapping.put(name, mappedName);
                }
            }
            
            //Set classId to symbolValues mapping
            XPathExpression xPathDataClasses = xPath.compile("config/sensorValueMapping/dataClass");
            XPathExpression xPathClassId = xPath.compile("@id");
            XPathExpression xPathValues = xPath.compile("value");
            XPathExpression xPathVal = xPath.compile("@val");
            XPathExpression xPathMapVal = xPath.compile("@prompt");
            
            NodeList classIds = (NodeList) xPathDataClasses.evaluate(confDoc, XPathConstants.NODESET);
            if(classIds != null){
                for(int i = 0; i < classIds.getLength(); i++) {
                    SymbolValues vals = new SymbolValues();
                    Integer classId = Integer.parseInt(xPathClassId.evaluate(classIds.item(i)));
                    
                    NodeList symbolVals = (NodeList) xPathValues.evaluate(classIds.item(i), XPathConstants.NODESET);
                    if(symbolVals != null){
                        for(int j = 0; j < symbolVals.getLength(); j++) {
                            vals.addSymbolValue(new SymbolValue(xPathMapVal.evaluate(symbolVals.item(j)), Double.parseDouble(xPathVal.evaluate(symbolVals.item(j)))));
                        }                
                        classId_symbolValues.put(classId, vals); 
                    }
                } 
            }
            
            //Set dataclass to layerValues mapping
            XPathExpression xpDataClasses = xPath.compile("config/feature[@name='LiveImaging']/dataclass");
            XPathExpression xpClassId = xPath.compile("@id");
            XPathExpression xpTapes = xPath.compile("type");
            XPathExpression xpMapLayer = xPath.compile("@layer");
            
            NodeList dataClassIds = (NodeList) xpDataClasses.evaluate(confDoc, XPathConstants.NODESET);
            if(dataClassIds != null){
                for(int i = 0; i < dataClassIds.getLength(); i++) {
                    Integer classId = Integer.parseInt(xpClassId.evaluate(dataClassIds.item(i)));
                    
                    NodeList symbolVals = (NodeList) xpTapes.evaluate(dataClassIds.item(i), XPathConstants.NODESET);
                    ArrayList<Integer> arrLayers = new ArrayList<Integer>();
                    if(symbolVals != null){
                        for(int j = 0; j < symbolVals.getLength(); j++) {
                            arrLayers.add(Integer.parseInt(xpMapLayer.evaluate(symbolVals.item(j))));
                        }                
                        dataClassId_layerValues.put(classId, arrLayers); 
                    }
                } 
            }
            
            //Set allowed alert types
            XPathExpression xPathAlertTypes = xPath.compile("config/feature[@name='alerts']/type/@name");
            NodeList alertTypeNodeList = (NodeList) xPathAlertTypes.evaluate(confDoc, XPathConstants.NODESET);
            if(alertTypeNodeList != null)
            {
                for(int i = 0; i < alertTypeNodeList.getLength(); i++)
                {
                    alertableObjectTypes.add(alertTypeNodeList.item(i).getNodeValue());
                }
            }
            
            //Set data analysis excluded types
            XPathExpression xPathExcludedTypes = xPath.compile("config/feature[@name='DataAnalysis']/setting[@name='excludedObjects']/@value");
            String excludedTypes = xPathExcludedTypes.evaluate(confDoc);
            if(excludedTypes != null){
                String[] excludedTypesArray = excludedTypes.split(",");
                for(int i = 0; i < excludedTypesArray.length; i++){
                    dataAnalysisExcludedTypes.add(excludedTypesArray[i]);
                }
            }
            
            //Set data analysis excluded types
            xPathExcludedTypes = xPath.compile("config/feature[@name='DataView']/setting[@name='allowedObjects']/@value");
            excludedTypes = xPathExcludedTypes.evaluate(confDoc);
            if(excludedTypes != null){
                String[] excludedTypesArray = excludedTypes.split(",");
                for(int i = 0; i < excludedTypesArray.length; i++){
                    dataViewTypes.add(excludedTypesArray[i]);
                }
            }
            
        }catch (SAXException e) {
            log.error(e.getLocalizedMessage(), e);              
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            log.error(e.getLocalizedMessage(), e);
        }catch (XPathExpressionException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        
        try{
            //init object details config
            objDetailsConf.init(CommonFunctions.getStandardConfigFileName());
            //init metrics config
            metricsConfig.init(CommonFunctions.getStandardConfigFileName());
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }
    }
    
    public static ArrayList<Integer> getLayersValueByClassId(Integer classId) {
        return dataClassId_layerValues.get(classId);
    }
    
    public static SymbolValues getSymbolValuesByClassId(Integer classId){
        return classId_symbolValues.get(classId);
    }
    
    public static String getDisplayableUserRoleName(String user_role){
        String result = userRoleMapping.get(user_role);
        return  result != null? result: user_role;
    }
    
    public static Map<String, List<Block>> getObjDetailsConfigMap(){
        return objDetailsConf.getBlockMap();
    }
    
    public static MetricsConfig getMetricsConfig(){
        return metricsConfig;
    }
    
    /**
     * Gets the list of allowed object types which should be shown in New Alert TypeCombo combobox
     * @return
     */
    public static List<String> getAlertableObjectTypes()
    {
        return alertableObjectTypes;
    }
    
    public static List<String> getDataAnalysisExcludedTypes(){
        return dataAnalysisExcludedTypes;
    }
    
    public static List<String> getDataViewTypes(){
        return dataViewTypes;
    }
}