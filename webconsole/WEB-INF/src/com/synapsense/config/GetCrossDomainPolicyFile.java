package com.synapsense.config;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.BaseHttpServlet;

public class GetCrossDomainPolicyFile extends BaseHttpServlet {

    private static final long serialVersionUID = -3829973766082976452L;
    private static final Log LOG = LogFactory.getLog(GetCrossDomainPolicyFile.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String path = "crossdomain.xml";
        LOG.info("GetCrossDomainPolicyFile: " + path);
        
        File f = new File(getServletContext().getRealPath("/") + path);
        
        
        if (path == null || !f.exists()) {
            String requestUri = request.getRequestURI();
            
            response.sendError(HttpServletResponse.SC_NOT_FOUND,
                       requestUri);
            return;
        }
        
        try (InputStream is = new BufferedInputStream(new FileInputStream(f))) {
            int size = is.available();
            byte[] b = new byte[size];
            is.read(b);
            
            sendResponse(response, b);

            //Set headers after sendResponse
            response.setContentType("text/x-cross-domain-policy");
            response.setHeader("X-Permitted-Cross-Domain-Policies", "by-content-type");
            response.setContentLength(size);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    

}
