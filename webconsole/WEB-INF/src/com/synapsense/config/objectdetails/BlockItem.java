package com.synapsense.config.objectdetails;


public class BlockItem {
   protected String propertyName = "";
   protected String propertyLabel = "";
   protected String propertyTooltip = "";
   
   public BlockItem(){       
   }
   
   public BlockItem(String propertyName, String propertyLabel, String propertyTooltip){
       this.propertyName = propertyName;
       this.propertyLabel = propertyLabel;
       this.propertyTooltip = propertyTooltip;
   }
   
   public String getPropertyName() {
       return propertyName;
   }
   public void setPropertyName(String propertyName) {
       this.propertyName = propertyName;
   }
   public String getPropertyLabel() {
       return propertyLabel;
   }
   public void setPropertyLabel(String propertyLabel) {
       this.propertyLabel = propertyLabel;
   }

   public String getPropertyTooltip() {
       return propertyTooltip;
   }

   public void setPropertyTooltip(String propertyTooltip) {
       this.propertyTooltip = propertyTooltip;
   }
   
}
