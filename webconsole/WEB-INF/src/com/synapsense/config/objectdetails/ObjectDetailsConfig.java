package com.synapsense.config.objectdetails;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.utilities.XMLHelper;


public class ObjectDetailsConfig {
    private static Log LOG = LogFactory.getLog(ObjectDetailsConfig.class);
    protected Map<String, List<Block>> blockMap = new HashMap<String, List<Block>>();
    
    public Map<String, List<Block>> getBlockMap(){
        return blockMap;
    }
    
    public void init(String configFileName){
        File confFile = new File(configFileName);      
        Document confDoc = null;
        
        try{
            
            confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
            XPathFactory  factoryX = XPathFactory.newInstance();
            XPath xPath = factoryX.newXPath();
            
            XPathExpression xPathTypes = xPath.compile("config/object_details/*");
            XPathExpression xPathLabel = xPath.compile("@labelprompt");
            XPathExpression xPathTooltip = xPath.compile("@tooltipprompt");
            XPathExpression xPathBlocks = xPath.compile("block");
            XPathExpression xPathBlockProps = xPath.compile("*");
            
            NodeList types = (NodeList) xPathTypes.evaluate(confDoc, XPathConstants.NODESET);
            
            for(int i = 0; i < types.getLength(); i++){
                String typeName = types.item(i).getNodeName();
                NodeList blocks = (NodeList)xPathBlocks.evaluate(types.item(i), XPathConstants.NODESET);
                
                List<Block> blockList = new ArrayList<Block>();
                
                for(int bNum = 0; bNum < blocks.getLength(); bNum++){
                    Block block = new Block();
                    NodeList props = (NodeList)xPathBlockProps.evaluate(blocks.item(bNum), XPathConstants.NODESET);                    
                    for(int pNum = 0; pNum < props.getLength(); pNum++){
                        String propertyName = props.item(pNum).getNodeName();
                        String propLabel = xPathLabel.evaluate(props.item(pNum));
                        String propTooltip = xPathTooltip.evaluate(props.item(pNum));
                        if(propLabel == null){
                            propLabel = "";
                        }
                        if(propTooltip == null){
                            propTooltip = "";
                        }
                        block.addItem(new BlockItem(propertyName, propLabel, propTooltip));
                    }
                    blockList.add(block);
                }
                
                blockMap.put(typeName, blockList);
            }            
            
        }catch (SAXException e) {
            LOG.error(e.getLocalizedMessage(), e);              
        } catch (IOException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }catch (XPathExpressionException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
}
