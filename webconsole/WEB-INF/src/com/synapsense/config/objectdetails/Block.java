package com.synapsense.config.objectdetails;

import java.util.ArrayList;
import java.util.List;

public class Block {
    protected List<BlockItem> items = new ArrayList<BlockItem>();
    
    public Block(){        
    }
    public void addItem(BlockItem item){
        items.add(item);
    }
    
    public void removeItem(BlockItem item){
        items.remove(item);
    }
    
    public List<BlockItem> getItems(){
        return items;
    }
}
