package com.synapsense.config.types;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.session.ApplicationWatch;
import com.synapsense.util.unitconverter.ConverterException;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;
import com.synapsense.util.unitconverter.UnitSystems;
import com.synapsense.utilities.es.EnvironmentTypes;


/**
 * @author pahunov
 *
 * Utility class to obtain description of types and properties from the
 * types.xml
 */
public class TypeUtil {
    
    public static String CONFIG_FILE_NAME = null;
    
    private static final Log LOG = LogFactory.getLog(TypeUtil.class);

    private static TypeUtil instance ;
    
    private Map<String, Map<String, Property>> propMap;
    
    private Map<String, Type> typeMap;

    private TypeUtil() throws JAXBException {
        JAXBContext jc = JAXBContext.newInstance( Types.class, Type.class, Property.class, Formula.class, VarBinding.class, Expression.class );
        Unmarshaller um = jc.createUnmarshaller();

        File configFile;
        if (TypeUtil.CONFIG_FILE_NAME != null) {
            configFile = new File(TypeUtil.CONFIG_FILE_NAME);
        } else {
            configFile = new File(ApplicationWatch.getPath() + "conf/types.xml");
        }
        Types types = (Types) um.unmarshal(configFile);
        
        propMap = new HashMap<String, Map<String, Property>> ();
        typeMap = new HashMap<String, Type> ();
        for (Type type : types.getType()) {
            String typeName = type.getName();
            //We need to save order of the properties, so use LinkedHashMap here
            Map<String, Property> map = new LinkedHashMap<String, Property>();
            
            for (Property property : type.getProperty()) {
                String propName = property.getName();
                map.put(propName, property);
            }
            propMap.put(typeName, map);
            typeMap.put(typeName, type);
        }
            
    }
    
    public static TypeUtil getInstance(){
        if (instance == null) {
            try {
                instance = new TypeUtil();
            } catch (JAXBException e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
        }
        return instance;
    }
    
    public Property getProperty(String typeName, String propName) {
        Property pd = null;
        Map<String, Property> map =propMap.get(typeName);
        if (map != null) {
            pd = map.get(propName);
        }
        
        return pd;
    }
    
    public Type getType(String typeName) {
        return typeMap.get(typeName);
    }

    public Set<String> listTypeNames() {
        return typeMap.keySet();
    }

    public List<String> listPropertyNames(String typeName) {
        Type type = typeMap.get(typeName);
        List<String> names = new LinkedList<String>();
        if (type != null) {
            for (Property property : type.getProperty()) {
                names.add(property.getName());
            }
        }
        return names;
    }

    public List<String> listVisiblePropertyNames(String typeName) {
        Type type = typeMap.get(typeName);
        List<String> names = new LinkedList<String>();
        if (type != null) {
            for (Property property : type.getProperty()) {
                if(property.isVisible()){
                    names.add(property.getName());
                }
            }
        }
        return names;
    }
    
    /**
     * Gets displayable type name from config file by environment type name
     * @param typeName - environment object name
     * @return
     */
    public String getDisplayableTypeName(String typeName){
        return "object_type_" + typeName.toLowerCase();
    }

    /**
     * Gets displayable property name from config file by object name and property name
     * @param typeName - environment type name
     * @param propertyName - property name
     */
    public String getDisplayablePropertyName(String typeName, String propertyName) {
        return "object_property_" + typeName.toLowerCase() + "_" + propertyName.toLowerCase();
    }

    public String getDisplayableDataclassName(String dataclass){

        UnitSystems uss = Env.getUnitSystemsService().getUnitSystems();
        Dimension d = uss.getSimpleDimensions().get(dataclass);

        if (d == null) {
            try {
                d = uss.getUnitSystem(Env.DEFAULT_UNIT_SYSTEM).getDimensionsMap().get(dataclass);
            } catch (ConverterException e) {
                LOG.error(Env.DEFAULT_UNIT_SYSTEM + " units system is not defined", e);
            }
        }
        
        if (d == null) {
            return dataclass;
        } else {
            return d.getLongUnits();
        }
    }

    /**
     * Gets  property description from config file by object name and property name
     * @param typeName - environment type name
     * @param propertyName - property name
     */
    public String getPropertyDescr(String typeName, String propertyName) {
        return "typename/" + typeName.toLowerCase() + "/propdescr/" + propertyName.toLowerCase();
    }
    
    public String getPropertyDataclass(String typeName, String propertyName) {
        String result = null;
        if (EnvironmentTypes.TYPE_NODE.equals(typeName) ||
            EnvironmentTypes.TYPE_SENSOR.equals(typeName)) {
            result = propertyName;
        } else {
            Property property = getProperty(typeName, propertyName);
            if (property != null) {
                result = property.getDataclass();
                if (result == null) {
                    //try another config
                    PropertyUnits pu = Env.getUnitSystemsService().getUnitReslovers().getUnits(typeName, propertyName);
                    if (pu != null) {
                        result = pu.getDimensionReference().getDimension();
                    }
                }
            }
        }

        return result;
    }
    
}
