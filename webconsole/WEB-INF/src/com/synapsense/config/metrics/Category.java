package com.synapsense.config.metrics;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private String categoryName = "";
    private String categoryTitle = "";
    private List<String> propertyNames = new ArrayList<String>();
    
    public Category(){		
    }
    public Category(String categoryName, String categoryTitle, List<String> propertyNames) {
        this.categoryName = categoryName;
        this.categoryTitle = categoryTitle;
        this.propertyNames = propertyNames;
    }
    
    public String getCategoryName() {
        return categoryName;
    }
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
    public String getCategoryTitle() {
        return categoryTitle;
    }
    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }
    public List<String> getPropertyNames() {
        return propertyNames;
    }
    public void setPropertyNames(List<String> propertyNames) {
        this.propertyNames = propertyNames;
    }
}
