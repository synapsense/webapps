package com.synapsense.config.metrics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.utilities.XMLHelper;

public class MetricsConfig {
    private static Log LOG = LogFactory.getLog(MetricsConfig.class);	
    private Map<String, List<Category>> categoriesMap = new HashMap<String, List<Category>>();
    
    public List<Category> getCategories(String objectTypeName){
        return categoriesMap.get(objectTypeName);
    }
    
    public List<String> getMetricsPropNames(String objectTypeName){
        List<String> result = new ArrayList<String>();		
        if(categoriesMap.containsKey(objectTypeName)){
            List<Category> categoryList = categoriesMap.get(objectTypeName);
            for (int i = 0; i < categoryList.size(); i++){
                result.addAll(categoryList.get(i).getPropertyNames());
            }
        }		
        return result;
    }
    
    public void init(String configFileName){
        File confFile = new File(configFileName);      
        Document confDoc = null;
        
        try{
            
            confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
            XPathFactory  factoryX = XPathFactory.newInstance();
            XPath xPath = factoryX.newXPath();
                        
            XPathExpression xPathMetricsViewColumn = xPath.compile("config/metricsview/*");
            XPathExpression xPathCategory = xPath.compile("category");
            XPathExpression xPathProperty = xPath.compile("*");
            NodeList typesList = (NodeList) xPathMetricsViewColumn.evaluate(confDoc, XPathConstants.NODESET);
            
            if (typesList != null) {
                for (int i = 0; i < typesList.getLength(); i++) {
                    String typeName = typesList.item(i).getNodeName();
                    List<Category> categories = new ArrayList<Category>();
                    
                    NodeList categoryList = (NodeList) xPathCategory.evaluate(typesList.item(i), XPathConstants.NODESET);//typesList.item(i).getChildNodes();
                                        
                    if(categoryList != null){            			
                        for(int j = 0; j < categoryList.getLength(); j++){
                            List<String> properties = new ArrayList<String>();            				

                            String categoryName = categoryList.item(j).getAttributes().getNamedItem("name").getNodeValue();
                            String categoryTitle = categoryList.item(j).getAttributes().getNamedItem("title").getNodeValue();;
                            NodeList propsList = (NodeList) xPathProperty.evaluate(categoryList.item(j), XPathConstants.NODESET); //categoryList.item(j).getChildNodes();
                            
                            if(propsList != null){
                                for(int k = 0; k < propsList.getLength(); k++){
                                    properties.add(propsList.item(k).getNodeName());
                                }
                            }
                            categories.add(new Category(categoryName, categoryTitle, properties));
                        }
                    }
                    
                    this.categoriesMap.put(typeName, categories);
                }
            }
                                  
        }catch (SAXException e) {
            LOG.error(e.getLocalizedMessage(), e);              
        } catch (IOException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }catch (XPathExpressionException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
}
