package com.synapsense.activecontrol;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.synapsense.dataAccess.UnitConverter;
import com.synapsense.dto.TO;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.UserPreference;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.synapsense.dataAccess.Env;
import com.synapsense.exception.AcApiException;
import com.synapsense.util.ConvertingEnvironment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.lang.LangManager;

public class ActiveControlService extends BaseHttpServlet {
    public static final String AC_TEMPERATURE = "temperature";
    public static final String AC_PRESSURE = "pressure";
    public static final String AC_RESOURCE = "resource";
    public static final String AC_METHOD = "method";
    public static final String AC_PARAMS= "params";
    public static final String AC_TARGET= "target";
    public static final String AC_BATCH= "batch";
    public static final String AC_DATE_FIELD = "date";
    public static final String AC_SET_INPUT_CONFIG_COMMAND = "set_input_config";
    public static final String AC_SET_MANUAL_OUTPUT_COMMAND = "set_manual_output";
    public static final String AC_SET_INPUT_TARGET_COMMAND = "set_input_target";
    public static final String UNIT_SYSTEM_US = "Server";
    public static final String UNIT_SYSTEM_SI = "SI";
    public static final String UNIT_SYSTEM_TEMPERATURE_DIM = "200";
    public static final String UNIT_SYSTEM_PRESSURE_DIM = "202";
    public static final String UNITS_SYSTEM = "UnitsSystem";
    public static final String DATE_FORMAT = "DateFormat";
    public static final String TEMPERATURE_STRATEGY_STATUSLAST_ADJUSTMENT = "temperature_strategy_statuslast_adjustment";
    public static final String PRESSURE_STRATEGY_STATUSLAST_ADJUSTMENT = "pressure_strategy_statuslast_adjustment";
    public static final String LAST_ADJUSTMENT_SEPARATOR = ",";

    private static Long id = 1L;
    private static final long serialVersionUID = 4049611810300952518L;
    private static final Log LOG = LogFactory.getLog(ActiveControlService.class);
    private static final ThreadLocal<ConvertingEnvironment> conenv = new ThreadLocal<>();
    private static final ThreadLocal<HttpServletRequest> req = new ThreadLocal<>();
    private static String[] AC_TEMPERAURE_PROPS_FOR_CONVERSION = new String[]{
            "temperature_strategy_statusinputs_statustarget",
            "temperature_strategy_statusinputs_statusactual",
            "temperature_strategy_statususer_setpoint",
            "temperature_strategy_statusfailsafe",
            "temperature_strategy_statuslast_adjustment",
            "temperature_driver_statussetpoint",
            "temperature_driver_statusrat",
            "temperature_driver_statussat",
            "temperature_balancer_statusinputs_statusactual",
            "temperature_balancer_statusinputs_statustarget",
            "temperature_balancer_statusinputs_statusactual",
            "temperaturetarget",
            "temperatureactual",
            "temperature_strategy_statusaoe_target",
            "temperature_strategy_statusaoe_actual",
            "resulttarget"

    };

    private static String[] AC_PRESSURE_PROS_FOR_CONVERSION = new String[]{
            "pressure_strategy_statusinputs_statustarget",
            "pressure_strategy_statusinputs_statusactual",
            "pressure_balancer_statusinputs_statustarget",
            "pressure_balancer_statusinputs_statusactual",
            "pressure_strategy_statuslast_adjustment",
            "pressuretarget",
            "pressureactual",
            "pressure_strategy_statusaoe_target",
            "pressure_strategy_statusaoe_actual",
            "resulttarget"
    };

    private TO<?> currentUser = null;


    static{
        Arrays.sort(AC_TEMPERAURE_PROPS_FOR_CONVERSION); // required for binary search.
        Arrays.sort(AC_PRESSURE_PROS_FOR_CONVERSION); // required for binary search.
    }


    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)	throws ServletException, IOException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Document document = null;

        ActiveControlService.conenv.set(Env.getEnv(request));
        ActiveControlService.req.set(request);
        String currentUserName =  (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        currentUser= Env.getUserDAO().getUser(currentUserName);
        LOG.info("Current User is " + this.currentUser);
        
        id = (id == Long.MAX_VALUE) ? 1L : id + 1;

        try {
            JSONObject body = new JSONObject(request.getParameter("jsonrpcRequest"));

            convertUnitsForAC(body);
            
            // Make sure the jsonrpc call has the required items.
            body.put("jsonrpc", "2.0");
            body.put("id", id);
            LOG.info("JSON Request: " + body.toString());

            // Call the active control service (ErlNodeService) in ES
            String respStr = Env.getActiveControlService(request).call_ac(body.toString());
            LOG.info("Response: " + respStr);

            JSONObject jsonResponse = new JSONObject(respStr);
            String xmlResponse = "";
            
            // If the return JSON has an "error" element, it should be in the form of
            // error:{code:'12345', message:'Some text'}
            // Get the message and use it as the message in a ServletException
            // TODO: Convert error message to more descriptive text
            if (jsonResponse.has("error")) {
                JSONObject error = jsonResponse.getJSONObject("error");
                xmlResponse = "<error>" + XML.toString(error) + "</error>";
                
            } else {
                Long retId = jsonResponse.getLong("id");
                Long sentId = body.getLong("id");
                if (retId == null || retId != sentId) {
                        // The return id should be the same as the request id
                        // If not, throw an exception to show a message indicating that
                        // the id was invalid.
                        //
                        String errMsg = LangManager.getInstance().getLocaleString(
                                LangManager.getCurrentLocale(request), "error/ac_invalid_response_id");
                        xmlResponse = "<error><code>100</code><message>" + errMsg + "</message></error>";
                } else {
                    // To build the document, the XML must have a root object.
                    // The xml built from the json response doesn't, so create 
                    // one called 'result'
                    if (jsonResponse.has("batch")) {
                        JSONArray batch = (JSONArray) jsonResponse.get("batch");
                        for (int i=0; i < batch.length(); i++){
                            JSONObject activeControlResponse = (JSONObject) batch.getJSONObject(i);
                            String parentProp = "";
                            if (activeControlResponse.has("resource")){
                                parentProp = activeControlResponse.getString("resource");
                            }
                            convUnitsForUI(parentProp, activeControlResponse);
                        }
                        xmlResponse = "<result><success>ok</success>" + XML.toString(batch) + "</result>";
                    } else {
                        Object result = jsonResponse.get("result");
                        if (result instanceof String) {
                            xmlResponse = "<success>" + result + "</success>";
                        } else {
                            // if we get here, it is a json object.
                            JSONObject activeControlResponse = (JSONObject) result;
                            String parentProp = "";
                            if (activeControlResponse.has("resource")){
                                parentProp = activeControlResponse.getString("resource");
                            }
                            convUnitsForUI(parentProp, activeControlResponse);
                            xmlResponse = "<result>" + XML.toString(result) + "</result>";
                        }
                    }
                }
            }

            LOG.info("XML Response: " + xmlResponse);

            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder;
            builder = factory.newDocumentBuilder();
            document = builder.parse( new InputSource( new StringReader( xmlResponse ) ) );

        } catch (AcApiException | JSONException | ParserConfigurationException | SAXException e) {
            throw new ServletException(e.getMessage(), e);
        }

        sendResponse(response, document);
    }

    /**
     * Convert temperature, pressure to SI standard and date to user's locale in AC response.
     * @param parentProperty
     * @param result
     * @throws JSONException
     */
    private void convUnitsForUI(String parentProperty, JSONObject result) throws JSONException{
        Iterator<String> iter = result.keys();
        while (iter.hasNext()) {
            Object key = iter.next();
            Object o = result.get((String)key);
            if (o instanceof JSONObject) {
                convUnitsForUI(parentProperty + key, (JSONObject)o);
            }else if (o instanceof JSONArray){
                JSONArray data = (JSONArray)o;
                for (int i=0; i < data.length(); i++){
                    Object arrayItem = data.get(i);
                    if (arrayItem instanceof JSONObject) {
                        convUnitsForUI(parentProperty + (String) key, (JSONObject)arrayItem);
                    }
                }
            } else {
               this.handleProperty(parentProperty, (String) key, result);
            }
        }
    }

    /**
     * Do the actual conversion based on the dimension of the property.
     * @param parentProperty
     * @param name
     * @param jsonObject
     * @throws JSONException
     */
    private void handleProperty(String parentProperty, String name, JSONObject jsonObject) throws JSONException {
        //LOG.info("Look for " + parentProperty + name+ " in prop Map and value=" +  jsonObject.get(name));
        if (jsonObject.isNull(name)){
            return;
        }
        String dimension = getDimensionforProperty(parentProperty + name);
        // if no dimension is configured, no conversion is needed.
        if (dimension != null) {
            if (dimension.equals(AC_DATE_FIELD)) {
                String[] last_adj =jsonObject.getString(name).split(LAST_ADJUSTMENT_SEPARATOR);
                long adjDate = Long.parseLong(last_adj[1]);
                String dateFormat = UserPreference.getString(this.currentUser, DATE_FORMAT, Env.getUserDAO());
                SimpleDateFormat dF = new SimpleDateFormat(dateFormat);
                String convDate = dF.format(new Date(adjDate));
                jsonObject.put(name, last_adj[0] + LAST_ADJUSTMENT_SEPARATOR + convDate);
            }else {
                String system = UserPreference.getString(this.currentUser, UNITS_SYSTEM, Env.getUserDAO());
                if (system.equalsIgnoreCase(UNIT_SYSTEM_SI)) {
                    double res = UnitConverter.convertValue(jsonObject.getDouble(name), dimension, UNIT_SYSTEM_US, UNIT_SYSTEM_SI);
                    jsonObject.put(name, res);
                }
            }
        }
        //LOG.info("Converted " + name + "to" + jsonObject.get(name));
    }

    /**
      Determine what dimension to use for this property.
      search for the property in one of the pre-defined arrays of
      known values. To add new fields, simply add them to the
      temperature or pressure arrays.
      For last_adjustment, return date.
      For any of the temperature properties, return 200.
      For any of the pressure  properties, return 202.
      For all others, no conversion is needed.
      @String property - name of the property.
     @return null or the dimension.
     */
    private String getDimensionforProperty(String property) {
        if (property.equalsIgnoreCase(TEMPERATURE_STRATEGY_STATUSLAST_ADJUSTMENT) || property.equalsIgnoreCase(PRESSURE_STRATEGY_STATUSLAST_ADJUSTMENT)){
            return AC_DATE_FIELD;
        }
        int index = -1;
        index = Arrays.binarySearch(AC_TEMPERAURE_PROPS_FOR_CONVERSION, property);
        // index falls within range of the array
        if (index < AC_TEMPERAURE_PROPS_FOR_CONVERSION.length && index > -1) {
           return UNIT_SYSTEM_TEMPERATURE_DIM;
        }

        index = Arrays.binarySearch(AC_PRESSURE_PROS_FOR_CONVERSION, property);

        if (index < AC_PRESSURE_PROS_FOR_CONVERSION.length && index > -1) {
            return UNIT_SYSTEM_PRESSURE_DIM;
        }

        return null;
    }

    /**
     * Convert pressure and temperature values back to US Standard for AC.
     * @param body
     * @throws JSONException
     */

    private void convertUnitsForAC(JSONObject body) throws JSONException{
        String system = UserPreference.getString(this.currentUser, UNITS_SYSTEM, Env.getUserDAO());
        if (system.equalsIgnoreCase(UNIT_SYSTEM_US)) {
            return; // if same unit system as server, no conversion is needed.
        }
        if (body.has(AC_PARAMS)) {
           JSONArray params = body.getJSONArray(AC_PARAMS);
            String method = body.getString(AC_METHOD);// all params must have a method property
            if (AC_SET_INPUT_CONFIG_COMMAND.equalsIgnoreCase(method)){
                JSONObject req = (JSONObject) params.get(1);
                if (req.has(AC_RESOURCE)){
                    req.put(AC_TARGET, transformUnitToAC(req.getString(AC_RESOURCE), req.getDouble(AC_TARGET)));
                }
            } else if (AC_SET_MANUAL_OUTPUT_COMMAND.equalsIgnoreCase(method) || AC_SET_INPUT_TARGET_COMMAND.equalsIgnoreCase(method)) {
                params.put(3, transformUnitToAC(params.getString(2), params.getDouble(3)));
            }
        } else if (body.has(AC_BATCH)){
            JSONArray batch = body.getJSONArray(AC_BATCH);
            for (int i=0; i < batch.length(); i++){
                convertUnitsForAC(batch.getJSONObject(i));
            }
        }
    }

    /**
     * Use UnitConvertor to convert temperature or pressure values.
     * resource is assumed to be either temperature or pressure.
     * @param resource
     * @param value
     * @return
     */

    private double transformUnitToAC(String resource, double value) {
        return UnitConverter.convertValue(value, resource.equalsIgnoreCase(AC_TEMPERATURE) ? UNIT_SYSTEM_TEMPERATURE_DIM : UNIT_SYSTEM_PRESSURE_DIM,
                                                    UNIT_SYSTEM_SI, UNIT_SYSTEM_US);
    }
}
