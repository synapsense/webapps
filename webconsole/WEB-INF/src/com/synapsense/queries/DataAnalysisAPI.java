package com.synapsense.queries;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.config.UIConfig;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.DataRetrievingUtils;
import com.synapsense.utilities.es.EnvXMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.PropertyId;

public class DataAnalysisAPI extends BaseHttpServlet {
    private static final long serialVersionUID = -5342080856581997372L;
    private static final Log LOG = LogFactory.getLog(DataAnalysisAPI.class);
    
    private static final List<String> types = UIConfig.getDataAnalysisExcludedTypes();
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{

        String type = request.getParameter("type");
        Document result = null;
        try{
            if (type != null && type.equalsIgnoreCase("props")) {
                String par_ids = request.getParameter("ids");
                if(!"".equals(par_ids)) {
                    ArrayList<Document> objectsProperties = new ArrayList<Document>();
                    String[] objIdsArr = par_ids.split(",");
                    
                    for(String id : objIdsArr) {
                        objectsProperties.add(getSensors(request, id));
                    }
                    result = getCrossProperties(request, objectsProperties);
                }
            } else {
                result = getObjects(request, type, request.getParameter("id"));
            }
        }catch(Exception e){
            throw new ServletException(e.getLocalizedMessage(), e);
        }
        
        String str = "<result>Failed</result>";
        if(result != null){
            str = XMLHelper.getDocumentXml(result);
        }
        sendResponse(response, str);
        if (result != null){
            LOG.debug(XMLHelper.getDocumentXml(result));
        }
    }
    
    private TO<?> getTheMostGeneralParent(TO<?> id, HttpServletRequest request) throws ObjectNotFoundException {
        // WARNING: This method is the dirty hack. 
        // It is designed to add support to the Data Analysis tree for the objects with several parents
        // Returns the id's parent which has the most number of children
        Collection<TO<?>> parents = Env.getEnv(request).getParents(id);
        parents = filterByAllowedDataAnalysisTypes(parents, true);
        
        if(parents.size() == 1) {
            return parents.iterator().next();
        } else {
            TO<?> maxParent = null;
            long maxNumChildren = -1;
            for(TO<?> pto : parents) {
                Collection<TO<?>> children = Env.getEnv(request).getChildren(pto);
                long numChildren = children.size();
                if(numChildren > maxNumChildren) {
                    maxNumChildren = numChildren;
                    maxParent = pto;
                }
            }
            return maxParent;
        }
    }
    
    private Document getCrossProperties(HttpServletRequest request, ArrayList<Document> objectsPropertiesDocs) throws XPathExpressionException, DOMException, JSONException, ParserConfigurationException {
        Document result = null;
        PropertyId propertyIdFirst;
        PropertyId propertyIdCross;
        ArrayList<Node> crossNodeList = new ArrayList<Node>();
        
        XPath xpath = XPathFactory.newInstance().newXPath();
        XPathExpression exprSensors = xpath.compile("sensors/*");        
        XPathExpression exprSensorId = xpath.compile("id/text()");        
        XPathExpression exprSensorName = xpath.compile("name/text()");
        
        for(int i = 0; i < objectsPropertiesDocs.size(); i++) {
            String parentId = objectsPropertiesDocs.get(i).getDocumentElement().getAttribute("parentId");            
            NodeList nodelist = (NodeList)exprSensors.evaluate(objectsPropertiesDocs.get(i), XPathConstants.NODESET);
            for(int j = 0; j < nodelist.getLength(); j++) {
                Node nodeId = (Node) exprSensorId.evaluate(nodelist.item(j), XPathConstants.NODE);
                String id = nodeId.getNodeValue();                
                nodeId.setNodeValue(id + "|" + parentId);
            }
        }
        
        if(objectsPropertiesDocs.size() == 1) {
            return objectsPropertiesDocs.get(0);
        }
        
        NodeList nodelistFirstDoc = (NodeList)exprSensors.evaluate(objectsPropertiesDocs.get(0), XPathConstants.NODESET);
        
        for(int i = 0; i < nodelistFirstDoc.getLength(); i++) {
            Node nodeFirst = nodelistFirstDoc.item(i);
            propertyIdFirst = PropertyId.fromString( ((Node) exprSensorId.evaluate(nodeFirst, XPathConstants.NODE)).getNodeValue().split("\\|")[0]);
            crossNodeList.clear();
            
            for(int j = 1; j < objectsPropertiesDocs.size(); j++) {
                NodeList nodelistCrossDoc = (NodeList)exprSensors.evaluate(objectsPropertiesDocs.get(j), XPathConstants.NODESET);
                
                for(int k = 0; k < nodelistCrossDoc.getLength(); k++) {
                    Node nodeCross = nodelistCrossDoc.item(k);
                    propertyIdCross = PropertyId.fromString( ((Node) exprSensorId.evaluate(nodeCross, XPathConstants.NODE)).getNodeValue().split("\\|")[0]);
             
                    if(TOFactory.getInstance().loadTO(propertyIdFirst.getObjectId()).getTypeName().equals(EnvironmentTypes.TYPE_SENSOR) 
                            && TOFactory.getInstance().loadTO(propertyIdCross.getObjectId()).getTypeName().equals(EnvironmentTypes.TYPE_SENSOR)) {
                        String nameFirst = ((Node) exprSensorName.evaluate(nodeFirst, XPathConstants.NODE)).getNodeValue();
                        String nameCross = ((Node) exprSensorName.evaluate(nodeCross, XPathConstants.NODE)).getNodeValue();
                        if(nameFirst.equals(nameCross)) {
                            crossNodeList.add(nodeCross);
                            break;
                        }
                            
                    } else if(propertyIdFirst.getPropName().equals(propertyIdCross.getPropName())) {
                        crossNodeList.add(nodeCross);
                        break;
                    }
                }
            }
            
            if(crossNodeList.size() == objectsPropertiesDocs.size() - 1) {
                if(result == null) {
                    result = XMLHelper.createNewXmlDocument("sensors");
                }
                Node root = result.getDocumentElement();
                Node importedNode = result.importNode(nodeFirst, true);
                
                String ids = "";
                crossNodeList.add(importedNode);
                
                for(int n = 0; n < crossNodeList.size(); n++) {
                    String pref = ";";
                    if(n == 0) {
                        pref = "";
                    }
                    Node nodeId = (Node) exprSensorId.evaluate(crossNodeList.get(n), XPathConstants.NODE);
                    String id = nodeId.getNodeValue();                        
                    ids += pref + id;
                }
                
                Node nodeId = (Node) exprSensorId.evaluate(importedNode, XPathConstants.NODE);
                nodeId.setNodeValue(ids);
                
                root.appendChild(importedNode);                    
            }
        }
            
        return result; 
    }
    
    private Collection<TO<?>> getChildren(HttpServletRequest request, TO<?> obj, String type) throws ObjectNotFoundException {
        Collection<TO<?>> children;
        
        LOG.debug("Get '" + type + "' children of '" + obj.getTypeName() + "'");
        
        children = new HashSet<TO<?>>(Env.getEnv(request).getChildren(obj, type));
        
        return children;
        
    }
    
    private Document getObjects(HttpServletRequest request, String type, String id) throws ServletException
    {
        Document result = null;
        try {
            ArrayList<String> props = new ArrayList<String>(2);
            //props.add("id");
            props.add("name");
            
            result = XMLHelper.createNewXmlDocument("objects");
            Element root = null;

            TO<?> obj = TOFactory.getInstance().loadTO(id);
            if("go_up".equals(type)) {
                ArrayList<String> parentsNamesArr = getParentsNames(Env.getEnv(request), obj, request);
                String parentsNamesStr = "";
                for(String parentName : parentsNamesArr) {
                    parentsNamesStr += parentName + ": ";
                }
                result.getDocumentElement().setAttribute("pathToParent", parentsNamesStr);
            }
            
            if("go_up".equals(type)) {
                obj = getTheMostGeneralParent(obj, request);
            }
            
            
            Set<String> typeSet = new HashSet<String>();

            Collection<TO<?>> children = getChildren(request, obj, null);

            for(TO<?> child : children) {
                if(!types.contains(child.getTypeName())) {
                    typeSet.add(child.getTypeName());
                }
            }
            
            if(typeSet.isEmpty()) {
                return null;
            }

            if (!obj.getTypeName().equals(EnvironmentTypes.TYPE_ROOT)) {
                String objName = Env.getEnv(request).getPropertyValue(obj, "name", String.class);
                root = result.createElement("parent");
                root.setAttribute("parent", objName);
                root.setAttribute("name", "..");
                root.setAttribute("id", TOFactory.getInstance().saveTO(obj));
                result.getDocumentElement().appendChild(root);
            }

            List<String> tList = new ArrayList<String>(typeSet);
            Collections.sort(tList);
            
            for (String t: tList) {
                Collection<TO<?>> objects = getChildren(request, obj, t);
                Document doc = EnvXMLHelper.retrieveXmlWithAttr(objects, props, "objects", null, request, "name");
                
                Element el = doc.getDocumentElement();
                NodeList list = el.getChildNodes();
                
                if (list.getLength() > 0 && !EnvironmentTypes.TYPE_PUE.equals(t)) {
                    root = result.createElement("category");
                    root.setAttribute("name", TypeUtil.getInstance().getDisplayableTypeName(t));
                    root.setAttribute("type", t);
                    for (int i = 0; i < list.getLength(); i++) {
                        root.appendChild(result.importNode(list.item(i), true));
                    }
                } else if(EnvironmentTypes.TYPE_PUE.equals(t) && list.getLength() == 1) {
                    root = result.createElement(list.item(0).getNodeName());
                    root.setAttribute("name", t);
                    NamedNodeMap nnm = list.item(0).getAttributes();
                    for (int i=0; i < nnm.getLength(); i++) {
                        root.setAttribute(nnm.item(i).getNodeName(), nnm.item(i).getNodeValue());
                    }	
                }
                result.getDocumentElement().appendChild(root);
                
            }
            
            
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
            throw new ServletException("Object is removed", e);
        } catch (PropertyNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (UnableToConvertPropertyException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return result;
    }
    
    private Collection<TO<?>> filterByAllowedDataAnalysisTypes(Collection<TO<?>> toCol, boolean allowRoot) {
        Collection<TO<?>> toAllowedCol = new ArrayList<TO<?>>();
        List<String> excludedTypeNames = new ArrayList<String>(UIConfig.getDataAnalysisExcludedTypes());
        if(allowRoot) {
            excludedTypeNames.remove(EnvironmentTypes.TYPE_ROOT);
        }	    
        
        for(TO<?> objTo : toCol) {
            if(!excludedTypeNames.contains(objTo.getTypeName())) {
                toAllowedCol.add(objTo);
            }
        }
        return toAllowedCol;
    }
    
    /**
     * Gets sensors, linked sensors and historical properties
     * @param par_id
     * @param par_type
     * @return
     */
    private Document getSensors(HttpServletRequest request, String par_id){
        Document result = null;
        Environment env = Env.getEnv(request);
        
        try{
            TO<?> parent = TOFactory.getInstance().loadTO(par_id);
            ArrayList<String> props = new ArrayList<String>(2);
            //props.add("id");
            props.add("name");
            
            String par_type = parent.getTypeName();
            String par_name = null;
            try {
                par_name = env.getPropertyValue(parent, "name", String.class);
            } catch (PropertyNotFoundException e) {
                // assuming name is equal to type name
                par_name = par_type;
            }
            
            result = XMLHelper.createNewXmlDocument("sensors");
            Element root = result.getDocumentElement();
            if (par_type.equalsIgnoreCase(EnvironmentTypes.TYPE_NODE))
            {
                Collection<CollectionTO> colColTo = env.getPropertyValue(env.getChildren(parent, EnvironmentTypes.TYPE_SENSOR), new String[]{"name","dataclass"});
                
                List<CollectionTO> sortedList = new ArrayList<CollectionTO>(colColTo);
                Collections.sort(sortedList,
                        new Comparator<CollectionTO> () {						
                        @Override
                        public int compare(CollectionTO cto1, CollectionTO cto2){
                            String name1 = (String)cto1.getSinglePropValue("name").getValue();
                            String name2 = (String)cto2.getSinglePropValue("name").getValue();
                            return name1.compareTo(name2);
                        }
                });
                
                
                for (CollectionTO colTo : sortedList) {
                    ValueTO dataclassProperty = DataRetrievingUtils.getPropertyByName("dataclass", colTo.getPropValues());
                    if(dataclassProperty != null
                        && DataRetrievingUtils.isClassIdExcluded(dataclassProperty.getValue().toString(), request.getSession().getServletContext())){
                        continue;					
                    }
                    
                    Element sensor = result.createElement("sensor");					

                    Element child = result.createElement("name");
                    child.setTextContent((String) colTo.getPropValue("name").get(0).getValue());
                    sensor.appendChild(child);
                    
                    child = result.createElement("id");
                    child.setTextContent(new PropertyId(TOFactory.getInstance().saveTO(colTo.getObjId()), "lastValue").toString());
                    sensor.appendChild(child);
                    
                    root.appendChild(sensor);
                }
            }else{				
                ArrayList<String> histProps = EnvTypeUtil.getHistoricalPropertyNames(par_type, env);				
                ArrayList<ValueTO> sensProps = EnvTypeUtil.getPropertySensors(parent, env);
                List<String> orderedProps = TypeUtil.getInstance().listPropertyNames(par_type);
                                
                CommonFunctions.removeExcludedHistProps(histProps); // patch: we don't want to see x&y in DA. See Bug 9162 
                
                //first add ordered properties
                for(int i = 0; i < orderedProps.size(); i++){
                    String currProp = orderedProps.get(i);
                    
                    if(histProps.contains(currProp)){
                        addProperty(currProp, result, root, par_id, par_type, par_name);
                        histProps.remove(currProp);
                    }else{
                        ValueTO sensProp = EnvTypeUtil.getPropertyByName(currProp, sensProps);
                        if(sensProp != null){
                            addProperty(currProp, result, root, par_id, par_type, par_name);
                            sensProps.remove(sensProp);
                        }
                    }
                }
                
                //add other properties
                for(ValueTO p : sensProps){
                    histProps.add(p.getPropertyName());
                }				
                CommonFunctions.sortPropsAlphabetically(histProps, par_type, request);				
                for(String propName : histProps){
                    addProperty(propName, result, root, par_id, par_type, par_name);      	
                }
            }			
            
            result.getDocumentElement().setAttribute("parentName", par_name);
            result.getDocumentElement().setAttribute("parentId", par_id);
            
        }catch (Exception e) {			
            LOG.error(e.getLocalizedMessage(), e);
        }
    
        return result;
    }

    private void addProperty(String propertyName, Document doc, Element root, String par_id, String par_type, String par_name) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
        PropertyId id = new PropertyId();
            
        String dispName = TypeUtil.getInstance().getDisplayablePropertyName(par_type.toUpperCase(), propertyName);		    		
        
        Element sensor = doc.createElement("sensor");					
        Element child = doc.createElement("name");
        child.setTextContent(dispName);
        sensor.appendChild(child);
        
        id.setObjectId(par_id);
        id.setPropName(propertyName);
        
        child = doc.createElement("id");
        child.setTextContent(id.toString());
        sensor.appendChild(child);
        
        child = doc.createElement("parentName");
        child.setTextContent(par_name);
        sensor.appendChild(child);
        
        root.appendChild(sensor);
    }
    
    private ArrayList<String> getParentsNames(Environment env, TO<?> objId, HttpServletRequest req) {
        try {
            TO<?> p = getTheMostGeneralParent(objId, req);
            if(!EnvironmentTypes.TYPE_ROOT.equals(p.getTypeName())) {
                ArrayList<String> arr = getParentsNames(env, p, req);
                String pName = env.getPropertyValue(p, "name", String.class);
                arr.add(pName);
                return arr;
            }
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }catch (PropertyNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (UnableToConvertPropertyException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return new ArrayList<String>();
    }
}
