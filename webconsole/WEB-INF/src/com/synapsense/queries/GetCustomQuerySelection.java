package com.synapsense.queries;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.presentation.GetPropertyData;
import com.synapsense.service.Environment;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.PropertyId;

public class GetCustomQuerySelection extends BaseHttpServlet {
    private static final Log LOG = LogFactory.getLog(GetCustomQuerySelection.class);
    public static final long serialVersionUID = 0;

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        getCustomQuerySelection(request, response);
    }
    
    private void getCustomQuerySelection(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        try{
            sendResponse(response, getCustomQuerySelectionDoc(request));
        }catch(Exception e){
            throw new ServletException(e.getMessage(), e);
        }
    }
    
    private Document getCustomQuerySelectionDoc(HttpServletRequest request) throws ParserConfigurationException, NumberFormatException, ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, XPathExpressionException, SAXException, IOException, JSONException{
        
        Document customQuerySelectionDoc = XMLHelper.createNewXmlDocument("data_requests");
        Element customQuerySelectionRoot = customQuerySelectionDoc.getDocumentElement();
        
            String objectsIds = request.getParameter("selectedDataRequests");
            ArrayList<PropertyId> propertyIds = new ArrayList<PropertyId>();
            Collection<TO<?>> sensors = new HashSet<TO<?>>();
            
            GetPropertyData.getObjectsIds(objectsIds, propertyIds, sensors);
            
        //add simple ids to xml
        addSensorIdsToSelectionDoc(request, sensors, customQuerySelectionDoc, customQuerySelectionRoot);
        //add wide ids to xml
        addPropertyIdsToSelectionDoc(request, propertyIds, customQuerySelectionDoc, customQuerySelectionRoot);
            
        return customQuerySelectionDoc;
    }			
                                    
    private void addSensorIdsToSelectionDoc(HttpServletRequest request, Collection<TO<?>> sensors, Document customQuerySelectionDoc, Element customQuerySelectionRoot) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, ParserConfigurationException{
        Environment env = Env.getEnv(request);
        for(TO<?> sensor: sensors){
            String description = null;
            String objectName = null;
            String objectType = null;
            String parentName = null;
            
            try{				
                //get node name
                Collection<TO<?>> node = env.getParents(sensor, EnvironmentTypes.TYPE_NODE);
                TO<?> parentNode = node.iterator().next();
                objectName = env.getPropertyValue(parentNode, "name", String.class);
                objectType = sensor.getTypeName();
                
                Collection<TO<?>> room = env.getParents(parentNode, EnvironmentTypes.TYPE_ROOM);
                TO<?> parentParent = room.iterator().next();
                parentName = env.getPropertyValue(parentParent, "name", String.class);
            
                //get sensor details
                description = env.getPropertyValue(sensor, "name", String.class);

                addRequestToXml(objectName, parentName, description, new PropertyId(sensor.toString(), "lastValue").toString(), objectType, customQuerySelectionDoc, customQuerySelectionRoot, parentNode);
            }catch (ObjectNotFoundException e) {
                //object was not found, just skip the entry
                LOG.info("Object " + sensor + " was not found. Custom Query entry was skipped");
            }
        }
    }
    private void addPropertyIdsToSelectionDoc(HttpServletRequest request, List<PropertyId> propertyIds, Document customQuerySelectionDoc, Element customQuerySelectionRoot) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, ParserConfigurationException, XPathExpressionException, SAXException, IOException{		
        for (PropertyId propertyID :  propertyIds){
            String description = null;
            String objectName = null;
            String parentName = null;
            
            try{
                TO<?> obj = TOFactory.getInstance().loadTO(propertyID.getObjectId());
                try {
                    objectName = Env.getEnv(request).getPropertyValue(obj, "name", String.class);
                } catch (PropertyNotFoundException e) {
                    // assuming name is equal to type name
                    objectName = obj.getTypeName();
                }
                
                Collection<TO<?>> objParents = Env.getEnv(request).getParents(obj);
                TO<?> parentNode = objParents.iterator().next();
                parentName = Env.getEnv(request).getPropertyValue(parentNode, "name", String.class);
            
                description = TypeUtil.getInstance().getDisplayablePropertyName(TOFactory.getInstance().loadTO(propertyID.getObjectId()).getTypeName(), propertyID.getPropName());			
                addRequestToXml(objectName, parentName, description, propertyID.toString(), TOFactory.getInstance().loadTO(propertyID.getObjectId()).getTypeName(), customQuerySelectionDoc, customQuerySelectionRoot, obj);
            }catch (ObjectNotFoundException e) {
                //object was not found, just skip the entry
                LOG.info("Object " + propertyID.getObjectId() + " was not found. Custom Query entry was skipped");
            }
        }		
    }
                    
    private void addRequestToXml(String objectName, String parentName, String description, String objectId, String requestChannel, Document customQuerySelectionDoc, Element customQuerySelectionRoot, TO<?> parentTO) throws ParserConfigurationException{		
        Document objectDoc = XMLHelper.createNewXmlDocument("data_request");
        Element objectRoot = objectDoc.getDocumentElement();
                
        Node nameNode = XMLHelper.createXmlTextNode(objectDoc, "name", objectName);
        Node requestObjectId = XMLHelper.createXmlTextNode(objectDoc, "id", objectId);
        Node requestChannelNode = XMLHelper.createXmlTextNode(objectDoc, "type", requestChannel);
        Node requestNameNode = XMLHelper.createXmlTextNode(objectDoc, "data_request_name", description);
        Node parentTypeNode = XMLHelper.createXmlTextNode(objectDoc, "parentType", parentTO.getTypeName());
        Node parentIdNode = XMLHelper.createXmlTextNode(objectDoc, "parentId", TOFactory.getInstance().saveTO(parentTO));
        Node parentNameNode = XMLHelper.createXmlTextNode(objectDoc, "parentName", parentName);
                    
        objectRoot.appendChild(nameNode);
        objectRoot.appendChild(requestObjectId);
        objectRoot.appendChild(requestChannelNode);
        objectRoot.appendChild(requestNameNode);
        objectRoot.appendChild(parentTypeNode);
        objectRoot.appendChild(parentIdNode);
        objectRoot.appendChild(parentNameNode);
                
        customQuerySelectionRoot.appendChild(customQuerySelectionDoc.importNode(objectRoot, true));		
    }
    
}
