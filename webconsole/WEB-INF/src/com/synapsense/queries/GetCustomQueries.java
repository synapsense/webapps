package com.synapsense.queries;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.permissions.PermissionUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.PropertyId;

public class GetCustomQueries extends BaseHttpServlet {
    public static final long serialVersionUID = 0;

    private static final Log LOG = LogFactory.getLog(GetCustomQueries.class);
    
    private static String[] privileges = {"CREATE_EDIT_DELETE_OWN_PRIVATE_QUERIES","CREATE_EDIT_DELETE_OWN_PUBLIC_QUERIES","CREATE_EDIT_DELETE_ALL_QUERIES","ACCESS_CUSTOM_QUERIES"};
    private static Map<String, String[]> privMap = new HashMap<String, String[]>();
    static {
        privMap.put("or", privileges);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            getCustomQueries(request, response);
        } catch (JSONException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    private void getCustomQueries(HttpServletRequest request,
            HttpServletResponse response) throws JSONException,ServletException {
        try {
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 1);
            response.setDateHeader("Last-modified", System.currentTimeMillis());
            sendResponse(response, getCustomQueriesDoc(request));
        } catch (IOException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    private Document getCustomQueriesDoc(HttpServletRequest request) {
        
        Document customQueryDoc = null;		
        try {
            customQueryDoc = XMLHelper.createNewXmlDocument("queries");
            Element customQueryDocRoot = customQueryDoc.getDocumentElement();
            
            String currentUserName = (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            if(currentUserName == null) {
                throw new SessionExpiredException("Session was destroyed");
            }
            TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);
                        
            Node currUserNode = XMLHelper.createXmlTextNode(customQueryDoc,
                    "curr_user", String.valueOf(currentUser.getID()));
            customQueryDocRoot.appendChild(currUserNode);

            Environment env = Env.getLocalEnv(request);
            Collection<TO<?>> queries = env.getObjectsByType(EnvironmentTypes.TYPE_CUSTOMQUERY);
            if (queries.isEmpty()) {
                return customQueryDoc;
            }
            
            Collection<CollectionTO> colColTo = env.getAllPropertiesValues(queries);
            List<Node> privateQueryNodes = new ArrayList<Node>();
            List<Node> publicQueryNodes = new ArrayList<Node>();
            
            List<CollectionTO> sortedQueries = CollectionTOSorter.sortObjects(colColTo);
            for (CollectionTO queryColTo : sortedQueries) {
                String idsStr = (String)queryColTo.getPropValue(RequestVariableNames.CUSTOM_DATA_REQUEST_IDS).get(0).getValue();
                String linesDescrStr = (String)queryColTo.getPropValue(RequestVariableNames.CUSTOM_QUERY_LINES_DESCR).get(0).getValue();
                PropertyId[] propIds = PropertyId.arrayFromString(idsStr);
                
                LinkedHashMap<PropertyId, JSONArray> propId_lineDescrMap = new LinkedHashMap<PropertyId, JSONArray>();                
                JSONArray lineDescrJsonArr = new JSONArray(linesDescrStr);
                
                for(int i = 0; i < propIds.length; i++) {
                    propId_lineDescrMap.put(propIds[i], lineDescrJsonArr.getJSONArray(i));
                }
                if(!filterNotExistingEnvironmentObjects(propId_lineDescrMap, env)) {
                    LOG.info("Removing " + queryColTo.getObjId() + " since all assosiated objects were deleted");
                    env.deleteObject(queryColTo.getObjId());
                    continue;
                }

                filterByPermission(currentUser, env, propId_lineDescrMap);
                
                if (!propId_lineDescrMap.keySet().isEmpty()) {
                    Integer isPrivate = (Integer) queryColTo.getPropValue(RequestVariableNames.IS_PRIVATE).get(0).getValue();
                    Integer userId = (Integer) queryColTo.getPropValue(RequestVariableNames.CUSTOM_QUERY_USERID).get(0).getValue();
                    
                    boolean hasPrivs = UserUtils.hasPrivileges(currentUser, privMap);
                    boolean hasRootPrivs = (UserUtils.hasPrivilege(currentUser, "CREATE_EDIT_DELETE_ALL_QUERIES") || UserUtils.hasRootPrivilege(currentUser));
                    
                    if (!hasPrivs || (hasPrivs && isPrivate.equals(1) && !currentUser.getID().equals(userId) && !hasRootPrivs)) {
                        continue;
                    }
                    
                    PropertyId[] propIdArr = new PropertyId[propId_lineDescrMap.keySet().size()];
                    propId_lineDescrMap.keySet().toArray(propIdArr);					
                    String associatedSensors = PropertyId.toString(propIdArr);
                    
                    lineDescrJsonArr = new JSONArray();
                    for(Object o : propId_lineDescrMap.values()) {
                        lineDescrJsonArr.put(o);
                    }
                    linesDescrStr = lineDescrJsonArr.toString();
                    
                    Node queryNode = XMLHelper.createXmlNode(customQueryDoc,
                            "query", new HashMap<String, String>());

                    Node queryIdNode = XMLHelper.createXmlNodeWithValue(
                            customQueryDoc, "id",
                            TOFactory.getInstance().saveTO(queryColTo.getObjId()));
                    
                    Node queryNameNode = XMLHelper.createXmlTextNode(
                            customQueryDoc, "name", 
                            (String) queryColTo.getPropValue("name").get(0).getValue());

                    Node dataRequestsNode = XMLHelper.createXmlTextNode(
                            customQueryDoc,
                            "data_request_ids",
                            associatedSensors);

                    Node userNode = XMLHelper.createXmlTextNode(
                            customQueryDoc, "user_id",
                            String.valueOf(userId));

                    Node isPrivateNode = XMLHelper.createXmlTextNode(
                            customQueryDoc, "is_private",
                            String.valueOf(isPrivate));

                    Node queryDescNode = XMLHelper.createXmlTextNode(
                            customQueryDoc,
                            "description",
                            (String) queryColTo.getPropValue(RequestVariableNames.CUSTOM_QUERY_DESCRIPTION).get(0).getValue());

                    Node queryLinesDescNode = XMLHelper.createXmlTextNode(
                            customQueryDoc,
                            RequestVariableNames.CUSTOM_QUERY_LINES_DESCR,
                            linesDescrStr);
                    
                    Node queryChartDescNode = XMLHelper.createXmlTextNode(
                            customQueryDoc,
                            RequestVariableNames.CUSTOM_QUERY_CHART_DESCR,
                            (String) queryColTo.getPropValue(RequestVariableNames.CUSTOM_QUERY_CHART_DESCR).get(0).getValue());
                            
                    queryNode.appendChild(queryIdNode);
                    queryNode.appendChild(queryNameNode);
                    queryNode.appendChild(dataRequestsNode);
                    queryNode.appendChild(userNode);
                    queryNode.appendChild(isPrivateNode);
                    queryNode.appendChild(queryDescNode);
                    queryNode.appendChild(queryLinesDescNode);
                    queryNode.appendChild(queryChartDescNode);

                    //to sort queries
                    if (isPrivate.equals(1)) {
                        privateQueryNodes.add(queryNode);
                    } else {
                        publicQueryNodes.add(queryNode);
                    }
                }
            }
            
            privateQueryNodes.addAll(publicQueryNodes);
            for (Node node : privateQueryNodes) {
                customQueryDocRoot.appendChild(node);
            }


        } catch (JSONException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (EnvException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return customQueryDoc;
    }

    //return true if all query objects where deleted
    private boolean filterNotExistingEnvironmentObjects(HashMap<PropertyId, JSONArray> propId_lineDescrMap, Environment env) {
        List<PropertyId> objectsToFilter = new ArrayList<PropertyId>();
        boolean atleastOneObjExists = false;
        
        for(PropertyId propId : propId_lineDescrMap.keySet()) {
            if(!env.exists(propId.getObjectTO())) {
                objectsToFilter.add(propId);
            } else {
                atleastOneObjExists = true;
            }
        }
        for(PropertyId objectToFilter : objectsToFilter) {
            propId_lineDescrMap.remove(objectToFilter);
        }
        return atleastOneObjExists;
    }

    private void filterByPermission(TO<?> currentUser, Environment env, HashMap<PropertyId, JSONArray> propId_lineDescrMap) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        List<PropertyId> objectsToFilter = new ArrayList<PropertyId>();
        
        if(!UserUtils.hasRootPrivilege(currentUser)) {
            for(PropertyId propId : propId_lineDescrMap.keySet()) {
                if(!isPermittedObject(currentUser, env, propId)) {
                    objectsToFilter.add(propId);
                }
            }
            for(PropertyId objectToFilter : objectsToFilter) {
                propId_lineDescrMap.remove(objectToFilter);
            }
        }
    }
    
    private boolean isPermittedObject(TO<?> currentUser, Environment env, PropertyId propId) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        TO<?> obj = propId.getObjectTO();
        String typeName = obj.getTypeName();
        Collection<TO<?>> parentCTO = null;
        boolean objectPermitted = true;
        
        if(typeName.equals(EnvironmentTypes.TYPE_DC)) {
            return dc_or_room_HasPermissionForUser(currentUser, env, obj);
        } else if(typeName.equals(EnvironmentTypes.TYPE_ROOM)) {
            if(dc_or_room_HasPermissionForUser(currentUser, env, obj)) {
                TO<?> dc = env.getRelatedObjects(obj, EnvironmentTypes.TYPE_DC, false).iterator().next();
                return dc_or_room_HasPermissionForUser(currentUser, env, dc);
            }
            return false;
        }
        
        parentCTO = env.getRelatedObjects(obj, EnvironmentTypes.TYPE_ROOM, false);
        if(parentCTO.size() > 0) {
            TO<?> room = parentCTO.iterator().next();
            objectPermitted &= dc_or_room_HasPermissionForUser(currentUser, env, room);
        }
        parentCTO = env.getRelatedObjects(obj, EnvironmentTypes.TYPE_DC, false);
        if(parentCTO.size() > 0) {
            TO<?> dc = parentCTO.iterator().next();
            objectPermitted &= dc_or_room_HasPermissionForUser(currentUser, env, dc);
        }
        return objectPermitted;
    }
    
    private boolean dc_or_room_HasPermissionForUser(TO<?> currentUser, Environment env, TO<?> DC_or_ROOM) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        String typeName = DC_or_ROOM.getTypeName();
        
        if(typeName.equals(EnvironmentTypes.TYPE_DC) || typeName.equals(EnvironmentTypes.TYPE_ROOM)) {
            String permittedUsers = env.getPropertyValue(DC_or_ROOM, "permissions", String.class);
            String userName = (String) Env.getUserDAO().getProperty(currentUser, UserManagementService.USER_NAME);
            return PermissionUtils.isUserPermitted(permittedUsers,userName);
        }
        return true;
    }
}
