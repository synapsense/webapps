package com.synapsense.queries;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.es.EnvironmentTypes;

public class SaveDataAnalysisCustomQuery extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(SaveDataAnalysisCustomQuery.class);
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String resultMessage = ErrorMessages.UNABLE_TO_COMPLETE;
        try{
            resultMessage = createDataAnalysisCustomQuery(request);
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        sendResponse(response, "<result>" + resultMessage + "</result>");
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    private String createDataAnalysisCustomQuery(HttpServletRequest request) throws EnvException {
        String result = null;
        
        String rewriteCustomQuery = request.getParameter(RequestVariableNames.REWRITE_CUSTOM_QUERY);
        String isNewCustomQuery = request.getParameter(RequestVariableNames.IS_NEW_CUSTOM_QUERY);
        String nameIsChanged = request.getParameter(RequestVariableNames.CUSTOM_QUERY_NAME_IS_CHANGED);
        String customQueryName = request.getParameter(RequestVariableNames.CUSTOM_QUERY_NAME);
        
        if(isCustomQueryNameInUse(request, customQueryName)) {
            if("true".equals(isNewCustomQuery) || "true".equals(nameIsChanged)) {
                return ErrorMessages.DUPLICATE_CUSTOM_QUERY_NAME;
            }

            if("true".equals(rewriteCustomQuery)) {
                removeExistingCustomQuery(request, customQueryName);
            }
        }
        
        int numRowsAffected = addDataAnalysisCustomQuery(request, isNewCustomQuery);
        if(numRowsAffected == -1) {
            result = ErrorMessages.OBJECT_NOT_FOUND;
        } else if(numRowsAffected > 0) {
            result = ErrorMessages.SUCCESS;
        } else {
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        
        return result;
    }
    
    private int addDataAnalysisCustomQuery(HttpServletRequest request, String isNewCustomQuery) throws EnvException {
        
        Environment env = Env.getEnv(request);
        ObjectType[] queries = env.getObjectTypes(new String[]{EnvironmentTypes.TYPE_CUSTOMQUERY});

        //If "customquery" type doesn't exist, create it
        if (queries == null || queries.length < 1)
        {
            PropertyDescr[] props = {
                    new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr(RequestVariableNames.CUSTOM_DATA_REQUEST_IDS, String.class.getName()),
                    new PropertyDescr(RequestVariableNames.IS_PRIVATE, Integer.class.getName()),
                    new PropertyDescr(RequestVariableNames.CUSTOM_QUERY_DESCRIPTION, String.class.getName()),
                    new PropertyDescr(RequestVariableNames.CUSTOM_QUERY_USERID, Integer.class.getName()),
                    new PropertyDescr(RequestVariableNames.CUSTOM_QUERY_LINES_DESCR, String.class.getName()),
                    new PropertyDescr(RequestVariableNames.CUSTOM_QUERY_CHART_DESCR, String.class.getName())
            };
            
            ObjectType newtype;
            newtype = env.createObjectType(EnvironmentTypes.TYPE_CUSTOMQUERY);
            newtype.getPropertyDescriptors().add(props[0]);
            newtype.getPropertyDescriptors().add(props[1]);
            newtype.getPropertyDescriptors().add(props[2]);
            newtype.getPropertyDescriptors().add(props[3]);
            newtype.getPropertyDescriptors().add(props[4]);
            newtype.getPropertyDescriptors().add(props[5]);
            newtype.getPropertyDescriptors().add(props[6]);
            env.updateObjectType(newtype);			
        }
        
        //Try to create new object. If success return 1. Else return 0.
        String currentUserName = (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUserName == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        try {
            TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);

            ValueTO [] ValueArr = {
                    new ValueTO("name", request.getParameter(RequestVariableNames.CUSTOM_QUERY_NAME)),
                    new ValueTO(RequestVariableNames.CUSTOM_DATA_REQUEST_IDS, request.getParameter(RequestVariableNames.CUSTOM_DATA_REQUEST_IDS)),
                    new ValueTO(RequestVariableNames.IS_PRIVATE, Integer.parseInt(request.getParameter(RequestVariableNames.IS_PRIVATE))),
                    new ValueTO(RequestVariableNames.CUSTOM_QUERY_DESCRIPTION, request.getParameter(RequestVariableNames.CUSTOM_QUERY_DESCRIPTION)),
                    new ValueTO(RequestVariableNames.CUSTOM_QUERY_USERID, currentUser.getID()),
                    new ValueTO(RequestVariableNames.CUSTOM_QUERY_LINES_DESCR, request.getParameter(RequestVariableNames.CUSTOM_QUERY_LINES_DESCR)),
                    new ValueTO(RequestVariableNames.CUSTOM_QUERY_CHART_DESCR, request.getParameter(RequestVariableNames.CUSTOM_QUERY_CHART_DESCR))					
                    };
            
            if ("true".equals(isNewCustomQuery)) {
                env.createObject(EnvironmentTypes.TYPE_CUSTOMQUERY, ValueArr);
            } else {
                String userId = request.getParameter(RequestVariableNames.CUSTOM_QUERY_USERID);
                ValueArr[4] = new ValueTO(RequestVariableNames.CUSTOM_QUERY_USERID, Integer.valueOf(userId));
                String idCustomQuery = request.getParameter("idCustomQuery");
                TO<?> query = TOFactory.getInstance().loadTO(idCustomQuery);
                env.setAllPropertiesValues(query, ValueArr);
            }
            
            return 1;
        } catch (ObjectNotFoundException e) {			
            //LOG.error(e.getLocalizedMessage(), e);
            return -1;
        } catch (Exception e) {			
            LOG.error(e.getLocalizedMessage(), e);
        }
        return 0;
    }
    
    private boolean isCustomQueryNameInUse(HttpServletRequest request,
            String queryName) {

        Environment env = Env.getEnv(request);
        ValueTO[] props = {new ValueTO("name",queryName)};
        
        try {
            return env.getObjects(EnvironmentTypes.TYPE_CUSTOMQUERY, props).isEmpty() ? false : true;
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
    
    private void removeExistingCustomQuery(HttpServletRequest request, String queryName) {
        
        Environment env = Env.getEnv(request);
        ValueTO[] props = {new ValueTO("name",queryName)};
        
        Collection<TO<?>> obj = env.getObjects(EnvironmentTypes.TYPE_CUSTOMQUERY, props);
        String customQueryId = TOFactory.getInstance().saveTO(obj.iterator().next());
        TO<?> query = TOFactory.getInstance().loadTO(customQueryId);
        
        try {
            Env.getEnv(request).deleteObject(query);
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
}
