package com.synapsense.statistics;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;

public class SendStatistics extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -7542583056619206594L;

    private static final Log LOG = LogFactory.getLog(SendStatistics.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String openedWindow = req.getParameter("statisticItemName");
        LOG.debug("Opened window: " + openedWindow);
        String userName = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        UserManagementService userMan = Env.getUserDAO();

        try {
            TO<?> user = Env.getUserDAO().getUser(userName);
            Collection<TO<?>> userGroups = userMan.getUserGroups(user);

            for (TO<?> group : userGroups) {
                Map<String, Long> userStatistic = new HashMap<String, Long>();
                String statistics = (String) Env.getUserDAO().getProperty(group, UserManagementService.GROUP_STATISTICS);
                StringTokenizer st = new StringTokenizer(statistics, ",");
                while (st.hasMoreTokens()) {
                    String[] array = st.nextToken().split("=");
                    userStatistic.put(array[0].trim(), Long.valueOf(array[1]));
                }
                if (userStatistic.containsKey(openedWindow)) {
                    Long actualVal = userStatistic.get(openedWindow);
                    userStatistic.put(openedWindow, actualVal + 1);
                } else {
                    userStatistic.put(openedWindow, 1l);
                }
                String statStr = userStatistic.toString();
                userMan.updateGroupStatistics(group, statStr.substring(1, statStr.length() - 1));
            }

        } catch (UserManagementException e) {
            LOG.warn("Could not set actual values for statistic", e);
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }
        sendResponse(resp, "<result></result>");
    }
}
