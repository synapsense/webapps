package com.synapsense.statistics;

import java.io.IOException;
import java.util.Collection;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.BaseHttpServlet;

public class GetStatistics extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 4778151158297494116L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserManagementService userManagement = Env.getUserDAO();

        String destinationFileName = "userStatistic.csv";
        resp.setContentType("APPLICATION/OCTET-STREAM; charset=utf-8");
        String disHeader = "Attachment; Filename=\"" + destinationFileName + "\"";
        resp.setHeader("Content-Disposition", disHeader);
        Collection<TO<?>> groups = userManagement.getAllGroups();
        for (TO<?> group : groups) {
            String statistics = (String) Env.getUserDAO().getProperty(group, UserManagementService.GROUP_STATISTICS);
            statistics = statistics != null ? statistics.trim() : "";
            if (!statistics.isEmpty()) {
                String groupName = (String) Env.getUserDAO().getProperty(group, UserManagementService.GROUP_NAME);
                resp.getOutputStream().print(groupName);
                resp.getOutputStream().print("\n");
                StringTokenizer st = new StringTokenizer(statistics, ",");
                while (st.hasMoreTokens()) {
                    String[] array = st.nextToken().split("=");
                    resp.getOutputStream().print(array[0].trim() + ",");
                    resp.getOutputStream().print(array[1].trim());
                    resp.getOutputStream().print("\n");
                }
                resp.getOutputStream().print("\n");
            }
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
