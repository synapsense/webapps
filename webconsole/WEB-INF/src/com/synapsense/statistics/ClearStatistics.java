package com.synapsense.statistics;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.BaseHttpServlet;

public class ClearStatistics extends BaseHttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -4586573629452861080L;
    private static final Log LOG = LogFactory.getLog(ClearStatistics.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserManagementService userManagement = Env.getUserDAO();
        String clearFor = req.getParameter("clearFor");
        try {
            if (clearFor.equals("0")) {
                removeStatisticsForAll(userManagement);
            } else {
                TO<?> group = userManagement.getGroup(clearFor);
                removeStatistics(group, userManagement);
            }
            sendResponse(resp, "<result></result>");
        } catch (UserManagementException e) {
            LOG.warn("Could not clear statistics",e);
        }
    }

    private void removeStatistics(TO<?> group, UserManagementService userManagement) throws UserManagementException {
        userManagement.updateGroupStatistics(group, null);
    }

    private void removeStatisticsForAll(UserManagementService userManagement) throws UserManagementException {

        Collection<TO<?>> groups = userManagement.getAllGroups();
        for (TO<?> group : groups) {
            removeStatistics(group, userManagement);
        }
    }
}
