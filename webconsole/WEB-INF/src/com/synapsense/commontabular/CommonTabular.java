package com.synapsense.commontabular;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.config.UIConfig;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.lang.LangManager;

public class CommonTabular extends BaseHttpServlet {
    
    private static final long serialVersionUID = -4204573486270345477L;

    private static final Log LOG = LogFactory.getLog(CommonTabular.class);

    
    private static List<String> allowedTypes = new ArrayList<String>();
    static{
        allowedTypes.addAll(UIConfig.getDataViewTypes());
//		allowedTypes.add("DC");
//		allowedTypes.add("PUE");
    }
    
    private static String[] privileges = {"CREATE_EDIT_DELETE_OWN_PRIVATE_TEMPLATES","CREATE_EDIT_DELETE_OWN_PUBLIC_TEMPLATES","CREATE_EDIT_DELETE_ALL_TEMPLATES","RESTORE_DEFAULT_TEMPLATES"};
    private static Map<String, String[]> privMap = new HashMap<String, String[]>();
    static {
        privMap.put("or", privileges);
    }
        
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        
        String reqType = req.getParameter("requestType");
        String str = "<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>";
        
        try{
            if("getTypes".equals(reqType)){
                str = XMLHelper.getDocumentXml(getTypesDoc(req));
            }else if("getProperties".equals(reqType)){
                str = XMLHelper.getDocumentXml(getPropsDoc(req));
            }else if("getTemplates".equals(reqType)){
                str = XMLHelper.getDocumentXml(getTemplatesDoc(req));
            }else if("addTemplate".equals(reqType)){
                String status = addTemplate(req);
                str = "<result>" + status + "</result>";
            }else if("updateTemplate".equals(reqType)){
                String status = updateTemplate(req);
                str = "<result>" + status + "</result>";
            }else if("deleteTemplate".equals(reqType)){
                String status = deleteTemplate(req);
                str = "<result>" + status + "</result>";
            }else if("deleteTemplateType".equals(reqType)){
                deleteTabularTemplateType(req);
                str = "<result>" + ErrorMessages.SUCCESS + "</result>";
            }else if("createDefaultTemplates".equals(reqType)){
                String status = createDefaultTemplates(req);
                str = "<result>" + status + "</result>";
            }
            else if("generalValidation".equals(reqType)){
                String status = validateGeneral(req);
                str = "<result>" + status + "</result>";
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage(), e);
        }
        
        sendResponse(resp, str);
        
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private String addTemplate(HttpServletRequest request) throws JSONException, EnvException{
        String status = ErrorMessages.SUCCESS;		
        String jsonString = request.getParameter("template");
        JSONObject obj = new JSONObject(jsonString);
        String templateName = obj.getString("templateName");
        int isPrivate = Integer.parseInt(request.getParameter("isPrivate"));
        String description = request.getParameter("description");
                
        TabularUtils.createTabularTemplateTypeIfNotExist(request);
        
        if(isTemplateNameInUse(request, templateName, null)){
            return ErrorMessages.DUPLICATE_TEMPLATE_NAME;
        }
        
        String currentUserName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> curentUser = Env.getUserDAO().getUser(currentUserName);
        
        ValueTO [] ValueArr = {
                new ValueTO("name", templateName),
                new ValueTO("isPrivate", isPrivate),
                new ValueTO("jsonString", jsonString),
                new ValueTO("descr", description),
                new ValueTO("userId", curentUser.getID())
        };		
        Env.getEnv(request).createObject(EnvironmentTypes.TYPE_TABULAR_TEMPLATE, ValueArr);

        return status;
    }
    
    private String updateTemplate(HttpServletRequest request) throws JSONException, EnvException{
        String status = ErrorMessages.SUCCESS;
        
        String jsonString = request.getParameter("template");
        JSONObject obj = new JSONObject(jsonString);
        String templateName = obj.getString("templateName");
        int isPrivate = Integer.parseInt(request.getParameter("isPrivate"));
        String description = request.getParameter("description");
        String templateId = request.getParameter("templateId");
        
        TO<?> template = TOFactory.getInstance().loadTO(templateId);
        
        if(isTemplateNameInUse(request, templateName, template)){
            return ErrorMessages.DUPLICATE_TEMPLATE_NAME;
        }
        
        ValueTO [] ValueArr = {
                new ValueTO("name", templateName),
                new ValueTO("isPrivate", isPrivate),
                new ValueTO("jsonString", jsonString),
                new ValueTO("descr", description)
        };
        
        Env.getEnv(request).setAllPropertiesValues(template, ValueArr);
        
        return status;
    }
    
    private String deleteTemplate(HttpServletRequest request){
        String status = ErrorMessages.SUCCESS;
        String templateId = request.getParameter("templateId");
        TO<?> templateTO = TOFactory.getInstance().loadTO(templateId);
        
        try {
            Env.getEnv(request).deleteObject(templateTO);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            status = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        
        return status;
    }
    
    private String validateGeneral(HttpServletRequest request){
        String status = ErrorMessages.SUCCESS;
        String templateName = request.getParameter("templateName");
        String templateId = request.getParameter("templateId");

        TO<?> templateToUpdate = null;
        if(templateId != null){//edit template validation
            templateToUpdate = TOFactory.getInstance().loadTO(templateId);
        }
        
        if(isTemplateNameInUse(request, templateName, templateToUpdate)){
            status = ErrorMessages.DUPLICATE_TEMPLATE_NAME;
        }
        return status;
    }
    
    private Document getTemplatesDoc(HttpServletRequest request) throws ParserConfigurationException, JSONException{
        String templateId = request.getParameter("templateId");
        Document doc = XMLHelper.createNewXmlDocument("templates");
        Element root = doc.getDocumentElement();
        
        Environment env = Env.getLocalEnv(request);
        
        Collection<TO<?>> templates;
        if(templateId != null){
            templates = new ArrayList<TO<?>>();
            TO<?> templateTO = TOFactory.getInstance().loadTO(templateId); 
            if(env.exists(templateTO)){				
                templates.add(templateTO);
            }
        }else{
            templates = env.getObjectsByType(EnvironmentTypes.TYPE_TABULAR_TEMPLATE);
        }
        
        if(!templates.isEmpty()){
                    
            Collection<CollectionTO> ctos = env.getAllPropertiesValues(templates);
            
            List<CollectionTO> ctosList = new ArrayList<CollectionTO>();
            ctosList.addAll(ctos);
            
            
            //sort by template name
            final LangManager langMan = LangManager.getInstance();
            final String localeID = LangManager.getCurrentLocaleId(request);
            
            Collections.sort(ctosList, new Comparator<CollectionTO>(){
                @Override
                public int compare(CollectionTO cto1, CollectionTO cto2) {
                    String templateName1 = langMan.getLocaleString(localeID, (String)cto1.getPropValue("name").get(0).getValue());
                    String templateName2 = langMan.getLocaleString(localeID, (String)cto2.getPropValue("name").get(0).getValue());
                    return templateName1.compareTo(templateName2);
                }
            });
            
            for(CollectionTO cto : ctosList){
                Integer isPrivate = (Integer)cto.getPropValue("isPrivate").get(0).getValue();
                Integer userId = (Integer)cto.getPropValue("userId").get(0).getValue();
                
                if(isPrivate == 1){
                    String currentUserName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
                    TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);
                    
                    boolean hasPrivs = UserUtils.hasPrivileges(currentUser, privMap);
                    boolean hasRootPrivs = (UserUtils.hasPrivilege(currentUser, "CREATE_EDIT_DELETE_ALL_TEMPLATES") || UserUtils.hasRootPrivilege(currentUser));
                    
                    if (!hasPrivs || (hasPrivs && !currentUser.getID().equals(userId) && !hasRootPrivs)) {
                        continue;
                    }
                }
                
                String templateName = (String)cto.getPropValue("name").get(0).getValue();
                String jsonString = (String)cto.getPropValue("jsonString").get(0).getValue();
                String description = (String)cto.getPropValue("descr").get(0).getValue();
                
                Element templateElem = doc.createElement("template");
                templateElem.setAttribute("id", TOFactory.getInstance().saveTO(cto.getObjId()));
                templateElem.setAttribute("name", templateName);
                templateElem.setAttribute("descr", description);
                templateElem.setAttribute("isPrivate", isPrivate.toString());
                templateElem.setAttribute("userId", userId.toString());
                templateElem.setAttribute("jsonStr", jsonString);
                root.appendChild(templateElem);
            }
        }
        return doc;
    }
    
    private Document getTypesDoc(HttpServletRequest request) throws ParserConfigurationException, ObjectNotFoundException{
        Document doc = XMLHelper.createNewXmlDocument("types");
        Element root = doc.getDocumentElement();
        String baseTypeName = request.getParameter("baseType");
        String relation = request.getParameter("relation");
        Environment env = Env.getEnv(request);
        
        String typesArr[]  = null;
        
        if(baseTypeName != null){//types should be filtered
            if("children".equals(relation)){
                typesArr = env.getObjectTypes();
            }else{
                typesArr = TabularUtils.getRelatedParentTypes(request, baseTypeName);
            }
        }else{
            typesArr = env.getObjectTypes();
        }
        
        final LangManager langMan = LangManager.getInstance();
        final String loc = LangManager.getCurrentLocaleId(request);
        
        List<String> types = Arrays.asList(typesArr);
        Collections.sort(types, new Comparator<String>(){
                                    @Override
                                    public int compare(String typeName1, String typeName2) {
                                        String dispName1 = langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName1));
                                        String dispName2 = langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName2));
                                        return dispName1.compareTo(dispName2);
                                    }
        });
    
        for(String typeName : types){
            if(allowedTypes.contains(typeName)){
                Element typeElem = doc.createElement("type");
                typeElem.setAttribute("name", typeName);
                typeElem.setAttribute("dispName", langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName)));
                root.appendChild(typeElem);
            }
        }
        return doc;
    }
    
    private Document getPropsDoc(HttpServletRequest request) throws ParserConfigurationException{		
        Document doc = XMLHelper.createNewXmlDocument("type");
        Element root = doc.getDocumentElement();
        String typeName = request.getParameter("type");
        root.setAttribute("name", typeName);
        
        List<String> orderedProps = TypeUtil.getInstance().listVisiblePropertyNames(typeName);
        List<PropertyDescr> propDescrs = new ArrayList<PropertyDescr>();
        propDescrs.addAll(Env.getEnv(request).getObjectType(typeName).getPropertyDescriptors()); //clone set just in case
        
        //add ordered properties first
        for(String propName : orderedProps){
            PropertyDescr pDescr = null;
            for(PropertyDescr pd : propDescrs){
                if(pd.getName().equals(propName)){
                    pDescr = pd;
                    break;
                }
            }
            if(pDescr != null){
                addPropDescriptorToDoc(pDescr, typeName, doc, root);
                propDescrs.remove(pDescr);
            }
        }
        
        //add other properties
        CommonFunctions.sortPropsAlphabetically(propDescrs, typeName, request);
        for(PropertyDescr pDescr : propDescrs){
            addPropDescriptorToDoc(pDescr, typeName, doc, root);
        }
        
        return doc;
    }
    
    private void addPropDescriptorToDoc(PropertyDescr pDescr, String typeName, Document doc, Element root){
        Element propElem = doc.createElement("property");
        propElem.setAttribute("name", pDescr.getName());
        propElem.setAttribute("dispName", TypeUtil.getInstance().getDisplayablePropertyName(typeName, pDescr.getName()));
        propElem.setAttribute("className", pDescr.getType().toString());
        propElem.setAttribute("typeName", typeName);
        root.appendChild(propElem);
    }
    
    private boolean isTemplateNameInUse(HttpServletRequest req, String templateName, TO<?> templateToUpdate) {
        boolean result = false;
        Collection<TO<?>> templatesWithSameName = Env.getEnv(req).getObjects(EnvironmentTypes.TYPE_TABULAR_TEMPLATE, new ValueTO[]{new ValueTO("name", templateName)});
        
        if(templateToUpdate != null){//edit template validation
            if(!templatesWithSameName.isEmpty() && !templatesWithSameName.contains(templateToUpdate)){
                result = true;
            }
        }else{//new template validation
            if(!templatesWithSameName.isEmpty()){
                result = true;
            }
        }
        
        return result;
    }

    private void deleteTabularTemplateType(HttpServletRequest request) throws EnvException{
        Env.getEnv(request).deleteObjectType(EnvironmentTypes.TYPE_TABULAR_TEMPLATE);
    }

    private String createDefaultTemplates(HttpServletRequest request){
        String status = ErrorMessages.SUCCESS;
        try{
            TabularUtils.createDefaultTemplates(request);
        }catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            status = ErrorMessages.UNABLE_TO_COMPLETE;
        }		
        return status;
    }
}
