package com.synapsense.commontabular;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.EnvironmentTypes;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.AutoCloseInputStream;
import com.synapsense.utilities.UserPreference;

public class TabularUtils {
    
    /**
     * Creates default templates
     * @param request
     * @throws EnvException
     */
    public static void createDefaultTemplates(HttpServletRequest request) throws EnvException{
        Environment env = Env.getEnv(request);
        
        createTabularTemplateTypeIfNotExist(request);

        //At first delete old templates with same names
        String pressureName = "Pressure";
        String cracName = "CRAH/CRAC";
        String nodeName = "Nodes";
        String rackName = "Environmental Racks";
        String powerRackName = "Power Racks";
        String ionDeltaName = "Power Meters (Delta)";
        String ionWyeName = "Power Meters (Wye)";
        String firmwareName = "Node Firmware Report";
        String batteryStatusName = "Battery Status Report";
        String controlStratName = "Control Sensors (Stratification)";
        String controlIntakeName = "Control Sensors (Intake/Pressure)";
        String preferenceTemplatePrefix = "Template_";
        String tType = EnvironmentTypes.TYPE_TABULAR_TEMPLATE;
        Collection<TO<?>> templs = env.getObjects(tType, new ValueTO[]{new ValueTO("name", pressureName)});
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", cracName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", nodeName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", rackName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", powerRackName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", ionDeltaName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", ionWyeName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", firmwareName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", batteryStatusName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", controlStratName)}));
        templs.addAll(env.getObjects(tType, new ValueTO[]{new ValueTO("name", controlIntakeName)}));
        for(TO<?> template : templs){
            env.deleteObject(template);
        }
        // remove user customizations for these templates.
        UserPreference.removePreference(preferenceTemplatePrefix + pressureName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + cracName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + nodeName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + rackName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + powerRackName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + ionDeltaName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + ionWyeName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + firmwareName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + batteryStatusName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + controlStratName, Env.getUserDAO());
        UserPreference.removePreference(preferenceTemplatePrefix + controlIntakeName, Env.getUserDAO());


        // Create default templates
        createDefaultTemplate(pressureName, getResourceAsString("Pressure.json"), request);
        createDefaultTemplate(cracName, getResourceAsString("CrahCrac.json"), request);
        createDefaultTemplate(nodeName, getResourceAsString("Nodes.json"), request);
        createDefaultTemplate(rackName, getResourceAsString("EnvironmentalRacks.json"), request);
        createDefaultTemplate(powerRackName, getResourceAsString("PowerRacks.json"), request);
        createDefaultTemplate(ionDeltaName, getResourceAsString("PowerMetersDelta.json"), request);
        createDefaultTemplate(ionWyeName, getResourceAsString("PowerMetersWye.json"), request);
        createDefaultTemplate(firmwareName, getResourceAsString("NodeFirmwareReport.json"), request);
        createDefaultTemplate(batteryStatusName, getResourceAsString("BatteryStatusReport.json"), request);
        createDefaultTemplate(controlIntakeName, getResourceAsString("ControlSensorsIntakePressure.json"), request);
    }

    /** Convenience method to load a resource file as a string using the class loader of this class. */
    private static String getResourceAsString(String resourceName) {
        try {
            // NOTE: For commons-io 2.3 and above we can use the simpler IOUtils.toString(URL)
            return IOUtils.toString(new AutoCloseInputStream(TabularUtils.class.getResourceAsStream(resourceName)), StandardCharsets.UTF_8.name());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates tabular type if it is not exist
     * @param request
     * @throws EnvException
     */
    public static void createTabularTemplateTypeIfNotExist(HttpServletRequest request) throws EnvException{
        Environment env = Env.getEnv(request);
        ObjectType[] templateType = env.getObjectTypes(new String[]{EnvironmentTypes.TYPE_TABULAR_TEMPLATE});
        
        if (templateType == null || templateType.length < 1)
        {
            PropertyDescr[] props = {
                    new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr("isPrivate", Integer.class.getName()),
                    new PropertyDescr("jsonString", String.class.getName()),
                    new PropertyDescr("descr", String.class.getName()),
                    new PropertyDescr("userId", Integer.class.getName())
            };
            ObjectType newtype = env.createObjectType(EnvironmentTypes.TYPE_TABULAR_TEMPLATE);

            for(int i = 0; i < props.length; i++){
                newtype.getPropertyDescriptors().add(props[i]);
            }
            env.updateObjectType(newtype);
        }
    }
    
    /**
     * Gets types which is parents of passed type
     * @param request
     * @param baseTypeName
     * @return
     * @throws ObjectNotFoundException
     */
    public static String[] getRelatedParentTypes(HttpServletRequest request, String baseTypeName) throws ObjectNotFoundException{
        int n = 5; //objects number to inspect
        Collection<TO<?>> objects = Env.getEnv(request).getObjectsByType(baseTypeName);			
        List<TO<?>> parents = new ArrayList<TO<?>>();
        
        for(TO<?> concreteObject : objects){
            parents.addAll(getAllParents(request, concreteObject));
            n--;
            if(n <= 0){
                break;
            }
        }			
        Set<String> parentTypes = new HashSet<String>();
        for(TO<?> parent : parents){
            parentTypes.add(parent.getTypeName());
        }		
        return parentTypes.toArray(new String[0]);
    }
    
    private static List<TO<?>> getAllParents(HttpServletRequest request, TO<?> obj) throws ObjectNotFoundException{
        List<TO<?>> resultList = new ArrayList<TO<?>>();
        getRelatedParents(Env.getEnv(request), obj, resultList);
        return resultList;
    }
    
    private static void getRelatedParents(Environment env, TO<?> obj, List<TO<?>> resultList) throws ObjectNotFoundException{
        Collection<TO<?>> parents = env.getParents(obj);
        resultList.addAll(parents);
        for(TO<?> parent : parents){
            getRelatedParents(env, parent, resultList);
        }
    }
    
    private static void createDefaultTemplate(String templateName, String jsonString, HttpServletRequest request) throws EnvException{
        String description = "";
        ValueTO [] ValueArr = {
                new ValueTO("name", templateName),
                new ValueTO("isPrivate", 0), //always public
                new ValueTO("jsonString", jsonString),
                new ValueTO("descr", description),
                new ValueTO("userId", -1)
        };		
        Env.getEnv(request).createObject(EnvironmentTypes.TYPE_TABULAR_TEMPLATE, ValueArr);
    }
}
