package com.synapsense.dashboard;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.permissions.PermissionUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.es.EnvironmentTypes;

public class PermissionFilterUtils {

    private static final Log LOG = LogFactory.getLog(PermissionFilterUtils.class);
    private static final String EVERYONE_IS_ALLOWED = "__everyone";

    public static void filterByPermissions(Collection<CollectionTO> dashboards, HttpServletRequest req) {
        String currentUserName = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);

        if (!UserUtils.hasRootPrivilege(currentUser)) {
            String currUserIdStr = currentUser.getID().toString();
            Environment env = Env.getEnv(req);
            Set<TO<?>> curUserAllowedTOs = new HashSet<TO<?>>();
            curUserAllowedTOs.addAll(env.getObjectsByType(EnvironmentTypes.TYPE_DC));
            curUserAllowedTOs.addAll(env.getObjectsByType(EnvironmentTypes.TYPE_ROOM));

            Map<TO<?>, Set<String>> dcAndRoomsPermMap = getDcAndRoomsPermissionsMap(req, false);
            List<String> adminIds = getAdminIds();

            Collection<CollectionTO> dashboardsToExclude = new ArrayList<CollectionTO>();
            for (CollectionTO cto : dashboards) {
                String userIdStr = cto.getPropValue("userId").get(0).getValue().toString();
                if (!currUserIdStr.equals(userIdStr)) {
                    Set<TO<?>> allowedTOs = new HashSet<TO<?>>();
                    if (adminIds.contains(userIdStr)) {
                        // all is allowed for admin
                        allowedTOs.addAll(dcAndRoomsPermMap.keySet());
                    } else {
                        for (Entry<TO<?>, Set<String>> TOEntry : dcAndRoomsPermMap.entrySet()) {
                            if (TOEntry.getValue().contains(userIdStr)
                                    || TOEntry.getValue().contains(EVERYONE_IS_ALLOWED)) {
                                allowedTOs.add(TOEntry.getKey());
                            }
                        }
                    }

                    boolean excludeDashboard = false;
                    for (TO<?> allowedTO : allowedTOs) {
                        if (!curUserAllowedTOs.contains(allowedTO)) {
                            excludeDashboard = true;
                            break;
                        }
                    }
                    if (excludeDashboard) {
                        dashboardsToExclude.add(cto);
                    }
                }
            }
            dashboards.removeAll(dashboardsToExclude);
        }
    }

    public static void filterByGroupsPermissions(Collection<CollectionTO> dashboards, HttpServletRequest req) {
        Collection<CollectionTO> dashboardsToExclude = new ArrayList<CollectionTO>();

        Map<TO<?>, Set<String>> userDCAndRoomsPermMap = getDcAndRoomsPermissionsMap(req, false);
        ArrayList<String> groupsList = getGroupsList(req);
        List<String> adminIds = getAdminIds();

        for (CollectionTO cto : dashboards) {
            String userIdStr = cto.getPropValue("userId").get(0).getValue().toString();
            Set<TO<?>> creatorAllowedTOs = new HashSet<TO<?>>();
            if (adminIds.contains(userIdStr)) {
                // all is allowed for admin
                creatorAllowedTOs.addAll(userDCAndRoomsPermMap.keySet());
            } else {
                for (Entry<TO<?>, Set<String>> TOEntry : userDCAndRoomsPermMap.entrySet()) {
                    if (TOEntry.getValue().contains(userIdStr) || TOEntry.getValue().contains(EVERYONE_IS_ALLOWED)) {
                        creatorAllowedTOs.add(TOEntry.getKey());
                    }
                }
            }

            Set<TO<?>> groupsAllowedTOs = getGroupsAllowedTOs(req, groupsList);
            boolean excludeDashboard = false;
            for (TO<?> allowedTO : creatorAllowedTOs) {
                if (!groupsAllowedTOs.contains(allowedTO)) {
                    excludeDashboard = true;
                    break;
                }
            }

            if (excludeDashboard) {
                dashboardsToExclude.add(cto);
            }
        }
        dashboards.removeAll(dashboardsToExclude);
    }

    private static HashSet<TO<?>> getGroupsAllowedTOs(HttpServletRequest req, ArrayList<String> groupsList) {
        HashSet<TO<?>> groupsAllowedTOs = new HashSet<TO<?>>();

        Map<TO<?>, Set<String>> dcAndRoomsPermMap = getDcAndRoomsPermissionsMap(req, true);
        for (String group : groupsList) {
            if ("Administrators".equals(group)) {
                // all is allowed for admin
                groupsAllowedTOs.addAll(dcAndRoomsPermMap.keySet());
            } else {
                for (Entry<TO<?>, Set<String>> TOEntry : dcAndRoomsPermMap.entrySet()) {
                    if (TOEntry.getValue().contains(group) || TOEntry.getValue().contains(EVERYONE_IS_ALLOWED)) {
                        groupsAllowedTOs.add(TOEntry.getKey());
                    }
                }
            }
        }

        return groupsAllowedTOs;
    }

    public static ArrayList<String> getGroupsList(HttpServletRequest req) {
        String groupsStr = req.getParameter("groups");
        ArrayList<String> groupsList = new ArrayList<String>();
        if (groupsStr != null && !"".equals(groupsStr)) {
            try {
                JSONObject groups = new JSONObject(groupsStr);
                JSONArray objects = null;
                if (!groups.isNull("groups")) {
                    objects = groups.getJSONArray("groups");
                    for (int i = 0; i < objects.length(); i++) {
                        JSONObject object = objects.getJSONObject(i);
                        groupsList.add(object.getString("name"));
                    }
                }
            } catch (JSONException e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
        }
        return groupsList;
    }

    private static Map<TO<?>, Set<String>> getDcAndRoomsPermissionsMap(HttpServletRequest req, boolean byGroups) {
        Map<TO<?>, Set<String>> dcAndRoomsPermMap = new HashMap<TO<?>, Set<String>>();

        HttpSession session = req.getSession();
        String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        String userpass = (String) session.getAttribute(SessionVariableNames.SESSION_PASSWORD);
        Env.login(username, userpass);
        Environment convEnv = Env.getEnv();
        String[] props = { "permissions" };
        Collection<TO<?>> dcs = convEnv.getObjectsByType(EnvironmentTypes.TYPE_DC);
        Collection<TO<?>> rooms = convEnv.getObjectsByType(EnvironmentTypes.TYPE_ROOM);

        Collection<CollectionTO> dcCtos = Collections.emptyList();
        Collection<CollectionTO> roomCtos = Collections.emptyList();
        if (!dcs.isEmpty()) {
            dcCtos = convEnv.getPropertyValue(dcs, props);
        }
        if (!rooms.isEmpty()) {
            roomCtos = convEnv.getPropertyValue(rooms, props);
        }

        fillPermissionMap(dcAndRoomsPermMap, dcCtos, byGroups);
        fillPermissionMap(dcAndRoomsPermMap, roomCtos, byGroups);

        return dcAndRoomsPermMap;
    }

    private static void fillPermissionMap(Map<TO<?>, Set<String>> permMapToFill, Collection<CollectionTO> ctos,
            boolean byGroups) {
        for (CollectionTO cto : ctos) {
            ValueTO valTO = cto.getSinglePropValue("permissions");
            LOG.debug("fillPermissionMap: valTO = " +  (valTO == null ? "NULL" : valTO.getPropertyName() + "=" + valTO.getValue()));
            if (valTO != null) {
                String permissions = (String) valTO.getValue();
                Set<String> permObjsIds = new HashSet<String>();
    
                if (PermissionUtils.isEveryonePermitted(permissions)) {
                    permObjsIds.add(EVERYONE_IS_ALLOWED);
                } else {
                    if (byGroups) {
                        permObjsIds.addAll(fillPermittedGroups(permissions));
                    } else {
                        permObjsIds.addAll(fillPermittedUsers(permissions));
                    }
                }
                permMapToFill.put(cto.getObjId(), permObjsIds);
            }
        }
    }

    private static Collection<String> fillPermittedUsers(String permissions) {
        Set<String> permUserIds = new HashSet<String>();
        UserManagementService ums = Env.getUserDAO();

        Collection<String> permUsers = PermissionUtils.getPermittedUsers(permissions);
        Collection<String> permGroups = PermissionUtils.getPermittedGroups(permissions);

        UserManagementService userDao = Env.getUserDAO();
        for (String userName : permUsers) {
            TO<?> user = userDao.getUser(userName);
            if (user != null) {
                permUserIds.add(user.getID().toString());
            } else {
                LOG.warn("Cannot find user by name : " + userName
                        + ". It does not exists but still used in permissions");
            }
        }

        for (String groupId : permGroups) {
            try {
                TO<?> group = userDao.getGroup(groupId);
                Collection<TO<?>> groupUsrs = ums.getGroupUsers(group);
                for (TO<?> u : groupUsrs) {
                    permUserIds.add(u.getID().toString());
                }
            } catch (UserManagementException e) {
                LOG.warn("Cannot find user group by name : " + groupId
                        + ". It does not exists but still used in permissions", e);
            }
        }
        return permUserIds;
    }

    private static Collection<String> fillPermittedGroups(String permissions) {
        Set<String> permGroupIds = new HashSet<String>();
        UserManagementService ums = Env.getUserDAO();

        Collection<String> permUsers = PermissionUtils.getPermittedUsers(permissions);
        Collection<String> permGroups = PermissionUtils.getPermittedGroups(permissions);

        for (String groupName : permGroups) {
            permGroupIds.add(groupName);
        }

        for (String userId : permUsers) {
            try {
                TO<?> user = ums.getUser(userId);
                Collection<String> groupUsrs = ums.getUserGroupIds(user);
                for (String gr : groupUsrs) {
                    permGroupIds.add(gr);
                }
            } catch (UserManagementException e) {
                LOG.warn("Cannot find user by name : " + userId + ". It does not exists but still used in permissions", e);
            }
        }
        return permGroupIds;
    }

    private static List<String> getAdminIds() {
        List<String> adminIds = new ArrayList<String>();
        Collection<TO<?>> allUsers = Env.getUserDAO().getAllUsers();
        for (TO<?> user : allUsers) {
            if (UserUtils.hasRootPrivilege(user)) {
                adminIds.add(user.getID().toString());
            }
        }
        return adminIds;
    }
}
