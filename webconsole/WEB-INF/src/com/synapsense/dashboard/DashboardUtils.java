package com.synapsense.dashboard;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.EnvironmentTypes;

public class DashboardUtils {

    public static void createDashboardTypeIfNotExist(HttpServletRequest request) throws EnvException {
        Environment env = Env.getEnv(request);
        ObjectType[] templateType = env.getObjectTypes(new String[] { EnvironmentTypes.TYPE_DASHBOARD });

        if (templateType == null || templateType.length < 1) {
            PropertyDescr[] props = { new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr("isPrivate", Integer.class.getName()),
                    new PropertyDescr("jsonString", String.class.getName()),
                    new PropertyDescr("descr", String.class.getName()),
                    new PropertyDescr("applicationType", String.class.getName()),
                    new PropertyDescr("userId", Integer.class.getName()) };
            ObjectType newtype = env.createObjectType(EnvironmentTypes.TYPE_DASHBOARD);

            for (int i = 0; i < props.length; i++) {
                newtype.getPropertyDescriptors().add(props[i]);
            }
            env.updateObjectType(newtype);
        }
    }
    
    public static void createDashboardTemplateTypeIfNotExist(HttpServletRequest request) throws EnvException {
        Environment env = Env.getEnv(request);
        ObjectType[] templateType = env.getObjectTypes(new String[] {"DASHBOARD_TEMPLATE"});

        if (templateType == null || templateType.length < 1) {
            PropertyDescr[] props = { new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr("applicationType", String.class.getName()),
                    new PropertyDescr("isPrivate", Integer.class.getName()),
                    new PropertyDescr("dashJsonString", String.class.getName()),
                    new PropertyDescr("objIDsToReplace", String.class.getName()),
                    new PropertyDescr("descr", String.class.getName()),
                    new PropertyDescr("userId", Integer.class.getName()) };
            ObjectType newtype = env.createObjectType("DASHBOARD_TEMPLATE");

            for (int i = 0; i < props.length; i++) {
                newtype.getPropertyDescriptors().add(props[i]);
            }
            env.updateObjectType(newtype);
        }
    }
    
    public static void createDashboardCarouselTypeIfNotExist(HttpServletRequest request) throws EnvException {
        Environment env = Env.getEnv(request);
        ObjectType[] templateType = env.getObjectTypes(new String[] {EnvironmentTypes.TYPE_DASHBOARD_CAROUSEL});

        if (templateType == null || templateType.length < 1) {
            PropertyDescr[] props = { new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr("applicationType", String.class.getName()),
                    new PropertyDescr("isPrivate", Integer.class.getName()),
                    new PropertyDescr("jsonString", String.class.getName()),
                    new PropertyDescr("descr", String.class.getName()),
                    new PropertyDescr("userId", Integer.class.getName()) };
            ObjectType newtype = env.createObjectType(EnvironmentTypes.TYPE_DASHBOARD_CAROUSEL);

            for (int i = 0; i < props.length; i++) {
                newtype.getPropertyDescriptors().add(props[i]);
            }
            env.updateObjectType(newtype);
        }
    }
    
    public static void createDashboardTemplatesIfNotExists(HttpServletRequest request) throws EnvException {
        Environment env = Env.getEnv(request);
        Collection<TO<?>> dashTemplates = env.getObjectsByType("DASHBOARD_TEMPLATE");

        if (dashTemplates.size() < 1) {
            // Create default dashboard templates
            String applicationType = "WebConsole";
            // DC Summary
            String templateName = "DC Summary";
            String objIDsToReplace = "DC:350";
            String dashJsonString = "{\"dispContArray\":[{\"dispObjJsonObj\":{\"staticParentId\":\"DC:350\",\"title\":\"LiveImaging -Temp-Top\",\"layerType\":\"200_131072\"},\"height\":654,\"width\":410,\"x\":0,\"y\":0,\"dispObjClassName\":\"liveimagingdo\"},{\"dispObjJsonObj\":{\"staticParentId\":\"DC:350\",\"title\":\"Active Alerts\"},\"height\":327,\"width\":411,\"x\":410,\"y\":0,\"dispObjClassName\":\"alertgridviewdo\"},{\"dispObjJsonObj\":{\"title\":\"Histogram of RACK Top Intake temperatures\",\"propName\":\"cTop\",\"objType\":\"RACK\",\"chartColor\":\"55\",\"staticParentId\":\"DC:350\",\"increment\":\"5\"},\"height\":327,\"width\":411,\"x\":410,\"y\":327,\"dispObjClassName\":\"temphistogramdo\"}],\"fieldHeight\":654,\"fieldWidth\":821}";
            createDefaultDashboardTemplate(templateName, applicationType, objIDsToReplace, dashJsonString, request);
            
            // DC & PUE Summary
            templateName = "DC & PUE Summary";
            objIDsToReplace = "DC:350,PUE:424";
            dashJsonString = "{\"dispContArray\":[{\"dispObjJsonObj\":{\"staticParentId\":\"DC:350\",\"title\":\"LiveImaging -Temp-Top\",\"layerType\":\"200_131072\"},\"height\":327,\"width\":410,\"x\":0,\"y\":0,\"dispObjClassName\":\"liveimagingdo\"},{\"dispObjJsonObj\":{\"staticParentId\":\"DC:350\",\"title\":\"Active Alerts\"},\"height\":327,\"width\":411,\"x\":410,\"y\":0,\"dispObjClassName\":\"alertgridviewdo\"},{\"dispObjJsonObj\":{\"title\":\"Histogram of RACK Top Intake temperatures\",\"propName\":\"cTop\",\"objType\":\"RACK\",\"chartColor\":\"55\",\"staticParentId\":\"DC:350\",\"increment\":\"5\"},\"height\":327,\"width\":411,\"x\":410,\"y\":327,\"dispObjClassName\":\"temphistogramdo\"},{\"dispObjJsonObj\":{\"cosmeticData\":{\"showLabels\":true,\"chartTemplate\":{\"showLegend\":false,\"title\":\"\",\"lineTemplates\":[{\"propDispName\":\"object_property_pue_itpower\",\"color\":13837609,\"propertyId\":\"PUE:424->itPower\",\"propName\":\"itPower\",\"weight\":1},{\"propDispName\":\"object_property_pue_coolingpower\",\"color\":2045544,\"propertyId\":\"PUE:424->coolingPower\",\"propName\":\"coolingPower\",\"weight\":1},{\"propDispName\":\"object_property_pue_powerloss\",\"color\":15767810,\"propertyId\":\"PUE:424->powerLoss\",\"propName\":\"powerLoss\",\"weight\":1},{\"propDispName\":\"object_property_pue_lightingpower\",\"color\":31038,\"propertyId\":\"PUE:424->lightingPower\",\"propName\":\"lightingPower\",\"weight\":1},{\"propDispName\":\"object_property_pue_infrastructurepower\",\"color\":9803263,\"propertyId\":\"PUE:424->infrastructurePower\",\"propName\":\"infrastructurePower\",\"weight\":1}],\"objTypeName\":\"PUE\",\"legendPosition\":\"bottom\",\"labelType\":0,\"bgcolor\":16777215},\"showValues\":true},\"title\":\"Power Usage History\",\"shownInterval\":7200000},\"height\":327,\"width\":205,\"x\":0,\"y\":327,\"dispObjClassName\":\"puehistoricalonflydo\"},{\"dispObjJsonObj\":{\"cosmeticData\":{\"addUnits\":true,\"gaugeAngle\":180},\"staticParentId\":\"PUE:424\",\"title\":\"PUE\",\"chartType\":\"AngularGauge\",\"propertyTemplate\":{\"propClass\":\"\",\"propertyName\":\"pue\",\"dispPropertyName\":\"\",\"propertyObjectType\":\"PUE\"}},\"height\":163,\"width\":205,\"x\":205,\"y\":327,\"dispObjClassName\":\"fusionwidgetgaugedo\"},{\"dispObjJsonObj\":{\"cosmeticData\":{\"addUnits\":true,\"gaugeAngle\":180},\"staticParentId\":\"PUE:424\",\"title\":\"DCiE\",\"chartType\":\"AngularGauge\",\"propertyTemplate\":{\"propClass\":\"\",\"propertyName\":\"dcie\",\"dispPropertyName\":\"\",\"propertyObjectType\":\"PUE\"}},\"height\":164,\"width\":205,\"x\":205,\"y\":490,\"dispObjClassName\":\"fusionwidgetgaugedo\"}],\"fieldHeight\":654,\"fieldWidth\":821}";
            createDefaultDashboardTemplate(templateName, applicationType, objIDsToReplace, dashJsonString, request);
            
            // SEMS
            applicationType = "SEMS";
            templateName = "SEMS - DC Summary";
            objIDsToReplace = "DC:130";
            dashJsonString = "{\"fieldHeight\":852,\"dispContArray\":[{\"height\":220,\"y\":0,\"dispObjClassName\":\"fusionwidgetgaugedo\",\"dispObjJsonObj\":{\"propertyTemplate\":{\"propClass\":\"\",\"propertyName\":\"avgCS\",\"propertyObjectType\":\"DC\",\"dispPropertyName\":\"\"},\"chartType\":\"AngularGauge\",\"title\":\"Average CRAH/CRAC Supply Temperature\",\"cosmeticData\":{\"addUnits\":true,\"gaugeAngle\":180},\"staticParentId\":\"DC:130\"},\"x\":1233,\"width\":411},{\"height\":440,\"y\":0,\"dispObjClassName\":\"autorefreshchartdo\",\"dispObjJsonObj\":{\"shownInterval\":7200000,\"title\":\"Rack Top Intake Temperatures\",\"cosmeticData\":{\"chartTemplate\":{\"legendPosition\":\"bottom\",\"labelType\":2,\"title\":\"\",\"bgcolor\":16777215,\"lineTemplates\":[{\"propDispName\":\"object_property_dc_avgrct\",\"propertyId\":\"DC:130->avgRCT\",\"color\":32768,\"propName\":\"avgRCT\",\"weight\":1},{\"propDispName\":\"object_property_dc_maxrct\",\"propertyId\":\"DC:130->maxRCT\",\"color\":16711680,\"propName\":\"maxRCT\",\"weight\":1},{\"propDispName\":\"object_property_dc_minrct\",\"propertyId\":\"DC:130->minRCT\",\"color\":255,\"propName\":\"minRCT\",\"weight\":1}],\"showLegend\":true,\"objTypeName\":\"DC\"},\"showValues\":true,\"showLabels\":true}},\"x\":0,\"width\":1233},{\"height\":220,\"y\":220,\"dispObjClassName\":\"fusionwidgetgaugedo\",\"dispObjJsonObj\":{\"propertyTemplate\":{\"propClass\":\"\",\"propertyName\":\"avgCR\",\"propertyObjectType\":\"DC\",\"dispPropertyName\":\"\"},\"chartType\":\"AngularGauge\",\"title\":\"Average CRAH/CRAC Return Temperature\",\"cosmeticData\":{\"addUnits\":true,\"gaugeAngle\":180},\"staticParentId\":\"DC:130\"},\"x\":1233,\"width\":411},{\"height\":206,\"y\":646,\"dispObjClassName\":\"autorefreshchartdo\",\"dispObjJsonObj\":{\"shownInterval\":7200000,\"title\":\"CRAH/CRAC Average DeltaT\",\"cosmeticData\":{\"chartTemplate\":{\"legendPosition\":\"bottom\",\"labelType\":0,\"title\":\"\",\"bgcolor\":16777215,\"lineTemplates\":[{\"propDispName\":\"object_property_dc_avgcdt\",\"propertyId\":\"DC:130->avgCDT\",\"color\":0,\"propName\":\"avgCDT\",\"weight\":1}],\"showLegend\":true,\"objTypeName\":\"DC\"},\"showValues\":true,\"showLabels\":true}},\"x\":961,\"width\":683},{\"height\":412,\"y\":440,\"dispObjClassName\":\"editablechartdo\",\"dispObjJsonObj\":{\"chartType\":\"Column3D\",\"title\":\"Top Intake Rate of Change\",\"cosmeticData\":{\"chartTemplate\":{\"legendPosition\":\"bottom\",\"labelType\":2,\"title\":\"\",\"bgcolor\":16777215,\"lineTemplates\":[{\"propDispName\":\"object_property_dc_minctrofchange\",\"propertyId\":\"DC:130->minCTROfChange\",\"color\":255,\"propName\":\"minCTROfChange\",\"weight\":1},{\"propDispName\":\"object_property_dc_avgctrofchange\",\"propertyId\":\"DC:130->avgCTROfChange\",\"color\":3381606,\"propName\":\"avgCTROfChange\",\"weight\":1},{\"propDispName\":\"object_property_dc_maxctrofchange\",\"propertyId\":\"DC:130->maxCTROfChange\",\"color\":16711680,\"propName\":\"maxCTROfChange\",\"weight\":1}],\"showLegend\":true,\"objTypeName\":\"DC\"},\"showValues\":true,\"showLabels\":true}},\"x\":0,\"width\":480},{\"height\":412,\"y\":440,\"dispObjClassName\":\"editablechartdo\",\"dispObjJsonObj\":{\"chartType\":\"Column3D\",\"title\":\"Top Intake Dew Point\",\"cosmeticData\":{\"chartTemplate\":{\"legendPosition\":\"bottom\",\"labelType\":2,\"title\":\"\",\"bgcolor\":16777215,\"lineTemplates\":[{\"propDispName\":\"object_property_dc_minrdp\",\"propertyId\":\"DC:130->minRDP\",\"color\":52479,\"propName\":\"minRDP\",\"weight\":1},{\"propDispName\":\"object_property_dc_avgrdp\",\"propertyId\":\"DC:130->avgRDP\",\"color\":3381606,\"propName\":\"avgRDP\",\"weight\":1},{\"propDispName\":\"object_property_dc_maxrdp\",\"propertyId\":\"DC:130->maxRDP\",\"color\":8388608,\"propName\":\"maxRDP\",\"weight\":1}],\"showLegend\":true,\"objTypeName\":\"DC\"},\"showValues\":true,\"showLabels\":true}},\"x\":480,\"width\":481},{\"height\":206,\"y\":440,\"dispObjClassName\":\"autorefreshchartdo\",\"dispObjJsonObj\":{\"shownInterval\":7200000,\"title\":\"CRAH/CRAC Supply Temperature\",\"cosmeticData\":{\"chartTemplate\":{\"legendPosition\":\"right\",\"labelType\":2,\"title\":\"\",\"bgcolor\":16777215,\"lineTemplates\":[{\"propDispName\":\"object_property_dc_mincs\",\"propertyId\":\"DC:130->minCS\",\"color\":52479,\"propName\":\"minCS\",\"weight\":1},{\"propDispName\":\"object_property_dc_avgcs\",\"propertyId\":\"DC:130->avgCS\",\"color\":3381606,\"propName\":\"avgCS\",\"weight\":1},{\"propDispName\":\"object_property_dc_maxcs\",\"propertyId\":\"DC:130->maxCS\",\"color\":8388608,\"propName\":\"maxCS\",\"weight\":1}],\"showLegend\":true,\"objTypeName\":\"DC\"},\"showValues\":true,\"showLabels\":true}},\"x\":961,\"width\":683}],\"fieldWidth\":1644}";
            createDefaultDashboardTemplate(templateName, applicationType, objIDsToReplace, dashJsonString, request);
        }
    }
    
    private static void createDefaultDashboardTemplate(String templateName, String appType, String objIDsToReplace, String dashJsonString, HttpServletRequest request) throws EnvException {
        String description = "";
        ValueTO [] ValueArr = {
                new ValueTO("name", templateName),
                new ValueTO("applicationType", appType),
                new ValueTO("isPrivate", 0), //always public
                new ValueTO("dashJsonString", dashJsonString),
                new ValueTO("objIDsToReplace", objIDsToReplace),
                new ValueTO("descr", description),
                new ValueTO("userId", -1)
        };		
        Env.getEnv(request).createObject("DASHBOARD_TEMPLATE", ValueArr);
    }
    
}
