package com.synapsense.dashboard;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.config.UIConfig;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.InvalidValueException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.lang.LangManager;

public class DashboardHelper extends BaseHttpServlet{
    /**
     * 
     */
    private static final long serialVersionUID = -9039244391023991772L;	
    private static final Log LOG = LogFactory.getLog(DashboardHelper.class);
    
    private static List<String> allowedTypes = new ArrayList<String>();
    static{
        allowedTypes.addAll(UIConfig.getDataViewTypes());
        allowedTypes.add("DC");
        allowedTypes.add("PUE");
        allowedTypes.add("INSPECTOR");
        
        allowedTypes.remove("IPMISERVER");
        allowedTypes.remove("IPMINETWORK");
    }
    
    private static String[] privileges = {"CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS","CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS","CREATE_EDIT_DELETE_ALL_DASHBOARDS"};
    private static Map<String, String[]> privMap = new HashMap<String, String[]>();
    static {
        privMap.put("or", privileges);
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
        throws ServletException, IOException {
        
        LOG.info(this.getServletName() + " is started ...");
        
        String reqType = req.getParameter("requestType");
        String str = "<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>";
        
        try{
            if("addDashboard".equals(reqType)){
                String status = addDashboard(req);
                str = "<result>" + status + "</result>";
            }else if("updateDashboard".equals(reqType)){
                String status = updateDashboard(req);
                str = "<result>" + status + "</result>";
            }else if("getDashboards".equals(reqType)){
                str = XMLHelper.getDocumentXml(getDashboardsDoc(req));
            }else if("getDashboard".equals(reqType)){
                str = XMLHelper.getDocumentXml(getDashboardDoc(req));
            }else if("deleteDashboard".equals(reqType)){
                String status = deleteDashboard(req);
                str = "<result>" + status + "</result>";
            }else if("getObjsByType".equals(reqType)){
                str = XMLHelper.getDocumentXml(getObjsByTypeDoc(req));
            }else if("getTypes".equals(reqType)){
                str = XMLHelper.getDocumentXml(getTypesDoc(req));
            }else if("getProperties".equals(reqType)){
                str = XMLHelper.getDocumentXml(getPropsDoc(req));
            }else if("getAvailablePUE".equals(reqType)){
                str = XMLHelper.getDocumentXml(getAvailablePUEDoc(req));
            }else if("deleteDashboardType".equals(reqType)){
                str = "<result>" + ErrorMessages.SUCCESS + "</result>";
                Env.getEnv(req).deleteObjectType(EnvironmentTypes.TYPE_DASHBOARD);
            } else if ("generalCarouselValidation".equals(reqType)) {
                String status = validateGeneral(req);
                str = "<result>" + status + "</result>";
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage(), e);
        }
        
        sendResponse(resp, str);
        
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    protected Document getDashboardsDoc(HttpServletRequest req) throws ParserConfigurationException{
        Document doc = XMLHelper.createNewXmlDocument("dashboards");
        Element root = doc.getDocumentElement();
        
        Environment env = Env.getLocalEnv(req);
        Collection<TO<?>> dashboards = env.getObjectsByType(EnvironmentTypes.TYPE_DASHBOARD);
        if(!dashboards.isEmpty()){
            Collection<CollectionTO> ctos = env.getAllPropertiesValues(dashboards);
            
            String filter = req.getParameter("filter");
            if(filter != null && "byGroups".equals(filter)) {
                PermissionFilterUtils.filterByGroupsPermissions(ctos, req);
            }else {
                PermissionFilterUtils.filterByPermissions(ctos, req);
            }
            
            List<CollectionTO> ctosList = new ArrayList<CollectionTO>();
            ctosList.addAll(ctos);
            
            Collections.sort(ctosList, new Comparator<CollectionTO>(){
                @Override
                public int compare(CollectionTO cto1, CollectionTO cto2) {
                    String dashName1 = (String)cto1.getPropValue("name").get(0).getValue();
                    String dashName2 = (String)cto2.getPropValue("name").get(0).getValue();
                    return dashName1.compareTo(dashName2);
                }
            });
            
            for(CollectionTO cto : ctosList){
                addDashboardToDoc(cto, doc, root, req);
            }
        }
        
        return doc;
    }
    
    protected Document getDashboardDoc(HttpServletRequest req) throws ParserConfigurationException{
        Document doc = XMLHelper.createNewXmlDocument("dashboards");
        Element root = doc.getDocumentElement();
        Environment env = Env.getEnv(req);
        
        String dashboardId = req.getParameter("dashboardId");

        TO<?> dasboard = TOFactory.getInstance().loadTO(dashboardId);
        
        if(!env.exists(dasboard)){
            return doc;
        }
        
        Collection<TO<?>> wrapper = new ArrayList<TO<?>>(1);
        wrapper.add(dasboard);
        Collection<CollectionTO> ctos = env.getAllPropertiesValues(wrapper);
        
        PermissionFilterUtils.filterByPermissions(ctos, req);
        
        if(!ctos.isEmpty()){
            addDashboardToDoc(ctos.iterator().next(), doc, root, req);
        }
        
        return doc;
    }
    
    protected void addDashboardToDoc(CollectionTO cto, Document doc, Element root, HttpServletRequest request){
        String applicationType = (String)cto.getSinglePropValue("applicationType").getValue();
        
        if (Env.CONSOLE_APP_NAME.equals(Env.appName) && !Env.CONSOLE_APP_NAME.equals(applicationType)) {
            return;
        }
        
        Integer isPrivate = (Integer)cto.getPropValue("isPrivate").get(0).getValue();
        Integer userId = (Integer)cto.getPropValue("userId").get(0).getValue();
        
        if(isPrivate == 1){
            String currentUserName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);
            
            boolean hasPrivs = UserUtils.hasPrivileges(currentUser, privMap);
            boolean hasRootPrivs = (UserUtils.hasPrivilege(currentUser, "CREATE_EDIT_DELETE_ALL_DASHBOARDS") || UserUtils.hasRootPrivilege(currentUser));
            
            if (!hasPrivs || (hasPrivs && !currentUser.getID().equals(userId) && !hasRootPrivs)) {
                return;
            }
        }
        
        Element dashboardElem = doc.createElement("dashboard");				
        dashboardElem.setAttribute("id", TOFactory.getInstance().saveTO(cto.getObjId()));
        dashboardElem.setAttribute("name", (String)cto.getSinglePropValue("name").getValue());
        dashboardElem.setAttribute("jsonString", (String)cto.getSinglePropValue("jsonString").getValue());
        dashboardElem.setAttribute("isPrivate", cto.getSinglePropValue("isPrivate").getValue().toString());
        dashboardElem.setAttribute("descr", (String)cto.getSinglePropValue("descr").getValue());
        dashboardElem.setAttribute("userId", cto.getSinglePropValue("userId").getValue().toString());
        root.appendChild(dashboardElem);
    }
    
    protected String addDashboard(HttpServletRequest request) throws EnvException{
        String status = ErrorMessages.SUCCESS;		
        String jsonString = request.getParameter("jsonString");
        String dashboardName = request.getParameter("dashboardName");
        int isPrivate = Integer.parseInt(request.getParameter("isPrivate"));
        String description = request.getParameter("description");
                
        DashboardUtils.createDashboardTypeIfNotExist(request);
        
        if(isDashboardNameInUse(request, dashboardName, null)){
            return ErrorMessages.DUPLICATE_DASHBOARD_NAME;
        }
        
        String currentUserName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);
        
        ValueTO [] ValueArr = {
                new ValueTO("name", dashboardName),
                new ValueTO("applicationType", Env.appName),
                new ValueTO("jsonString", jsonString),
                new ValueTO("isPrivate", isPrivate),
                new ValueTO("descr", description),
                new ValueTO("userId", currentUser.getID())
        };		
        TO<?> dashboard = Env.getEnv(request).createObject(EnvironmentTypes.TYPE_DASHBOARD, ValueArr);
        status = ErrorMessages.SUCESS_WITH_RESULT + TOFactory.getInstance().saveTO(dashboard);

        return status;
    }
    
    protected String updateDashboard(HttpServletRequest request) throws EnvException{
        String status = ErrorMessages.SUCCESS;		
        String jsonString = request.getParameter("jsonString");
        String dashboardName = request.getParameter("dashboardName");
        String isPrivateStr = request.getParameter("isPrivate");
        String description = request.getParameter("description");
        String dashboardId = request.getParameter("dashboardId");

        TO<?> dasboard = TOFactory.getInstance().loadTO(dashboardId);
        
        List<ValueTO> vals = new ArrayList<ValueTO>(5);
        
        try {
            if(dashboardName != null){
                if(isDashboardNameInUse(request, dashboardName, dasboard)){
                    return ErrorMessages.DUPLICATE_DASHBOARD_NAME;
                }			
                vals.add(new ValueTO("name", dashboardName));
            }
            if(jsonString != null){
                vals.add(new ValueTO("jsonString", jsonString));
            }
            if(isPrivateStr != null){
                vals.add(new ValueTO("isPrivate", Integer.parseInt(isPrivateStr)));
            }
            if(description != null){
                vals.add(new ValueTO("descr", description));
            }
            ValueTO [] ValueArr = vals.toArray(new ValueTO[0]);		
    
            Env.getEnv(request).setAllPropertiesValues(dasboard, ValueArr);
        } catch (InvalidValueException e) {
            return ErrorMessages.UNABLE_TO_SAVE_DASHBOARD;
        }

        return status;
    }
    
    protected String validateGeneral(HttpServletRequest request){
        String status = ErrorMessages.SUCCESS;		
        String carName = request.getParameter("carouselName");
        String carId = request.getParameter("carouselId");

        TO<?> carToUpdate = null;
        if(carId != null){ //edit carousel validation
            carToUpdate = TOFactory.getInstance().loadTO(carId);
        }		
        if(isCarouselNameInUse(request, carName, carToUpdate)){
            status = ErrorMessages.DUPLICATE_CAROUSEL_NAME;
        }
        
        return status;
    }
    
    private boolean isDashboardNameInUse(HttpServletRequest req, String dashboardName, TO<?> templateToUpdate) {
        boolean result = false;
        Collection<TO<?>> dashboardsWithSameName = Env.getEnv(req).getObjects(EnvironmentTypes.TYPE_DASHBOARD, new ValueTO[]{new ValueTO("name", dashboardName)});
        
        if(templateToUpdate != null){//edit template validation
            if(!dashboardsWithSameName.isEmpty() && !dashboardsWithSameName.contains(templateToUpdate)){
                result = true;
            }
        }else{//new template validation
            if(!dashboardsWithSameName.isEmpty()){
                result = true;
            }
        }
        
        return result;
    }
    
    private boolean isCarouselNameInUse(HttpServletRequest req, String carouselName, TO<?> carouselToUpdate) {
        boolean result = false;
        Collection<TO<?>> carosuelsWithSameName = Env.getEnv(req).getObjects("DASHBOARD_CAROUSEL", new ValueTO[]{new ValueTO("name", carouselName)});
        
        if(carouselToUpdate != null){//edit carousel validation
            if(!carosuelsWithSameName.isEmpty() && !carosuelsWithSameName.contains(carouselToUpdate)){
                result = true;
            }
        }else{//new carousel validation
            if(!carosuelsWithSameName.isEmpty()){
                result = true;
            }
        }
        
        return result;
    }
    
    protected String deleteDashboard(HttpServletRequest request) throws EnvException{
        String status = ErrorMessages.SUCCESS;		
        String id = request.getParameter("id");
                
        Env.getEnv(request).deleteObject(TOFactory.getInstance().loadTO(id));
        
        UserPreference.removePreferenceWithValue("lastShownDashboardId", id, Env.getUserDAO());
        UserPreference.removePreferenceWithValue("atLoginDashboardId", id, Env.getUserDAO());

        return status;
    }
    
    protected Document getObjsByTypeDoc(HttpServletRequest req) throws ParserConfigurationException{
        Document doc = XMLHelper.createNewXmlDocument("objects");
        Element root = doc.getDocumentElement();
        String objTypeName = req.getParameter("objTypeName");
        
        Environment env = Env.getEnv(req);
        Collection<TO<?>> objects = env.getObjectsByType(objTypeName);
        if(!objects.isEmpty()){
            Collection<CollectionTO> ctos = env.getPropertyValue(objects, new String[]{"name"});
            List<CollectionTO> ctosList = null;
            if(ctos instanceof List){
                ctosList = (List<CollectionTO>)ctos;
            }else{
                ctosList = new ArrayList<CollectionTO>(ctos);
            }
            
            Collections.sort(ctosList, new Comparator<CollectionTO>(){
                @Override
                public int compare(CollectionTO cto1, CollectionTO cto2) {
                    String dispName1 = (String)cto1.getSinglePropValue("name").getValue();
                    String dispName2 = (String)cto2.getSinglePropValue("name").getValue();
                    return dispName1.compareToIgnoreCase(dispName2);
                }
                });
            
            for(CollectionTO cto : ctos){
                Element objectElem = doc.createElement("object");
                objectElem.setAttribute("id", TOFactory.getInstance().saveTO(cto.getObjId()));
                objectElem.setAttribute("name", (String)cto.getSinglePropValue("name").getValue());
                root.appendChild(objectElem);
            }
        }
        return doc;
    }
    
    private Document getTypesDoc(HttpServletRequest request) throws ParserConfigurationException, ObjectNotFoundException{
        Document doc = XMLHelper.createNewXmlDocument("types");
        String typesListStr = request.getParameter("typesList");
        Element root = doc.getDocumentElement();
        Environment env = Env.getEnv(request);
        
        String typesArr[]  = null;
        if(typesListStr != null){
            typesArr = typesListStr.split(",");
        }else{
            typesArr = env.getObjectTypes();
        }
        
        final LangManager langMan = LangManager.getInstance();
        final String loc = LangManager.getCurrentLocaleId(request);
        
        List<String> types = Arrays.asList(typesArr);
        Collections.sort(types, new Comparator<String>(){
                                    @Override
                                    public int compare(String typeName1, String typeName2) {
                                        String dispName1 = langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName1));
                                        String dispName2 = langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName2));
                                        return dispName1.compareTo(dispName2);
                                    }
        });
    
        for(String typeName : types){
            if(allowedTypes.contains(typeName)){
                Element typeElem = doc.createElement("type");
                typeElem.setAttribute("name", typeName);
                typeElem.setAttribute("dispName", langMan.getLocaleString(loc, TypeUtil.getInstance().getDisplayableTypeName(typeName)));
                root.appendChild(typeElem);
            }
        }
        return doc;
    }
    
    private Document getPropsDoc(HttpServletRequest request) throws ParserConfigurationException{		
        Document doc = XMLHelper.createNewXmlDocument("type");
        Element root = doc.getDocumentElement();
        String typeName = request.getParameter("type");
        root.setAttribute("name", typeName);
        
        List<String> orderedProps = TypeUtil.getInstance().listVisiblePropertyNames(typeName);
        List<PropertyDescr> propDescrs = new ArrayList<PropertyDescr>();
        propDescrs.addAll(Env.getEnv(request).getObjectType(typeName).getPropertyDescriptors()); //clone set just in case
        
        //add ordered properties first
        for(String propName : orderedProps){
            PropertyDescr pDescr = null;
            for(PropertyDescr pd : propDescrs){
                if(pd.getName().equals(propName)){
                    pDescr = pd;
                    break;
                }
            }
            if(pDescr != null){
                Class<?> clazz = pDescr.getType();
                if(clazz.isAssignableFrom(Double.class) || clazz.isAssignableFrom(TO.class)){
                    addPropDescriptorToDoc(pDescr, typeName, doc, root);
                }
                propDescrs.remove(pDescr);
            }
        }
        
        //add other properties
        CommonFunctions.sortPropsAlphabetically(propDescrs, typeName, request);		
        for(PropertyDescr pDescr : propDescrs){
            Class<?> clazz = pDescr.getType();
            if(clazz.isAssignableFrom(Double.class) || clazz.isAssignableFrom(TO.class)){
                addPropDescriptorToDoc(pDescr, typeName, doc, root);
            }
        }
        
        return doc;
    }
    
    private void addPropDescriptorToDoc(PropertyDescr pDescr, String typeName, Document doc, Element root){
        Element propElem = doc.createElement("property");
        propElem.setAttribute("name", pDescr.getName());
        propElem.setAttribute("dispName", TypeUtil.getInstance().getDisplayablePropertyName(typeName, pDescr.getName()));
        root.appendChild(propElem);
    }
    
    protected Document getAvailablePUEDoc(HttpServletRequest req) throws ParserConfigurationException, ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        Document doc = XMLHelper.createNewXmlDocument("objects");
        Element root = doc.getDocumentElement();
        
        Environment env = Env.getEnv(req);
        Collection<TO<?>> objects = env.getObjectsByType(EnvironmentTypes.TYPE_PUE);
        if(!objects.isEmpty()){
            TO<?> pue = objects.iterator().next();
            String pueType = env.getPropertyValue(pue, "type", String.class);
            
            Element objectElem = doc.createElement("object");
            objectElem.setAttribute("id", TOFactory.getInstance().saveTO(pue));
            objectElem.setAttribute("pueType", pueType);
            root.appendChild(objectElem);
        }
        return doc;
    }
}
