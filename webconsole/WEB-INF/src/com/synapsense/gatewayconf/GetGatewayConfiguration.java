package com.synapsense.gatewayconf;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.XMLHelper;

public class GetGatewayConfiguration extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 7466430356761565898L;
    private static final Log LOG = LogFactory.getLog(GetGatewayConfiguration.class);
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {

        LOG.info(this.getServletName() + " is started ...");

        String str = "<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>";
        String reqType = request.getParameter("requestType");

        try {
            if ("getTypes".equals(reqType)) {
                str = XMLHelper.getDocumentXml(getTypesDoc(request));
            } else {
                str = XMLHelper.getDocumentXml(getPropsDoc(request));
            }
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }

        sendResponse(response, str);

        LOG.info(this.getServletName() + " is finished ...");
    }

    private Document getTypesDoc(HttpServletRequest request) throws ParserConfigurationException {
        Document doc = XMLHelper.createNewXmlDocument("types");
        Element root = doc.getDocumentElement();

        TypeUtil tUtil = TypeUtil.getInstance();
        Set<String> typeNames = tUtil.listTypeNames();
        ObjectType[] types = Env.getEnv(request).getObjectTypes(typeNames.toArray(new String[0]));

        for (ObjectType objType : types) {
            Element type = doc.createElement("type");
            String typeName = objType.getName();
            type.setAttribute("name", typeName);
            type.setAttribute("mappedname", tUtil.getDisplayableTypeName(typeName));
            root.appendChild(type);
        }
        return doc;
    }
    
    private Document getPropsDoc(HttpServletRequest request) throws ParserConfigurationException {
        Document doc = XMLHelper.createNewXmlDocument("props");
        Element root = doc.getDocumentElement();
        Environment env = Env.getEnv(request);

        String typeName = request.getParameter("typeName");
        String reqType = request.getParameter("requestType");

        TypeUtil tUtil = TypeUtil.getInstance();
        List<String> propNames = tUtil.listVisiblePropertyNames(typeName);

        for (String propertyName : propNames) {
            try {
                Element propElem = doc.createElement("prop");
                PropertyDescr pdescr = env.getPropertyDescriptor(typeName, propertyName);
                propElem.setAttribute("name", propertyName);
                propElem.setAttribute("mappedname", TypeUtil.getInstance().getDisplayablePropertyName(typeName, propertyName));
                Class<?> propClass = pdescr.getType();
                if (Double.class.isAssignableFrom(propClass)) {
                    root.appendChild(propElem);
                } else if (TO.class.isAssignableFrom(propClass) && "modbus".equals(reqType)) {
                    //In case when the subject property is TO, we need to check whether this property is a reference to some object, which has a property "lastValue". 
                    //If there is no such objects found, we hide this property.
                    Collection<TO<?>> objs = env.getObjectsByType(typeName);
                    Collection<CollectionTO> colObjs = env.getPropertyValue(objs, new String[] { propertyName });
                    for(CollectionTO obj : colObjs) {
                        ValueTO vto = obj.getPropValue(propertyName).get(0);
                        TO<?> to = (TO<?>) vto.getValue();
                        if(to == null) continue;
                        env.getPropertyValue(to, "lastValue", Double.class);
                        root.appendChild(propElem);
                        break;
                    }
                }
            } catch (PropertyNotFoundException e) {
                LOG.info(e.getLocalizedMessage());
            } catch (ObjectNotFoundException e) {
                LOG.info(e.getLocalizedMessage());
            } catch (UnableToConvertPropertyException e) {
                LOG.info(e.getLocalizedMessage());
            }
        }
        return doc;
    }

}
