package com.synapsense.objectmonitor;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.Environment;

public class Info {
    
    protected static Map<String, List<String>> infoMap = new HashMap<String, List<String>>();
    
    protected static Map<String, String[]> monitoredPropsMap = new HashMap<String, String[]>();
    
    protected static String[] defaultMonitoredProps = {"lastValue"};
    
    static{
        /*List<String> props = new ArrayList<String>();
        props.add("itPowerInputs");
        props.add("powerLossInputs");
        props.add("coolingPowerInputs");		
        infoMap.put("PUE", props);*/
        
        /*props = new ArrayList<String>();
        props.add("inputA");
        props.add("inputB");
        infoMap.put("MATH_OPERATION", props);*/
        
        List<String> props = new ArrayList<String>();
        props.add("source");
        infoMap.put("GRAPH_INLET", props);
        
        props = new ArrayList<String>();
        props.add("input");
        infoMap.put("GRAPH_OUTLET", props);
        
        monitoredPropsMap.put("PUE", new String[]{"lastValue", "itPower", "coolingPower", "lightingPower", "infrastructurePower"});
        monitoredPropsMap.put("MATH_OPERATION", new String[]{"lastValue", "opA", "opB", "opM"});
    };
    
    public static List<String> getInputPropsForType(String typeName, Environment env){
        if(!infoMap.containsKey(typeName)){
            ObjectType objType = env.getObjectType(typeName);
            if(objType != null){
                List<String> props = new ArrayList<String>();
                for(PropertyDescr pd : objType.getPropertyDescriptors()){
                    if(pd.isCollection()){
                        props.add(pd.getName());
                    }
                }
                Collections.sort(props);
                infoMap.put(typeName, props);
            }
        }		
        return infoMap.get(typeName);
    }
    
    public static String[] getMonitoredPropertiesForType(String typeName, Environment env){
        String[] monitProps = monitoredPropsMap.get(typeName);
        if(monitProps == null){
            monitProps = defaultMonitoredProps;
        }
        return monitProps;
    }
}
