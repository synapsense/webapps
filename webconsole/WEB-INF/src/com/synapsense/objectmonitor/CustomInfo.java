package com.synapsense.objectmonitor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.synapsense.service.Environment;

public class CustomInfo {
    protected Map<String, List<String>> infoMap = new HashMap<String, List<String>>();
    protected Map<String, String[]> monitoredProps = new HashMap<String, String[]>();
    
    public List<String> getInputPropsForType(String typeName, Environment env){
        List<String> inputProps = infoMap.get(typeName);
        if(inputProps == null){
            inputProps = Info.getInputPropsForType(typeName, env);
        }
        return inputProps;
    }
    
    public String[] getMonitoredPropertiesForType(String typeName, Environment env){
        String[] props = monitoredProps.get(typeName);
        if(props == null){
            props = Info.getMonitoredPropertiesForType(typeName, env);
        }
        return props;
    }
    
    public void addInputPropsForType(String typeName, List<String> inputProps){
        infoMap.put(typeName, inputProps);
    }
    
    public void addMonitoredPropsForType(String typeName, String[] monitProps){
        monitoredProps.put(typeName, monitProps);
    }
    
    public void removeInputProps(String typeName){
        infoMap.remove(typeName);
    }
    
    public void removeMonitoredPropsForType(String typeName){
        monitoredProps.remove(typeName);
    }
}
