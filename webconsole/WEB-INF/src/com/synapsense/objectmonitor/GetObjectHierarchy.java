package com.synapsense.objectmonitor;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.dataconverters.UsersDataOptions;

public class GetObjectHierarchy extends BaseHttpServlet {
    private static final long serialVersionUID = 5342234793198734680L;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {

            TO<?> objectTO = TOFactory.getInstance().loadTO(request.getParameter("objId"));
            CustomInfo customInfo = prepareCustomInfo(request);
            
            Element rootElement = TreeCreator.create(objectTO, customInfo, request);

            StringBuffer outputBuffer = new StringBuffer();
            SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),UsersDataOptions.getTzIdFromRequest(request));
            TreePrinter.printElement(rootElement, outputBuffer, sdf, Env.getEnv(request));
            
            /*response.setContentType("APPLICATION/OCTET-STREAM");
            String disHeader = "Attachment; Filename=\"data.xml\"";
            response.setHeader("Content-Disposition", disHeader);*/
            sendResponse(response, outputBuffer.toString());
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }
    }
    
    protected CustomInfo prepareCustomInfo(HttpServletRequest request){
        CustomInfo customInfo = new CustomInfo();
        //TODO: use json to send/parse [objType, inputProps, monitoredProps]
        String concretePropsStr = request.getParameter("props");
        if(concretePropsStr != null && !concretePropsStr.isEmpty()){
            String [] concreteProps = concretePropsStr.split(",");
            List<String> inputProps = new ArrayList<String>();
            for(int i = 0; i < concreteProps.length; i++){
                inputProps.add(concreteProps[i]);
            }
            
            String objType = TOFactory.getInstance().loadTO(request.getParameter("objId")).getTypeName();
            customInfo.addInputPropsForType(objType, inputProps);
        }
        return customInfo;
    }
}
