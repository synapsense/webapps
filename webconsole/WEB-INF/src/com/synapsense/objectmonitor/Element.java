package com.synapsense.objectmonitor;

import java.util.ArrayList;
import java.util.List;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.utilities.es.PropertyId;

public class Element {	
    protected List<Element> inputObjects = new ArrayList<Element>();
    protected TO<?> to = null;
    PropertyId propId = null;
    
    protected List<ValueTO> currentValues = null;
    
    public List<ValueTO> getCurrentValues() {
        return currentValues;
    }

    public void setCurrentValues(List<ValueTO> currentValues) {
        this.currentValues = currentValues;
    }

    public TO<?> getTo() {
        return to;
    }

    public void setTo(TO<?> to) {
        this.to = to;
    }

    public PropertyId getPropId() {
        return propId;
    }

    public void setPropId(PropertyId propId) {
        this.propId = propId;
    }
        
    public Element(){
    }
    
    public List<Element> getInputObjects(){
        return this.inputObjects;
    }
    
    public void setInputObjects(List<Element> inputObjects){
        this.inputObjects = inputObjects;
    }
}
