package com.synapsense.objectmonitor;

import java.text.SimpleDateFormat;
import java.util.List;

import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.CommonFunctions;

public class TreePrinter {
    
    public static void printElement(Element elem, StringBuffer outputBuffer, SimpleDateFormat dateFormatter, Environment env) throws ObjectNotFoundException, UnableToConvertPropertyException{
        
        String xmlElementName = "object";
        
        String nameStr = "";
        if(elem.getPropId() != null){
            nameStr = elem.getPropId().getPropName();
            xmlElementName = "property";
        }else if(elem.getTo() != null){
            try {
                nameStr = env.getPropertyValue(elem.getTo(), "name", String.class);
                
                if(nameStr != null){
                    nameStr = CommonFunctions.escapeXML(nameStr);
                }
                
            } catch (PropertyNotFoundException e) {
                nameStr = TOFactory.getInstance().saveTO(elem.getTo());
            }
        }
        
        StringBuffer propsVal = new StringBuffer();
        List<ValueTO> elemProps = elem.getCurrentValues();
        
        if(elemProps != null && !elemProps.isEmpty()){
            for(ValueTO v : elemProps){
                propsVal.append(" ").append(v.getPropertyName()).append("='");
                if(v.getValue() != null){
                    propsVal.append(v.getValue().toString());
                }
                propsVal.append("' ");
                
                if("lastValue".equals(v.getPropertyName())){
                    propsVal.append("timestamp='").append(dateFormatter.format(v.getTimeStamp())).append("'");
                }
            }
        }
        
        outputBuffer.append("\n").append("<").append(xmlElementName).append(" name='").append(nameStr).append("'").append(propsVal);
        
        List<Element> inputs = elem.getInputObjects();
        if(inputs != null && !inputs.isEmpty()){
            
            outputBuffer.append(">");
            
            for(Element el : inputs){
                printElement(el, outputBuffer, dateFormatter, env);
            }
            
            outputBuffer.append("\n").append("</").append(xmlElementName).append(">");
        }else{
            
            outputBuffer.append("/>");
        }
    }
}
