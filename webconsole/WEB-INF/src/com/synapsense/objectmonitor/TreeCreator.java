package com.synapsense.objectmonitor;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.PropertyId;

public class TreeCreator {
    
    protected static int maxTreeLevel = -1;
    
    public static Element create(TO<?> objectTO, CustomInfo customInfo, HttpServletRequest request){
        
        Environment env = Env.getEnv(request);
        
        Element result = null;

        try {
            //TO<?> pueTO = TOFactory.getInstance().loadTO("PUE:2415");
            
            Element rootElement = new Element();
            rootElement.setTo(objectTO);
            
            populate(rootElement, customInfo, env, new HashSet<TO<?>>());
            
            result = rootElement;
        
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return result;
    }
    
    protected static void populate(Element elem, CustomInfo customInfo, Environment env, Set<TO<?>> passedTOs) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        List<String> props = customInfo.getInputPropsForType(elem.getTo().getTypeName(), env);
        
        if(passedTOs != null){
            
            if(maxTreeLevel > 0 && passedTOs.size() >= maxTreeLevel){
                System.out.println("max level reached.");
                return;
            }
            
            if(passedTOs.contains(elem.getTo())){
                System.out.println("Infinity loop, object " + elem.getTo() + " is passed twice!");
                return;
            }else{
                passedTOs.add(elem.getTo());
            }
        }
        
        //get properties values (for example, lastValue)
        try{
            List<TO<?>> wrapper = new ArrayList<TO<?>>();
            wrapper.add(elem.getTo());
            Collection<CollectionTO> cto = env.getPropertyValue(wrapper, customInfo.getMonitoredPropertiesForType(elem.getTo().getTypeName(), env));			
            wrapper = null;
            
            if(!cto.isEmpty()){
                elem.setCurrentValues(cto.iterator().next().getPropValues());
            }
        }catch(Exception e){
            //just skeep
        }
        
        if(props != null && !props.isEmpty()){
            
            List<Element> rootInputs = elem.getInputObjects();
            for(String propName : props){
                Element propElem = new Element();
                propElem.setPropId(new PropertyId(TOFactory.getInstance().saveTO(elem.getTo()), propName));
                rootInputs.add(propElem);
                
                List<Element> propInputs = propElem.getInputObjects();
                PropertyId pId = propElem.getPropId();
                Collection<TO<?>> inputs = getInputs(pId, env);
                
                if(inputs != null && !inputs.isEmpty()){
                    for(TO<?> input : inputs){
                        Element objElem = new Element();
                        objElem.setTo(input);
                        propInputs.add(objElem);
                        
                        populate(objElem, customInfo, env, passedTOs);
                    }
                }
            }
            
            if(passedTOs != null){
                passedTOs.remove(elem.getTo());
            }
        }else{
            
            if(passedTOs != null){
                passedTOs.remove(elem.getTo());
            }
            return;
        }
    }
    
    @SuppressWarnings("unchecked")
    protected static Collection<TO<?>> getInputs(PropertyId pId, Environment env){
        Collection<TO<?>> inputs = new ArrayList<TO<?>>();
        
        List<TO<?>> wrapper = new ArrayList<TO<?>>();
        wrapper = new ArrayList<TO<?>>();
        wrapper.add(pId.getObjectTO());
        Collection<CollectionTO> cto = env.getPropertyValue(wrapper, new String[]{pId.getPropName()});
        if(!cto.isEmpty()){
            ValueTO vto = cto.iterator().next().getSinglePropValue(pId.getPropName());
            if(vto != null){
                Object value = vto.getValue();
                if(value != null){
                    if(value instanceof Collection){
                        inputs = (Collection<TO<?>>)value;
                    }else if(value instanceof TO){
                        inputs.add((TO<?>)value);
                    }
                }
            }
        }
        return inputs;
    }

}
