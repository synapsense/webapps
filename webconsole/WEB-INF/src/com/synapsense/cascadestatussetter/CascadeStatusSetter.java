package com.synapsense.cascadestatussetter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.ActiveControlUtils;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.es.EnvironmentTypes;

public class CascadeStatusSetter {
    private static final Log LOG = LogFactory.getLog(CascadeStatusSetter.class);
    
    private static final Set<String> balckNodeParentSet = new HashSet<String>();
    static {
        balckNodeParentSet.add(EnvironmentTypes.TYPE_NETWORK);
        balckNodeParentSet.add(EnvironmentTypes.TYPE_ROOM);
    }
    
    public static String setObjectStatus(Environment env, TO<?> to, int status) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        String result = ErrorMessages.SUCCESS;
        Collection<TO<?>> affectedObjects = new ArrayList<TO<?>>();
        getAllAffectedObjects(env, to, affectedObjects);

        // Set status
        for (TO<?> obj : affectedObjects) {
            if (obj.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_NODE)) {
                Collection<TO<?>> sensors = env.getChildren(obj, EnvironmentTypes.TYPE_SENSOR);
                for (TO<?> sensor : sensors) {
                    env.setPropertyValue(sensor, "status", status);
                    env.setPropertyValue(sensor, "lastValue", -5000d);
                }
            }
            
            try {
                env.setPropertyValue(obj, "status", status);
            } catch (PropertyNotFoundException e) {
                // object has no status property - that is OK - just skip and continue
                LOG.info(obj + " object excluded from cascade enabling/disbaling, since it has no status property");
            }
        }
        
        return result;
    }
    
    private static void getAllAffectedObjects(Environment env, TO<?> to, Collection<TO<?>> result) throws ObjectNotFoundException {
        result.add(to);		
        Collection<TO<?>> affObjects =	getAffectedObjects(env, to);		
        for (TO<?> affObj : affObjects) {
            if (result.contains(affObj)) {
                continue;
            }
            getAllAffectedObjects(env, affObj, result);
        }
    }
    
    private static Collection<TO<?>> getAffectedObjects(Environment env, TO<?> to) throws ObjectNotFoundException {
        Collection<TO<?>> objects = null;
        
        if (to.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_NODE)) {
            objects = excludeObjects(env.getParents(to), balckNodeParentSet);
            // we intentionally don't add node sensors here, to avoid unnecessary processing
        } else {
            objects = env.getChildren(to, EnvironmentTypes.TYPE_NODE);
            
            if (ActiveControlUtils.getCoinEnvParentTypes().contains(to.getTypeName())) {
                objects.addAll(env.getChildren(to, EnvironmentTypes.TYPE_CONTROLPOINT_SINGLECOMPARE));
                objects.addAll(env.getChildren(to, EnvironmentTypes.TYPE_CONTROLPOINT_STRAT));
            }
        }
        
        return objects;
    }
    
    private static Collection<TO<?>> excludeObjects (Collection<TO<?>> objects, Set<String> typesToExclude) {
        Collection<TO<?>> result = new ArrayList<TO<?>>();
        for (TO<?> obj : objects) {
            if (!typesToExclude.contains(obj.getTypeName())) {
                result.add(obj);
            }
        }
        return result; 
    }
}
