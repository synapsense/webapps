package com.synapsense.session;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import java.util.Properties;


import javax.naming.Context;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.SessionTrackingMode;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.config.UIConfig;
import com.synapsense.dataAccess.Env;
import com.synapsense.service.Environment;
import com.synapsense.service.nsvc.Event;
import com.synapsense.service.nsvc.NotificationService;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEvent;
import com.synapsense.service.nsvc.events.ConfigurationCompleteEventProcessor;
import com.synapsense.service.nsvc.events.ConfigurationStartedEvent;
import com.synapsense.service.nsvc.events.ConfigurationStartedEventProcessor;
import com.synapsense.service.nsvc.events.DispatchingProcessor;
import com.synapsense.service.nsvc.filters.EventTypeFilter;
import com.synapsense.transport.TransportManager;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.ConsoleConfigurationData;
import com.synapsense.utilities.lang.LangManager;
import com.synapsense.utilities.lang.ResBundleConverter;

public class ApplicationWatch implements ServletContextListener, ConfigurationStartedEventProcessor, ConfigurationCompleteEventProcessor, Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 6510276629426733190L;
    private static final Log LOG = LogFactory.getLog(ServletContextListener.class);
    private static String path = "./";
    private static boolean configuring = false;
    
    private static final String DISPATCHING_PROCESSOR = "DispatchingProcessor";

    public static String getPath() {
        return path;
    }
    
    public static boolean isConfiguring() {
        return configuring;
    }
    
    @Override
    public void contextDestroyed(ServletContextEvent ce) {
        LOG.debug("contextDestroyed()");
        try {
            // not a subscriber now
            Properties p = getConnectionProperties(ce.getServletContext());
            p.put("com.synapsense.transport","com.synapsense.transport.rmi.AsyncRmiClientTransport");
            //p.put("com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiServiceName", "SynapServer/RmiNotificationTransport");
            NotificationService notificationService = TransportManager.getProxy(p, NotificationService.class).getService();
            DispatchingProcessor dispProcessor = (DispatchingProcessor) ce.getServletContext().getAttribute(DISPATCHING_PROCESSOR);
            notificationService.deregisterProcessor(dispProcessor);
            ce.getServletContext().setAttribute(DISPATCHING_PROCESSOR, null);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent ce) {
        LOG.debug("contextInitialized()");
        
        ServletContext servletContext = ce.getServletContext();
        
        //Configure URL session tracking mode 
        Set<SessionTrackingMode> sessionTrackingModes = new HashSet<SessionTrackingMode>(1);
        sessionTrackingModes.add(SessionTrackingMode.URL);
        servletContext.setSessionTrackingModes(sessionTrackingModes);
        //Delete jsessionid cookie
        servletContext.getSessionCookieConfig().setMaxAge(0);

        path = servletContext.getRealPath("/");
        Properties p = Env.getConnectionProperties(servletContext);
        Env.setContextParams(servletContext);
        Env.loginAsSynapUserIfRemote(); // for development version. Does nothing in production version.
        Env.init(p);
        Env.loginAsSynapUser();
        this.updateConsoleConfigurationData();
        ResBundleConverter.init();
        CommonFunctions.loadInitialStates(null);
        UIConfig.init();
        LangManager.init(servletContext);
         
        try {
            p.put("com.synapsense.transport", "com.synapsense.transport.rmi.AsyncRmiClientTransport");
            p.put("com.synapsense.nsvc.transport.rmi.AsyncRmiClientTransport.RmiServiceName", "SynapServer/RmiNotificationTransport");
            NotificationService notificationService = TransportManager.getProxy(p, NotificationService.class).getService();
            // subscribe to events
            DispatchingProcessor dispProcessor = new DispatchingProcessor().putCcep(this).putCsep(this);
            Collection<Class<? extends Event>> events = new HashSet<Class<? extends Event>>();
            events.add(ConfigurationStartedEvent.class);
            events.add(ConfigurationCompleteEvent.class);
            notificationService.registerProcessor(dispProcessor, new EventTypeFilter(events));
            servletContext.setAttribute(DISPATCHING_PROCESSOR, dispProcessor);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    private void updateConsoleConfigurationData(){
        //Env.login(Env.DEFAULT_USER, Env.DEFAULT_PASSWORD);
        Environment env = Env.getEnv();
        ConsoleConfigurationData.update(env);
    }

    private Properties getConnectionProperties(ServletContext c){
        final String URL_PKG_PREFIXES = c.getInitParameter("URL_PKG_PREFIXES");
        Properties environment = new Properties();
        environment.put(Context.URL_PKG_PREFIXES,URL_PKG_PREFIXES);
        return environment;
    }

    @Override
    public ConfigurationCompleteEvent process(ConfigurationCompleteEvent e) {
        LOG.info("Configuration Complete");
        configuring = false;
        return e;
    }

    @Override
    public ConfigurationStartedEvent process(ConfigurationStartedEvent e) {
        LOG.info("Configuration Started");
        configuring = true;
        return e;
    }

}
