package com.synapsense.session;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;


public class GetContextParameters extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 2500933470299041235L;

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }    
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String param = request.getParameter(RequestVariableNames.PARAM_TYPE);
        sendResponse(response, "<result>" + getServletContext().getInitParameter(param) + "</result>");
    }

}
