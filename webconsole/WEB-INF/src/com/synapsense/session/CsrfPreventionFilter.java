package com.synapsense.session;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import javax.servlet.http.HttpSession;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.RequestVariableNames;

/**
 * @author aklushin Provides basic CSRF protection for a web application.
 */
public class CsrfPreventionFilter extends BaseFilter {

    private static final Log log = LogFactory
            .getLog(CsrfPreventionFilter.class);

    private String randomClass = SecureRandom.class.getName();
    private Random randomSource = null;
    private int nonceCacheSize = 10;

    @Override
    public void destroy() {
    }

    /**
     * Sets the number of previously issued nonces that will be cached on a LRU
     * basis to support parallel requests, limited use of the refresh and back
     * in the browser and similar behaviors that may result in the submission of
     * a previous nonce rather than the current one. If not set, the default
     * value of 10 will be used.
     * 
     * @param nonceCacheSize
     *            The number of nonces to cache
     */
    public void setNonceCacheSize(int nonceCacheSize) {
        this.nonceCacheSize = nonceCacheSize;
    }

    /**
     * Specify the class to use to generate the nonces. Must be in instance of
     * {@link Random}.
     * 
     * @param randomClass
     *            The name of the class to use
     */
    public void setRandomClass(String randomClass) {
        this.randomClass = randomClass;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        super.init(filterConfig);
        
        // Set the parameters
        Enumeration<?> paramNames = filterConfig.getInitParameterNames();

        while (paramNames.hasMoreElements()) {
            String paramName = (String) paramNames.nextElement();
            try {
                BeanUtils.setProperty(this, paramName,
                        filterConfig.getInitParameter(paramName));
            } catch (Exception e) {
                log.warn("cannot set property:" + paramName, e);
            }
        }

        try {
            Class<?> clazz = Class.forName(randomClass);
            randomSource = (Random) clazz.newInstance();
        } catch (ClassNotFoundException e) {
            ServletException se = new ServletException("Invalid random class",
                    e);
            throw se;
        } catch (InstantiationException e) {
            ServletException se = new ServletException("Invalid random class",
                    e);
            throw se;
        } catch (IllegalAccessException e) {
            ServletException se = new ServletException("Invalid random class",
                    e);
            throw se;
        }
    }

    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding("UTF-8");
        
        if (request instanceof HttpServletRequest
                && response instanceof HttpServletResponse) {

            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;

            String path = req.getServletPath();
            if (req.getPathInfo() != null) {
                path = path + req.getPathInfo();
            }
            boolean skipNonceCheck = false;
            boolean entryPointRequest = isEntryPoint(path);
            if (!entryPointRequest) {
                skipNonceCheck = isNotCheckedUrl(path);
            }
            // actually "getresponseheadres" indicate can client
            // parse response or not
            boolean getresponseheadres = "true".equals(req
                    .getParameter(RequestVariableNames.GETRESPONSEHEADERS));
            if (entryPointRequest) {
                if (!getresponseheadres) {
                    sendError(res, HttpServletResponse.SC_CONFLICT,
                            "Invalid client");
                    return;
                }
                CsrfResponseWrapper wResponse = new CsrfResponseWrapper(res);
                wResponse.addCsrfOpenTagToResponse();
                chain.doFilter(request, wResponse);
                // entry point create a session and it can invalidate
                // a session.
                HttpSession session = req.getSession(false);
                /*
                 * if (session != null) { session.invalidate(); }
                 */
                // session = req.getSession(true);
                String newNonce = generateNonce();
                if (session != null) {
                    LruCache<String> nonceCache = new LruCache<String>(
                            nonceCacheSize);
                    nonceCache.add(newNonce);
                    session.setAttribute(SessionVariableNames.CSRF_NONCE,
                            nonceCache);
                }
                wResponse.addCsrfCloseTagToResponse(newNonce);
            } else if (!skipNonceCheck) {
                /*
                 * if (!getresponseheadres) { sendError(res,
                 * HttpServletResponse.SC_CONFLICT, "Invalid client"); return; }
                 */
                HttpSession session = req.getSession(false);
                if (session != null) {
                    @SuppressWarnings("unchecked")
                    LruCache<String> nonceCache = (LruCache<String>) session
                            .getAttribute(SessionVariableNames.CSRF_NONCE);
                    String previousNonce = req
                            .getParameter(RequestVariableNames.CSRF_NONCE);
                    if (nonceCache != null
                            && nonceCache.contains(previousNonce)) {
                        
                        if(getresponseheadres){
                            CsrfResponseWrapper wResponse = new CsrfResponseWrapper(res);
                            String newNonce = generateNonce();
                            wResponse.addCsrfOpenTagToResponse();
                            
                            chain.doFilter(request, wResponse);
                        
                            session = req.getSession(false);
                            if (session != null){ //After chain.doFilter session can be invalidated
                                nonceCache.add(newNonce);
                            }
                            wResponse.addCsrfCloseTagToResponse(newNonce);
                        }else{
                            chain.doFilter(request, res);
                        }
                    } else {
                        sendError(res, HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }
                } else {
                    sendError(res, HttpServletResponse.SC_FORBIDDEN);
                    return;
                }
            } else {
                if (getresponseheadres) {
                    HttpSession session = req.getSession(false);
                    String newNonce = null;
                    if (session != null) {
                        @SuppressWarnings("unchecked")
                        LruCache<String> nonceCache = (LruCache<String>) session
                                .getAttribute(SessionVariableNames.CSRF_NONCE);
                        if (nonceCache != null) {
                            newNonce = generateNonce();
                            nonceCache.add(newNonce);
                        }
                    }
                    
                    if (newNonce != null) {
                        CsrfResponseWrapper wResponse = new CsrfResponseWrapper(res);
                        wResponse.addCsrfOpenTagToResponse();						
                        chain.doFilter(request, wResponse);						
                        wResponse.addCsrfCloseTagToResponse(newNonce);
                    }else{
                        chain.doFilter(request, res);
                    }					
                } else {
                    chain.doFilter(request, response);
                }
            }
        } else {
            chain.doFilter(request, response);
        }
    }

    private void sendError(HttpServletResponse response, int code, String msg)
            throws IOException {
        response.sendError(code, msg);
    }

    private void sendError(HttpServletResponse response, int code)
            throws IOException {
        response.sendError(code);
    }

    /**
     * Generate a once time token (nonce) for authenticating subsequent
     * requests. This will also add the token to the session. The nonce
     * generation is a simplified version of ManagerBase.generateSessionId().
     * 
     */
    protected String generateNonce() {
        byte random[] = new byte[16];

        // Render the result as a String of hexadecimal digits
        StringBuilder buffer = new StringBuilder();

        randomSource.nextBytes(random);

        for (int j = 0; j < random.length; j++) {
            byte b1 = (byte) ((random[j] & 0xf0) >> 4);
            byte b2 = (byte) (random[j] & 0x0f);
            if (b1 < 10)
                buffer.append((char) ('0' + b1));
            else
                buffer.append((char) ('A' + (b1 - 10)));
            if (b2 < 10)
                buffer.append((char) ('0' + b2));
            else
                buffer.append((char) ('A' + (b2 - 10)));
        }

        return buffer.toString();
    }
    
    protected static class CsrfResponseWrapper extends HttpServletResponseWrapper {
        
        public CsrfResponseWrapper(HttpServletResponse response) throws UnsupportedEncodingException, IOException {			
            super(response);
            this.getResponse().setContentType("text/xml; charset=utf-8");
        }
                
        public void addCsrfOpenTagToResponse() throws IOException{
            this.getWriter().write("<csrf_prevention_resultset><body>");
        }
        
        public void addCsrfCloseTagToResponse(String nonce) throws IOException{
            String str = "</body><headers><header name=\""
                        + RequestVariableNames.CSRF_NONCE + "\" value=\"" + nonce
                        + "\"/></headers></csrf_prevention_resultset>";
            
            this.getWriter().write(str);			
        }
        
        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            throw new IllegalStateException("Method getOutputStream() is depricated when HttpRequest has parameter '" + RequestVariableNames.GETRESPONSEHEADERS + "=true'. Method getWriter() should be used instead.");
        }
    }

    private static class LruCache<T> {

        // Although the internal implementation uses a Map, this cache
        // implementation is only concerned with the keys.
        private final Map<T, T> cache;

        public LruCache(final int cacheSize) {
            cache = Collections.synchronizedMap(new LinkedHashMap<T, T>() {
                private static final long serialVersionUID = 1L;

                @Override
                protected boolean removeEldestEntry(Map.Entry<T, T> eldest) {
                    if (size() > cacheSize) {
                        return true;
                    }
                    return false;
                }
            });
        }

        public void add(T key) {
            cache.put(key, null);
        }

        public boolean contains(T key) {
            return cache.containsKey(key);
        }
    }
}
