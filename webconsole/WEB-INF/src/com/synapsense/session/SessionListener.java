package com.synapsense.session;

import java.io.File;
import java.io.FilenameFilter;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        String chartsDirName = session.getServletContext().getRealPath("/")
        + File.separator + "resources/charts";
        //HttpSession session = request.getSession();
        //String chartsDirName = this.getServletContext().getRealPath("/")
        //		+ File.separator + "resources/charts";
        File chartsDir = new File(chartsDirName);
        
        if (!chartsDir.exists() || !chartsDir.isDirectory()){
            return;
        }
        
        String[] filesList = chartsDir.list( new DirectoryFilter("chart_" + session.getId()) );
        //There are several files for historical data charts
        for (int i=0; i < filesList.length; i++){
            File chartFile = new File(chartsDirName + File.separator + 
                    filesList[i]);			
            chartFile.delete();
        }
        
        String[] LIFileList = chartsDir.list( new DirectoryFilter("LI_" + session.getId()) );
        //There are several files for LI
        for (int i=0; i < LIFileList.length; i++){
            File LIFile = new File(chartsDirName + File.separator + 
                    LIFileList[i]);			
            LIFile.delete();
        }
        

    }
    class DirectoryFilter implements FilenameFilter {
        String fileNamePrefix;
        
        DirectoryFilter(String filePrefix){
            fileNamePrefix = filePrefix;
        }
        
        
        public boolean accept(File dir, String name) {			
            return name.startsWith(fileNamePrefix);
        }
    }

}
