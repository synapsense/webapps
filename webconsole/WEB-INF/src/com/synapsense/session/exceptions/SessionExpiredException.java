package com.synapsense.session.exceptions;


public class SessionExpiredException extends RuntimeException {
    public SessionExpiredException(String desc) {
        super(desc);
    }
    
    public SessionExpiredException(String desc, Throwable reason) {
        super(desc, reason);
    }

    private static final long serialVersionUID = -7023131290814833128L;
}
