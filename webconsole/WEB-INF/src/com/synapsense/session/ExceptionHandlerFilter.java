package com.synapsense.session;

import java.io.IOException;
import java.io.PrintWriter;

import javax.ejb.EJBAccessException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.XMLHelper;

/**
 * @author pahunov
 * This filters handles uncaught exceptions from the servlets.
 * It also checks if the system is in maintenance state
 */
public class ExceptionHandlerFilter extends BaseFilter {

    private static final Log LOG = LogFactory.getLog(ExceptionHandlerFilter.class);
    
    private static int ERROR_CODE_AUTHENTICATION_FAILURE = 10;
    private static int ERROR_CODE_SESSION_EXPIRED = 11;
    private static int ERROR_CODE_REQUEST_FAILED = 12;
    private static int ERROR_CODE_CONFIGURING = 13;

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
            FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpSession session = httpRequest.getSession(false);
        
        String errorXml = null;
        boolean loggedIn = false;
        
        try {
            loggedIn = Env.login((HttpServletRequest) request);
            String path = httpRequest.getServletPath();
            if (httpRequest.getPathInfo() != null) {
                path = path + httpRequest.getPathInfo();
            }
            
            if (ApplicationWatch.isConfiguring() && !isNotCheckedUrl(path) && !isEntryPoint(path)) {
                errorXml = XMLHelper.getErrorXML(ERROR_CODE_CONFIGURING," Maintenance");
            } else {
                request.setCharacterEncoding("UTF-8");
                chain.doFilter(request, response);
            }
        }catch(EJBAccessException e) {
            LOG.warn(e.getLocalizedMessage());
            if(session != null){
                session.invalidate();
            }
            errorXml = XMLHelper.getErrorXML(ERROR_CODE_AUTHENTICATION_FAILURE,"Authentication failure");
        }catch(SessionExpiredException e) {
            LOG.warn(e.getLocalizedMessage());
            if(session != null){
                session.invalidate(); 
            }
            errorXml = XMLHelper.getErrorXML(ERROR_CODE_SESSION_EXPIRED,"Session expired");
        }catch(ServletException e) {
            LOG.error(e.getLocalizedMessage(), e);
            errorXml = XMLHelper.getErrorXML(ERROR_CODE_REQUEST_FAILED,(e.getCause() != null) ? e.getCause().getMessage() : e.getMessage());
        }catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            errorXml = XMLHelper.getErrorXML(ERROR_CODE_REQUEST_FAILED,  "Request failed: " + ((e.getCause() != null) ? e.getCause().getMessage() : e.getMessage()));
        } finally {
            if (errorXml != null) {
                writeToResponse((HttpServletResponse) response, errorXml);
            }
            if (loggedIn) {
                Env.logout();
            }
        }
    }
    
    private void writeToResponse(HttpServletResponse response, String xmlStr) throws IOException {
        response.setContentType("text/xml; charset=utf-8");
        PrintWriter out = response.getWriter();
        out.print(xmlStr);
    }
}
