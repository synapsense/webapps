package com.synapsense.presentation;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

import com.synapsense.dataAccess.Env;
import com.synapsense.service.Environment;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.dto.TO;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.EnvXMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetTreeView extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetTreeView.class);
    
    private class ObjectHierarchy {
        private String typeName;
        private List<String> childTypes = new ArrayList<String>();
        
        ObjectHierarchy(String tn, String... cts) {
            this.typeName = tn;
            for (String ct : cts) {
                this.childTypes.add(ct);
            }
        }
        
        public String getTypeName() { return typeName; }
        public List<String> getChildTypes() { return childTypes; }
    }
    
    private static final Map<String, ObjectHierarchy> hierarchy =
            new HashMap<String, ObjectHierarchy>();
    {
        ObjectHierarchy root = new ObjectHierarchy(EnvironmentTypes.TYPE_ROOT, EnvironmentTypes.TYPE_DC);
        hierarchy.put(EnvironmentTypes.TYPE_ROOT, root);
        ObjectHierarchy drawing = new ObjectHierarchy(EnvironmentTypes.TYPE_DC, EnvironmentTypes.TYPE_ROOM);
        hierarchy.put(EnvironmentTypes.TYPE_DC, drawing);
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        Document treeDoc = null;
        try {
            treeDoc = XMLHelper.createNewXmlDocument("tree_view");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return;
        }

        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 1);
        response.setDateHeader("Last-modified", System.currentTimeMillis());

        try {
            Collection<TO<?>> roots = Env.getEnv(request).getObjectsByType(
                    EnvironmentTypes.TYPE_ROOT);
            Element root = treeDoc.createElement("root");
            root.setAttribute("name", "Root");
            Iterator<TO<?>> iterator = roots.iterator();
            if (iterator.hasNext()) {
                addChildren(roots.iterator().next(), treeDoc, root, request);
            }
            treeDoc.getDocumentElement().appendChild(root);
            
            logDOM(root);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        sendResponse(response, treeDoc);
    }


    void addChildren(TO<?> parent, Document treeDoc, Element node,
            HttpServletRequest request) throws ObjectNotFoundException {
        if (parent == null) {
            return;
        }

        Environment env = Env.getEnv(request);

        String openNodesStr = request.getParameter("openNodes");
        List<String> openNodes = convertIdsStringToArrayList(openNodesStr);
        String selectionId = request.getParameter("selectionId");
        String highlightedId = request.getParameter("highlightedId");
        String[] props = { "name", "status", "numAlerts" };

        ObjectHierarchy h = hierarchy.get(parent.getTypeName());
        if (h != null) {
            for (String ct : h.getChildTypes()) {
                Collection<TO<?>> tos = env.getRelatedObjects(parent, ct, true);
                LOG.debug("Found " + (tos != null ? tos.size() : 0) + " children of '" + h.getTypeName() + "'");
                if (tos == null || tos.isEmpty()) {
                    continue;
                }
                
                Collection<CollectionTO> propsCol = env.getPropertyValue(tos, props);
                propsCol = CollectionTOSorter.sortObjects(propsCol);
                for (CollectionTO propTo : propsCol) {
                    TO<?> to = propTo.getObjId();
                    try {
                        String type = to.getTypeName();
                        String name = env.getPropertyValue(to, "name", String.class);
                        LOG.debug("Child of type '" + type + "' with name = '" + name + "'");
                    } catch (Exception e) {// handle common exceptions from proxy env
                        LOG.trace(e.getLocalizedMessage());
                    }

                    Element elem = treeDoc.createElement(ct);
                    if (elem != null) {
                        EnvXMLHelper.writeIDs(to, elem);
                        EnvXMLHelper.writePropAsAttrs(propTo, elem);
                        
                        try {
                            String instance = env.getPropertyValue(to,
                                    "instance_url", String.class);
    
                            if (instance != null && !"".equals(instance)) {
                                setActiveAlert(instance, elem);
                                continue;
                            }
                        } catch (Exception e) {// handle common exceptions from proxy env
                            LOG.trace(e.getLocalizedMessage());
                        }
    
                        // Laszlo opttree attributes
                        try {
                            String serializedTO = TOFactory.getInstance().saveTO(to);
    
                            if (serializedTO.equals(selectionId)) {
                                elem.setAttribute("selected", "true");
                            }
                            if (serializedTO.equals(highlightedId)) {
                                elem.setAttribute("highlighted", "true");
                            }
                            if (openNodes.contains(serializedTO)) {
                                elem.setAttribute("__OPTTREE_META_open", "true");
                            }
                        } catch (DOMException e) {
                            LOG.error(e.getLocalizedMessage(), e);
                        }
                        
                        logDOM(elem);
                        node.appendChild(elem);
                    }
                    
                    addChildren(to, treeDoc, elem != null ? elem : node, request);
                }
            }
        }
    }

    private static void logDOM(Node dom) {
        DOMImplementationLS lsImpl = (DOMImplementationLS)dom.getOwnerDocument().getImplementation().getFeature("LS", "3.0");
        LSSerializer serializer = lsImpl.createLSSerializer();
        serializer.getDomConfig().setParameter("xml-declaration", false); //by default its true, so set it to false to get String without xml-declaration
        String str = serializer.writeToString(dom);
        LOG.debug(dom.getNodeName() + " = " + str);
    }

    private void setActiveAlert(String instance, Element newelem) {
        InputStream is = null;
        try {

            URL checkAlert = new URL(instance + "/HasActiveAlertsXML?");

            is = checkAlert.openStream();

            DocumentBuilderFactory factory = DocumentBuilderFactory
                    .newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xmlDoc = builder.parse(is);

            Element e = xmlDoc.getDocumentElement();
            if ((e != null) && (e.getAttribute("v").equalsIgnoreCase("true"))) {
                newelem.setAttribute("alert", "true");
            }
        } catch (MalformedURLException e) {
            LOG.error(e.getLocalizedMessage());
        } catch (IOException e) {
            LOG.error(e.getLocalizedMessage());
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage());
        } catch (SAXException e) {
            LOG.error(e.getLocalizedMessage());
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    LOG.error(e.getLocalizedMessage());
                }
            }
        }
    }

    private List<String> convertIdsStringToArrayList(String str) {
        List<String> arr = new ArrayList<String>();

        if (str != null) {
            String[] strArr = str.split(",");
            arr = Arrays.asList(strArr);
        }

        return arr;
    }
}
