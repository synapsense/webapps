package com.synapsense.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class GetDataRequestIQY extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetDataRequestIQY.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        generateIqy(request, response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
    
    private void generateIqy(HttpServletRequest request, HttpServletResponse response){

        String servletName = request.getParameter("servletName");
        
        StringBuffer xlsUrl = new StringBuffer("http://");
        xlsUrl.append(request.getServerName())		
            .append(":")
            .append(request.getServerPort())
            .append(request.getContextPath())
            .append("/");
    
        if (servletName == null){
            servletName = "getDataRequestXML";
        }
        
        xlsUrl.append(servletName)
              .append("?")
              .append(request.getQueryString());
        
        if(request.getParameter("mode") == null){
            xlsUrl.append("&")
                  .append(RequestVariableNames.MODE)
                  .append("=")
                  .append(RequestVariableNames.MODE_EXCEL);
        }				

        try {
           // set the http content type to "APPLICATION/OCTET-STREAM
           response.setContentType("APPLICATION/OCTET-STREAM");

           // initialize the http content-disposition header to
           // indicate a file attachment with the default filename
           // "data.iqy"
           String disHeader = "Attachment; Filename=\"data.iqy\"";
           response.setHeader("Content-Disposition", disHeader);
           response.getOutputStream().println("WEB");
           response.getOutputStream().println("1");
           response.getOutputStream().println(xlsUrl.toString());
        } catch (IOException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }	
}
