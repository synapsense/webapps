package com.synapsense.presentation;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.es.ActivityLogConstans;

public class ManageAnnotations extends BaseHttpServlet {
    public static final long	serialVersionUID	= 0;
    private static final Log LOG = LogFactory.getLog(ManageAnnotations.class);

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        String action = request.getParameter(RequestVariableNames.ACTION);
        String status = "";
        if("update".equals(action)) {
            status = updateAnnotation(request);
        } else if("delete".equals(action)) {
            status = deleteAnnotation(request);
        } else if("add".equals(action)) {
            status = addNewAnnotation(request);
        } else {
            status = "";
        }

        if("".equals(status)) {
            status = "Error!";
        }
        sendResponse(response, "<result>" + status + "</result>");
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        doGet(request,response);
    }
    
    private String updateAnnotation(HttpServletRequest request) {
        String status = ErrorMessages.UNABLE_TO_COMPLETE;
        String annotationId = request.getParameter(RequestVariableNames.ID);
        String annotationNote = request.getParameter(RequestVariableNames.ANNOTATION_NOTE);
        String annotationTimestamp = request.getParameter(RequestVariableNames.ANNOTATION_DATETIME);
        String annotationDc = request.getParameter(RequestVariableNames.ANNOTATION_DC);
        String annotationStatus = request.getParameter(RequestVariableNames.ANNOTATION_STATUS);
        
        try {
            ActivityRecord ar = Env.getActivityLogServ(request).getRecord(Integer.parseInt(annotationId));
            ar.setDescription(annotationNote);
            ar.setTime(new Date(Long.parseLong(annotationTimestamp)));
            if(annotationDc != null && !annotationDc.isEmpty()) {
                TO<?> dc = (Boolean.parseBoolean(annotationStatus)) ? TOFactory.EMPTY_TO : TOFactory.getInstance().loadTO(annotationDc);
                ar.setObject(dc);
            }
            Env.getActivityLogServ(request).updateRecord(ar);
            status = ErrorMessages.SUCCESS;
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return status;
    }
    
    private String addNewAnnotation(HttpServletRequest request) {
        String status = ErrorMessages.UNABLE_TO_COMPLETE;
        
        int whateverWillbeIgnoredId = -1;
        String annotationNote = request.getParameter(RequestVariableNames.ANNOTATION_NOTE);
        String annotationTimestamp = request.getParameter(RequestVariableNames.ANNOTATION_DATETIME);
        String annotationDc = request.getParameter(RequestVariableNames.ANNOTATION_DC);
        String annotationStatus = request.getParameter(RequestVariableNames.ANNOTATION_STATUS);
        
        try {
            TO<?> dc = (Boolean.parseBoolean(annotationStatus)) ? TOFactory.EMPTY_TO : TOFactory.getInstance().loadTO(annotationDc);
            String currentUserName = (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            Env.getActivityLogServ(request).addRecord(new ActivityRecord(whateverWillbeIgnoredId, currentUserName, ActivityLogConstans.MODULE_ANNOTATION, ActivityLogConstans.ACTION_MANAGE_ANNOTATION, annotationNote, new Date(Long.parseLong(annotationTimestamp)),dc));
            
            status = ErrorMessages.SUCCESS;
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return status;
    }
    
    private String deleteAnnotation(HttpServletRequest request) {
        String status = ErrorMessages.UNABLE_TO_COMPLETE;
        String annotationId = request.getParameter(RequestVariableNames.ID);
        
        try {
            Env.getActivityLogServ(request).deleteRecord(Integer.parseInt(annotationId));
            status = ErrorMessages.SUCCESS;
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return status;
    }
}
    
