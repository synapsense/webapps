package com.synapsense.presentation;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.DataRestrictor;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.dataAccess.Env;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.elementmodel.basic.WrapperModelElement;

public class GetActivityLog extends BaseHttpServlet {
    public static final long	serialVersionUID	= 0;
    private static final Log log = LogFactory.getLog(GetActivityLog.class);
    private static int maxRecordsNumberToShow = 10000;
    
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {		
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 1);
        response.setDateHeader("Last-modified", System.currentTimeMillis());

        sendResponse(request, response, getModel(request));
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        doGet(request,response);
    }
    
    private ModelElement getModel(HttpServletRequest request) {
        ModelElement objectsElement = null;
        try {
            Collection<ActivityRecord> records = getFilteredRecords(request);
            int existingNum = records.size();
            records = DataRestrictor.decreaseCollectionToAllowedSize(records, maxRecordsNumberToShow);
            
            objectsElement = new ModelElement("activity_log");
            objectsElement.addChild(new WrapperModelElement(records));
            
            DataRestrictor.addRestrictionInfoToModel(objectsElement, existingNum, records.size());
        } catch(Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }	
        return objectsElement;
    }
    
    private Collection<ActivityRecord> getFilteredRecords(HttpServletRequest request) {
        String searchBy = request.getParameter(RequestVariableNames.SEARCH_BY);
        String searchCriteria = request.getParameter(RequestVariableNames.SEARCH_CRITERIA);
        
        String byCriteria = null;
        Date beginTime = null;
        Date endTime = null;
        if(searchBy != null && !"".equals(searchBy)) {
            String paramsArr[] = searchCriteria.split("\\;");
            byCriteria = paramsArr[0];
            beginTime = new Date(Long.parseLong(paramsArr[1]));
            endTime = new Date(Long.parseLong(paramsArr[2]));
        }
        
        ActivityRecordFilter arf = new ActivityRecordFilter();
        arf.filterByTime(beginTime, endTime);
        
        if (RequestVariableNames.FILTER_BY_ACTION_AND_TIME.equals(searchBy)) {	    		
            arf.filterByAction(byCriteria);	    		
        } else if (RequestVariableNames.FILTER_BY_MODULE_AND_TIME.equals(searchBy)) {
            arf.filterByModule(byCriteria);				
        } else if (RequestVariableNames.FILTER_BY_USER_AND_TIME.equals(searchBy)) {
            arf.filterByUser(byCriteria);	    		
        } else if (RequestVariableNames.FILTER_BY_DATE_ONLY.equals(searchBy)) {
            arf.filterByTime(beginTime, endTime);				
        } else {
            arf = new ActivityRecordFilter();	    		
        }
                    
        return Env.getActivityLogServ(request).getRecords(arf);
    }
}
