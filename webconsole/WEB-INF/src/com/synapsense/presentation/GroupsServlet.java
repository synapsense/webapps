package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CRERule;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ConfigurationException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.service.Environment;
import com.synapsense.service.RulesEngine;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GroupsServlet extends BaseHttpServlet {
    private static final long serialVersionUID = 4680810271017285477L;
    private static final Log LOG = LogFactory.getLog(GetActivityLog.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse response) throws IOException, ServletException {
        String action = req.getParameter("action");
        
        String message = ErrorMessages.SUCCESS;
        try{
            if("getGroupsList".equals(action)){
                sendResponse(response, getGroupsList(req));
                return;
            }else if("getAffectedGroups".equals(action)){
                sendResponse(response, getAffectedGroups(req));
                return;
            }else if("getAllGroups".equals(action)){
                sendResponse(response, getAllGroups(req));
                return;
            }else if("create".equals(action)){
                message = createGroup(req);
            }else if("update".equals(action)){
                message = updateGroup(req);
            }else if("addAllRackServers".equals(action)){
                message = addAllRackServers(req);
            }else if("removeAllRackServers".equals(action)){
                message = removeAllRackServers(req);
            }else if("getAffectedGroupObjects".equals(action)){
                sendResponse(response, getAffectedGroupObjects(req));
                return;
            }else if("addSelectedGroupObjects".equals(action)){
                message = addSelectedGroupObjects(req);
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage(), e);
        }
        sendResponse(response, "<result>" + message + "</result>");
    }
    
    private Document getAllGroups(HttpServletRequest request) throws ParserConfigurationException, ObjectNotFoundException{
        Document objectsDoc = XMLHelper.createNewXmlDocument("groups");
        Element objectsDocRoot = objectsDoc.getDocumentElement();
        
        Environment env = Env.getEnv(request);
        Collection<TO<?>> groups = env.getObjectsByType(EnvironmentTypes.TYPE_GROUP);
        
        addGroupsToDocument(groups, new String[]{"name"}, false, objectsDoc, objectsDocRoot, request);
        
        return objectsDoc;
        
    }
    private Document getAffectedGroups(HttpServletRequest request) throws ParserConfigurationException, ObjectNotFoundException{
        Document objectsDoc = XMLHelper.createNewXmlDocument("groups");
        Element objectsDocRoot = objectsDoc.getDocumentElement();
        
        Environment env = Env.getEnv(request);
        Collection<TO<?>> groups = new HashSet<TO<?>>();
        String[] rackIds = request.getParameter("rackIds").split(",");
        
        for(String rackId : rackIds){
            TO<?> rack = TOFactory.getInstance().loadTO(rackId);
            
            Collection<TO<?>> servers = env.getChildren(rack, EnvironmentTypes.TYPE_SERVER);
            for(TO<?> server : servers){
                groups.addAll(env.getParents(server, EnvironmentTypes.TYPE_GROUP));
            }

            Collection<TO<?>> rpdus = env.getChildren(rack, EnvironmentTypes.TYPE_RPDU);
            for(TO<?> rpdu : rpdus){
                groups.addAll(env.getParents(rpdu, EnvironmentTypes.TYPE_GROUP));
            }
        }
        
        addGroupsToDocument(groups, new String[]{"name"}, false, objectsDoc, objectsDocRoot, request);
        
        return objectsDoc;
    }
    
    private Document getGroupsList(HttpServletRequest request) throws ParserConfigurationException, DOMException, ObjectNotFoundException{
        Document objectsDoc = XMLHelper.createNewXmlDocument("groups");
        Element objectsDocRoot = objectsDoc.getDocumentElement();
        
        Environment env = Env.getEnv(request);
        Collection<TO<?>> groups = env.getObjectsByType(EnvironmentTypes.TYPE_GROUP);
        
        addGroupsToDocument(groups, new String[]{"name", "desc"}, true, objectsDoc, objectsDocRoot, request);
        
        return objectsDoc;
    }
    
    private String createGroup(HttpServletRequest request) throws EnvException {
        String resultMessage = ErrorMessages.SUCCESS;
        
        createGroupTypeIfNotExist(request);
        
        Environment env = Env.getEnv(request);
        Collection<TO<?>> groupsWithSameName = env.getObjects(EnvironmentTypes.TYPE_GROUP, new ValueTO[]{new ValueTO("name", request.getParameter("name"))});
                
        if(groupsWithSameName.isEmpty()){
            ValueTO [] valueArr = getValueTOsFromRequest(request).toArray(new ValueTO[1]);
            TO<?> newGroup = env.createObject(EnvironmentTypes.TYPE_GROUP, valueArr);
            TO<?> rootObj = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT).iterator().next();
            env.setRelation(rootObj, newGroup);
            
            try{
                attachRuleToGroup(newGroup, request);
            }catch(Exception e){
                LOG.error(e.getLocalizedMessage(), e);
                resultMessage = "-1|" + e.getLocalizedMessage();
            }			
        }else{
            resultMessage = ErrorMessages.DUPLICATE_GROUP_NAME;
        }		
        return resultMessage;
    }
    
    private void attachRuleToGroup(TO<?> group, HttpServletRequest request) throws ObjectExistsException, ConfigurationException, ObjectNotFoundException, PropertyNotFoundException{		
        RulesEngine cre = Env.getCRE();
        
        String ruleTypeName = "com.synapsense.rulesengine.core.script.metric.GroupMetrics";
        TO<?> ruleType = cre.getRuleTypeId(ruleTypeName);
        
        if (ruleType == null) {
            ruleType = cre.addRuleType(new RuleType(ruleTypeName, null, "Java", true, "0 5/15 * * * ?"));
        }
        
        String uniqRuleName = TOFactory.getInstance().saveTO(group);
        CRERule rule = new CRERule(uniqRuleName, ruleType, group, "demandPower");
        cre.addRule(rule);		
    }
    
    private String updateGroup(HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        String resultMessage = ErrorMessages.SUCCESS;
        
        Environment env = Env.getEnv(request);
        Collection<TO<?>> groupsWithSameName = env.getObjects(EnvironmentTypes.TYPE_GROUP, new ValueTO[]{new ValueTO("name", request.getParameter("name"))});
        TO<?> group = TOFactory.getInstance().loadTO(request.getParameter("groupId"));
        
        if(groupsWithSameName.isEmpty() || group.equals(groupsWithSameName.iterator().next())){
            ValueTO [] valueArr = getValueTOsFromRequest(request).toArray(new ValueTO[1]);
            env.setAllPropertiesValues(group, valueArr);
        }else{
            resultMessage = ErrorMessages.DUPLICATE_GROUP_NAME;
        }
        return resultMessage;
    }
    
    private String addAllRackServers(HttpServletRequest request) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException, UnsupportedRelationException {
        String resultMessage = ErrorMessages.SUCCESS;
        TO<?> group = TOFactory.getInstance().loadTO(request.getParameter("groupId"));
        String[] rackIds = request.getParameter("rackIds").split(",");		
        Environment env = Env.getEnv(request);		
        
        List<Relation> relations = new ArrayList<Relation>();
        List<TO<?>> allServers = new ArrayList<TO<?>>();
        
        for(String rackId : rackIds){
            TO<?> rack = TOFactory.getInstance().loadTO(rackId);
            
            Collection<TO<?>> servers = env.getChildren(rack, EnvironmentTypes.TYPE_SERVER);
            for(TO<?> server : servers){
                Collection<TO<?>> parentGroups = env.getParents(server, EnvironmentTypes.TYPE_GROUP);
                if(!parentGroups.contains(group)){
                    allServers.add(server);
                }
            }
        }
        
        if(!allServers.isEmpty()) {
            relations.add(new Relation(group, allServers));
            env.setRelation(relations);
        }
        
        return resultMessage;
    }
    
    private String removeAllRackServers(HttpServletRequest request) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        String resultMessage = ErrorMessages.SUCCESS;
        TO<?> group = TOFactory.getInstance().loadTO(request.getParameter("groupId"));
        String[] rackIds = request.getParameter("rackIds").split(",");		
        Environment env = Env.getEnv(request);
        
        List<Relation> relations = new ArrayList<Relation>();
        List<TO<?>> allServers = new ArrayList<TO<?>>();
        
        for(String rackId : rackIds){
            TO<?> rack = TOFactory.getInstance().loadTO(rackId);			
            Collection<TO<?>> servers = env.getChildren(rack, EnvironmentTypes.TYPE_SERVER);
            allServers.addAll(servers);
            }
        
        if(!allServers.isEmpty()){
            relations.add(new Relation(group, allServers));
            env.removeRelation(relations);
        }
        
        return resultMessage;
    }
    
    private Collection<ValueTO> getValueTOsFromRequest(HttpServletRequest req){
        Collection<ValueTO> vals = new ArrayList<ValueTO>();		
        String []propNames = {"name","desc"};
        for(String propName : propNames){
            String propValue = req.getParameter(propName);
            if(propValue != null){
                vals.add(new ValueTO(propName, propValue));
            }
        }
        return vals;
    }
    
    private void addGroupsToDocument(Collection<TO<?>> groups, String []propNames, boolean addMembersNum, Document objectsDoc, Element objectsDocRoot, HttpServletRequest request) throws DOMException, ObjectNotFoundException{
        Environment env = Env.getEnv(request);
        if(!groups.isEmpty()){
            Collection<CollectionTO> ctos = env.getPropertyValue(groups, propNames);
            
            List<CollectionTO> ctosList = new ArrayList<CollectionTO>(ctos);
            
            Collections.sort(ctosList, new Comparator<CollectionTO>(){
                                        @Override
                                        public int compare(CollectionTO cto1, CollectionTO cto2){
                                            int result = 0;
                                            try{
                                                String name1 = cto1.getPropValue("name").iterator().next().getValue().toString();
                                                String name2 = cto2.getPropValue("name").iterator().next().getValue().toString();
                                                result = name1.compareTo(name2);
                                            }catch(Exception e){
                                                LOG.error(e.getLocalizedMessage(), e);
                                            }
                                            return result;
                                        }
                            });
            
            for(CollectionTO cto: ctosList){
                Element groupelem = objectsDoc.createElement("group");			
                objectsDocRoot.appendChild((Node)groupelem);
                
                TO<?> group = cto.getObjId();
                groupelem.setAttribute("EnvObjId", TOFactory.getInstance().saveTO(group));
                if(addMembersNum){
                    groupelem.setAttribute("n_members", String.valueOf(env.getChildren(group).size()));
                }
                for(ValueTO vto : cto.getPropValues()){
                    String value = vto.getValue() == null ? "" : vto.getValue().toString();
                    groupelem.appendChild(XMLHelper.createXmlTextNode(objectsDoc, vto.getPropertyName(), value));
                }
            }
        }
    }
    
    private void createGroupTypeIfNotExist(HttpServletRequest request) throws EnvException{
        Environment env = Env.getEnv(request);
        ObjectType[] groupType = env.getObjectTypes(new String[]{EnvironmentTypes.TYPE_GROUP});
        
        if (groupType == null || groupType.length < 1)
        {
            PropertyDescr[] props = {
                    new PropertyDescr("name", String.class.getName()),
                    new PropertyDescr("desc", String.class.getName()),
                    new PropertyDescr("location", String.class.getName()),
                    new PropertyDescr("demandPower", Double.class.getName(), true),
                    new PropertyDescr("apPower", Double.class.getName(), true),
            };
            ObjectType newtype = env.createObjectType(EnvironmentTypes.TYPE_GROUP);

            for(int i = 0; i < props.length; i++){
                newtype.getPropertyDescriptors().add(props[i]);
            }
            env.updateObjectType(newtype);
        }
    }
    
    private Document getAffectedGroupObjects(HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, ParserConfigurationException {
        Document doc = XMLHelper.createNewXmlDocument("groupobjects");
        Node rootNode = doc.getDocumentElement();
        Node objRootNode = XMLHelper.appendNodeWithText(rootNode, "objects", null);

        String groupId = request.getParameter("groupId");
        TO<?> group = TOFactory.getInstance().loadTO(groupId);
        
        String[] rackIds = request.getParameter("rackIds").split(",");
        
        Environment env = Env.getEnv(request);
        
        Collection<TO<?>> allGroups = env.getObjectsByType(EnvironmentTypes.TYPE_GROUP);
        Map<TO<?>, String> groupNames = new HashMap<TO<?>, String>();
        
        if(!allGroups.isEmpty()){
            Collection<CollectionTO> allGroupsCto = env.getPropertyValue(allGroups, new String[]{"name"});
            for(CollectionTO cto : allGroupsCto){
                groupNames.put(cto.getObjId(), (String)cto.getSinglePropValue("name").getValue());
            }
        }
        
        for(String rackId : rackIds){
            TO<?> rack = TOFactory.getInstance().loadTO(rackId);
            addRackAssetToXml(rack, group, objRootNode, groupNames, env);
        }
        
        return doc;
    }
    
    private void addRackAssetToXml(TO<?> powerRack, TO<?> currentGroup, Node rootNode, Map<TO<?>, String> groupNames, Environment env) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        
        String powerRackName = env.getPropertyValue(powerRack, "name", String.class);
        String platform = env.getPropertyValue(powerRack, "platform", String.class);
        
        String childrenType = Constants.POWER_RACK_PLATFORM_SC.equalsIgnoreCase(platform) ? EnvironmentTypes.TYPE_SERVER : EnvironmentTypes.TYPE_RPDU;
        
        Collection<TO<?>> assets = env.getChildren(powerRack, childrenType);
        
        for(TO<?> asset : assets){
            String assetId = TOFactory.getInstance().saveTO(asset);
            String assetName = env.getPropertyValue(asset, "name", String.class);
            
            Collection<TO<?>> parentGroups = env.getParents(asset, EnvironmentTypes.TYPE_GROUP);			
            String inGroup = parentGroups.contains(currentGroup) ? "true" : "false";
            
            Node serverNode = XMLHelper.appendNodeWithText(rootNode, "object", null);
            
            XMLHelper.setNodeAttribute(serverNode, "name", assetName);
            XMLHelper.setNodeAttribute(serverNode, "id", assetId);
            XMLHelper.setNodeAttribute(serverNode, "pwrrackname", powerRackName);
            XMLHelper.setNodeAttribute(serverNode, "ingroup", inGroup);			
            XMLHelper.setNodeAttribute(serverNode, "parentgroups", getParentGroupNames(parentGroups, groupNames));
        }
    }
    
    private String getParentGroupNames(Collection<TO<?>> parentGroups, Map<TO<?>, String> groupNames){
        List<String> parentGroupNames = new ArrayList<String>();
        parentGroupNames.clear();
        for(TO<?> pGroup : parentGroups){
            String groupName = groupNames.get(pGroup);
            if(groupName != null){
                parentGroupNames.add(groupName);
            }
        }
        Collections.sort(parentGroupNames);
        
        String result = "";
        for(int i = 0; i < parentGroupNames.size(); i++){
            if(!result.isEmpty()){
                result += ", ";
            }
            result += parentGroupNames.get(i);
        }
        
        return result;
    }
    
    private String addSelectedGroupObjects(HttpServletRequest request) {
        String resultMessage = ErrorMessages.SUCCESS;
        Environment env = Env.getEnv(request);
        String groupId = request.getParameter("groupId");
        TO<?> group = TOFactory.getInstance().loadTO(groupId);
        
        try {
            JSONObject save = new JSONObject(request.getParameter("savejson"));
            JSONArray objects = null;
            
            List<Relation> relations = new ArrayList<Relation>();
            List<TO<?>> allObjectsToAdd = new ArrayList<TO<?>>();
            List<TO<?>> allObjectsToRemove = new ArrayList<TO<?>>();
            
            if(!save.isNull("objects")) {
                objects = save.getJSONArray("objects");
                for(int i = 0; i < objects.length(); i++) {
                    JSONObject object = objects.getJSONObject(i);
                    String serverId = object.getString("id");
                    boolean ingroup = "true".equals(object.getString("ingroup"));
                    TO<?> objTO = TOFactory.getInstance().loadTO(serverId);
                    Collection<TO<?>> parentGroups = env.getParents(objTO, EnvironmentTypes.TYPE_GROUP);
                    
                    if(ingroup) {
                        if(!parentGroups.contains(group)){
                            allObjectsToAdd.add(objTO);
                        }
                    } else {
                        if(parentGroups.contains(group)){
                            allObjectsToRemove.add(objTO);
                        }
                    }
                }
            }
            
            if(!allObjectsToAdd.isEmpty()) {
                relations.add(new Relation(group, allObjectsToAdd));
                env.setRelation(relations);
            }			
            if(!allObjectsToRemove.isEmpty()) {
                relations.clear();
                relations.add(new Relation(group, allObjectsToRemove));
                env.removeRelation(relations);
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            resultMessage = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        return resultMessage;
    }	
}
