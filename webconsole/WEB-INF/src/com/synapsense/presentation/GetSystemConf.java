package com.synapsense.presentation;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.lang.LangManager;

public class GetSystemConf extends BaseHttpServlet {

    private static final long serialVersionUID = -4127739442552604257L;
    private static final Log LOG = LogFactory.getLog(GetSystemConf.class);
    private ThreadLocal<LangManager> langManager = new ThreadLocal<LangManager>();

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        langManager.set(LangManager.getInstance());
        sendResponse(response, createConfDoc(request));
    }
    
    private Document createConfDoc (HttpServletRequest request) {
        Document document = null;
        try {
            document = XMLHelper.createNewXmlDocument("system");
            Element rootEl = document.getDocumentElement();
            Element configurationEl = document.createElement("configuration");
            rootEl.appendChild(configurationEl);
            
            addESNode(request,configurationEl);
            addCONTROLLER_STATUSNode(request, configurationEl);
            addWSNGATEWAYNode(request, configurationEl);
            addMODBUSGATEWAYNode(request, configurationEl);
            addSNMPAGENTNode(request, configurationEl);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return document; 
    }
    
    private void addSNMPAGENTNode (HttpServletRequest request, Node root) {
        String SNMPAGENT = "SNMPAGENT";
        addSystemPathNode(request, root, SNMPAGENT);
    }
    
    private void addWSNGATEWAYNode (HttpServletRequest request, Node root) {
        String WSNGATEWAY = "WSNGATEWAY";
        addSystemPathNode(request, root, WSNGATEWAY);
    }
    
    private void addSystemPathNode (HttpServletRequest request, Node root, String esTypeName) {
        Environment env = Env.getEnv(request);
        Collection<TO<?>> TOs = env.getObjectsByType(esTypeName);
        Document doc = root.getOwnerDocument();
        for (TO<?> to:TOs) {
            Element systemPathEl = doc.createElement("path");
            String name = "";
            String type = "";
            String address = "";
            try {
                name = env.getPropertyValue(to, "name", String.class);
                type = TypeUtil.getInstance().getDisplayableTypeName(esTypeName);
                LangManager lm = langManager.get();
                type = lm.getLocaleString(LangManager.getCurrentLocale(request), type);
                address = env.getPropertyValue(to, "address", String.class);
            } catch (UnableToConvertPropertyException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (PropertyNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (ObjectNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
            systemPathEl.setAttribute("name", name);
            systemPathEl.setAttribute("type", type);
            systemPathEl.setAttribute("address", address);
            root.appendChild(systemPathEl);
        }
    }
    
    private void addMODBUSGATEWAYNode (HttpServletRequest request, Node root) {
        String esTypeName = "MODBUSNETWORK";
        Environment env = Env.getEnv(request);
        Collection<TO<?>> TOs = env.getObjectsByType(esTypeName);
        Document doc = root.getOwnerDocument();
        for (TO<?> to:TOs) {
            Element systemPathEl = doc.createElement("path");
            String name = "";
            String type = "";
            String address = "";
            try {
                name = env.getPropertyValue(to, "name", String.class);
                type = TypeUtil.getInstance().getDisplayableTypeName(esTypeName);
                LangManager lm = langManager.get();
                type = lm.getLocaleString(LangManager.getCurrentLocale(request), type);
                address = env.getPropertyValue(to, "port", String.class);
                int startIndx = address.indexOf("TCP:");
                address = address.substring((startIndx!=-1?startIndx:0));
            } catch (UnableToConvertPropertyException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (PropertyNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (ObjectNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
            systemPathEl.setAttribute("name", name);
            systemPathEl.setAttribute("type", type);
            systemPathEl.setAttribute("address", address);
            root.appendChild(systemPathEl);
        }
    }
    
    private void addCONTROLLER_STATUSNode (HttpServletRequest request, Node root) {
        Environment env = Env.getEnv(request);
        Collection<TO<?>> TOs = env.getObjectsByType(EnvironmentTypes.TYPE_CONTROLLER_STATUS);
        Document doc = root.getOwnerDocument();
        for (TO<?> to:TOs) {
            Element systemPathEl = doc.createElement("path");
            String name = "";
            String type = "";
            String address = "";
            try {
                name = env.getPropertyValue(to, "name", String.class);
                type = TypeUtil.getInstance().getDisplayableTypeName(EnvironmentTypes.TYPE_CONTROLLER_STATUS);
                LangManager lm = langManager.get();
                type = lm.getLocaleString(LangManager.getCurrentLocale(request), type);
                InetAddress ipAddress = InetAddress.getByName(name);
                address = ipAddress.getHostAddress();
            } catch (UnableToConvertPropertyException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (PropertyNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (ObjectNotFoundException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } catch (UnknownHostException e) {
                LOG.warn("Couldn't resolve " + name + " to IP");
                address = name;
            }
            systemPathEl.setAttribute("name", name);
            systemPathEl.setAttribute("type", type);
            systemPathEl.setAttribute("address", address);
            root.appendChild(systemPathEl);
        }
    }
    
    private void addESNode (HttpServletRequest request, Node root) {
        //TODO
        //???
        Document doc = root.getOwnerDocument();
        Element systemPathEl = doc.createElement("path");
        LangManager lm = langManager.get();
        String name = lm.getLocaleString(LangManager.getCurrentLocale(request), "text/app_server");
        String type = lm.getLocaleString(LangManager.getCurrentLocale(request), "object_type_server");
        systemPathEl.setAttribute("name", name);
        systemPathEl.setAttribute("type", type);
        String address = "";
        try {
            address = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        systemPathEl.setAttribute("address", address);
        root.appendChild(systemPathEl);
    }
}