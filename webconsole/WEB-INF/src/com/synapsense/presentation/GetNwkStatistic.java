package com.synapsense.presentation;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.elementmodel.basic.WrapperModelElement;
import com.synapsense.elementmodel.es.ElementHelper;
import com.synapsense.nwkstatistic.NwkStatistic;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetNwkStatistic extends BaseHttpServlet{
    private static final long serialVersionUID = 8292792128097606156L;
    private static final Log LOG = LogFactory.getLog(GetNwkStatistic.class);
            
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
        try {
            String reqType = request.getParameter("reqType");
            boolean includeHistory = Boolean.valueOf(request.getParameter("includeHistory"));
            ModelElement modelElement = null;
            
            if("statistic".equals(reqType)){
                modelElement = getNwkStat(request, includeHistory);
            }
            
            sendResponse(request, response, modelElement);
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }
    
    private ModelElement getNwkStat(HttpServletRequest request, boolean includeHistory) throws ParserConfigurationException{
        ModelElement rootElem = new ModelElement("object");
        Environment env = Env.getEnv(request);
        Collection<TO<?>> nodes;
        
        Date start = new Date(Long.parseLong(request.getParameter("startDate")));
        Date end = new Date(Long.parseLong(request.getParameter("endDate")));
        boolean isAll = Boolean.valueOf(request.getParameter("allNodes"));
        
        
        try{
            if(isAll){
                TO<?> objId = TOFactory.getInstance().loadTO(request.getParameter("dcId"));
                nodes = env.getRelatedObjects(objId, EnvironmentTypes.TYPE_NODE, true);
            }else{
                String[] nodeIds = request.getParameter("nodeIds").split(",");
                nodes = new ArrayList<TO<?>>(nodeIds.length);
                for(String nodeId : nodeIds){
                    nodes.add(TOFactory.getInstance().loadTO(nodeId));
                }
            }		
            
            
            NumberFormat nf = CommonFunctions.getNumberFormat(request);
            
            for(TO<?> node : nodes){
                ModelElement nodeElem = new ModelElement("WSNNODE");
                nodeElem.addAttribute("id", TOFactory.getInstance().saveTO(node));
                nodeElem.addAttribute("type", node.getTypeName());
                
                nodeElem.addChild(ElementHelper.createModelElementWithValue("name", env.getPropertyValue(node, "name", String.class)));
                nodeElem.addChild(ElementHelper.createModelElementWithValue("mac", Long.toHexString(env.getPropertyValue(node, "mac", Long.class))));
                
                Collection <TO<?>> parents =  env.getParents(node, EnvironmentTypes.TYPE_NETWORK);
                
                if(!parents.isEmpty()){
                    nodeElem.addChild(ElementHelper.createModelElementWithValue("panId", env.getPropertyValue(parents.iterator().next(), "panId", Integer.class)));						
                }
                
                Double avgSlots = NwkStatistic.getNodeAverageSlotCount(node, start, end);
                if(avgSlots != null){
                    nodeElem.addChild(ElementHelper.createModelElementWithValue("avgSlotCount", nf.format(avgSlots)));
                }
                
                TO<?> route = env.getPropertyValue(node, "route", TO.class);
                if (route != null) {
                    try {
                        String routeName = env.getPropertyValue(route, "name", String.class);
                        nodeElem.addChild(ElementHelper.createModelElementWithValue("routeName", routeName));
                    } catch (Exception e) {
                        LOG.error(e.getMessage(), e);
                    }
                }
                
                if(includeHistory){
                    ModelElement statRootElem = new ModelElement("STATISTICS");
                    nodeElem.addChild(statRootElem);
                    statRootElem.addChild(new WrapperModelElement(NwkStatistic.getNodeStatistic(node, start, end)));
                }
                
                rootElem.addChild(nodeElem);
            }
            
        }catch(Exception e){
            LOG.error(e.getMessage(), e);
        }
        
        return rootElem;
    }
}
