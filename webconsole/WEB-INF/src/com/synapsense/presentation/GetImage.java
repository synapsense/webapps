package com.synapsense.presentation;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.utilities.BaseHttpServlet;


public class GetImage extends BaseHttpServlet {	
    private static final long serialVersionUID = 1L;
    private static final Log LOG = LogFactory.getLog(GetImage.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            String path = request.getParameter("path");
            LOG.info("GetImage: " + path);
            TO<?> to = TOFactory.getInstance().loadTO(path);
            getFromTO(request, response, to);
     }
    
    private void getFromTO(HttpServletRequest request, HttpServletResponse response, TO<?> to)
                    throws IOException, ServletException {
        Object imgSource = null;
        Long imageTimestamp = null;
        ValueTO imageVTO = null;
        byte [] imageRawData = null;
        
        try {
            String propName = request.getParameter("propName");
            if(propName == null || propName.isEmpty()) {
                propName = "image";
            }
            imageVTO = Env.getEnv(request).getPropertyValue(to, propName, ValueTO.class);            
            imgSource = imageVTO.getValue();

            if (imgSource != null) {
                imageRawData = ((BinaryData) imgSource).getValue();
                imageTimestamp = imageVTO.getTimeStamp();
            }
            
            if(imageRawData != null) {

                response.setHeader("Cache-Control", "public,must-revalidate");
                response.setDateHeader("Expires", 0);
                long isModifiedSince = request.getDateHeader("if-modified-since");
                if(isModifiedSince != -1) {
                    if(new Date(imageTimestamp).toString().equals(new Date(isModifiedSince).toString())) {
                        response.sendError(HttpServletResponse.SC_NOT_MODIFIED);
                        return;
                    }                
                }            
                response.setDateHeader("Last-Modified", imageTimestamp);

                ImageInputStream input = ImageIO.createImageInputStream(new ByteArrayInputStream(imageRawData));
                if (ImageIO.getImageReaders(input).hasNext()) {
                    String formatName = ImageIO.getImageReaders(input).next().getFormatName();                
                    String mimeType = getServletContext().getMimeType("." + formatName);
                    response.setContentType(mimeType);
                }
                response.setContentLength(imageRawData.length);
                response.getOutputStream().write(imageRawData);                
            } else {
                String requestUri = request.getRequestURI();                
                response.sendError(HttpServletResponse.SC_NOT_FOUND, requestUri);
            }
        } catch (ObjectNotFoundException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        } catch (PropertyNotFoundException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        } catch (UnableToConvertPropertyException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    } 
}
