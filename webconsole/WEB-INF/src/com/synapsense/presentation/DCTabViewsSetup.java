package com.synapsense.presentation;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class DCTabViewsSetup extends BaseHttpServlet {
    private static final long serialVersionUID = 3674014235261163532L;
    private static final Log LOG = LogFactory.getLog(DCTabViewsSetup.class);
    
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        LOG.info("In get");
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try
        {
            Document resultDoc = XMLHelper.createNewXmlDocument("dctabviewsetup");
            Node rootNode = resultDoc.getFirstChild();
            Node visibilityNode  = XMLHelper.createXmlTextNode(resultDoc, "visibility", null);
            rootNode.appendChild(visibilityNode);
            Node successNode = XMLHelper.createXmlTextNode(resultDoc, "success", "true");
            rootNode.appendChild(successNode);
            
            
            //Setup forms
            setupActiveControlTabView(visibilityNode, request);
            
            sendResponse(response, resultDoc);
        } catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
            sendResponse(response, "<dctabviewsetup><success>false</success></<dctabviewsetup>");
        }
    }
    
    protected void setTabVisibility(String tabID, boolean visible, Node visibilityNode) {
        Node tabVisibleNode = XMLHelper.createXmlTextNode(visibilityNode.getOwnerDocument(), tabID, visible ? "true" : "false");
        visibilityNode.appendChild(tabVisibleNode);
    }
    
    protected void setupActiveControlTabView(Node visibilityNode, HttpServletRequest request) {
        String dcID = request.getParameter("dcId");
        TO<?> dc = TOFactory.getInstance().loadTO(dcID);
        
        boolean res = false;
        try {
            Collection<TO<?>> collTO = Env.getEnv(request).getChildren(dc);
            for(TO<?> set : collTO) {
                if(EnvironmentTypes.TYPE_SET.equals(set.getTypeName())) {
                    res = true;
                    break;
                }
            }
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        setTabVisibility("activecontroltabview", res, visibilityNode);
    }

}
