package com.synapsense.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;

public class SetLanguage extends BaseHttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 4253611792677902684L;
    private static final Log LOG = LogFactory.getLog(SetLanguage.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        HttpSession session = req.getSession(false);
        if (session != null) {
            String lang = req.getParameter("lang");

            LOG.info("Setting  " + session.getAttribute(SessionVariableNames.SESSION_USER) + "'s language to " + lang);
            Env.setLanguage(req, lang);
        }
        sendResponse(resp, "<true/>");
    }

}
