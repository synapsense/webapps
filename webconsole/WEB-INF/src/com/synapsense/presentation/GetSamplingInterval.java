package com.synapsense.presentation;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.PropertyId;

public class GetSamplingInterval extends BaseHttpServlet {
    private static final long serialVersionUID = -6466254099029937168L;
    private static final Log LOG = LogFactory.getLog(GetSamplingInterval.class);    
    private static Integer interval = 5;
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        sendResponse(response, "<result> <interval v='" + getInterval(request) + "' /> </result>");
    }
    
    private static Integer getInterval(HttpServletRequest request){
        try{
            int minInterval = Integer.MAX_VALUE;
            PropertyId propertyId = null;
            Environment env = Env.getEnv(request);
            TO<?> sensor = null;
            
            Date startTime = new Date();                
            String [] selected_idsArray = request.getParameter("selected_ids").split(",");
            
            for(String objectId : selected_idsArray) {
                propertyId = PropertyId.fromString(objectId);                
                
                TO<?> to = TOFactory.getInstance().loadTO(propertyId.getObjectId());
                
                sensor = null;
                if(to.getTypeName().equals(EnvironmentTypes.TYPE_SENSOR)) {
                    sensor = to;
                } else {
                    Object obj = env.getPropertyValue(to, propertyId.getPropName(), Object.class);                    
                    if(obj instanceof TO<?> && ((TO<?>)obj).getTypeName().equals(EnvironmentTypes.TYPE_SENSOR)) {
                        sensor = (TO<?>)obj;
                    }
                }
                
                if(sensor != null) {
                    TO<?> node = env.getParents(sensor, EnvironmentTypes.TYPE_NODE).iterator().next();
                    String samplingStr = env.getPropertyValue(node, "period", String.class);
                    int mins = convertSamplingIntervalToMinutes(samplingStr);
                    if(minInterval > mins) {
                        minInterval = mins;
                    }
                }                    
            }
            
            if(minInterval != Integer.MAX_VALUE) {
                interval = minInterval;
            }
            LOG.info("Processing time delta: " + (new Date().getTime() - startTime.getTime()) + " ms. Interval = " + interval + " min.");
            
        }catch(Exception e){
            interval = 5;
            LOG.error(e.getLocalizedMessage(), e);
        }  
    
        return interval;
    }
    
    private static int convertSamplingIntervalToMinutes(String samplingIntStr){
        int result = 0;
        
        String []arr = samplingIntStr.split(" ");
        String units = arr[0];
        String freq = arr[1];
                
        result = Integer.parseInt(units);
        
        if(freq == "sec"){
            result = result / 60;
        }else if(freq == "hours"){
            result = result * 60;
        }
        
        return result;
    }
        
}
