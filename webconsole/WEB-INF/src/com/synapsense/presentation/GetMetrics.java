package com.synapsense.presentation;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.synapsense.config.UIConfig;
import com.synapsense.config.metrics.Category;
import com.synapsense.config.metrics.MetricsConfig;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.service.Environment;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ValueTO;
import com.synapsense.dto.TO;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.lang.LangManager;

public class GetMetrics extends BaseHttpServlet {	
    private static final long serialVersionUID = 8963518013716364233L;
    private static final Log LOG = LogFactory.getLog(GetMetrics.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
        try {
            sendResponse(response, getMetrics(request));
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
     }
    
   private Document getMetrics(HttpServletRequest request) throws NumberFormatException, ObjectNotFoundException, XPathExpressionException, ParserConfigurationException, SAXException, IOException {
        
        Environment env = Env.getEnv(request);
        String IdStr = request.getParameter("id");		
        
        LOG.info("Getting metrics for object with id = " + IdStr);
                
        Document result = XMLHelper.createNewXmlDocument("object");
        Element root = result.getDocumentElement();

        TO<?> obj = TOFactory.getInstance().loadTO(IdStr);
        
        MetricsConfig metricsConfig = UIConfig.getMetricsConfig();
        List<String> propNamesList = metricsConfig.getMetricsPropNames(obj.getTypeName().toUpperCase());
                
        String [] propNamesArray = propNamesList.toArray(new String[propNamesList.size()]);

        LOG.info("Metrics props names: " + propNamesList.toString());
        
        //wrap object to collection		
        Collection<TO<?>> objWrapper = new ArrayList<TO<?>>();
        objWrapper.add(obj);
        
        //get metric properties
        Collection<CollectionTO> metricProps = env.getPropertyValue(objWrapper, propNamesArray);
        NumberFormat numberFormatter = CommonFunctions.getNumberFormat(request);
        LangManager lm = LangManager.getInstance();

        if(metricProps != null && metricProps.iterator().hasNext()){
        
            CollectionTO propCollection = metricProps.iterator().next();
        
            List<Category> categories = metricsConfig.getCategories(obj.getTypeName().toUpperCase());
            
            for(int i = 0; i < categories.size(); i++){				
                Category currCategory = categories.get(i);
                
                Document categoryDoc = XMLHelper.createNewXmlDocument("category");
                Element categoryRoot = categoryDoc.getDocumentElement();
                                
                List<String> propNames = currCategory.getPropertyNames();
                for(int j = 0; j < propNames.size(); j++){
                    String propName = propNames.get(j);
                    
                    ValueTO vto = propCollection.getSinglePropValue(propName);
                    String valStr = getValueStr(vto, numberFormatter);
                    if(valStr != null){						
                        Element newel = XMLHelper.createXmlNodeWithValue(categoryDoc, propName, valStr);
                        Timestamp ts = new Timestamp(vto.getTimeStamp());
                        SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),UsersDataOptions.getTzIdFromRequest(request));
                        if(ts != null){				
                            newel.setAttribute("t", sdf.format(ts));
                            newel.setAttribute("lt", String.valueOf(vto.getTimeStamp()));
                        }
                        
                        String localeID = LangManager.getCurrentLocaleId(request);
                        
                        String mappedNameId = TypeUtil.getInstance().getDisplayablePropertyName(obj.getTypeName().toUpperCase(), propName);
                        String descrId = TypeUtil.getInstance().getPropertyDescr(obj.getTypeName(), propName);
                        
                        if (!lm.isPromptExists(localeID, descrId)) {
                            descrId = mappedNameId;
                        }
                        
                        //set displayable name to be able to set sort path in Laszlo
                        newel.setAttribute("dName", lm.getLocaleString(localeID, mappedNameId));						
                        newel.setAttribute("descr", descrId);
                        
                        String unitName = Env.getUnits(obj, propName, request);
                        if(unitName != null){
                            newel.setAttribute("u", unitName);
                        }						
                        
                        String isHistoric = EnvTypeUtil.getHistoricalPropertyNames(obj.getTypeName(), env).contains(propName) ? "true" : "false";
                        newel.setAttribute("ishistoric", isHistoric);
                        
                        categoryRoot.appendChild(newel);
                    }					
                }
                
                categoryRoot.setAttribute("name", currCategory.getCategoryName());
                categoryRoot.setAttribute("title", currCategory.getCategoryTitle().replace("/", "_"));
                
                root.appendChild(result.importNode(categoryRoot, true));
            }
        }				
        
        return result;
   }
   
   private String getValueStr(ValueTO value, NumberFormat numberFormatter){
       String result = null;	   
       if(value != null){
           Object val = value.getValue();
           if(val != null){
               if (val instanceof Number) {
                   result = numberFormatter.format(val);
               } else {
                   result = val.toString();
               }
           }
       }	   
        return result;
   }

}
