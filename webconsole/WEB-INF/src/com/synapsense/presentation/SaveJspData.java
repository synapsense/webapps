package com.synapsense.presentation;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.XMLHelper;

public class SaveJspData extends BaseHttpServlet {
    public static final long	serialVersionUID	= 0;

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException
    {
        String html = request.getParameter(RequestVariableNames.TABLE_HTML);
        String key = request.getParameter(RequestVariableNames.TABLE_KEY);

        request.getSession().setAttribute(SessionVariableNames.getDataVariableName(key.toString()), html);
        sendResponse(response, "<result>true</result>");
    }

}
