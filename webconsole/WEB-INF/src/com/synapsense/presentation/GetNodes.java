package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.nwkstatistic.NwkStatistic;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.EnvXMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

/**
 * Gets nodes and routes for DC
 * @author akhoroshenkih
 * @author abogachek
 *
 */
public class GetNodes extends BaseHttpServlet {
    private static final long serialVersionUID = 6534571788795112589L;
    private static final Log LOG = LogFactory.getLog(GetNodes.class);
    
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
    
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
        try {
            Document doc = null;
            String networkIdStr = request.getParameter("networkId");
            if(networkIdStr != null){
                doc = createRoutesXML(request);
            }else{
                doc = createNetworksDoc(request);
            }
            sendResponse(response, doc);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);    		
        }
     }
    
    private Document createNetworksDoc(HttpServletRequest request){
        Document doc = null;
        Element root = null;
        TO<?> selectedNwk = null;
        Environment env = Env.getEnv(request);
        
        String dcId = request.getParameter("selectedDC");
        String nodeId = request.getParameter("nodeId");

        try{			
            doc = XMLHelper.createNewXmlDocument("dc");
            root = doc.getDocumentElement();
            if(dcId != null) {
                root.setAttribute("name", dcId);
                
                // specific node is selected
                if (nodeId != null) {
                    selectedNwk = env.getParents(TOFactory.getInstance().loadTO(nodeId), EnvironmentTypes.TYPE_NETWORK).iterator().next();
                }
                
                String [] props = {"name", Constants.TYPE_NETWORK_PANID_PROPERTY};
                Collection<TO<?>> networks = env.getChildren(TOFactory.getInstance().loadTO(dcId), EnvironmentTypes.TYPE_NETWORK);
                Collection<CollectionTO> networkProps = env.getPropertyValue(networks, props);
                networkProps = CollectionTOSorter.sortObjects(networkProps);
                
                for(CollectionTO net : networkProps){
                    Element networkElem = doc.createElement("network");				
                    for(ValueTO v : net.getPropValues()){
                        if(Constants.TYPE_NETWORK_PANID_PROPERTY.equals(v.getPropertyName())){
                            //format panId into the HEX
                            networkElem.setAttribute(v.getPropertyName(), String.format("%X", v.getValue()));
                        }else{
                            networkElem.setAttribute(v.getPropertyName(), v.getValue().toString());
                        }
                    }		
                    networkElem.setAttribute(Constants.TYPE_NETWORK_ID_PROPERTY, net.getObjId().toString());
                    if ((selectedNwk != null) && (selectedNwk.equals(net.getObjId()))) {
                        networkElem.setAttribute("selected", "true");
                    }
                    root.appendChild(networkElem);
                }
            }
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        return doc;
    }

    /**
     * Creates main xml document
     * @param request
     * @return
     */
    private Document createRoutesXML(HttpServletRequest request){
        Environment env = Env.getEnv(request);
        Document doc = null;
        Element root = null;
        Integer hopLevelInt = 0;
        boolean exactHopLevel = false;
        boolean notCommunicatingOnly = false;
        
        // Read parameters from the request
        String IdStr = request.getParameter("id");
        String networkIdStr = request.getParameter("networkId");
                
        try {   		
            hopLevelInt = Integer.parseInt(request.getParameter("hopLevel"));
        } catch (NumberFormatException e){}
        
        exactHopLevel = Boolean.parseBoolean(request.getParameter("exactHopLevel"));
        notCommunicatingOnly = Boolean.parseBoolean(request.getParameter("notCommunicatingOnly"));
        
        try{
            // load DC and Network TOs
            TO<?> obj = TOFactory.getInstance().loadTO(IdStr);
            TO<?> nwk = TOFactory.getInstance().loadTO(networkIdStr);
            
            // read DC properties from the object model
            Collection<TO<?>> wrap = new ArrayList<TO<?>>(1);
            wrap.add(obj);
            String [] objP = {"x", "y", "width", "height"};
            Collection<CollectionTO> objProps = env.getPropertyValue(wrap, objP);
            
            // write object properties to the doc
            doc = XMLHelper.createNewXmlDocument("object");
            root = doc.getDocumentElement();
            
            EnvXMLHelper.writeIDs(obj, root);
            for(ValueTO c : objProps.iterator().next().getPropValues()){
                Element xmlNode = XMLHelper.createXmlNodeWithValue(doc, c.getPropertyName(), c.getValue().toString());
                root.appendChild(xmlNode);
            }
            
            // create document for nodes
            Element nodesRoot = doc.createElement("children");
            
            // get collection of Wireless nodes related to selected network
            Collection<TO<?>> nodes = env.getRelatedObjects(nwk, EnvironmentTypes.TYPE_NODE, true);
            
            // get collection of Gateways related to selected network
            Collection<TO<?>> gateways = env.getRelatedObjects(nwk, EnvironmentTypes.TYPE_GATEWAY, true);
            
            //add gateways to the xml document
            int warnings = 0;

            if(gateways != null && !gateways.isEmpty()) {
                String [] propNames = {"name", Constants.TYPE_GATEWAY_PHYSICAL_ID_PROPERTY, Constants.TYPE_GATEWAY_X_PROPERTY,
                        Constants.TYPE_GATEWAY_Y_PROPERTY, Constants.TYPE_GATEWAY_STATUS_PROPERTY, 
                        Constants.TYPE_GATEWAY_ADDRESS_PROPERTY, Constants.TYPE_GATEWAY_LOCATION_PROPERTY, "numAlerts"};
            
                Collection<CollectionTO> gatewayProps = env.getPropertyValue(gateways, propNames);

                for (CollectionTO col : gatewayProps) {
                    Element gatewayElem = doc.createElement(EnvironmentTypes.TYPE_GATEWAY);
                    TO<?> gateway = col.getObjId();
                    gatewayElem.setAttribute("id", TOFactory.getInstance().saveTO(gateway));
                    gatewayElem.setAttribute("type", gateway.getTypeName());
                    
                    List<ValueTO> gatewayVals = col.getPropValues();
                    for(ValueTO v : gatewayVals){
                        if(v.getValue() == null){
                            continue;
                        }					
                        String propName = v.getPropertyName();
                        
                        if (Constants.TYPE_GATEWAY_STATUS_PROPERTY.equals(propName)) {
                            int status = (Integer)v.getValue();
                            gatewayElem.setAttribute("alive", String.valueOf(status & Constants.GATEWAY_STATUS_ALIVE));
                            gatewayElem.setAttribute("master", String.valueOf(status & Constants.GATEWAY_STATUS_MASTER));
                            gatewayElem.setAttribute("sync", String.valueOf(status & Constants.GATEWAY_STATUS_SYNC));
                            
                            warnings += ((status & Constants.GATEWAY_STATUS_ALIVE) + (status & Constants.GATEWAY_STATUS_SYNC)) < 2 ? 1 : 0;				        
                       } else {
                            gatewayElem.setAttribute(propName, v.getValue().toString());
                       }
                    }

                    nodesRoot.appendChild(gatewayElem);
                }
            }
            
            
            HashMap<TO<?>, Integer> routes = new HashMap<TO<?>, Integer>();
            HashMap<TO<?>, TO<?>> links = new HashMap<TO<?>, TO<?>>();
            Stack<TO<?>> neighbors = new Stack<TO<?>>();
            // request required props for every node in the network
            Collection<CollectionTO> nodeProps = env.getPropertyValue(nodes, new String[] {"x", "y", "name", "status", "numAlerts", "mac", "location", "nodeAppVersion", "nodeNwkVersion", "nodeOSVersion", "route", "batteryOperated", "platformId"});
            
            for (CollectionTO col: nodeProps) {
                TO<?> node = col.getObjId();
                TO<?> parent = (TO<?>)col.getSinglePropValue("route").getValue();
                links.put(node, parent);
            }
            
            for (TO<?> node: links.keySet()) {
                if (routes.containsKey(node)) {
                    // node is already in the result collection
                    continue;
                } else {
                    TO<?> route = node;
                    
                    do { 
                        neighbors.push(route);
                        route = links.get(route);
                    } while ((neighbors.size() < 10) && route != null && !route.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_GATEWAY));

                    // add all nodes to result with corresponding hop count
                    int size = neighbors.size();
                    for (int i = 1; i <= size; i++) {
                        routes.put(neighbors.pop(), i);
                    }
                }
            }
            
            //add nodes and routes to the xml document
            for (CollectionTO col: nodeProps) {
                TO<?> node = col.getObjId();
                Integer nodeHopLevelInt = routes.get(node);
                if (nodeHopLevelInt == null) {
                    nodeHopLevelInt = 0;
                }
                if (exactHopLevel && (nodeHopLevelInt == hopLevelInt) ||
                        ((!exactHopLevel) && (nodeHopLevelInt <= hopLevelInt)) || 
                        hopLevelInt == 0) {
                    
                    Integer status = (Integer)col.getSinglePropValue("status").getValue();
                    // If node status is 0 increment total number of warnings
                    if (status == 0) warnings++;

                    if ((!notCommunicatingOnly) || (notCommunicatingOnly && status == 0)) {
                        // add current node data to the nodes doc
                        Element nodeElem = doc.createElement(EnvironmentTypes.TYPE_NODE);
                        nodeElem.setAttribute("id", TOFactory.getInstance().saveTO(node));
                        nodeElem.setAttribute("type", node.getTypeName());

                        //add property values
                        List<ValueTO> nodeVals = col.getPropValues();
                        for(ValueTO v : nodeVals){
                            if(v.getValue() == null){
                                continue;
                            }
                            String propName = v.getPropertyName();
                            String val = "";
                            if ("mac".equals(propName) ) {
                                val = Long.toHexString((Long) v.getValue()).toUpperCase();
                            } else {
                                val = v.getValue().toString();
                            }
                            nodeElem.setAttribute(propName, val);
                            if ("route".equals(propName)) {
                                if (((TO<?>)v.getValue()).getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_GATEWAY)) {
                                    nodeElem.setAttribute("isNeighborNode", "false");   	   					        
                                } else {
                                    nodeElem.setAttribute("isNeighborNode", "true");
                                }
                            }
                        }
                    
                        //add statistic
                        NwkStatistic.addStatisticAsChildXml(nodeElem, NwkStatistic.getNodeStatistic(node), request);
                        
                        nodesRoot.appendChild(nodeElem);
                    }
                }
            }
            
            root.appendChild(nodesRoot);

            //write information to the main doc (warnings, alerts, node number and so on)
            Element xmlNode = XMLHelper.createXmlNodeWithValue(doc, "nodesNumber", Integer.toString(nodes.size()));
            root.appendChild(xmlNode);
            xmlNode = XMLHelper.createXmlNodeWithValue(doc, "alertsNumber", Integer.toString(env.getPropertyValue(nwk, "numAlerts", Integer.class)));
            root.appendChild(xmlNode);
            xmlNode = XMLHelper.createXmlNodeWithValue(doc, "warningsNumber", Integer.toString((warnings)));
            root.appendChild(xmlNode);
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return doc;
    }
}
