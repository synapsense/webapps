package com.synapsense.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.utilities.BaseHttpServlet;

public class SetUnitsSystem extends BaseHttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 4253611792677902684L;
    private static final Log LOG = LogFactory.getLog(SetUnitsSystem.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String system = req.getParameter("system");

        LOG.info("Setting units system to " + system);
        Env.setUnitsSystem(system, req);

        sendResponse(resp, "<true/>");
    }

}
