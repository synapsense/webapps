package com.synapsense.presentation;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.lang.LangManager;

public class GetHistoricalDataForCO2Dlg extends BaseHttpServlet {
    private static final Log LOG = LogFactory
            .getLog(GetHistoricalDataForCO2Dlg.class);
    private static final long serialVersionUID = -3180377512280872671L;
    protected ThreadLocal<NumberFormat> intNumberFormatter = new ThreadLocal<NumberFormat>();
    protected ThreadLocal<NumberFormat> commonNumberFormatter = new ThreadLocal<NumberFormat>();
    
    private static final int conversionThreshold = 100000;

    private static enum UnitsEnum {
        COST,
        ENERGY,
        EMISSION
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        LOG.info("Start GetHistoricalDataForCO2Dlg servlet...");
        
        NumberFormat nf = CommonFunctions.getNumberFormat(request);
        nf.setMaximumFractionDigits(0);
        nf.setMinimumFractionDigits(0);
        intNumberFormatter.set(nf);
        
        commonNumberFormatter.set(CommonFunctions.getNumberFormat(request));
        
        String reqType = request.getParameter("requestType");
        String str = "<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>";
        if("getProperties".equals(reqType)){
            str = XMLHelper.getDocumentXml(createDoc(request));
        }
        else if("getHistoricalData".equals(reqType)){
            str = XMLHelper.getDocumentXml(createHistoricalDoc(request));
        }
        else if("setHistoricalData".equals(reqType)){
            String status = saveDoc(request);
            
            try {
                // wait 1 sec to let Batch Updater save values into database
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                LOG.info("GetHistoricalDataForCO2Dlg servlet InterruptedException ");
            }
            
            str = "<result>" + status + "</result>";
        }else{
            str = XMLHelper.getDocumentXml(createDoc(request));
        }
        
        sendResponse(response, str);
        LOG.info("Finish GetHistoricalDataForCO2Dlg servlet...");
    }

    private Document createDoc(HttpServletRequest request)
            throws ServletException, IOException {
        /////////////////////
        //TODO: aklyushin need code review.
        //////////////////////
        Environment env = Env.getEnv(request);
        Document document = null;
        String dcID = request.getParameter("dcID");
        try {
            document = XMLHelper.createNewXmlDocument("data");
            Element root = document.getDocumentElement();
            
            TO<?> dc = TOFactory.getInstance().loadTO(dcID);
            Collection<TO<?>> PUEs = env.getChildren(dc, "PUE");
            if (PUEs.isEmpty()) {
                LOG.info("There is no PUE object in the " + dcID);
                return document;
            }
            TO<?> pue = PUEs.iterator().next();
            
            //Create calendar and set time to the last second of the day
            //We set calendar's time to the last second of the last day of the current month
            //and move time back on 12 months. It is imply that calculation mechanism has set
            //a common value for every month at the last second of the last day of every month.
            //If not this will not work.
            //This algorithm is supplied by Dmitry Grudzinskiy [dmitryg@synapsense.com].
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.DATE, 1);
            cal.set(Calendar.AM_PM, Calendar.AM);
            cal.add(Calendar.MONTH, -11);
            
            Element energyUsageEl = document.createElement("energyUsage");
            root.appendChild(energyUsageEl);
            Element energyCostEl = document.createElement("energyCost");
            root.appendChild(energyCostEl);
            Element emissionEl = document.createElement("emission");
            root.appendChild(emissionEl);
            Double annualEU = 0.;
            Double annualEC = 0.;
            Double annualCO2 = 0.;
            //Last value is the last value in cache 
            ArrayList<Double> collection = new ArrayList<Double>(12*4);
            boolean useMwh = false;
            boolean use1000USD = false;
            boolean useKTonn = false;
            for (int i = 0; i < 12; i++) {
                Date start = cal.getTime();
                
                int month = cal.get(Calendar.MONTH);
                if (month == 0) {
                    collection.add(11.0);
                } else {
                    collection.add(new Double(month - 1));
                }

                Double energyUsage = getMonthValue(pue, "energyUsage", start, env);
                if (i != 0) {
                    annualEU += energyUsage;
                }
                collection.add(energyUsage);
                if (energyUsage >= conversionThreshold) {
                    useMwh = true;
                }
                
                Double energyCosts = getMonthValue(pue, "energyCost", start, env);
                if (i != 0) {
                    annualEC += energyCosts;
                }
                collection.add(energyCosts);
                //if (energyCosts < 1000 && energyCosts != 0.) {
                if (energyCosts >= conversionThreshold) {
                    use1000USD = true;
                }
                
                Double co2 = getMonthValue(pue, "emission", start, env);
                if (i != 0) {
                    annualCO2 += co2;
                }
                collection.add(co2);
                if (co2 >= conversionThreshold) {
                    useKTonn = true;
                }
                cal.add(Calendar.MONTH, 1);
            }

            String currentMonthStr = String.valueOf(cal.get(Calendar.MONTH) == 0 ? 11 : cal.get(Calendar.MONTH) - 1);
            Double currentEU = env.getPropertyValue(pue, "energyUsage", Double.class);
            if(currentEU == null){
                currentEU = 0.0;
            }
            useMwh = useMwh || currentEU >= conversionThreshold;
            annualEU += currentEU;
            Double currentEC = env.getPropertyValue(pue, "energyCost", Double.class);
            if(currentEC == null){
                currentEC = 0.0;
            }
            use1000USD = use1000USD || currentEC >= conversionThreshold;
            annualEC += currentEC;
            Double currentCO2 = env.getPropertyValue(pue, "emission", Double.class);
            if(currentCO2 == null){
                currentCO2 = 0.0;
            }
            useKTonn = useKTonn || currentCO2 >= conversionThreshold;
            annualCO2 += currentCO2;
            //Add historical months
            for (int i=0;i<collection.size();i+=4) {
                String currentMonthInt = String.valueOf(collection.get(i).intValue());
                Double energyUsege = collection.get(i+1);
                if (useMwh) {
                    energyUsege = energyUsege/1000;
                }
                addMonth(energyUsageEl, currentMonthInt, energyUsege);
                
                Double energyCosts = collection.get(i+2);
                if (use1000USD) {
                    energyCosts = energyCosts/1000;
                }
                addMonth(energyCostEl, currentMonthInt, energyCosts);
                
                Double co2 = collection.get(i+3);
                if (useKTonn) {
                    co2 = co2/1000;
                }
                addMonth(emissionEl, currentMonthInt, co2);
            }
            //Add current month
            if (useMwh) {
                addMonth(energyUsageEl, currentMonthStr, currentEU/1000);
            } else {
                addMonth(energyUsageEl, currentMonthStr, currentEU);
            }
            if (use1000USD) {
                addMonth(energyCostEl, currentMonthStr, currentEC/1000);
            } else {
                addMonth(energyCostEl, currentMonthStr, currentEC);
            }
            if (useKTonn) {
                addMonth(emissionEl, currentMonthStr, currentCO2/1000);
            } else {
                addMonth(emissionEl, currentMonthStr, currentCO2);
            }
            
            Currency currency = getCurrency(dc, request);
            energyCostEl.setAttribute("mainunit", currency.getSymbol());
            
            //Add annual values            
            energyUsageEl.setAttribute("annualunit", getDimension(annualEU, UnitsEnum.ENERGY, dc, request));
            energyUsageEl.setAttribute("annual", transformValue(annualEU, true));
            
            energyCostEl.setAttribute("annualunit", getDimension(annualEC, UnitsEnum.COST, dc, request));
            energyCostEl.setAttribute("annual", transformValue(annualEC, true));
            
            emissionEl.setAttribute("annualunit", getDimension(annualCO2, UnitsEnum.EMISSION, dc, request));
            emissionEl.setAttribute("annual",  transformValue(annualCO2, true));
            
            //Add units	        
            energyUsageEl.setAttribute("unit", getDimension(useMwh, UnitsEnum.ENERGY, dc, request));
            energyCostEl.setAttribute("unit", getDimension(use1000USD, UnitsEnum.COST, dc, request));
            emissionEl.setAttribute("unit", getDimension(useKTonn, UnitsEnum.EMISSION, dc, request));
            
            //add common properties
            addCommonInfoToDoc(dc, pue, root, request);
            return document;
        } catch (Exception e) {
            throw new ServletException(
                    "Unable to complete due to the following error: ", e);
        } 
            
        
    }
    
    // very ugly
    private Double getMonthValue(TO<?> pue, String property, Date date, Environment env) throws EnvException {
        Collection<ValueTO> startVals = env.getHistory(pue, property, date, 2, Double.class); 
        
        // Two values always
        if (startVals.size() != 2) {
            return 0.0;
        }

        // Second must be always zero
        ValueTO[] svArr = startVals.toArray(new ValueTO[startVals.size()]);
        if (!svArr[1].getValue().equals(0.0)) {
            return 0.0;
        }

        // Both from the same month
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int month = cal.get(Calendar.MONTH);
        for (ValueTO val : svArr) {
            cal.setTimeInMillis(val.getTimeStamp());
            if (month != cal.get(Calendar.MONTH)) {
                return 0.0;
            }
        }
        return (Double) svArr[0].getValue();
    }

    private Document createHistoricalDoc(HttpServletRequest request)
            throws ServletException, IOException {
        Environment env = Env.getEnv(request);
        Document document = null;
        String dcID = request.getParameter("dcID");
        try {
            document = XMLHelper.createNewXmlDocument("data");
            Element root = document.getDocumentElement();

            TO<?> dc = TOFactory.getInstance().loadTO(dcID);
            Collection<TO<?>> PUEs = env.getChildren(dc, "PUE");
            if (PUEs.isEmpty()) {
                LOG.info("There is no PUE object in the " + dcID);
                return document;
            }
            TO<?> pue = PUEs.iterator().next();

            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.DATE, 1);
            cal.set(Calendar.AM_PM, Calendar.AM);
            
            Element energyDataEl = document.createElement("energyData");

            Boolean isSet = false;
            for (int i = 0; i < 12; i++) {

                Date start = cal.getTime();			
                Integer month = cal.get(Calendar.MONTH);
                Integer year = cal.get(Calendar.YEAR);

                Double energyUsage = getMonthValue(pue, "energyUsage", start,
                        env);
                Double energyCosts = getMonthValue(pue, "energyCost", start,
                        env);
                energyCosts = energyCosts/1000;
                Double co2 = getMonthValue(pue, "emission", start, env);

                Element monthDataEl = document.createElement("month");
                monthDataEl.setAttribute("index", month.toString());
                monthDataEl.setAttribute("year", year.toString());
                monthDataEl.setAttribute("energyUsage",  transformValue(energyUsage, false));
                monthDataEl.setAttribute("energyCost",  transformValue(energyCosts, false));
                monthDataEl.setAttribute("emission",  transformValue(co2, false));
                energyDataEl.appendChild(monthDataEl);
                cal.add(Calendar.MONTH, -1);
                
            }
            cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.DATE, 1);
            cal.set(Calendar.AM_PM, Calendar.AM);
            Date enddate = cal.getTime();
            cal.add(Calendar.YEAR, -1);
            Date startdate = cal.getTime();

            Collection<ValueTO> energyVals = env.getHistory(pue, "energyUsage",
                    startdate, enddate, Double.class);
            energyVals.addAll(env.getHistory(pue, "energyCost", startdate,
                    enddate, Double.class));
            energyVals.addAll(env.getHistory(pue, "emission", startdate,
                    enddate, Double.class));
            if (energyVals.size() > 0) {
                isSet = true;
            }
            Element isSetEl = document.createElement("isHistoryDataSetted");
            isSetEl.setTextContent(isSet.toString());
            Element currency = document.createElement("currency");
            Currency cur = getCurrency(dc, request);
            currency.setTextContent(cur.getCurrencyCode());

            root.appendChild(energyDataEl);
            root.appendChild(isSetEl);
            root.appendChild(currency);
            return document;
        } catch (Exception e) {
            throw new ServletException(
                    "Unable to complete due to the following error: ", e);
        }

    }

    private String saveDoc(HttpServletRequest request) throws ServletException {
        String status = ErrorMessages.SUCCESS;
        Environment env = Env.getEnv(request);
        String dcID = request.getParameter("dcID");
        
        try {

            TO<?> dc = TOFactory.getInstance().loadTO(dcID);
            Collection<TO<?>> PUEs = env.getChildren(dc, "PUE");

            if (PUEs.isEmpty()) {
                LOG.info("There is no PUE object in the " + dc);
                status = ErrorMessages.OBJECT_NOT_FOUND;
                return status;
            }
            TO<?> pue = PUEs.iterator().next();
            Double emissionFactor = env.getPropertyValue(pue, "emissionFactor",
                    Double.class);
            String jsonString = request.getParameter("energyData");
            JSONArray jarr = new JSONArray(jsonString);
            Calendar cal = Calendar.getInstance();
            cal.set(Calendar.HOUR, 0);
            cal.set(Calendar.MINUTE, 1);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            cal.set(Calendar.DATE, 1);
            cal.set(Calendar.AM_PM, Calendar.AM);
            
            ArrayList<ValueTO> currentValues = getCurrentValues(pue,  env);
        
            
            for (int i = 0; i < jarr.length(); i++) {
                JSONObject jobj = jarr.getJSONObject(i);
                cal.set(Calendar.MONTH, jobj.getInt("index"));
                cal.set(Calendar.YEAR, jobj.getInt("year") );

                Date startDate = cal.getTime();
                Long time = startDate.getTime();
                
                ValueTO energyUsageVal = new ValueTO("energyUsage", jobj.getDouble("energyUsage"), time);
                ValueTO energyCostVal = new ValueTO("energyCost",jobj.getDouble("energyCost") * 1000, time);
                ValueTO emissionVal = new ValueTO("emission",jobj.getDouble("energyUsage") * emissionFactor / 1000,time);


                ValueTO[] vtos = new ValueTO[] { energyUsageVal, energyCostVal,
                        emissionVal };

                env.setAllPropertiesValues(pue, vtos);

                time = time + 10000L;

                energyUsageVal = new ValueTO("energyUsage", 0.0, time);
                energyCostVal = new ValueTO("energyCost", 0.0, time);
                emissionVal = new ValueTO("emission", 0.0, time);

                vtos = new ValueTO[] { energyUsageVal, energyCostVal,
                        emissionVal };

                env.setAllPropertiesValues(pue, vtos);
            }
            
            
            if(!currentValues.isEmpty()){
                /*
                 * Entering data for 12 months clears data for current month. (Bug #6820 comment 3)
                 * After script added all historical values, current values for all parameters are 0 (last added value for each properties)
                 * Thats why it's necessary to restore last value with same value and timestamp.
                 */
                env.setAllPropertiesValues(pue, currentValues.toArray(new ValueTO[1]));
            }
            
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }

        return status;
    }
    
    private ArrayList<ValueTO> getCurrentValues(TO<?> pue, Environment env){
        Collection<TO<?>> ctos = new ArrayList<TO<?>>();
        ctos.add(pue);
        Collection<CollectionTO> cto = env.getPropertyValue(ctos, new String[]{"energyUsage", "energyCost", "emission"});
        
        CollectionTO pueCto = cto.iterator().next();
    
        ArrayList<ValueTO> vtos = new ArrayList<ValueTO>();

        for(ValueTO val : pueCto.getPropValues()){
            if(val.getValue() != null && val.getTimeStamp() != 0){
                vtos.add(val);
            }
        }
        return vtos;
    }
    
    private void addCommonInfoToDoc(TO<?> dc, TO<?> pue, Element docRoot, HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        Environment env = Env.getEnv(request);
        
        Document document = docRoot.getOwnerDocument();
        Element commonEl = document.createElement("common");
        docRoot.appendChild(commonEl);
        
        Double estEmissionReduction = env.getPropertyValue(pue, "estEmissionReduction", Double.class);
        Double estCostReduction = env.getPropertyValue(pue, "estCostReduction", Double.class);
        
        Element el = XMLHelper.createXmlNodeWithValue(document, "estEmissionReduction", transformValue(estEmissionReduction, true));
        el.setAttribute("u", getDimension(estEmissionReduction, UnitsEnum.EMISSION, dc, request));
        commonEl.appendChild(el);
        
        el = XMLHelper.createXmlNodeWithValue(document, "estCostReduction", transformValue(estCostReduction, true));
        el.setAttribute("u", getDimension(estCostReduction, UnitsEnum.COST, dc, request));
        commonEl.appendChild(el);
    }
    
    private void addMonth (Element root,String monthIndex,Double value) {
        String valueStr;
        if (Math.abs(value) > 1) {
            valueStr = intNumberFormatter.get().format(value);
        }else{
            valueStr = commonNumberFormatter.get().format(value);
        }
        Element month = XMLHelper.createXmlNodeWithValue(root.getOwnerDocument(), "month", valueStr);
        month.setAttribute("index", monthIndex);
        root.appendChild(month);
    }
    
    private String getDimension(Double value, UnitsEnum unit, TO<?> dc, HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        Boolean isShort = false;
        if (value != null && Math.abs(value) >= conversionThreshold) {
            isShort = true;
        }		
        return getDimension(isShort, unit, dc, request);
    }
    
    private String getDimension(Boolean isShort, UnitsEnum unit, TO<?> dc, HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        LangManager lm = LangManager.getInstance();
        
        String result = "";
        if(UnitsEnum.COST == unit){
            Currency currency = getCurrency(dc, request);
            result = isShort ? "K " + currency.getCurrencyCode() : " " + currency.getCurrencyCode();
        }else if(UnitsEnum.EMISSION == unit){
            result = isShort ? "dimension/metric_kilotons" : "dimension/metric_tons";
        }else if(UnitsEnum.ENERGY == unit){
            result = isShort ? "dimension/mwh" : "dimension/kwh";
        }
        return lm.getLocaleString(LangManager.getCurrentLocale(request), result);
    }
    
    private Currency getCurrency(TO<?> dc, HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        String currValue = Env.getEnv(request).getPropertyValue(dc, "currency", String.class);
        currValue = (currValue == null) ? Currency.getInstance(Locale.getDefault()).getCurrencyCode() : currValue;
        return Currency.getInstance(currValue);
    }
    
    private String transformValue(Double value, Boolean toInt){
        value = convertValue(value);
        if(value == null){
            return "";
        }
        if(toInt){
            return formatValue(value);
        } else {
            return commonNumberFormatter.get().format(value);
        }
    }
    
    private Double convertValue(Double value){
        if(value == null){
            return value;
        }
        if (Math.abs(value) >= conversionThreshold) {
            value = value / 1000;
        }
        return value;
    }
    
    private String formatValue(Double value){
        String formattedValue = "";
        if (Math.abs(value) < 1) {
            formattedValue = commonNumberFormatter.get().format(value);
        }else{
            formattedValue = intNumberFormatter.get().format(value);
        }
        return formattedValue;
    }
}