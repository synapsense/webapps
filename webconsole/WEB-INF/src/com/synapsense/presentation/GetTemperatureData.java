package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

public class GetTemperatureData extends BaseHttpServlet {

    private static final long serialVersionUID = -557141344331541271L;
    private static final Log LOG = LogFactory.getLog(GetHistoricalDataForCO2Dlg.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response)	throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.info("Start GetTemperatureData servlet...");
        String str = XMLHelper.getDocumentXml(createDoc(request));
        sendResponse(response, str);
        LOG.info("Finish GetTemperatureData servlet...");
    }
        
    private ArrayList<Double> createPropValsList (HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        
        Environment env = Env.getEnv(request);
        ArrayList<Double> propVals = new ArrayList<Double>();
        
        String masterID = request.getParameter("masterId");
        if(masterID==null || masterID==""){
            return propVals;
        }
        String objType = request.getParameter("objType");
        if(objType==null || objType==""){
            return propVals;
        }
        String propName = request.getParameter("propName");
        if(propName==null || propName==""){
            return propVals;
        }
        
        TO<?> to = TOFactory.getInstance().loadTO(masterID);
        Collection <TO<?>> objects = env.getRelatedObjects(to, objType, true);
        Collection <CollectionTO> ctos = env.getPropertyValue(objects, new String[]{propName});
        
        Class<?> typeClass = env.getPropertyDescriptor(objType, propName).getType();		
        Double propVal=null;
        
        if (typeClass.equals(TO.class)){
            for (CollectionTO cto : ctos) {			
                TO<?> sensor = (TO<?>) cto.getSinglePropValue(propName).getValue();
                if(sensor != null){
                    try {	
                        propVal = env.getPropertyValue(sensor, "lastValue", Double.class);
                        addPropertyValue(propVal, propVals);
                    } catch (PropertyNotFoundException e) {
                        LOG.error(e.getLocalizedMessage(), e);
                        propVals.clear();
                        return propVals;
                    }
                }
            }
            return propVals;
        }
                
        if (Number.class.isAssignableFrom(typeClass)) {
            for (CollectionTO cto : ctos) {
                Object value = cto.getSinglePropValue(propName).getValue();
                if (value != null) {
                    propVal = ((Number) value).doubleValue();
                    addPropertyValue(propVal, propVals);
                }
            }
            return propVals;
        }
        
        return propVals;
    }
    
    private void addPropertyValue (Double propertyValue, ArrayList<Double> propertyValues){
        if(!(propertyValue == null || propertyValue == -2000 || propertyValue == -3000 || propertyValue == -5000)){
            propertyValues.add(propertyValue);
        }	
    }
    
    private Document createDoc(HttpServletRequest request) throws ServletException{
        Document document = null;

        try{
            document = XMLHelper.createNewXmlDocument("data");
        
            String strincr = request.getParameter("increment");
            if(strincr==null || strincr==""){
                return document;
            }
            String isMultOfIncr = request.getParameter("ismultofincrement");

            int increment = Integer.parseInt(strincr);
            
            if(increment <= 0){
                throw new ServletException("Unable to complete GetTemperatureData because of 'increment' <= 0");
            }		
                            
            ArrayList<Double> propVals = createPropValsList(request);

            if(propVals.isEmpty()){
                return document;
            }
                
            Element root = document.getDocumentElement();
            
            double min = propVals.get(0).doubleValue();				
            double max = min;
            
            for(Double prop: propVals ){
                min=Math.min(min, prop);
                max=Math.max(max, prop);
            }
            
            int startPoint = (int) Math.floor(min);
            
            if(isMultOfIncr!=null && isMultOfIncr.equals("true")){
                startPoint -= startPoint % increment;
            }
            
            int floorMax = (int) Math.floor(max);
            StringBuffer label= new StringBuffer();
            Element set=null;
            int count = 0;
            int newI = 0;
                        
            for(int i = startPoint; i <= floorMax; i+=increment){
                
                count = 0;
                newI = i + increment;
                
                label.setLength(0);
                
                for(Double prop: propVals ){
                    if((i<=prop) && (prop < newI)){
                        count ++;
                    }
                }
                
                set = document.createElement("set");
                label.append(i);

                if(increment!=1){
                    label.append("-");
                    label.append(newI-1);
                }
                
                set.setAttribute("label", label.toString());
                set.setAttribute("value", String.valueOf(count));
                root.appendChild(set);				
            }
            
        } catch (Exception e) {
            throw new ServletException("Unable to complete due to the following error: ", e);
        } 
    
        return document;
    }
        
}
