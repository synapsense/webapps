package com.synapsense.presentation;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.synapsense.dataAccess.Env;
import com.synapsense.service.DiagnosticService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

/**
 * @author akhoroshenkih
 */
public class DiagnosticPack extends BaseHttpServlet{
    private static final Log LOG = LogFactory.getLog(DiagnosticPack.class);
    private static final long serialVersionUID = -6402550337541675347L;
    private static final String INFO_FILE_NAME = "support-info.xml";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
        doPost(request,response);
    }
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException,ServletException {
        LOG.info("Started...");
        String reqType = req.getParameter("reqType");
        if("zipPackage".equals(reqType)){
            String fileName = getDisplayablePackageName(); //actually it is zip file (just disguise it by extension changing)
            resp.setContentType("APPLICATION/OCTET-STREAM");
            String disHeader = "Attachment; Filename=\"" + fileName + "\"";
            resp.setHeader("Content-Disposition", disHeader);
            
            ServletOutputStream os = resp.getOutputStream();
            DiagnosticService diagSrv = Env.getDiagnosticServ(req);
            String packId = diagSrv.generateDiagnosticPackage();            
            try{
                long offset = 0;
                byte data[];
                while ((data = diagSrv.readDiagnosticFileBytes(packId, offset)) != null){
                    offset += data.length;
                    os.write(data);
                }
            }catch(Exception e){
                LOG.error(e.getLocalizedMessage(), e);
            }finally{
                diagSrv.deletePackage(packId);
            }    		
        }else if("info".equals(reqType)){			
            Document infoDoc = XMLHelper.loadXmlFromByteArray(Env.getFileWritter().readFileBytes(INFO_FILE_NAME));
            String str = XMLHelper.getDocumentXml(infoDoc);
            sendResponse(resp, str);
        }
        LOG.info("Finished.");
    }
    
    private String getDisplayablePackageName(){
        String result = "pack";
        try{
            Calendar cal = Calendar.getInstance();
            cal.setTime(new Date());
            result = convertToStr(cal.get(Calendar.YEAR)) + convertToStr(cal.get(Calendar.MONTH) + 1)
                     + convertToStr(cal.get(Calendar.DAY_OF_MONTH)) + convertToStr(cal.get(Calendar.HOUR_OF_DAY))
                     + convertToStr(cal.get(Calendar.MINUTE));
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        result += ".ssp";
        return result;
    }
    
    private String convertToStr(int num){
        return num > 9 ? String.valueOf(num) : "0" + String.valueOf(num);
    }

}
