package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.dataconverters.old.DataConverter;
import com.synapsense.utilities.dataconverters.old.DataConverterCSV;
import com.synapsense.utilities.dataconverters.old.DataConverterHtmlTable;
import com.synapsense.utilities.dataconverters.old.XmlDataConverter;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.es.EnvHistoryUtil;
import com.synapsense.utilities.lang.LangManager;
import com.synapsense.utilities.sensors.SensorPoint;

public class GetPropertyData extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetPropertyData.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            convertData(request, response);
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }
    }

    private void convertData(HttpServletRequest request, HttpServletResponse response) throws IOException, ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, JSONException{
        
        Environment env = Env.getEnv(request);
        String objectIdsStr = request.getParameter("selectedDataRequests");		
        ArrayList<PropertyId> propertyIds = new ArrayList<PropertyId>();
        
        Collection<TO<?>> sensorIds = new HashSet<TO<?>> ();
                
        getObjectsIds(objectIdsStr, propertyIds, sensorIds);
        
        DataConverter converter;
        if (request.getParameter("mode") == null)
        {
            converter = new XmlDataConverter(response, request);
        }
        else if (request.getParameter("mode").equals("csv")){
            converter = new DataConverterCSV(response, request);
        }else{
            converter = new DataConverterHtmlTable(response, request);
        }						
                
        //set decimal digits
        converter.setDecimalDigits(UsersDataOptions.getDecimalDigits(request));
                
        //Make header
        converter.addElementToHeader("Object Name");
        converter.addElementToHeader("Data Type");
        converter.addElementToHeader("Channel");
        converter.addElementToHeader("Data Value");
        converter.addLastElementToHeader("Timestamp");
        
        //hack: we should add time in long to the xmlConverter..
        if(converter instanceof XmlDataConverter){
            converter.addElementToHeader("TimeInMs");
        }
        
        //Begin build
        converter.beginBuild();		
                
        //build for simple ids
        Collection<CollectionTO> vArr = new ArrayList<CollectionTO>();
        if(!sensorIds.isEmpty()){
            vArr = env.getPropertyValue(sensorIds, new String[] {"dataclass", "channel", "desc"});
        }
        for (CollectionTO v: vArr) {
            TO<?> sensor = v.getObjId();
                TO<?> node = env.getParents(sensor, EnvironmentTypes.TYPE_NODE).iterator().next();

                converter.clearConstantRowPart();
                
                //Make constant row part
                String description = TypeUtil.getInstance().getDisplayableDataclassName(String.valueOf(v.getSinglePropValue("dataclass").getValue()));
                String requestChannel = getPropertyValueStr("channel", v);
                
                converter.addElementToConstantRowPart(env.getPropertyValue(node, "name", String.class));
                converter.addElementToConstantRowPart(description);
                converter.addElementToConstantRowPart(requestChannel);
                    
                //set Map for values
                converter.clearElementMap();
                Integer dataclass = (Integer)v.getSinglePropValue("dataclass").getValue();
                
                switch (dataclass) {
                    case 211:
                        converter.addToElementMap("1.0", "Open");
                        converter.addToElementMap("0.0", "Closed");
                        break;
                    case 213: 
                        converter.addToElementMap("1.0", "Present");
                        converter.addToElementMap("0.0", "Absent");
                        break;
                    case 214:
                        converter.addToElementMap("1.0", "On");
                        converter.addToElementMap("0.0", "Off");
                }
            List<SensorPoint> points = getDataPoints(request, v.getObjId(), EnvironmentTypes.TYPE_SENSOR, "lastValue", false);
            converter.buildDataPart(points);						
        }
        
        LangManager lm = LangManager.getInstance();
        String langID = LangManager.getCurrentLocaleId(request);
        
        //build for wide ids		
        for (PropertyId propertyId : propertyIds){
            String description = "n/a";
            String requestChannel = "n/a";
            List<SensorPoint> points = new ArrayList<SensorPoint>();
            
                TO<?> obj = TOFactory.getInstance().loadTO(propertyId.getObjectId());
                converter.clearConstantRowPart();				
                
                if (obj != null) {				    
                    PropertyDescr propertyDescr = env.getObjectType((obj.getTypeName())).getPropertyDescriptor("name");
                    String objName = obj.getTypeName();
                    
                    if(propertyDescr != null) {
                        objName = env.getPropertyValue(obj, "name", String.class);
                    } 
                    converter.addElementToConstantRowPart(objName);
                    
                    points = getDataPoints(request, obj, propertyId.getObjectTO().getTypeName(), propertyId.getPropName(), false);
                    description = TypeUtil.getInstance().getDisplayablePropertyName(propertyId.getObjectTO().getTypeName(), propertyId.getPropName());
                    description = lm.getLocaleString(langID, description);
                    
                    converter.addElementToConstantRowPart(description);					
                    converter.addElementToConstantRowPart(requestChannel);
                    
                }else{
                    continue;
                }																				
                    
            converter.buildDataPart(points);		
        }

        //End build
        converter.endBuild();		
    }
        
    /**
     * @param requestIds
     * @return
     * Get properties and sensors
     * @throws JSONException 
     */
    public static void getObjectsIds(String requestIds, List<PropertyId> properties, Collection<TO<?>> sensors) throws JSONException {
        PropertyId[] propertyIds = PropertyId.arrayFromString(requestIds);
        
        for (PropertyId propertyId : propertyIds) {
            if (propertyId.getObjectTO().getTypeName().equals(EnvironmentTypes.TYPE_SENSOR)) {
                sensors.add(propertyId.getObjectTO());
            } else {
                properties.add(propertyId);
            }
        }
    }
    
    /**
     * 
     * @param request
     * @param objId
     * @param objType
     * @param propName
     * @return
     */
    private List<SensorPoint> getDataPoints(HttpServletRequest request,
            TO<?> object, String objType, String propName, boolean filtered) {
    
        Environment env = Env.getEnv(request);
        ArrayList<SensorPoint> points = new ArrayList<SensorPoint>();
                
        try {                                               
            //if property is not historical, it can be here only linked sensor
            if(!isHistorical(objType, propName, env) && !objType.equalsIgnoreCase(EnvironmentTypes.TYPE_SENSOR)){
                object = env.getPropertyValue(object, propName, TO.class); //object is sensorTO
                propName = "lastValue";
            }
            
            if(filtered){
                points = EnvHistoryUtil.getFilteredHistoricSensorPointsFromRequest(request, object, propName, env);
            }else{
                points = EnvHistoryUtil.getHistoricSensorPointsFromRequest(request, object, propName, env);
            }            
        } catch (Exception e) {         
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return points;
    }
        
   private boolean isHistorical(String type , String propName, Environment env){
       return EnvTypeUtil.getHistoricalPropertyNames(type, env).contains(propName);       
   }
   
   private String getPropertyValueStr(String propertyName, CollectionTO props){
       String result = "n/a";
       ValueTO vto = props.getSinglePropValue(propertyName);
       if(vto != null){
           Object value = vto.getValue();
           if(value != null){
               result = value.toString();
           }
       }
       return result;
   }
}
