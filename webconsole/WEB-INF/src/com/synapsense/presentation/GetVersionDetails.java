package com.synapsense.presentation;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;



public class GetVersionDetails extends BaseHttpServlet
{
    public static final long	serialVersionUID	= 0;
    private static final Log LOG = LogFactory.getLog(GetVersionDetails.class);

    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException
    {
        sendResponse(response, getAppVersionsDoc(request));
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException
    {
        doGet(request,response);
    }

    private Document getAppVersionsDoc(HttpServletRequest request)
    {
        Document applicationDoc = null;
        try {
            applicationDoc  = XMLHelper.createNewXmlDocument("application");
            Element rootNode = applicationDoc.getDocumentElement();
            
            String appVer = getServletContext().getInitParameter("ApplicationVersion");
            String appRev= getServletContext().getInitParameter("ApplicationRevision");
            String acVersionStr = getACVersion(request);
            
            Node verNode = XMLHelper.createXmlTextNode(applicationDoc, "appVersion", appVer);
            Node revNode = XMLHelper.createXmlTextNode(applicationDoc, "appRevision", appRev);
            Node acVerNode = XMLHelper.createXmlTextNode(applicationDoc, "acVersion", acVersionStr);
            
            rootNode.appendChild(verNode);
            rootNode.appendChild(revNode);
            rootNode.appendChild(acVerNode);
            
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return applicationDoc;
        
    }

    private String getACVersion(HttpServletRequest request)
    {		
        String acProductVersion = "";
        
        HttpSession session = request.getSession(false);
        String app_name = session.getServletContext().getInitParameter("APP_NAME");
        if(session == null || (session != null && session.getAttribute(SessionVariableNames.SESSION_USER) == null)){
            return acProductVersion;
        }
        
        Environment env = Env.getEnv(request);
        
        Collection<TO<?>> acObjects = env.getObjectsByType(EnvironmentTypes.TYPE_CONTROLLER_STATUS);
        if ((acObjects != null) && (acObjects.size()>0)) {
            TO<?> to = acObjects.iterator().next();
            try {
                acProductVersion = env.getPropertyValue(to, "productVersion", String.class);
            } catch (ObjectNotFoundException e) {
                LOG.error("Controller status object is not found. Can not determine Active Control Version.");
            } catch (PropertyNotFoundException e) {
                LOG.error("Active Control Version is unknown.");
            } catch (UnableToConvertPropertyException e) {
                LOG.error("Active Control Version: Unable to convert exception.");
            }
        }

        return acProductVersion;
    }
}
