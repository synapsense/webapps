package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Locale.Category;
import java.util.MissingResourceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.lang.LangManager;

public class GetLocales extends BaseHttpServlet {
    public static final long	serialVersionUID	= 0;
    private static final Log log = LogFactory.getLog(GetLocales.class);
    
    public void doGet(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        String mode = request.getParameter("mode");
        
        Document countriesDoc = ("country".equals(mode)) ? getCountriesDocument(request) : getCurrencyDocument(request);
        
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 1);
        response.setDateHeader("Last-modified", System.currentTimeMillis());

        if(mode!=null) {
            getXmlFormattedCountries(request,response, countriesDoc);
        }
    }
    
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException {
        doGet(request,response);
    }
    
    /*
     *  Method to get xml data 
     * 
     */
    private void getXmlFormattedCountries(HttpServletRequest request,HttpServletResponse response, Document countriesDoc) throws IOException,ServletException{
        sendResponse(response, countriesDoc);
        if (countriesDoc != null) {
            log.debug(XMLHelper.getDocumentXml(countriesDoc));
        }
    }
    
    private Document getCountriesDocument(HttpServletRequest request) {
        Locale loc = Locale.getDefault(Category.FORMAT);
        Locale[] locs = Locale.getAvailableLocales();
                
        Document countriesDoc = null;
        
        try {
            countriesDoc = XMLHelper.createNewXmlDocument("countries");
            Element root = countriesDoc.getDocumentElement();
            HashMap<String, String> countries = new HashMap<String, String>();
            List<String> sortedList = new ArrayList<String>();
            Locale selLoc = Locale.US;
            
            Locale currLocale = LangManager.getCurrentLocale(request);
            String currCountry = currLocale.getCountry();
            String currLang = currLocale.getLanguage();
            
            for(Locale l : locs) {
                if(l.getCountry() == "") {
                    continue;
                }
                if (currCountry.equals(l.getCountry()) && currLang.equals(l.getLanguage())) {
                    selLoc = l;
                    break;
                }				
            }			
            for(Locale l : locs) {
                if(l.getCountry() == "") {
                    continue;
                }				
                try {
                    countries.put(l.getDisplayCountry(selLoc), l.getISO3Country());
                } catch (MissingResourceException e) {
                    //ignore, there is no  3-letter country code for some locales
                }				
            }
            
            sortedList.addAll(countries.keySet());			
            Collections.sort(sortedList);
            
            for(String s : sortedList) {
                Node recordNode = countriesDoc.createElement("country");
                
                Node countryName = XMLHelper.createXmlTextNode(countriesDoc, "name", s);
                recordNode.appendChild(countryName);
                Node isCurrent = XMLHelper.createXmlTextNode(countriesDoc, "isCurrent", (loc.getISO3Country().equals(countries.get(s))) ? "true" : "false");
                recordNode.appendChild(isCurrent);
                Node countryCode = XMLHelper.createXmlTextNode(countriesDoc, "codeISO3", countries.get(s));
                recordNode.appendChild(countryCode);
                root.appendChild(recordNode);
            }
        } catch (ParserConfigurationException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return countriesDoc;
    }
    
    private Document getCurrencyDocument(HttpServletRequest request) {
        Locale loc = Locale.getDefault(Category.FORMAT);
        Locale[] locs = Locale.getAvailableLocales();
        
        Document currenciesDoc = null;
        
        try {
            currenciesDoc = XMLHelper.createNewXmlDocument("currencies");
            Element root = currenciesDoc.getDocumentElement();
            HashMap<String, String> curr = new HashMap<String, String>();
            List<String> sortedList = new ArrayList<String>();
            
            for(Locale l : locs) {
                if(l.getCountry() == "") {
                    continue;
                }
                
                Currency cur = Currency.getInstance(l);
                curr.put(cur.getCurrencyCode(), cur.getSymbol(l));
            }
            
            for(String l : curr.keySet()) {
                sortedList.add(l);
            }
            
            Collections.sort(sortedList);
            
            for(String s : sortedList) {
                Node recordNode = currenciesDoc.createElement("currency");
                
                Node currencyName = XMLHelper.createXmlTextNode(currenciesDoc, "name", s);
                recordNode.appendChild(currencyName);
                Node isCurrent = XMLHelper.createXmlTextNode(currenciesDoc, "isCurrent", (Currency.getInstance(loc).getCurrencyCode().equals(s)) ? "true" : "false");
                recordNode.appendChild(isCurrent);
                Node currencySymbol = XMLHelper.createXmlTextNode(currenciesDoc, "symbol", curr.get(s));
                recordNode.appendChild(currencySymbol);
                root.appendChild(recordNode);
            }
        } catch (ParserConfigurationException e) {
            log.error(e.getLocalizedMessage(), e);
        }
        return currenciesDoc;
    }
    
}
