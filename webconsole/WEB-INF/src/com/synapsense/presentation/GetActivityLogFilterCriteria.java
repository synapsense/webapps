/**
 * 
 */
package com.synapsense.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.Translator;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.ActivityLogConstans;

/**
 * @author dinniss
 */
public class GetActivityLogFilterCriteria extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetActivityLogFilterCriteria.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Document filterDoc = getActivityFilters(request);
        sendResponse(response, filterDoc);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    private Document getActivityFilters(HttpServletRequest request) {
        Document filterDoc = null;

        String userName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> user = Env.getUserDAO().getUser(userName);
        Translator translator = new Translator();
        translator.setLanguage(UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));

        try {
            filterDoc = XMLHelper.createNewXmlDocument("activity_filters");
            Element filterRoot = filterDoc.getDocumentElement();

            // Get the modules
            Document moduleDoc = getActivityModules(translator);
            Element moduleRoot = moduleDoc.getDocumentElement();

            // Get the actions
            Document actionDoc = getActivityActions(translator);
            Element actionRoot = actionDoc.getDocumentElement();

            // Get the users
            Document usersDoc = getAllUsers(request);
            Element usersRoot = usersDoc.getDocumentElement();

            filterRoot.appendChild(filterDoc.importNode(moduleRoot, true));
            filterRoot.appendChild(filterDoc.importNode(actionRoot, true));
            filterRoot.appendChild(filterDoc.importNode(usersRoot, true));

        } catch (ParserConfigurationException pcE) {
            LOG.error(pcE.getLocalizedMessage(), pcE);
        }

        return filterDoc;
    }

    /**
     * Provides an Xml document with the available activity modules
     * 
     * @param request
     * @return
     */
    public static Document getActivityModules(Translator translator) {
        Document modulesDoc = null;

        try {
            modulesDoc = XMLHelper.createNewXmlDocument("activity_modules");
            Element modulesRoot = modulesDoc.getDocumentElement();

            List<String> modules = Env.getActivityLogServ().getActivityModules();

            if (!modules.contains(ActivityLogConstans.MODULE_ANNOTATION)) {
                modules.add(ActivityLogConstans.MODULE_ANNOTATION);
            }

            Collections.sort(modules);

            for (String module : modules) {
                Element currModElem = modulesDoc.createElement("module");
                Node nameNode = XMLHelper.createXmlTextNode(modulesDoc, "name", translator.translate(module));

                // set value for datacombobox of laszlo console
                ((Element) nameNode).setAttribute("value", module);

                currModElem.appendChild(nameNode);
                modulesRoot.appendChild(currModElem);
            }

        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return modulesDoc;
    }

    /**
     * Provides an Xml document with the available activity actions
     * 
     * @param request
     * @return
     */
    public static Document getActivityActions(Translator translator) {
        Document modulesDoc = null;

        try {
            modulesDoc = XMLHelper.createNewXmlDocument("activity_actions");
            Element modulesRoot = modulesDoc.getDocumentElement();

            List<String> actions = Env.getActivityLogServ().getActivityActions();

            Collections.sort(actions);

            for (String action : actions) {
                Element currModElem = modulesDoc.createElement("action");
                Node nameNode = XMLHelper.createXmlTextNode(modulesDoc, "name", translator.translate(action));

                // set value for datacombobox of laszlo console
                ((Element) nameNode).setAttribute("value", action);

                currModElem.appendChild(nameNode);
                modulesRoot.appendChild(currModElem);
            }

        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return modulesDoc;
    }

    /**
     * Retrieve all users
     * 
     * @param request
     * @return
     */
    private Document getAllUsers(HttpServletRequest request) {
        Document usersDoc = null;

        try {

            usersDoc = XMLHelper.createNewXmlDocument("users");
            Element usersRoot = usersDoc.getDocumentElement();

            List<TO<?>> users = new ArrayList<>(Env.getUserDAO().getAllUsers());

            Collections.sort(users, new Comparator<TO<?>>() {
                @Override
                public int compare(TO<?> user1, TO<?> user2) {
                    String userName1 = (String) Env.getUserDAO().getProperty(user1, UserManagementService.USER_NAME);
                    String userName2 = (String) Env.getUserDAO().getProperty(user2, UserManagementService.USER_NAME);
                    return userName1.compareTo(userName2);
                }
            });

            // hard code: add "anonymous" user
            // (bug#4768)
            addUserNode(usersDoc, usersRoot, "anonymous");

            for (TO<?> user : users) {
                String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
                addUserNode(usersDoc, usersRoot, userName);
            }

            if (usersDoc != null) {
                LOG.debug(XMLHelper.getDocumentXml(usersDoc));
            }
        } catch (ParserConfigurationException pcE) {
            LOG.error(pcE.getMessage());
        }

        return usersDoc;
    }
    
    private static final void addUserNode(Document usersDoc, Node usersRoot, String userName) {
        Node userNode = usersDoc.createElement("user");

        Node userNameNode = XMLHelper.createXmlTextNode(usersDoc, "userName", userName);

        // set value for datacombobox of laszlo console
        ((Element) userNameNode).setAttribute("value", userName);

        userNode.appendChild(userNameNode);
        usersRoot.appendChild(userNode);
    }
}
