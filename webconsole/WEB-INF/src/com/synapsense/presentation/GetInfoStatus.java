package com.synapsense.presentation;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.audit.Auditor;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetInfoStatus extends BaseHttpServlet {
    private static final long serialVersionUID = -4654821855497016929L;
    private static final Log LOG = LogFactory.getLog(GetInfoStatus.class);
    private final String REAL_LI_SEVICE_NAME = "LiveImagingAuditor";
    private final String REAL_DM_SEVICE_NAME = "RmiServiceAuditor";
    private final String REAL_CP_SEVICE_NAME = "ControlProcessAuditor";
    private static final long CP_UPDATE_PERIOD = 2 * 60 * 1000;

    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        doGet(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws IOException, ServletException {
        try {
            Document infoDoc = XMLHelper.createNewXmlDocument("INFO");
            Node root = infoDoc.getFirstChild();
            //root.appendChild(getCountDCNode(request, infoDoc));
            root.appendChild(getInfoNode(request, infoDoc));
            root.appendChild(getStatusNode(request, infoDoc));
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires",1);
            response.setDateHeader("Last-modified", System.currentTimeMillis());
            sendResponse(response, infoDoc);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    private Element getACStatusNode(HttpServletRequest req, Document doc) {
        Element acStatusNode = null;
        //We want to show our own message for CP 
        // let's check if we have any CONTROLLER_STATUS objects in the model
        Environment env = Env.getEnv(req);
        Collection<TO<?>> controllers = env.getObjectsByType(EnvironmentTypes.TYPE_CONTROLLER_STATUS);
        if (controllers.size() > 1 ) {
            acStatusNode = doc.createElement("StatusRow");
            acStatusNode.setAttribute("name", "text_control_process");

            int numControllersOffline = 0;
            int index = 0;
            // check statuses of all 'controller status' objects
            Collection<CollectionTO> properties = env.getPropertyValue(controllers, new String[] { "name", "active" });
            for (CollectionTO cto : properties) {
                String host = (String) cto.getSinglePropValue("name").getValue();
                ValueTO vto = cto.getSinglePropValue("active");
                boolean online = ((Long) vto.getValue() == 1) ? true : false;
                String active = activeToStr(online);
                
                if (online == false) {
                    numControllersOffline++;
                }
                
                acStatusNode.setAttribute("online" + index, Boolean.toString(online));                        		
                acStatusNode.setAttribute("host" + index, host);                        		
                acStatusNode.setAttribute("active" + index, active);                        		

                index++;
            }
            if (numControllersOffline == 0) {
                acStatusNode.setAttribute("status", "text/app_status/running");
            } else if (numControllersOffline == index) {
                acStatusNode.setAttribute("status", "text/app_status/stopped");                        		
            } else {
                acStatusNode.setAttribute("status", "text/app_status/partial");
            }
        }
        
        return acStatusNode;
    }
    
    private Element getStatusNode (HttpServletRequest req,Document doc) {
        Element statusSetNode = doc.createElement("StatusSet");
        Boolean displayServerStatus = true;

        //Retrieving service's status
        AuditorSvc auditorSvc = Env.getAuditorSvc();
        Map<String,Auditor> map = auditorSvc.getAuditors();
        for (Auditor auditor:map.values()) {
            Element statusRowNode = doc.createElement("StatusRow");
            String name = auditor.getName();
            
            String[] nameArr = name.split("\\.");
            String mapSeviceName = null;
            String realServiceName = nameArr[nameArr.length-1];
            if (REAL_LI_SEVICE_NAME.equals(realServiceName)) {
                mapSeviceName = "text_imaging_server";
            } else if (REAL_DM_SEVICE_NAME.equals(realServiceName)) {
                mapSeviceName = "text_device_manager";
            }
            
            if (mapSeviceName != null) {
                statusRowNode.setAttribute("name", mapSeviceName);
                if(REAL_DM_SEVICE_NAME.equals(realServiceName) && auditor.getLastStatus().equals("OK")) {
                    // Gathering the hostname of service
                    String configuration = auditor.getConfiguration();
                    configuration = configuration.replace("http://", "");
                    configuration = configuration.split("/")[0];

                    statusRowNode.setAttribute("configuration", configuration);
                }
                
                if (auditor.getLastStatus().equals("OK")) {
                    statusRowNode.setAttribute("status", "text_app_status_running");
                }
                else if (auditor.getLastStatus().equalsIgnoreCase("undefined")) {
                    statusRowNode.setAttribute("status", "text_app_status_unknown");
                }
                else {
                    statusRowNode.setAttribute("status", "text_app_status_stopped");
                }
                statusSetNode.appendChild(statusRowNode);
            }
        }

        // Check for AC status
        Element acStatusNode = this.getACStatusNode(req, doc);
        if (acStatusNode != null) {
            statusSetNode.appendChild(acStatusNode);
        }
        
        // Only add Application Server status if Active Control is not configured
        if (acStatusNode == null) {
            // Application server is hardcoded in the assumption that console runs in the same JVM
            Element firstStatusRowNode = doc.createElement("StatusRow");
            firstStatusRowNode.setAttribute("name", "text_environmental_server");
            firstStatusRowNode.setAttribute("status", "text_app_status_running");
            statusSetNode.appendChild(firstStatusRowNode);
        }

        return statusSetNode;
    }

    private String activeToStr(boolean active) {
        if (active == true)
            return "online";
        return "offline";
    }

    private Element getInfoNode (HttpServletRequest req, Document doc) {
        Environment env = Env.getEnv(req);
        Collection<TO<?>> root = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
        //Retrieving alert's counters
        AlertService alertService = Env.getAlertingServ(req);
        
        Integer totalAlerts = 0;
        
        if (!root.isEmpty()) {
            totalAlerts += alertService.getNumActiveAlerts(root.iterator().next(),true);
        }
        totalAlerts += alertService.getNumActiveAlerts(TOFactory.EMPTY_TO,true);
        
        Integer countOpenedAlerts = 0;
        AlertFilterBuilder afb = new AlertFilterBuilder();
        afb.opened();
        countOpenedAlerts += alertService.getNumAlerts(afb.build());
        
        //Gather warnings number
        int totalWarnings = 0;
        try{
            Collection<TO<?>> allNodes = env.getObjectsByType(EnvironmentTypes.TYPE_NODE);
            if (!allNodes.isEmpty()) {
                String [] nodesProps = {"status"};        	
                Collection<CollectionTO> props = env.getPropertyValue(allNodes, nodesProps);
                for(CollectionTO p : props){
                    ValueTO v = p.getPropValues().get(0);
                    if(v != null){
                        Integer status = (Integer)v.getValue();
                        if(status != null && (status == 0)){
                            totalWarnings++;
                        }
                    }
                }
            }

            Collection<TO<?>> allGateways = env.getObjectsByType(EnvironmentTypes.TYPE_GATEWAY);
            if (!allGateways.isEmpty()) {
                String [] gatewayProps = {"status"};        	
                Collection<CollectionTO> gwProps = env.getPropertyValue(allGateways, gatewayProps);
                for(CollectionTO p : gwProps){
                    ValueTO v = p.getPropValues().get(0);
                    if(v != null){
                        Integer status = (Integer)v.getValue();
                        if(status != null){
                            totalWarnings += ((status & Constants.GATEWAY_STATUS_ALIVE) + (status & Constants.GATEWAY_STATUS_SYNC)) < 2 ? 1 : 0;
                        }
                    }
                }
            }
        }catch(Exception e){
            LOG.error("Exeption during warning number retrieving.");
            LOG.error(e.getLocalizedMessage(), e);
        }

        //Add Info
        Element infoSetNode = doc.createElement("InfoSet");
        Element infoRowNode = doc.createElement("InfoRow");
        infoRowNode.setAttribute("text", "Active Alerts");
        Element numAlerts = doc.createElement("Alerts");
        numAlerts.setAttribute("v", Integer.toString(totalAlerts));
        infoRowNode.appendChild(numAlerts);
        Element numOpenedAlerts = doc.createElement("OpenedAlerts");
        numOpenedAlerts.setAttribute("v", Integer.toString(countOpenedAlerts));
        infoRowNode.appendChild(numOpenedAlerts);
        Element numWarnings = doc.createElement("Warnings");
        numWarnings.setAttribute("v", Integer.toString(totalWarnings));
        infoRowNode.appendChild(numWarnings);

        infoSetNode.appendChild(infoRowNode);
        return infoSetNode;
    }
}
