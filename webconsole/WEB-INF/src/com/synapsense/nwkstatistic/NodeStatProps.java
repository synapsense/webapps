package com.synapsense.nwkstatistic;

public class NodeStatProps {
    public static final String WRONG_SLOT_COUNT = "wrongSlotCount";
    public static final String NWK_TIMESTAMP = "timestamp";
    public static final String NUMBER_ONE_HOP_RETR = "numberOneHopRetr";
    public static final String RADIO_ON_TIME = "radioOnTime";
    public static final String FREE_SLOT_COUNT = "freeSlotCount";
    public static final String TOTAL_SLOT_COUNT = "totalSlotCount";
    public static final String TIME_SYNC_LOST_TIMEOUT = "timeSyncLostTimeout";
    public static final String NOISE_THRESHOLD = "noiseThreshold";
    
    public static final String DUPLICATE_PACKETS = "dupPacks";
    public static final String BROADCAST_RETRANSMITION = "broadcastRetr";
    public static final String FORWARDING_PACKS_DROP = "fwPacksDrop";
}
