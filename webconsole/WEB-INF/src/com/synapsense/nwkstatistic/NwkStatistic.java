package com.synapsense.nwkstatistic;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.impl.dao.DataObjectDAO;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.dataconverters.UsersDataOptions;

public class NwkStatistic {
    private static final Log LOG = LogFactory.getLog(NwkStatistic.class);
    
    private static List<PropEntry> propEntries = new ArrayList<PropEntry>();
    
    public static List<PropEntry> getPropEntries() {
        return Collections.unmodifiableList(propEntries);
    }

    static StatisticData st = null;
    static{
        propEntries.add(new PropEntry(NodeStatProps.NWK_TIMESTAMP, "getNwkTimestamp"));
        propEntries.add(new PropEntry(NodeStatProps.WRONG_SLOT_COUNT, "getWrongSlotCount"));
        propEntries.add(new PropEntry(NodeStatProps.NUMBER_ONE_HOP_RETR, "getNumberOneHopRetransmission"));
        propEntries.add(new PropEntry(NodeStatProps.RADIO_ON_TIME, "getRadioOnTime"));
        propEntries.add(new PropEntry(NodeStatProps.FREE_SLOT_COUNT, "getFreeSlotCount"));
        propEntries.add(new PropEntry(NodeStatProps.TOTAL_SLOT_COUNT, "getTotalSlotCount"));
        propEntries.add(new PropEntry(NodeStatProps.TIME_SYNC_LOST_TIMEOUT, "getTimeSyncsLostTimeout"));
        propEntries.add(new PropEntry(NodeStatProps.NOISE_THRESHOLD, "getNoiseThresh"));
        
        //These properties are not needed for now
        //propEntries.add(new PropEntry(NodeStatProps.DUPLICATE_PACKETS, "getDuplicatePackets"));
        //propEntries.add(new PropEntry(NodeStatProps.BROADCAST_RETRANSMITION, "getBroadcastRetransmission"));
        //propEntries.add(new PropEntry(NodeStatProps.FORWARDING_PACKS_DROP, "getForwardingPacketsDropped"));
        
    }
    
    public static StatisticData getNodeStatistic(TO<?> node){
        StatisticData result = null;
        int nodeID = (Integer)node.getID();
        ValueTO v = Env.getNwkStat().getLatestData(nodeID);
        if(v != null){
            Object valueObj = v.getValue();
            if(valueObj != null){
                result = (StatisticData)valueObj;
            }
        }		
        return result;
    }
    
    public static Collection<StatisticData> getNodeStatistic(TO<?> node, Date start, Date end){
        Collection<StatisticData> result = new ArrayList<StatisticData>();
        DataObjectDAO<Integer,StatisticData> nwkDao = Env.getNwkStat();
        Collection<ValueTO> vtos = nwkDao.getData((Integer)node.getID(), start, end);
        
        for(ValueTO vto : vtos){
            result.add((StatisticData)vto.getValue());
        }
        
        return result;
    }
    
    public static Double getNodeAverageSlotCount(TO<?> node, Date start, Date end){
        Double result = Env.getNwkStat().getAverageSlotCount((Integer)node.getID(), start, end);
        return result;
    }

    public static void addStatisticAsChildXml(Element xmlElem, Collection<StatisticData> statDataList, HttpServletRequest request){
        for(StatisticData statData : statDataList){
            addStatisticAsChildXml(xmlElem, statData, request);
        }
    }
    
    public static void addStatisticAsChildXml(Element xmlElem, StatisticData statData, HttpServletRequest request){
        if(statData != null){
            SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request), UsersDataOptions.getTzIdFromRequest(request));
            Element statElem = xmlElem.getOwnerDocument().createElement("STAT");
            for(PropEntry entry : propEntries){
                try {
                    Method meth = StatisticData.class.getMethod(entry.getFuncName());
                    Object result = meth.invoke(statData);
                    Class<?> clazz = meth.getReturnType();
                    
                    Element propElem = xmlElem.getOwnerDocument().createElement(entry.getPropName());
                    
                    if(result != null){
                        String resultStr = convertResultToString(result, clazz, sdf);
                        propElem.setAttribute("v", resultStr);
                    }
                    statElem.appendChild(propElem);
                    
                } catch (Exception e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
            xmlElem.appendChild(statElem);
        }
    }
    
    public static String convertResultToString(Object result, Class<?> clazz, SimpleDateFormat sdf){
        String resultStr = "";
        if(clazz.equals(Date.class)){
            resultStr = sdf.format((Date)result);
        }else{
            resultStr = result.toString();
        }
        return resultStr;
    }
}
