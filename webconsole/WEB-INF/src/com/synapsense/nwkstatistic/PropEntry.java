package com.synapsense.nwkstatistic;

public class PropEntry {
    
    private String propName;
    private String funcName;
    
    PropEntry(String propName, String funcName){
        this.propName = propName;
        this.funcName = funcName;
    }
    
    public String getPropName() {
        return propName;
    }
    
    public String getFuncName() {
        return funcName;
    }
    
}
