package com.synapsense.tasktracker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.synapsense.session.exceptions.SessionExpiredException;

public class TaskStorage {
    private static final String TASK_MAP_NAME = "taskTrackerMap";
    private static TaskStorage instance = null;
    
    private TaskStorage(){        
    }
    
    public static TaskStorage getInstance(){
        if(instance == null){
            instance = new TaskStorage();
        }
        return instance;
    }    
    
    public void addTask(TaskID taskId, TaskData data, HttpServletRequest request){
        if(taskId != null){
            HttpSession session = request.getSession(false);       
            if (session == null) {
                throw new SessionExpiredException("Session Expired.");
            }
            
            HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
            taskMap.remove(taskId);
            taskMap.put(taskId, data);
            session.setAttribute(TASK_MAP_NAME, taskMap); 
        }
    }
    
    public void updateTask(TaskID taskId, TaskData data, HttpServletRequest request){
        if(taskId != null){
            HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
            TaskData tData = taskMap.get(taskId);
            if(tData != null){
                tData.setTaskDescr(data.getTaskDescr());
                tData.setTaskState(data.getTaskState());
            } 
        }
    }
    
    public void updateTaskState(TaskID taskId, TaskState state, HttpServletRequest request){
        if(taskId != null){
            HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
            TaskData data = taskMap.get(taskId);
            if(data != null){
                data.setTaskState(state);  
            } 
        }
    }
    
    public TaskData getTaskData(TaskID taskId, HttpServletRequest request){
        if(taskId != null){
            HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
            return taskMap.get(taskId);
        }else{
            return null;
        }
    }
    
    public void removeTask(TaskID taskId, HttpServletRequest request){
        if(taskId != null){
            HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
            taskMap.remove(taskId); 
        }
    }
    
    public void removeCompletedTasks(HttpServletRequest request){
        HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
        Set<TaskID> set = taskMap.keySet();
        Set<TaskID> listToRemove = new HashSet<TaskID>();
        for(TaskID tId : set){
            if (this.getTaskData(tId, request).state.getExecutionState() != StatesEnum.IN_PROGRESS){
                listToRemove.add(tId);
            }
        }        
        for(TaskID tId : listToRemove){
            taskMap.remove(tId);
        }        
    }
    
    public HashMap<TaskID, TaskData> getAllTasks(HttpServletRequest request){
        HashMap<TaskID, TaskData> taskMap = getTaskMap(request);
        HashMap<TaskID, TaskData> clonedMap = new HashMap<TaskID, TaskData>();
        clonedMap.putAll(taskMap);
        return clonedMap;
    }
    
    @SuppressWarnings("unchecked")
    protected HashMap<TaskID, TaskData> getTaskMap(HttpServletRequest request){
        HttpSession session = request.getSession(false);        
        if (session == null) {
            throw new SessionExpiredException("Session Expired.");
        }
        
        HashMap<TaskID, TaskData> taskMap = (HashMap<TaskID, TaskData>)session.getAttribute(TASK_MAP_NAME);        
        if(taskMap == null){
            taskMap = new HashMap<TaskID, TaskData>();
        }
        return taskMap;
    }
}
