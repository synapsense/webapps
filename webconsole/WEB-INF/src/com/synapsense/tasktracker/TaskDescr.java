package com.synapsense.tasktracker;

public class TaskDescr {
    protected String name = "";
    public TaskDescr(){        
    }
    
    public TaskDescr(String name){
        if(name == null){
            name = "";
        }
        this.name = name;
    }
    
    public void setTaskName(String name){
        this.name = name;
    }
    
    public String getTaskName(){
        return this.name;
    }
}
