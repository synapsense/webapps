package com.synapsense.tasktracker;

public class TaskID {
    protected String taskId = "";
    
    public TaskID(){
    }
    
    public void setDataFromStr(String taskIdRepresentationStr){
        taskId = taskIdRepresentationStr;
    }
    
    @Override
    public String toString(){
        return taskId;
    }
    
    @Override
    public int hashCode(){
        return taskId.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object arg0){
        if(arg0 instanceof TaskID){
            return arg0.toString().equals(this.toString());
        }else{
            return false;
        }
    }
}
