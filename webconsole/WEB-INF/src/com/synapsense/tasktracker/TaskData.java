package com.synapsense.tasktracker;

public class TaskData {
    protected TaskState state = null;
    protected TaskDescr descr = null;
    
    public TaskData(){        
    }    
    public TaskData(TaskState state, TaskDescr descr){
        this.state = state;
        this.descr = descr;
    }
    
    public void setTaskState(TaskState state){
        this.state = state;
    }    
    public void setTaskDescr(TaskDescr descr){
        this.descr = descr;
    }
    
    public TaskState getTaskState(){
        return state;
    }    
    public TaskDescr getTaskDescr(){
        return descr;
    }
    
    public void copy(TaskData tData){
        this.state = tData.state;
        this.descr = tData.descr;
    }
}
