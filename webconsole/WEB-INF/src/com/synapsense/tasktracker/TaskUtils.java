package com.synapsense.tasktracker;

import javax.servlet.http.HttpServletRequest;

public class TaskUtils {
    public static TaskID getTaskIDByRequest(HttpServletRequest req){
        String taskIdStr = req.getParameter("taskId");
        TaskID taskId = null;
        if(taskIdStr != null){
            taskId = new TaskID();
            taskId.setDataFromStr(taskIdStr);
        }
        return taskId;
    }
}
