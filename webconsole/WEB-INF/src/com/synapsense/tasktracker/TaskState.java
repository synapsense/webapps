package com.synapsense.tasktracker;

public class TaskState {
    protected StatesEnum executionState = null;
    protected String stateNote = "";
    
    public TaskState(){
    }    
    public TaskState(StatesEnum executionState, String stateNote){
        this.executionState = executionState;
        this.stateNote = stateNote;
    }
    
    public void setExecutionState(StatesEnum executionState){
        this.executionState = executionState;
    }    
    public void setStateNote(String stateNote){
        this.stateNote = stateNote;
    }    
    public void updateState(StatesEnum executionState, String stateNote){
        this.executionState = executionState;
        this.stateNote = stateNote;
    }
    
    public String packToStr(){
        return this.executionState.name() + "," + this.stateNote;
    }
    
    public StatesEnum getExecutionState(){
        return executionState;
    }    
    public String getStateNote(){
        return stateNote;
    }
}