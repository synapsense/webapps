package com.synapsense.tasktracker;

public enum StatesEnum {
    IN_PROGRESS,
    DONE,
    ERROR
}
