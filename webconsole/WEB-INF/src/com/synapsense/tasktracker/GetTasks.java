package com.synapsense.tasktracker;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

public class GetTasks extends BaseHttpServlet {
    private static final long serialVersionUID = 4062679591285336090L;
    private static final Log LOG = LogFactory.getLog(GetTasks.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
        throws IOException, ServletException {
        
        String str = "<result>" + "Done" + "</result>";
        
        //get All tasks
        if("getAll".equals(request.getParameter("type"))){
            Document tasksDoc = getTasksDoc(request);
            str = XMLHelper.getDocumentXml(tasksDoc);
            
        }else if("removeTask".equals(request.getParameter("type"))){
            TaskID taskId = new TaskID();
            taskId.setDataFromStr(request.getParameter("taskId"));            
            TaskStorage.getInstance().removeTask(taskId, request);
            
        }else if("removeAllCompleted".equals(request.getParameter("type"))){            
            TaskStorage.getInstance().removeCompletedTasks(request);
        }
        sendResponse(response, str);
        return;
    }
    
    protected Document getTasksDoc(HttpServletRequest request){
        Document doc = null;
        
        try{
            doc = XMLHelper.createNewXmlDocument("tasks");
            Element docRoot = doc.getDocumentElement();
            
            HashMap<TaskID, TaskData> tasksMap = TaskStorage.getInstance().getAllTasks(request);            
            
            Set<TaskID> tasksIds = tasksMap.keySet();
            for(TaskID tId : tasksIds){
                TaskData currentData = tasksMap.get(tId);
                
                Document taskDoc = XMLHelper.createNewXmlDocument("task");
                Element taskDocRoot = taskDoc.getDocumentElement();
                
                taskDocRoot.setAttribute("id", tId.toString());
                taskDocRoot.setAttribute("name", currentData.getTaskDescr().getTaskName());
                
                taskDocRoot.setAttribute("eState", currentData.getTaskState().getExecutionState().name());
                taskDocRoot.setAttribute("stateNote", currentData.getTaskState().getStateNote());
                //taskDocRoot.setAttribute("state", currentData.getTaskState().packToStr());
                                
                docRoot.appendChild(doc.importNode((Node)taskDocRoot, true));
            }
            
        }catch(SessionExpiredException ex){
            throw ex;
        }
        catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return doc;
    }
}
