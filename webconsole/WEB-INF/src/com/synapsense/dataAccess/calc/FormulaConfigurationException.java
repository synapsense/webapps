package com.synapsense.dataAccess.calc;

public class FormulaConfigurationException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = 7768055421307508032L;

    public FormulaConfigurationException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public FormulaConfigurationException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }

    public FormulaConfigurationException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public FormulaConfigurationException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
