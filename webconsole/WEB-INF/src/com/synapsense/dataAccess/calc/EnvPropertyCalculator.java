package com.synapsense.dataAccess.calc;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.jxpath.JXPathContext;
import org.apache.commons.jxpath.JXPathIntrospector;
import org.apache.commons.jxpath.JXPathNotFoundException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.matheclipse.parser.client.eval.DoubleEvaluator;
import org.matheclipse.parser.client.eval.DoubleVariable;
import org.matheclipse.parser.client.eval.IDoubleValue;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.config.types.Formula;
import com.synapsense.config.types.Property;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.config.types.VarBinding;

/**
 * @author apahunov
 * 
 * This utility class calculated "virtual" properties of the ES objects based on formulas
 * defined in conf/types.xml
 *
 */
public class EnvPropertyCalculator implements PropertyCalculator{
    private static final Log LOG = LogFactory.getLog(EnvPropertyCalculator.class);
    Environment env;
    long interval = 300000;

    static {
        //Assign handler for EnvObjectContext
        JXPathIntrospector.registerDynamicClass(EnvObjectContext.class, DynamicEnvPropertyHandler.class);
    }
    
    public EnvPropertyCalculator(Environment env) {
        this.env = env;
    }
    
    @Override
    public Number calculateProperty(TO<?> obj, String propName) throws FormulaConfigurationException, ObjectNotFoundException{
        Property property = TypeUtil.getInstance().getProperty(obj.getTypeName(), propName);
        
        if (property == null) {
            throw new FormulaConfigurationException("Property " + propName + " doesn't have formula associated");
        }
        
        Formula formula = property.getFormula();
        
        if (formula == null) {
            throw new FormulaConfigurationException("Property " + propName + " doesn't have formula associated");
        }
        
        List<VarBinding> varBindingList = formula.getVarbinding();
        
        DoubleEvaluator engine = new DoubleEvaluator();
        
        JXPathContext context = JXPathContext.newContext(new EnvObjectContext(obj, env));
        
        for (VarBinding varBinding : varBindingList) {
            String path = varBinding.getPath();
            if (path == null) {
                return null;
            }

            Object value = null;
            
            try {
                value = context.getValue(path);
            } catch (JXPathNotFoundException e) {
                //property is not set
                return null;
            }
            if (value == null) {
                return null;
            }

            IDoubleValue vd = new DoubleVariable(((Number)value).doubleValue());
            engine.defineVariable(varBinding.getVarname(), vd);
        } 
        
        Number result = engine.evaluate(formula.getExpression().getText());
        String className = property.getClassname();
        if (result != null && result.equals(Double.NaN)) {
            result = null;
        } else if ("java.lang.Integer".equals(className)) {
            result = result.intValue();
        }
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("EnvPropertyCalculator.calculateProperty(\"" + obj + "\", " + propName + ") returns " + result);
        }

        return result;
    }

    @Override
    public Collection<ValueTO> calculatePropertyHistory(TO<?> obj, String propName, Date start, Date end) throws FormulaConfigurationException, ObjectNotFoundException {
        Property property = TypeUtil.getInstance().getProperty(obj.getTypeName(), propName);
        
        if (property == null) {
            throw new FormulaConfigurationException("Property " + propName + " doesn't have formula associated");
        }
        
        Formula formula = property.getFormula();
        
        if (formula == null) {
            throw new FormulaConfigurationException("Property " + propName + " doesn't have formula associated");
        }
        
        List<VarBinding> varBindingList = formula.getVarbinding();
        
        Map<String, Collection<ValueTO>> history = new HashMap<String, Collection<ValueTO>>();
                
        for (VarBinding varBinding : varBindingList) {
            String path = varBinding.getPath();
            if (path == null) {
                break;
            }

            String path1;
            String path2;
            
            int lastSlashIndex = path.lastIndexOf("/");
            if (lastSlashIndex != -1) {
                path1 = path.substring(0, lastSlashIndex);
                path2 = path.substring(lastSlashIndex + 1);
            } else {
                path1 = null;
                path2 = path;
            }
            
            if (path1 != null && !path1.isEmpty()) {
                JXPathContext context = JXPathContext.newContext(new EnvObjectContext(obj, env));
                obj = ((EnvObjectContext) context.getValue(path1)).getObject();
            }

            Collection<ValueTO> values;
            try {
                values = (Collection<ValueTO>) env.getHistory(obj, path2, start, end, Double.class);
                history.put(varBinding.getVarname(), values);
            } catch (PropertyNotFoundException e) {
                throw new FormulaConfigurationException(e.getLocalizedMessage(), e);
            } catch (UnableToConvertPropertyException e) {
                throw new FormulaConfigurationException(e.getLocalizedMessage(), e);
            }
        } 

        Collection<ValueTO> result = new LinkedList<ValueTO>();
        DoubleEvaluator engine = new DoubleEvaluator();
outer:	for (long t = end.getTime() / interval * interval; t >= start.getTime(); t -= interval) {
            engine.clearVariables();
            for (Map.Entry<String, Collection<ValueTO>> entry : history.entrySet()) {
                Collection<ValueTO> hist = entry.getValue();
                ValueTO value = extractValue(hist, t);
                if (value == null) {
                    //or add null?
                    continue outer;
                }
                
                IDoubleValue vd = new DoubleVariable((Double) value.getValue());
                engine.defineVariable(entry.getKey(), vd);
            }
            Double res = engine.evaluate(formula.getExpression().getText());
            result.add(new ValueTO(propName, res, t));
        }
        
        if (LOG.isDebugEnabled()) {
            LOG.debug("EnvPropertyCalculator.calculatePropertyHistory(\"" + obj + "\", " + propName + ") returns " + result);
        }

        return result;
    }

    
    /**
     * Extracts value closest to provided time, removes processed points
     * @param history
     * @param time
     * @return
     */
    private ValueTO extractValue(Collection<ValueTO> history, long time) {
        Iterator<ValueTO> it = history.iterator();
        while (it.hasNext()) {
            ValueTO valTo = it.next();
        
            if (valTo.getTimeStamp() > time) {
                //throw it away
                history.remove(valTo);
            }else if (valTo.getTimeStamp() <= time && valTo.getTimeStamp() > time - interval) {
                history.remove(valTo);
                return valTo;
            } else {
                return null;
            }
            //updated iterator
            it = history.iterator();
        }
        return null;
    }
}
