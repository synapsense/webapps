package com.synapsense.dataAccess.calc;

import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;

public interface PropertyCalculator {
    Number calculateProperty(TO<?> obj, String propName) throws ObjectNotFoundException, FormulaConfigurationException;
    Collection<ValueTO>  calculatePropertyHistory(TO<?> obj, String propName, Date start, Date end) throws FormulaConfigurationException, ObjectNotFoundException;
}
