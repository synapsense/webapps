package com.synapsense.dataAccess.calc;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.jxpath.DynamicPropertyHandler;

import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;

/**
 * @author pahunov
 *
 * This class is used by JXPathContext to obtain properties of the ES objects .
 * It is registered within JXPathIntrospector.registerDynamicClass using
 * JXPathIntrospector.registerDynamicClass(EnvObjectContext.class, DynamicEnvPropertyHandler.class); 
 */
public class DynamicEnvPropertyHandler implements DynamicPropertyHandler {

    @Override
    public Object getProperty(Object obj, String propertyName) {
        EnvObjectContext c = (EnvObjectContext) obj;
        Environment env = c.getEnv();
        TO<?> to = c.getObject();
        Object res = null;
        Object value;
        try {
            value = env.getPropertyValue(to, propertyName, Object.class);
        } catch (EnvException e) {
            value = null;
        }
        
        if (value != null) {
            // For TOs we need to return EnvObjectContext to pass env 
            // to the next xpath evaluation step 
            
            if (value instanceof TO<?>) {
                res = new EnvObjectContext((TO<?>) value, env);
            } else if (value instanceof Collection<?>) {
                Set<EnvObjectContext> colRes = new HashSet<EnvObjectContext>();
                Collection<?> col = (Collection<?>) value;
                for (Object o : col) {
                    colRes.add(new EnvObjectContext((TO<?>) o, env));
                }
                res = colRes;
            } else {
                res = value;
            }
        }
        return res;
    }

    @Override
    public String[] getPropertyNames(Object obj) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setProperty(Object obj, String propertyName, Object value) {
        throw new UnsupportedOperationException();
    }

}
