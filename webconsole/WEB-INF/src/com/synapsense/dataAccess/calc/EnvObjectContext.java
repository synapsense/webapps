package com.synapsense.dataAccess.calc;


import com.synapsense.dto.TO;
import com.synapsense.service.Environment;


/**
 * @author pahunov
 *
 * This is utility class passed to JXPathContext.newContext(), used
 * to store Environment and TO pair as we need both to be able to fetch
 * object properties from ES
 */
public class EnvObjectContext {
    private TO<?> object;
    private Environment env;
    
    public EnvObjectContext(TO<?> object, Environment env) {
        super();
        this.object = object;
        this.env = env;
    }
    public TO<?> getObject() {
        return object;
    }
    public Environment getEnv() {
        return env;
    }
}
