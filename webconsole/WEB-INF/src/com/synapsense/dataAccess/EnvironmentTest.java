package com.synapsense.dataAccess;

import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.utilities.es.EnvironmentTypes;

public class EnvironmentTest {

    /**
     * @param args
     */

    public static void main(String[] args) {

        Properties p = new Properties();
        p.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        Env.defaultHost = "127.0.0.1";
        Env.appName = "SEMS";
        Env.loginAsSynapUserIfRemote();
        Env.loginAsSynapUser();
        Env.init(p);		
        Environment env = Env.getEnv();
        
        Env.login("admin", "admin");

        try {
            TO<?> root = (env.getObjectsByType(EnvironmentTypes.TYPE_ROOT)).iterator().next();
            System.out.println("Root is " + root);
            //env.setPropertyValue(root, "version", "6.5");
            //env.setPropertyValue(root, "modelVersion", "2.5");
            
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        System.out.println("DONE!!!");
    }
    
    
    static final Environment getEnv(String ip) {

        final String INITIAL_CONTEXT_FACTORY = "org.jboss.naming.remote.client.InitialContextFactory";

        String PROVIDER_URL = "remote://" + ip + ":4447";
        Properties connectProperties = new Properties();
        connectProperties.put(Context.INITIAL_CONTEXT_FACTORY,
                INITIAL_CONTEXT_FACTORY);
        connectProperties.put(Context.PROVIDER_URL, PROVIDER_URL);
        connectProperties.put("jboss.naming.client.ejb.context", "true");
        connectProperties
                .put("jboss.naming.client.connect.options.org.xnio.Options.SASL_POLICY_NOPLAINTEXT",
                        "false");
        
        connectProperties.put(Context.SECURITY_PRINCIPAL, "admin");
        connectProperties.put(Context.SECURITY_CREDENTIALS, "admin");
        connectProperties.put("SynapEnvRemote",
                "SynapServer/Environment/remote");

        Environment env = null;

        try {
            InitialContext ctx = new InitialContext(connectProperties);
            
            long t = System.currentTimeMillis();
            env = ServerLoginModule
                    .getProxy(
                            ctx,
                            "es-ear/es-core/Environment!com.synapsense.service.Environment",
                            Environment.class);
            
            System.out.println(System.currentTimeMillis() - t + " ms");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return env;

    }


}
