package com.synapsense.dataAccess;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.security.client.SecurityClient;
import org.jboss.security.client.SecurityClientFactory;

import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.DRulesEngine;
import com.synapsense.service.DiagnosticService;
import com.synapsense.service.Environment;
import com.synapsense.service.ErlNodeService;
import com.synapsense.service.ErlNodeServiceManagement;
import com.synapsense.service.FileWriter;
import com.synapsense.service.LocalizationService;
import com.synapsense.service.RulesEngine;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.audit.AuditorSvc;
import com.synapsense.service.impl.dao.NetworkStatisticDAOIF;
import com.synapsense.service.impl.messages.base.StatisticData;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.ConvertingEnvironment;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.ConvertingEnvironmentProxy;
import com.synapsense.util.ServerLoginModule;
import com.synapsense.util.TranslatingActivityLogService;
import com.synapsense.util.TranslatingAlertService;
import com.synapsense.util.TranslatingEnvironment;
import com.synapsense.util.Translator;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.lang.LangManager;

public class Env {

    private static final Log log = LogFactory.getLog(Env.class);

    private static InitialContext ctx = null;

    private static AlertService alertServ = null;
    private static ActivityLogService activityLogService = null;
    private static DRuleTypeService<Object> druleType = null;
    private static AuditorSvc auditorSvc = null;
    private static DRulesEngine dre = null;
    private static RulesEngine cre = null;
    private static UserManagementService userManagementService = null;
    private static LocalizationService localizationService = null;
    private static ConvertingEnvironmentProxy cEnvProxy = null;
    private static TranslatingEnvironment tEnvProxy = null;
    private static FileWriter fileWritter = null;
    //private static RMIAdaptor server = null;
    private static UnitSystemsService usService = null;
    private static DiagnosticService diagnosticServ = null;
    private static NetworkStatisticDAOIF<Integer, StatisticData> nwkDao = null;
    private static ReportingManagementService reportManager = null;
    private static ErlNodeService activeControlService = null;

    //final private static String RMI_ADAPTOR_JNDI_NAME = "jmx/invoker/RMIAdaptor";

    final public static String DEFAULT_UNIT_SYSTEM = "Server";	
    final public static String CONSOLE_APP_NAME = "WebConsole";
    
    private static String defaultUser = "synapsense";
    private static String defaultPassword = "synap$ense";
    
    public static String appName;
    public static String defaultHost = "localhost";

    public static String dbVersion;
    public static String minModelVersion;
    public static String maxModelVersion;


    // private static HashMap<String, ConvertingEnvironment> envMap = new
    // HashMap<String, ConvertingEnvironment>();

    private static void initInitialContext(Properties c) throws NamingException {
        ctx = new InitialContext(c);
    }
    
    public static String getDefaultUser() {
        return defaultUser;
    }

    public static Properties getConnectionProperties(ServletContext c) {
        //final String INITIAL_CONTEXT_FACTORY = c.getInitParameter("INITIAL_CONTEXT_FACTORY");
        final String URL_PKG_PREFIXES = c.getInitParameter("URL_PKG_PREFIXES");
        
        //final String PROVIDER_URL = c.getInitParameter("PROVIDER_URL");

        Properties environment = new Properties();
        //environment.put(Context.INITIAL_CONTEXT_FACTORY,INITIAL_CONTEXT_FACTORY);
        environment.put(Context.URL_PKG_PREFIXES,URL_PKG_PREFIXES);
        //environment.put(Context.PROVIDER_URL, PROVIDER_URL);
        return environment;
    }
    
    public static void setContextParams(ServletContext c) {
        defaultHost = c.getInitParameter("PROVIDER_HOST");
        appName = c.getInitParameter("APP_NAME");
        defaultUser = c.getInitParameter("SYNAP_USER_NAME");
        defaultPassword = c.getInitParameter("SYNAP_USER_PASSWORD");

        dbVersion = c.getInitParameter("DATABASE_VERSION");
        minModelVersion = c.getInitParameter("MIN_OBJECT_MODEL_VERSION");
        maxModelVersion = c.getInitParameter("MAX_OBJECT_MODEL_VERSION");
    }
    
    private static void initReportManager(Properties c){
        if (reportManager == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                reportManager = ServerLoginModule.getService(ctx, "ReportingManagementService",
                        ReportingManagementService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
            
    public static ErlNodeService getActiveControlService(HttpServletRequest req){
        return activeControlService;
    }
        
    private static void initActiveControlService(Properties c){
        if (activeControlService == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                activeControlService = ServerLoginModule.getService(ctx, "ErlNodeService",
                        ErlNodeService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
            
    public static ReportingManagementService getReportManager(HttpServletRequest req){
        return reportManager;
    }
        
    @SuppressWarnings("unchecked")
    private static void initNwkStat(Properties c){
        if (nwkDao == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                nwkDao = ServerLoginModule.getService(ctx, "es-dal",  "NetworkStatisticDAO", NetworkStatisticDAOIF.class);
            } catch (Exception e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static NetworkStatisticDAOIF<Integer, StatisticData> getNwkStat() {
        if (nwkDao == null) {
            throw new IllegalStateException("NetworkStatistic is not initialized. Call Env.init(Properties c) first");
        }
        return nwkDao;
    }

    private static void initDiagnosticService(Properties c) {
        if (diagnosticServ == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                diagnosticServ = ServerLoginModule.getService(ctx, "DiagnosticService", DiagnosticService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static DiagnosticService getDiagnosticServ(HttpServletRequest req) {
        return diagnosticServ;
    }

    private static void initAlertingServ(Properties c) {
        if (alertServ == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                //TODO: wrap into UnitedAS
                alertServ = ServerLoginModule.getService(ctx, "AlertService", AlertService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static AlertService getAlertingServ(HttpServletRequest req) {
        HttpSession session = req.getSession();
        Env.initAlertingServ(getConnectionProperties(session.getServletContext()));
        String userName = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        AlertService as;
        TO<?> user = Env.getUserDAO().getUser(userName);

        if (!UserUtils.hasRootPrivilege(user)) {
            AlertService fas = (AlertService) session.getAttribute(SessionVariableNames.SESSION_ALERTING_SERVICE);
            if (fas == null) {
                FilteringAlertService fasProxy = new FilteringAlertService(alertServ,
                        cEnvProxy.getProxiedEnvironment(), Env.getEnv(req));
                fas = fasProxy.getProxy();
                session.setAttribute(SessionVariableNames.SESSION_ALERTING_SERVICE, fas);
            }
            as = fas;
        } else {
            as = alertServ;
        }

        TranslatingAlertService tas = new TranslatingAlertService(as);
        tas.setLanguage(UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));
        return tas;
    }

    static AlertService getAlertService() {
        if (alertServ == null) {
            throw new IllegalStateException("AlertService not initialized. Call Env.init(Properties c) first");
        }
        return alertServ;
    }

    private static void initActivityLogServ(Properties c) {
        if (activityLogService == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                activityLogService = ServerLoginModule.getService(ctx, "ActivityLogService", ActivityLogService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static ActivityLogService getActivityLogServ(HttpServletRequest req) {
        if (activityLogService == null) {
            throw new IllegalStateException("ActivityLogService not initialized. Call Env.init(Properties c) first");
        }

        HttpSession session = req.getSession();
        String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        TranslatingActivityLogService service = new TranslatingActivityLogService(activityLogService);
        TO<?> user = Env.getUserDAO().getUser(username);
        service.setLanguage(UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));
        // session.setAttribute(SessionVariableNames.SESSION_TRANSLATING_ENVIRONMENT,
        // tEnvProxy);

        return service;
    }

    public static ActivityLogService getActivityLogServ() {
        if (activityLogService == null) {
            throw new IllegalStateException("ActivityLogService not initialized. Call Env.init(Properties c) first");
        }

        return activityLogService;
    }

    @SuppressWarnings("unchecked")
    private static void initDRuleTypeDAO(Properties c) {
        if (druleType == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                    
                druleType = ServerLoginModule.getService(ctx, "es-dal", "DRuleTypeObjectDAO", DRuleTypeService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }

    }

    public static DRuleTypeService<Object> getDRuleTypeDAO() {
        if (druleType == null) {
            throw new IllegalStateException("DRuleTypeService not initialized. Call Env.init(Properties c) first");
        }
        return druleType;
    }

    private static void initAuditorSvc(Properties c) {
        if (auditorSvc == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                auditorSvc = ServerLoginModule.getService(ctx, "AuditorSvcImpl", AuditorSvc.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static AuditorSvc getAuditorSvc() {
        if (auditorSvc == null) {
            throw new IllegalStateException("AuditorSvc not initialized. Call Env.init(Properties c) first");
        }
        return auditorSvc;
    }

    private static Environment getCacheEnvironment(final Environment env) {
        Environment res = null;
        try {
            Class<?> clazz = Class.forName("com.synapsense.dal.objectcache.CacheSvcImpl");
            Method m = clazz.getMethod("getInstance");
            final long pause = 1000;
            Object cache = null;
            while(true) {
                try {
                    cache = m.invoke(null, new Object[] {});
                    break;
                } catch (InvocationTargetException e) {
                    log.info("Environment Server not available, waiting for "+pause/1000+" sec");
                    try {
                        Thread.sleep(pause);
                    } catch (InterruptedException e1) {
                        log.info("Interrupted while waiting for ES start");
                        break;
                    }
                }
            }
            clazz = Class.forName("com.synapsense.dal.objectcache.CacheEnvironmentDispatcher");
            m = clazz.getMethod("getEnvironment", Class.forName("com.synapsense.dal.objectcache.CacheSvc"),
                    Environment.class);
            final Environment cacheEnv = (Environment) m.invoke(null, cache, env);

            res = (Environment) Proxy.newProxyInstance(Environment.class.getClassLoader(),
                    new Class[] { Environment.class }, new InvocationHandler() {
                        @Override
                        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                            try {
                                if (method.getName().startsWith("set")) {
                                    return method.invoke(env, args);
                                } else {
                                    return method.invoke(cacheEnv, args);
                                }
                            } catch (InvocationTargetException e) {
                                // unwrap original cause
                                throw e.getCause();
                            }
                        }
                    });
            Class<?> creenvDispClass = Class.forName("com.synapsense.rulesengine.CREEnvironmentDispatcher");
            return (Environment) creenvDispClass.getConstructor(Environment.class, RulesEngine.class).newInstance(res,
                    getCRE());
        } catch (ClassNotFoundException e) {
            // that's ok, we are in the standalone jvm
        } catch (SecurityException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (NoSuchMethodException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (IllegalArgumentException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (IllegalAccessException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (InvocationTargetException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (InstantiationException e) {
            log.error(e.getLocalizedMessage(), e);
        }

        return res;
    }

    private static void initEnv(Properties c) {
        if (cEnvProxy == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
                Env.initUnitSystemsService(c);
                
                Environment env = ServerLoginModule.getService(ctx, "Environment", Environment.class);
                
                Environment cache = getCacheEnvironment(env);
                if (cache != null) {
                    env = cache;
                    log.info("Use CacheEnvironmentDispatcher");
                } else {
                    log.info("Use remote Environment");
                }

                cEnvProxy = new ConvertingEnvironmentProxy();
                cEnvProxy.setUnitSystems(usService.getUnitSystems());
                cEnvProxy.setUnitResolvers(usService.getUnitReslovers());
                cEnvProxy.setProxiedEnvironment(env);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    /**
     * Returns Environment wrapped into a set of proxies providing services like unit conversion, translation, filtering, permissions etc.
     * For WebConsole getLocalEnv and getEnv are identical. 
     * For SEMS getLocalEnv returns Environment that is not wrapped into UnitedProxy
     * 
     * @param req
     * @return
     */
    public static ConvertingEnvironment getLocalEnv(HttpServletRequest req) {
        HttpSession session = req.getSession();
        ConvertingEnvironment proxyEnv = (ConvertingEnvironment) session
                .getAttribute(SessionVariableNames.SESSION_LOCAL_ENVIRONMENT);

        if (proxyEnv == null) {
            Env.initEnv(getConnectionProperties(session.getServletContext()));

            try {
                String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
                
                ConvertingEnvironmentProxy ce = (ConvertingEnvironmentProxy) cEnvProxy.clone();
                TO<?> user = Env.getUserDAO().getUser(username);
                String system = UserPreference.getString(user, "UnitsSystem", Env.getUserDAO());
                if (system != null) {
                    ce.setConversionEnabled(!Env.DEFAULT_UNIT_SYSTEM.equals(system));
                    ce.setTargetSystem(system);
                }

                Environment proxiedEnv = ce.getProxiedEnvironment();

                // Insert one more proxy
                tEnvProxy = new TranslatingEnvironment(proxiedEnv);
                tEnvProxy.setLanguage(UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));
                session.setAttribute(SessionVariableNames.SESSION_TRANSLATING_ENVIRONMENT, tEnvProxy);
                proxiedEnv = tEnvProxy;
                // Insert one more proxy
                FilteringEnvironment fe = new FilteringEnvironment(proxiedEnv, username, 
                        UserUtils.getUserGroupNames(user), 
                        UserUtils.hasRootPrivilege(user));
                ce.setProxiedEnvironment(fe.getProxy());
                proxyEnv = ce.getProxy();

                session.setAttribute(SessionVariableNames.SESSION_LOCAL_ENVIRONMENT, proxyEnv);
                session.setAttribute(username, ce);
            } catch (ConvertingEnvironmentException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }

        return proxyEnv;
    }


    /**
     * Returns Environment wrapped into a set of proxies providing services like unit conversion, translation, filtering, permissions etc.
     * For WebConsole getLocalEnv and getEnv are identical. 
     * For SEMS getEnv returns Environment that is also wrapped into UnitedProxy
     * 
     * @param req
     * @return
     */
    public static ConvertingEnvironment getEnv(HttpServletRequest req) {
        HttpSession session = req.getSession();
        ConvertingEnvironment proxyEnv = (ConvertingEnvironment) session
                .getAttribute(SessionVariableNames.SESSION_ENVIRONMENT);

        if (proxyEnv == null) {
            Env.initEnv(getConnectionProperties(session.getServletContext()));

            try {
                String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
                String userPswd = (String) session.getAttribute(SessionVariableNames.SESSION_PASSWORD);
                ConvertingEnvironmentProxy ce = (ConvertingEnvironmentProxy) cEnvProxy.clone();
                TO<?> user = Env.getUserDAO().getUser(username);
                String system = UserPreference.getString(user, "UnitsSystem", Env.getUserDAO());
                if (system != null) {
                    ce.setConversionEnabled(!Env.DEFAULT_UNIT_SYSTEM.equals(system));
                    ce.setTargetSystem(system);
                }

                Environment proxiedEnv = ce.getProxiedEnvironment();

                // Insert one more proxy
                tEnvProxy = new TranslatingEnvironment(proxiedEnv);
                tEnvProxy.setLanguage(UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));
                session.setAttribute(SessionVariableNames.SESSION_TRANSLATING_ENVIRONMENT, tEnvProxy);
                proxiedEnv = tEnvProxy;
                // Insert one more proxy
                FilteringEnvironment fe = new FilteringEnvironment(proxiedEnv, username, 
                        UserUtils.getUserGroupNames(user), 
                        UserUtils.hasRootPrivilege(user));
                ce.setProxiedEnvironment(fe.getProxy());
                proxyEnv = ce.getProxy();

                session.setAttribute(SessionVariableNames.SESSION_ENVIRONMENT, proxyEnv);
                session.setAttribute(username, ce);
            } catch (ConvertingEnvironmentException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }

        return proxyEnv;
    }

    /**
     * WARNING: do NOT use this method if you don't really know what you are
     * doing. Use "getEnv(HttpServletRequest req)" instead.
     * 
     * @return
     */
    public static Environment getEnv() {
        if (cEnvProxy == null) {
            throw new IllegalStateException("ConvertingEnvironment not initialized. Call Env.init(Properties c) first");
        }
        return cEnvProxy.getProxiedEnvironment();
    }

    public static Dimension getDimension(TO<?> to, String propName, HttpServletRequest req) {
        Dimension dim = null;
        ConvertingEnvironment cEnv = Env.getEnv(req);

        if (cEnv != null) {
            dim = cEnv.getDimension(to, propName);
        }
        return dim;
    }

    public static String getUnits(TO<?> to, String propName, HttpServletRequest req) {
        String unit = null;
        Dimension d = Env.getDimension(to, propName, req);
        if (d != null) {
            unit = d.getUnits();
        }
        
        if (unit != null && !unit.isEmpty()) {
            unit = Env.getLocalizationService().getString(unit, LangManager.getCurrentLocale(req));
        }
        
        return unit;
    }

    public static String getLongUnits(TO<?> to, String propName, HttpServletRequest req) {
        String unit = null;
        Dimension d = Env.getDimension(to, propName, req);
        if (d != null) {
            unit = d.getLongUnits();
        }
        return unit;
    }

    public static void setUnitsSystem(String system, HttpServletRequest req) {
        ConvertingEnvironment cEnv = Env.getEnv(req);

        if (cEnv != null) {
            try {
                cEnv.setConversionEnabled(!Env.DEFAULT_UNIT_SYSTEM.equals(system));
                cEnv.setTargetSystem(system);
            } catch (ConvertingEnvironmentException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    private static void initFW(Properties c){
        if (fileWritter == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                fileWritter = ServerLoginModule.getService(ctx, "FileWriterService", FileWriter.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    public static FileWriter getFileWritter(){
        if (fileWritter == null) {
            throw new IllegalStateException("FileWriter not initialized. Call Env.init(Properties c) first");
        }
        return fileWritter; 
    }
    
    /*private static void initRMIAdaptor(Properties c){
        if (server == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                server = (RMIAdaptor) ctx.lookup(RMI_ADAPTOR_JNDI_NAME);
            } catch (Exception e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    public static RMIAdaptor getRMIAdaptor(){
        if (server == null) {
            throw new IllegalStateException("RMIAdaptor not initialized. Call Env.init(Properties c) first");
        }
        return server; 
    }*/
    
    private static void initDRE(Properties c){
        if (dre == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                dre = ServerLoginModule.getService(ctx, "es-dre", "DRulesEngineService", DRulesEngine.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static DRulesEngine getDRE() {
        if (dre == null) {
            throw new IllegalStateException("DRulesEngine was not initialized. Call Env.init(Properties c) first");
        }
        return dre;
    }

    private static void initCRE(Properties c) {
        if (cre == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                cre = ServerLoginModule.getService(ctx, "RulesEngineService", RulesEngine.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static RulesEngine getCRE() {
        if (cre == null) {
            throw new IllegalStateException(
                    "Calculation RulesEngine was not initialized. Call Env.init(Properties c) first");
        }
        return cre;
    }

    private static void initUserDAO(Properties c) {
        if (userManagementService == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                userManagementService = ServerLoginModule.getService(ctx, "UserManagementService", UserManagementService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    public static UserManagementService getUserDAO() {
        if (userManagementService == null) {
            throw new IllegalStateException(
                    "UserManagementService was not initialized. Call Env.init(Properties c) first");
        }
        return userManagementService;
    }

    private static void initLocalizationService(Properties c) {
        if (localizationService == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                localizationService = ServerLoginModule.getService(ctx, "LocalizationService",
                        LocalizationService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static LocalizationService getLocalizationService() {
        if (localizationService == null) {
            throw new IllegalStateException(
                    "LocalizationService was not initialized. Call Env.init(Properties c) first");
        }
        return localizationService;
    }

    private static void initUnitSystemsService(Properties c) {
        if (usService == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }

                usService = ServerLoginModule.getService(ctx, "UnitSystemsServiceImpl", UnitSystemsService.class);
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public static UnitSystemsService getUnitSystemsService() {
        if (usService == null) {
            throw new IllegalStateException("UnitSystemsService was not initialized. Call Env.init(Properties c) first");
        }
        return usService;
    }
    
    private static void holdUntilESAvailable(Properties c) {
        if (cEnvProxy == null) {
            try {
                if (ctx == null) {
                    Env.initInitialContext(c);
                }
            } catch (NamingException e) {
                log.error(e.getLocalizedMessage(), e);
            }

            while(true) {
                final long pause = 1000;
                try{
                    ServerLoginModule.getService(ctx, "Environment", Environment.class);
                    break;
                } catch (NamingException e) {
                    log.info("Environment Server not available, waiting for "+pause/1000+" sec");
                    try {
                        Thread.sleep(pause);
                    } catch (InterruptedException e1) {
                        log.info("Interrupted while waiting for ES start");
                        break;
                    }
                }
            }
        }
    }
    
    public static void init(Properties c) {
        Env.holdUntilESAvailable(c);
        Env.initAlertingServ(c);
        Env.initActivityLogServ(c);
        Env.initAuditorSvc(c);
        Env.initDRE(c);
        Env.initCRE(c);
        Env.initDRuleTypeDAO(c);
        Env.initEnv(c);
        Env.initLocalizationService(c);
        Translator.setLocalizationService(Env.getLocalizationService());
        Env.initUserDAO(c);
        Env.initFW(c);
        //Env.initRMIAdaptor(c);
        Env.initUnitSystemsService(c);
        Env.initDiagnosticService(c);
        Env.initNwkStat(c);
        Env.initReportManager(c);
        Env.initActiveControlService(c);
    }

    public static boolean login(HttpServletRequest req) {
        if (req != null) {
            HttpSession session = req.getSession(false);
            String username = null;
            String password = null;
            if (session != null) {
                username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
                password = (String) session.getAttribute(SessionVariableNames.SESSION_PASSWORD);
            }
            if (username == null || password == null) { //probably this in login servlet
                username = (String)req.getParameter(RequestVariableNames.USER_NAME);
                password = (String)req.getParameter(RequestVariableNames.PASSWORD);
            }

            if (username == null || password == null) {
                // try to get login and password from the basic http
                // authorization
                String header = req.getHeader("Authorization");

                if (header != null && header.toLowerCase().startsWith("basic ")) {
                    String coded = header.substring(6);
                    String decoded = new String(Base64.decodeBase64(coded));

                    int index = decoded.indexOf(":");
                    if (index > 0) {
                        username = decoded.substring(0, index);
                        password = decoded.substring(index + 1);
                        ;
                    }

                }
            }
            
            if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
                login(defaultUser, defaultPassword);
                return false;
            }
            return login(username, password);
            
        }
        login(defaultUser, defaultPassword);
        return false;
    }
    
    public static boolean login(String username, String password) {
        login(defaultHost, username, password);
        return true;
    }
    
    public static boolean loginAsSynapUser(String host) {
        if (host == null) {
            host = defaultHost;
        }
        return login(host, defaultUser, defaultPassword);
    }
    
    public static boolean loginAsSynapUser() {
        // return login(defaultHost, defaultUser, defaultPassword);
        SecurityClient sc;
        try {
            sc = SecurityClientFactory.getSecurityClient();
            sc.setSimple(defaultUser, defaultPassword);
            sc.login();
        } catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
            return false;
        }
        return true;
    }
    
    public static boolean loginAsSynapUserIfRemote() {
        return login(defaultHost, defaultUser, defaultPassword);
    }
    
    public static boolean login(String host, String username, String password) {
//		EJBClientConfigurator.setupEJBClientContextSelector(host, username, password);
        return true;
//		try {
//			ServerLoginModule.login(username, password);
//			return true;
//		} catch (LoginException e) {
//			log.error(e.getLocalizedMessage(), e);
//		}
//
//		return false;
    }

    public static boolean logout() {
//		EJBClientConfigurator.resetEJBClientContextSelector();
        return true;
//		try {
//			ServerLoginModule.logout();
//			return true;
//		} catch (LoginException e) {
//			log.error(e.getLocalizedMessage(), e);
//		}
//
//		return false;
    }

    public static void setLanguage(HttpServletRequest req, String lang) {
        TranslatingEnvironment te = (TranslatingEnvironment) req.getSession().getAttribute(
                SessionVariableNames.SESSION_TRANSLATING_ENVIRONMENT);
        if (te != null) {
            te.setLanguage(lang);
        }
        req.getSession().setAttribute(SessionVariableNames.SESSION_LANGUAGE, lang);
    }
}
