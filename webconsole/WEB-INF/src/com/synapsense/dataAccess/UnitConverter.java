package com.synapsense.dataAccess;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.util.unitconverter.ConverterException;

public class UnitConverter {
    private static final Log log = LogFactory.getLog(UnitConverter.class);

    public static Double convertValue(Double value, String dataclass, String baseSystem, String targetSystem) {
        try {
            return Env.getUnitSystemsService().getUnitSystems().getUnitConverter(baseSystem, targetSystem, dataclass).convert(value);
        } catch (ConverterException e) {
            log.error(e.getLocalizedMessage(), e);
            return value;
        }
    }

    public static String convertValue(String value, String dataclass, String baseSystem, String targetSystem) {
        return UnitConverter.convertValue(Double.valueOf(value), dataclass, baseSystem, targetSystem).toString();
    }
}
