package com.synapsense.reports;

import com.synapsense.ESConfigurator.utils.UploadFile;
import com.synapsense.dataAccess.Env;
import com.synapsense.exception.ObjectExistsException;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.management.ReportParameter;
import com.synapsense.service.reporting.management.ReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.lang.LangManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ManageReports extends HttpServlet {

    private static final long serialVersionUID = 1786291968664320930L;
    private static final Log LOG = LogFactory.getLog(ManageReports.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.debug("Execution started ...");
        
        String status = ErrorMessages.SUCCESS;		
        String action =  request.getParameter("action");
        
        try{
            if("execute".equals(action)){
                status = execute(request);
            }else if("schedule".equals(action)){
                status = schedule(request);
            }else if("update".equals(action)){
                status = update(request);
            }if("addTemplate".equals(action)){
                addTemplate(request);
            }else if("deleteReports".equals(action)){
                deleteReportsAndCommands(request);
            }else if("deleteTask".equals(action)){
                deleteTask(request);
            }else if("deleteTemplate".equals(action)){
                deleteTemplate(request);
            }
            
        }catch(ReportException e){
            status = "-1|" + e.getLocalizedMessage();
            LOG.error(e.getLocalizedMessage(), e);
        }catch(ReportingException e){
            status = "-1|" + e.getLocalizedMessage();
            LOG.error(e.getLocalizedMessage(), e);
        }catch(ObjectExistsException e){
            status = "-1|" + e.getLocalizedMessage();
            LOG.error(e.getLocalizedMessage(), e);
        }catch(Exception e){
            status = ErrorMessages.UNABLE_TO_COMPLETE; //"-1|" + e.getLocalizedMessage();
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        String str = "<result>" + status + "</result>";
        
        response.setContentType("text/xml; charset=utf-8");
        response.getWriter().print(str);
        
        LOG.debug("Execution finished.");
    }
    
    private void addTemplate(HttpServletRequest request) throws ServletException, RemoteException, ObjectExistsException, ReportingException, JSONException{
        String templateFileId = request.getParameter("templateFileId");
        byte[] template = UploadFile.getUploadedFile(templateFileId);
        
        if (template != null) {
            ReportingManagementService rm = Env.getReportManager(request);
            try{
                rm.addReportTemplate(new String(template));
                LOG.info("Template was added.");
            }finally{
                UploadFile.deleteUploadedFile(templateFileId);
            }
        }
    }
    
    private void deleteReportsAndCommands(HttpServletRequest request) throws ServletException, RemoteException, ReportingException, JSONException{
        String reportIdsStr =  request.getParameter("reportIds");
        ReportingManagementService rm = Env.getReportManager(request);
        String[] idsArr = {};
        if(reportIdsStr != null && !"".equals(reportIdsStr)) {
            idsArr = reportIdsStr.split(",");
            for(String reportId : idsArr) {
                rm.removeReport(Integer.parseInt(reportId));
            }
            LOG.info("Reports were removed");
        }
        
        String commandIdsStr =  request.getParameter("commandIds");
        if(commandIdsStr != null && !"".equals(commandIdsStr)) {
            idsArr = commandIdsStr.split(",");
            for(String commandId : idsArr) {
                rm.clearReportCommand(Integer.parseInt(commandId));
            }
            LOG.info("Commands was cleared");
        }
    }
    
    private void deleteTemplate(HttpServletRequest request) throws ServletException, RemoteException, ReportingException, JSONException{
        String templateName = request.getParameter("templateName");
        ReportingManagementService rm = Env.getReportManager(request);
        rm.removeReportTemplate(templateName);
        LOG.info("Template was removed");
    }
    
    private void deleteTask(HttpServletRequest request) throws ServletException, RemoteException, ReportingException, JSONException{
        String taskId =  request.getParameter("taskId");
        ReportingManagementService rm = Env.getReportManager(request);

        rm.removeReportTask(Integer.parseInt(taskId));
    }

    private String update(HttpServletRequest request) throws ServletException, RemoteException, ReportingException, JSONException, ReportException{
        String result = ErrorMessages.SUCCESS;
        
        ReportingManagementService rm = Env.getReportManager(request);
        ReportTaskInfo taskInfo = prepareTaskInfo(request);
        try {
            // reschedule task
            rm.updateReportTask(taskInfo);
            LOG.info("Report task has been updated");
        } catch (ObjectExistsException e) {
            result = "-1|" + LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request), "message_report_task_is_already_scheduled", taskInfo.getName());
        } catch (ReportingException e) {
            LOG.warn("Failed to update report task", e);
            throw new ReportException("Failed to update report task");
        }
        return result;
    }
    
    private String schedule(HttpServletRequest request)throws ServletException, RemoteException, ReportingException, JSONException, ReportException{
        String result = ErrorMessages.SUCCESS;
        
        ReportingManagementService rm = Env.getReportManager(request);
        ReportTaskInfo taskInfo = prepareTaskInfo(request);

        try{
            rm.scheduleReportTask(taskInfo);
        } catch (ObjectExistsException e) {
            result = "-1|" + LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request), "message_report_task_is_already_scheduled", taskInfo.getName());
        } catch(Exception e){
            throw new ReportException("Can't schedule task");
        }
        
        LOG.info("Task was scheduled with cron : " + taskInfo.getCron());
        return result;
    }

    private String execute(HttpServletRequest request) throws ServletException, RemoteException, ReportingException, JSONException, ReportException{
        String result = ErrorMessages.SUCCESS;
        
        ReportingManagementService rm = Env.getReportManager(request);
        ReportTaskInfo taskInfo = prepareTaskInfo(request);
        
        try {
            rm.runComplexReportTask(taskInfo);
        } catch (ObjectExistsException e) {
            result = "-1|" + LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request), "message_report_task_is_already_scheduled", taskInfo.getName());
        }
        
        LOG.info("Task run.");
        return result;
    }

    private ReportTaskInfo prepareTaskInfo(HttpServletRequest request) throws JSONException, RemoteException, ReportingException, ReportException{
        String cronExpression =  request.getParameter("cronExpression");
        String recipients = request.getParameter("recipients");
        String[] exportFormats =  request.getParameter("exportFormats").split(",");

        String templateName =  request.getParameter("templateName");
        String reportName =  request.getParameter("reportName");
        String params =  request.getParameter("params");

        ReportingManagementService rm = Env.getReportManager(request);
        ReportTemplateInfo templateInfo = rm.getReportTemplateInfo(templateName);

        Collection<ReportParameter> reportParameters = templateInfo.getParameters();
        Map<String, Class<?>> parsTypes = new HashMap<String, Class<?>>();

        for(ReportParameter rp : reportParameters){
            parsTypes.put(rp.getName(), rp.getType());
        }
        JSONArray jsonArray = new JSONArray(params);


        ReportTaskInfo taskInfo = ReportTaskInfo.newInstance(templateName, reportName);

        String taskId =  request.getParameter("taskId");
        // check taskId is not null cause this method is used for two purposes
        // to schedule new tasks and to update existing ones
        if (taskId != null) {
          taskInfo.setTaskId(Integer.parseInt(taskId));
        }
        taskInfo.setCron(cronExpression);
        taskInfo.addExportFormats(Arrays.asList(exportFormats));

        for(int i = 0; i < jsonArray.length(); i++) {
            JSONObject obj = jsonArray.getJSONObject(i);
            String paramName = obj.getString("name");
            Object value = obj.get("value");

            Class<?> parClass = parsTypes.get(paramName);
            Object paramValue = rm.parseReportParameterValue(value.toString(), parClass);
            taskInfo.addParameterValue(paramName, paramValue);
        }

        if(recipients != null && !recipients.isEmpty()) {
            for(String recipient : recipients.split(",")) {
                taskInfo.addMailRecipient(recipient);
            }
        }
        
        return taskInfo;
    }

}
