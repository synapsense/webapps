package com.synapsense.reports;

import com.synapsense.dataAccess.Env;
import com.synapsense.service.FileWriter;
import com.synapsense.utilities.BaseHttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.File;
import java.io.IOException;


public class GetReport extends BaseHttpServlet {	
    private static final long serialVersionUID = 1L;
    private static final Log LOG = LogFactory.getLog(GetReport.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
            LOG.info("GetReport: " + request.getParameter("url"));
            getReport(request, response);
     }
    
    private void getReport(HttpServletRequest request, HttpServletResponse response)
                    throws IOException, ServletException {
        
        ServletOutputStream outputStream = response.getOutputStream();
        //First we need to get URL after that we need to get file by chunks using that URL
        //Get URL
        FileWriter fws = Env.getFileWritter();

        String fileName = request.getParameter("url");
        File f = new File(fileName);
        String baseName = f.getName();
        
        response.addHeader("Content-Disposition", "filename=" + baseName);
        
        //Get file by chunks
        byte data[] = null;
        try {
            long offset = 0;
            while ((data = fws.readFileBytes(fileName, (int) offset)) != null) {
                offset += data.length;
                outputStream.write(data);
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
    } 
}
