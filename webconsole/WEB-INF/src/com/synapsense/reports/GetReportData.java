package com.synapsense.reports;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.reporting.dto.CommandStatus;
import com.synapsense.service.reporting.dto.RemoteCommandProxy;
import com.synapsense.service.reporting.dto.ReportInfo;
import com.synapsense.service.reporting.dto.ReportTaskInfo;
import com.synapsense.service.reporting.dto.ReportTemplateInfo;
import com.synapsense.service.reporting.dto.ReportTitle;
import com.synapsense.service.reporting.management.ReportParameter;
import com.synapsense.service.reporting.management.ReportingException;
import com.synapsense.service.reporting.management.ReportingManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.CollectionUtils;
import com.synapsense.utilities.StringUtils;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.lang.LangManager;

public class GetReportData extends HttpServlet {

    private static final long serialVersionUID = 5272217835959650509L;
    private static final Log LOG = LogFactory.getLog(GetReportData.class);
    
    private static final String SCHEDULE_CUSTOM_QUERIES = "SCHEDULE_CUSTOM_QUERIES";
    private static final String MANAGE_REPORT_TEMPLATES = "MANAGE_REPORT_TEMPLATES";
    final static String DIMENSION_NAME = "dimensionName";
    final static String PROPERTY_NAME = "propertyName";

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.debug("Execution started");
        String action = request.getParameter("action");

        Document resultDoc = null;

        try {
            if ("getTemplates".equals(action)) {
                resultDoc = getTemplates(request);
            } else if ("getTemplateDescription".equals(action)) {
                resultDoc = getTemplateData(request);
            } else if ("getCommon".equals(action)) {
                resultDoc = getCommonReports(request);
            } else if ("getScheduled".equals(action)) {
                resultDoc = getScheduledTasks(request);
            } else if ("getTaskDescription".equals(action)) {
                resultDoc = getTaskDescription(request);
            } else if ("getReportView".equals(action)) {
                resultDoc = getReportView(request);
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        String str = "<result></result>";
        if (resultDoc != null) {
            str = XMLHelper.getDocumentXml(resultDoc);
        }

        response.setContentType("text/xml; charset=utf-8");
        response.getWriter().print(str);

        LOG.debug("Execution finished");
    }

    private Document getReportView(HttpServletRequest request) throws ParserConfigurationException, RemoteException,
            ReportingException {
        Document resultDoc = null;

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();

        int requestId = Integer.parseInt(request.getParameter("reportId"));
        ReportingManagementService rm = Env.getReportManager(request);

        ReportInfo generatedReportInfo = rm.getReportInfo(requestId);

        Map<String, String> map = generatedReportInfo.getExportedMap();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            Element newelem = resultDoc.createElement("entry");
            newelem.setAttribute("format", entry.getKey());
            newelem.setAttribute("url", entry.getValue());
            resultDocRoot.appendChild(newelem);
        }

        return resultDoc;
    }

    private Document getTaskDescription(HttpServletRequest request) throws ParserConfigurationException,
            RemoteException, ReportingException {
        Document resultDoc = null;

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();

        String taskId = request.getParameter("taskId");
        String templateName = request.getParameter("templateName");

        ReportingManagementService rm = Env.getReportManager(request);
        ReportTaskInfo taskInfo = rm.getReportTask(Integer.parseInt(taskId));
        if (taskInfo == null)
            return resultDoc;
        
        Element parametersRoot = resultDoc.createElement("parameters");
        parametersRoot.setAttribute("reportName", taskInfo.getName());
        
        Element formatElem = resultDoc.createElement("taskDescription");
        
        formatElem.setAttribute("exportFormats", StringUtils.join(taskInfo.getExportFormats(), ","));
        formatElem.setAttribute("cronExpression", taskInfo.getCron());
        resultDocRoot.appendChild(formatElem);
        
        ReportTemplateInfo rti = rm.getReportTemplateInfo(templateName);
        Collection<ReportParameter> crp = rti.getParameters();
        
        for(Map.Entry<String, Object> entry : taskInfo.getParametersValues().entrySet()){
            Element newelem = resultDoc.createElement("parameter");
            newelem.setAttribute("name", entry.getKey());
            newelem.setAttribute("type", entry.getValue().getClass().toString());
            for(ReportParameter rp : crp) {
                if(rp.getName().equals(entry.getKey())) {
                    String description = rp.getDesription();
                    try {
                        if (description != null
                                && new JSONObject(description).has("isChartCustomizable")) {
                            newelem.setAttribute(
                                    "description",
                                    new JSONObject(description).getBoolean("isChartCustomizable") 
                                    ? addAllPropertiesAndDimensionToDescription(description)
                                            : description); 
                        } else {
                            newelem.setAttribute("description", rp.getDesription());
                        }
                    } catch (JSONException e) {
                        LOG.error(e.getLocalizedMessage(), e);
                    }
                }
            }
            newelem.setAttribute("value", String.valueOf(entry.getValue()));
            parametersRoot.appendChild(newelem);
        }
        resultDocRoot.appendChild(parametersRoot);
        
        Set<String> usr = taskInfo.getEmailRecipients();
        Element usersRoot = resultDoc.createElement("users");
        UserManagementService ums = Env.getUserDAO();
        Collection<TO<?>> users = ums.getAllUsers();
        for(TO<?> user : users) {
            Element newelem = resultDoc.createElement("user");
            String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
            String firstName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_FIRST_NAME);
            String lastName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_LAST_NAME);
            String email = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_PRIMARY_EMAIL);
            newelem.setAttribute("userName", userName);
            newelem.setAttribute("name", lastName + ", " + firstName);
            newelem.setAttribute("email", email);
            newelem.setAttribute("isSelected", String.valueOf(usr.contains(userName)));
            usersRoot.appendChild(newelem);
        }
        resultDocRoot.appendChild(usersRoot);

        return resultDoc;
    }

    private Document getCommonReports(HttpServletRequest request) throws ParserConfigurationException, RemoteException,
            ReportingException {

        Document resultDoc = null;

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();
        Document reportsDoc = XMLHelper.createNewXmlDocument("reports");
        Element reportsRoot = reportsDoc.getDocumentElement();

        ReportingManagementService rm = Env.getReportManager(request);

        Collection<RemoteCommandProxy> commands = rm.getReportCommands();

        // add uncompleted commands
        for (RemoteCommandProxy command : commands) {
            int commandId = command.getCommandId();
            if (CommandStatus.COMPLETED.equals(rm.getReportCommandStatus(commandId))) {// show
                                                                                       // only
                                                                                       // uncompleted
                                                                                       // commands
                continue;
            }

            Element newelem = reportsDoc.createElement("report");
            String reportName = command.getCommandName();
            if (command.getTaskInfo() != null) {
                reportName = command.getTaskInfo().getName();
            }

            newelem.setAttribute("timestamp", "N/A");
            newelem.setAttribute("lt", "0");
            newelem.setAttribute("name", reportName);				
            newelem.setAttribute("commandId", String.valueOf(command.getCommandId()));				
            newelem.setAttribute("progress", String.valueOf(rm.getReportCommandProgress(commandId) * 100));
            this.addStatusInfoAttributes(newelem, rm.getReportCommandStatus(commandId), request);

            reportsRoot.appendChild(newelem);
        }

        // add generated reports
        SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),
                UsersDataOptions.getTzIdFromRequest(request));
        
        Collection<ReportTitle> rTitles = rm.getGeneratedReports();

        for (ReportTitle r : rTitles) {
            Element newelem = reportsDoc.createElement("report");
            newelem.setAttribute("name", r.getTaskName());
            newelem.setAttribute("timestamp", sdf.format(r.getTimestamp()));
            newelem.setAttribute("lt", String.valueOf(r.getTimestamp()));
            newelem.setAttribute("reportId", String.valueOf(r.getReportId()));
            newelem.setAttribute("url", "none");
            newelem.setAttribute("progress", "100"); // 100%, since report is
            this.addStatusInfoAttributes(newelem, CommandStatus.COMPLETED, request);
            reportsRoot.appendChild(newelem);
        }

        resultDocRoot.appendChild(resultDoc.importNode(reportsRoot, true));

        return resultDoc;
    }

    private Document getTemplateData(HttpServletRequest request) throws ParserConfigurationException, RemoteException,
            ReportingException {
        Document resultDoc = null;

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();

        ReportingManagementService rm = Env.getReportManager(request);
        String templateName = request.getParameter("templateName");
        ReportTemplateInfo tInfo = null;

        if (templateName != null && !templateName.isEmpty()) {
            tInfo = rm.getReportTemplateInfo(templateName);
        }

        Element templatesRoot = resultDoc.createElement("parameters");
        if (tInfo != null) {
            Collection<ReportParameter> params = tInfo.getParameters();
            for (ReportParameter rp : params) {
                Element newelem = resultDoc.createElement("parameter");
                newelem.setAttribute("name", rp.getName());
                newelem.setAttribute("type", rp.getType().toString());
                
                String description = rp.getDesription();
                try {
                    if (description != null
                            && new JSONObject(description).has("isChartCustomizable")) {
                        newelem.setAttribute(
                                "description",
                                new JSONObject(description).getBoolean("isChartCustomizable") 
                                ? addAllPropertiesAndDimensionToDescription(description)
                                        : description); 
                    } else {
                        newelem.setAttribute("description", rp.getDesription());
                    }
                } catch (JSONException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
                
                templatesRoot.appendChild(newelem);
            }
        }
        resultDocRoot.appendChild(templatesRoot);

        Element usersRoot = resultDoc.createElement("users");
        UserManagementService ums = Env.getUserDAO();
        Collection<TO<?>> users = ums.getAllUsers();
        for(TO<?> user : users) {
            Element newelem = resultDoc.createElement("user");
            String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
            String firstName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_FIRST_NAME);
            String lastName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_LAST_NAME);
            String email = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_PRIMARY_EMAIL);
            newelem.setAttribute("userName", userName);
            newelem.setAttribute("name", lastName + ", " + firstName);
            newelem.setAttribute("email", email);
            newelem.setAttribute("isSelected", "false");
            usersRoot.appendChild(newelem);
        }

        resultDocRoot.appendChild(usersRoot);
        return resultDoc;
    }

    private Document getTemplates(HttpServletRequest request) throws ParserConfigurationException, RemoteException,
            ReportingException {
        Document resultDoc = null;

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();
        Document templatesDoc = XMLHelper.createNewXmlDocument("templates");
        Element templatesRoot = templatesDoc.getDocumentElement();

        ReportingManagementService rm = Env.getReportManager(request);

        Collection<String> templates = rm.getAvailableReportTemplates();

        String userName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> user = Env.getUserDAO().getUser(userName);
        boolean canScheduleSystem = UserUtils.hasPrivilege(user, SCHEDULE_CUSTOM_QUERIES);
        boolean canManage = UserUtils.hasPrivilege(user, MANAGE_REPORT_TEMPLATES);
        
        for (String template : templates) {
            boolean isSystem = rm.getReportTemplateInfo(template).isSystem();
            if (isSystem && canScheduleSystem || canManage) {
                Element newelem = templatesDoc.createElement("template");
                newelem.setAttribute("name", template);
                newelem.setAttribute("system", Boolean.toString(isSystem));
                templatesRoot.appendChild(newelem);
            }
        }

        resultDocRoot.appendChild(resultDoc.importNode(templatesRoot, true));

        return resultDoc;
    }

    private Document getScheduledTasks(HttpServletRequest request) throws ParserConfigurationException,
            RemoteException, ReportingException {
        Document resultDoc = null;
        String templateName = request.getParameter("templateName");

        resultDoc = XMLHelper.createNewXmlDocument("result");
        Element resultDocRoot = resultDoc.getDocumentElement();
        Element templatesRoot = resultDoc.createElement("tasks");

        ReportingManagementService rm = Env.getReportManager(request);

        Collection<ReportTaskInfo> taskInfos = rm.getReportTasks(templateName);
        for (ReportTaskInfo info : taskInfos) {
            Element newelem = resultDoc.createElement("task");
            newelem.setAttribute("name", info.getName());
            newelem.setAttribute("id", String.valueOf(info.getTaskId()));
            templatesRoot.appendChild(newelem);
        }

        resultDocRoot.appendChild(templatesRoot);
        return resultDoc;
    }

    private void addStatusInfoAttributes(Element xmlElement, CommandStatus status, HttpServletRequest request) {
        String statusCode = "Unknown";
        String statusPrompt = "Unknown";

        if (CommandStatus.CANCELLED.equals(status)) {
            statusCode = "CANCELLED";
            statusPrompt = "Cancelled";
        } else if (CommandStatus.COMPLETED.equals(status)) {
            statusCode = "COMPLETED";
            statusPrompt = "Completed";
        } else if (CommandStatus.FAILED.equals(status)) {
            statusCode = "FAILED";
            statusPrompt = "Failed";
        } else if (CommandStatus.IN_PROGRESS.equals(status)) {
            statusCode = "IN_PROGRESS";
            statusPrompt = "In progress";
        } else if (CommandStatus.NOT_STARTED.equals(status)) {
            statusCode = "NOT_STARTED";
            statusPrompt = "Not started";
        } else if (CommandStatus.SUSPENDED.equals(status)) {
            statusCode = "SUSPENDED";
            statusPrompt = "Suspended";
        }

        LangManager langMan = LangManager.getInstance();
        Locale localeId = LangManager.getCurrentLocale(request);

        xmlElement.setAttribute("status", statusCode);
        xmlElement.setAttribute("statusLabel", langMan.getLocaleString(localeId, statusPrompt));
        xmlElement.setAttribute("statusMessage", status.getStatusMessage());
    }
    
    private String addAllPropertiesAndDimensionToDescription(String description) throws JSONException {
        List<JSONObject> allDimensions = CollectionUtils.newList();
        
        JSONObject descriptionJSON = new JSONObject(description);
        JSONArray itemsArray = descriptionJSON.getJSONArray("items");
        for (int i = 0; i < itemsArray.length(); i++) {
            JSONObject item = (JSONObject) itemsArray.get(i);
            String fullProperty = (String) item.get("value");
            String [] propertyArray = fullProperty.split("\\.");
            String propertyName = propertyArray[1];
            String objectTypeName = propertyArray[0];
            String dataclass = TypeUtil.getInstance().getPropertyDataclass(objectTypeName, propertyName);
            allDimensions.add(new JSONObject().put(
                    DIMENSION_NAME, dataclass).put(PROPERTY_NAME, fullProperty));
        }

        descriptionJSON.put("dimensions", new JSONArray(allDimensions));
        return descriptionJSON.toString();
    }
}
