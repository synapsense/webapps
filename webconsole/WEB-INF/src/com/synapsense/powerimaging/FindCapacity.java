package com.synapsense.powerimaging;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class FindCapacity extends BaseHttpServlet {
    public static final long serialVersionUID = 0;

    // private static final Log LOG = LogFactory.getLog(FindCapacity.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {

            getRacks(request, response);
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    private void getRacks(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ParserConfigurationException, EnvException, ServletException {
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 1);
        response.setDateHeader("Last-modified", System.currentTimeMillis());
        sendResponse(response, getRacksDoc(request));
    }

    private Document getRacksDoc(HttpServletRequest request) throws ParserConfigurationException, EnvException {
        String idStr = request.getParameter("id");

        String uSpaceStr = request.getParameter("uSpace");
        Integer uSpaceRequired = null;
        if (uSpaceStr != null && !uSpaceStr.isEmpty()) {
            uSpaceRequired = Integer.valueOf(uSpaceStr);
        }

        String property = request.getParameter("property");
        String capacityStr = request.getParameter("capacity");
        Double capacityRequired = null;
        if (capacityStr != null && !capacityStr.isEmpty()) {
            capacityRequired = Double.parseDouble(capacityStr);
        }

        TO<?> parent = TOFactory.getInstance().loadTO(idStr);
        Environment env = Env.getEnv(request);

        Document doc = XMLHelper.createNewXmlDocument("objects");

        Collection<TO<?>> racks = env.getRelatedObjects(parent, EnvironmentTypes.TYPE_POWER_RACK, true);
        if (racks.isEmpty()) {
            return doc;
        }

        String[] propArray = { "name", "uSpaceAvailable", "maxUBlock", "envRack" };

        Collection<CollectionTO> objects = env.getPropertyValue(racks, propArray);

        TO<?> dc = parent;
        if (!EnvironmentTypes.TYPE_DC.equals(parent.getTypeName())) {
            Collection<TO<?>> dcs = env.getParents(parent, EnvironmentTypes.TYPE_DC);
            if (!dcs.isEmpty()) {
                dc = dcs.iterator().next();
            }
        }
        Double dcPowerFactor = env.getPropertyValue(dc, "powerFactor", Double.class);

        // First we need to filter out racks that don't have available uspace

        Collection<CollectionTO> filteredRacks = new ArrayList<CollectionTO>();
        if (uSpaceRequired == null) {
            // No u space set
            filteredRacks = objects;
        } else {
            for (CollectionTO obj : objects) {
                Integer maxUBlock = (Integer) obj.getSinglePropValue("maxUBlock").getValue();
                if (maxUBlock != null && maxUBlock >= uSpaceRequired) {
                    filteredRacks.add(obj);
                }
            }
        }

        Element root = doc.getDocumentElement();
        NumberFormat formatter = CommonFunctions.getNumberFormat(request);
        HashMap<TO<?>, List<Element>> envRackBuff = new HashMap<TO<?>, List<Element>>();

        // Now take all the phases and check availability
        for (CollectionTO rackColTO : filteredRacks) {

            String[] phaseProps = new String[] { "name", "maxCurrentThreshold", "maxHistCurrent", "maxPowerThreshold",
                    "maxHistApPower" };
            Collection<TO<?>> phases = env.getRelatedObjects(rackColTO.getObjId(), EnvironmentTypes.TYPE_PHASE, true);
            if (phases.isEmpty()) {
                continue;
            }
            Collection<CollectionTO> phaseVals = env.getPropertyValue(phases, phaseProps);

            for (CollectionTO phaseColTO : phaseVals) {
                if (capacityRequired != null) {
                    Double threshold = null;
                    Double value = null;

                    if ("current".equals(property)) {
                        threshold = (Double) phaseColTO.getSinglePropValue("maxCurrentThreshold").getValue();
                        value = (Double) phaseColTO.getSinglePropValue("maxHistCurrent").getValue();
                    } else if ("power".equals(property) || "demandPower".equals(property)) {
                        threshold = (Double) phaseColTO.getSinglePropValue("maxPowerThreshold").getValue();
                        value = (Double) phaseColTO.getSinglePropValue("maxHistApPower").getValue();
                    }

                    double capacityAvailable;
                    if (threshold == null || value == null || ("demandPower".equals(property) && dcPowerFactor == null)) {
                        continue;
                    } else {
                        capacityAvailable = threshold - value;
                        if ("demandPower".equals(property)) {
                            capacityAvailable *= dcPowerFactor;
                        }
                        if (capacityAvailable < capacityRequired) {
                            continue;
                        }
                    }
                }

                Element rackElem = doc.createElement(rackColTO.getObjId().getTypeName());
                root.appendChild(rackElem);
                rackElem.setAttribute("id", TOFactory.getInstance().saveTO(rackColTO.getObjId()));
                rackElem.setAttribute("name", rackColTO.getSinglePropValue("name").getValue().toString());
                rackElem.setAttribute(
                        "rpduName",
                        env.getPropertyValue(env.getParents(phaseColTO.getObjId(), EnvironmentTypes.TYPE_RPDU)
                                .iterator().next(), "name", String.class));
                rackElem.setAttribute("phaseName", phaseColTO.getSinglePropValue("name").getValue().toString());

                Double maxHistCurrent = (Double) phaseColTO.getSinglePropValue("maxHistCurrent").getValue();
                Double maxCurrentThreshold = (Double) phaseColTO.getSinglePropValue("maxCurrentThreshold").getValue();
                if (maxHistCurrent != null && maxCurrentThreshold != null) {
                    rackElem.setAttribute("availableCurrent", formatter.format(maxCurrentThreshold - maxHistCurrent));
                }

                Double maxHistApPower = (Double) phaseColTO.getSinglePropValue("maxHistApPower").getValue();
                Double maxPowerThreshold = (Double) phaseColTO.getSinglePropValue("maxPowerThreshold").getValue();
                if (maxPowerThreshold != null && maxHistApPower != null) {
                    rackElem.setAttribute("availablePower", formatter.format(maxPowerThreshold - maxHistApPower));
                }

                if (maxPowerThreshold != null && maxHistApPower != null && dcPowerFactor != null) {
                    rackElem.setAttribute("availableDemandPower",
                            formatter.format((maxPowerThreshold - maxHistApPower) * dcPowerFactor));
                }

                Integer maxUBlock = (Integer) rackColTO.getSinglePropValue("maxUBlock").getValue();
                if (maxUBlock != null) {
                    rackElem.setAttribute("maxUBlock", Integer.toString(maxUBlock));
                }

                Integer uSpaceAvailable = (Integer) rackColTO.getSinglePropValue("uSpaceAvailable").getValue();
                if (uSpaceAvailable != null) {
                    rackElem.setAttribute("availableUSpace", uSpaceAvailable.toString());
                }

                TO<?> envRack = (TO<?>) rackColTO.getSinglePropValue("envRack").getValue();
                if (envRack != null) {
                    rackElem.setAttribute("envRack", TOFactory.getInstance().saveTO(envRack));
                    List<Element> rackElements = envRackBuff.get(envRack);
                    if (rackElements == null) {
                        rackElements = new ArrayList<Element>();
                        envRackBuff.put(envRack, rackElements);
                    }
                    rackElements.add(rackElem);
                }
            }
        }

        String[] sensorProps = { "cTop", "cMid", "cBot" };
        // converting to hash set cause it is serializable
        if (envRackBuff.isEmpty()) {
            return doc;
        }
        Collection<CollectionTO> sensorColTO = env.getPropertyValue(new HashSet<TO<?>>(envRackBuff.keySet()),
                sensorProps);
        HashMap<TO<?>, List<Element>> rackSensorsMap = new HashMap<TO<?>, List<Element>>();
        for (CollectionTO sColTO : sensorColTO) {
            List<Element> rackElems = envRackBuff.get(sColTO.getObjId());
            for (Element rackElem : rackElems) {
                for (String sensorName : sensorProps) {
                    TO<?> sensor = (TO<?>) sColTO.getSinglePropValue(sensorName).getValue();
                    Element sensorElem = doc.createElement(sensorName);
                    rackElem.appendChild(sensorElem);

                    List<Element> sensorElems = rackSensorsMap.get(sensor);
                    if (sensorElems == null) {
                        sensorElems = new ArrayList<Element>();
                        rackSensorsMap.put(sensor, sensorElems);
                    }
                    sensorElems.add(sensorElem);
                }
            }
        }
        if (rackSensorsMap.isEmpty()) {
            return doc;
        }
        Collection<CollectionTO> sensorValues = env.getPropertyValue(new HashSet<TO<?>>(rackSensorsMap.keySet()),
                new String[] { "lastValue", "aMin", "aMax", "rMin", "rMax" });
        for (CollectionTO sensValue : sensorValues) {
            for (ValueTO value : sensValue.getPropValues()) {
                List<Element> sensorElems = rackSensorsMap.get(sensValue.getObjId());
                for (Element sensorElem : sensorElems) {
                    Element valueElem = doc.createElement(value.getPropertyName());
                    sensorElem.appendChild(valueElem);
                    valueElem.setAttribute("v", formatter.format(value.getValue()));
                }
            }
        }
        return doc;
    }
}