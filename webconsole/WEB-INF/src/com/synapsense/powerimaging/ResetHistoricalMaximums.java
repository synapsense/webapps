package com.synapsense.powerimaging;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.es.EnvironmentTypes;

public class ResetHistoricalMaximums extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final String[] childTypes = {EnvironmentTypes.TYPE_RPDU, 
                                                EnvironmentTypes.TYPE_PHASE, 
                                                EnvironmentTypes.TYPE_SERVER,
                                                EnvironmentTypes.TYPE_PLUG};

    private static final Log LOG = LogFactory.getLog(ResetHistoricalMaximums.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            Environment env = Env.getEnv(request);
            String[] idStrs = request.getParameter("ids").split(",");
            for (String idStr : idStrs) {
                TO<?> to = TOFactory.getInstance().loadTO(idStr);
                resetHistoricalMaximums(env, to);
            }
            sendResponse(response, "<result>" + ErrorMessages.SUCCESS + "</result>");
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    private void resetHistoricalMaximums(Environment env, TO<?> to) throws PropertyNotFoundException, UnableToConvertPropertyException {
        Collection<TO<?>> powerObjects = new HashSet<TO<?>>();
        powerObjects.add(to);
        
        for (String childType : childTypes) {
            powerObjects.addAll(env.getRelatedObjects(to, childType, true));
        }
        
        for (TO<?> powerObject : powerObjects) {
            try {
                Double maxApPower = env.getPropertyValue(powerObject, "maxApPower", Double.class);
                env.setPropertyValue(powerObject, "maxHistApPower", maxApPower);
                
                if (powerObject.getTypeName().equals(EnvironmentTypes.TYPE_PLUG)
                        || powerObject.getTypeName().equals(EnvironmentTypes.TYPE_PHASE)) {
                    Double maxCurrent = env.getPropertyValue(powerObject, "maxCurrent", Double.class);
                    env.setPropertyValue(powerObject, "maxHistCurrent", maxCurrent);
                    if (powerObject.getTypeName().equals(EnvironmentTypes.TYPE_PHASE)) {
                        env.setPropertyValue(powerObject, "potentialMaxCurrent", maxCurrent);
                    }
                }
                
                if (!powerObject.getTypeName().equals(EnvironmentTypes.TYPE_RPDU) &&
                    !powerObject.getTypeName().equals(EnvironmentTypes.TYPE_PLUG)) {
                    env.setPropertyValue(powerObject, "lastResetTime", System.currentTimeMillis());
                }
                if (!powerObject.getTypeName().equals(EnvironmentTypes.TYPE_SERVER) &&
                        !powerObject.getTypeName().equals(EnvironmentTypes.TYPE_PLUG)) {
                    env.setPropertyValue(powerObject, "potentialMaxApPower", maxApPower);
                }
            } catch (ObjectNotFoundException e) {
                LOG.warn(e.getLocalizedMessage());
            }
        }
    }
}