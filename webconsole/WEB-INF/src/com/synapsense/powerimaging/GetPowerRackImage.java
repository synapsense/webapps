package com.synapsense.powerimaging;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetPowerRackImage extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    public static final int DEFAULT_RACK_HEIGHT = 42;

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        try {
            
            String rackId = request.getParameter("id");
            Environment env = Env.getEnv(request);
            TO<?> rackTO = TOFactory.getInstance().loadTO(rackId);
            String rackName = env.getPropertyValue(rackTO, "name", String.class);
            Integer rackHeight = env.getPropertyValue(rackTO, "height", Integer.class);
            if (rackHeight == null) {
                rackHeight = DEFAULT_RACK_HEIGHT;
            }
            
            Collection<TO<?>> servers = env.getChildren(rackTO, EnvironmentTypes.TYPE_SERVER);
            RackDrawer.Server[] serversArr = new RackDrawer.Server[servers.size()];
            if (!servers.isEmpty()) {
                Collection<CollectionTO> serversProps = env.getPropertyValue(servers, new String[] {"uHeight", "uLocation"}); 
                
                int i = 0;
                for (CollectionTO serverProps : serversProps) {
                    Integer uHeight = (Integer) serverProps.getSinglePropValue("uHeight").getValue();
                    Integer uLocation = (Integer) serverProps.getSinglePropValue("uLocation").getValue();
                    serversArr[i++] = new RackDrawer.Server(uLocation, uHeight);
                }
            }

            RackDrawer.Rack rack = new RackDrawer.Rack(rackName, rackHeight, serversArr);
    
            InputStream[] resources = new InputStream[4];
            resources[0] =  this.getServletContext().getResourceAsStream("/resources/images/patchpanel_top.png");
            resources[1] =  this.getServletContext().getResourceAsStream("/resources/images/patchpanel_mid.png");
            resources[2] =  this.getServletContext().getResourceAsStream("/resources/images/patchpanel_bot.png");
            resources[3] =  this.getServletContext().getResourceAsStream("/resources/images/serverImg.png");

            ImageIO.write(RackDrawer.getRackImage(rack, resources), "PNG", response.getOutputStream());
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }
}
