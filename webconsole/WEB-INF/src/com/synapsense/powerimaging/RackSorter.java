package com.synapsense.powerimaging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.utilities.es.CollectionTOSorter;

public class RackSorter {

    final static double ALLOWABLE_ROW_DEVIATION = 30d;
    
    /**
     * @param racks
     * @return
     */
    public static List<Row> sort(Collection<CollectionTO> racks, String coordinate) {
        
        List<Row>  groups = new ArrayList<Row>();
        
        String otherCoordinate = "x".equals(coordinate) ? "y" : "x";
        
        for (CollectionTO rack : racks) {
            double x = (Double) rack.getSinglePropValue(coordinate).getValue();
            
            Row r = null;
            for (Row row : groups) {
                if (x < row.max + ALLOWABLE_ROW_DEVIATION && x > row.min - ALLOWABLE_ROW_DEVIATION) {
                    r = row;
                    break;
                }
            }
            
            if (r == null) {
                r = new Row();
                groups.add(r);
            }
            
            Collections.sort(groups, new Comparator<Row>() {

                @Override
                public int compare(Row r1, Row r2) {
                    return Double.compare(r1.min, r2.min);
                }
            });
            
            r.addRack(rack, coordinate);
        }

        for (Row row : groups) {
            row.racks = CollectionTOSorter.sortObjects(row.racks, otherCoordinate);
        }
        
        return groups;
    }
    
    /**
     * @param racks
     * @return
     */
    public static List<Row> sort(Collection<CollectionTO> racks) {
        String coordinate = "x";
        if (!racks.isEmpty()) {
            double rotation = (Double) racks.iterator().next().getSinglePropValue("rotation").getValue();
            rotation = Math.abs(rotation % 180);
            if (rotation > 45 && rotation < 135) {
                coordinate = "y";
            }
        }
        return RackSorter.sort(racks, coordinate);
    }
    
    public static class Row {
        
        private List<CollectionTO> racks = new ArrayList<CollectionTO>();
        private double min = Double.POSITIVE_INFINITY;
        private double max = Double.NEGATIVE_INFINITY;
        
        private void addRack(CollectionTO rack, String coordinate) {
            racks.add(rack);
            
            double xory = (Double) rack.getSinglePropValue(coordinate).getValue();
            if (xory > max) {
                max = xory;
            }
            
            if (xory < min) {
                min = xory;
            }
        }
        
        public List<CollectionTO> getRacks() {
            return racks;
        }
    }
}
