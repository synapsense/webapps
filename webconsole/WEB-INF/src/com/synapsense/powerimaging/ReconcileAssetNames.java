package com.synapsense.powerimaging;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.es.EnvironmentTypes;

public class ReconcileAssetNames extends BaseHttpServlet {
        public static final long serialVersionUID = 0;

        public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            doGet(req, resp);
        }
        
        public void doGet(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
            try {
                Environment env = Env.getEnv(request);
                reconcileServerNames(env);
                sendResponse(response, "<result>" + ErrorMessages.SUCCESS + "</result>");
            } catch (Exception e) {
                throw new ServletException(e.getLocalizedMessage(), e);
            }
        }

    private void reconcileServerNames(Environment env) throws PropertyNotFoundException,
            UnableToConvertPropertyException {
            Collection<TO<?>> racks = env.getObjectsByType(EnvironmentTypes.TYPE_POWER_RACK);
            if (!racks.isEmpty()) {
                Collection<CollectionTO> rackObjs = env.getPropertyValue(racks, new String[] {"name"});
                for (CollectionTO rackObj : rackObjs) {
                    try {
                        String rackName = (String) rackObj.getSinglePropValue("name").getValue();
                        Collection<TO<?>> servers = env.getChildren(rackObj.getObjId(), EnvironmentTypes.TYPE_SERVER);
                        if (!servers.isEmpty()) {
                            Collection<CollectionTO> serverObjs = env.getPropertyValue(servers, new String[] {"name", "uLocation"});
                            for (CollectionTO serverObj : serverObjs) {
                                String serverName = (String) serverObj.getSinglePropValue("name").getValue();
                                if (serverName.startsWith("*")) {
                                    Integer uLocation = (Integer) serverObj.getSinglePropValue("uLocation").getValue();
                                    String newServerName = "*" + rackName + "-U" + uLocation;
                                    if (!serverName.equals(newServerName)) {
                                        try {
                                            env.setPropertyValue(serverObj.getObjId(), "name", newServerName);
                                        } catch (ObjectNotFoundException e) {
                                            // deleted?
                                        }
                                    }
                                }
                            }
                        }

                        Collection<TO<?>> rpdus = env.getChildren(rackObj.getObjId(), EnvironmentTypes.TYPE_RPDU);
                        if (!rpdus.isEmpty()) {
                            Collection<CollectionTO> rpduObjs = env.getPropertyValue(rpdus, new String[] {"name", "rId"});
                            for (CollectionTO rpduObj : rpduObjs) {
                                String rpduName = (String) rpduObj.getSinglePropValue("name").getValue();
                                Integer rpduId = (Integer) rpduObj.getSinglePropValue("rId").getValue();
                                String newRpduName = "RPDU_" + (rpduId + 1) + "_" + rackName;
                                if (!newRpduName.equals(rpduName)) {
                                    try {
                                        env.setPropertyValue(rpduObj.getObjId(), "name", newRpduName);
                                    } catch (ObjectNotFoundException e) {
                                        // deleted?
                                    }
                                }
                            }
                        }
                    } catch (ObjectNotFoundException e) {
                        // deleted?
                    }
                }
            }
        }

}
