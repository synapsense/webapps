package com.synapsense.powerimaging;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.EnvironmentTypes;

public class ManageMaxCurrentAndPowerThresholds extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try {
            String action = request.getParameter("action");
            if ("updateAll".equals(action)) {
                updateAllMaxCurrentAndPowerThresholds(request);
                sendResponse(response, "<result>" + ErrorMessages.SUCCESS + "</result>");
            } else if ("set".equals(action)) {
                String status = setMaxThreshold(request);
                sendResponse(response, "<result>" + status + "</result>");
            } else if ("get".equals(action)) {
                sendResponse(response, getRackDocument(request));
            }
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    private void updateAllMaxCurrentAndPowerThresholds(HttpServletRequest request) throws EnvException {
        String idStr = request.getParameter("dc");
        TO<?> dc = TOFactory.getInstance().loadTO(idStr);
        Double derateFactor;
        Environment env = Env.getEnv(request);
        try {
            derateFactor = env.getPropertyValue(dc, "derateFactor", Double.class);
        } catch (ObjectNotFoundException e) {
            // deleted?
            return;
        }

        Collection<TO<?>> rs = env.getRelatedObjects(dc, "POWER_RACK", true);
        if (rs.isEmpty()) {
            return;
        }
        updateRackThresholds(env, rs, derateFactor);
    }

    private static final int DELTA = 1;

    private static String BREAKER_CURRENT = "breakerCurrentThreshold";
    private static String BREAKER_POWER = "breakerPowerThreshold";

    private String setMaxThreshold(HttpServletRequest request) throws EnvException {
        // Input parameters can be either rpdu id or comma-separated list of
        // power rack ids
        String result = ErrorMessages.SUCCESS;
        String rpduIdStr = request.getParameter("rpdu");

        Collection<TO<?>> racks = new HashSet<TO<?>>();
        Environment env = Env.getEnv(request);

        TO<?> dc = TOFactory.getInstance().loadTO(request.getParameter("dc"));
        Double derateFactor = env.getPropertyValue(dc, "derateFactor", Double.class);

        if (rpduIdStr != null) {
            // update one rack's rpdu only
            TO<?> rpdu = TOFactory.getInstance().loadTO(rpduIdStr);
            TO<?> rack = env.getParents(rpdu, EnvironmentTypes.TYPE_POWER_RACK).iterator().next();
            updateBrakersThresholds(rpdu, env, request);
            racks.add(rack);
        } else {
            String rackIdsStr = request.getParameter("racks");
            for (String rackIdStr : rackIdsStr.split(",")) {
                TO<?> rack = TOFactory.getInstance().loadTO(rackIdStr);
                racks.add(rack);
                Collection<TO<?>> rpdus = env.getChildren(rack, EnvironmentTypes.TYPE_RPDU);
                if (!rpdus.isEmpty()) {
                    for (TO<?> rpdu : rpdus) {
                        updateBrakersThresholds(rpdu, env, request);
                    }
                } else {
                    result = ErrorMessages.SOME_RACKS_CAN_NOT_BE_CONFIGURED;
                }
            }
        }

        // update rack's threshold based on new values
        updateRackThresholds(env, racks, derateFactor);
        return result;
    }

    private void updateBrakersThresholds(TO<?> rpdu, Environment env, HttpServletRequest request) throws EnvException {
        int rpduType = env.getPropertyValue(rpdu, "type", Integer.class);
        Map<String, TO<?>> phasesMap = getPhaseLabelMap(rpdu, env);

        if (!phasesMap.isEmpty()) {
            if (rpduType == DELTA) {
                String enLegs = request.getParameter("enLegs");

                String curThreshold = request.getParameter(BREAKER_CURRENT + "A");
                String powerThreshold = request.getParameter(BREAKER_POWER + "A");

                // if at least one is set the other one is set automatically
                if (curThreshold != null) {
                    for (Map.Entry<String, TO<?>> phaseEntry : phasesMap.entrySet()) {
                        TO<?> phase = phaseEntry.getValue();

                        if (curThreshold.isEmpty() || enLegs.indexOf(phaseEntry.getKey()) == -1) {
                            env.setPropertyValue(phase, BREAKER_CURRENT, null);
                            env.setPropertyValue(phase, BREAKER_POWER, null);
                        } else {
                            env.setPropertyValue(phase, BREAKER_CURRENT, Double.valueOf(curThreshold));
                            env.setPropertyValue(phase, BREAKER_POWER, Double.valueOf(powerThreshold));
                        }
                    }
                }
            } else {
                for (Map.Entry<String, TO<?>> entry : phasesMap.entrySet()) {
                    String phaseName = entry.getKey();

                    TO<?> phase = entry.getValue();

                    String curThreshold = request.getParameter(BREAKER_CURRENT + phaseName);
                    String powerThreshold = request.getParameter(BREAKER_POWER + phaseName);

                    // if at least one is set the other one is set automatically
                    if (curThreshold != null) {
                        env.setPropertyValue(phase, BREAKER_CURRENT,
                                curThreshold.isEmpty() ? null : Double.valueOf(curThreshold));
                        env.setPropertyValue(phase, BREAKER_POWER,
                                powerThreshold.isEmpty() ? null : Double.valueOf(powerThreshold));
                    }
                }
            }
        }
    }

    private void updateRackThresholds(Environment env, Collection<TO<?>> rcks, Double derateFactor) throws EnvException {

        Collection<CollectionTO> racks = env.getChildren(rcks, EnvironmentTypes.TYPE_RPDU);

        for (CollectionTO rack : racks) {
            Double rackMaxPowerThreshold = 0D;

            for (ValueTO rpdu : rack.getPropValues()) {
                TO<?> rpduTo = (TO<?>) rpdu.getValue();

                if (!Integer.valueOf(1).equals(env.getPropertyValue(rpduTo, "status", Integer.class))) {
                    continue;
                }

                Double rpduMaxPowerThreshold = null;

                Collection<TO<?>> ps = env.getChildren(rpduTo, EnvironmentTypes.TYPE_PHASE);
                Collection<CollectionTO> phases = env.getPropertyValue(ps, new String[] { "breakerCurrentThreshold",
                        "breakerPowerThreshold" });

                for (CollectionTO phase : phases) {
                    Double breakerCurrentThreshold = (Double) phase.getSinglePropValue("breakerCurrentThreshold")
                            .getValue();
                    setPropertyValue(env, phase.getObjId(), "maxCurrentThreshold",
                            breakerCurrentThreshold == null ? null : breakerCurrentThreshold * derateFactor / 100);

                    Double breakerPowerThreshold = (Double) phase.getSinglePropValue("breakerPowerThreshold")
                            .getValue();
                    if (breakerPowerThreshold != null) {
                        double phaseMaxPowerThreshold = breakerPowerThreshold * derateFactor / 100;
                        setPropertyValue(env, phase.getObjId(), "maxPowerThreshold", phaseMaxPowerThreshold);
                        rpduMaxPowerThreshold = agg(rpduMaxPowerThreshold, phaseMaxPowerThreshold);
                    } else {
                        setPropertyValue(env, phase.getObjId(), "maxPowerThreshold", null);
                    }
                }

                setPropertyValue(env, rpduTo, "maxPowerThreshold", rpduMaxPowerThreshold);

                if (rpduMaxPowerThreshold != null && rackMaxPowerThreshold != null) {
                    rackMaxPowerThreshold = rackMaxPowerThreshold + rpduMaxPowerThreshold;
                } else {
                    rackMaxPowerThreshold = null;
                }
            }
            setPropertyValue(env, rack.getObjId(), "maxPowerThreshold", rackMaxPowerThreshold);
        }
    }

    private Double agg(Double val1, Double val2) {
        if (val1 == null) {
            return val2;
        }
        if (val2 == null) {
            return val1;
        }
        return val1 + val2;
    }

    private <REQUESTED_TYPE> void setPropertyValue(Environment env, TO<?> objId, String propName, REQUESTED_TYPE value)
            throws PropertyNotFoundException, UnableToConvertPropertyException {
        try {
            env.setPropertyValue(objId, propName, value);
        } catch (ObjectNotFoundException e) {
            // deleted?
        }
    }

    private Document getRackDocument(HttpServletRequest request) throws DOMException, ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException, ParserConfigurationException {

        // Input parameters can be either rpdu id or comma-separated list of
        // power rack ids
        String rpduIdStr = request.getParameter("rpdu");

        Collection<TO<?>> racks = new HashSet<TO<?>>();
        Collection<TO<?>> phases = new HashSet<TO<?>>();
        TO<?> rpdu = null;
        Environment env = Env.getEnv(request);

        if (rpduIdStr != null) {
            rpdu = TOFactory.getInstance().loadTO(rpduIdStr);
            racks.add(env.getParents(rpdu, EnvironmentTypes.TYPE_POWER_RACK).iterator().next());
        } else {
            String rackIdsStr = request.getParameter("racks");
            for (String rackIdStr : rackIdsStr.split(",")) {
                racks.add(TOFactory.getInstance().loadTO(rackIdStr));
            }
        }

        Collection<CollectionTO> rackObjs = env.getPropertyValue(racks, new String[] { "name", "nominalVoltage",
                "platform" });
        // Collection<CollectionTO> rackObjs = env.getPropertyValue(racks, new
        // String[] { "name", "nominalVoltage"});
        CollectionTO firstRack = rackObjs.iterator().next();
        TO<?> dc = env.getRelatedObjects(firstRack.getObjId(), EnvironmentTypes.TYPE_DC, false).iterator().next();

        // get derateFactor from parent DC
        Double derateFactor = env.getPropertyValue(dc, "derateFactor", Double.class);
        Double nominalVoltage = (Double) firstRack.getSinglePropValue("nominalVoltage").getValue();
        String platform = (String) firstRack.getSinglePropValue("platform").getValue();
        String enLegs = "";
        StringBuffer name = new StringBuffer();
        StringBuffer id = new StringBuffer();

        NumberFormat numberFormat = CommonFunctions.getNumberFormat(request);
        int numPhases = -1;
        Integer rpduType = null;

        for (CollectionTO rackObj : CollectionTOSorter.sortObjects(rackObjs, "name")) {
            Double nv = (Double) rackObj.getSinglePropValue("nominalVoltage").getValue();
            TO<?> rack = rackObj.getObjId();
            // Don't return nominalVoltage if it is different for different
            // racks
            if (nominalVoltage != null && nv != null && !nv.equals(nominalVoltage)) {
                nominalVoltage = null;
            }

            // join rack names and ids
            name.append(rackObj.getSinglePropValue("name").getValue() + ", ");
            id.append(TOFactory.getInstance().saveTO(rack) + ",");

            // collect all phases
            if (rpdu == null) {
                Collection<TO<?>> rpdus = env.getChildren(rack, EnvironmentTypes.TYPE_RPDU);
                for (TO<?> rackRpdu : rpdus) {
                    Integer type = env.getPropertyValue(rackRpdu, "type", Integer.class);
                    if (rpduType != null && !rpduType.equals(type)) {
                        return getErrorDoument("message/selected_rpdus_have_different_type");
                    }
                    rpduType = type;

                    Collection<TO<?>> rackPhases = env.getChildren(rackRpdu, EnvironmentTypes.TYPE_PHASE);
                    if (numPhases > -1 && numPhases != rackPhases.size()) {
                        return getErrorDoument("message/selected_rpdus_have_different_number_of_phases");
                    }
                    numPhases = rackPhases.size();

                    phases.addAll(rackPhases);
                }

            } else {
                rpduType = env.getPropertyValue(rpdu, "type", Integer.class);
            }
        }

        Document doc = XMLHelper.createNewXmlDocument("POWER_RACK");
        Element root = doc.getDocumentElement();
        root.setAttribute("id", id.substring(0, id.length() - 1));
        root.appendChild(XMLHelper.createXmlNodeWithValue(doc, "name", name.substring(0, name.length() - 2)));
        root.appendChild(XMLHelper.createXmlNodeWithValue(doc, "derateFactor", numberFormat.format(derateFactor)));
        root.appendChild(XMLHelper.createXmlNodeWithValue(doc, "platform", platform));
        if (nominalVoltage != null) {
            root.appendChild(XMLHelper.createXmlNodeWithValue(doc, "nominalVoltage",
                    numberFormat.format(nominalVoltage)));
        }

        Element rpduElem = doc.createElement("RPDU");
        root.appendChild(rpduElem);
        // if we only work with single rpdu
        if (rpdu != null) {
            rpduElem.setAttribute("id", rpduIdStr);
            rpduElem.appendChild(XMLHelper.createXmlNodeWithValue(doc, "name",
                    env.getPropertyValue(rpdu, "name", String.class)));
        }
        
        if (rpduType != null) {
            rpduElem.appendChild(XMLHelper.createXmlNodeWithValue(doc, "type", rpduType.toString()));
        }

        if (rpdu != null) {
            phases.addAll(env.getChildren(rpdu, EnvironmentTypes.TYPE_PHASE));
        }

        Double breakerCurrentThreshold = null;
        Double breakerPowerThreshold = null;
        if (!phases.isEmpty()) {

            for (Map.Entry<String, Collection<CollectionTO>> entry : getPhaseProperties(phases, env,
                    new String[] { "breakerCurrentThreshold", "breakerPowerThreshold", "name" }).entrySet()) {
                Collection<CollectionTO> phaseObjs = entry.getValue();
                String phaseName = entry.getKey();

                CollectionTO firstPhaseObj = phaseObjs.iterator().next();
                breakerCurrentThreshold = (Double) firstPhaseObj.getSinglePropValue("breakerCurrentThreshold")
                        .getValue();
                breakerPowerThreshold = (Double) firstPhaseObj.getSinglePropValue("breakerPowerThreshold").getValue();

                for (CollectionTO phaseObj : phaseObjs) {
                    if (breakerCurrentThreshold == null && breakerPowerThreshold == null) {
                        break;
                    }
                    // Don't return breakerCurrentThreshold if it is different
                    // for
                    // different phases

                    Double bct = (Double) phaseObj.getSinglePropValue("breakerCurrentThreshold").getValue();
                    if (breakerCurrentThreshold != null && bct != null && !bct.equals(breakerCurrentThreshold)) {
                        breakerCurrentThreshold = null;
                    }else if(bct == null){
                        breakerCurrentThreshold = null;
                    }

                    // Don't return breakerPowerThreshold if it is different for
                    // different phases
                    Double bpt = (Double) phaseObj.getSinglePropValue("breakerPowerThreshold").getValue();
                    if (breakerPowerThreshold != null && bpt != null && !bpt.equals(breakerPowerThreshold)) {
                        breakerPowerThreshold = null;
                    }else if( bpt == null){
                        breakerPowerThreshold = null;
                    }

                    if (rpduType != null && rpduType == DELTA) {
                        enLegs += phaseName;
                    }
                }

                rpduElem.appendChild(XMLHelper.createXmlNodeWithValue(doc, "breakerCurrentThreshold" + phaseName,
                        breakerCurrentThreshold != null ? numberFormat.format(breakerCurrentThreshold) : ""));

                rpduElem.appendChild(XMLHelper.createXmlNodeWithValue(doc, "breakerPowerThreshold" + phaseName,
                        breakerPowerThreshold != null ? numberFormat.format(breakerPowerThreshold) : ""));
            }

        }
        if (enLegs.equals("") && rpduType != null && rpduType == DELTA) {
            enLegs = "ABC";
        }
        rpduElem.appendChild(XMLHelper.createXmlNodeWithValue(doc, "enLegs", enLegs));

        return doc;
    }

    private Document getErrorDoument(String errorPrompt) throws ParserConfigurationException {
        Document doc = XMLHelper.createNewXmlDocument("errormessage");
        Element root = doc.getDocumentElement();
        root.setAttribute("prompt", errorPrompt);

        return doc;
    }

    private Map<String, TO<?>> getPhaseLabelMap(TO<?> rpdu, Environment env) throws EnvException {
        Collection<TO<?>> phases = env.getChildren(rpdu, EnvironmentTypes.TYPE_PHASE);
        HashMap<String, TO<?>> phaseLabelMap = new HashMap<String, TO<?>>(3);
        for (TO<?> phase : phases) {
            phaseLabelMap.put(env.getPropertyValue(phase, "name", String.class), phase);
        }
        return phaseLabelMap;
    }

    private Map<String, Collection<CollectionTO>> getPhaseProperties(Collection<TO<?>> phases, Environment env,
            String[] properties) {

        Map<String, Collection<CollectionTO>> result = new HashMap<String, Collection<CollectionTO>>();

        if (!Arrays.asList(properties).contains("name")) {
            properties = Arrays.copyOf(properties, properties.length + 1);
            properties[properties.length - 1] = "name";
        }

        Collection<CollectionTO> phaseObjs = env.getPropertyValue(phases, properties);

        for (CollectionTO phaseObj : phaseObjs) {
            String name = (String) phaseObj.getSinglePropValue("name").getValue();

            Collection<CollectionTO> namedPhases = result.get(name);
            if (namedPhases == null) {
                namedPhases = new HashSet<CollectionTO>();
                result.put(name, namedPhases);
            }
            namedPhases.add(phaseObj);
        }
        
        return result;
    }
}
