package com.synapsense.powerimaging;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import javax.imageio.ImageIO;

/**
 * @author apakhunov
 *
 * This is utility class to draw rack image similar to rackrearview.lzx that appears in
 * rack details dialog.
 *
 */
public class RackDrawer {

    public static final int GAP = 1;
    public static final int RACK_WIDTH = 160;
    public static final int SERVER_WIDTH = 120;
    public static final int SERVER_HEIGHT = 11;
    public static final int HEADER_HEIGHT = 50;
    
    public static BufferedImage getRackImage(Rack rack, InputStream[] resources) throws IOException, NullPointerException {
        
        if (rack == null) {
            throw new NullPointerException("Rack can not be null");
        }
        
        BufferedImage topImage = ImageIO.read(resources[0]);
        BufferedImage midImage = ImageIO.read(resources[1]);
        BufferedImage botImage = ImageIO.read(resources[2]);

        int rackHeight = rack.getuHeight();

        int topHeight = topImage.getHeight() * RACK_WIDTH  / topImage.getWidth();
        int midHeight = (SERVER_HEIGHT + GAP) * rackHeight + GAP;
        int botHeight = botImage.getHeight() * RACK_WIDTH  / botImage.getWidth();

        
        int imageHeight = HEADER_HEIGHT + topHeight + midHeight  + botHeight;
        
        BufferedImage image = new BufferedImage(RACK_WIDTH, imageHeight, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = image.createGraphics();
        
        //set up quality
        RenderingHints renderHints = new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                                 RenderingHints.VALUE_ANTIALIAS_ON);
        renderHints.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHints(renderHints);		
        
        //draw rack name
        g.setColor(Color.BLACK);
        int fontSize = 14;
        Font font = new Font("Calibri", Font.BOLD, fontSize);
        g.setFont(font);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        String rackName = rack.getName();
        int w = fontMetrics.stringWidth(rackName);
        
        //I expect 15 chars will always fit the width
        if (w > RACK_WIDTH && rackName.length() > 15) {
            for (int i = 15; i < rackName.length(); i++) {
                String tmpName = rackName.substring(0, i) + "...";
                if (fontMetrics.stringWidth(tmpName) > RACK_WIDTH) {
                    rackName = rackName.substring(0, i - 1) + "...";
                    w = fontMetrics.stringWidth(rackName);
                    break;
                }
            }
        }
        int titleOffset = (RACK_WIDTH - w) / 2;
        g.drawString(rackName, titleOffset, 20);

        //draw num assets
        Server[] servers = rack.getServers();
        int numAssets;
        if (servers == null) {
            numAssets = 0;
        } else {
            numAssets = servers.length;
        }
        String numAssetsString = numAssets + " Assets";
        w = fontMetrics.stringWidth(numAssetsString);
        titleOffset = (RACK_WIDTH - w) / 2;
        g.drawString(numAssetsString, titleOffset, 40);

        
        //font for u-location
        g.setColor(Color.WHITE);
        fontSize = SERVER_HEIGHT * 2 / 3;
        font = new Font("Calibri", Font.BOLD, fontSize);
        g.setFont(font);
        fontMetrics = g.getFontMetrics(font);
        
        g.translate(0, HEADER_HEIGHT);

        //draw top
        g.drawImage(topImage, 
                0, 0, RACK_WIDTH - 1, topHeight - 1,
                0, 0, topImage.getWidth(), topImage.getHeight(),
                null);
        
        //draw cells
        for (int i = 0; i < rackHeight; i++) {
            int y = topHeight + (SERVER_HEIGHT + GAP) * i;
            
            g.drawImage(midImage, 
                    0, y, RACK_WIDTH - 1, y + SERVER_HEIGHT,
                    0, 0, midImage.getWidth(), midImage.getHeight(),
                    null);
            
            String num = Integer.toString(rackHeight - i);
            g.drawString(num, 17 - fontMetrics.stringWidth(num), y + fontSize + (SERVER_HEIGHT - fontSize) / 2);
        }
        
        //draw servers
        if (servers != null) {
            BufferedImage serverImage = ImageIO.read(resources[3]);
            int serverOffset = (RACK_WIDTH - SERVER_WIDTH) / 2;  
            for (Server server : servers) {
                int y = topHeight + (SERVER_HEIGHT + GAP) * (rackHeight - server.getuLocation() + 1 - server.getuHeight());
                
                
                g.drawImage(serverImage, 
                        serverOffset, y, serverOffset + SERVER_WIDTH - 1, y + SERVER_HEIGHT * server.getuHeight() + GAP * (server.getuHeight() - 1),
                        0, 0, serverImage.getWidth(), serverImage.getHeight(),
                        null);
            }
        }
        
        //draw bottom
        g.drawImage(botImage, 
                0, topHeight + midHeight - 1, RACK_WIDTH - 1, botHeight - 1 + topHeight + midHeight,
                0, 0, botImage.getWidth(), botImage.getHeight(),
                null);
        
        return image;
    }
    
    public static class Rack {

        private String name;

        private int uHeight;
        
        private Server[] servers;

        
        public Rack(String name, int uHeight, Server[] servers) {
            super();
            this.name = name;
            this.uHeight = uHeight;
            this.servers = servers;
        }

        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
        public int getuHeight() {
            return uHeight;
        }
        
        public void setuHeight(int uHeight) {
            this.uHeight = uHeight;
        }
        
        public Server[] getServers() {
            return servers;
        }
        
        public void setServers(Server[] servers) {
            this.servers = servers;
        }
    }

    public static class Server {

        private int uLocation;
        
        private int uHeight;

        public Server(int uPosition, int uHeight) {
            super();
            this.uLocation = uPosition;
            this.uHeight = uHeight;
        }

        public int getuLocation() {
            return uLocation;
        }
        
        public void setuLocation(int uPosition) {
            this.uLocation = uPosition;
        }
        
        public int getuHeight() {
            return uHeight;
        }
        
        public void setuHeight(int uHeight) {
            this.uHeight = uHeight;
        }
    }
}

