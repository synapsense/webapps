package com.synapsense.permissions;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.User;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.UserUtils;

public class PermissionUtils {
    private static final Log LOG = LogFactory.getLog(PermissionUtils.class);
    
    private static Collection<String> getPermitted(String permissions, String entityName) {
        Collection<String> res = Collections.emptyList();
        try {
            if (permissions != null && !permissions.isEmpty()) {
                JSONObject jObj = new JSONObject(permissions);
                if (jObj.has(entityName)) {
                    JSONArray jArr = jObj.getJSONArray(entityName);

                    res = new ArrayList<String>(jArr.length());
                    for (int i = 0; i < jArr.length(); i++) {
                        res.add(jArr.getString(i));
                    }
                }
            }
        } catch (JSONException e) {
            LOG.error(e.getMessage(), e);
        }
        
        return res;
        
    }
    
    public static Collection<String> getPermittedUsers(String permissions) {
        return getPermitted(permissions, "users");
    }

    public static Collection<String> getPermittedGroups(String permissions) {
        return getPermitted(permissions, "groups");
    }
    
    public static boolean isUserPermitted(String permissions, String userName) {
        boolean res = false;
        
        UserManagementService ums = Env.getUserDAO();

        try {
            TO<?> user = ums.getUser(userName);
            if(isEveryonePermitted(permissions) || UserUtils.hasRootPrivilege(user)) {
                return true;
            }

            Collection<String> permittedGroups = getPermittedGroups(permissions);
            Collection<String> permittedUsers = getPermittedUsers(permissions);
            
            //allow no one
            if (permittedUsers.isEmpty() && permittedGroups.isEmpty()) {
                return false;
            }
            
            if(permittedUsers.contains(userName.toLowerCase())) {
                return true;
            }
            
            Collection<String> userGroups = ums.getUserGroupIds(ums.getUser(userName));
            for(String group : userGroups) {
                if(permittedGroups.contains(group)) {
                    res = true;
                    break;
                }
            }
            
        } catch (UserManagementException e) {
            LOG.error(e.getMessage(), e);
        }

        return res;
    }

    public static boolean isEveryonePermitted(String permissions) {
        LOG.debug("isEveryOnePermitted( " + permissions + " )");
        boolean res = false;
        try {
            if (permissions != null && !permissions.isEmpty()) {
                JSONObject jObj = new JSONObject(permissions);
                res = jObj.getBoolean("isEveryonePermitted");
                if (!res) {
                    JSONArray users = jObj.getJSONArray("users");
                    JSONArray groups = jObj.getJSONArray("groups");
                    if (users.length() == 0 && groups.length() == 0) {
                        res = true;
                    }
                }
            } else {
                res = true;
            }
        } catch (JSONException e) {
            LOG.error(e.getMessage(), e);
        }

        LOG.debug("IsEveryonePermitted() returning " + Boolean.toString(res));
        return res;
    }
    
    public static String getPermissionsString(boolean isEveryonePermitted, Collection<String> permittedUsers, Collection<String> permittedGroups) {
        JSONObject jObj = new JSONObject();
        try {
            jObj.put("isEveryonePermitted", isEveryonePermitted);
            if (!isEveryonePermitted) {
                jObj.put("users", permittedUsers);
                jObj.put("groups", permittedGroups);
            }
        
        } catch (JSONException e) {
            LOG.error(e.getMessage(), e);
        }
        return jObj.toString();
        
    }

    public static String validatePermissions(String permissions) {
        LOG.debug("validatePermissions( " + permissions + " )");

        String validatedPermissions = null;
        UserManagementService userDAO = Env.getUserDAO();
        boolean changedPermissions = false;

        try {
            if (permissions == null || permissions.isEmpty()) {
                validatedPermissions = PermissionUtils.getPermissionsString(true, null, null);
            } else {
                boolean isEveryonePermitted;
                try {
                    JSONObject jObj = new JSONObject(permissions);
                    isEveryonePermitted = jObj.getBoolean("isEveryonePermitted");
                } catch (JSONException e1) {
                    isEveryonePermitted = true;
                    changedPermissions = true;
                }

                Collection<String> groupNames = PermissionUtils.getPermittedGroups(permissions);
                Iterator<String> it = groupNames.iterator();
                while (it.hasNext()) {
                    //check group existence
                    String name = it.next();
                    TO<?> group = userDAO.getGroup(name);
                    if (group == null) {
                        changedPermissions = true;
                        it.remove();
                    }
                }

                Collection<String> userNames = PermissionUtils.getPermittedUsers(permissions);
                it = userNames.iterator();
                while (it.hasNext()) {
                    //check user existence
                    String name = it.next();
                    TO<?> user = userDAO.getUser(name);
                    
                    //user was deleted
                    if (user == null) {
                        changedPermissions = true;
                        it.remove();
                    }
                }

                if (!isEveryonePermitted) {
                    if (userNames.isEmpty() && groupNames.isEmpty()) {
                        isEveryonePermitted = true;
                        changedPermissions = true;
                    }
                }
                
                if (changedPermissions) {
                    validatedPermissions = PermissionUtils.getPermissionsString(isEveryonePermitted, userNames, groupNames);
                }
            }
        } catch (Exception e) {
            LOG.error("Failed to validatePermissions (" + e.getLocalizedMessage() + ")");
        }
        
        LOG.debug("validatedPermissions = " + validatedPermissions); 
        return validatedPermissions;
    }

}
