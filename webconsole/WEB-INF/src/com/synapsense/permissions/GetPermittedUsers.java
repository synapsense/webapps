package com.synapsense.permissions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;

public class GetPermittedUsers extends BaseHttpServlet {

    private static final long serialVersionUID = 8396107017395659036L;
    private static final Log LOG = LogFactory.getLog(GetPermittedUsers.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try{
            Document doc = null;
            boolean reverse = "true".equals(request.getParameter("reverse"));
            if (reverse) {
                doc = getNotPermittedUsersDoc(request);
            } else {
                doc = getPermittedUsersDoc(request);
            }
            sendResponse(response, doc);
        }catch(Exception e){
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    private Document getPermittedUsersDoc(HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        Document doc = null;
        
        try {
            doc = XMLHelper.createNewXmlDocument("users");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        if (doc == null) {
            return doc;
        }
        
        Element root = doc.getDocumentElement();

        String permissions = getPermissionsString(request);
        
        //if "permissions" property is null we assume that everybody has access
        if (PermissionUtils.isEveryonePermitted(permissions)) {
            dumpUser(root, "group", Constants.EVERYONE_NAME, Constants.EVERYONE_ID);
            return doc;
        } 

        Collection<String> userNames = PermissionUtils.getPermittedUsers(permissions);
        Collection<String> userGroups = PermissionUtils.getPermittedGroups(permissions);
        
        UserManagementService userDAO = Env.getUserDAO();
        Collection<String> invalidUsers = new HashSet<String>();
        Collection<String> invalidGroups = new HashSet<String>();
        
        for (String groupName : userGroups) {
            
            try {
                //check group existence
                TO<?> group = userDAO.getGroup(groupName);
                userDAO.getGroupUsers(group);
                dumpUser(root, "group", groupName, groupName);
            } catch (UserManagementException e) {
                invalidGroups.add(groupName);
            }
            
        }

        for (String userName : userNames) {
            
            TO<?> user = userDAO.getUser(userName);
            
            //user was deleted
            if (user == null) {
                invalidUsers.add(userName);
                continue;
            }
            
            String firstName = (String) userDAO.getProperty(user, UserManagementService.USER_FIRST_NAME);
            String lastName = (String) userDAO.getProperty(user, UserManagementService.USER_LAST_NAME);
            dumpUser(root, "user", firstName + " " + lastName + " [" + userName + "]", userName);
        }
        
        
        if (!invalidUsers.isEmpty() || !invalidGroups.isEmpty()) {
            userNames.removeAll(invalidUsers);
            userGroups.removeAll(invalidGroups);
            try {
                setPermissionsString(request, PermissionUtils.getPermissionsString(false, userNames, userGroups));
            } catch (EnvException e) {
                LOG.error(e.getLocalizedMessage(), e);
            } 
        }
        
        return doc;
        
    }

    private Document getNotPermittedUsersDoc(HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        Document doc = null;
        
        try {
            doc = XMLHelper.createNewXmlDocument("users");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        if (doc == null) {
            return doc;
        }

        String permissions = request.getParameter("permissions");
        
        Element root = doc.getDocumentElement();
        
        //add "Everyone" option in it is not selected 
        if (!PermissionUtils.isEveryonePermitted(permissions)) {
            dumpUser(root, "group", Constants.EVERYONE_NAME, Constants.EVERYONE_ID);
        }


        Collection<String> userNames = PermissionUtils.getPermittedUsers(permissions);
        Collection<String> userGroups = PermissionUtils.getPermittedGroups(permissions);

        UserManagementService userDAO = Env.getUserDAO();
        Collection<TO<?>> users = userDAO.getAllUsers();
        
        Collection<TO<?>> groups = userDAO.getAllGroups();
        
        List<String> sortedGroupNames = new ArrayList<String>();
        for (TO<?> group : groups) {
            String groupName = (String) userDAO.getProperty(group, UserManagementService.GROUP_NAME);

            //group have permissions, skip
            if (userGroups != null && userGroups.contains(groupName) || UserUtils.isRootGroup(group)) {
                continue;
            }
            
            sortedGroupNames.add(groupName);
        }
        
        Collections.sort(sortedGroupNames);
        
        for (String groupName : sortedGroupNames) {
            dumpUser(root, "group", groupName, groupName);
        }
        
        Map<String, TO<?>> nameToUser = new HashMap<String, TO<?>>();
        List<String> sortedUserNames = new ArrayList<String>();
        for (TO<?> user : users) {
            //admins have all permissions by default
            if (UserUtils.hasRootPrivilege(user)) {
                continue;
            }
            
            String userName = (String) userDAO.getProperty(user, UserManagementService.USER_NAME);

            //user have permissions, skip
            if (userNames != null && userNames.contains(userName)) {
                continue;
            }
            
            sortedUserNames.add(userName);
            nameToUser.put(userName, user);
        }
        
        Collections.sort(sortedUserNames);
        
        for (String userName : sortedUserNames) {
        	TO<?> user = nameToUser.get(userName);
            String firstName = (String) userDAO.getProperty(user, UserManagementService.USER_FIRST_NAME);
            String lastName = (String) userDAO.getProperty(user, UserManagementService.USER_LAST_NAME);
            dumpUser(root, "user", firstName + " " + lastName + " [" + userName + "]", userName);
        }
        
        return doc;
    }
    
    
    private void dumpUser(Element root, String elementName, String displayName, String name) {
        Document doc = root.getOwnerDocument(); 
        
        Node userNode = XMLHelper.createXmlNode(doc, elementName, new HashMap<String,String>());
        root.appendChild(userNode);

        Node userNameStringNode = XMLHelper.createXmlTextNode(doc, "displayName", displayName);
        userNode.appendChild(userNameStringNode);

        Node userNameNode = XMLHelper.createXmlTextNode(doc, "name", name);
        userNode.appendChild(userNameNode);
    }
    
    private String getPermissionsString(HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        Environment env = Env.getEnv(request);
        String objId = request.getParameter("objId");
        TO<?> obj = TOFactory.getInstance().loadTO(objId);
        return env.getPropertyValue(obj, "permissions", String.class);
        
    }
    
    private void setPermissionsString(HttpServletRequest request, String permissions) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        Environment env = Env.getEnv(request); 
        String objId = request.getParameter("objId");
        TO<?> obj = TOFactory.getInstance().loadTO(objId);
        env.setPropertyValue(obj, "permissions", permissions);
        
    }
}
