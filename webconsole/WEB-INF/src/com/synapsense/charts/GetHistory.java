package com.synapsense.charts;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.charts.lvcharts.LvCommonUtils;
import com.synapsense.charts.lvcharts.LvRetrievingUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.sensors.SensorPoint;

public class GetHistory extends BaseHttpServlet {
    private static final long serialVersionUID = 5698577982793562098L;
    private static final Log LOG = LogFactory.getLog(GetHistory.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.info("Execution Started");
        long beginTime = Long.parseLong(request.getParameter("beginTime"));
        long endTime = Long.parseLong(request.getParameter("endTime"));
        String propertyIds = request.getParameter("propIds");
        List<PropertyId> propertyIdsList = new ArrayList<PropertyId>();
        if(!propertyIds.isEmpty()){
            propertyIdsList = Arrays.asList(PropertyId.arrayFromString(propertyIds));
        }
        
        StringBuffer series = new StringBuffer();
        StringBuffer values; 
        StringBuffer times;
        NumberFormat numberFormat = CommonFunctions.getNumberFormat(request);
        
        for(PropertyId propID : propertyIdsList){
            List<SensorPoint> points = LvRetrievingUtils.getPointsForProperty(propID, beginTime, endTime, request);
               
            series.append("<serie pid='").append(propID).append("'");
            series.append(" u='").append(getUnitName(propID, request)).append("'");
            series.append(">");
            
            values = new StringBuffer();
            times = new StringBuffer();
            
            if(points != null && !points.isEmpty()){
                LvCommonUtils.convertPointsToBuffers(values, times, points, numberFormat);
                series.append("<v>")
                      .append(values).append("</v><t>")
                      .append(times).append("</t>");
            }
            
            series.append("</serie>");
        }
        sendResponse(response, "<result>" + series + "</result>");
        LOG.info("Execution Finished");
    }
    
    private static String getUnitName(PropertyId pId, HttpServletRequest request){
        String unitName = "";
        return unitName;
    }
}
