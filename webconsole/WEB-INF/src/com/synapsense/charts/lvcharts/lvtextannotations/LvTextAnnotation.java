package com.synapsense.charts.lvcharts.lvtextannotations;

public class LvTextAnnotation {
    protected long time;
    protected String description;
    protected String id;
    
    public LvTextAnnotation(){		
    }
    
    public LvTextAnnotation(String id, long time, String description){
        this.id = id;
        this.time = time;
        this.description = description;
    }
    
    public long getTime() {
        return time;
    }
    public void setTime(long time) {
        this.time = time;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    
}
