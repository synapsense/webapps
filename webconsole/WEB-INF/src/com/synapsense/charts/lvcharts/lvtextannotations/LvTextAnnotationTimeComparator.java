package com.synapsense.charts.lvcharts.lvtextannotations;

import java.util.Comparator;


public class LvTextAnnotationTimeComparator implements Comparator<Object>{
    boolean isASC = true;
    
    public LvTextAnnotationTimeComparator(){		
    }
    public LvTextAnnotationTimeComparator(boolean is_ASC){
        isASC = is_ASC;
    }
    
    public int compare(Object o1, Object o2){   	
        //for performance: type checking (o1,o2 instanceof LvTextAnnotation) is skipped
        LvTextAnnotation sp1 = (LvTextAnnotation)o1;
        LvTextAnnotation sp2 = (LvTextAnnotation)o2;
            
        int res = 0;
        if(sp1.getTime() > sp2.getTime()){
            res = 1;
        }else if(sp1.getTime() < sp2.getTime()){
            res = -1;
        }
        
        if(!isASC){
            res = -res;
        }
        return res;
    }
}
