package com.synapsense.charts.lvcharts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONException;

import com.synapsense.charts.lvcharts.lvtextannotations.LvTextAnnotation;
import com.synapsense.charts.lvcharts.lvtextannotations.LvTextAnnotationTimeComparator;
import com.synapsense.config.UIConfig;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityLogService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.activitylog.ActivityRecordFilter;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.DataRetrievingUtils;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.es.EnvHistoryUtil;
import com.synapsense.utilities.es.ValueRanges;
import com.synapsense.utilities.indexfinder.IndexFinder;
import com.synapsense.utilities.lang.LangManager;
import com.synapsense.utilities.pointsfilter.PointsFilter;
import com.synapsense.utilities.sensors.PointComparator;
import com.synapsense.utilities.sensors.SensorPoint;
import com.synapsense.utilities.sensors.TimestampPointComparator;
import com.synapsense.utilities.symbolvalues.SymbolValues;

public class LvRetrievingUtils {
    private static final Log LOG = LogFactory.getLog(LvRetrievingUtils.class);
    public static final int MAX_POINTS = 3500;
    private static LvTextAnnotationTimeComparator annotationTimeComparator = new LvTextAnnotationTimeComparator();
    
    public static HashMap<PropertyId, List<SensorPoint>> m(HttpServletRequest request) throws JSONException{
        HashMap<PropertyId, List<SensorPoint>> filteredPoints = null;
        
        //String s = null;
        //s.lastIndexOf(1);
        
        String reqType = request.getParameter("reqType");
        
        if("zoom".equalsIgnoreCase(reqType)){
            filteredPoints = zoom(request);
        }else if("scroll".equalsIgnoreCase(reqType)){
            filteredPoints = addExtra(request);
        }else{
            filteredPoints = getData(request);
        }        
        return filteredPoints;
    }
    
    public static HashMap<PropertyId, List<SensorPoint>> zoom(HttpServletRequest request){
        HashMap<PropertyId, List<SensorPoint>> filteredPointsMap = new HashMap<PropertyId, List<SensorPoint>>();

        double percent = Double.parseDouble(request.getParameter("percent"));
        long beginTime = Long.parseLong(request.getParameter("beginTime"));
        long endTime = Long.parseLong(request.getParameter("endTime"));
        long extraLength = ((Double)((endTime - beginTime) * percent)).longValue();
        
        LvChartID chartID = new LvChartID();
        chartID.setDataFromStr(request.getParameter("lvChartID"));
        
        List<LvPropertyData> propDataList = LvStorage.getInstance().getPropertiesData(chartID, request);
        
        LvPropertyData currPropData = null;
        List<SensorPoint> totalPoints;
        List<SensorPoint> additionPoints = new ArrayList<SensorPoint>();
        List<SensorPoint> points = new ArrayList<SensorPoint>();
        List<SensorPoint> filteredPoints;// = new ArrayList<SensorPoint>();
        
        IndexFinder iFinder = new IndexFinder(false);
        PointsFilter filter = new PointsFilter();

        int step;
        CachedSerieDescr serieDescr = null;
        
        try{

            for(int i = 0; i < propDataList.size(); i++){
                currPropData = propDataList.get(i);
                PropertyId propID = currPropData.getPropertyID();
                totalPoints = currPropData.getTotalPoints();
                
                step = 0;
                filteredPoints = new ArrayList<SensorPoint>();
                
                long date = endTime;
                long date2 = beginTime;
                if(!totalPoints.isEmpty()){
                    date = totalPoints.get(0).time_stamp;
                    date2 = totalPoints.get(totalPoints.size() - 1).time_stamp;
                }
                
                //if points are not already retrieved, retrieve it
                if(date < endTime + extraLength){
                    additionPoints = getPointsForProperty(propID, date, endTime + extraLength, request);
                    additionPoints.addAll(totalPoints);
                    totalPoints = additionPoints;
                }
                
                if(date2 > beginTime - extraLength){
                    additionPoints = getPointsForProperty(propID, beginTime - extraLength, date2, request);
                    totalPoints.addAll(additionPoints);
                }
                
                //get main interval
                int startIndex = iFinder.findIndex(totalPoints, endTime);
                int endIndex = iFinder.findIndex(totalPoints, beginTime);
                
                if(endIndex - startIndex < 2){
                    continue;
                }
                
                //get points in the desired interval
                points = new ArrayList<SensorPoint>();
                points.addAll(totalPoints.subList(startIndex, endIndex));
                
                //filter points
                if(points.size() > MAX_POINTS){
                    step = (points.size() / MAX_POINTS) * 2;
                    filteredPoints = filter.pickByStep(points, step);
                }else{
                    filteredPoints.addAll(points);
                }
                
                //add extra points with step
                //add points to the end of time (but to the array begin)
                int index1 = iFinder.findIndex(totalPoints, endTime);
                int index2 = iFinder.findIndex(totalPoints, endTime + extraLength);
                
                points = new ArrayList<SensorPoint>();
                if(index1 > index2){
                    points.addAll(totalPoints.subList(index2, index1)); 
                }else if(index1 > 0){
                    points.add(totalPoints.get(index1));
                }
                
                if(step != 0){
                    points = filter.pickByStep(points, step);
                }                
                points.addAll(filteredPoints);
                filteredPoints = points;
                
                //add points to the begin of time (but to the array end)
                index1 = iFinder.findIndex(totalPoints, beginTime - extraLength);
                index2 = iFinder.findIndex(totalPoints, beginTime);

                points = new ArrayList<SensorPoint>();

                if(index1 > index2){
                    points.addAll(totalPoints.subList(index2, index1));
                }else if(index1 > 0){
                    points.add(totalPoints.get(index1));
                }
                
                //points.addAll(totalPoints.subList(index2, index1));
                
                if(step != 0){
                    points = filter.pickByStep(points, step);
                }                
                filteredPoints.addAll(points);
                
                //store
                filteredPointsMap.put(propID, filteredPoints);
                
                serieDescr = currPropData.getSerieDescriptor();
                serieDescr.setExtraLength(extraLength);
                serieDescr.setStep(step);
            }
            
        }catch(OutOfMemoryError outError){
            throw outError;
        }catch(SessionExpiredException sessionExp){
            throw sessionExp;
        }
        catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        LvStorage.getInstance().setPropertiesData(chartID, propDataList, request);
        
        return filteredPointsMap;
    }
    
    public static HashMap<PropertyId, List<SensorPoint>> addExtra(HttpServletRequest request){
        HashMap<PropertyId, List<SensorPoint>> filteredPointsMap = new HashMap<PropertyId, List<SensorPoint>>();
        
        boolean isForward = false;        
        if("true".equalsIgnoreCase(request.getParameter("isForward"))){
            isForward = true;
        }        
        long timeEdge = Long.parseLong(request.getParameter("timeEdge"));
        
        LvChartID chartID = new LvChartID();
        chartID.setDataFromStr(request.getParameter("lvChartID"));
        
        List<LvPropertyData> propDataList = LvStorage.getInstance().getPropertiesData(chartID, request);
        
        IndexFinder iFinder = new IndexFinder(false);
        PointsFilter filter = new PointsFilter();

        LvPropertyData currPropData = null;
        CachedSerieDescr currDescr = null;
        int step;
        long extraLength;        
        List<SensorPoint> totalPoints;
        List<SensorPoint> points;
        List<SensorPoint> filteredPoints ;//= new ArrayList<SensorPoint>();
        
        try{            

            for(int i = 0; i < propDataList.size(); i++){
                currPropData = propDataList.get(i);
                PropertyId propID = currPropData.getPropertyID();
                currDescr = currPropData.getSerieDescriptor();
                step = currDescr.getStep();
                extraLength = currDescr.getExtraLength();
                                
                totalPoints = currPropData.getTotalPoints();
                
                filteredPoints = new ArrayList<SensorPoint>();
                
                long date = timeEdge;
                long date2 = timeEdge;
                if(!totalPoints.isEmpty()){
                    date = totalPoints.get(0).time_stamp;
                    date2 = totalPoints.get(totalPoints.size() - 1).time_stamp;
                }
                
                if(isForward){
                    long extraInterval = extraLength;
                    if(date < timeEdge){
                        extraInterval = (timeEdge - date) + extraLength;
                    }                    
                    points = getPointsForProperty(propID, date, date + extraInterval, request);
                }else{
                    long extraInterval = extraLength;
                    if(date2 > timeEdge){
                        extraInterval = (date2 - timeEdge) + extraLength;
                    }
                    points = getPointsForProperty(propID, date2 - extraInterval, date2, request);
                }
                
                if(isForward){
                    points.addAll(totalPoints); 
                }else{
                    totalPoints.addAll(points);
                    points = totalPoints;
                }
                
                int index1 = -1;
                int index2 = -1;
                if(isForward){
                    index1 = iFinder.findIndex(points, timeEdge);
                    index2 = iFinder.findIndex(points, timeEdge + extraLength);
                }else{
                    index1 = iFinder.findIndex(points, timeEdge - extraLength);
                    index2 = iFinder.findIndex(points, timeEdge);
                }
                
                if(index1 > index2){
                    filteredPoints.addAll(points.subList(index2, index1));
                }else if(index1 > 0){
                    filteredPoints.add(points.get(index1));
                }
                                
                if(step != 0){
                    filteredPoints = filter.pickByStep(filteredPoints, step);
                }
                
                //replace old total points with new total points
                filteredPointsMap.put(propID, filteredPoints);
            }            
        }catch(OutOfMemoryError outError){
            throw outError;
        }catch(SessionExpiredException sessionExp){
            throw sessionExp;
        }
        catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        LvStorage.getInstance().setPropertiesData(chartID, propDataList, request);
        
        return filteredPointsMap;
    }
    
    public static HashMap<PropertyId, List<SensorPoint>> getData(HttpServletRequest request) throws JSONException{
        HashMap<PropertyId, List<SensorPoint>> filteredPointsMap = new HashMap<PropertyId, List<SensorPoint>>();
        //HashMap<PropertyId, List<SensorPoint>> totalPointsMap = new HashMap<PropertyId, List<SensorPoint>>();
        HashMap<PropertyId, CachedSerieDescr> descriptorsMap = new HashMap<PropertyId, CachedSerieDescr>();
        List<LvPropertyData> propDataList = new ArrayList<LvPropertyData>();
        
        String bgPropID = request.getParameter("bgPropID");
        double percent = Double.parseDouble(request.getParameter("percent"));
        
        long beginTime = Long.parseLong(request.getParameter("beginTime"));
        long endTime = Long.parseLong(request.getParameter("endTime"));
        
        long extraLength = ((Double)((endTime - beginTime) * percent)).longValue();
        
        LvChartID chartID = new LvChartID();
        chartID.setDataFromStr(request.getParameter("lvChartID"));
        
        //temporary for custom queries
        boolean addOwnerNameToSerie = false;
        if(request.getParameter("addOwnerNameToSerie") != null && request.getParameter("addOwnerNameToSerie").equalsIgnoreCase("true")){
            addOwnerNameToSerie = true;
        }
        
        List<SensorPoint> points = null;
        List<SensorPoint> filteredPoints; //new ArrayList<SensorPoint>();
        List<SensorPoint> extraPoints = new ArrayList<SensorPoint>();
        List<SensorPoint> totalPoints;//new ArrayList<SensorPoint>();
        
        PointsFilter filter = new PointsFilter();
        int step = 0;
        CachedSerieDescr serieDescr = null;
        PropertyId[] propIDs = PropertyId.arrayFromString(request.getParameter("propIDs"));
        for(PropertyId propID : propIDs){
            step = 0;
            
            filteredPoints = new ArrayList<SensorPoint>();
            totalPoints = new ArrayList<SensorPoint>();
            
            //get main points
            points = getPointsForProperty(propID, beginTime, endTime, request);
            
            if(points.isEmpty()){
                //continue;
            }
            
            //filter points if needed
            if(points.size() > MAX_POINTS){
                step = (points.size() / MAX_POINTS) * 2;
                filteredPoints = filter.pickByStep(points, step);
            }else{
                filteredPoints.addAll(points);
            }
            
            //get points between "end" and "extra end"
            extraPoints = getPointsForProperty(propID, endTime, endTime + extraLength, request);
            totalPoints.addAll(extraPoints);
            totalPoints.addAll(points);
            
            //pick points by step, if needed
            if(step != 0){
                extraPoints = filter.pickByStep(extraPoints, step);
            }
            extraPoints.addAll(filteredPoints);
            
            //get points between "extra begin" and "begin"
            filteredPoints = getPointsForProperty(propID, beginTime - extraLength, beginTime, request);
            
            //pick points by step, if needed
            totalPoints.addAll(filteredPoints);
            if(step != 0){
                filteredPoints = filter.pickByStep(filteredPoints, step);
            }            
            extraPoints.addAll(filteredPoints);
            
            //store
            filteredPointsMap.put(propID, extraPoints);            
            
            TO<?> propertyOwner = TOFactory.getInstance().loadTO(propID.getObjectId());
            String propertyOwnerName = null;
            LangManager langManager = LangManager.getInstance();
            String langID = LangManager.getCurrentLocaleId(request);
            String dispPropertyName = langManager.getLocaleString(langID, TypeUtil.getInstance().getDisplayablePropertyName(propertyOwner.getTypeName(), propID.getPropName()));            
            String serieName = dispPropertyName;
            //for sensor, get its name
            if(EnvironmentTypes.TYPE_SENSOR.equals(propertyOwner.getTypeName())){
                try{
                    serieName = Env.getEnv(request).getPropertyValue(propertyOwner, "name", String.class);
                    
                    if(addOwnerNameToSerie){
                        propertyOwner = Env.getEnv(request).getParents(propertyOwner, EnvironmentTypes.TYPE_NODE).iterator().next();
                    }
                }catch(Exception e){
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
            
            if(addOwnerNameToSerie){
                try {
                    propertyOwnerName = Env.getEnv(request).getPropertyValue(propertyOwner, "name", String.class);
                    if(propertyOwnerName == null){
                        propertyOwnerName = propertyOwner.getTypeName();
                    }
                } catch(Exception e){
                    LOG.error(e.getLocalizedMessage(), e);
                }
                if(propertyOwnerName != null){
                    serieName = propertyOwnerName + ":" + serieName;
                }
            }
            
            String unitName = "";
            
            ValueRanges ranges = null; 
            SymbolValues symbolVals = null;
            //temp debug
            try{
                TO<?> object = TOFactory.getInstance().loadTO(propID.getObjectId());
                String propName = propID.getPropName();
                Environment env = Env.getEnv(request);
                
                unitName = Env.getUnits(object, propName, request); 
                
                if(unitName == null || unitName.isEmpty()){
                    unitName = Env.getLongUnits(object, propName, request);
                    
                    if(unitName == null || unitName.isEmpty()){
                        unitName = dispPropertyName;
                    }else{
                        unitName = langManager.getLocaleString(langID, unitName);
                    }
                }
                
                if(!EnvironmentTypes.TYPE_SENSOR.equals(object.getTypeName()) && !EnvTypeUtil.getHistoricalPropertyNames(object.getTypeName(), env).contains(propName)){
                    TO<?> linkedObject = env.getPropertyValue(object, propName, TO.class); //object is a link to TO
                    
                    if(linkedObject == null){
                        //store empty data
                        serieDescr = new CachedSerieDescr(step, extraLength, serieName, unitName, ranges, symbolVals);
                        descriptorsMap.put(propID, serieDescr);                        
                        propDataList.add(new LvPropertyData(propID, serieDescr, totalPoints));
                        continue;
                    }
                    if(EnvironmentTypes.isSensorType(linkedObject.getTypeName())){
                        if(propID.toString().equals(bgPropID)){
                            ranges = EnvTypeUtil.getValuesRangesBySensor(linkedObject, env);
                        }
                        symbolVals = getSymbolValuesBySensor(linkedObject, env);
                        propName = "lastValue";
                        object = linkedObject;
                    }
                }else if(EnvironmentTypes.TYPE_SENSOR.equals(object.getTypeName())){
                    symbolVals = getSymbolValuesBySensor(object, env);
                }                
                
                if(ranges == null){
                    ranges = DataRetrievingUtils.getPropertyRanges(object, propName, env);
                }

            }catch(OutOfMemoryError outError){
                throw outError;
            }catch(SessionExpiredException sessionExp){
                throw sessionExp;
            }
            catch(Exception e){
                LOG.error(e.getLocalizedMessage(), e);
            }
            //end temp debug
            
            serieDescr = new CachedSerieDescr(step, extraLength, serieName, unitName, ranges, symbolVals);
            descriptorsMap.put(propID, serieDescr);
            
            //store data
            LvPropertyData propData = new LvPropertyData(propID, serieDescr, totalPoints);
            propDataList.add(propData);
        }

        LvStorage.getInstance().setPropertiesData(chartID, propDataList, request);
        
        return filteredPointsMap;
    }
    
    public static List<LvTextAnnotation> getTextAnnotations(LvPropertyData propData, HttpServletRequest request){
        Environment env = Env.getEnv(request);
        TO<?> dc = null;		 
        
        Collection<TO<?>> tos = env.getRelatedObjects(propData.getPropertyID().getObjectTO(), EnvironmentTypes.TYPE_DC, false);
        if(!tos.isEmpty()){
            dc = tos.iterator().next();
        }
        
        long extraLength = propData.getSerieDescriptor().getExtraLength();//actually, "extraLength" is the same for whole chart (it is duplicated for each series, because of mistake in design and laziness after)
        long beginTime = 0L;
        long endTime = 0L;
        
        if(request.getParameter("beginTime") != null){
            beginTime = Long.parseLong(request.getParameter("beginTime")) - extraLength;
            endTime = Long.parseLong(request.getParameter("endTime")) + extraLength;
        }else{
            long timeEdge = Long.parseLong(request.getParameter("timeEdge"));			
            if("true".equalsIgnoreCase(request.getParameter("isForward"))){
                beginTime = timeEdge;
                endTime = timeEdge + extraLength;
            }else{
                beginTime = timeEdge - extraLength;
                endTime = timeEdge;
            }			
        }
        
        //get annotations by DC, beginTime, endTime
        List<LvTextAnnotation> result = getTextAnnotations(dc, beginTime, endTime, request);
        return result;
    }
    
    private static List<LvTextAnnotation> getTextAnnotations(TO<?> dc, long beginTime, long endTime, HttpServletRequest request){
        List<LvTextAnnotation> result = new ArrayList<LvTextAnnotation>();
        
        //get system collection
        ActivityLogService alService = Env.getActivityLogServ(request);
        ActivityRecordFilter arFilter = new ActivityRecordFilter();
        arFilter.filterByTime(new Date(beginTime), new Date(endTime));
        arFilter.filterByModule(ActivityLogConstans.MODULE_ANNOTATION);
        arFilter.filterByObject(TOFactory.EMPTY_TO);
        
        Collection<ActivityRecord> arCollection = alService.getRecords(arFilter);
        
        //get DC collection
        if(dc != null){
            arFilter = new ActivityRecordFilter();
            arFilter.filterByTime(new Date(beginTime), new Date(endTime));
            arFilter.filterByModule(ActivityLogConstans.MODULE_ANNOTATION);
            arFilter.filterByObject(dc);
            
            arCollection.addAll(alService.getRecords(arFilter));
        }
        
        //convert activity records to LvAnnotations 
        for(ActivityRecord a : arCollection){
            result.add(new LvTextAnnotation(a.getId().toString(), a.getTime().getTime(), a.getDescription()));
        }
        
        Collections.sort(result, annotationTimeComparator);		
        
        return result;
    }
    
    public static List<SensorPoint> getPointsForProperty(PropertyId property, long startMs, long endMs, HttpServletRequest request){
        List<SensorPoint> points = new ArrayList<SensorPoint>(1);
        try{
            Environment env = Env.getEnv(request);
            TO<?> object = TOFactory.getInstance().loadTO(property.getObjectId());
            String propName = property.getPropName();

            //if property is not historical - it is linked sensor (since calculated can't be passed here)
            if(!EnvironmentTypes.TYPE_SENSOR.equals(object.getTypeName()) && !EnvTypeUtil.getHistoricalPropertyNames(object.getTypeName(), env).contains(propName)){
                object = env.getPropertyValue(object, propName, TO.class); //object is sensorTO
                propName = "lastValue";
            }
        
            if(object == null){//if linked object is null, return empty collection of points
                LOG.info("getPointsForProperty. Linked object is null for property_id " + property);
                return points;
            }
            
            points = EnvHistoryUtil.removeExcluded(EnvHistoryUtil.getPointsBetweenDates(object, propName, new Date(startMs), new Date(endMs), env));
            
            //sort points collection by time stamp
            PointComparator timeComp = new TimestampPointComparator(false);             
            Collections.sort(points, timeComp);
        }catch(OutOfMemoryError outError){
            throw outError;
        }catch(SessionExpiredException sessionExp){
            throw sessionExp;
        }
        catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }        
        return points;
    }
    
    /**
     * Gets symbolValues for sensor from UIConfig.  
     * @param sensor
     * @param env
     * @return
     */
    public static SymbolValues getSymbolValuesBySensor(TO<?> sensor, Environment env){
        SymbolValues result = null;        
        try{
            Integer dataclass = env.getPropertyValue(sensor, "dataclass", Integer.class);        
            result = UIConfig.getSymbolValuesByClassId(dataclass);
        }catch(SessionExpiredException sessionExp){
            throw sessionExp;
        }
        catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        return result;
    }
}
