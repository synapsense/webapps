package com.synapsense.charts.lvcharts;

public class LvChartID {
    private String chartId = "";
    
    public String toString(){
        return chartId;
    }
    
    public void setDataFromStr(String str){
        chartId = str;
    }
    
    @Override
    public int hashCode(){
        return this.toString().hashCode();
    }
    
    @Override
    public boolean equals(Object arg0){
        if(arg0 instanceof LvChartID){
            return arg0.toString().equals(this.toString());  
        }else{
            return false;
        }
    }
}
