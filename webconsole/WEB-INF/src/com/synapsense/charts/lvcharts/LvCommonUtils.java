package com.synapsense.charts.lvcharts;

import java.text.NumberFormat;
import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public class LvCommonUtils {
    
    public static void convertPointsToBuffers(StringBuffer values, StringBuffer times, List<SensorPoint> points, NumberFormat nf){

        values.append(points.get(points.size() - 1).value);
        times.append(points.get(points.size() - 1).time_stamp);
        
        for(int i = points.size() - 2; i > -1; i--){            
            values.append(",").append(points.get(i).value);
            times.append(",").append(points.get(i).time_stamp);
        }
    }
}
