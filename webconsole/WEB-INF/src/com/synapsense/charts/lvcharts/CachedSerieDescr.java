package com.synapsense.charts.lvcharts;

import com.synapsense.utilities.es.ValueRanges;
import com.synapsense.utilities.symbolvalues.SymbolValues;

public class CachedSerieDescr {
    private int step = 0;
    private long extraLength = 0L;
    
    private String serieName = "";
    private String serieUnit = "";
    
    private ValueRanges ranges = null;
    
    private SymbolValues symbolValues = null;
    
    public CachedSerieDescr(int step, long extraLength){
        this.step = step;
        this.extraLength = extraLength;
    }
    
    public CachedSerieDescr(int step, long extraLength, String serieName, String serieUnit, ValueRanges ranges, SymbolValues symbolValues){
        this.step = step;
        this.extraLength = extraLength;
        this.serieName = serieName;
        this.serieUnit = serieUnit;
        this.ranges = ranges;
        this.symbolValues = symbolValues;
    }
    
    public CachedSerieDescr(){
    }
    
    public int getStep(){
        return step;
    }
    public long getExtraLength(){
        return extraLength;
    }    
    public String getSerieName(){
        return serieName;
    }    
    public String getSerieUnit(){
        return serieUnit;
    }
    public ValueRanges getValueRanges(){
        return ranges;
    }
    public SymbolValues getSymbolValues(){
        return symbolValues;
    }
    
    public void setStep(int step){
        this.step = step;
    }
    public void setExtraLength(long extraLength){
        this.extraLength = extraLength;
    }
    public void setSerieName(String serieName){
        this.serieName = serieName;
    }
    public void setSerieUnit(String serieUnit){
        this.serieUnit = serieUnit;
    }
    public void setValueRanges(ValueRanges ranges){
        this.ranges = ranges;
    }
}
