package com.synapsense.charts.lvcharts;

import java.util.List;

import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.sensors.SensorPoint;

public class LvPropertyData {
    private PropertyId propertyID;
    private List<SensorPoint> totalPoints;
    private CachedSerieDescr serieDescriptor;
    
    public LvPropertyData(){        
    }
    
    public LvPropertyData(PropertyId propertyID, CachedSerieDescr serieDescriptor, List<SensorPoint> totalPoints){
        this.propertyID = propertyID;
        this.serieDescriptor = serieDescriptor;
        this.totalPoints = totalPoints; 
    }
    
    public PropertyId getPropertyID(){
        return propertyID;
    }    
    public List<SensorPoint> getTotalPoints(){
        return totalPoints;
    }    
    public CachedSerieDescr getSerieDescriptor(){
        return serieDescriptor;
    }
    
    public void setPropertyID(PropertyId propertyID){
        this.propertyID = propertyID;
    }    
    public void setTotalPoints(List<SensorPoint> totalPoints){
        this.totalPoints = totalPoints;
    }    
    public void setSerieDescriptor(CachedSerieDescr serieDescriptor){
        this.serieDescriptor = serieDescriptor;
    }
}
