package com.synapsense.charts.lvcharts;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.session.exceptions.SessionExpiredException;

public class LvStorage {
    private static final Log LOG = LogFactory.getLog(LvStorage.class);
    
    private volatile static LvStorage instance = null;
    
    private LvStorage(){        
    }
    
    public static LvStorage getInstance(){
        if(instance == null){            
            synchronized(LvStorage.class){
                if(instance == null){
                    instance = new LvStorage();
                }
            }
        }
        return instance;
    }
    
    @SuppressWarnings("unchecked")
    public void setPropertiesData(LvChartID chart_id, List<LvPropertyData> propDataList, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        
        Map<LvChartID, List<LvPropertyData>> propertiesDataMap = (Map<LvChartID, List<LvPropertyData>>)session.getAttribute("propertiesDataMap");
        
        LOG.info("setPropertiesData. chart_id, propDataList != null, propertiesDataMap != null: " + chart_id + ", " + (propDataList != null) + ", " + (propertiesDataMap != null));
        
        if(propertiesDataMap == null){
            synchronized(LvStorage.class){                
                propertiesDataMap = (Map<LvChartID, List<LvPropertyData>>)session.getAttribute("propertiesDataMap");                
                LOG.info("setPropertiesData. synch section: chart_id, propertiesDataMap != null: " + chart_id + ", " + (propertiesDataMap != null));                
                if(propertiesDataMap == null){
                    propertiesDataMap = new ConcurrentHashMap<LvChartID, List<LvPropertyData>>();
                    session.setAttribute("propertiesDataMap", propertiesDataMap); 
                }
            }
        }

        propertiesDataMap.put(chart_id, propDataList);
    }
    
    @SuppressWarnings("unchecked")
    public List<LvPropertyData> getPropertiesData(LvChartID chart_id, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        
        Map<LvChartID, List<LvPropertyData>> propertiesDataMap = (Map<LvChartID, List<LvPropertyData>>)session.getAttribute("propertiesDataMap");
        
        List<LvPropertyData> result = new ArrayList<LvPropertyData>();
        if(propertiesDataMap != null){
            result = propertiesDataMap.get(chart_id);
        }        
        
        LOG.info("getPropertiesData. chart_id, result != null, propertiesDataMap != null: " + chart_id + ", " + (result != null) + ", " + (propertiesDataMap != null));
        
        if(result == null){
            String str = "";
            Set<LvChartID> ids = propertiesDataMap.keySet();
            for(LvChartID cid : ids){
                str += cid.toString() + ",";
            }
            LOG.info("getPropertiesData. result is null. keyset: " + str);
        }
        
        return result;
        //return propertiesDataMap == null ? new ArrayList<LvPropertyData>() : propertiesDataMap.get(chart_id);
    }
    
    @SuppressWarnings("unchecked")
    public void releaseData(LvChartID chart_id, HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        Map<LvChartID, List<LvPropertyData>> propertiesDataMap = (Map<LvChartID, List<LvPropertyData>>)session.getAttribute("propertiesDataMap");

        if(propertiesDataMap != null){
            propertiesDataMap.remove(chart_id);
        }
        LOG.info("releaseData. chart_id: " + chart_id);
    }
    
    public void releaseAllData(HttpServletRequest request){
        HttpSession session = request.getSession(false);
        if(session == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        session.removeAttribute("propertiesDataMap");
        LOG.info("!!! releaseAllData. !!!");
    }
    
}