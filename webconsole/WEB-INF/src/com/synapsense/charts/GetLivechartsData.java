package com.synapsense.charts;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.charts.lvcharts.CachedSerieDescr;
import com.synapsense.charts.lvcharts.LvChartID;
import com.synapsense.charts.lvcharts.LvPropertyData;
import com.synapsense.charts.lvcharts.LvRetrievingUtils;
import com.synapsense.charts.lvcharts.LvStorage;
import com.synapsense.charts.lvcharts.lvtextannotations.LvTextAnnotation;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.sensors.SensorPoint;

public class GetLivechartsData extends BaseHttpServlet {
    private static final long serialVersionUID = 7799635161431617515L;
    private static final Log LOG = LogFactory.getLog(GetLivechartsData.class);
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
    
    
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        try{
            Date totalB = new Date();
            
            //free memory (release chart data)
            if("releaseData".equalsIgnoreCase(request.getParameter("reqType"))){
                String chartId = request.getParameter("lvChartID");
                
                if("all".equals(chartId)){
                    LvStorage.getInstance().releaseAllData(request);
                } else {
                    LvChartID chartID = new LvChartID();
                    chartID.setDataFromStr(chartId);                
                    LvStorage.getInstance().releaseData(chartID, request); 
                }
                sendResponse(response, "<result>" + "" + "</result>");
                return;
            }
            
            NumberFormat numberFormat = CommonFunctions.getNumberFormat(request);
            
            HashMap<PropertyId, List<SensorPoint>> filteredPointsMap = LvRetrievingUtils.m(request);
            
            StringBuffer values;// = new StringBuffer(); 
            StringBuffer times;// = new StringBuffer();
            List<SensorPoint> points;
            StringBuffer series = new StringBuffer();
            StringBuffer annotations = new StringBuffer();
            
            LvChartID chartID = new LvChartID();
            chartID.setDataFromStr(request.getParameter("lvChartID"));
             
            List<LvPropertyData> propDataList = LvStorage.getInstance().getPropertiesData(chartID, request);
            
            CachedSerieDescr currDescr;
            LvPropertyData currPropData;
            
            Date bTime = new Date();
            if(propDataList.size() != 0) {
                
                bTime = new Date();
                //retrieve text annotations
                List<LvTextAnnotation> annotationList = LvRetrievingUtils.getTextAnnotations(propDataList.get(0), request);
                for(LvTextAnnotation ta : annotationList){
                    annotations.append("<annotation id='").append(ta.getId())
                    .append("' time='").append(ta.getTime()).append("'>")
                    .append("<descr><![CDATA[")
                    .append(ta.getDescription())
                    .append("]]></descr>")
                    .append("</annotation>");
                }
                
                annotations.insert(0, "<annotations>");
                annotations.append("</annotations>");
                
                LOG.info("Annotations procesing time: " + ((new Date()).getTime() - bTime.getTime()) + " ms");
                
                bTime = new Date();
                //fill series
                for(int i = 0; i < propDataList.size(); i ++){
                    currPropData = propDataList.get(i);
                    PropertyId propID = currPropData.getPropertyID();
                    values = new StringBuffer();
                    times = new StringBuffer();
                    
                    points = filteredPointsMap.get(propID);
                    currDescr = currPropData.getSerieDescriptor();
                    
                    String serieName = CommonFunctions.escapeXML(currDescr.getSerieName());
                    
                    series.append("<serie pid='").append(propID).append("' name='").append(serieName)
                    .append("' u='").append(currDescr.getSerieUnit()).append("'");
                    
                    if(currDescr.getValueRanges() != null){
                        series.append(" ranges='").append(currDescr.getValueRanges().packToStr()).append("'");
                    }
                    
                    if(currDescr.getSymbolValues() != null){
                        series.append(" symbolvalues='").append(currDescr.getSymbolValues().packToStr(request)).append("'");
                    }
                    
                    series.append(">");
                    
                    if(points != null && !points.isEmpty()){
                        convertPointsToBuffers(values, times, points, numberFormat);
                        series.append("<v>")
                              .append(values).append("</v><t>")
                              .append(times).append("</t>");
                    }
                    
                    series.append("</serie>");
                }
            }else {
                series.append("<serie/>");
            }
            
            Date eTime = new Date();
            
            LOG.info("Converting to Str time: " + (eTime.getTime() - bTime.getTime()) + " ms");
            sendResponse(response, "<result>" + series + annotations + "</result>");
            Date totalE = new Date();
            LOG.info("done. Total time: " + (totalE.getTime() - totalB.getTime()) + " ms");
            
        }catch(OutOfMemoryError outError){
            LvChartID chartID = new LvChartID();
            chartID.setDataFromStr(request.getParameter("lvChartID"));            
            LvStorage.getInstance().releaseData(chartID, request);
            sendResponse(response, "<result><exception>" + "outofmemory" + "</exception></result>");
            return;
        }catch(SessionExpiredException sessionExp){
            throw sessionExp;
        }
        catch(Exception e){
            throw new ServletException(e.getLocalizedMessage(), e);
        }        
    }
        
    /** Following functions should be removed in separate class */
    public static void convertPointsToBuffers(StringBuffer values, StringBuffer times, List<SensorPoint> points, NumberFormat nf){
        //PointComparator timeComp = new TimestampPointComparator(false);             
        //sort points collection by time stamp              
        //Collections.sort(points, timeComp);

        //values.append(nf.format(points.get(points.size() - 1).value));
        values.append(points.get(points.size() - 1).value);
        times.append(points.get(points.size() - 1).time_stamp);
        
        for(int i = points.size() - 2; i > -1; i--){            
            //values.append(",").append(nf.format(points.get(i).value));
            values.append(",").append(points.get(i).value);
            times.append(",").append(points.get(i).time_stamp);
        }
    }
}
