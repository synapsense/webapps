package com.synapsense.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.Environment;
import com.synapsense.util.ConvertingEnvironmentException;
import com.synapsense.util.unitconverter.UnitResolvers;
import com.synapsense.util.unitconverter.UnitResolvers.DimensionRef;
import com.synapsense.util.unitconverter.UnitResolvers.PropertyUnits;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;

public class GetDataclasses extends BaseHttpServlet {
    private static final long serialVersionUID = 1927795901826078292L;
    static private final Log LOG = LogFactory.getLog(GetDataclasses.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        try {
            Environment env = Env.getEnv();
            UnitResolvers unitResolvers = Env.getUnitSystemsService().getUnitReslovers();
            ObjectType[] types = env.getObjectTypes(env.getObjectTypes());
            
            Document doc = XMLHelper.createNewXmlDocument("types");
            Element rootElement = doc.getDocumentElement();
            
            for(ObjectType type : types) {
                Element typeNode = doc.createElement("type");
                typeNode.setAttribute("name", type.getName());
                
                
                for(PropertyDescr prop : type.getPropertyDescriptors()) {
                    PropertyUnits units = unitResolvers.getUnits(type.getName(), prop.getName());
                    try {
                        if(units != null) {
                            DimensionRef dimRef = units.getDimensionReference();
                            if(dimRef == null) {
                                continue;
                            }
                            String dimension = dimRef.getDimension();
                            
                            Element propNode = doc.createElement("property");
                            propNode.setAttribute("name", prop.getName());
                            propNode.setAttribute("dataclass", String.valueOf(dimension));					
                            typeNode.appendChild(propNode);
                        }
                    } catch(ConvertingEnvironmentException ex) {
                        // Ignoring the exception for properties, resolved by UnitResolvers (like WSNSENSOR)
                        continue;
                    }
                }
                
                if(typeNode.hasChildNodes()) {
                    rootElement.appendChild(typeNode);
                }
            }
            
            sendResponse(resp, doc);
        } catch(Exception ex) {
            LOG.error(ex.getLocalizedMessage(), ex);
        }
    }
}
