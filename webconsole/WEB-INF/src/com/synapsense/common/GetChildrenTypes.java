package com.synapsense.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.LogUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetChildrenTypes extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetChildrenTypes.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {		
        try {
            sendResponse(response, createObjectTypeXML(request));
        } catch (Exception e) {
            LOG.error(e.getClass()+ ": " + e.getMessage());
            LogUtils.printStackTrace(LOG, e);
        }
     }
    
    private Document createObjectTypeXML(HttpServletRequest request) throws ServletException {
        String IdsStr = request.getParameter("id");
        
        LOG.debug("Getting types of existing children for object with id = " + IdsStr);
        
        Document doc = null;
        Element root = null;
        
        try {
            
            
            String [] ids = IdsStr.split(",");		    
            Collection<TO<?>> objs = new ArrayList<TO<?>>();
            for(int i = 0; i < ids.length; i++){
                if(!ids[i].isEmpty()){
                    objs.add(TOFactory.getInstance().loadTO(ids[i])); 
                }
            }
            
            doc = XMLHelper.createNewXmlDocument("types");
            root = doc.getDocumentElement();
            
            Environment env = Env.getEnv(request);
            Collection<String> types = null;
            for(TO<?> obj : objs){
                types = new HashSet<String>();
                getChildTypes(obj, env, types);

                if (hasCustomMetrics(obj, request)) {
                    types.add("INSPECTOR");
                }
                
                if(hasEnergyWye(obj, request)){
                    types.add("ENERGY_WYE");
                }
                
                Document typeDoc = XMLHelper.createNewXmlDocument(obj.getTypeName());
                Element typeDocRoot = typeDoc.getDocumentElement();
                for(String nameType:types) {
                    typeDocRoot.appendChild(XMLHelper.createXmlNodeWithValue(typeDoc, "type", nameType));
                }
                
                root.appendChild(doc.importNode((Node)typeDocRoot, true));
            }
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
            throw new ServletException("Object is removed", e);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return doc;
    }

    private static void getChildTypes(TO<?> obj, Environment env, Collection<String> types) {
        Collection<TO<?>> children = null;
        
        try {
            String name = env.getPropertyValue(obj, "name", String.class);
            LOG.debug("Getting children of '" + name + "'");
            
            children = env.getChildren(obj);
        } catch (ObjectNotFoundException | PropertyNotFoundException | UnableToConvertPropertyException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return;
        }
        
        LOG.debug("Found " + children != null ? children.size() : 0 + " children");
        for(TO<?> child : children) {
            String typeName = child.getTypeName();
            LOG.debug("  Child Type = " + typeName);
            types.add(typeName);
            // we need to get all layers of rooms
            //		DC->ROOM->OBJECT and
            //		DC->ROOM->ROOM->...->OBJECT
            if (typeName.equals(EnvironmentTypes.TYPE_DC) ||
                typeName.equals(EnvironmentTypes.TYPE_ROOM)) {
                getChildTypes(child, env, types);
            }
        }
    }
    
//   	private boolean hasPowerObjects(HttpServletRequest request, Collection<TO<?>> children) {
//		Collection<String> names = new HashSet<String>();
//
//		for (TO<?> child : children) {
//			names.add(child.getTypeName());
//		}
//
//		for (String name : names) {
//			Set<PropertyDescr> setProp = Env.getEnv(request).getObjectType(name).getPropertyDescriptors();
//			boolean hasPwr = false;
//			boolean hasMaxPwr = false;
//			for (PropertyDescr descr : setProp) {
//				if ( "pwr".equals(descr.getName())) {
//					hasPwr = true;
//				}else if ("maxpwr".equals(descr.getName())) {
//					hasMaxPwr = true;
//				}
//				if (hasPwr && hasMaxPwr) {
//					return true;
//				}
//			}
//		}
//   		
//   		return false;
//   	}
    
    private boolean hasEnergyWye(TO<?> obj,HttpServletRequest request) throws ObjectNotFoundException{
        boolean result = false;
        /*Collection<TO<?>> instrs = Env.getEnv(request).getChildren(obj, EnvironmentTypes.TYPE_INSTRUMENTED_OBJECT);
        if(!instrs.isEmpty()){
            String [] props = new String[1]; props[0] = "type"; 
            Collection<CollectionTO> ctos = Env.getEnv(request).getPropertyValue(instrs, props);
            for(CollectionTO cto : ctos){
                if (EnvironmentTypes.SUB_TYPE_ENERGY_WYE.equals(cto.getPropValue(props[0]).get(0).getValue())){
                    result = true;
                    break;
                }
            } 
        }*/
        return result;
    }
    
    private boolean hasCustomMetrics(TO<?> obj,HttpServletRequest request) {
        if (!EnvironmentTypes.TYPE_DC.equals(obj.getTypeName())) {
            return false;
        }
        Environment env = Env.getEnv(request);
        Collection<TO<?>> insp = env.getRelatedObjects(obj, EnvironmentTypes.TYPE_INSPECTOR, true);
        return (insp.isEmpty()) ? false : true;
    }
}
