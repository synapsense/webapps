package com.synapsense.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.DataRetrievingUtils;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.PropertyId;
import com.synapsense.utilities.es.ValueRanges;

public class GetObjectDetails extends BaseHttpServlet {
    /**
     * 
     */
    private static final long serialVersionUID = -9199128459597083341L;
    static protected final String PROP_ID_TAG = "propID";
    static protected final String DISP_NAME_TAG = "displayable_name";
    static protected final String BG_TAG = "bg";
    
    private static final Log LOG = LogFactory.getLog(GetObjectDetails.class);
            
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try{
            Document propsDoc = getPropertiesDoc(request);
            
            Document objectDoc = XMLHelper.createNewXmlDocument("object");
            Element objectDocRoot = objectDoc.getDocumentElement();
            
            objectDocRoot.appendChild(objectDoc.importNode((Node)propsDoc.getDocumentElement(), true));
            
            sendResponse(response, objectDoc);
        } catch (Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    private Document getPropertiesDoc(HttpServletRequest request) throws ParserConfigurationException{
        String idStr = request.getParameter("id");
        Environment env = Env.getEnv(request);
        
        Document propsDoc = XMLHelper.createNewXmlDocument("props");
        Element propsDocRoot = propsDoc.getDocumentElement();
        
        TO<?> obj = TOFactory.getInstance().loadTO(idStr);
        String type = obj.getTypeName();
        
        PropertyId tempPropId = new PropertyId();
        try{            
            ArrayList<String> histProps = EnvTypeUtil.getHistoricalPropertyNames(type, env);

            CommonFunctions.removeExcludedHistProps(histProps); // patch: we don't want to see x&y here. See Bug 9162
            
            ArrayList<ValueTO> sensProps = EnvTypeUtil.getPropertySensors(obj, env);            
            Collection<TO<?>> sensors = env.getChildren(obj, EnvironmentTypes.TYPE_SENSOR);
            
            if (!sensors.isEmpty()) {
            
                Collection<CollectionTO> sensorsProps = env.getAllPropertiesValues(sensors);
                sensorsProps = CollectionTOSorter.sortObjects(sensorsProps);
                
                //for native sensors
                for(CollectionTO sensorProps : sensorsProps){                  
                    
                    //skip disabled sensors
                    ValueTO enabled = sensorProps.getSinglePropValue("enabled");
                    if((Integer)enabled.getValue() != 1){
                        continue;
                    }
                    
                    ValueTO dataclass = sensorProps.getSinglePropValue("dataclass");
                    
                    //skip excluded class IDs
                    if(dataclass != null && DataRetrievingUtils.isClassIdExcluded(dataclass.getValue().toString(), request.getSession().getServletContext())){
                        continue;
                    }
                    
                    //add sensor to xml, since its class id is not excluded
                    String channel = sensorProps.getSinglePropValue("channel").getValue().toString();
                    String sensorName = sensorProps.getSinglePropValue("name").getValue().toString();
                    
                    Document propDoc = XMLHelper.createNewXmlDocument("lastValue");
                    Element propDocRoot = propDoc.getDocumentElement();
                    
                    tempPropId.setObjectId(TOFactory.getInstance().saveTO(sensorProps.getObjId()));
                    tempPropId.setPropName("lastValue");
                    
                    propDocRoot.setAttribute(PROP_ID_TAG, tempPropId.toString());
                    propDocRoot.setAttribute(DISP_NAME_TAG, sensorName + "[" + channel + "]");
                    
                    propsDocRoot.appendChild(propsDoc.importNode((Node)propDocRoot, true));
                }
            }
            
            for(ValueTO p : sensProps){
                histProps.add(p.getPropertyName());
            }

            List<String> orderedProps = TypeUtil.getInstance().listPropertyNames(type);
            
            //first add ordered properties
            for(int i = 0; i < orderedProps.size(); i++){
                String currProp = orderedProps.get(i);
                
                if(histProps.contains(currProp)){
                    tempPropId.setPropName(currProp);
                    tempPropId.setObjectId(TOFactory.getInstance().saveTO(obj));
                    
                    addPropertyToDoc(tempPropId, propsDoc, request);
                    
                    histProps.remove(currProp);
                }
            }
            
            //add other properties
            for(String propName : histProps){
                tempPropId.setPropName(propName);
                tempPropId.setObjectId(TOFactory.getInstance().saveTO(obj));
                
                addPropertyToDoc(tempPropId, propsDoc, request);
            }
            
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return propsDoc;
    }
    
    private void addPropertyToDoc(PropertyId propId, Document propsDoc, HttpServletRequest request) throws ParserConfigurationException, EnvException{
        Element propsDocRoot = propsDoc.getDocumentElement();
        
        Document propDoc = XMLHelper.createNewXmlDocument(propId.getPropName());
        Element propDocRoot = propDoc.getDocumentElement();
        TO<?> to = TOFactory.getInstance().loadTO(propId.getObjectId());
       
        propDocRoot.setAttribute(PROP_ID_TAG, propId.toString());
        propDocRoot.setAttribute(DISP_NAME_TAG, TypeUtil.getInstance().getDisplayablePropertyName(to.getTypeName().toUpperCase(), propId.getPropName()));
        
        //if background is present, mark it in attribute
        Environment env = Env.getEnv(request);
        ValueRanges ranges = null;
        if(!EnvTypeUtil.getHistoricalPropertyNames(to.getTypeName(), env).contains(propId.getPropName())){
            TO<?> linkedObject = env.getPropertyValue(to, propId.getPropName(), TO.class);
            if(EnvironmentTypes.TYPE_SENSOR.equals(linkedObject.getTypeName())){                
               ranges = EnvTypeUtil.getValuesRangesBySensor(linkedObject, env);
                }
            }
        
        if(ranges == null){
            ranges = DataRetrievingUtils.getPropertyRanges(TOFactory.getInstance().loadTO(propId.getObjectId()), propId.getPropName(), env);
        }
        
        if(ranges != null && !(ranges.aMax == 0 && ranges.aMin == 0 && ranges.rMax == 0 && ranges.rMin == 0)){
            propDocRoot.setAttribute(BG_TAG, "true");
        }

        propsDocRoot.appendChild(propsDoc.importNode((Node)propDocRoot, true));
    }

}
