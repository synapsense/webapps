package com.synapsense.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.config.types.Property;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetTypeVolatileProperties extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 7625920948423756292L;
    private static final Log LOG = LogFactory
            .getLog(GetTypeVolatileProperties.class);
    
    private static final String[] dataclasses = {"200", "201", "202", "204", "205",
                                                 "206", "207", "209", "211", "213", "214"};
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        sendResponse(resp, getPropsDoc(req));
    }

    private Document getPropsDoc(HttpServletRequest req) {

        String type = req.getParameter("type");
        Boolean onlyAlertable = Boolean.valueOf(req.getParameter("onlyAlertable"));
        //String subType = req.getParameter("subType");
        Document doc = null;
        try {

            doc = XMLHelper.createNewXmlDocument("type");
            Element root = doc.getDocumentElement();
            root.setAttribute("name", type);

            //for nodes and sensors we show dataclasses instead of properties
            if (EnvironmentTypes.TYPE_SENSOR.equals(type) || EnvironmentTypes.TYPE_NODE.equals(type)) {
                for (String dataclass: dataclasses) {
                    String name = TypeUtil.getInstance().getDisplayableDataclassName(dataclass);
                    Node propNode = XMLHelper.createXmlNode(doc, "property",
                            new HashMap<String, String>());

                    Node nameNode = XMLHelper.createXmlTextNode(doc, "name",
                            name);
                    propNode.appendChild(nameNode);

                    Node valueNode = XMLHelper.createXmlTextNode(doc, "value",
                            dataclass);
                    propNode.appendChild(valueNode);

                    root.appendChild(propNode);
                }
            } else {
                Environment env = Env.getEnv(req);
                
                String[] typeNames = type.split(",");
                
                
                ObjectType objType = null;
                for (String typeName : typeNames) {
                    objType = env.getObjectType(typeName);
                    if (objType != null) {
                        Set<String> props = getCommonProperties(typeNames, onlyAlertable, env);
                        List<String> others = new ArrayList<String>();
                        others.addAll(props);
                        
                        //first add ordered properties
                        List<String> orderedProps = TypeUtil.getInstance().listPropertyNames(typeName);					
                        for(int i = 0; i < orderedProps.size(); i++){
                            String prop = orderedProps.get(i);
                            if (props.contains(prop)) {								
                                addPropertyToDoc(doc, root, typeName, prop);
                                others.remove(prop); //remove to not add this property twice
                            }
                        }
                        
                        //add other properties
                        CommonFunctions.sortPropsAlphabetically(others, typeName, req);
                        for (String prop : others) {
                            addPropertyToDoc(doc, root, typeName, prop);
                        }
                        break;
                    }
                }
            }

        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        return doc;
    }
    
    private Set<String> getCommonProperties (String[] typeNames, Boolean onlyAlertable, Environment env) {
        Set<String> res = new HashSet<String>();
        boolean firstPass = true;

        for (String typeName : typeNames) {
            ObjectType type = env.getObjectType(typeName);
            if (type != null) {
                Set<String> tmp = new HashSet<String>();
                for (PropertyDescr desc : type.getPropertyDescriptors()) {
                    
                    if (CommonFunctions.getHistPropsToExclude().contains(desc.getName())) { 
                        continue; // patch: we don't want to see x&y here. See Bug 9162
                    }
                    
                    if ((desc.isHistorical() && Number.class.isAssignableFrom(desc.getType())) || desc.getTypeName().contains(".TO")) {
                        if (onlyAlertable) {
                            Property prop = TypeUtil.getInstance().getProperty(typeName, desc.getName());
                            if (prop != null && !prop.isAlertable()) {
                                continue;
                            }
                        }
                        if (firstPass) {
                            res.add(desc.getName());
                        } else {
                            tmp.add(desc.getName());
                        }
                    }
                }
                if (firstPass) {
                    firstPass = false;
                } else {
                    res.retainAll(tmp);
                }
            }
            
        }
        return res;
    }

    private void addPropertyToDoc(Document doc, Element root, String typeName, String propName){
        Node propNode = XMLHelper.createXmlNode(doc, "property", new HashMap<String, String>());

        Node nameNode = XMLHelper.createXmlTextNode(doc, "name", TypeUtil.getInstance().getDisplayablePropertyName(typeName, propName));
        propNode.appendChild(nameNode);

        Node valueNode = XMLHelper.createXmlTextNode(doc, "value", propName);
        propNode.appendChild(valueNode);
        root.appendChild(propNode);
    }

}
