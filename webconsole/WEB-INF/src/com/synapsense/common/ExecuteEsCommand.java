
package com.synapsense.common;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.cascadestatussetter.CascadeStatusSetter;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.BinaryData;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.dao.to.BinaryDataImpl;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.lang.LangManager;

public class ExecuteEsCommand extends BaseHttpServlet {
    private static final long serialVersionUID = 4253611792677902684L;
    private static final Log log = LogFactory.getLog(ExecuteEsCommand.class);
    private static final int MAX_STR_LENGTH = 50;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        String result = ErrorMessages.SUCCESS;
        String params =  req.getParameter("params");
        Document resDoc = XMLHelper.getDocument("<result>" + ErrorMessages.SUCCESS + "</result>");
        Element root = resDoc.getDocumentElement();
        root.setAttribute("id", req.getParameter("id"));
        try {
            List<String> results = new ArrayList<String>();
            List<Element> nodesList = new ArrayList<Element>();
            JSONArray jsonArray = new JSONArray(params);
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String command = obj.getString("command");
                
                Element commandNode = resDoc.createElement("command");
                commandNode.setAttribute("name", command);
                
                if ("set_static".equals(command)) {
                    processSetStaticPropertiesCommand(req, obj);
                } else if ("set".equals(command)) {
                    processSetPropertiesCommand(req, obj);
                } else if ("delete".equals(command)) {
                    processDeleteObjectCommand(req, obj);
                }else if("set_relation".equals(command)){
                    setRelationCommand(req, obj);
                }else if("remove_relation".equals(command)){
                    removeRelationCommand(req, obj);
                } else if ("create".equals(command)) {
                    results.add(processCreateObjectCommand(req, obj, commandNode));					
                } else if ("updateObj".equals(command)){
                    results.add(processUpdateObjectCommand(req, obj, commandNode));
                } else if ("set_status_cascade".equals(command)) {
                    results.add(setStatusCascadeCommand(req, obj));
                }
                nodesList.add(commandNode);
            }
            
            result = getCombinedResult(results);			
            root.setTextContent(result);
            
            for(Element n : nodesList){
                root.appendChild(n);
            }			
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }
        sendResponse(resp, resDoc);
    }
    
    /**
     * Returns combined result. For now just first result which is not success.
     * @param results - list of command results
     * @return
     */
    private String getCombinedResult(List<String> results){
        String result = ErrorMessages.SUCCESS;
        for(String res : results){
            if(!res.equals(result)){
                result = res;
                break;
            }
        }
        return result;
    }
    
    private void processSetStaticPropertiesCommand(HttpServletRequest req, JSONObject obj) throws JSONException, ServletException {
        String propName = obj.getString("propName");
        String propValue = obj.getString("propValue");
        String typeName = obj.getString("typeName");
        String childrenType = obj.getString("childrenType");
        String message = obj.getString("message");
        String user = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        
        Environment env = Env.getEnv(req);
        
        if (childrenType.isEmpty()) {
            ActivityRecord record = this.prepareActivityRecord(req, typeName, propName, propValue, message, user);
            setPropertyValue(env, typeName, propName, propValue);
            logRecord(record, req);
        }
    }
    
    private void processSetPropertiesCommand(HttpServletRequest req, JSONObject obj) throws JSONException, ServletException {
        String propName = obj.getString("propName");
        String propValue = obj.get("propValue").toString();
        String objId = obj.getString("objId");
        String childrenType = obj.getString("childrenType");
        String message = obj.getString("message");
        String user = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        
        TO<?> envObj = TOFactory.getInstance().loadTO(objId);
        
        Environment env = Env.getEnv(req);
        
        if (childrenType.isEmpty()) {
            ActivityRecord record = this.prepareActivityRecord(req, envObj, propName, propValue, message, user);
            setPropertyValue(env, envObj, propName, propValue);
            logRecord(record, req);
        } else {
            Collection<TO<?>> objects = env.getRelatedObjects(envObj, childrenType, true);
            for (TO<?> to : objects) {
                ActivityRecord record = this.prepareActivityRecord(req, to, propName, propValue, message, user);
                setPropertyValue(env, to, propName, propValue);
                logRecord(record, req);
            }
        }
    }
    
    private void setPropertyValue(Environment env, TO<?> to, String propName, String propValue) throws ServletException {
        Object valueToSet = null;
            Class<?> clazz;
            try {
                clazz = EnvTypeUtil.getPropertyClass(propName, to.getTypeName(), env);
                if (String.class.equals(clazz)) {
                    valueToSet = propValue;
                    } else if (BinaryData.class.equals(clazz)) {
                        if (!"null".equals(propValue) && !"".equals(propValue)) {
                        byte[] bytes = new byte[propValue.length() / 2];
                        for (int i = 0; i < propValue.length(); i += 2) {
                        bytes[i / 2] = (byte)(Integer.parseInt(propValue.substring(i, i + 2), 16) & 0xFF);
                        }
                    
                        valueToSet = new BinaryDataImpl(bytes);
                        } else {
                        valueToSet = null;
                        }	
                } else {
                    Method valueOf = clazz.getDeclaredMethod("valueOf", new Class[] {String.class});
                    valueToSet = valueOf.invoke(null, propValue);
                }
                env.setPropertyValue(to, propName, valueToSet);
            } catch (Exception e) {
                throw new ServletException(e.getLocalizedMessage(), e);
            } 
        }
    
    private void setPropertyValue(Environment env, String typeName, String propName, String propValue) throws ServletException {
        Object valueToSet = null;
            Class<?> clazz;
            try {
                clazz = EnvTypeUtil.getPropertyClass(propName, typeName, env);
                if (String.class.equals(clazz)) {
                    valueToSet = propValue;
            } else if (BinaryData.class.equals(clazz)) {
                if (!"null".equals(propValue) && !"".equals(propValue)) {
                    byte[] bytes = new byte[propValue.length() / 2];
                    
                    for (int i = 0; i < propValue.length(); i += 2) {
                        bytes[i / 2] = (byte)(Integer.parseInt(propValue.substring(i, i + 2), 16) & 0xFF);
                    }
                    
                    valueToSet = new BinaryDataImpl(bytes);
                } else {
                    valueToSet = null;
                }
                } else {
                    Method valueOf = clazz.getDeclaredMethod("valueOf", new Class[] {String.class});
                    valueToSet = valueOf.invoke(null, propValue);
                }
                env.setPropertyValue(typeName, propName, valueToSet);
            } catch (Exception e) {
                throw new ServletException(e.getLocalizedMessage(), e);
            } 
        }
    
    private void removeRelationCommand(HttpServletRequest req, JSONObject obj) throws JSONException, ServletException,
            EnvException {
        String parentId = obj.getString("parentId");
        String childId = obj.getString("childId");

        TO<?> parentObj = TOFactory.getInstance().loadTO(parentId);
        TO<?> childObj = TOFactory.getInstance().loadTO(childId);
        
        Environment env = Env.getEnv(req);
        env.removeRelation(parentObj, childObj);
    }
    
    private void setRelationCommand(HttpServletRequest req, JSONObject obj) throws JSONException, ServletException,
            EnvException {
        String parentId = obj.getString("parentId");
        String childId = obj.getString("childId");

        TO<?> parentObj = TOFactory.getInstance().loadTO(parentId);
        TO<?> childObj = TOFactory.getInstance().loadTO(childId);
        
        Environment env = Env.getEnv(req);
        env.setRelation(parentObj, childObj);
    }
    
    private String setStatusCascadeCommand(HttpServletRequest req, JSONObject obj) throws JSONException, ServletException, EnvException {
        String objectId = obj.getString("objId");
        Integer status = obj.getInt("status");		
        TO<?> object = TOFactory.getInstance().loadTO(objectId);		
        Environment env = Env.getEnv(req);
        
        return CascadeStatusSetter.setObjectStatus(env, object, status);
    }
    
    private void processDeleteObjectCommand(HttpServletRequest req, JSONObject obj) throws JSONException,
            ServletException, EnvException {
        String objId = obj.getString("objId");
        String message = obj.getString("message");
        String user = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);

        TO<?> envObj = TOFactory.getInstance().loadTO(objId);
        Env.getEnv(req).deleteObject(envObj);
        
        logRecord(this.prepareActivityRecord(req, envObj, message, user), req);
    }
    
    private String findDuplicatedProperty(JSONObject obj, JSONArray propsJsonArray, String objType, TO<?> toToSkip, HttpServletRequest req)  throws JSONException{
        String dupProperty = null;
        Map<String, String> propsMap = new HashMap<String, String>();
        List<String> uniqueProps = new ArrayList<String>();
        
        if(obj.has("uniqueProperties")){									
            JSONArray uniquePropsJsonArray = obj.getJSONArray("uniqueProperties");
            for (int i = 0; i < uniquePropsJsonArray.length(); i++) {
                uniqueProps.add(uniquePropsJsonArray.getString(i));
            }
            if(!uniqueProps.isEmpty()){
                for (int i = 0; i < propsJsonArray.length(); i++) {
                    JSONObject o = propsJsonArray.getJSONObject(i);
                    String pName = o.get("name").toString();
                    if(uniqueProps.contains(pName)){
                        propsMap.put(pName, o.get("value").toString());
                    }
                }
            }
        }
        
        if(!propsMap.isEmpty()){
            dupProperty = this.findDuplicatedProperty(objType, propsMap, toToSkip, req);
        }
        return dupProperty;
    }
    
    private String getDuplicatedPropertyMessage(String typeName, String propName, HttpServletRequest req){
        LangManager lm = LangManager.getInstance();
        String locale = LangManager.getCurrentLocaleId(req);
        String dispName = TypeUtil.getInstance().getDisplayablePropertyName(typeName, propName);
        dispName = lm.getLocaleString(locale, dispName);
        String dispTypeName = TypeUtil.getInstance().getDisplayableTypeName(typeName);
        dispTypeName = lm.getLocaleString(locale, dispTypeName);
        String msg = lm.getLocaleString(locale, "message/value_already_exists", dispTypeName, dispName);
        String message = "-2|" + msg;
        return message;
    }
    
    private String processCreateObjectCommand(HttpServletRequest req, JSONObject obj, Element commandNode) throws JSONException,
            ServletException, EnvException {
        Document doc = commandNode.getOwnerDocument();
        String result = ErrorMessages.SUCCESS;
        String type = obj.getString("type");
        String parentId = obj.getString("parentId");
        String message = obj.getString("message");

        if (obj.has("properties")) {
            Object props = obj.get("properties");				
    
            JSONObject propObj = null;
            if (props instanceof String) {
                propObj = new JSONObject(props);
            } else if (props instanceof JSONObject) {
                propObj = (JSONObject) props;
            } else {
                throw new ServletException("Invalid properties parameter");
            }
    
            String user = (String) req.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            Environment env = Env.getEnv(req);
            
            if (propObj.has("properties")) {
                JSONArray propsJsonArray = propObj.getJSONArray("properties");
                
                String dupProperty = findDuplicatedProperty(obj, propsJsonArray, type, null, req);				
                if (dupProperty != null) {
                    result = getDuplicatedPropertyMessage(type, dupProperty, req);
                }else {
                    TO<?> envObj = Env.getEnv(req).createObject(type);						
                    for (int i = 0; i < propsJsonArray.length(); i++) {
                        JSONObject o = propsJsonArray.getJSONObject(i);
                        this.setPropertyValue(env, envObj, o.get("name").toString(), o.get("value").toString());
                    }
                    if (parentId != null && !parentId.isEmpty()) {
                        env.setRelation(TOFactory.getInstance().loadTO(parentId), envObj);
                    }
                    
                    Node idNode = XMLHelper.createXmlTextNode(doc, "id", TOFactory.getInstance().saveTO(envObj));
                    commandNode.appendChild(idNode);
                    
                    logRecord(this.prepareActivityRecord(req, envObj, message, user), req);
                }
            }
        }
        commandNode.appendChild(XMLHelper.createXmlTextNode(doc, "result", result));				
        return result;
    }
    
    private String processUpdateObjectCommand(HttpServletRequest req, JSONObject obj, Element commandNode) throws JSONException,
    ServletException, EnvException {
        Document doc = commandNode.getOwnerDocument();
        String result = ErrorMessages.SUCCESS;
        String objId = obj.getString("objId");
        Environment env = Env.getEnv(req);
        
        if (obj.has("properties")) {
            Object props = obj.get("properties");				
    
            JSONObject propObj = null;
            if (props instanceof String) {
                propObj = new JSONObject(props);
            } else if (props instanceof JSONObject) {
                propObj = (JSONObject) props;
            } else {
                throw new ServletException("Invalid properties parameter");
            }

            if (propObj.has("properties")) {
                JSONArray propsJsonArray = propObj.getJSONArray("properties");				
                TO<?> envObj = TOFactory.getInstance().loadTO(objId);				
                String dupProperty = findDuplicatedProperty(obj, propsJsonArray, envObj.getTypeName(), envObj, req);				
                if (dupProperty != null) {
                    result = getDuplicatedPropertyMessage(envObj.getTypeName(), dupProperty, req);;
                }else {
                    for (int i = 0; i < propsJsonArray.length(); i++) {
                        JSONObject o = propsJsonArray.getJSONObject(i);
                        this.setPropertyValue(env, envObj, o.get("name").toString(), o.get("value").toString());
                    }
                }
            }
        }		
        commandNode.appendChild(XMLHelper.createXmlTextNode(doc, "result", result));		
        return result;
    }
    
    private String findDuplicatedProperty(String objType, Map<String, String> propNamesAndVals, TO<?> toToSkip, HttpServletRequest req){
        String duplicatedPropName = null;
        Environment env = Env.getEnv(req);
        Collection<TO<?>> objs = env.getObjectsByType(objType);
        if(objs.isEmpty()){
            return duplicatedPropName;
        }
        
        Collection<CollectionTO> ctos = env.getPropertyValue(objs, propNamesAndVals.keySet().toArray(new String[0]));
                
        for(CollectionTO cto : ctos){	
            if (cto.getObjId().equals(toToSkip)){
                continue;
            }
            for(Map.Entry<String, String> entry : propNamesAndVals.entrySet()){
                String propName = entry.getKey();
                String propValue = entry.getValue();
                ValueTO vto = cto.getSinglePropValue(propName);
                if(vto != null && propValue.equals(vto.getValue())){
                    duplicatedPropName = propName;
                    break;
                }
            }
            if(duplicatedPropName != null){
                break;
            }
        }
        return duplicatedPropName;
    }
    
    private void logRecord(ActivityRecord record, HttpServletRequest request){
        if(record != null){
            Env.getActivityLogServ(request).addRecord(record);
        }
    }
    
    private Object getPropertyValue(Object typeNameOrTo, String propName, HttpServletRequest request) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        if(typeNameOrTo != null){
            if(typeNameOrTo instanceof TO<?>){
                Class<?> clazz = EnvTypeUtil.getPropertyClass(propName, ((TO<?>)typeNameOrTo).getTypeName(), Env.getEnv(request));
                return Env.getEnv(request).getPropertyValue((TO<?>)typeNameOrTo, propName, clazz);
            }else if(typeNameOrTo instanceof String){
                Class<?> clazz = EnvTypeUtil.getPropertyClass(propName, (String)typeNameOrTo, Env.getEnv(request));
                return Env.getEnv(request).getPropertyValue((String)typeNameOrTo, propName, clazz);
            }
        }
        return null;
    }
    
    private String convertValueToBaseSystem(Object typeNameOrTo, String propName, String propValue, HttpServletRequest request){
        if(typeNameOrTo != null){
            Double value = Double.valueOf(propValue);
            if(typeNameOrTo instanceof TO<?>){
                return Env.getEnv(request).convertValueToBaseSystem((TO<?>)typeNameOrTo, propName, value).toString();
            }else if(typeNameOrTo instanceof String){
                return Env.getEnv(request).convertValueToBaseSystem((String)typeNameOrTo, propName, value).toString();
            }
        }
        return "'" + LocalizationUtils.getLocalizedString("text/n_a") + "'";
    }
    
    private String getTruncatedString(String str) {
        String tail = "...";
        return (str.length() >= MAX_STR_LENGTH) ? str.substring(0, MAX_STR_LENGTH - tail.length()).concat(tail) : str;
    }
    
    private ActivityRecord prepareActivityRecord(HttpServletRequest request, Object typeNameOrTo, String propName, String propValue, String message, String user) {
        ActivityRecord result = null;
        
        TO<?> to = null;
        String typeName = null;
        if(typeNameOrTo != null){
            if(typeNameOrTo instanceof TO<?>){
                to = (TO<?>)typeNameOrTo;
            }else if(typeNameOrTo instanceof String){
                typeName = (String)typeNameOrTo;
            }
        }
        
        if (message != null && !message.isEmpty()) {
            String[] tokens = message.split("\\|");
            String objId = "";
            String objName = "";
            
            try {
                String valStr = "null";
                String prevValStr = "null";
                String prop = "";
                String dataclass = null;
                if (to != null) {
                    objId = TOFactory.getInstance().saveTO(to);
                } else {
                    objId = typeName;
                }
                if (propName != null) {
                    if(typeName != null || to != null){
                        String tName = typeName != null ? typeName : to.getTypeName();
                        prop = Env.getLocalizationService().getString(TypeUtil.getInstance().getDisplayablePropertyName(tName, propName), Env.getLocalizationService().getDefaultLocale());
                        if (prop == null) {
                            prop = propName;
                        }
                        
                        Dimension dim = Env.getDimension(to, propName, request);
                        if (dim != null) {
                            dataclass = dim.getName();
                        }
                        
                    }
                    if (propValue != null && isNumeric(propValue) && dataclass != null) {
                        valStr = getConvString(dataclass, typeNameOrTo, propName, propValue, request);
                    } else {
                        valStr = propValue;
                    }
                    Object prevValue = this.getPropertyValue(typeNameOrTo, propName, request);
                    objName = this.getPropertyValue(typeNameOrTo, "name", request).toString();
                    if(prevValue != null && isNumeric(propValue) && dataclass != null){
                        prevValStr = getConvString(dataclass, typeNameOrTo, propName, prevValue.toString(), request);
                    } else if(prevValue != null){
                        prevValStr = prevValue.toString();
                    }
                    
                    return new ActivityRecord(user, tokens[0], tokens[1], LocalizationUtils.getLocalizedString(tokens[2], objName, objId, prop, getTruncatedString(prevValStr), getTruncatedString(valStr)));
                }
                
            } catch (ObjectNotFoundException e) {
                log.error(e.getLocalizedMessage(), e);
            } catch (PropertyNotFoundException e) {
                log.error(e.getLocalizedMessage(), e);
            } catch (UnableToConvertPropertyException e) {
                log.error(e.getLocalizedMessage(), e);
            }
        }		
        return result;
    }
    
    private ActivityRecord prepareActivityRecord(HttpServletRequest request, Object typeNameOrTo, String message, String user) {
        ActivityRecord result = null;
        
        TO<?> to = null;
        String typeName = null;
        if(typeNameOrTo != null){
            if(typeNameOrTo instanceof TO<?>){
                to = (TO<?>)typeNameOrTo;
            }else if(typeNameOrTo instanceof String){
                typeName = (String)typeNameOrTo;
            }
        }
        
        if (message != null && !message.isEmpty()) {
            String[] tokens = message.split("\\|");
            String objId = "";
            if (to != null) {
                objId = TOFactory.getInstance().saveTO(to);
            } else {
                objId = typeName;
            }
            result = new ActivityRecord(user, tokens[0], tokens[1], LocalizationUtils.getLocalizedString(tokens[2], objId));
        }
        return result;
    }
    
    private String getConvString(String dataclass, Object typeNameOrTo, String propName, String propValue, HttpServletRequest request) {
        return (typeNameOrTo != null) ? "$conv(" + this.convertValueToBaseSystem(typeNameOrTo, propName, propValue, request) + "," + dataclass + ")$" : propValue;
    }
    
    private boolean isNumeric(String str) {  
        try {
            Double.parseDouble(str);
        } catch(NumberFormatException e) {
            return false;
        }
        return true;
    }
}
