package com.synapsense.common;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.config.types.Property;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;


public class GetProperties extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 3544135144558306325L;
    static private final Log LOG = LogFactory.getLog(GetProperties.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        try {
            String idsStr = req.getParameter("ids");
            String[] ids = idsStr.split(";");
            
            Map<String, Collection<TO<?>>> map = new HashMap<String, Collection<TO<?>>>();
            
            for (String id : ids) {
                TO<?> obj = TOFactory.getInstance().loadTO(id);
                
                Collection<TO<?>> objs = map.get(obj.getTypeName());
                if (objs == null) {
                    objs = new HashSet<TO<?>>();
                    map.put(obj.getTypeName(), objs);
                }
                objs.add(obj);
            }
            
            Environment env = Env.getEnv(req);
            ObjectType objType = null;
            
            //find intersection of property descriptions
            Set<String> types = map.keySet();
            Set<PropertyDescr> propDescrs = types.size() > 0 ? env.getObjectType(types.iterator().next()).getPropertyDescriptors() : new HashSet<PropertyDescr>(0);
            for (String type : types) {
                objType = env.getObjectType(type);
                Set<PropertyDescr> descrs = objType.getPropertyDescriptors();
                descrs.retainAll(propDescrs);
                propDescrs = descrs;
            }
            
            //filter props
            Collection<String> props = new HashSet<String>();
            for (PropertyDescr propDescr : propDescrs) {
                boolean isEditable = false;
                Property property = TypeUtil.getInstance().getProperty(objType.getName(), propDescr.getName());
                if(property != null) {
                    isEditable = property.isEditable();
                }
                if (isEditable) {
                    props.add(propDescr.getName());
                }
            }
            
            String[] properties = new String[props.size()];
            props.toArray(properties);
            
            Collection<CollectionTO> ccto = new HashSet<CollectionTO>();
            for (Collection<TO<?>> objs : map.values()) {
                 ccto.addAll(env.getPropertyValue(objs, properties));
            }
            
            //map that accumulated common properties of the objects, 
            //if properties aren'r equal they are substituted with ""
            Map<String, Object> valuesMap = new HashMap<String, Object>();
            for (CollectionTO cto : ccto) {
                for (ValueTO vto : cto.getPropValues()) {
                    Object value = valuesMap.get(vto.getPropertyName());
                    if (value == null) {
                        valuesMap.put(vto.getPropertyName(), vto.getValue() == null ? "" : vto.getValue());
                    } else if ("".equals(value)) {
                        continue;
                    } else if (!value.equals(vto.getValue())) {
                        valuesMap.put(vto.getPropertyName(), "");
                    }
                }
            }

            Document doc = XMLHelper.createNewXmlDocument("component");
            Element root = doc.getDocumentElement();

            for(Map.Entry<String, Object> entry : valuesMap.entrySet()) {
                Element node = (Element) XMLHelper.createXmlTextNode(doc, "property", entry.getValue().toString());
                String prop = entry.getKey();
                node.setAttribute("name", prop);
                node.setAttribute("display", TypeUtil.getInstance().getDisplayablePropertyName(objType.getName(), prop));
                node.setAttribute("type", objType.getPropertyDescriptor(prop).getType().getSimpleName());
                root.appendChild(node);
            }
            sendResponse(resp, doc);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

}
