package com.synapsense.common;

import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.Tag;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.PropertyTO;
import com.synapsense.util.CRC8Converter;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.DataRetrievingUtils;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.EnvXMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

/**
* 
* This servlet can be used to fetch the hierarchy of objects from the ES.
* objectRequest parameter is the string representing JSON objects that describes that
* hierarchy. It has the following structure:
*   class objectRequest {
*  	 id, //string, id of the root object, should be filled for the root request only
*  	 type, //string, type of objects to request, should be filled for the child requests only
*  	 properties: propertiesArray, //arrays of property names, that should be added to response
*  	 parents, //array of objectRequest, describing which parent TOs to expand in the response
*  	 requestDirectParentsOnly, //boolean, says where to fetch only direct parents or any ancestors
*  	 links, //array of objectRequest, describing which linked TOs to expand in the response
*  	 children, //array of objectRequest, describing which child TOs to expand in the response
*  	 requestDirectChildrenOnly, //boolean, says where to fetch only direct children or any descendants
*    displayPropertyObjectsProperties,
*    requestFilteredChildrenOnly //boolean, exclude objects which have not children 
*   }
*   
*   Example of Laszlo code forming the request parameters:
*       var objectRequest = {id: "SET:1",
*                            properties: ["name"],
*                            tags:[{propname:"name",tagnames:["tag1",...]}],
*                            filter: [{name: "name", value: "value"}],
*                            children: [{type: "CONTROLOUT_CRAH", links: [{type: "WSNSENSOR", properties:["lastValue"]}]}, 
*                                       {type: "CONTROLOUT_VFD"}],
*                            parents: [{type: "DC"}]
*                           };
*       params.addValue("objectRequest", JSON.stringify(objectRequest));
*
* @author aklushin
* @author pahunov
*
*/
//TODO It is time to rewrite this servlet completely
//Because it is so confused to support...
public class GetObjectXML extends BaseHttpServlet {

    private static final long serialVersionUID = -7955486536881197580L;
    private static final Log LOG = LogFactory.getLog(GetObjectXML.class);

    protected ThreadLocal<SimpleDateFormat> sdf = new ThreadLocal<SimpleDateFormat>();
    protected ThreadLocal<NumberFormat> numberFormat = new ThreadLocal<NumberFormat>();
    protected ThreadLocal<Environment> env = new ThreadLocal<Environment>();

    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        try {
            Environment env;
            String useLocalEnv = request.getParameter("useLocalEnv");
            if ("true".equals(useLocalEnv)) {
                env = Env.getLocalEnv(request);
            } else {
                env = Env.getEnv(request);
            }
            
            this.env.set(env);
            numberFormat.set(CommonFunctions.getNumberFormat(request));
            sdf.set(new SynapDateFormat(
                    UsersDataOptions.getDateFormat(request), UsersDataOptions
                            .getTzIdFromRequest(request)));

            String objectRequestStr = request.getParameter("objectRequest");
            String objectsRequestStr = request.getParameter("objectsRequest");
            ObjectRequest[] objectsRequest = null;
            if (objectRequestStr != null) {
                objectsRequest = new ObjectRequest[] { new ObjectRequest(
                        objectRequestStr) };
            } else if (objectsRequestStr != null) {
                JSONArray jsonObjects = new JSONArray(objectsRequestStr);
                objectsRequest = new ObjectRequest[jsonObjects.length()];
                for (int i = 0; i < jsonObjects.length(); i++) {
                    objectsRequest[i] = new ObjectRequest(jsonObjects
                            .getJSONObject(i).toString());
                }
            } else {
                LOG.error(this.getServletName() + " - empty objects request.");
                throw new ServletException(
                        "Empty object request was received by "
                                + this.getServletName());
            }
            sendResponse(response, createObjectsXMLDocument(objectsRequest,
                    request));
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
    }

    protected Document createObjectsXMLDocument(ObjectRequest[] objectsRequest,
            HttpServletRequest request) throws IOException, ServletException {
        Document document = null;
        try {
            document = XMLHelper.createNewXmlDocument("objects");
            Node root = document.getDocumentElement();
            for (ObjectRequest objRequest : objectsRequest) {
                addObjectsSetToXMLNode(root, objRequest, request);
            }
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return document;
    }

    protected void addObjectsSetToXMLNode(Node root,
            ObjectRequest objectRequest, HttpServletRequest request)
            throws IOException, ServletException {
        Environment env = this.env.get();
        Collection<TO<?>> objColl = null;
        if (objectRequest.getId() == null) {
            Map<String, String> filterMap = objectRequest.getFilter();
            if (filterMap.size() > 0) {
                objColl = getFilteredObjects(objectRequest.getFilter(),
                        objectRequest.getType(), env);
            } else {
                objColl = env.getObjectsByType(objectRequest.getType());
            }
        } else {
            objColl = Arrays.asList(new TO<?>[] { objectRequest.getId() });
        }
        
        for (TO<?> envObj : objColl) {
            addObjectToNode(envObj, root, objectRequest, request);
        }
    }
    
    protected Map<String,Set<Tag>> getTagsMap (ObjectRequest objectRequest,TO<?> envObj) {
        Map<String,Set<Tag>> tagsMap = new HashMap<String, Set<Tag>>();
        if (objectRequest.getTags() != null) {
            Set<Entry<String, Set<String>>> entrySet = objectRequest.getTags().entrySet();
            tagsMap = new HashMap<String, Set<Tag>>(entrySet.size());
            for (Entry<String, Set<String>> entry:entrySet) {
                tagsMap.put(entry.getKey(), new HashSet<Tag>(env.get().getTags(Arrays.asList(new TO<?>[]{envObj}), 
                                Arrays.asList(new String[]{entry.getKey()}), 
                                new ArrayList<String>(entry.getValue())).
                                entrySet().iterator().next().getValue().
                                entrySet().iterator().next().getValue()));
                
            }
        }
        return tagsMap;
    }
    
    protected CollectionTO getProperties (String[] properties,TO<?> envObj, ObjectRequest objectRequest) {
        CollectionTO colTo = null;
        try {
            if (properties == null) {
                colTo = new CollectionTO(envObj, new LinkedList<ValueTO>(
                        env.get().getAllPropertiesValues(envObj)));
            } else if (properties.length > 0) {
                Collection<TO<?>> col = new HashSet<TO<?>>();
                col.add(envObj);
                colTo = env.get()
                        .getPropertyValue(col, properties)
                        .iterator().next();
            }
        } catch (ObjectNotFoundException e) {
            
        }
                
        colTo = removeExcludedProperties(colTo, objectRequest);
        
        return colTo;
    }
    
    protected CollectionTO removeExcludedProperties (CollectionTO colTo, ObjectRequest objectRequest) {
        Set<String> excludedProps = objectRequest.getExcludedProperties();
        if (colTo != null && excludedProps != null) {			
            for (ValueTO vto : colTo.getPropValues()) {
                if (excludedProps.contains(vto.getPropertyName())) {
                    colTo.remove(vto);
                }
            }
        }	
        return colTo;
    }

    protected Element addObjectToNode(TO<?> envObj, Node root,
            ObjectRequest objectRequest, HttpServletRequest request)
            throws IOException, ServletException {
        try {
            Document doc = root.getOwnerDocument();
            Element objectNode = doc.createElement(envObj.getTypeName());
            CollectionTO colTo = getProperties(objectRequest.getProperties(), envObj, objectRequest);
            
            Map<String,Set<Tag>> tagsMap = getTagsMap(objectRequest,envObj);
            
            EnvXMLHelper.writeIDs(envObj, objectNode);
            writePropsAndTags(request, objectNode, colTo, tagsMap, objectRequest);
            
            addRelatedObjectsToXMLNode(objectNode, envObj,
                    objectRequest.getParentsRequests(), request, false);
            addRelatedObjectsToXMLNode(objectNode, envObj,
                    objectRequest.getChildrenRequests(), request, true);
            
            root.appendChild(objectNode);
            return objectNode;
        } catch (ObjectNotFoundException e) {
            throw new ServletException(
                    "Could not add object to XML due to the following error: " + e.getMessage(), e);
        } catch (PropertyNotFoundException e) {
            throw new ServletException(
                    "Could not add object to XML due to the following error: " + e.getMessage(), e);
        } catch (NamingException e) {
            throw new ServletException(
                    "Could not add object to XML due to the following error: " + e.getMessage(), e);
        }
    }

    protected void addRelatedObjectsToXMLNode(Node node, TO<?> obj,
            ObjectRequest[] objectRequests, HttpServletRequest request,
            boolean isChild) throws ObjectNotFoundException, NamingException,
            ServletException, PropertyNotFoundException {

        if (objectRequests == null || objectRequests.length == 0) {
            return;
        }

        Document doc = node.getOwnerDocument();
        Environment env = this.env.get();
        /*
         * //temporary patch: add WYE-type for (ObjectRequest objectRequest :
         * objectRequests) { if
         * (EnvironmentTypes.TYPE_ION_DELTA.equals(objectRequest.getType())) {
         * objectRequests = Arrays.copyOf(objectRequests, objectRequests.length
         * + 1); objectRequests[objectRequests.length - 1] = new
         * ObjectRequest(EnvironmentTypes.TYPE_ION_WYE, objectRequest.getId(),
         * objectRequest.getProperties(), objectRequest.getFilter(),
         * objectRequest.getChildrenRequests(), objectRequest.getLinkRequests(),
         * objectRequest.requestDirectChildrenOnly()); break; } }
         */

        for (ObjectRequest objectRequest : objectRequests) {
            Node objectsNode = null;
            if (isChild) {
                objectsNode = doc.createElement("children");
            } else {
                objectsNode = doc.createElement("parents");
            }

            Collection<TO<?>> objects = null;

            String objectsType = objectRequest.getType();
            String[] objectProps = objectRequest.getProperties();
            if ("all".equals(objectsType)) {
                if (isChild) {
                    objects = env.getChildren(obj);
                } else {
                    objects = env.getParents(obj);
                }
            } else {
                if (isChild) {
                    if (objectRequest.requestDirectChildrenOnly()) {
                        objects = env.getChildren(obj, objectsType);
                    } else {
                        objects = env.getRelatedObjects(obj, objectsType,
                                isChild);
                    }
                } else {
                    if (objectRequest.requestDirectParentsOnly()) {
                        objects = env.getParents(obj, objectsType);
                    } else {
                        objects = env.getRelatedObjects(obj, objectsType,
                                isChild);
                    }
                }
            }
            if (objects == null || objects.isEmpty()) {
                continue;
            }

            Collection<CollectionTO> valueArray = null;

            if (objectProps == null) {
                valueArray = env.getAllPropertiesValues(objects);
            } else {
                if (EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(objects
                        .iterator().next().getTypeName())) {
                    // add "type" for classID retrieving
                    objectProps = DataRetrievingUtils
                            .addPropertyNameToExistingArray(objectProps,
                                    "dataclass");
                }
                valueArray = DataRetrievingUtils.getPropertyValue(objects,
                        objectProps, env);
            }

            if (valueArray == null) {
                return;
            }

            List<CollectionTO> valueList = CollectionTOSorter
                    .sortObjects(valueArray);

            for (CollectionTO p : valueList) {
                TO<?> object = p.getObjId();

                // skip excluded class IDs
                if (EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(object
                        .getTypeName())
                        && DataRetrievingUtils.isClassIdExcluded(p
                                .getSinglePropValue("dataclass").getValue()
                                .toString(), request.getSession()
                                .getServletContext())) {
                    continue;
                }
                
                if (!isFilteredObject(objectRequest.getFilter(),
                        object.getTypeName(), p)) {
                    continue;
                }
                
                Map<String,Set<Tag>> tagsMap = getTagsMap(objectRequest,object);

                Node objectNode = XMLHelper.createXmlNode(doc,
                        object.getTypeName(), new HashMap<String, String>());
                EnvXMLHelper.writeIDs(object, (Element) objectNode);
                writePropsAndTags(request, (Element) objectNode, p, tagsMap, objectRequest);
                addRelatedObjectsToXMLNode(objectNode, object,
                        objectRequest.getParentsRequests(), request, false);
                addRelatedObjectsToXMLNode(objectNode, object,
                        objectRequest.getChildrenRequests(), request, true);
                //checking that objects node has children which fits the specified filter
                //if the requestFilteredChildrenOnly is 'true' and there is not children excluding objectNode from response
                NodeList chList = ((Element)objectNode).getElementsByTagName("children");
                if(objectRequest.requestFilteredChildrenOnly() && chList.getLength() > 0 && !chList.item(0).hasChildNodes()) continue;
                objectsNode.appendChild(objectNode);
            }
            node.appendChild(objectsNode);
        }
    }
    
    /**
     * This method is used in order to add tags inside 
     * property when this property was not requested 
     * @param root
     * @param tagsMap
     */
    protected void writeTags (Node root,Map<String,Set<Tag>> tagsMap) {
        Document doc = root.getOwnerDocument();
        for (Entry<String, Set<Tag>> entry:tagsMap.entrySet()) {
            Element propEl = doc.createElement(entry.getKey());
            for (Tag tag:entry.getValue()) {
                writeTag(propEl,tag);
            }
            root.appendChild(propEl);
        }
    }
    
    protected void writeTag (Node root,Tag tag) {
        if (tag != null) {
            Element tagEl = root.getOwnerDocument().createElement("tag");
            tagEl.setAttribute("name", tag.getTagName());
            String value = tag.getValue() == null?"":tag.getValue().toString();
            tagEl.setTextContent(value);
            root.appendChild(tagEl);
        }
    }

    protected void writePropsAndTags(HttpServletRequest request, Element newelem,
            CollectionTO listProp, Map<String,Set<Tag>> tagsMap,ObjectRequest objectRequest)
            throws ObjectNotFoundException, ServletException, NamingException,
            PropertyNotFoundException {
        if (listProp != null) {
            TO<?> obj = listProp.getObjId();
            // first append ordered properties
            ArrayList<ValueTO> otherProps = appendOrderedPropertiesToDoc(request,
                    obj, newelem, listProp, tagsMap, objectRequest);
    
            // add other properties to doc
            CommonFunctions.sortPropsAlphabetically(otherProps, listProp.getObjId().getTypeName(), request);			
            for (ValueTO prop : otherProps) {
                appendPropValueToNode(request, obj, prop, 
                        tagsMap.get(prop.getPropertyName()),newelem, objectRequest,
                        listProp.getPropValues());
                tagsMap.remove(prop.getPropertyName());
            }
        }
        writeTags(newelem, tagsMap);
    }

    protected ArrayList<ValueTO> appendOrderedPropertiesToDoc(
            HttpServletRequest request, TO<?> obj, Element newelem,
            CollectionTO listProp, Map<String,Set<Tag>> tagsMap, ObjectRequest objectRequest)
            throws ObjectNotFoundException, ServletException, NamingException,
            PropertyNotFoundException {
        ArrayList<ValueTO> arr = new ArrayList<ValueTO>();
        arr.addAll(listProp.getPropValues());

        List<String> propNames = TypeUtil.getInstance().listPropertyNames(
                obj.getTypeName());

        for (int i = 0; i < propNames.size(); i++) {
            ValueTO currProp = null;
            List<ValueTO> colVto = listProp.getPropValue(propNames.get(i));
            if (colVto != null && !colVto.isEmpty()) {
                currProp = colVto.get(0);
            }
            if (currProp != null) {
                appendPropValueToNode(request, obj, currProp, 
                        tagsMap.get(currProp.getPropertyName()), newelem,
                        objectRequest, listProp.getPropValues());
                arr.remove(currProp);
                tagsMap.remove(currProp.getPropertyName());
            }
        }

        return arr;
    }
    
    protected void appendPropValueToNode(HttpServletRequest request, TO<?> obj,
            ValueTO prop, Set<Tag> tags,Node node, ObjectRequest objectRequest,
            Collection<ValueTO> listProp) throws ObjectNotFoundException,
            ServletException, NamingException, PropertyNotFoundException {
            
            this.appendPropValueToNode(request, obj,prop, tags, node, objectRequest, listProp, false);
    }

    protected void appendPropValueToNode(HttpServletRequest request, TO<?> obj,
            ValueTO prop, Set<Tag> tags,Node node, ObjectRequest objectRequest,
            Collection<ValueTO> listProp, boolean isChild) throws ObjectNotFoundException,
            ServletException, NamingException, PropertyNotFoundException {
        Document doc = node.getOwnerDocument();
        if (prop == null) {
            return;
        }

        Object o = prop.getValue();

        boolean isHistorical = EnvTypeUtil.isHistorical(obj.getTypeName(),
                prop.getPropertyName(), this.env.get());
        Class<?> clazz = EnvTypeUtil.getPropertyClass(prop.getPropertyName(),
                obj.getTypeName(), this.env.get());

        Element child = doc.createElement(prop.getPropertyName());
        if (tags != null) {
            for (Tag tag:tags) {
                writeTag(child, tag);
            }
        }
        ObjectRequest linkRequest;

        if (clazz.equals(TO.class)) {
            
            if (o == null) {
                return;
            }
            if ((linkRequest = objectRequest.getLinkRequest(((TO<?>)o).getTypeName())) == null) {
                child.setAttribute("v", o.toString());
                node.appendChild(child);
                return;
            }

            TO<?> sensor = (TO<?>) o;
            Node s;

            NodeList links = ((Element) node).getElementsByTagName("links");
            if (links.getLength() > 0) {
                s = links.item(0);
            } else {
                s = XMLHelper.createXmlNode(doc, "links",
                        new HashMap<String, String>());
            }

            node.appendChild(s);

            
            CollectionTO colTo = getProperties(linkRequest.getProperties(), sensor, linkRequest);
            
            if (!isFilteredObject(linkRequest.getFilter(),
                    sensor.getTypeName(), colTo)) {
                return;
            }

            Map<String,Set<Tag>> tagsMap = getTagsMap(objectRequest, sensor);
            
            EnvXMLHelper.writeIDs(sensor, child);
            writePropsAndTags(request, child, colTo, tagsMap, linkRequest);
            s.appendChild(child);
            addRelatedObjectsToXMLNode(child, sensor,
                    linkRequest.getParentsRequests(), request, false);
            addRelatedObjectsToXMLNode(child, sensor,
                    linkRequest.getChildrenRequests(), request, true);

            // temp patch: since modbusproperty doesn't have range values,
            // retrieve it here and write
            if (EnvironmentTypes.isIonType(obj.getTypeName())) {
                try {
                    // add ranges doc
                    Document rangesDoc = DataRetrievingUtils
                            .getPropertyRangesDoc(obj.getTypeName(),
                                    prop.getPropertyName(), listProp);
                    if (rangesDoc != null) {
                        child.appendChild(doc.importNode(
                                (Node) rangesDoc.getDocumentElement(), true));
                    }

                    // add description doc
                    Document descrDoc = DataRetrievingUtils
                            .getPropertyDescrDoc(obj, prop.getPropertyName(),
                                    request);
                    if (descrDoc != null) {
                        child.appendChild(doc.importNode(
                                (Node) descrDoc.getDocumentElement(), true));
                    }
                } catch (ParserConfigurationException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }

            return;
        } else if (Number.class.isAssignableFrom(clazz)) {
            // Temporary patch. Show initial sensor state as disconnected.
            if ("lastValue".equals(prop.getPropertyName())
                    && (o == null || (o != null && (Double) o == -5000.0))) {
                o = -3000.0;
            }

            Timestamp ts = new Timestamp(prop.getTimeStamp());
            
            if (o != null) {
                if (o instanceof Double) {
                    child.setAttribute(
                            "v",
                            CommonFunctions.getFormattedNumber(
                                    numberFormat.get(), (Double) o));
                } else {
                    // hack
                    if ("mac".equals(prop.getPropertyName())) {
                        child.setAttribute("v", Long.toHexString((Long) o).toUpperCase());
                    } else if("pId".equals(prop.getPropertyName())){// another hack
                        child.setAttribute("v", CRC8Converter.codeToHr((Long)o));
                    } else {
                        child.setAttribute("v", ((Number) o).toString());
                    }
                }
            }else{
                if(ts.getTime() != 0){
                    child.setAttribute("v", ""); //if ts is not 0, it possibly means that value was incorrectly calculated, so add it to the XML  
                }
            }
            
            String units = Env.getUnits(obj, prop.getPropertyName(), request);
            if (units != null && !"".equals(units)) {
                child.setAttribute("u", units);
            }

            child.setAttribute("t", sdf.get().format(ts));
            child.setAttribute("lt", String.valueOf(ts.getTime()));

            if (isHistorical) {
                child.setAttribute("h", "true");
            }
        //add Collection type of properties
        }else if (clazz.equals(Collection.class)){
            @SuppressWarnings("unchecked")
            Collection<TO<?>> props = ((Collection<TO<?>>) prop.getValue());
            
            for(TO<?> propVal: props){
        //		appendTOPropValueToNode(request, propVal, objectRequest, child);
                Collection<ValueTO> propVals = this.env.get().getAllPropertiesValues(propVal);
                Map<String,Set<Tag>> tagsMap = getTagsMap(objectRequest, propVal);
                Element child2 = doc.createElement(propVal.getTypeName().toString());
                child2.setAttribute("id", propVal.getTypeName().toString() + ":" +propVal.getID().toString());
                if(objectRequest.displayPropertyObjectsProperties()&&!isChild){
                    for (ValueTO propValTo : propVals) {					
                        appendPropValueToNode(request, propVal, propValTo,tagsMap.get(propValTo.getPropertyName()),child2, objectRequest, propVals, true);
                    }
                }
                addRelatedObjectsToXMLNode(child2, propVal, objectRequest.getParentsRequests(), request, false);
                addRelatedObjectsToXMLNode(child2, propVal, objectRequest.getChildrenRequests(), request, true);
                child.appendChild(child2);
                
            }
            
            //add Collection type of properties
        } else if (clazz.equals(PropertyTO.class)) {
            //TODO: this is quick patch. Generic implementation is required.
            if (o != null) {
                PropertyTO pto = (PropertyTO)o;
                Object propValue = null;
                try {
                    propValue = env.get().getPropertyValue(pto.getObjId(), pto.getPropertyName(), Object.class);
                } catch (Exception e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
                
                if (propValue != null) {
                    child.setAttribute("pv", propValue.toString());
                }
                child.setAttribute("v", o.toString());
            }
        } else if (o != null) {
            child.setAttribute("v", o.toString());
        }

        node.appendChild(child);
    }
    
    @SuppressWarnings("unused")
    private void appendTOPropValueToNode(HttpServletRequest request,  ValueTO prop, ObjectRequest objectRequest, Node node) throws ServletException, ObjectNotFoundException, PropertyNotFoundException, NamingException{

        ObjectRequest linkRequest;
        Document doc = node.getOwnerDocument();
        Element child = doc.createElement(prop.getPropertyName());
        
        //
        Object o = prop.getValue();

        if (o == null) {
            return;
        }
        if ((linkRequest = objectRequest.getLinkRequest(((TO<?>)o).getTypeName())) == null) {
            child.setAttribute("v", o.toString());
            node.appendChild(child);
            return;
        }

        TO<?> sensor = (TO<?>) o;
        Node s;

        NodeList links = ((Element) node).getElementsByTagName("links");
        if (links.getLength() > 0) {
            s = links.item(0);
        } else {
            s = XMLHelper.createXmlNode(doc, "links",
                    new HashMap<String, String>());
        }

        node.appendChild(s);

        
        CollectionTO colTo = getProperties(linkRequest.getProperties(), sensor, linkRequest);
        
        if (!isFilteredObject(linkRequest.getFilter(), sensor.getTypeName(), colTo)) {
            return;
        }

        Map<String,Set<Tag>> tagsMap = getTagsMap(objectRequest, sensor);
        
        EnvXMLHelper.writeIDs(sensor, child);
        writePropsAndTags(request, child, colTo, tagsMap, linkRequest);
        s.appendChild(child);
        addRelatedObjectsToXMLNode(child, sensor,
                linkRequest.getParentsRequests(), request, false);
        addRelatedObjectsToXMLNode(child, sensor,
                linkRequest.getChildrenRequests(), request, true);

        return;
    
    }

    private Collection<TO<?>> getFilteredObjects(Map<String, String> filterMap,
            String objType, Environment env) throws ServletException {
        Collection<TO<?>> objColl = new HashSet<TO<?>>();
        ValueTO[] vp = new ValueTO[filterMap.size()];
        int i = 0;
        for (String s : filterMap.keySet()) {
            vp[i++] = new ValueTO(s, getObjectByClass(s, filterMap.get(s),
                    objType, env));
        }
        objColl = env.getObjects(objType, vp);
        return objColl;
    }

    private boolean isFilteredObject(Map<String, String> filterMap,
            String objType, CollectionTO colTo) throws ServletException {
        int isNeeded = 0;
        if (filterMap.size() > 0) {
            for (String p : filterMap.keySet()) {
                String propName = colTo.getSinglePropValue(p).getPropertyName();
                ValueTO v = new ValueTO(p, getObjectByClass(propName,
                        filterMap.get(p), objType, env.get()));
                if (v.getPropertyName().equals(
                        colTo.getSinglePropValue(propName).getPropertyName())
                        && v.getValue().equals(
                                colTo.getSinglePropValue(propName).getValue())) {
                    isNeeded++;
                }
            }
            if (isNeeded != filterMap.size()) {
                return false;
            }
        }
        return true;
    }

    private Object getObjectByClass(String propName, Object propValue,
            String objType, Environment env) throws ServletException {
        Class<?> clazz;
        Object valueToSet = null;

        try {
            clazz = EnvTypeUtil.getPropertyClass(propName, objType, env);
            if (String.class.equals(clazz)) {
                valueToSet = propValue;
            } else {
                Method valueOf = clazz.getDeclaredMethod("valueOf",
                        new Class[] { String.class });
                valueToSet = valueOf.invoke(null, propValue);
            }
        } catch (Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
        return valueToSet;
    }

    /**
     * @author pahunov
     * 
     *         Parses request parameters
     */
    protected static class ObjectRequest {

        public static final String ALL_LINK_TYPES = "*"; // client will use it as link type when it is needed to expand links of all types
        
        public enum Params {
            properties, tags, filter, type, id, requestDirectChildrenOnly, requestFilteredChildrenOnly, requestDirectParentsOnly, parents, links, children, displayPropertyObjectsProperties, excludedProperties
        }
        
        /**
         * Map of property name - tag name pairs 
         */
        private Map<String,Set<String>> tags = null;

        /**
         * List of properties to return
         */
        private String[] properties = null;

        /**
         * Array of properties to return
         */
        private Map<String, String> filter = null;

        /**
         * Type of objects to request Should be set only for not root requests
         */
        private String type = null;

        /**
         * Id of objects to request Should be set only for root requests
         */
        private TO<?> id = null;

        /**
         * Child objects to request
         */
        private ObjectRequest[] childrenRequests = null;

        /**
         * Parent objects to request
         */
        private ObjectRequest[] parentsRequests = null;

        /**
         * Objects links to request
         */
        private ObjectRequest[] linkRequests = null;

        /**
         * Show which method to call: env.getChildren() or
         * env.getRelatedObjects()
         */
        private boolean _requestDirectChildrenOnly = true;
        
        /**
         * Show which method to call: getFilteredObjects() or
         * env.getRelatedObjects()
         */
        private boolean _requestFilteredChildrenOnly = false;

        /**
         * Show which method to call: env.getParent() or env.getRelatedObjects()
         */
        private boolean _requestDirectParentsOnly = true;

        /**
         * Set in "true" if need description of object included in property of Collection<TO<?>> type
         */
        private boolean _displayPropertyObjectsProperties = false;
        
        /**
         * List of properties that should be excluded
         */
        private Set<String> excludedProperties = null;
        
        public ObjectRequest(String jsonString) throws JSONException {
            JSONObject obj = new JSONObject(jsonString);
            if (obj.has(Params.properties.toString())) {
                JSONArray propsJsonArray = obj.getJSONArray(Params.properties
                        .toString());
                properties = new String[propsJsonArray.length()];
                for (int i = 0; i < propsJsonArray.length(); i++) {
                    properties[i] = propsJsonArray.getString(i);
                }
            }
            
            if (obj.has(Params.tags.toString())) {
                JSONArray propsJsonArray = obj.getJSONArray(Params.tags
                        .toString());
                tags = new HashMap<String, Set<String>>(propsJsonArray.length());
                for (int i = 0; i < propsJsonArray.length(); i++) {
                    JSONObject tagJ = propsJsonArray.getJSONObject(i);
                    String propname = tagJ.getString("propname");
                    JSONArray tagnamesJsonArray = tagJ.getJSONArray("tagnames");
                    Set<String> tagnames = new HashSet<String>();
                    for (int j=0;j<tagnamesJsonArray.length();j++) {
                        tagnames.add(tagnamesJsonArray.getString(j));
                    }
                    if (tags.containsKey(propname)) {
                        tags.get(propname).addAll(tagnames);
                    } else {
                        tags.put(propname, tagnames);
                    }
                }
            }

            if (obj.has(Params.filter.toString())) {
                JSONArray filterJsonArray = obj.getJSONArray(Params.filter
                        .toString());
                filter = new HashMap<String, String>();
                for (int i = 0; i < filterJsonArray.length(); i++) {
                    JSONObject o = filterJsonArray.getJSONObject(i);
                    filter.put(o.get("name").toString(), o.get("value")
                            .toString());
                }
            }
            if (obj.has(Params.type.toString())) {
                type = obj.getString(Params.type.toString());
            }
            if (obj.has(Params.id.toString())) {
                id = TOFactory.getInstance().loadTO(
                        obj.getString(Params.id.toString()));
            }
            if (obj.has(Params.requestDirectChildrenOnly.toString())) {
                _requestDirectChildrenOnly = obj
                        .getBoolean(Params.requestDirectChildrenOnly.toString());
            }
            if (obj.has(Params.requestFilteredChildrenOnly.toString())) {
                _requestFilteredChildrenOnly = obj
                        .getBoolean(Params.requestFilteredChildrenOnly.toString());
            }
            if (obj.has(Params.requestDirectParentsOnly.toString())) {
                _requestDirectParentsOnly = obj
                        .getBoolean(Params.requestDirectParentsOnly.toString());
            }
            if (obj.has(Params.displayPropertyObjectsProperties.toString())) {
                _displayPropertyObjectsProperties = obj
                        .getBoolean(Params.displayPropertyObjectsProperties.toString());
            }			
            if (obj.has(Params.parents.toString())) {
                JSONArray parentsJsonArray = obj.getJSONArray(Params.parents
                        .toString());
                parentsRequests = new ObjectRequest[parentsJsonArray.length()];
                for (int i = 0; i < parentsJsonArray.length(); i++) {
                    parentsRequests[i] = new ObjectRequest(parentsJsonArray
                            .getJSONObject(i).toString());
                }
            }

            if (obj.has(Params.links.toString())) {
                JSONArray linksJsonArray = obj.getJSONArray(Params.links
                        .toString());
                linkRequests = new ObjectRequest[linksJsonArray.length()];
                for (int i = 0; i < linksJsonArray.length(); i++) {
                    linkRequests[i] = new ObjectRequest(linksJsonArray
                            .getJSONObject(i).toString());
                }
            }

            if (obj.has(Params.children.toString())) {
                JSONArray childrenJsonArray = obj.getJSONArray(Params.children
                        .toString());
                childrenRequests = new ObjectRequest[childrenJsonArray.length()];
                for (int i = 0; i < childrenJsonArray.length(); i++) {
                    childrenRequests[i] = new ObjectRequest(childrenJsonArray
                            .getJSONObject(i).toString());
                }
            }
            
            if (obj.has(Params.excludedProperties.toString())) {
                excludedProperties = new HashSet<String>();
                JSONArray exPropsJsonArray = obj.getJSONArray(Params.excludedProperties.toString());				
                for (int i = 0; i < exPropsJsonArray.length(); i++) {
                    excludedProperties.add(exPropsJsonArray.getString(i));
                }
            }
        }

        public Set<String> getExcludedProperties () {
            return excludedProperties;
        }
        
        public String[] getProperties() {
            return properties;
        }
        
        public void setProperties (String[] properties) {
            this.properties = properties;
        }
        
        public Map<String,Set<String>> getTags() {
            return tags;
        }
        
        public void setTags (Map<String,Set<String>> tags) {
            this.tags = tags;
        }
                

        public Map<String, String> getFilter() {
            return (filter != null) ? filter : new HashMap<String, String>();
        }

        public String getType() {
            return type;
        }

        public TO<?> getId() {
            return id;
        }

        public ObjectRequest getLinkRequest(String type) {
            if (linkRequests != null && type != null) {
                for (ObjectRequest linkRequest : linkRequests) {
                    String rType = linkRequest.getType();
                    if (type.equals(rType) || ALL_LINK_TYPES.equals(rType)) {
                        return linkRequest;
                    }
                }
            }
            return null;
        }

        public ObjectRequest[] getChildrenRequests() {
            return childrenRequests;
        }

        public ObjectRequest[] getParentsRequests() {
            return parentsRequests;
        }

        public boolean requestDirectChildrenOnly() {
            return _requestDirectChildrenOnly;
        }

        public boolean requestFilteredChildrenOnly() {
            return _requestFilteredChildrenOnly;
        }

        public boolean requestDirectParentsOnly() {
            return _requestDirectParentsOnly;
        }
        
        public boolean displayPropertyObjectsProperties() {
            return _displayPropertyObjectsProperties;
        }	
        
        public String toString() {
            StringBuilder str = new StringBuilder();
            
            str.append("ID: " + ((this.id == null) ? "null" : (this.id.getTypeName() + ":" + this.id.getID())) + "\r\n");
            str.append("TYPE: " + ((this.type == null) ? "null" : this.type) + "\r\n");
            str.append("DISPLAY PROPERTY OBJECT PROPERTIES: " + Boolean.toString(this._displayPropertyObjectsProperties) + "\r\n");
            str.append("REQUEST DIRECT CHILDREN ONLY: " + Boolean.toString(this._requestDirectChildrenOnly) + "\r\n");
            str.append("REQUEST DIRECT PARENTS ONLY: " + Boolean.toString(this._requestDirectParentsOnly) + "\r\n");
            str.append("REQUEST FILTERED CHILDREN ONLY: " + Boolean.toString(this._requestFilteredChildrenOnly) + "\r\n");
            str.append("FILTER: " + ((this.filter == null) ? "null" : this.filter.toString()) + "\r\n");
            str.append("TAGS: " + ((this.tags == null) ? "null" : this.tags.toString()) + "\r\n");
            str.append("PROPERTIES: " + ((this.properties == null) ? "null" : this.properties.toString()) + "\r\n");
            str.append("EXCLUDED PROPERTIES: " + ((this.excludedProperties == null) ? "null" : this.excludedProperties.toString()) + "\r\n");
            str.append("PARENT REQUESTS:\r\n");
            if (this.parentsRequests != null) {
                for (ObjectRequest parent : this.parentsRequests) {
                    str.append(parent.toString());
                }
            }
            str.append("CHILDREN REQUESTS:\r\n");
            if (this.childrenRequests != null) {
                for (ObjectRequest child : this.childrenRequests) {
                    str.append(child.toString());
                }
            }
            str.append("LINK REQUESTS:\r\n");
            if (this.linkRequests != null) {
                for (ObjectRequest link : this.linkRequests) {
                    str.append(link.toString());
                }
            }
            return str.toString();
            
        }
        
    }
}
