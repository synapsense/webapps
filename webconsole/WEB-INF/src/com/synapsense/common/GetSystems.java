package com.synapsense.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.service.UnitSystemsService;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.util.unitconverter.UnitSystem;
import com.synapsense.util.unitconverter.UnitSystems;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;


public class GetSystems extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 3544135144558306325L;
    static private final Log LOG = LogFactory.getLog(GetSystems.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        
        
        
        try {
            Document doc = XMLHelper.createNewXmlDocument("systems");
            Element rootElement = doc.getDocumentElement();
            UnitSystemsService uss = Env.getUnitSystemsService();
            UnitSystems unitSystems = uss.getUnitSystems();
            
            for (UnitSystem unitSystem  : unitSystems.getUnitSystems().values()) {
                Element s = doc.createElement("system");
                rootElement.appendChild(s);
                s.setAttribute("name", unitSystem.getName());
                for (Dimension dimension : unitSystem.getDimensionsMap().values()) {
                    Element d = doc.createElement("dimension");
                    s.appendChild(d);
                    d.setAttribute("name", dimension.getName());
                    d.setAttribute("units", dimension.getUnits());
                    d.setAttribute("long", dimension.getLongUnits());
                }
            }

            Element s = doc.createElement("simpleDimensions");
            rootElement.appendChild(s);
            for (Dimension dimension : unitSystems.getSimpleDimensions().values()) {
                Element d = doc.createElement("dimension");
                s.appendChild(d);
                d.setAttribute("name", dimension.getName());
                d.setAttribute("units", dimension.getUnits());
                d.setAttribute("long", dimension.getLongUnits());
            }
            
            

            sendResponse(resp, doc);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

}
