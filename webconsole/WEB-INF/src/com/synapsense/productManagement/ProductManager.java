package com.synapsense.productManagement;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.session.ApplicationWatch;
import com.synapsense.utilities.XMLHelper;

public class ProductManager {
    private static final Log LOG = LogFactory.getLog(ProductManager.class);
    public static final String SYNAP_PRODUCT = "synap";
    
    private String product = SYNAP_PRODUCT;
    
    private static ProductManager instance = null;
    
    
    private ProductManager(){		
    }
    
    public static void updateProduct(){
        if(instance == null){
            instance = new ProductManager();
        }
        
        try{
            //retrieve instance.product from configuration file 
            retrieveProductFromConfig();
        }catch(Exception e){
            LOG.error("Exception occured during product type reading! Use synap type by default..", e);
            instance.product = SYNAP_PRODUCT;
        }
    }
    
    public static ProductManager getInstance(){
        if(instance != null){
            return instance;
        }
        
        instance = new ProductManager();
        
        //retrieve instance.product from configuration file
        updateProduct();
        
        return instance;		
    }
    
    public String getProduct(){
        return product;
    }
    
    private static void retrieveProductFromConfig() throws SAXException, IOException, ParserConfigurationException, XPathExpressionException{
        String confFileName = getStandardProductConfigFileName();
        // Get XML-configuration document
        File confFile = new File(confFileName);
        Document confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);

        // get xPath variable
        XPath xPath = XPathFactory.newInstance().newXPath();
        
        XPathExpression xPathColumn = xPath.compile("product/type");
        
        NodeList productType = (NodeList) xPathColumn.evaluate(confDoc, XPathConstants.NODESET);
        
        if (productType != null) {
            String prod = productType.item(0).getTextContent();
            if(SYNAP_PRODUCT.equals(prod)){
                instance.product = SYNAP_PRODUCT;
            }else{
                LOG.error("Product type is not defined! Use synap by default. ");
            }
        }
    }
    
    private static String getStandardProductConfigFileName(){
        String confUrl = "conf/productconf.xml";
        String configFileName = ApplicationWatch.getPath() + confUrl;
        
        return configFileName;
    }
    
    
}
