package com.synapsense.elementmodel.es;

import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.List;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.elementmodel.basic.xmldumper.XmlDumperUtils;
import com.synapsense.nwkstatistic.NwkStatistic;
import com.synapsense.nwkstatistic.PropEntry;
import com.synapsense.service.impl.messages.base.StatisticData;

public class NodeStatisticXmlWriter {
    private static final Log LOG = LogFactory.getLog(NodeStatisticXmlWriter.class);
    
    public static void writeRecordCollection(Collection<StatisticData> statDataCollection, Writer writer, EnvContext envCtx) throws IOException{
        List<PropEntry> propEntries = NwkStatistic.getPropEntries();
        SimpleDateFormat sdf = envCtx.getFormatter();
        for(StatisticData sData : statDataCollection){
            writeStatistic(sData, writer, propEntries, sdf);
        }
    }
    
    public static void writeStatistic(StatisticData statData, Writer writer, EnvContext envCtx) throws IOException{
        if(statData != null){
            List<PropEntry> propEntries = NwkStatistic.getPropEntries();
            SimpleDateFormat sdf = envCtx.getFormatter();
            writeStatistic(statData, writer, propEntries, sdf);
        }
    }
    
    private static void writeStatistic(StatisticData statData, Writer writer, List<PropEntry> propEntries, SimpleDateFormat sdf) throws IOException{
        writer.write("\n<STAT>");
        for(PropEntry entry : propEntries){
            try {
                Method meth = StatisticData.class.getMethod(entry.getFuncName());
                Object result = meth.invoke(statData);
                Class<?> clazz = meth.getReturnType();
                
                if(result != null){
                    String resultStr = NwkStatistic.convertResultToString(result, clazz, sdf);
                    XmlDumperUtils.writeXmlNodeWithAttributes(entry.getPropName(), null, writer, "v", resultStr);
                }else{
                    XmlDumperUtils.writeXmlNode(entry.getPropName(), null, writer);
                }
            } catch (Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
        }
        writer.write("\n</STAT>");
    }
}
