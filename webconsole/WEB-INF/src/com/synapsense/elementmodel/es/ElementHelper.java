package com.synapsense.elementmodel.es;

import com.synapsense.elementmodel.basic.ModelElement;

public class ElementHelper {
    
    public static ModelElement createModelElementWithValue(String elementName, Object value){
        ModelElement elem = new ModelElement(elementName);
        elem.addAttribute("v", value);
        return elem;
    }
}
