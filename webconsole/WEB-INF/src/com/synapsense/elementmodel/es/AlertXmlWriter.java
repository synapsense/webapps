package com.synapsense.elementmodel.es;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dto.Alert;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.elementmodel.basic.xmldumper.XmlDumperUtils;
import com.synapsense.service.Environment;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.es.EnvironmentTypes;

public class AlertXmlWriter {
    private static final Log LOG = LogFactory.getLog(AlertXmlWriter.class);
    
    public static void writeAlertCollection(Collection<Alert> alerts, Writer writer, EnvContext envCtx) throws IOException{
        for(Alert alert : alerts){
            writeAlert(alert, writer, envCtx.getFormatter(), envCtx.getEnv(), envCtx.getReq());
        }
    }
    
    private static void writeAlert(Alert alert, Writer writer, SimpleDateFormat formatter, Environment env, HttpServletRequest req) throws IOException{
        writer.write("\n<alert>");
            
         // remove "ConsoleAlert" prefix and add name
         String alertName = alert.getName().replaceFirst(Constants.CONSOLE_ALERT_PREFIX, "");
         XmlDumperUtils.writeXmlNode("alert_name", alertName, writer);
         XmlDumperUtils.writeXmlNode("alert_id", alert.getAlertId().toString(), writer);
         
         String time_stamp = formatter.format(new Date(alert.getAlertTime()));
         
         writer.write("\n<time_stamp");
         if(alert.getAlertTime() != null){
            writer.write(" lt=\"" + String.valueOf(alert.getAlertTime()) + "\"");
         }
         writer.write(">");
         writer.write(time_stamp + "</time_stamp>");
         
         String objectName = "text_system";
         TO<?> hostTO = alert.getHostTO();
         if(!hostTO.equals(TOFactory.EMPTY_TO)){
             try{
                 
                 Collection<TO<?>> tos = new HashSet<TO<?>>();
                 tos.add(hostTO);
                 CollectionTO colTo = env.getPropertyValue(tos, new String[] {"name", "location"}).iterator().next(); 
                 objectName = (String) colTo.getSinglePropValue("name").getValue();
                 
                 XmlDumperUtils.writeXmlNode("host", TOFactory.getInstance().saveTO(alert.getHostTO()), writer);
                 
                 ValueTO locVto = colTo.getSinglePropValue("location");
                 if (locVto != null) {
                     String location = (String) locVto.getValue();
                     if (location != null && !location.isEmpty()) {
                        XmlDumperUtils.writeXmlNode("location", location, writer);
                     }
                 }
                 
             }catch(Exception e){
                 LOG.error(e.getLocalizedMessage(), e);
             }
         }
         
        XmlDumperUtils.writeXmlNode("object_name", objectName, writer);
        XmlDumperUtils.writeXmlNode("alert_message", alert.getFullMessage(), writer);
        XmlDumperUtils.writeXmlNode("alert_pure_message", alert.getMessage(), writer); // it is needed for dismissed alert animation
        XmlDumperUtils.writeXmlNode("alert_isActive", alert.getStatus().toString(), writer); 
       
        XmlDumperUtils.writeXmlNode("alert_description", alert.getTypeDescr(), writer);
        XmlDumperUtils.writeXmlNode("alert_priority_name", alert.getPriority(), writer);           
        XmlDumperUtils.writeXmlNode("alert_priority_number", "" + AlertUtils.priorityNameToInt(alert.getPriority()), writer);
        
        XmlDumperUtils.writeXmlNode("acknowledgement", alert.getAcknowledgement(), writer);
         
         Date ack_time = alert.getAcknowledgementTime();
         if (ack_time != null) {
            XmlDumperUtils.writeXmlNode("ack_time", (ack_time != null) ? formatter.format(ack_time) : "", writer);
         }
         
         String user = alert.getUser();
         if (user != null) {
            XmlDumperUtils.writeXmlNode("ack_user", user, writer);
         }
         
         //adding DC info
         boolean getDCinfo = Boolean.valueOf(req.getParameter("getDCinfo"));
         if(getDCinfo){
            String dcName = "";
             try {
                TO<?> objId = alert.getHostTO();
                if (EnvironmentTypes.TYPE_DC.equals(objId.getTypeName())) {
                    dcName = env.getPropertyValue(objId, "name", String.class);
                } else {
                    Collection<TO<?>> objDCs = env.getRelatedObjects(objId,  EnvironmentTypes.TYPE_DC, false); 
                    if(!objDCs.isEmpty()){
                        TO<?> dc = objDCs.iterator().next();
                        dcName = env.getPropertyValue(dc, "name", String.class);
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
            XmlDumperUtils.writeXmlNode("dc_site", dcName, writer);
        }
         
        writer.write("\n</alert>");
    }
}
