package com.synapsense.elementmodel.es;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.service.Environment;

public class EnvContext {
    protected Environment env;
    protected SimpleDateFormat formatter;
    protected HttpServletRequest req;
    
    public EnvContext(){
        super();
    }
    
    public EnvContext(Environment env, SimpleDateFormat formatter) {
        super();
        this.env = env;
        this.formatter = formatter;
    }
    
    public EnvContext(Environment env, SimpleDateFormat formatter, HttpServletRequest req) {
        super();
        this.env = env;
        this.formatter = formatter;
        this.req = req;
    }
    
    public HttpServletRequest getReq() {
        return req;
    }

    public void setReq(HttpServletRequest req) {
        this.req = req;
    }

    public Environment getEnv() {
        return env;
    }
    public void setEnv(Environment env) {
        this.env = env;
    }
    public SimpleDateFormat getFormatter() {
        return formatter;
    }
    public void setFormatter(SimpleDateFormat formatter) {
        this.formatter = formatter;
    }
    
}
