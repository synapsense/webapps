package com.synapsense.elementmodel.es;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Collection;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.elementmodel.basic.xmldumper.XmlDumperUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityRecord;

public class ActivityRecordXmlWriter {
    private static final Log LOG = LogFactory.getLog(ActivityRecordXmlWriter.class);
    
    public static void writeRecordCollection(Collection<ActivityRecord> records, Writer writer, EnvContext envCtx) throws IOException{
        for(ActivityRecord record : records){
            writeRecord(record, writer, envCtx);
        }
    }
    
    public static void writeRecord(ActivityRecord record, Writer writer, EnvContext envCtx) throws IOException{
        Environment env = envCtx.getEnv();
        SimpleDateFormat sdf = envCtx.getFormatter();
        TO<?> dc = record.getObject();
        String dcName = getParentName(dc, env);
        
        writer.write("\n<activity>");		
        XmlDumperUtils.writeXmlNode("activity_action_type", record.getAction(), writer);
        XmlDumperUtils.writeXmlNode("activity_desc", record.getDescription(), writer);
        XmlDumperUtils.writeXmlNode("username", record.getUserName(), writer);
        
        XmlDumperUtils.writeXmlNodeWithAttributes("timestamp", sdf.format(record.getTime()), writer, "l", String.valueOf(record.getTime().getTime()));
        
        XmlDumperUtils.writeXmlNode("dc", dcName, writer);
        XmlDumperUtils.writeXmlNode("id", record.getId().toString(), writer);
        XmlDumperUtils.writeXmlNode("isSystem", String.valueOf(dc.equals(TOFactory.EMPTY_TO)), writer);
        XmlDumperUtils.writeXmlNode("activity_module", record.getModule(), writer);		
        writer.write("\n</activity>");
    }
    
    private static String getParentName(TO<?> parent, Environment env){
        String parentName = "";
        try {
            parentName = (parent.equals(TOFactory.EMPTY_TO)) ? "" : env.getPropertyValue(parent, "name", String.class);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return parentName;
    }
            
}
