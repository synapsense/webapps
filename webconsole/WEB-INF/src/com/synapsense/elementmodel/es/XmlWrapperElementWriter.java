package com.synapsense.elementmodel.es;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import com.synapsense.dto.Alert;
import com.synapsense.elementmodel.basic.WrapperModelElement;
import com.synapsense.elementmodel.basic.xmldumper.WrapperElementWriter;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.service.impl.messages.base.StatisticData;

public class XmlWrapperElementWriter implements WrapperElementWriter{
    EnvContext envCtx;
        
    public XmlWrapperElementWriter(){
        super();
    }
    public XmlWrapperElementWriter(EnvContext envCtx) {
        super();
        this.envCtx = envCtx;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void writeWrapperElem(WrapperModelElement modelElement, Writer writer) throws IOException {
        
        Object object = modelElement.getObject();
        if(object instanceof Collection){
            @SuppressWarnings("rawtypes")
            Collection c = (Collection)object; 
            if(!c.isEmpty()){
                Object obj = c.iterator().next();
                if(obj instanceof Alert){
                    AlertXmlWriter.writeAlertCollection((Collection<Alert>)object, writer, envCtx);
                }else if(obj instanceof ActivityRecord){
                    ActivityRecordXmlWriter.writeRecordCollection((Collection<ActivityRecord>)object, writer, envCtx);
                }else if(obj instanceof StatisticData){
                    NodeStatisticXmlWriter.writeRecordCollection((Collection<StatisticData>)object, writer, envCtx);
                }
            }
        }
    }

    public void setEnvCtx(EnvContext envCtx) {
        this.envCtx = envCtx;
    }
}
