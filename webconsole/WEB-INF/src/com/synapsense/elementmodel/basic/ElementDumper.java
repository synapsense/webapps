package com.synapsense.elementmodel.basic;

import java.io.IOException;
import java.io.Writer;

public abstract class ElementDumper {
    public abstract void dump(ModelElement e, Writer writer) throws IOException;
}
