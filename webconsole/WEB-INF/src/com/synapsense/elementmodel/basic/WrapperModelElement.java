package com.synapsense.elementmodel.basic;


public class WrapperModelElement extends BaseModelElement{
    protected Object object = null;
    
    public WrapperModelElement() {
        super();
    }
    
    public WrapperModelElement(Object object) {
        super();
        this.object = object;
    }

    public Object getObject() {
        return object;
    }

    public void setObject(Object object) {
        this.object = object;
    }
}
