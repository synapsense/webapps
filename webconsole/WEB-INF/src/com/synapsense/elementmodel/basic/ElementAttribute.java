package com.synapsense.elementmodel.basic;

public class ElementAttribute {
    protected String name = "";
    protected Object value = "";
    
    public ElementAttribute(){
    }
    
    public ElementAttribute(String name, Object value){
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Object getValue() {
        return value;
    }
    public void setValue(Object value) {
        this.value = value;
    }
}
