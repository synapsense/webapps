package com.synapsense.elementmodel.basic;

import java.util.ArrayList;
import java.util.List;


public class ModelElement extends BaseModelElement {
    protected List<BaseModelElement> childElements = null;
    protected String innerText = null;
    protected String name = "";
    protected List<ElementAttribute> attributes = null;
    
    public ModelElement(){
    }
    
    public ModelElement(String name){
        this.name = name;
    }
    
    public void addAttribute(String attrName, Object attrValue){
        createAttributesArrayIfNotExists();
        attributes.add(new ElementAttribute(attrName, attrValue));
    }
    
    public void addAttribute(ElementAttribute attr){
        createAttributesArrayIfNotExists();
        attributes.add(attr);
    }
    
    public void addChild(BaseModelElement child){
        createChildArrayIfNotExists();
        childElements.add(child);
    }
    
    public List<BaseModelElement> getChildElements() {
        return childElements;
    }
    
    public void setChildElements(List<BaseModelElement> childElements) {
        this.childElements = childElements;
    }
    public String getInnerText() {
        return innerText;
    }
    public void setInnerText(String innerText) {
        this.innerText = innerText;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<ElementAttribute> getAttributes() {
        return attributes;
    }
    public void setAttributes(List<ElementAttribute> attributes) {
        this.attributes = attributes;
    }
    
    private void createChildArrayIfNotExists(){
        if(childElements == null){
            childElements = new ArrayList<BaseModelElement>(1);
        }
    }
    
    private void createAttributesArrayIfNotExists(){
        if(attributes == null){
            attributes = new ArrayList<ElementAttribute>(1);
        }
    }
}
