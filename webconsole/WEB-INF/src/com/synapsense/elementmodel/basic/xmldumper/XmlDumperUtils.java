package com.synapsense.elementmodel.basic.xmldumper;

import java.io.IOException;
import java.io.Writer;

import com.synapsense.utilities.CommonFunctions;

public class XmlDumperUtils {
    public static void writeXmlNode(String nodeName, String value, Writer writer) throws IOException{
        if(value == null || value.isEmpty()){
            writer.write("\n<" + nodeName + "/>");
        }else{
            writer.write("\n<" + nodeName + ">" + CommonFunctions.escapeXML(value) + "</" + nodeName + ">");
        }
    }
    
    /**
     * Writes xml node with attributes.
     * NOTE: variable number of "attributes" parameters is used to avoid addition "ElementAttribute" classes creation 
     * @param nodeName
     * @param value
     * @param writer
     * @param attributes - attributes[i] - attribute name, attributes[i + 1] - attribute value 
     * @throws IOException
     */
    public static void writeXmlNodeWithAttributes(String nodeName, String value, Writer writer, String... attributes) throws IOException{
        if(value == null || value.isEmpty()){
            writer.write("\n<" + nodeName);
            writeXmlAttributes(writer, attributes);
            writer.write("/>");
        }else{
            writer.write("\n<" + nodeName);
            writeXmlAttributes(writer, attributes);
            writer.write(">" + CommonFunctions.escapeXML(value) + "</" + nodeName + ">");
        }
    }
    
    private static void writeXmlAttributes(Writer writer, String... attributes) throws IOException{
        for(int i = 0; i < attributes.length - 1; i += 2){
            writer.write(" " + attributes[i] + "=\"" + CommonFunctions.escapeXML(attributes[i + 1]) + "\"");
        }
    }
}
