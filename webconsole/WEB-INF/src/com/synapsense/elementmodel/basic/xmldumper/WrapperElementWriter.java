package com.synapsense.elementmodel.basic.xmldumper;

import java.io.IOException;
import java.io.Writer;

import com.synapsense.elementmodel.basic.WrapperModelElement;

public interface WrapperElementWriter {
    public void writeWrapperElem(WrapperModelElement modelElement, Writer writer) throws IOException;
}
