package com.synapsense.elementmodel.basic.xmldumper;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import com.synapsense.elementmodel.basic.BaseModelElement;
import com.synapsense.elementmodel.basic.ElementAttribute;
import com.synapsense.elementmodel.basic.ElementDumper;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.elementmodel.basic.WrapperModelElement;
import com.synapsense.utilities.CommonFunctions;

public class XmlElementDumper extends ElementDumper {
    protected WrapperElementWriter wrapperElemWriter = null;
    
    public XmlElementDumper() {
        super();
    }
    
    public XmlElementDumper(WrapperElementWriter wrapperElemWriter) {
        super();
        this.wrapperElemWriter = wrapperElemWriter;
    }
    
    public void setWrapperElemWriter(WrapperElementWriter wrapperElemWriter) {
        this.wrapperElemWriter = wrapperElemWriter;
    }

    @Override
    public void dump(ModelElement e, Writer writer) throws IOException{
        writeModelElem(e, writer);
    }

    private void writeModelElem(ModelElement modelElement, Writer writer) throws IOException{
        writer.write("\n");
        writer.write("<" + modelElement.getName());
        
        //attributes
        if(modelElement.getAttributes() != null){
            for(ElementAttribute attr : modelElement.getAttributes()){
                Object value = attr.getValue();
                if(value != null){
                    writer.write(" " + attr.getName() + "=\"" + CommonFunctions.escapeXML(value.toString()) + "\"");
                }
            }
        }
        writer.write(">");
        
        //inner text
        if(modelElement.getInnerText() != null){
            writer.write(CommonFunctions.escapeXML(modelElement.getInnerText()));
        }
        
        //children
        if(modelElement.getChildElements() != null){
            for(BaseModelElement childElement : modelElement.getChildElements()){
                if(childElement instanceof ModelElement){
                    writeModelElem((ModelElement)childElement, writer);
                }else if(childElement instanceof WrapperModelElement){
                    writeWrapperElem((WrapperModelElement)childElement, writer);
                }
            }
        }
        
        writer.write("</" + modelElement.getName() + ">");
    }
    
    private void writeWrapperElem(WrapperModelElement modelElement, Writer writer) throws IOException{
        if(wrapperElemWriter != null){
            wrapperElemWriter.writeWrapperElem(modelElement, writer);
        }
    }
    
    
    /**
     * Just example of usage
     * @param args
     * @throws IOException
     */
    public static void main(String args[]) throws IOException{
        ModelElement firstElement = new ModelElement("Container");
        firstElement.addAttribute("color", "green");
        firstElement.addAttribute("mode", "1");
        
        ModelElement otherElement = new ModelElement("book");
        otherElement.addAttribute("weight", "20");
        otherElement.addAttribute("auth", "ttt");			
        firstElement.addChild(otherElement);
        
        otherElement.addChild(new ModelElement("pageOne"));
        otherElement.addChild(new ModelElement("pageTwo"));
        
        otherElement = new ModelElement("bottle");
        otherElement.addAttribute("height", "10");
        otherElement.addAttribute("label", "kkk");			
        firstElement.addChild(otherElement);
        
        StringWriter sw = new StringWriter();
        new XmlElementDumper().dump(firstElement, sw);
        
        System.out.println(sw.getBuffer());
    }
}
