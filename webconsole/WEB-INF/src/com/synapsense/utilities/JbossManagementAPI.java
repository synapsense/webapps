package com.synapsense.utilities;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.NameCallback;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.callback.UnsupportedCallbackException;
import javax.security.sasl.RealmCallback;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.LifecycleException;
import org.apache.catalina.connector.Connector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.as.controller.client.ModelControllerClient;
import org.jboss.dmr.ModelNode;
import org.jboss.dmr.ModelType;

import com.synapsense.session.SessionVariableNames;

public class JbossManagementAPI {
    private static final Log Log = LogFactory.getLog(JbossManagementAPI.class);

    public static String JBOSS_WEBSERVICE_NAME = "jboss.web:type=Service,serviceName=jboss.web";

    /* =============Default settings for new http connector */
    final String HTTP_acceptCount = "100";
    final String HTTP_connectionTimeout = "20000";
    final String HTTP_disableUploadTimeout = "true";
    // final Boolean HTTP_emptySessionPath=true;
    final Boolean HTTP_enableLookups = false;
    final String HTTP_maxHttpHeaderSize = "8192";
    final String HTTP_maxThreads = "250";

    /* =============Default settings for new https connector */
    final String HTTPS_maxThreads = "150";
    final String HTTPS_scheme = "https";
    final boolean HTTPS_secure = true;
    final String HTTPS_clientAuth = Boolean.FALSE.toString();
    final String HTTPS_sslProtocol = "TLS";
    final String HTTPS_SSLEnabled = Boolean.TRUE.toString();
    final String HTTPS_strategy = "ms";

    final String HTTPS_compression = "on";
    final String HTTPS_compressionMinSize = "2048";
    final String HTTPS_noCompressionUserAgents = "gozilla, traviata";
    final String HTTPS_compressableMimeType = "text/html,text/xml,text/JavaScript,application/x-JavaScript,application/JavaScript";

    public static final String HTTP_PORT = "HTTP_PORT";
    public static final String HTTPS_PORT = "HTTPS_PORT";
    public static final String HTTP_CONNECTOR_ENABLED = "HTTP_CONNECTOR_ENABLED";
    public static final String HTTPS_CONNECTOR_ENABLED = "HTTPS_CONNECTOR_ENABLED";
    public static final String KEYSTORE_PASSWORD = "KEYSTORE_PASSWORD";
    public static final String KEYSTORE_FILENAME = "KEYSTORE_FILENAME";
    public static final String SMTP_HOST = "SMTP_HOST";
    public static final String SMTP_PORT = "SMTP_PORT";
    public static final String MAIL_FROM = "MAIL_FROM";
    public static final String SMTP_SSL = "SMTP_SSL";
    public static final String SMTP_USERNAME = "SMTP_USERNAME";
    public static final String SMTP_PASSWORD = "SMTP_PASSWORD";
    public static final String HTTP_PROTOCOL = "HTTP/1.1";

    private final static String OUTCOME_SUCCESS = "success";
    private ModelControllerClient client;

    private static class Attribute {
        String name;
        Class<?> type;
        String[] address;

        public Attribute(String name, Class<?> type, String[] address) {
            if (address == null || address.length % 2 == 1) {
                throw new IllegalArgumentException("Length of address must be even");
            }

            this.name = name;
            this.type = type;
            this.address = address;
        }
    }

    private static Map<String, Attribute> attributes = new HashMap<String, Attribute>();

    static {
        attributes.put(HTTP_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
                "standard-sockets", "socket-binding", "http" }));
        attributes.put(HTTPS_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
                "standard-sockets", "socket-binding", "https" }));
        attributes.put(HTTP_CONNECTOR_ENABLED, new Attribute("enabled", Boolean.class, new String[] { "subsystem",
                "web", "connector", "http" }));
        attributes.put(HTTPS_CONNECTOR_ENABLED, new Attribute("enabled", Boolean.class, new String[] { "subsystem",
                "web", "connector", "https" }));
        attributes.put(KEYSTORE_PASSWORD, new Attribute("password", String.class, new String[] { "subsystem", "web",
                "connector", "https", "ssl", "configuration" }));
        attributes.put(KEYSTORE_FILENAME, new Attribute("certificate-key-file", String.class, new String[] {
                "subsystem", "web", "connector", "https", "ssl", "configuration" }));
        attributes.put(SMTP_HOST, new Attribute("host", String.class, new String[] { "socket-binding-group",
                "standard-sockets", "remote-destination-outbound-socket-binding", "mail-smtp" }));
        attributes.put(SMTP_PORT, new Attribute("port", Integer.class, new String[] { "socket-binding-group",
                "standard-sockets", "remote-destination-outbound-socket-binding", "mail-smtp" }));
        attributes.put(MAIL_FROM, new Attribute("from", String.class, new String[] { "subsystem", "mail",
                "mail-session", "java:/Mail" }));
        attributes.put(SMTP_SSL, new Attribute("ssl", Boolean.class, new String[] { "subsystem", "mail",
                "mail-session", "java:/Mail", "server", "smtp" }));
        attributes.put(SMTP_USERNAME, new Attribute("username", String.class, new String[] { "subsystem", "mail",
                "mail-session", "java:/Mail", "server", "smtp" }));
        attributes.put(SMTP_PASSWORD, new Attribute("password", String.class, new String[] { "subsystem", "mail",
                "mail-session", "java:/Mail", "server", "smtp" }));
    }

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {
            JbossManagementAPI util = new JbossManagementAPI(InetAddress.getByName("localhost"), 9999, "admin", "admin");

            util.setAttribute(HTTP_PORT, "8080");

            System.out.println(HTTP_PORT + ": " + util.getAttribute(HTTP_PORT));
            System.out.println(HTTPS_PORT + ": " + util.getAttribute(HTTPS_PORT));
            System.out.println(HTTP_CONNECTOR_ENABLED + ": " + util.getAttribute(HTTP_CONNECTOR_ENABLED));
            System.out.println(HTTPS_CONNECTOR_ENABLED + ": " + util.getAttribute(HTTPS_CONNECTOR_ENABLED));
            System.out.println(KEYSTORE_PASSWORD + ": " + util.getAttribute(KEYSTORE_PASSWORD));
            System.out.println(KEYSTORE_FILENAME + ": " + util.getAttribute(KEYSTORE_FILENAME));
            System.out.println(SMTP_HOST + ": " + util.getAttribute(SMTP_HOST));
            System.out.println(SMTP_PORT + ": " + util.getAttribute(SMTP_PORT));
            System.out.println(MAIL_FROM + ": " + util.getAttribute(MAIL_FROM));
            System.out.println(SMTP_SSL + ": " + util.getAttribute(SMTP_SSL));
            System.out.println(SMTP_USERNAME + ": " + util.getAttribute(SMTP_USERNAME));
            System.out.println(SMTP_PASSWORD + ": " + util.getAttribute(SMTP_PASSWORD));

            // util.reloadServer();

            util.close();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static JbossManagementAPI createFromRequest(final HttpServletRequest request) throws NumberFormatException,
            UnknownHostException {
        HttpSession session = request.getSession(false);
        ServletContext context = session.getServletContext();
        String host = context.getInitParameter("MANAGEMENT_HOST");
        String port = context.getInitParameter("MANAGEMENT_PORT");

        String username = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        String password = (String) session.getAttribute(SessionVariableNames.SESSION_PASSWORD);

        return new JbossManagementAPI(InetAddress.getByName(host), Integer.parseInt(port), username, password);
    }

    public JbossManagementAPI(final InetAddress host, final int port, final String username, final String password) {
        client = createClient(host, port, username, password);
        System.out.println(client);
    }

    public String undefineAttribute(String name) throws IOException {

        Attribute attribute = attributes.get(name);

        if (attribute == null) {
            throw new IllegalArgumentException("Unknown attribute name: " + name);
        }

        ModelNode modelNode = new ModelNode();
        modelNode.get("operation").set("undefine-attribute");
        modelNode.get("name").set(attribute.name);
        setAddress(modelNode, attribute.address);

        ModelNode returnValNode = client.execute(modelNode);
        validateResult(returnValNode);
        return returnValNode.get("result").asString();
    }

    public String getAttribute(String name) throws IOException {

        Attribute attribute = attributes.get(name);

        if (attribute == null) {
            throw new IllegalArgumentException("Unknown attribute name: " + name);
        }

        ModelNode modelNode = new ModelNode();
        modelNode.get("operation").set("read-attribute");
        modelNode.get("name").set(attribute.name);
        setAddress(modelNode, attribute.address);

        ModelNode returnValNodeNode = client.execute(modelNode);
        validateResult(returnValNodeNode);
        ModelNode resultNode = returnValNodeNode.get("result");
        return ModelType.UNDEFINED.equals(resultNode.getType()) ? null : resultNode.asString();
    }

    public void setAttribute(String name, String value) throws IOException {

        Attribute attribute = attributes.get(name);

        if (attribute == null) {
            throw new IllegalArgumentException("Unknown attribute name: " + name);
        }

        ModelNode modelNode = new ModelNode();
        modelNode.get("operation").set("write-attribute");
        modelNode.get("name").set(attribute.name);
        setAddress(modelNode, attribute.address);

        if (Integer.class.equals(attribute.type)) {
            modelNode.get("value").set(Integer.parseInt(value));
        } else if (Boolean.class.equals(attribute.type)) {
            modelNode.get("value").set(Boolean.parseBoolean(value));
        } else if (String.class.equals(attribute.type)) {
            modelNode.get("value").set(value);
        } else {
            throw new UnsupportedOperationException("No handler for " + attribute.type + " class yet");
        }

        ModelNode returnValNode = client.execute(modelNode);
        validateResult(returnValNode);
    }

    public void reloadServer() throws IOException {
        ModelNode op = new ModelNode();
        op.get("operation").set("reload");

        ModelNode returnValNode = client.execute(op);
        validateResult(returnValNode);
    }

    public void setSystemProperty(String name, String value) throws IOException {
        ModelNode modelNode = new ModelNode();
        modelNode.get("operation").set("write-attribute");
        modelNode.get("name").set("value");
        modelNode.get("value").set(value);

        ModelNode address = modelNode.get("address");
        address.add("system-property", name);

        ModelNode returnValNode = client.execute(modelNode);
        validateResult(returnValNode);
    }

    public boolean isPortAvailableForSecuredConnector(int port) throws IOException, MalformedObjectNameException,
            InstanceNotFoundException, ReflectionException, MBeanException {
        boolean result = true;
        String defaultHttpPort = this.getAttribute(JbossManagementAPI.HTTP_PORT);
        
        if(Integer.parseInt(defaultHttpPort) == port){
            result = false; // secured connector can't be set on default http port...
        } else {
            for (Connector c : getAvailableConnectors()) {
                if (c.getPort() == port) {
                    result = false;
                    break;
                }
            }
        }
        return result;
    }

    public void close() throws IOException {
        client.close();
    }

    public static Connector[] getAvailableConnectors() throws MalformedObjectNameException, InstanceNotFoundException,
            ReflectionException, MBeanException {
        final String[] signature = {};
        final ObjectName jbossWeb = new ObjectName(JBOSS_WEBSERVICE_NAME);
        Connector[] list = (Connector[]) ManagementFactory.getPlatformMBeanServer().invoke(jbossWeb, "findConnectors",
                null, signature);
        return list;
    }

    public void reloadConnectors(boolean reloadHttp, boolean reloadHttps) throws IOException,
            MalformedObjectNameException, InstanceNotFoundException, MBeanException, ReflectionException,
            LifecycleException {
        Connector[] list = getAvailableConnectors();

        // remove connectors
        for (Connector c : list) {
            if ((c.getSecure() && reloadHttps) || (!c.getSecure() && reloadHttp)) {
                c.stop();
                try {
                    c.destroy();
                } catch (Exception e) {
                    Log.error(e.getLocalizedMessage(), e);
                }
            }
        }

        // restore from standalone.xml
        try {
            if (reloadHttp && !"false".equalsIgnoreCase(this.getAttribute(HTTP_CONNECTOR_ENABLED))) {
                Connector con = this.createConnector();
                addConnectorToService(con);
            }
        } catch (Exception e) {
            Log.error(e.getLocalizedMessage(), e);
        }

        try {
            if (reloadHttps && !"false".equalsIgnoreCase(this.getAttribute(HTTPS_CONNECTOR_ENABLED))) {
                Connector con = this.createSecureConnector();
                addConnectorToService(con);
            }
        } catch (Exception e) {
            Log.error(e.getLocalizedMessage(), e);
        }
    }

    private void addConnectorToService(Connector connector) throws MalformedObjectNameException,
            InstanceNotFoundException, ReflectionException, MBeanException, LifecycleException {
        final ObjectName jbossWeb = new ObjectName(JBOSS_WEBSERVICE_NAME);
        String[] sig = { Connector.class.getName() };
        Object[] opArgs = { connector };

        ManagementFactory.getPlatformMBeanServer().invoke(jbossWeb, "addConnector", opArgs, sig);
        connector.start();
    }

    private Connector createSecureConnector() {
        Connector con = null;
        try {
            con = new Connector(HTTP_PROTOCOL);
            con.setPort(Integer.parseInt(this.getAttribute(HTTPS_PORT)));
            con.setScheme("https");
            con.setSecure(true);
            con.setProperty("keystoreFile", this.getAttribute(KEYSTORE_FILENAME));
            con.setProperty("keystorePass", this.getAttribute(KEYSTORE_PASSWORD));

            con.setProperty("maxThreads", this.HTTPS_maxThreads);
            con.setProperty("clientAuth", this.HTTPS_clientAuth);
            con.setProperty("sslProtocol", this.HTTPS_sslProtocol);
            con.setProperty("SSLEnabled", this.HTTPS_SSLEnabled);
            con.setProperty("strategy", this.HTTPS_strategy);

            con.setProperty("compression", this.HTTPS_compression);
            con.setProperty("compressionMinSize", this.HTTPS_compressionMinSize);
            con.setProperty("noCompressionUserAgents", this.HTTPS_noCompressionUserAgents);
            con.setProperty("CompressableMimeType", this.HTTPS_compressableMimeType);
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
        }
        return con;
    }

    private Connector createConnector() {
        Connector con = null;
        try {
            con = new Connector(HTTP_PROTOCOL);
            con.setPort(Integer.parseInt(this.getAttribute(HTTP_PORT)));
            con.setScheme("http");
            con.setSecure(false);
            // con.setEmptySessionPath(this.HTTP_emptySessionPath);
            con.setEnableLookups(this.HTTP_enableLookups);
            con.setProperty("acceptCount", this.HTTP_acceptCount);
            con.setProperty("connectionTimeout", this.HTTP_connectionTimeout);
            con.setProperty("disableUploadTimeout", this.HTTP_disableUploadTimeout);
            con.setProperty("maxHttpHeaderSize", this.HTTP_maxHttpHeaderSize);
            con.setProperty("maxThreads", this.HTTP_maxThreads);
        } catch (Exception e) {
            Log.error(e.getMessage(), e);
        }
        return con;
    }

    private void setAddress(ModelNode modelNode, String[] address) {
        ModelNode addressNode = modelNode.get("address");
        for (int i = 0; i < address.length; i = i + 2) {
            addressNode.add(address[i], address[i + 1]);

        }
    }

    private void validateResult(ModelNode modelNode) {
        if (!OUTCOME_SUCCESS.equals(modelNode.get("outcome").asString())) {
            // TODO: define exception
            throw new RuntimeException("Error in Jboss Management API operation:\n" + modelNode.toString());
        }
    }

    private ModelControllerClient createClient(final InetAddress host, final int port, final String username,
            final String password) {

        final CallbackHandler callbackHandler = new CallbackHandler() {

            public void handle(Callback[] callbacks) throws IOException, UnsupportedCallbackException {
                for (Callback current : callbacks) {
                    if (current instanceof NameCallback) {
                        NameCallback ncb = (NameCallback) current;
                        ncb.setName(username);
                    } else if (current instanceof PasswordCallback) {
                        PasswordCallback pcb = (PasswordCallback) current;
                        pcb.setPassword(password.toCharArray());
                    } else if (current instanceof RealmCallback) {
                        RealmCallback rcb = (RealmCallback) current;
                        rcb.setText(rcb.getDefaultText());
                    } else {
                        throw new UnsupportedCallbackException(current);
                    }
                }
            }
        };

        return ModelControllerClient.Factory.create(host, port, callbackHandler);
    }
}
