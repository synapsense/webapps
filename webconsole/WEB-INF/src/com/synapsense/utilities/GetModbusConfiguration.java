package com.synapsense.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.service.Environment;
import com.synapsense.session.SessionVariableNames;

public class GetModbusConfiguration extends BaseHttpServlet {
    private static final long serialVersionUID = 4908182264987953026L;
    private static final Log LOG = LogFactory
            .getLog(GetModbusConfiguration.class);
    private static final String MODBUS_NETWORK_ADDRESS = "0.0.0.0:502";
    private static final Integer MAX_REGISTER_ID = 49999;
    private static final Integer MIN_REGISTER_ID = 40001;
    private static final Integer DEFAULT_SCALE = 2;
    
    private class ModbusColl extends Object {
        private CollectionTO key = null;
        private Collection<TO<?>> innerAr = new ArrayList<TO<?>>();
        
        public ModbusColl(CollectionTO objId, Collection<TO<?>> arr){
            this.key = objId;
            this.innerAr.addAll(arr);
        };
        
        public CollectionTO getKey() {
            return this.key;
        }
        
        public Collection<TO<?>> getValue() {
            return this.innerAr;
        }
    }

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        Document document = null;
        try {
            document = createConfDocument(request);
            String out = "<result><success>true</success>";
            out += "<key>" + saveData(request, document) + "</key>";
            out += "<file_name>modbusgw.xml</file_name></result>";
            sendResponse(response, out);
            
            return;
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        sendResponse(response,"<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>");
    }
    
    @SuppressWarnings("unchecked")
    private String saveData (HttpServletRequest request,Document document) {
        Long key = new Date().getTime();
        request.getSession().setAttribute(SessionVariableNames.getDataVariableName(key.toString()), XMLHelper.getDocumentXml(document));
        return key.toString();
    }

    @SuppressWarnings("unchecked")
    private Document createConfDocument(HttpServletRequest request)
            throws ParserConfigurationException {
        
        String userName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> user = Env.getUserDAO().getUser(userName);
        String scale = UserPreference.get(user, "DecimalFormat", Env.getUserDAO());
        int count = (scale == null || scale.isEmpty()) ? DEFAULT_SCALE : Integer.valueOf(scale);
        scale = (count == 0) ? "1" : String.format("1%0" + count + "d", 0);
        LOG.info("scale = " + scale);
        
        Integer ID = Integer.decode(request.getParameter("MBIDVALUE"));
        String typeName = request.getParameter("TYPEVALUE");
        String propertyName = request.getParameter("PROPERTYVALUE");
        // Static document part
        Document document = createModbusGatewayDoc();
        Element root = document.getDocumentElement();
        Element modbusnetEl = createModbusNetworkElement(document);
        root.appendChild(modbusnetEl);
        Element modbusdeviceEl = createModbusDeviceElement(document, 1);
        modbusnetEl.appendChild(modbusdeviceEl);
        // Dynamic document part
        Environment env = Env.getEnv(request);
        Collection<TO<?>> TOs = env.getObjectsByType(typeName);
        propertyName += ",name";
        
        String[] propsArr = propertyName.split(",");
        Collection<CollectionTO> props = env.getPropertyValue(TOs, propsArr);
        Collection<ModbusColl> innerTOmap = new ArrayList<ModbusColl>();
        Map<TO<?>,String> toNameMap = new HashMap<TO<?>, String>();
        for (CollectionTO property : props) {
            for(String prop : propsArr) {
                Object propertyValue = property.getSinglePropValue(prop)
                        .getValue();
                if("name".equals(prop)) continue; 
                String objectName = (String)property.getSinglePropValue("name").getValue();
                if (propertyValue instanceof TO) {
                    Collection<TO<?>> innerAr = new ArrayList<TO<?>>();
                    innerAr.add((TO<?>) propertyValue);
                    
                    innerTOmap.add(new ModbusColl(property, innerAr));
                    toNameMap.put(property.getObjId(), objectName);
                } else if (propertyValue instanceof Collection) {
                    innerTOmap.add(new ModbusColl(property, (Collection<TO<?>>) propertyValue));
                    toNameMap.put(property.getObjId(), objectName);
                } else {
                    modbusdeviceEl.appendChild(createObjectComment(document, objectName, prop));
                    modbusdeviceEl.appendChild(createRegisterElement(document, ID, scale, property.getObjId().toString(),
                            prop));
                    ID++;
                }
            }
        }
        
        handleInnerTOs(request, modbusdeviceEl, innerTOmap, toNameMap, ID, scale);
        return document;
    }
    
    private void handleInnerTOs(HttpServletRequest request, Element mbdevice,
            Collection<ModbusColl> tosMap, Map<TO<?>,String> toNameMap,int ID, String scale) {
        Document document = mbdevice.getOwnerDocument();
        String propertyNameStr = request.getParameter("PROPERTYVALUE");
        String[] propsArr = propertyNameStr.split(",");
        
        Environment env = Env.getEnv(request);
        for (ModbusColl entry : tosMap) {
            if (entry.getValue().isEmpty()) {
                continue;
            }
            Collection<CollectionTO> properties = env.getPropertyValue(
                    entry.getValue(), new String[] { "lastValue" });
            for (CollectionTO property : properties) {
                if(property.getPropValue("lastValue") == null) continue;
                if (ID > MAX_REGISTER_ID) {
                    mbdevice = createModbusDeviceElement(document,
                            (Integer.decode(mbdevice.getAttribute("id")) + 1));
                    ID = MIN_REGISTER_ID;
                }
                String propertyName = "";
                for(String propName : propsArr) {
                    Object propertyValue = entry.getKey().getSinglePropValue(propName).getValue();
                    if((propertyValue instanceof TO) && property.getObjId().equals((TO<?>)propertyValue)) {
                        propertyName = propName;
                        break;
                    }
                }
                
                mbdevice.appendChild(createObjectComment(document, 
                        toNameMap.get(entry.getKey().getObjId()), propertyName));
                mbdevice.appendChild(createRegisterElement(document, ID, scale,
                        property.getObjId().toString(), "lastValue"));
                ID++;
            }
        }
    }

    private Element createModbusNetworkElement(Document document) {
        Element modbusnetEl = document.createElement("tns:modbusnetwork");
        modbusnetEl.setAttribute("address", MODBUS_NETWORK_ADDRESS);
        return modbusnetEl;
    }

    private Element createModbusDeviceElement(Document document,
            Integer deviceId) {
        Element modbusdeviceEl = document.createElement("tns:modbusdevice");
        modbusdeviceEl.setAttribute("id", deviceId.toString());
        return modbusdeviceEl;
    }

    private Element createRegisterElement(Document document, Integer ID,
            String scale, String to, String property) {
        Element registerEl = document.createElement("tns:register");
        registerEl.setAttribute("id", ID.toString());
        registerEl.setAttribute("scale", scale);
        registerEl.setAttribute("to", to);
        registerEl.setAttribute("property", property);
        return registerEl;
    }

    private Comment createObjectComment(Document document, String to,
            String propertyName) {
        return document.createComment("object name=\"" + to + "\" property=\""
                + propertyName + "\"");
    }

    private Document createModbusGatewayDoc()
            throws ParserConfigurationException {
        Document document = XMLHelper.createNewXmlDocument("tns:modbusgateway");
        Element root = document.getDocumentElement();
        root.setAttribute("xmlns:tns",
                "http://www.synapsense.com/modbusgw/config");
        root.setAttribute("xmlns:xsi",
                "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation",
                "http://www.synapsense.com/modbusgw/config modbusgw.xsd");
        return document;
    }
}