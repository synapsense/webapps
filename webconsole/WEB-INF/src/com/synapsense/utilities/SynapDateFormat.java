package com.synapsense.utilities;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;
import javax.servlet.http.HttpServletRequest;

import com.synapsense.utilities.dataconverters.UsersDataOptions;

public class SynapDateFormat extends SimpleDateFormat{
    private static final long serialVersionUID = -4727323127287698998L;
    public SynapDateFormat(String pattern,String TZ_ID) {
        super(pattern);
        this.setTimeZone(TimeZone.getTimeZone(TZ_ID));
    }
    public SynapDateFormat(String pattern,Locale locale,String TZ_ID) {
        super(pattern,locale);
        this.setTimeZone(TimeZone.getTimeZone(TZ_ID));
    }
    public SynapDateFormat(String pattern,DateFormatSymbols formatSymbols,String TZ_ID) {
        super(pattern,formatSymbols);
        this.setTimeZone(TimeZone.getTimeZone(TZ_ID));
    }
    
    public SynapDateFormat(String pattern,HttpServletRequest req) {
        super(pattern);
        this.setTimeZone(TimeZone.getTimeZone(UsersDataOptions.getTzIdFromRequest(req)));
    }
    public SynapDateFormat(String pattern,Locale locale,HttpServletRequest req) {
        super(pattern,locale);
        this.setTimeZone(TimeZone.getTimeZone(UsersDataOptions.getTzIdFromRequest(req)));
    }
    public SynapDateFormat(String pattern,DateFormatSymbols formatSymbols,HttpServletRequest req) {
        super(pattern,formatSymbols);
        this.setTimeZone(TimeZone.getTimeZone(UsersDataOptions.getTzIdFromRequest(req)));
    }
}