/**
 * 
 */
package com.synapsense.utilities;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.ValueTO;
import com.synapsense.productManagement.ProductManager;
import com.synapsense.service.Environment;
import com.synapsense.session.ApplicationWatch;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.lang.LangManager;

/**
 * @author dinniss
 *
 */
public class CommonFunctions {
    static Log log = LogFactory.getLog(CommonFunctions.class);
    
    private static Set<String> histPropsToExclude = new HashSet<String>(); // historical properties that we don't want to see in general views. See Bug 9162
    
    static {
        histPropsToExclude.add("x");
        histPropsToExclude.add("y");
        histPropsToExclude.add("width");
        histPropsToExclude.add("height");
    }
    
    public static Set<String> getHistPropsToExclude() {
        return Collections.unmodifiableSet(histPropsToExclude);
    }
    
    public static void removeExcludedHistProps (Collection<String> collection) {
        for (String propToExclude : histPropsToExclude) {
            collection.remove(propToExclude);
        }
    }

    public static NumberFormat getNumberFormat(HttpServletRequest request) {
        NumberFormat numberFormatter = NumberFormat.getInstance(Locale.US);
        numberFormatter.setRoundingMode(RoundingMode.HALF_UP);
        numberFormatter.setGroupingUsed(false);

        int decimalDigits = UsersDataOptions.getDecimalDigits(request);
        numberFormatter.setMaximumFractionDigits(decimalDigits);
        numberFormatter.setMinimumFractionDigits(decimalDigits);
        
        if (numberFormatter instanceof DecimalFormat) {
            DecimalFormat decimalFormat = (DecimalFormat) numberFormatter;
            DecimalFormatSymbols decimalFormatSymbols = decimalFormat.getDecimalFormatSymbols();
            decimalFormatSymbols.setDecimalSeparator(UsersDataOptions.getDecimalSeparator(request));
            decimalFormat.setDecimalFormatSymbols(decimalFormatSymbols);
        }
        
        return numberFormatter;
    }

    public static String getFormattedNumber(NumberFormat numberFormatter, Double val) {
        String res = numberFormatter.format(val);
//		if(0.0 == Double.parseDouble(res)) {
//			res = numberFormatter.format(0.0);
//		}
        return res;
    }
    
    /**
     * Alphabetically sorts properties list
     * @param props - list of "String", "ValueTO" or "PropertyDescr"
     * @param typeName
     * @param request
     * @return
     */
    public static <T extends Object> List<T> sortPropsAlphabetically(List<T> props, final String typeName, HttpServletRequest request) {		
        final LangManager lm = LangManager.getInstance();
        final Locale locale = LangManager.getCurrentLocale(request);
        final TypeUtil typeUtil = TypeUtil.getInstance();
        
        Comparator<Object> c = new Comparator<Object>(){
            @Override
            public int compare(Object cto1, Object cto2) {
                if (cto1 instanceof ValueTO) {
                    String name1 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, ((ValueTO)cto1).getPropertyName()));
                    String name2 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, ((ValueTO)cto2).getPropertyName()));				
                    return name1.compareTo(name2);
                } else if (cto1 instanceof PropertyDescr) {
                    String name1 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, ((PropertyDescr)cto1).getName()));
                    String name2 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, ((PropertyDescr)cto2).getName()));				
                    return name1.compareTo(name2);
                } else if (cto1 instanceof String) {
                    String name1 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, (String)cto1));
                    String name2 = lm.getLocaleString(locale, typeUtil.getDisplayablePropertyName(typeName, (String)cto2));				
                    return name1.compareTo(name2);
                } else {
                    return 1;
                }
            }
        };
        
        Collections.sort(props, c);
                
        return props;
    }
    
    public static Date rollCurrentDate(int field, int amount){
        GregorianCalendar gc = new GregorianCalendar();				
        gc.roll(field, amount);		
        return gc.getTime();				
    }
    
    /**
     * Converts string "year-month-day" to date
     * @param dateStr
     * @return 
     */
    public static Date convertStringToDate(String dateStr,String TZ_ID){
        Date result = null;
        try{
            SimpleDateFormat formatter = new SynapDateFormat("yyyy-MM-dd",TZ_ID);
            result = formatter.parse(dateStr);
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }		
        return result;
    }

    /**
     * Converts string to date by format string
     * @param dateStr
     * @param formatString
     * @return 
     */
    public static Date convertStringToDate(String dateStr, String formatString,String TZ_ID){
        Date result = null;
        try{
            SimpleDateFormat formatter = new SynapDateFormat(formatString,TZ_ID);
            result = formatter.parse(dateStr);
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }		
        return result;
    }
    
    /**
     * Checks object for null and returns object's string representation
     * @param o
     * @return 
     */
    static public String convertObjectToString(Object o){
        return o == null ? "" : o.toString();				
    }

    /**
     * Replaces unallowed symbols (for file system) in file name
     * @param fileName
     * @return
     */
    public static String replaceUnallowedSymbolsForFileSystem(String fileName){
        String result = fileName.replaceAll(":", "");
        return result;
    }

    /**
     * Loads initial states of utility functions (for example, static variables).
     * This function called on first environment object retrieving.
     * @param env
     */
    public static void loadInitialStates(Environment env){
        ProductManager.updateProduct();//read product from config
    }
    
    /**
     * Gets standard configuration file
     * 
     * @param context
     * @return
     */
    public static final String getStandardConfigFileName(ServletContext context){
        String confUrl = "conf/uiconfig.xml";
        String configFileName = context.getRealPath("/") + confUrl;
        //String configFileName = ApplicationWatch.getPath() + confUrl;
        
        return configFileName;
    }
    
    /**
     * Gets standard configuration file
     * 
     * @param context
     * @return
     */
    public static final String getStandardConfigFileName(){
        String confUrl = "conf/uiconfig.xml";
        String configFileName = ApplicationWatch.getPath() + confUrl;
        
        return configFileName;
    }
    
    /**
     * Escapes XML string
     * @param str
     * @return
     */
    public static String escapeXML(String str){
        //NOTE: StringEscapeUtils.escapeXml(str) from org.apache.commons.lang.StringEscapeUtils can be used instead
        String escapedStr = str.replaceAll("&", "&amp;").
                                replaceAll("<", "&lt;").
                                replaceAll(">", "&gt;").
                                replaceAll("\"", "&quot;").
                                replaceAll("\'", "&#039;");
        return escapedStr;
    }
}
    
