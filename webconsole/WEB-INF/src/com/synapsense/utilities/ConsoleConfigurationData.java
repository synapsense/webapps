package com.synapsense.utilities;

import java.io.IOException;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;

import org.apache.catalina.connector.Connector;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.EnvironmentTypes;

public class ConsoleConfigurationData {
    private static final Log LOG = LogFactory.getLog(ConsoleConfigurationData.class);
    
    public static final String CONSOLE_CONFIG_DATA_NAME					= "WebConsole";
    public static final String CONSOLE_CONFIG_DATA_URL_PROPERTY_NAME	= "console_url";
    
    private static final String consolePath = "/synapsoft/console.html";
    
    public static void update(Environment env){
        try {
            //create console configuration data if not exists
            TO<?> consoleConfigTO = ConfigurationDataUtils.getConfigurationDataObject(CONSOLE_CONFIG_DATA_NAME, env);
            if(consoleConfigTO == null) {
                createConsoleConfigurationDataObject(env);
            }

            //get console URL from configuration data
            Properties props = ConfigurationDataUtils.getConfigurationProperties(CONSOLE_CONFIG_DATA_NAME, env);
            String storedUrl = props.getProperty(CONSOLE_CONFIG_DATA_URL_PROPERTY_NAME);
            
            //get current console URL
            String consoleURL = getConsoleUrl();
            
            //if current URL != URL from configuration data, update configuration
            if(consoleURL!= null && !consoleURL.equals(storedUrl)){
                props.setProperty(CONSOLE_CONFIG_DATA_URL_PROPERTY_NAME, consoleURL);
                ConfigurationDataUtils.setConfigurationProperties(CONSOLE_CONFIG_DATA_NAME, props, env);
            }
        }
        catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    private static String getConsoleUrl() throws UnknownHostException {
        String consoleURL = null;
        InetAddress addr = InetAddress.getLocalHost();
        Connector consoleConnector = null;
        
        try {
            Connector[] list = JbossManagementAPI.getAvailableConnectors();
            // Pick one of available connectors (if http and https both available, pick http).
            for (Connector c : list){
                if (consoleConnector == null || (consoleConnector != null && consoleConnector.getSecure())){
                    consoleConnector = c;
                }
            }
        } catch (Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
                
        if(consoleConnector != null){
            consoleURL = consoleConnector.getScheme() + "://" + addr.getCanonicalHostName() + ":" + consoleConnector.getPort() + consolePath;
            LOG.info("Console URL: " + consoleURL);
        }else{
            LOG.info("Appropriate connector was not found. Console URL will not be updated.");
        }
        
        return consoleURL;
    }
    
    private static void createConsoleConfigurationDataObject(Environment env) throws EnvException, IOException{
        Properties props = new Properties();
        StringWriter writer = new StringWriter();
        props.store(writer, null);
        
        env.createObject(EnvironmentTypes.TYPE_CONFIGURATION_DATA, new ValueTO[] {
                        new ValueTO("name", CONSOLE_CONFIG_DATA_NAME),
                        new ValueTO("config", "")});
    }
}
