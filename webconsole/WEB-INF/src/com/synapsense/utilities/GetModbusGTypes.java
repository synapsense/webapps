package com.synapsense.utilities;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.queries.DataAnalysisAPI;
import com.synapsense.service.Environment;
import com.synapsense.session.ApplicationWatch;

/**
 * 
 * @author aklushin
 * 
 */
public class GetModbusGTypes extends BaseHttpServlet {
    private static final long serialVersionUID = -6725255528458668352L;
    private static final Log LOG = LogFactory.getLog(DataAnalysisAPI.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost (HttpServletRequest request,HttpServletResponse response) 
            throws IOException, ServletException {
        Document typesDoc = XMLHelper.loadXmlFromFile(ApplicationWatch.getPath() + "conf/types.xml");
        clearTypesDoc(typesDoc, request);
        response.setHeader("Pragma", "no-cache");
        sendResponse(response, typesDoc);
    }
    
    private void clearTypesDoc (Document document, HttpServletRequest request) {
        Element typesEl = document.getDocumentElement();
        Environment env = Env.getEnv(request);
        NodeList typeList = typesEl.getElementsByTagName("type");
        for (int i=0;i<typeList.getLength();i++) {
            Element typeEl = (Element)typeList.item(i);
            String typeName = typeEl.getAttribute("name");
            typeEl.setAttribute("mappedname", TypeUtil.getInstance().getDisplayableTypeName(typeName));
            NodeList propertiesList = typeEl.getElementsByTagName("property");
            for (int j=0;j<propertiesList.getLength();j++) {
                Element propertyEl = (Element)propertiesList.item(j);
                String propertyName = propertyEl.getAttribute("name");
                if ("false".equals(propertyEl.getAttribute("visible")) ||
                        propertyName.isEmpty()) {
                    typeEl.removeChild(propertyEl);
                    j--;
                    continue;
                }				
                try {
                    PropertyDescr pdescr = env.getPropertyDescriptor(typeName, propertyName);
                    propertyEl.setAttribute("mappedname", TypeUtil.getInstance().getDisplayablePropertyName(typeName, propertyName));
                    Class<?> propClass = pdescr.getType();
                    if (Number.class.isAssignableFrom(propClass)) {
                        //do nothing
                    } else if (TO.class.isAssignableFrom(propClass)) {
                        //In case when the subject property is TO, we need to check whether this property is a reference to some object, which has a property "lastValue". 
                        //If there is no such objects found, we hide this property.
                        Collection<TO<?>> objs = env.getObjectsByType(typeName);
                        Collection<CollectionTO> colObjs = env.getPropertyValue(objs, new String[] { propertyName });
                        for(CollectionTO obj : colObjs) {
                            ValueTO vto = obj.getPropValue(propertyName).get(0);
                            TO<?> to = (TO<?>) vto.getValue();
                            if(to == null) continue;
                            env.getPropertyValue(to, "lastValue", Double.class);
                            break;
                        }
                    } else {
                        typeEl.removeChild(propertyEl);
                        j--;
                    }
                } catch (PropertyNotFoundException e) {
                    typeEl.removeChild(propertyEl);
                    j--;
                    LOG.info(e.getLocalizedMessage());
                } catch (ObjectNotFoundException e) {
                    typesEl.removeChild(typeEl);
                    i--;
                    LOG.info(e.getLocalizedMessage());
                    break;
                }catch (UnableToConvertPropertyException e) {
                    typesEl.removeChild(typeEl);
                    i--;
                    LOG.info(e.getLocalizedMessage());
                    break;
                }
            }
            if (propertiesList.getLength()==0) {
                typesEl.removeChild(typeEl);
                i--;
                continue;
            }
        }
    }
}