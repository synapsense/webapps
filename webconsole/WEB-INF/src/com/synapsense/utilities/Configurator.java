package com.synapsense.utilities;

import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.commontabular.TabularUtils;
import com.synapsense.dashboard.DashboardUtils;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.EnvironmentTypes;

/**
 * This class checks existence of certain objects in the model. It creates "CONFIGURATION_DATA" object for PowerImaging
 * configuration as well as default Custom Data Views Templates. In the future it can be extended to perform other
 * system initialization tasks.
 * 
 * This class may become a "singleton" in the future, but "static" methods are used now for simplicity and performance.
 * 
 * @author aklushin
 * @author ahorosh
 */
public class Configurator {
    private static final Log LOG = LogFactory.getLog(Configurator.class);

    private static final String CONFIGURATION_DATA_DEFAULT_LEGENDS = "25,0x009E0F;50,0x00FFFF;75,0xFFFF00;100,0xFF9900;Above Max Threshold,0xCF2A27|"
            + "20,0xFF9900;40,0xFFFF00;80,0x00FFFF;100,0x009E0F;Above Max Threshold,0xCF2A27";
    private static final String CONFIGURATION_DATA_DEFAULT_NAME = "PowerImagingLegend";
    private static int CONFIGURATION_DATA_NUM = 1;
    
    public static synchronized void updateConfiguration(HttpServletRequest request) throws ServletException {
        Environment env = Env.getEnv(request);
        TO<?> root = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT).iterator().next();
        createConfigurationData(Env.getLocalEnv(request), root);
        createCustomDataViews(request);
        createDashboardType(request);
        createDashboardTemplates(request);
    }

    private static void createConfigurationData(Environment env, TO<?> root) throws ServletException {
        if (env.getObjectsByType(EnvironmentTypes.TYPE_POWER_RACK).isEmpty()) {
            // Objects of the CONFIGURATION_DATA type are used only by condition - POWER_RACK objects are existing in
            // system
            // And more console will not allow any users to configure "Power Imaging".
            // @see EscFormSetup servlet.
            return;
        }
        ValueTO[] vals = { new ValueTO("name", CONFIGURATION_DATA_DEFAULT_NAME) };
        Collection<TO<?>> confDataTOs = env.getObjects(EnvironmentTypes.TYPE_CONFIGURATION_DATA, vals);
        if (!confDataTOs.isEmpty() && confDataTOs.size() != CONFIGURATION_DATA_NUM) {
            throw new ServletException("Invalid number of " + CONFIGURATION_DATA_DEFAULT_NAME + " objects.");
        } else if (confDataTOs.isEmpty()) {
            try {
                TO<?> to = env.createObject(EnvironmentTypes.TYPE_CONFIGURATION_DATA, new ValueTO[] {
                        new ValueTO("name", CONFIGURATION_DATA_DEFAULT_NAME),
                        new ValueTO("config", CONFIGURATION_DATA_DEFAULT_LEGENDS) });
                env.setRelation(root, to);
            } catch (EnvException e) {
                throw new ServletException("Unable to complete: ", e);
            }
        }
    }

    private static void createCustomDataViews(HttpServletRequest request) throws ServletException {
        Environment env = Env.getEnv(request);
        ObjectType[] templateType = env.getObjectTypes(new String[] { EnvironmentTypes.TYPE_TABULAR_TEMPLATE });
        if (templateType == null || templateType.length < 1) {
            try {
                TabularUtils.createDefaultTemplates(request);
            } catch (EnvException e) {
                LOG.error(e.getLocalizedMessage(), e);
                throw new ServletException("Unable to complete: ", e);
            }
        }
    }
    
    private static void createDashboardType(HttpServletRequest request) {
        try {
            DashboardUtils.createDashboardTypeIfNotExist(request);
            DashboardUtils.createDashboardTemplateTypeIfNotExist(request);
            DashboardUtils.createDashboardCarouselTypeIfNotExist(request);
        } catch (EnvException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    private static void createDashboardTemplates(HttpServletRequest request) {
        try {
            DashboardUtils.createDashboardTemplateTypeIfNotExist(request);
            DashboardUtils.createDashboardTemplatesIfNotExists(request);
        } catch (EnvException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
}
