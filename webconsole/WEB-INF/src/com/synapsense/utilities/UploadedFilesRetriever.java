package com.synapsense.utilities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class UploadedFilesRetriever {
    private static final Log LOG = LogFactory.getLog(UploadedFilesRetriever.class);
    
    public static List<byte[]> getFiles(HttpServletRequest request, HttpServletResponse response) throws IOException{
        List<byte[]> filesArray = new ArrayList<byte[]>();
        
        String fileContent = request.getParameter("fileContent");
        
        if (fileContent == null) {
            try {
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    // Create a factory for disk-based file items
                    FileItemFactory factory = new DiskFileItemFactory();

                    // Create a new file upload handler
                    ServletFileUpload upload = new ServletFileUpload(factory);

                    // Parse the request
                    @SuppressWarnings("unchecked")
                    List<FileItem> items = (List<FileItem>) upload.parseRequest(request);

                    Iterator<FileItem> iter = items.iterator();

                    while (iter.hasNext()) {
                        FileItem item = (FileItem) iter.next();
                        String fileName = item.getName();
                        String contentType = item.getContentType();
                        long sizeInBytes = item.getSize();

                        if (fileName != null && !fileName.equals("")
                                && sizeInBytes / 1024 != 0) {
                            LOG.info("File name :" + " " + fileName + "  "
                                    + "Content Type :" + contentType + " "
                                    + "Size in KB :" + sizeInBytes / 1024);
                            byte[] array = item.get();
                            filesArray.add(array);
                            LOG.info("Upload of file " + fileName + "was completed successfully");
                        }
                    }
                }
            } catch (FileUploadException e) {
                filesArray = null;
                LOG.error(e.getMessage(), e);
            }
        } else {
            try {
                String[] byteStrings = fileContent.split(",");
                byte[] data = new byte[byteStrings.length];

                for (int i = 0; i < byteStrings.length; i++) {
                    data[i] = Byte.parseByte(byteStrings[i]);
                }
                filesArray.add(data);
                LOG.info("File uploaded via doRequest successfully.");
            } catch (Exception e) {
                filesArray = null;
                LOG.error(e.getMessage(), e);
            }
            String result = filesArray == null ? "true" : "false";
            response.setContentType("text/xml; charset=utf-8");            
            response.getWriter().print("<success>" + result + "</success>");
        }		
        
        return filesArray;
    }
}
