package com.synapsense.utilities.activityLog;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import com.synapsense.dataAccess.Env;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.es.ActivityLogConstans;

public class ESConfiguratorActivityLog {

    public static void logRecord(HttpServletRequest request, String cn, String mn, String mess)
            throws ServletException {
    
        try {
            String user = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            Env.getActivityLogServ().addRecord(
                    new ActivityRecord(user, mn,
                            ActivityLogConstans.ACTION_UPDATE_CONFIGURATION,
                            LocalizationUtils.getLocalizedString(mess)));
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }
    }

}
