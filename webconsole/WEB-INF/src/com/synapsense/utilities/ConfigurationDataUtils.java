package com.synapsense.utilities;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Collection;
import java.util.Properties;

import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.es.EnvironmentTypes;

public class ConfigurationDataUtils {
    
    public static Properties getConfigurationProperties(String confDataName, Environment env) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, IOException{
        Properties props = new Properties();
        TO<?> configDataTO = getConfigurationDataObject(confDataName, env);
        if(configDataTO != null){
            String configStr = env.getPropertyValue(configDataTO, "config", String.class);
            if(configStr != null && !configStr.isEmpty()){
                props.load(new StringReader(configStr));
            }
        }		
        return props;
    }
    
    public static void setConfigurationProperties(String confDataName, Properties props, Environment env) throws IOException, ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException{
        TO<?> configDataTO = getConfigurationDataObject(confDataName, env);
        if(configDataTO != null){
            StringWriter writer = new StringWriter();
            props.store(writer, null);
            env.setPropertyValue(configDataTO, "config", writer.toString());
        }
    }
    
    public static TO<?> getConfigurationDataObject(String confDataName, Environment env){
        TO<?> configDataTO = null;
        ValueTO [] vals = {new ValueTO("name", confDataName)};            
        Collection<TO<?>> ctos = env.getObjects(EnvironmentTypes.TYPE_CONFIGURATION_DATA,  vals);
        if(!ctos.isEmpty()){
            configDataTO = ctos.iterator().next();
        }
        return configDataTO;
    }
}
