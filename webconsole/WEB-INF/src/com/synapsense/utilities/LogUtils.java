package com.synapsense.utilities;

import org.apache.commons.logging.Log;

public class LogUtils {
    
    /**
     * Prints stack trace if log level is DEBUG
     */
    static public void printStackTrace(Log logInstance, Exception e){		
        if(logInstance.isDebugEnabled()){
            Throwable newEx = e;
            while(newEx != null){
                logInstance.debug("Caused by: ", e);
                newEx = newEx.getCause();
            }	
        }		
    }
}
