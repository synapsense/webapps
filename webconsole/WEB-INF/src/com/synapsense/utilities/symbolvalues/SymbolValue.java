package com.synapsense.utilities.symbolvalues;

public class SymbolValue {
    public double value;
    public String label;
    
    public SymbolValue(String label, double value){
        this.label = label;
        this.value = value;
    }
}
