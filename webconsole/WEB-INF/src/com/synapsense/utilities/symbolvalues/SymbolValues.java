package com.synapsense.utilities.symbolvalues;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.utilities.lang.LangManager;


public class SymbolValues {
    private ArrayList<SymbolValue> symbolValuesArray = new ArrayList<SymbolValue>();
    
    public void addSymbolValue(SymbolValue value){
        symbolValuesArray.add(value);
    }
    
    public void clearSymbolValues(){
        symbolValuesArray.clear();
    }
    
    public ArrayList<SymbolValue> getSymbolValuesArray(){
        return symbolValuesArray;
    }
    
    public String packToStr(HttpServletRequest request){
        String result = null;
        String labels = "";
        String values = "";
        
        LangManager langMan = LangManager.getInstance();
        String langID = "";
        if(request != null){
            langID = LangManager.getCurrentLocaleId(request);
        }        
        
        if(!symbolValuesArray.isEmpty()){
            String label = symbolValuesArray.get(0).label;
            label = langMan == null ? label : langMan.getLocaleString(langID, label);
            
            labels += label;
            values += symbolValuesArray.get(0).value;
            
            for(int i = 1; i < symbolValuesArray.size(); i++){
                label = symbolValuesArray.get(i).label;
                label = langMan == null ? label : langMan.getLocaleString(langID, label);
                
                labels += "," + label;
                values += "," + symbolValuesArray.get(i).value;
            }
            
            result = labels + ";" + values;
        }
        
        return result;
    }
}
