package com.synapsense.utilities;

import java.io.IOException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;

import com.synapsense.dataAccess.Env;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.elementmodel.basic.xmldumper.XmlElementDumper;
import com.synapsense.elementmodel.es.EnvContext;
import com.synapsense.elementmodel.es.XmlWrapperElementWriter;
import com.synapsense.utilities.dataconverters.UsersDataOptions;

/**
 * Class provide base functionality to send a response to the user.
 * Methods of "sendResponse" set should be used to send a response to the user.
 * As rule these methods are last in Servlet.doPost invocation. 
 * @author aklushin
 * */
public class BaseHttpServlet extends HttpServlet {
    private static final long serialVersionUID = -4952014246486758061L;

    public void prepareResponse(HttpServletResponse response) {
        response.setContentType("text/xml; charset=utf-8");
    }

    public void sendResponse(HttpServletResponse response, Document document)
            throws IOException, ServletException {
        if (document != null) {
            prepareResponse(response);
            try{
                XMLHelper.writeXml(document, response.getWriter());
            }catch(TransformerException e){
                throw new ServletException("Result of servlet invocation can't be converted from Xml to string", e);
            }
        } else {
            throw new ServletException("Result of servlet invocation is null.");
        }
    }
    
    public void sendResponse(HttpServletRequest req, HttpServletResponse response, ModelElement modelElement)
            throws IOException, ServletException {
        if (modelElement != null) {
            prepareResponse(response);
            
            SimpleDateFormat formatter = new SynapDateFormat(UsersDataOptions.getDateFormat(req), UsersDataOptions.getTzIdFromRequest(req));
            EnvContext envCtx = new EnvContext(Env.getEnv(req), formatter, req);
            
            new XmlElementDumper(new XmlWrapperElementWriter(envCtx)).dump(modelElement, response.getWriter());
            
        } else {
            throw new ServletException("Result of servlet invocation is null.");
        }
    }

    public void sendResponse(HttpServletResponse response, String str)
            throws IOException, ServletException {
        if (str != null) {
            prepareResponse(response);
            response.getWriter().print(str);
        }
    }

    public void sendResponse(HttpServletResponse response, byte[] bytes)
            throws IOException, ServletException {
        prepareResponse(response);
        response.getOutputStream().write(bytes);
    }
}