package com.synapsense.utilities.es;

import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;


public class PropertyId {
    private String objectId;
    private String propName;
    private TO<?> to;
    
    public PropertyId(){
        objectId = null;
        propName = null;
    }

    public PropertyId(String objectId, String propName) {
        super();
        this.objectId = objectId;
        this.propName = propName;
    }

    public String getObjectId() {
        return objectId;
    }

    public TO<?> getObjectTO() {
        if (to == null && objectId != null) {
            to = TOFactory.getInstance().loadTO(objectId);
        }
        return to;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getPropName() {
        return propName;
    }

    public void setPropName(String propName) {
        this.propName = propName;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        if (!(obj instanceof PropertyId)) {
            return false;
        }
        PropertyId propId = (PropertyId) obj;
        
        if (this.propName == null) {
            if(propId.propName != null) {
                return false;
            }

            if (this.objectId == null) {
                if (propId.objectId != null) {
                    return false;
                }
            }
            
            return true;
        }
        
        return this.propName.equals(propId.propName) && this.objectId.equals(propId.objectId);
    }

    @Override
    public String toString(){
        return objectId + "->" + propName;
    }

    public static String toString(PropertyId[] propertyIds){
        StringBuffer result = new StringBuffer();
        for (PropertyId propertyId : propertyIds) {
            result.append(propertyId.toString()).append(",");
        }
        
        return result.substring(0, result.length() - 1);
    }

    static public PropertyId fromString(String propIdStr){
        String[] tokens = propIdStr.split("->");
        PropertyId result = new PropertyId();
        result.objectId = tokens[0];
        result.propName = tokens[1];
        return result;
    }

    static public PropertyId[] arrayFromString(String propIdStr){
        String[] ids = propIdStr.split(",");
        PropertyId[] result = new PropertyId[ids.length];
        
        for (int i = 0; i < ids.length; i++) {
            result[i] = PropertyId.fromString(ids[i]);
        }
        return result;
    }

}
