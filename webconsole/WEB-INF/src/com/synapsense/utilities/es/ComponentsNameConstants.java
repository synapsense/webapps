package com.synapsense.utilities.es;

public class ComponentsNameConstants {
    private ComponentsNameConstants() {}
    
    public static final String COMPONENT_ACTIVE_CONTROL				= "ActiveControl(tm)";
    public static final String COMPONENT_EMAIL						= "Email";
    public static final String COMPONENT_AUDITORS					= "Auditors";
    public static final String COMPONENT_SECURITY					= "Security";
    public static final String COMPONENT_SNMP						= "SNMP";
    public static final String COMPONENT_LIVEIMAGING				= "LiveImaging(tm)";
    public static final String COMPONENT_POEWRIMAGING				= "PowerImaging(tm)";
    public static final String COMPONENT_ALERT_MANAGEMENT			= "Alerts";
}
