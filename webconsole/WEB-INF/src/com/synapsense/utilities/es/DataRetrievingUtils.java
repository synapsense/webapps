package com.synapsense.utilities.es;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.config.types.Property;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.unitconverter.Dimension;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.lang.LangManager;
import com.synapsense.utilities.matcher.ConsoleMatcher;

public class DataRetrievingUtils {		
    
    static Log log = LogFactory.getLog(DataRetrievingUtils.class);
    /** class IDs of sensors that will not be shown*/
    private static HashSet<String> excludedClassIds = null;
    private static HashMap<String, ArrayList<String>> historicalProps = new HashMap<String, ArrayList<String>>();
    
    /**
     * Create xml-document by collection of object with sensors which are children.
     * @param objects - collection of objects
     * @param sensorProp - sensor properties to show
     * @param env - environment
     * @return
     * @throws ObjectNotFoundException
     * @throws ParserConfigurationException
     * @throws NamingException 
     */
    public static Document createDocWithChildSensors(Collection<TO<?>> objects, String [] objectProp, String [] sensorProp, HttpServletRequest request) throws ObjectNotFoundException, ParserConfigurationException, NamingException {
        Document objectsDocRoot = null;
        Element objectsParentDocRoot = null;
        
        if(objects == null || objects.size() == 0){
            return null;
        }
        
        Environment env = Env.getEnv(request);
        SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),UsersDataOptions.getTzIdFromRequest(request));
        String typeName = objects.iterator().next().getTypeName();
        
        objectsDocRoot = XMLHelper.createNewXmlDocument("children");
        objectsParentDocRoot = objectsDocRoot.getDocumentElement();
        
        
        Collection<CollectionTO> objectValuesArray = null;
        if(objectProp == null) {
            objectValuesArray = env.getAllPropertiesValues(objects);
        } else {
            //add "status" to the properties to calculate node display priority
            String [] propsWithStatus = addPropertyNameToExistingArray(objectProp, "status");
            propsWithStatus = addPropertyNameToExistingArray(propsWithStatus, "name"); //add name for filter
            objectValuesArray = env.getPropertyValue(objects, propsWithStatus);
        }
        
        //Filter objects by name
        filterObjectsByName(objectValuesArray, objects, request);		
        //Now working with filtered objects
        
        if(objects.isEmpty()){
            return objectsDocRoot;
        }
        
        //get hashMap "object TO - collection of child sensors TO"
        HashMap< TO<?>, Collection<TO<?>> > objSensorsMap = getObjectChildSensorsMap(objects, env);
        
        //get all needed sensors
        String [] propsWithType = addPropertyNameToExistingArray(sensorProp, "channel"); //add channel for sorting				
        HashMap <TO<?>, CollectionTO> sensorValuesMap = getSensorsMap(objSensorsMap, propsWithType, env);
        
        //add calculated display priority to make sorting base on this value
        addDispPriority(objectValuesArray);
        
        //sort objects
        List<CollectionTO> sortedObjects = CollectionTOSorter.sortObjects(objectValuesArray);
        //end sorting
        
        //run through objects properties and create document
        //for(CollectionTO p : objectValuesArray){
        for(CollectionTO p : sortedObjects){
            //add object 
            TO<?> currentObject = p.getObjId();
            
            Element newelem = objectsDocRoot.createElement(typeName);
            objectsParentDocRoot.appendChild((Node)newelem);
            
            EnvXMLHelper.writeIDs(currentObject, newelem);						
            
            //add object properties
            for(ValueTO pVal : p.getPropValues()) {
                appendPropValueToNode(currentObject, pVal, newelem, objectsDocRoot, request, sdf);
            }
            
            //add object sensors
            Element sensorsElem = objectsDocRoot.createElement("children");
            newelem.appendChild((Node)sensorsElem);
                        
            Collection<TO<?>> currObjSensors = getHashMapValueByTOkey(objSensorsMap, currentObject);
            
            List<CollectionTO> colTos = new LinkedList<CollectionTO> ();
            for (TO<?> sensor : currObjSensors) {
                List<ValueTO> sensorValueArr = getHashMapValueByTOkey(sensorValuesMap, sensor).getPropValues();
                colTos.add(new CollectionTO(sensor, sensorValueArr));
            }
            colTos = CollectionTOSorter.sortObjects(colTos);
            
            for(CollectionTO colTo : colTos){
                TO<?> sensor = colTo.getObjId();
                List<ValueTO> sensorValueArr = colTo.getPropValues();
                
                //skip excluded class IDs				
                ValueTO dataclass = colTo.getSinglePropValue("dataclass");
                if(dataclass != null
                    && isClassIdExcluded((dataclass.getValue()).toString(), request.getSession().getServletContext())){
                    continue;					
                }								
                                
                //add sensor to xml, since its class id is not excluded
                Element propertyElem = objectsDocRoot.createElement("WSNSENSOR");
                sensorsElem.appendChild((Node)propertyElem);
                
                //add sensor props
                for(ValueTO curP : sensorValueArr){
                    appendPropValueToNode(sensor, curP, propertyElem, objectsDocRoot, request, sdf);												
                }											
                
            }									
        }
        
        return objectsDocRoot;
    }
    
    /**
     * Removes objects from <code>objectValuesArray</code> and <code>objects</code>
     * if its names don't match filter criteria that is pointed in <code>request</code>
     * @param objectValuesArray
     * @param objects
     * @param request
     */
    private static void filterObjectsByName(Collection<CollectionTO> objectValuesArray, Collection<TO<?>> objects, HttpServletRequest request){
        String nameFilterPattern = request.getParameter("nameFilterPattern");      
        if(nameFilterPattern != null && !nameFilterPattern.isEmpty()){
            Collection<CollectionTO> filteredCollection = new ArrayList<CollectionTO>();
            for(CollectionTO c : objectValuesArray){                
                ValueTO v = null;;
                List<ValueTO> colVto = c.getPropValue("name");
                if (colVto != null && !colVto.isEmpty()) {
                    v = colVto.get(0);
                }
                if(v != null){
                    String currentObjectName = (String)v.getValue();
                    if(ConsoleMatcher.patternMatches(currentObjectName, nameFilterPattern)){
                        filteredCollection.add(c);
                    }
                }
            }
            //objectValuesArray = filteredCollection;
            objectValuesArray.clear();
            objectValuesArray.addAll(filteredCollection);
            objects.clear();
            for(CollectionTO c : objectValuesArray){
                objects.add(c.getObjId());
            }
        }
    }
    
    /**
     * Add property name to the <code>propNames</code>
     * @param propNames - array of property names
     * @param propertyNameForAddition - property name that will be added
     * @return
     */
    public static String[] addPropertyNameToExistingArray(String[] propNames, String propertyNameForAddition){
        int propLength = (propNames == null) ? 0 : propNames.length;
        
        HashSet<String> props = new HashSet<String>(propLength + 1);
        for(int i = 0; i < propLength; i++){
            props.add(propNames[i]);
            if (propNames[i].equals(propertyNameForAddition)) {
                return propNames;
            }
        }
        props.add(propertyNameForAddition);				
        
        return props.toArray(new String[0]);
    }
    
    /**
     * Temporary function to work with object TO and hashMap until "equals" for TO will not be implemented  
     * @param <T>
     * @param map - hashMap
     * @param key - TO<?> key
     * @return hashMap value
     */
    public static <T> T getHashMapValueByTOkey(HashMap<TO<?>, T> map, TO<?> key){
        T result = map.get(key);
        
        //if "equals" doesn't work, search by hands
        if(result == null){
            Set<TO<?>> keyS = map.keySet();
            Iterator<TO<?>> it = keyS.iterator();
            
            while(it.hasNext()){
                TO<?> checkingObj = it.next(); // current checking object
                
                int sId = (Integer)key.getID(); String sType = key.getTypeName();
                int cSId = (Integer)checkingObj.getID(); String cType = checkingObj.getTypeName();
                if(sId == cSId && sType.equalsIgnoreCase(cType)){
                    result = map.get(checkingObj);
                    break;
                }
            }
        }
        //end hands search
        
        return result;
    }
    
    /**
     * Gets Map of "objectTO - its children collection"
     * @param objects - objects collection
     * @param childTypeName - type name to get children
     * @param env
     * @return
     * @throws ObjectNotFoundException
     */
    public static HashMap< TO<?>, Collection<TO<?>> > getObjectChildrenMap(Collection<TO<?>> objects, String childTypeName, Environment env) throws ObjectNotFoundException{
        HashMap< TO<?>, Collection<TO<?>> > objectChildrenMap = new HashMap<TO<?>, Collection<TO<?>>>();
        
        Collection<CollectionTO> allObjectsChildren = env.getChildren(objects, childTypeName);		
        Collection<TO<?>> children = null;
        
        log.debug("Found " + allObjectsChildren.size() + " children of type '" + childTypeName + "'");
        
        //Retrieve children from CollectionTO
        for(CollectionTO currArray : allObjectsChildren){
            
            List<ValueTO> propValues = currArray.getPropValues();						
            
            children = new ArrayList<TO<?>> ();			
            for(ValueTO p : propValues){
                children.add((TO<?>)p.getValue());
            }
            
            objectChildrenMap.put(currArray.getObjId(), children);
        }
        
        return objectChildrenMap;
    }
    
    /**
     * Gets children properties of Map "objectTO - its children collection"
     * @param objectChildrenMap
     * @param childProp
     * @param env
     * @return
     */
    public static HashMap <TO<?>, CollectionTO> getObjectsMap(HashMap< TO<?>, Collection<TO<?>> > objectChildrenMap, String [] childProp, Environment env){
        if(objectChildrenMap.isEmpty())
            return new HashMap<TO<?>, CollectionTO>();
        //sensors of all objects
        ArrayList<TO<?>> allChildren = new ArrayList<TO<?>>();
        
        //gather sensors
        Set<TO<?>> keys = objectChildrenMap.keySet();
        Iterator<TO<?>> it = keys.iterator();		
        while(it.hasNext()){						
            allChildren.addAll(objectChildrenMap.get(it.next()));
        }				
        
        Collection<CollectionTO> childValuesArray = null;
        
        //convertToHashMap method treats null argument in the right way
        if(!allChildren.isEmpty()) {
            //get properties of all sensors
            if(childProp == null){
                childValuesArray = env.getAllPropertiesValues(allChildren);
            }else{			
                childValuesArray = env.getPropertyValue(allChildren, childProp);
            }				
        }
        
        //convert sensors to hashMap
        HashMap <TO<?>, CollectionTO> childValuesMap = convertToHashMap(childValuesArray);				
        
        return childValuesMap;
    }
    
    
    /**
     * Gets Map of "objectTO - its sensors collection"
     * @param objects
     * @param env
     * @return
     * @throws ObjectNotFoundException
     */
    public static HashMap< TO<?>, Collection<TO<?>> > getObjectChildSensorsMap(Collection<TO<?>> objects, Environment env) throws ObjectNotFoundException{
        HashMap< TO<?>, Collection<TO<?>> > objectSensorsMap = new HashMap<TO<?>, Collection<TO<?>>>();
        
        Collection<CollectionTO> allObjectsSensors = env.getChildren(objects, EnvironmentTypes.TYPE_SENSOR);		
        Collection<TO<?>> sensors = null;
        
        //Retrieve sensors from CollectionTO
        for(CollectionTO currArray : allObjectsSensors){
            
            List<ValueTO> propValues = currArray.getPropValues();						
            
            sensors = new ArrayList<TO<?>> ();			
            for(ValueTO p : propValues){
                sensors.add((TO<?>)p.getValue());
            }
            
            objectSensorsMap.put(currArray.getObjId(), sensors);
        }
        
        return objectSensorsMap;
    }
    
    /**
     * Gets sensor properties of Map "objectTO - its sensors collection"
     * @param objectSensorsMap
     * @param sensorProp
     * @param env
     * @return
     */
    public static HashMap <TO<?>, CollectionTO> getSensorsMap(HashMap< TO<?>, Collection<TO<?>> > objectSensorsMap, String [] sensorProp, Environment env){
        //sensors of all objects
        ArrayList<TO<?>> allSensors = new ArrayList<TO<?>>();
        
        //gather sensors
        Set<TO<?>> keys = objectSensorsMap.keySet();
        Iterator<TO<?>> it = keys.iterator();		
        while(it.hasNext()){						
            allSensors.addAll(objectSensorsMap.get(it.next()));
        }				
        
        Collection<CollectionTO> sensorValuesArray = null;
        
        //get properties of all sensors
        if(sensorProp == null){
            sensorValuesArray = env.getAllPropertiesValues(allSensors);
        }else{
            sensorValuesArray = env.getPropertyValue(allSensors, sensorProp);
        }				
        
        //convert sensors to hashMap
        HashMap <TO<?>, CollectionTO> sensorValuesMap = convertToHashMap(sensorValuesArray);				
        
        return sensorValuesMap;
    }
    
    
    /**
     * Create xml-document by collection of object with sensors which are link properties.
     * @param objects
     * @param sensorProp - sensor properties to show
     * @param env - environment
     * @return
     * @throws ObjectNotFoundException
     * @throws ParserConfigurationException
     * @throws NamingException 
     */
    public static Document createDocWithLinkedSensors(Collection<TO<?>> objects, String [] objProp, String [] sensorProp, HttpServletRequest request) throws ObjectNotFoundException, ParserConfigurationException, NamingException {
        Document objectsDocRoot = null;
        Element objectsParentDocRoot = null;
                
        if(objects == null || objects.size() == 0){
            return null;
        }
        
        Environment env = Env.getEnv(request);
        
        SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),UsersDataOptions.getTzIdFromRequest(request));
        
        String typeName = objects.iterator().next().getTypeName();
        
        //temp patch: for ion_delta and ion_wye use same name for xml-nodes
        if(EnvironmentTypes.TYPE_ION_WYE.equals(typeName)){
            typeName = EnvironmentTypes.TYPE_ION_DELTA;
        }
        
        objectsDocRoot = XMLHelper.createNewXmlDocument(typeName + "_units");
        objectsParentDocRoot = objectsDocRoot.getDocumentElement();
        
        Collection<CollectionTO> objectValuesArray = null;
        if(objProp == null) {
            objectValuesArray = env.getAllPropertiesValues(objects);			
        } else {
            objectValuesArray = env.getPropertyValue(objects, objProp);
        }

        //get all needed sensors
        HashMap <TO<?>, CollectionTO> sensorValuesMap = getSensorMapByCollection(objectValuesArray, sensorProp, env);
        
        //add calculated display priority to make sorting base on this value
        addDispPriority(objectValuesArray);
        
        //sort objects
        List<CollectionTO> sortedObjects = CollectionTOSorter.sortObjects(objectValuesArray);
        //end sorting
        
        //run through objects properties and create document
        //for(CollectionTO p : objectValuesArray){
        for(CollectionTO p : sortedObjects){			
            //add object 
            TO<?> currentObject = p.getObjId();
            
            Element newelem = objectsDocRoot.createElement(typeName);
            objectsParentDocRoot.appendChild((Node)newelem);
            
            EnvXMLHelper.writeIDs(currentObject, newelem);
            
            //add "SENSORs" section
            Element sensorsElem = objectsDocRoot.createElement("SENSORS");
            newelem.appendChild((Node)sensorsElem);
            
            //add object properties
            for(ValueTO pVal : p.getPropValues()){
                
                //if we have sensor, add it to the "SENSORS" section
                if (pVal.getValue() instanceof TO<?>){
                    TO<?> sensor = (TO<?>)pVal.getValue(); 												
                    
                    List<ValueTO> sensorValueArr = getHashMapValueByTOkey(sensorValuesMap, sensor).getPropValues();										
                    
                    String propName = pVal.getPropertyName();
                    
                    Element propertyElem = objectsDocRoot.createElement(propName);
                    sensorsElem.appendChild((Node)propertyElem);
                    
                    Document descrDoc = getPropertyDescrDoc(currentObject, propName, request);
                    if(descrDoc != null){
                        propertyElem.appendChild(objectsDocRoot.importNode((Node)descrDoc.getDocumentElement(), true));
                    }
                    
                    for(ValueTO curP : sensorValueArr){
                        appendPropValueToNode(sensor, curP, propertyElem, objectsDocRoot, request, sdf);
                    }
                }else{//if we have simple property																
                    String valueStr = EnvXMLHelper.convertPropertyValueToStr(pVal, LangManager.getInstance().getLocaleString(LangManager.getCurrentLocale(request), "text/n_a"));
                    Element node = XMLHelper.createXmlNodeWithValue(objectsDocRoot, pVal.getPropertyName(), valueStr);
                    
                    //only doubles can have units
                    if (pVal.getValue() instanceof Double) {
                        String units = Env.getUnits(currentObject, pVal.getPropertyName(), request);
                        if (units != null && !"".equals(units)) {
                            node.setAttribute("u", units);
                        }
                    }

                    newelem.appendChild(node);
                }				
            }
        }
        
        return objectsDocRoot;
    }
    
    /**
     * Convert Collection of CollectionTO to HasMap 
     * @param objectValuesArray
     * @param sensorProp
     * @param env
     * @return
     */
    private static HashMap <TO<?>, CollectionTO> getSensorMapByCollection(Collection<CollectionTO> objectValuesArray, String [] sensorProp, Environment env){
        Map<String,Set<TO<?>>> objectsByType = new HashMap<String, Set<TO<?>>>();
        
        //gather sensors
        for(CollectionTO p : objectValuesArray){				
            for(ValueTO pVal : p.getPropValues()){
                //if we have sensor, store it
                if (pVal.getValue() instanceof TO<?>){
                    TO<?> object = (TO<?>) pVal.getValue();
                    Set<TO<?>> newSet = objectsByType.get(object.getTypeName());
                    if (newSet == null) { 
                        newSet = new HashSet<TO<?>>();
                        objectsByType.put(object.getTypeName(),newSet);
                    }
                    newSet.add(object);
                }
            }								
        }
        
        Collection<CollectionTO> sensorValuesArray = new HashSet<CollectionTO>();
        
        for (String type : objectsByType.keySet()) { 
            Set<TO<?>> setOfTOs = objectsByType.get(type);
            //get properties of all sensors
            if(sensorProp == null){
                sensorValuesArray.addAll(env.getAllPropertiesValues(setOfTOs));
            }else{
                String[] fullProp = sensorProp;//addPropertiesForSensorTimeOut(sensorProp);
                Collection<CollectionTO> res =env.getPropertyValue(setOfTOs, fullProp); 
                sensorValuesArray.addAll(res);
            }
        }
        
        
        //convert sensors to hashMap
        HashMap <TO<?>, CollectionTO> sensorValuesMap = convertToHashMap(sensorValuesArray);
        //add node sampling interval for sensor time_out calculation
        //addNodeIntervalToProperties(sensorValuesMap, env);
        
        return sensorValuesMap;
    }
    
    /**
     * Converts property Collection to HashMap
     * @param valueArray
     * @return
     */
    public static HashMap <TO<?>, CollectionTO> convertToHashMap(Collection<CollectionTO> valueArray){
        HashMap <TO<?>, CollectionTO> resultMap = new HashMap <TO<?>, CollectionTO>();
        
        if(valueArray == null){
            return resultMap;
        }
        
        for(CollectionTO p : valueArray){																			
            resultMap.put(p.getObjId(), p);
        }

        return resultMap;
    }
    
    /**
     * Awful function to add property...
     */
    private static void appendPropValueToNode(TO<?> obj, ValueTO prop, Node node, Document doc, HttpServletRequest req, SimpleDateFormat sdf) throws ObjectNotFoundException {
        
        if (prop == null) {
            return;
        }

        Object o = prop.getValue();
        
        //Temporary patch. Show initial sensor state as disconnected.
        if ("lastValue".equals(prop.getPropertyName()) && (o == null || (o != null && (Double)o == -5000.0))){
            o = -3000.0;
            prop.setValue(o); //note: property value was changed here, but we don't care now..
        }
        
        if (o == null) {
            return;
        }

        Element child = doc.createElement(prop.getPropertyName());
        
        // Create the text node and append it to the prop node
        Attr attrNode = doc.createAttribute("v");
        attrNode.setValue(EnvXMLHelper.convertPropertyValueToStr(prop, LangManager.getInstance().getLocaleString(LangManager.getCurrentLocale(req), "text/n_a")));
        child.setAttributeNode(attrNode);

        if ("lastValue".equals(prop.getPropertyName())) {
            Timestamp ts = new Timestamp(prop.getTimeStamp());
            if( ts != null ) {
                String newTimestamp = sdf.format(ts);
                child.setAttribute("t", newTimestamp);
                child.setAttribute("lt", String.valueOf(prop.getTimeStamp())); //add time in long to be able to sort it
            }
        }
        if (o instanceof Double) {
            String units = Env.getUnits(obj, prop.getPropertyName(), req);
            if (units != null && !"".equals(units)) {
                child.setAttribute("u", units);
            }
        }
    
        node.appendChild(child);
    }
    
    // to get nodes which has specific type of sensor
    public static Document createDocWithSpecificChildSensors(Collection<TO<?>> objects, String [] objectProp, String [] sensorProp, int [] dataClassList, HttpServletRequest request) throws ObjectNotFoundException, ParserConfigurationException, NamingException {
        Document objectsDocRoot = null;
        Element objectsParentDocRoot = null;
        
        if(objects == null || objects.size() == 0){
            return null;
        }
        
        Environment env = Env.getEnv(request);
        
        SimpleDateFormat sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(request),UsersDataOptions.getTzIdFromRequest(request));
        
        String typeName = objects.iterator().next().getTypeName();
        
        objectsDocRoot = XMLHelper.createNewXmlDocument("children");
        objectsParentDocRoot = objectsDocRoot.getDocumentElement();
        
        
        Collection<CollectionTO> objectValuesArray = null;
        if(objectProp == null) {
            objectValuesArray = env.getAllPropertiesValues(objects);
        } else {
            objectValuesArray = env.getPropertyValue(objects, objectProp);
        }
        
        //get hashMap "object TO - collection of child sensors TO"
        HashMap< TO<?>, Collection<TO<?>> > objSensorsMap = getObjectChildSensorsMap(objects, env);
        
        //get all needed sensors
        HashMap <TO<?>, CollectionTO> sensorValuesMap = getSensorsMap(objSensorsMap, sensorProp, env);
        
        // get all necessary data class ids
        HashMap <Integer,Integer> dataClassMap = new HashMap<Integer,Integer>();
        
        for(int i=0;i< dataClassList.length;i++){
            dataClassMap.put(dataClassList[i], dataClassList[i]);
        }
                
        //add calculated display priority to make sorting base on this value
        addDispPriority(objectValuesArray);
        
        //sort objects
        List<CollectionTO> sortedObjects = CollectionTOSorter.sortObjects(objectValuesArray);
        //end sorting
                
        //run through objects properties and create document
        //for(CollectionTO p : objectValuesArray){
        for(CollectionTO p : sortedObjects){
            //add object 
            TO<?> currentObject = p.getObjId();
            
            Element newelem = objectsDocRoot.createElement(typeName);
            //objectsParentDocRoot.appendChild((Node)newelem);
            
            EnvXMLHelper.writeIDs(currentObject, newelem);						
            
            //add object properties
            for(ValueTO pVal : p.getPropValues()){				
                appendPropValueToNode(currentObject, pVal, newelem, objectsDocRoot, request, sdf);
            }
            
            //add object sensors
            Element sensorsElem = objectsDocRoot.createElement("children");
            newelem.appendChild((Node)sensorsElem);
                        
            Collection<TO<?>> currObjSensors = getHashMapValueByTOkey(objSensorsMap, currentObject);						
            
            boolean shouldNodeBeAdded = false;
            for(TO<?> sensor : currObjSensors){
                try {
                    Integer dataclass  = env.getPropertyValue(sensor, "dataclass", Integer.class);
                    
                    if(dataClassMap.containsKey(dataclass)){
                        Element propertyElem = objectsDocRoot.createElement("WSNSENSOR");
                        sensorsElem.appendChild((Node)propertyElem);
                        
                        List<ValueTO> sensorValueArr = getHashMapValueByTOkey(sensorValuesMap, sensor).getPropValues();
                        
                        //add sensor props
                        for(ValueTO curP : sensorValueArr){
                            appendPropValueToNode(sensor, curP, propertyElem, objectsDocRoot, request, sdf);												
                        }											
                            
                        //mark that we should add parent WSN node to the xml
                        shouldNodeBeAdded = true;
                    }
                    
                    
                } catch (PropertyNotFoundException e) {
                    log.error(e.getLocalizedMessage(), e);
                } catch (UnableToConvertPropertyException e) {
                    log.error(e.getLocalizedMessage(), e);
                }
            }
            
            if(shouldNodeBeAdded){
                //add WSN node to the xml
                objectsParentDocRoot.appendChild((Node)newelem);
            }
                
                
        }
        
        return objectsDocRoot;
    }
    
    /**
     * Get property values of <code>objs</code>
     * @param objs - object collection
     * @param propNames - property names
     * @param env - environment
     * @return collection of CollectionTO
     */
    public static Collection<CollectionTO> getPropertyValue(Collection<TO<?>> objs, String [] propNames, Environment env){
        
        Collection<CollectionTO> resultArray = new ArrayList<CollectionTO>();
        
        HashMap <String, Collection<TO<?>>> typesObjectMap = new HashMap <String, Collection<TO<?>>>();
        
        //separate object by its type in different collections
        for(TO<?> obj : objs){
            String currentType = obj.getTypeName();
            
            if(typesObjectMap.containsKey(currentType)){
                typesObjectMap.get(currentType).add(obj);
            }else{
                Collection<TO<?>> newObjCollection = new ArrayList<TO<?>>();
                newObjCollection.add(obj);
                typesObjectMap.put(currentType, newObjCollection);
            }    		
        }
        
        //separately get properties for each object collection
        Collection<Collection<TO<?>>> objsCollection = typesObjectMap.values();    	
        for(Collection<TO<?>> col : objsCollection){
            resultArray.addAll(env.getPropertyValue(col, propNames));
        }    	    	
        
        return resultArray;
    }
    
    /**
     * Gets property by name form list
     * @param propName - property name
     * @param propertyList - property list
     * @return
     */
    public static ValueTO getPropertyByName(String propName, Collection<ValueTO> propertyList){
        ValueTO result = null;
        
        for(ValueTO p : propertyList){
            if(p.getPropertyName().equals(propName)){
                result = p;
                break;
            }
        }
        
        return result;
    }
    
    /**
     * Returns <code>true</code> if sensor with pointed class id should not be displayed
     * @param classId - class Id
     * @param context - servlet context
     * @return
     */
    public static boolean isClassIdExcluded(String classId, ServletContext context){
        //read HashSet of excluded class IDs
        if(excludedClassIds == null){
            excludedClassIds = new HashSet<String>();    		    		

            // Get XML-configuration document
            File confFile = new File(CommonFunctions.getStandardConfigFileName(context));
            Document confDoc = null;
            try {
                confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
                                
                XPath xPath = XPathFactory.newInstance().newXPath();	    			    		
                XPathExpression xPathClassIdNodes = xPath.compile("config/excludedclassids/classid");
                XPathExpression xPathClassId = xPath.compile("@v");
                
                NodeList idssList = (NodeList) xPathClassIdNodes.evaluate(confDoc, XPathConstants.NODESET);
                
                if (idssList != null) {
                    for (int i = 0; i < idssList.getLength(); i++) {
                        excludedClassIds.add(xPathClassId.evaluate(idssList.item(i)));
                    }
                }
            } catch (SAXException e) {
                log.error(e.getLocalizedMessage(), e);				
            } catch (IOException e) {
                log.error(e.getLocalizedMessage(), e);
            } catch (ParserConfigurationException e) {
                log.error(e.getLocalizedMessage(), e);
            }catch (XPathExpressionException e) {
                log.error(e.getLocalizedMessage(), e);
            }    		    		    	    		
        }
        
        return excludedClassIds.contains(classId);
    }           
    
    /**
     * Returns null if class id for chart is not found
     * @param to
     * @param propName
     * @param env
     * @return
     */
    public static String getClassIdForChart(TO<?> to, String propName, HttpServletRequest req){
        String classId = null;
        Environment env = Env.getEnv(req);
        try{
            String objType = to.getTypeName();
            
            if(!historicalProps.containsKey(objType)){    		
                historicalProps.put(objType, EnvTypeUtil.getHistoricalPropertyNames(objType, env));
            }
            
            Dimension dim = null;
            
            //if property is not historical, it means that it is sensor
            if(!historicalProps.get(objType).contains(propName)){
                TO<?> sensor = env.getPropertyValue(to, propName, TO.class);
                dim = Env.getDimension(sensor, "lastValue", req);
            }else{
                //property is historical, get its dimension name as class id
                dim = Env.getDimension(to, propName, req);
            }
                        
            //if dimension for this property is found
            if(dim != null){
                //classId = dim.getName();
                classId = dim.getUnits();
            }
            
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }    	
        
        return classId;
    }
    
    /**
     * Returns list of collectionTO that is sorted by status
     * @param objectValuesArray
     * @return
     */
    private static void addDispPriority(Collection<CollectionTO> objectValuesArray) {
        
        for(CollectionTO p : objectValuesArray){			
            //calculate "num_display_priority"			
            int alertNumber = 0;
            ValueTO allAlertsProp = p.getSinglePropValue("numAlerts"); 
            
            if (allAlertsProp != null && allAlertsProp.getValue() != null) {
                alertNumber = ((Integer) allAlertsProp.getValue());
            }
            
            int isWarning = 0;
            ValueTO statusProp = p.getSinglePropValue("status");
            if(statusProp != null && statusProp.getValue() != null){
                // if status = 1 -> warning
                isWarning = ((Integer)statusProp.getValue() == 1) ? 0 :1; 
            }
            
            Integer numDisplayPriority = 1000*alertNumber + 100*isWarning;	
            p.addValue(new ValueTO("num_display_priority",numDisplayPriority));
        }		
    }
    
    public static Collection<TO<?>> getFilteredBySubType(Collection<TO<?>> objs, String subType, HttpServletRequest request){
        return getObjectsByPropertyValue(objs, new ValueTO("type", subType), request);
    }
    
    public static Collection<TO<?>> getObjectsByPropertyValue(Collection<TO<?>> objs, ValueTO propValue, HttpServletRequest request){
        Collection<TO<?>> result = new ArrayList<TO<?>>();
        if(objs.isEmpty()){
            return result;
        }
        Environment env = Env.getEnv(request);
        String [] pArr = new String[1];
        pArr[0] = propValue.getPropertyName();
        Collection<CollectionTO> objProps = env.getPropertyValue(objs, pArr);
        
        for(CollectionTO cTo : objProps){
            if(cTo.getPropValues().get(0).getValue().equals(propValue.getValue())){
                result.add(cTo.getObjId());
            }
        }
        return result;
    }
    
    /****************************************************/
    /********** PROPERTY UTILITIES SECTION **************/
    /****************************************************/ 
    
    public static ValueRanges getPropertyRanges(TO<?> object, String propertyName, Environment env){
        ValueRanges ranges = null;
        
        try{
            Property p  = TypeUtil.getInstance().getProperty(object.getTypeName(), propertyName);
            List<String> rangePropNames = getRangePropertyNames(p);
            if(!rangePropNames.isEmpty()){
                Collection<TO<?>> arr = new ArrayList<TO<?>>();
                arr.add(object);
                CollectionTO cto = env.getPropertyValue(arr, rangePropNames.toArray(new String[1])).iterator().next();
                cto.getPropValues();
                ranges = getPropertyRanges(object.getTypeName(), propertyName, cto.getPropValues());
            }
            
            if(ranges == null && (p != null && p.getMinval() != null && p.getMaxval() != null)){
                ranges = new ValueRanges();
                updateRangeWithAbsoluteValues(ranges, p);
            }
        }catch(Exception e){
            ranges = null;
            log.error(e.getLocalizedMessage(), e);
        }
        
        return ranges;
    }
        
    public static ValueRanges getPropertyRanges(String typeName, String propertyName, Collection<ValueTO> listProp){
        ValueRanges ranges = new ValueRanges();
    
        try{
            Property p  = TypeUtil.getInstance().getProperty(typeName, propertyName);
            
            ValueTO v = EnvTypeUtil.getPropertyByName(p.getMinref(), listProp);
            
            if(v != null) ranges.min = ((Number)v.getValue()).floatValue();            
            v = EnvTypeUtil.getPropertyByName(p.getMaxref(), listProp);
            if(v != null) ranges.max = ((Number)v.getValue()).floatValue();            
            v = EnvTypeUtil.getPropertyByName(p.getRminref(), listProp);
            if(v != null) ranges.rMin = ((Number)v.getValue()).doubleValue();            
            v = EnvTypeUtil.getPropertyByName(p.getAminref(), listProp);
            if(v != null) ranges.aMin = ((Number)v.getValue()).doubleValue();            
            v = EnvTypeUtil.getPropertyByName(p.getRmaxref(), listProp);
            if(v != null) ranges.rMax = ((Number)v.getValue()).doubleValue();            
            v = EnvTypeUtil.getPropertyByName(p.getAmaxref(), listProp);
            if(v != null) ranges.aMax = ((Number)v.getValue()).doubleValue();
                      
            updateRangeWithAbsoluteValues(ranges, p);
        }catch(Exception e){
            ranges = null;
            log.error(e.getLocalizedMessage(), e);
        }
        
        return ranges;
    }
    
    public static Document getPropertyRangesDoc(String typeName, String propertyName, Collection<ValueTO> listProp)throws ParserConfigurationException{
        Document doc = convertRangesToDoc(getPropertyRanges(typeName, propertyName, listProp));        
        return doc;
    }
    
    public static Document getPropertyDescrDoc(TO<?> obj, String propertyName, HttpServletRequest request)throws ParserConfigurationException{
        
        Document doc = XMLHelper.createNewXmlDocument("propertyDescr");
        Element docRoot = doc.getDocumentElement();
        
        String units = Env.getUnits(obj, propertyName, request);
        String longUnits = Env.getLongUnits(obj, propertyName, request);
        if(units != null){
            addPropAttribute(doc, docRoot, "u", units);
        }
        if(longUnits != null){
            addPropAttribute(doc, docRoot, "longUnits", longUnits);
        }
        
        return doc;
    }
    
    private static void updateRangeWithAbsoluteValues(ValueRanges ranges, Property p){
        try{
            String val = p.getMinval();
            if(val != null) ranges.min = Float.parseFloat(val);
            val = p.getMaxval();
            if(val != null) ranges.max = Float.parseFloat(val);            
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }
    }
    
    private static Document convertRangesToDoc(ValueRanges ranges) throws ParserConfigurationException{
        Document doc = XMLHelper.createNewXmlDocument("propertyType");
        
        if(ranges != null && (ranges.aMin != 0 || ranges.aMax != 0 || ranges.rMin != 0 || ranges.rMax != 0 
           || ranges.min.floatValue() != 0 || ranges.max.floatValue() != 0)){
            
            Element docRoot = doc.getDocumentElement();
            
            addPropAttribute(doc, docRoot, "min", ranges.min);
            addPropAttribute(doc, docRoot, "max", ranges.max);
            
            if(!(ranges.aMin == 0 && ranges.aMax == 0 && ranges.rMin == 0 && ranges.rMax == 0)){
                addPropAttribute(doc, docRoot, "rMin", ranges.rMin);
                addPropAttribute(doc, docRoot, "rMax", ranges.rMax);
                addPropAttribute(doc, docRoot, "aMin", ranges.aMin);
                addPropAttribute(doc, docRoot, "aMax", ranges.aMax); 
            }
        }        
        return doc;
    }
    
    private static List<String> getRangePropertyNames(Property p){
        List<String> result = new ArrayList<String>();
        
        if(p != null){
            if(p.getMinref() != null) result.add(p.getMinref());
            if(p.getMaxref() != null) result.add(p.getMaxref());
            if(p.getRminref() != null) result.add(p.getRminref());
            if(p.getRmaxref() != null) result.add(p.getRmaxref());
            if(p.getAminref() != null) result.add(p.getAminref());
            if(p.getAmaxref() != null) result.add(p.getAmaxref()); 
        }
        
        return result;
    }
    
    private static void addPropAttribute(Document doc, Element docRoot, String attrName, Object attrValue){
        if(attrValue != null){
            Element newelem = doc.createElement(attrName);
            newelem.setAttribute("v", attrValue.toString());
            docRoot.appendChild((Node)newelem);
        }
    }
}
