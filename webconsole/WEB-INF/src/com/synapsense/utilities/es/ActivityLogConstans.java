package com.synapsense.utilities.es;

public class ActivityLogConstans {
    private ActivityLogConstans() {}
    public static final String MODULE_ANNOTATION					= "Annotation";
    public static final String MODULE_USER_MANAGEMENT				= "UserManagement";
    public static final String MODULE_ACTIVE_CONTROL				= "ActiveControl";
    public static final String MODULE_EMAIL							= "Email";
    public static final String MODULE_AUDITORS						= "Auditors";
    public static final String MODULE_SECURITY						= "Security";
    public static final String MODULE_SNMP							= "SNMP";
    public static final String MODULE_LIVEIMAGING					= "LiveImaging";
    public static final String MODULE_POEWRIMAGING					= "PowerImaging";
    public static final String MODULE_ALERT_MANAGEMENT				= "AlertManagement";

    public static final String ACTION_MANAGE_ANNOTATION				= "ManageAnnotation";
    public static final String ACTION_USER_LOGIN					= "UserLogin";
    public static final String ACTION_USER_LOGOUT					= "UserLogout";
    public static final String ACTION_USER_LOGIN_FAILED				= "UserLoginFailed";
    public static final String ACTION_USER_CONTROL_PARAMETER_CHANGE	= "UserControlParameterChange";
    public static final String ACTION_UPDATE_CONFIGURATION			= "UpdateConfiguration";
    public static final String ACTION_DELETE_ALL_ALERTS				= "DeleteAllAlerts";

}
