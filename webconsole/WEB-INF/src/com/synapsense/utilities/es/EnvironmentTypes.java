package com.synapsense.utilities.es;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentTypes {
    private EnvironmentTypes() {}
    public static final String  TYPE_ROOT                       = "ROOT";
    public static final String  TYPE_DC                         = "DC";
    public static final String  TYPE_ROOM                       = "ROOM";
    public static final String  TYPE_INSPECTOR                  = "INSPECTOR";
    public static final String  TYPE_CRAC                       = "CRAC";
    public static final String  TYPE_RACK                       = "RACK";
    public static final String  TYPE_PRESSURE                   = "PRESSURE";
    public static final String  TYPE_PANEL                      = "PANEL";
    public static final String  TYPE_PDIPDU                     = "PDIPDU";
    public static final String  TYPE_CIRCUIT                    = "CIRCUIT";
    public static final String  TYPE_SENSOR                     = "WSNSENSOR";
    public static final String  TYPE_CALCED_SENSOR              = "CALCED_SENSOR";
    public static final String  TYPE_SANDOVAL_SENSOR            = "SANDOVALSENSOR";
    public static final String  TYPE_NODE                       = "WSNNODE";
    public static final String  TYPE_ROUTE                      = "ROUTE";
    public static final String  TYPE_NETWORK                    = "WSNNETWORK";
    public static final String  TYPE_CUSTOMQUERY                = "CUSTOMQUERY";
    public static final String  TYPE_GATEWAY                    = "WSNGATEWAY";
    public static final String  TYPE_PUE                        = "PUE";
    public static final String  TYPE_MODBUSPROPERTY             = "MODBUSPROPERTY";
    public static final String  TYPE_INSTRUMENTED_OBJECT        = "INSTRUMENTED_OBJECT";
    public static final String  TYPE_BANDED_DISPLAY             = "BANDED_DISPLAY";
    public static final String  SUB_TYPE_ENERGY_WYE             = "energy-wye";
    public static final String  TYPE_ION_DELTA                  = "ION_DELTA";
    public static final String  TYPE_ION_WYE                    = "ION_WYE";
    public static final String  TYPE_SET                        = "SET";
    public static final String  TYPE_CONTROLPOINT_SINGLECOMPARE = "CONTROLPOINT_SINGLECOMPARE";
    public static final String  TYPE_CONTROLPOINT_STRAT         = "CONTROLPOINT_STRAT";
    public static final String  TYPE_CONTROLOUT_CRAH            = "CONTROLOUT_CRAH";
    public static final String  TYPE_CONTROLOUT_CRAC            = "CONTROLOUT_CRAC";
    public static final String  TYPE_CONTROLOUT_VFD             = "CONTROLOUT_VFD";
    public static final String  TYPE_LOGICAL_ROOM               = "LOGICALROOM";
    public static final String  TYPE_CONTROLLER_STATUS          = "CONTROLLER_STATUS";
    public static final String  TYPE_CONFIGURATION_DATA         = "CONFIGURATION_DATA";
    public static final String  TYPE_GROUP                      = "GROUP";
    public static final String  TYPE_POWER_RACK                 = "POWER_RACK";
    public static final String  TYPE_SERVER                     = "SERVER";
    public static final String  TYPE_RPDU                       = "RPDU";
    public static final String  TYPE_PHASE                      = "PHASE";
    public static final String  TYPE_PLUG                       = "PLUG";
    public static final String  TYPE_TABULAR_TEMPLATE           = "TABULAR_TEMPLATE";
    public static final String  TYPE_MODBUSNETWORK 				= "MODBUSNETWORK";
    public static final String  TYPE_MODBUSDEVICE 				= "MODBUSDEVICE";
    public static final String  TYPE_ALERT_MESSAGE_TEMPLATE		= "ALERT_MESSAGE_TEMPLATE";
    public static final String  TYPE_DASHBOARD 					= "DASHBOARD";
    public static final String  TYPE_DASHBOARD_CAROUSEL 		= "DASHBOARD_CAROUSEL";
    public static final String  TYPE_GEO 						= "GEO";
    public static final String  TYPE_ES 						= "ES";
    public static final String  TYPE_IPMISENSOR 				= "IPMISENSOR";
    public static final String  MOBILE_DEVICE 					= "MOBILE_DEVICE";
    public static final String  TYPE_GENERICTEMPERATURE 		= "GENERICTEMPERATURE";
    public static final String  TYPE_VERTICALTEMPERATURE 		= "VERTICALTEMPERATURE";

    private static List<String> sensorTypes = new  ArrayList<String>();
    
    static {
        sensorTypes.add(EnvironmentTypes.TYPE_SENSOR);
        sensorTypes.add(EnvironmentTypes.TYPE_MODBUSPROPERTY);
        sensorTypes.add(EnvironmentTypes.TYPE_CALCED_SENSOR);
        sensorTypes.add(EnvironmentTypes.TYPE_SANDOVAL_SENSOR);
        sensorTypes.add(EnvironmentTypes.TYPE_IPMISENSOR);
    }
    
    private static List<String> ionTypes = new  ArrayList<String>();
    static{
        ionTypes.add(TYPE_ION_DELTA);
        ionTypes.add(TYPE_ION_WYE);
    }
    
    public static  List<String> listSensorTypes() {
        return sensorTypes;
    }
    
    public static  boolean isSensorType(String type) {
        return sensorTypes.contains(type);
    }
    
    public static boolean isIonType(String type){
       return ionTypes.contains(type);
    }
    
    public static List<String> getIonTypes(){
        return ionTypes;
    }
}