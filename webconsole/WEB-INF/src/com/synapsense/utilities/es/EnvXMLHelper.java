package com.synapsense.utilities.es;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.Text;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.GenericObjectTO;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;



public class EnvXMLHelper {

    static private ThreadLocal<NumberFormat> numberFormat = new ThreadLocal<NumberFormat>();

    public static void setNumberFormat(NumberFormat numberFormat){
        EnvXMLHelper.numberFormat.set(numberFormat);
    }
    
    public static void writePropAsAttrs(TO<?> obj, Element newelem, Environment env) throws ObjectNotFoundException
    {
        Collection<ValueTO> props = env.getAllPropertiesValues(obj);		
        for (ValueTO prop:props)
        {
            if (prop.getValue() != null) {
                newelem.setAttribute(prop.getPropertyName(), prop.getValue().toString());
            }
        }
    }

    
    public static void writePropAsAttrs(TO<?> obj, Element newelem, Environment env,String[] propNames) throws ObjectNotFoundException
    {
        ArrayList<TO<?>> arr = new ArrayList<TO<?>>();
        arr.add(obj);
        Collection<CollectionTO> colTos =env.getPropertyValue(arr, propNames);
        
        for (CollectionTO colTo : colTos) {
            EnvXMLHelper.writePropAsAttrs(colTo, newelem);
        }
    }
    
    public static void writePropAsAttrs(CollectionTO colTo, Element newelem) {
        Collection<ValueTO> props = colTo.getPropValues();
            for (ValueTO prop:props)
            {
                Object value = prop.getValue();
                if (value != null) {
                    newelem.setAttribute(prop.getPropertyName(), prop.getValue().toString());
                }
            }
    }

    public static void writePropAsChilds(TO<?> obj, Document doc, Element newelem, List<ValueTO> listProp, HttpServletRequest req) throws ObjectNotFoundException
    {
        for (ValueTO prop: listProp)
        {
            Element child = doc.createElement(prop.getPropertyName());
    
            EnvXMLHelper.appendPropValueToNode(prop.getValue(), child, doc, req);
            
            String units = Env.getUnits(obj, prop.getPropertyName(), req);
            if (units != null && !"".equals(units)) {
                child.setAttribute("u", units);
            }

            if ("lastValue".equals(prop.getPropertyName())){
                Element time = doc.createElement("data_time_stamp");
                EnvXMLHelper.appendPropValueToNode( new Timestamp(prop.getTimeStamp()), time, doc, req);
                newelem.appendChild(time);
            }
            
            newelem.appendChild(child);
        }
    }

    public static void appendPropValueToNode(Object o, Element node, Document doc, HttpServletRequest req) throws ObjectNotFoundException {
        
        if (o == null) {
            return;
        }
        
        if (o instanceof GenericObjectTO) {
            GenericObjectTO sensor = (GenericObjectTO) o;
            
            EnvXMLHelper.writePropAsChilds(sensor, doc, node, req);
        }
        else if (o instanceof Double) {
            node.setAttribute("v", CommonFunctions.getFormattedNumber(numberFormat.get(),(Double) o));
    
        }
        else {
            // Create the text node and append it to the prop node
            node.setAttribute("v", o.toString());
        }
    }

    public static void writePropAsChilds(TO<?> obj, Document doc, Element newelem, HttpServletRequest req) throws ObjectNotFoundException
    {
        Environment env = Env.getEnv(req);
        Collection<ValueTO> props = env.getAllPropertiesValues(obj);
    
        for (ValueTO prop:props)
        {
            Element child = doc.createElement(prop.getPropertyName());
    
            appendPropValueToNode(prop.getValue(), child, doc, req);
            
            String units = Env.getUnits(obj, prop.getPropertyName(), req);
            if (units != null && !"".equals(units)) {
                child.setAttribute("u", units);
            }

            if ("lastValue".equals(prop.getPropertyName())){
                Element time = doc.createElement("data_time_stamp");
                appendPropValueToNode( new Timestamp(prop.getTimeStamp()), time, doc, req);
                newelem.appendChild(time);
            }
            
            newelem.appendChild(child);
        }
    }

    public static void writeIDs(TO<?> obj, Element newelem) throws ObjectNotFoundException
    {
        newelem.setAttribute("id", TOFactory.getInstance().saveTO(obj));
        newelem.setAttribute("type", obj.getTypeName());
    }

    /**
     * Converts property value to string
     * @param pVal
     * @param numberFormatter
     * @return
     */
    public static String convertPropertyValueToStr(ValueTO pVal, String defaultText){		
        Object val = pVal.getValue();
        String valueStr;
        
        if (val == null){
            valueStr = defaultText;
        }else if (val instanceof Double) {
            valueStr = CommonFunctions.getFormattedNumber(numberFormat.get(),(Double) val);
        }else{															
            valueStr = val.toString();
        }
        
        return valueStr;
    }

    /**
     * this method retrieves data from the Server API
     * 
     * @param dataRequestName
     * @param pStatement
     * @return
     * @throws ObjectNotFoundException 
     * @throws ParserConfigurationException 
     * @throws UnableToConvertPropertyException 
     * @throws PropertyNotFoundException 
     * @throws SQLException
     */
    public static Document retrieveXml(Collection<TO<?>> objs,String rootNodeName,
            String innerNodesName, HttpServletRequest request) throws ObjectNotFoundException, ParserConfigurationException, PropertyNotFoundException, UnableToConvertPropertyException {
        return EnvXMLHelper.retrieveXml(objs, null, rootNodeName, innerNodesName, request);
    }

    public static Document retrieveXml(Collection<TO<?>> objs, List<String> propNames, String rootNodeName,
            String innerNodesName, HttpServletRequest request) throws ObjectNotFoundException, ParserConfigurationException, PropertyNotFoundException, UnableToConvertPropertyException {
        Document xmlDoc = null;
    
        Environment env = Env.getEnv(request);
        // Create the XML Document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        xmlDoc = docBuilder.newDocument();
    
        Node rootNode = xmlDoc.createElement(rootNodeName);
        xmlDoc.appendChild(rootNode);
    
        if (objs == null || objs.size() == 0)
            return xmlDoc;
                
        Collection<CollectionTO> valueArray = null;				
        
        if(propNames == null){			
            valueArray = env.getAllPropertiesValues(objs);
        }else{			
            String [] propsStrArray = propNames.toArray(new String[propNames.size()]);
            if(EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(objs.iterator().next().getTypeName())){
                //add "type" for classID retrieving
                propsStrArray = DataRetrievingUtils.addPropertyNameToExistingArray(propsStrArray, "dataclass");
            }
            valueArray = env.getPropertyValue(objs, propsStrArray);
        }
        
        valueArray = CollectionTOSorter.sortObjects(valueArray);
                
        for(CollectionTO vArr : valueArray){
            
            //add object
            TO<?> obj = vArr.getObjId();
            
            //skip excluded class IDs
            if(EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(obj.getTypeName())){								
                ValueTO dataclass = vArr.getSinglePropValue("dataclass");
                if(dataclass != null
                    && DataRetrievingUtils.isClassIdExcluded(dataclass.getValue().toString(), request.getSession().getServletContext())){
                    continue;					
                }
            }
                        
            Node recordNode = null;
            if (innerNodesName != null)
                recordNode = xmlDoc.createElement(innerNodesName);
            else
                recordNode = xmlDoc.createElement(obj.getTypeName());
            // Append the record node to the root node
            rootNode.appendChild(recordNode);
            
            Node idNode = XMLHelper.createXmlTextNode(xmlDoc, "id", TOFactory.getInstance().saveTO(obj));
            recordNode.appendChild(idNode);
            
            if (obj.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_CRAC)||obj.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_RACK))
            {
                TO<?> room = env.getParents(obj, EnvironmentTypes.TYPE_ROOM).iterator().next();
                                
                Element textNode = xmlDoc.createElement("roomid");
                textNode.setTextContent(TOFactory.getInstance().saveTO(room));
                recordNode.appendChild(textNode);
            }
            
            //add properties
            List<ValueTO> propValues = vArr.getPropValues();
            for(ValueTO pVal : propValues){				
                
                String columnName = pVal.getPropertyName();				
                String innerText = (pVal.getValue() != null) ? pVal.getValue().toString() : "";
    
                // Create a node for the column
                Node columnNode = xmlDoc.createElement(columnName);
    
                // Add the column node to the record node
                recordNode.appendChild(columnNode);
    
                // Create the text node and append it to the column node
                if (innerText != null) {
                    Text textNode = xmlDoc.createTextNode(innerText);
                    columnNode.appendChild(textNode);
                } else {
                    Text textNode = xmlDoc.createTextNode("");
                    columnNode.appendChild(textNode);
                }
                
            }//	for(ValueTO pVal : propValues)
                                    
        }//	for(CollectionTO vArr : valueArray)
                
        return xmlDoc;
    }

    public static Document retrieveXmlWithAttr(Collection<TO<?>> objs, ArrayList<String> propNames, String rootNodeName,
            String innerNodesName, HttpServletRequest request, String sortBy) throws ObjectNotFoundException, ParserConfigurationException, PropertyNotFoundException, UnableToConvertPropertyException {
        Document xmlDoc = null;
    
        Environment env = Env.getEnv(request);
        // Create the XML Document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        xmlDoc = docBuilder.newDocument();
    
        Node rootNode = xmlDoc.createElement(rootNodeName);
        xmlDoc.appendChild(rootNode);
    
        if (objs == null || objs.size() == 0)
            return xmlDoc;
                
        Collection<CollectionTO> valueArray = null;				
        
        if(propNames == null){			
            valueArray = env.getAllPropertiesValues(objs);
        }else{			
            String [] propsStrArray = propNames.toArray(new String[propNames.size()]);
            if(EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(objs.iterator().next().getTypeName())){
                //add "type" for classID retrieving
                propsStrArray = DataRetrievingUtils.addPropertyNameToExistingArray(propsStrArray, "dataclass");
            }
            valueArray = env.getPropertyValue(objs, propsStrArray);
        }
        
        valueArray = CollectionTOSorter.sortObjects(valueArray, sortBy);
                
        for(CollectionTO vArr : valueArray){
            
            //add object
            TO<?> obj = vArr.getObjId();
            
            //skip excluded class IDs
            if(EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(obj.getTypeName())){								
                ValueTO dataclass = vArr.getSinglePropValue("dataclass");
                if(dataclass != null
                    && DataRetrievingUtils.isClassIdExcluded(dataclass.getValue().toString(), request.getSession().getServletContext())){
                    continue;					
                }
            }
                        
            Node recordNode = null;
            if (innerNodesName != null)
                recordNode = xmlDoc.createElement(innerNodesName);
            else
                recordNode = xmlDoc.createElement(obj.getTypeName());
            // Append the record node to the root node
            rootNode.appendChild(recordNode);
            
            ((Element)recordNode).setAttribute("id", TOFactory.getInstance().saveTO(obj));
            
            if (obj.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_CRAC)||obj.getTypeName().equalsIgnoreCase(EnvironmentTypes.TYPE_RACK))
            {
                TO<?> room = env.getParents(obj, EnvironmentTypes.TYPE_ROOM).iterator().next();
                ((Element)recordNode).setAttribute("roomid", TOFactory.getInstance().saveTO(room));
            }
            
            //add properties
            List<ValueTO> propValues = vArr.getPropValues();
            for(ValueTO pVal : propValues){				
                
                String columnName = pVal.getPropertyName();				
                String innerText = (pVal.getValue() != null) ? pVal.getValue().toString() : "";
    
                // Add the column node to the record node
                // Create the text node and append it to the column node
                if (innerText != null) {
                    ((Element)recordNode).setAttribute(columnName, innerText);
                } else {
                    ((Element)recordNode).setAttribute(columnName, "");
                }
            }//	for(ValueTO pVal : propValues)
                                    
        }//	for(CollectionTO vArr : valueArray)
                
        return xmlDoc;
    }

}
