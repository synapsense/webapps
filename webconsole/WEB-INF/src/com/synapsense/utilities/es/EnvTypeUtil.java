package com.synapsense.utilities.es;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.config.types.Property;
import com.synapsense.config.types.Type;
import com.synapsense.config.types.TypeUtil;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.service.Environment;

public class EnvTypeUtil {
    static private final String TYPE_WILDCARD = "*";

    @SuppressWarnings("unchecked")
    static public ArrayList<String> getHistoricalPropertyNames(String objTypeName, Environment env){
        ArrayList<String> propNames;
        
        if(EnvTypeUtil.histProps.containsKey(objTypeName)){
            propNames = EnvTypeUtil.histProps.get(objTypeName);
        } else {
        
            propNames = new ArrayList<String>();
             
            ObjectType objType = env.getObjectType(objTypeName);					
             
            //Get property descriptors of type 
            Set<PropertyDescr> propDescrSet = objType.getPropertyDescriptors();
             
            //Get historic property names					
            for(PropertyDescr pDesc : propDescrSet){
                //Currently we chart only historical double properties
                if (pDesc.isHistorical() && pDesc.getType().isAssignableFrom(Double.class)){
                    propNames.add(pDesc.getName());
                }
            }
            
            //Merge with descriptors from types.xml
            Type type = TypeUtil.getInstance().getType(objTypeName);
            if(type != null){
                 List<Property> properties = type.getProperty();
                 for (Property property : properties) {
                     if (property.getFormula() != null && property.isVisible()) {
                         propNames.add(property.getName());
                     }
                 }
            }
             
            EnvTypeUtil.histProps.put(objTypeName, propNames);
        }	 
    
        return (ArrayList<String>) propNames.clone();
     }

    /**
     * Get collection of linked sensors by object
     * @param parent
     * @param env
     * @return
     * @throws ObjectNotFoundException
     */
    public static ArrayList<ValueTO> getPropertySensors(TO<?> parent, Environment env) throws ObjectNotFoundException{
        
        ArrayList<ValueTO> result = new ArrayList<ValueTO>();				
        
        //add sensors which are property								
        Collection<ValueTO> props = env.getAllPropertiesValues(parent);
        
        for (ValueTO prop:props)
        {						
            Object pVal = prop.getValue();
            
            if(pVal == null){
                continue;
            }
            
            if(pVal instanceof TO<?> && EnvironmentTypes.isSensorType(((TO<?>) pVal).getTypeName())){
                result.add(prop);
            }
            
            /*
            TO<?> checkingProp = null;
            
            //don't know how to check is property TO.. so use try...catch :(
            try{
                checkingProp = env.getPropertyValue(parent, prop.getPropertyName(), TO.class);
                
                String tName = checkingProp.getTypeName();
                if(tName.equals(EnvironmentTypes.TYPE_SENSOR)){					
                    result.add(prop);
                }
            }catch(Exception e){
            }												
            */
        }
        
        return result;
    }

    /**
     * Gets property by name form list
     * @param propName - property name
     * @param propertyList - property list
     * @return
     */
    public static <T extends Collection<ValueTO>> ValueTO getPropertyByName(String propName, T propertyList){
        ValueTO result = null;
        
        for(ValueTO p : propertyList){
            if(p.getPropertyName().equals(propName)){
                result = p;
                break;
            }
        }
        
        return result;
    }

    /*=================================================================*/
    /*====================== Functions for HISTORICAL =================*/
    /*=================================================================*/
    public static ValueRanges getValuesRangesBySensor(TO<?> sensor, Environment env) throws EnvException{
        ValueRanges result = new ValueRanges();    	    	                
                        
        Double defaultValue = 0.0;
        
        String min = String.valueOf(EnvTypeUtil.getPropertySafely(sensor, "min", defaultValue, env));
        String max = String.valueOf(EnvTypeUtil.getPropertySafely(sensor, "max", defaultValue, env));
        
        result.min = Float.parseFloat(min); 
        result.max = Float.parseFloat(max);
        
        result.aMin = (Double)EnvTypeUtil.getPropertySafely(sensor, "aMin", defaultValue, env);        
        result.aMax = (Double)EnvTypeUtil.getPropertySafely(sensor, "aMax", defaultValue, env);        
        result.rMin = (Double)EnvTypeUtil.getPropertySafely(sensor, "rMin", defaultValue, env);
        result.rMax = (Double)EnvTypeUtil.getPropertySafely(sensor, "rMax", defaultValue, env);                
        
        return result;
    }

    /**
     * Return sensor data type name by dataclass.
     * @param dataClassId
     * @return
     */
    public static String getSensorUnitString(Integer dataclass){
        
        return EnvTypeUtil.dataclassToTypeNameMap.get(dataclass);
    }

    private static HashMap <String, ArrayList<String>> histProps = new HashMap <String, ArrayList<String>>();
    //------------ yAxis Names Mapping
    private final static HashMap<String, String> dataclassToTypeNameMap = new HashMap<String, String>();

    static {		
        EnvTypeUtil.dataclassToTypeNameMap.put("200", "Temperature");
        EnvTypeUtil.dataclassToTypeNameMap.put("201", "Relative Humidity");
        EnvTypeUtil.dataclassToTypeNameMap.put("202", "Pressure");
        EnvTypeUtil.dataclassToTypeNameMap.put("203", "Purity");
        EnvTypeUtil.dataclassToTypeNameMap.put("204", "Particle Count");
        EnvTypeUtil.dataclassToTypeNameMap.put("205", "Voltage");
        EnvTypeUtil.dataclassToTypeNameMap.put("206", "Current");
        EnvTypeUtil.dataclassToTypeNameMap.put("207", "Energy");
        EnvTypeUtil.dataclassToTypeNameMap.put("208", "pH");
        EnvTypeUtil.dataclassToTypeNameMap.put("209", "Liquid Flow");
        EnvTypeUtil.dataclassToTypeNameMap.put("210", "Battery Strength");
        EnvTypeUtil.dataclassToTypeNameMap.put("211", "Door Status");
        EnvTypeUtil.dataclassToTypeNameMap.put("212", "Air Flow");
        EnvTypeUtil.dataclassToTypeNameMap.put("213", "Liquid Presence");
        EnvTypeUtil.dataclassToTypeNameMap.put("214", "Equipment Status");
    }

    /**
     * Gets property value with class of defaultValue parameter. If property is null return defaultValue.
     * @param parent - object TO
     * @param propName - property name
     * @param defaultValue - value which will be return if property is null 
     * @param env - environment
     * @return
     * @throws EnvException
     */
    public static Object getPropertySafely(TO<?> parent, String propName, Object defaultValue, Environment env) throws EnvException{
                
        Object result = env.getPropertyValue(parent, propName, defaultValue.getClass());
        
        if(result == null){
            result = defaultValue;
        }
        
        return result;
    }
    
    private static Map<String,Map<String, Class<?>>> invisiblePropsMap = new HashMap<String, Map<String, Class<?>>>();
    private static Map<String,Map<String,Class<?>>> propertyTypeMap = new HashMap<String, Map<String,Class<?>>>();
    
    static {
        Map<String, Class<?>> commonPropsMAp = new HashMap<String, Class<?>>();
        commonPropsMAp.put("numAlerts", Integer.class);
        commonPropsMAp.put("objectId", Integer.class);
        invisiblePropsMap.put(EnvTypeUtil.TYPE_WILDCARD, commonPropsMAp);
    }
    
    public static Class<?> getPropertyClass (String propName,String typeName,Environment environment) 
        throws PropertyNotFoundException,ObjectNotFoundException {

        Map<String, Class<?>> objMap = invisiblePropsMap.get(EnvTypeUtil.TYPE_WILDCARD);
        if (objMap != null) {
            Class<?> c = objMap.get(propName);
            if (c != null) {
                return c;
            }
        }
        
        Map<String, Class<?>> types = new HashMap<String, Class<?>>();
        if (!propertyTypeMap.containsKey(typeName)) {
            propertyTypeMap.put(typeName, types);
        } else {
            types = propertyTypeMap.get(typeName);
        }
        if (!types.containsKey(propName)) {
            PropertyDescr descr = environment.getPropertyDescriptor(typeName, propName);
            Class<?> clazz = descr.getType();
            types.put(propName, clazz);
            return clazz;
        } else {
            return types.get(propName);
        }
    }

    public static  boolean isHistorical(String type , String propName, Environment environment){
        
        List<String> propNames = EnvTypeUtil.getHistoricalPropertyNames(type, environment);
        
        return propNames.contains(propName);
   }
    
}
