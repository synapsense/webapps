package com.synapsense.utilities.es;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;

public class CollectionTOSorter {

    public static List<CollectionTO> sortObjects(Collection<CollectionTO> values, String sortBy) {
        List<CollectionTO> valuesList= new LinkedList<CollectionTO>(values);
        Collections.sort(valuesList, (sortBy != null && !sortBy.isEmpty()) ? new CollectionTOComparator(new SortingParams(sortBy, true)) : new CollectionTOComparator());
        return valuesList;
    }

    public static List<CollectionTO> sortObjects(Collection<CollectionTO> values) {
        List<CollectionTO> valuesList= new LinkedList<CollectionTO>(values);
        Collections.sort(valuesList, new CollectionTOComparator());
        return valuesList;
    }

}

class SortingParams {
    String property;
    boolean isAsc = true;

    public SortingParams(String property, boolean isAsc) {
        this.property = property;
        this.isAsc = isAsc;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public boolean isAsc() {
        return isAsc;
    }

    public void setAsc(boolean isAsc) {
        this.isAsc = isAsc;
    }
}

class CollectionTOComparator implements Comparator<CollectionTO> {
    private SortingParams params = null;
    private static HashMap <String, SortingParams> sortByPropsMap = new HashMap <String, SortingParams>();
    static {
        sortByPropsMap.put(EnvironmentTypes.TYPE_SENSOR, new SortingParams("channel", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_CIRCUIT, new SortingParams("num", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_PANEL, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_ROOM, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_DC, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_PDIPDU, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_CUSTOMQUERY, new SortingParams("name", true));
        //num_display_priority - property added in servlet, calculated based on status and number of active alerts
        sortByPropsMap.put(EnvironmentTypes.TYPE_CRAC, new SortingParams("num_display_priority", false));
        sortByPropsMap.put(EnvironmentTypes.TYPE_RACK, new SortingParams("num_display_priority", false));
        sortByPropsMap.put(EnvironmentTypes.TYPE_NODE, new SortingParams("num_display_priority", false));
        sortByPropsMap.put(EnvironmentTypes.TYPE_POWER_RACK, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_RPDU, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_PHASE, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_SERVER, new SortingParams("uLocation", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_SET, new SortingParams("name", true));
        sortByPropsMap.put(EnvironmentTypes.TYPE_NETWORK, new SortingParams("name", true));
    }
    
    public CollectionTOComparator() {
    }
    
    public CollectionTOComparator(SortingParams params) {	
        this.params = params;
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @Override
    public int compare(CollectionTO o1, CollectionTO o2) {
        TO<?> to1 = o1.getObjId();
        TO<?> to2 = o2.getObjId();
        SortingParams param = (this.params == null) ? sortByPropsMap.get(to1.getTypeName()) : this.params;
        if (to1.getTypeName().equals(to2.getTypeName()) && param != null) {
            String prop = param.getProperty();
            int multiplier = param.isAsc ? 1 : -1;
            List<ValueTO> vto1= o1.getPropValue(prop);
            List<ValueTO> vto2 = o2.getPropValue(prop);
            Object prop1 = null;
            Object prop2 = null;
            if (vto1 != null && vto1.size() == 1) {
                prop1 =  vto1.get(0).getValue();
            }
            if (vto2 != null && vto2.size() == 1) {
                prop2 = vto2.get(0).getValue();
            }
            if (prop1 == null && prop2 == null) {
                return 0;
            } else if (prop1 == null) {
                return -1;
            } else if (prop2 == null) {
                return 1;
            } else if (prop1 instanceof Comparable) {
                return ((Comparable) prop1).compareTo(prop2) * multiplier;
            } else if (prop2 instanceof Comparable) {
                return ((Comparable) prop2).compareTo(prop1) * multiplier;
            }
        }
        return 0;
    }
}
