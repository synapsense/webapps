package com.synapsense.utilities.es;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dto.ValueTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.EnvException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.sensors.SensorPoint;

public class EnvHistoryUtil {

    /** Values that will be excluded from list (for example, engineer values) */
    private static List<Double> excludedValues = new ArrayList<Double>();
    static{
        excludedValues.add(-2000.0);
        excludedValues.add(-3000.0);
        excludedValues.add(-5000.0);
    };

    /**
     * Gets array of "SensorPoint" by request.
     * @param request - HttpServletRequest 
     * @param object - object TO
     * @param propertyName - historical property name
     * @param env - environment
     * @return
     * @throws EnvException
     */
    public static ArrayList<SensorPoint> getHistoricSensorPointsFromRequest(HttpServletRequest request, TO<?> object, String propertyName, Environment env) throws EnvException{
        
        ArrayList<SensorPoint> result = new ArrayList<SensorPoint>();
        Date beginDate = new Date(Long.parseLong(request.getParameter(RequestVariableNames.BEGIN)));
        Date endDate = new Date(Long.parseLong(request.getParameter(RequestVariableNames.END)));
        String histByStr = request.getParameter(RequestVariableNames.HISTORY_BY);				
        
        if(RequestVariableNames.LAST_X_MEASURES.equals(histByStr)){
            int pointNumber = Integer.parseInt(request.getParameter("dataPoints"));
            result = EnvHistoryUtil.getLastXPoints(object, propertyName, pointNumber, env);
            
        }else{
            result = EnvHistoryUtil.getPointsBetweenDates(object, propertyName, beginDate, endDate, env);
        }				
        
        return result;
    }

    /**
     * Gets array of "SensorPoint" by request and filter out points with excluded values
     * @param request - HttpServletRequest 
     * @param object - object TO
     * @param propertyName - historical property name
     * @param env - environment
     * @return
     * @throws EnvException
     */
    public static ArrayList<SensorPoint> getFilteredHistoricSensorPointsFromRequest(HttpServletRequest request, TO<?> object, String propertyName, Environment env) throws EnvException{
        ArrayList<SensorPoint> allPoints = getHistoricSensorPointsFromRequest(request, object, propertyName, env);
        ArrayList<SensorPoint> result = new ArrayList<SensorPoint>();
        
        SensorPoint point = null;
        for(int i = 0; i < allPoints.size(); i++){
            point = allPoints.get(i);
            if(!isPointExcluded(point)){
                result.add(point);
            }
        }
        return result;
    }

    public static List<SensorPoint> removeExcluded(List<SensorPoint> allPoints){
        ArrayList<SensorPoint> result = new ArrayList<SensorPoint>();        
        SensorPoint point = null;
        for(int i = 0; i < allPoints.size(); i++){
            point = allPoints.get(i);
            if(!isPointExcluded(point)){
                result.add(point);
            }
        }
        return result;
    }

    /**
     * Gets dates from request
     * @param request
     * @param beginDate - will be set as retrieved begin date 
     * @param endDate - will be set as retrieved end date
     */
    public static void getDatesFromRequest(HttpServletRequest request, Date beginDate, Date endDate){		        
        String histByStr = request.getParameter(RequestVariableNames.HISTORY_BY);
        
        //if between dates
        if(RequestVariableNames.HISTORY_BETWEEN_DATES.equals(histByStr)){
            EnvHistoryUtil.getBetweenDatesFromRequest(request, beginDate, endDate);        	
            
        }else if(RequestVariableNames.HISTORY_BETWEEN_DATES_AND_TIME.equals(histByStr)){	    	
            getBetweenDatesAndTimeFromRequest(request, beginDate, endDate);
        }
        else{
            EnvHistoryUtil.getAgedDatesFromRequest(request, beginDate, endDate);        
        }                
    }

    private static void getBetweenDatesAndTimeFromRequest(HttpServletRequest request, Date beginDate, Date endDate){
        
        //get dates 
        EnvHistoryUtil.getBetweenDatesFromRequest(request, beginDate, endDate);
        
        //set hours, seconds and milliseconds
        String bHours = request.getParameter(RequestVariableNames.BEGIN_HOUR);
        String eHours = request.getParameter(RequestVariableNames.END_HOUR);
        
        setTime(beginDate, Integer.valueOf(bHours), 0, 0);
        setTime(endDate, Integer.valueOf(eHours), 0, 0);		
    }
    
    private static void setTime(Date date, int hours, int minutes, int seconds){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);
        date.setTime(cal.getTimeInMillis());
    }
    /**
     * 
     * @param request
     * @param beginDate - will be set as retrieved begin date
     * @param endDate - will be set as retrieved end date
     */
    public static void getAgedDatesFromRequest(HttpServletRequest request, Date beginDate, Date endDate){
        String agedBy = request.getParameter("agedBy");
        GregorianCalendar gc = new GregorianCalendar();
        
        //Set end date as current time 
        endDate.setTime(gc.getTimeInMillis());		
        
        //Roll calendar
        if(agedBy != null){
        if (RequestVariableNames.AGED_BY_HOUR.equals(agedBy)) {
            gc.add(Calendar.HOUR_OF_DAY, -1);
        }else if(RequestVariableNames.AGED_BY_DAY.equals(agedBy)){
            gc.add(Calendar.DAY_OF_YEAR, -1);
        }else if(RequestVariableNames.AGED_BY_MONTH.equals(agedBy)){
            gc.add(Calendar.MONTH, -1);
        }else if (RequestVariableNames.AGED_BY_WEEK.equals(agedBy)){
            gc.add(Calendar.WEEK_OF_YEAR, -1);
        }
        }else{
            String agedDays = request.getParameter("agedDays");
            String agedHours = request.getParameter("agedHours");
            String agedMins = request.getParameter("agedMins");
            String agedSecs = request.getParameter("agedSecs");
    
            gc.add(Calendar.DAY_OF_YEAR, Integer.parseInt(agedDays)*(-1));
            gc.add(Calendar.HOUR_OF_DAY, Integer.parseInt(agedHours)*(-1));
            gc.add(Calendar.MINUTE, Integer.parseInt(agedMins)*(-1));
            gc.add(Calendar.SECOND, Integer.parseInt(agedSecs)*(-1));			
        }
        
        //Set begin date as rolled time
        beginDate.setTime(gc.getTimeInMillis());
    }

    /**
     * 
     * @param request
     * @param beginDate - will be set as retrieved begin date
     * @param endDate - will be set as retrieved end date
     */
    public static void getBetweenDatesFromRequest(HttpServletRequest request, Date beginDate, Date endDate){
        String beginDateStr = request.getParameter(RequestVariableNames.BEGIN);
        String endDateStr = request.getParameter(RequestVariableNames.END);
        
        Date bDate;
        Date eDate;
        
        //add hours, minutes and seconds to the dates
        if (beginDateStr.indexOf(" ") == -1) {//doesn't contain time
            bDate = CommonFunctions.convertStringToDate(beginDateStr, "yyyy-MM-dd",UsersDataOptions.getTzIdFromRequest(request));
            eDate = CommonFunctions.convertStringToDate(endDateStr, "yyyy-MM-dd",UsersDataOptions.getTzIdFromRequest(request));

            eDate.setTime(eDate.getTime() + 24 * 60 * 60 * 1000 - 1);      	
        } else {
            bDate = CommonFunctions.convertStringToDate(beginDateStr, "yyyy-MM-dd HH:mm",UsersDataOptions.getTzIdFromRequest(request));
            eDate = CommonFunctions.convertStringToDate(endDateStr, "yyyy-MM-dd HH:mm",UsersDataOptions.getTzIdFromRequest(request));
        }
        
        beginDate.setTime(bDate.getTime());
        endDate.setTime(eDate.getTime());
    }

    /**
     * Gets historical point array.
     * @param object - object TO
     * @param propertyName - historical property name (if sensor, use "lastValue")
     * @param beginDate - begin date
     * @param endDate - end date 
     * @param env - environment
     * @return
     * @throws EnvException
     */
    public static ArrayList<SensorPoint> getPointsBetweenDates(TO<?> object, String propertyName, Date beginDate, Date endDate, Environment env)throws EnvException{
        ArrayList<SensorPoint> result = new ArrayList<SensorPoint>();								
        
        
        //check if property is link to a sensor
        ValueTO vto = env.getPropertyValue(object, propertyName, ValueTO.class);
        Object value = vto.getValue();
        if (value instanceof TO<?> && EnvironmentTypes.isSensorType(((TO<?>) value).getTypeName())) {
            object = (TO<?>) value;
            propertyName = "lastValue";
        }
        
        Collection<ValueTO> points = env.getHistory(object, propertyName, beginDate, endDate, Double.class);		
    
        for(ValueTO point : points){
            result.add(EnvHistoryUtil.convertHistToSensorPoint(point));
        }
        return result;		
    }

    /**
     * Gets historical point array.
     * @param object - object TO
     * @param propertyName - historical property name (if sensor, use "lastValue")
     * @param pointNumber - number of point
     * @param env - environment
     * @return
     * @throws EnvException
     */
    public static ArrayList<SensorPoint> getLastXPoints(TO<?> object, String propertyName, int pointNumber, Environment env)throws EnvException{
        ArrayList<SensorPoint> result = new ArrayList<SensorPoint>();				
        
        Collection<ValueTO> points = env.getHistory(object, propertyName, pointNumber, 0, Double.class);
    
        for(ValueTO point : points){
            result.add(EnvHistoryUtil.convertHistToSensorPoint(point));
        }
        return result;		
    }

    /**
     * Converts historical property to SensorPoint object
     * @param hist
     * @return
     */
    public static SensorPoint convertHistToSensorPoint(ValueTO hist){
        SensorPoint point = new SensorPoint();								
        point.time_stamp = hist.getTimeStamp();
        point.value = (Double)hist.getValue();
        return point;
    }

    /**
     * 
     * @param point
     * @return
     */
    private static boolean isPointExcluded(SensorPoint point){
        boolean result = false;
        Double value = point.value;
        
        if(value == null){
            result = true;
        }else{
            for(int i = 0; i < excludedValues.size(); i++){
                if(excludedValues.get(i).doubleValue() == value.doubleValue()){
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

}
