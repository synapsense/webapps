package com.synapsense.utilities;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.dataAccess.Env;
import com.synapsense.utilities.es.EnvironmentTypes;

public class MobileDeviceUtils {
    public static boolean isMobileDevicesPresent(HttpServletRequest request) {
        boolean result = !(Env.getEnv(request).getObjectsByType(EnvironmentTypes.MOBILE_DEVICE)).isEmpty();
        return result;
    }
}
