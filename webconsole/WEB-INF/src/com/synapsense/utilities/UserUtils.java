package com.synapsense.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.permissions.PermissionUtils;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.es.EnvironmentTypes;

public class UserUtils {

    public static String ROOT_PERMISSION = "root_permission";

    private static final Log LOG = LogFactory.getLog(UserUtils.class);

    public static void actionsOnUserLogon(HttpServletRequest request) {
        cleanUpPermissions(new RemovePermissionsCallback() {
            @Override
            public String execute(String permissions) {
                return PermissionUtils.validatePermissions(permissions);
            }

        }, request);
    }

    public static void actionsOnUserDeleting(final TO<?> user, HttpServletRequest request) {
        cleanUpPermissions(new RemovePermissionsCallback() {
            private Permissions updated;

            @Override
            public String execute(String permissions) {
                updated = new Permissions(permissions);
                return updated.removeUser(user) ? updated.toString() : null;
            }

        }, request);
    }

    public static void actionsOnGroupDeleting(final TO<?> group, HttpServletRequest request) {
        cleanUpPermissions(new RemovePermissionsCallback() {
            private Permissions updated;

            @Override
            public String execute(String permissions) {
                updated = new Permissions(permissions);
                return updated.removeGroup(group) ? updated.toString() : null;
            }

        }, request);
    }

    private interface RemovePermissionsCallback {
        String execute(String permissions);
    }

    private static void cleanUpPermissions(RemovePermissionsCallback removeCallback, HttpServletRequest request) {
        LOG.debug("cleanUpPermissions()");
        try {
            Environment env = Env.getEnv(request);
            Collection<TO<?>> to_col = env.getObjectsByType(EnvironmentTypes.TYPE_DC);
            to_col.addAll(env.getObjectsByType(EnvironmentTypes.TYPE_ROOM));

            for (TO<?> obj : to_col) {
                LOG.debug("Checking permissions on " + obj.getTypeName() + ":" + obj.getID());
                String permissions = env.getPropertyValue(obj, "permissions", String.class);
                LOG.debug("permissions = " + permissions);
                String updatedPermissions = null;
                
                if (permissions == null || permissions.isEmpty()) {
                    updatedPermissions = PermissionUtils.getPermissionsString(true, null, null);
                } else {
                    updatedPermissions = removeCallback.execute(permissions);
                }
                
                if (updatedPermissions != null) {
                    LOG.debug("updatedPermissions = " + updatedPermissions);
                    env.setPropertyValue(obj, "permissions", updatedPermissions);
                }
            }
        } catch (Exception e) {
            LOG.info("Failed to clean up references to user or user group (" + e.getLocalizedMessage() + ")");
        }
    }

    private static class Permissions {
        private boolean isEveryOneOptionSet;
        private final Collection<String> userNames;
        private final Collection<String> userGroups;

        private Permissions(String permissions) {
            isEveryOneOptionSet = PermissionUtils.isEveryonePermitted(permissions);
            userNames = PermissionUtils.getPermittedUsers(permissions);
            userGroups = PermissionUtils.getPermittedGroups(permissions);
        }

        public String toString() {
            return PermissionUtils.getPermissionsString(isEveryOneOptionSet, userNames, userGroups);
        }

        public boolean removeUser(TO<?> user) {
            String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
            boolean ret = userNames.remove(userName);
            if (!this.isEveryOneOptionSet && userNames.isEmpty() && userGroups.isEmpty()) {
                this.isEveryOneOptionSet = true;
                ret = true;
            }
            return ret;
        }

        public boolean removeGroup(TO<?> group) {
            String groupName = (String) Env.getUserDAO().getProperty(group, UserManagementService.GROUP_NAME);
            boolean ret = userGroups.remove(groupName);
            if (this.isEveryOneOptionSet && userNames.isEmpty() && userGroups.isEmpty()) {
                this.isEveryOneOptionSet = true;
                ret = true;
            }
            return ret;
        }
    }

    public static boolean isLastAdmin(TO<?> user) {
        if (user == null) {
            return false;
        }

        try {
            String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
            Collection<String> collAdmin = Env.getUserDAO().getGroupUserIds(getRootGroup());

            int adminCount = 0;
            for (String us : collAdmin) {
                adminCount++;
            }
            if (adminCount < 2 && collAdmin.contains(userName)) {
                return true;
            }
        } catch (UserManagementException e) {
            LOG.error("Failed to getGroupUsers for root group (" + e.getLocalizedMessage() + ")");
        }
        return false;
    }

    public static boolean isRootGroup(TO<?> group) {
        try {
            Collection<String> privs = Env.getUserDAO().getGroupPrivilegeIds(group);
            if (privs.contains(ROOT_PERMISSION))
                return true;
        } catch (UserManagementException e) {
            LOG.error("Failed to getGroupPrivilegeIds groupName=" + group.getID() + "(" + e.getLocalizedMessage()
                    + ")");
        }
        return false;
    }

    public static TO<?> getRootGroup() {
        Collection<TO<?>> grs = Env.getUserDAO().getAllGroups();
        for (TO<?> gr : grs) {
            if (isRootGroup(gr)) {
                return gr;
            }
        }
        return null;
    }

    public static boolean hasPrivilege(TO<?> user, String privilegeName) {
        Collection<String> privs = new ArrayList<String>();
        try {
            privs = Env.getUserDAO().getUserPrivilegeIds(user);

            if (privs.isEmpty())
                return false;

            if (privs.contains(ROOT_PERMISSION))
                return true;

            return privs.contains(privilegeName);
        } catch (UserManagementException e) {
        }

        return false;
    }

    public static boolean hasRootPrivilege(TO<?> user) {
        return hasPrivilege(user, ROOT_PERMISSION);
    }

    public static boolean hasPrivileges(TO<?> user, Map<String, String[]> privilegeNames) {
        boolean res = false;
        Collection<String> privs = new ArrayList<String>();
        try {
            privs = Env.getUserDAO().getUserPrivilegeIds(user);

            if (privs.isEmpty())
                return false;
            if (privs.contains(ROOT_PERMISSION))
                return true;

            String privNames[] = {};
            for (String op : privilegeNames.keySet()) {
                privNames = privilegeNames.get(op);
            }

            if (privilegeNames.containsKey("or")) {
                for (String priv : privNames) {
                    if (privs.contains(priv)) {
                        res = true;
                        break;
                    }
                }
            } else {
                res = true;
                for (String priv : privNames) {
                    if (!privs.contains(priv)) {
                        res = false;
                        break;
                    }
                }
            }
        } catch (UserManagementException e) {
            LOG.debug("Failed to getUserPrivilegeIds userID=" + user.getID() + " (" + e.getLocalizedMessage() + ")");
        }
        return res;
    }

    public static Collection<String> getUserGroupNames(TO<?> user) {
        try {
            return Env.getUserDAO().getUserGroupIds(user);
        } catch (UserManagementException e) {
            LOG.error(e.getMessage(), e);
        }
        return Collections.emptyList();
    }

    @SuppressWarnings("unused")
    private static void removeNotExistingUsersFromPermissions(HttpServletRequest request) {
        try {
            Environment env = Env.getEnv(request);
            Collection<TO<?>> to_col = env.getObjectsByType(EnvironmentTypes.TYPE_DC);
            to_col.addAll(env.getObjectsByType(EnvironmentTypes.TYPE_ROOM));

            for (TO<?> obj : to_col) {
                String permittedUsersStr = env.getPropertyValue(obj, "permissions", String.class);
                if (permittedUsersStr != null) {
                    boolean requestUpdatePermissions = false;
                    ArrayList<String> permittedUsersNames = new ArrayList<String>(Arrays.asList(permittedUsersStr
                            .split(",")));
                    if (permittedUsersNames.isEmpty()) {
                        continue;
                    }

                    ArrayList<String> namesToRemove = new ArrayList<String>();
                    for (String permittedUser : permittedUsersNames) {
                        if (!permittedUser.equals(Constants.EVERYONE_ID)) {
                            TO<?> user = Env.getUserDAO().getUser(permittedUser);
                            if (user == null) {
                                namesToRemove.add(permittedUser);
                            }
                        }
                    }

                    for (String nameToRemove : namesToRemove) {
                        while (permittedUsersNames.contains(nameToRemove)) {
                            permittedUsersNames.remove(permittedUsersNames.indexOf(nameToRemove));
                            requestUpdatePermissions = true;
                        }
                    }

                    if (requestUpdatePermissions == true) {
                        env.setPropertyValue(obj, "permissions", StringUtils.join(permittedUsersNames, ","));
                    }
                }
            }
        } catch (Exception e) {
            LOG.info("Failed to removeNotExistingUsersFromPermissions (" + e.getLocalizedMessage() + ")");
        }
    }
}
