package com.synapsense.utilities;

import java.util.Arrays;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Vector;

public abstract class ProgressTracker {

    protected byte percent = 100;
    protected String message = "";
    protected String result = "";
    protected boolean completed = true;
    protected Vector<Object> callStack = new Vector<Object>();
    
    private static ThreadLocal<ProgressTracker> instance = new ThreadLocal<ProgressTracker>();
    
    public static synchronized ProgressTracker getInstance() {
        ProgressTracker pt = instance.get();
        if (pt == null) {
            pt = new ProgressTrackerDummy();
        }
        return pt;
    }
    
    public static synchronized void setInstance(ProgressTracker instance) {
        ProgressTracker.instance.set(instance);
    }
    
    public static synchronized ProgressTracker createNew () {
        return new ProgressTrackerImpl();
    }
    
    public abstract void start(Object who);
    public abstract void stop(Object who);
    public abstract void reset ();
    public abstract void updateProgress(byte percent, String...args);
    public abstract byte getPercent();
    public abstract String getMessage();
    public abstract byte[] getResultBytes ();
    public abstract String getResultString ();
    public abstract boolean isCompleted();
    
    static private class ProgressTrackerImpl extends ProgressTracker {
        public synchronized void start(Object who) {
            callStack.add(who);
            this.percent = 0;
            this.message = "";
            this.completed = false;
        }
        
        public synchronized void stop(Object who) {
            int index = callStack.lastIndexOf(who);
            if (index != -1) {
                ListIterator<Object> it = callStack.listIterator(index);
                while (it.hasNext()) {
                    it.next();
                    it.remove();
                }
            }
            if (callStack.isEmpty()) {
                this.completed = true;
            }
        }
        
        public synchronized void reset () {
            callStack.removeAllElements();
        }
        
        public synchronized void updateProgress(byte percent, String...args) {
            this.percent = percent;
            Iterator<String> it = Arrays.asList(args).iterator();
            if (it.hasNext()) {
                this.message = it.next();
            } if (it.hasNext()) {
                this.result = it.next();
            }
        }
        
        public synchronized byte[] getResultBytes () {
            return result.getBytes();
        }
        public synchronized String getResultString () {
            return result;
        }
        
        public synchronized byte getPercent() {
            return percent;
        }

        public synchronized String getMessage() {
            return message;
        }

        public synchronized boolean isCompleted() {
            return completed;
        }
    }
    
    static private class ProgressTrackerDummy extends ProgressTracker {
        public void start(Object who){}
        public void stop(Object who){}
        public void reset (){}
        public void updateProgress(byte percent, String...args){}
        public byte[] getResultBytes () {return new byte[]{};}
        public String getResultString () {return result;}
        public byte getPercent(){return percent;}
        public String getMessage(){return message;}
        public boolean isCompleted(){return completed;}
    }
}
