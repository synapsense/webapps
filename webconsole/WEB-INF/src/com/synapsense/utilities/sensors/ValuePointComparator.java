package com.synapsense.utilities.sensors;

public class ValuePointComparator extends PointComparator {

    @Override
    public int compare(Object o1, Object o2) {
                
        if(o1 instanceof SensorPoint && o2 instanceof SensorPoint){
            SensorPoint sp1 = (SensorPoint)o1;
            SensorPoint sp2 = (SensorPoint)o2;
            
            if(sp1.value > sp2.value){
                return 1;
            }else if (sp1.value < sp2.value){
                return -1;
            }else{
                return 0;
            }
        }
        
        return -1;
    }

}
