package com.synapsense.utilities.sensors;

public class SensorPoint{
    public SensorPoint(){};
    
    public SensorPoint(double Value, long timeStamp){
        value = Value;
        time_stamp = timeStamp;
    };
    
    public double value = 0.0;
    public long time_stamp = 0L;
};