package com.synapsense.utilities.sensors;

import java.util.Comparator;

public abstract class PointComparator implements Comparator<Object> {

    boolean isASC = true;
    abstract public int compare(Object o1, Object o2);
    
    public PointComparator(){		
    }
    public PointComparator(boolean is_ASC){
        isASC = is_ASC;
    }
}
