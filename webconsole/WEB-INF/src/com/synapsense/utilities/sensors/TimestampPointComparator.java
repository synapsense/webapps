package com.synapsense.utilities.sensors;

public class TimestampPointComparator extends PointComparator {

    public TimestampPointComparator(){		
    }
    public TimestampPointComparator(boolean is_ASC){
        super(is_ASC);
    }
    
    @Override
    public int compare(Object o1, Object o2) {
        
        //comment type checking to increase sorting performance ..
        
        //if(o1 instanceof SensorPoint && o2 instanceof SensorPoint){
            SensorPoint sp1 = (SensorPoint)o1;
            SensorPoint sp2 = (SensorPoint)o2;
            
            int res = 0;
            if(sp1.time_stamp > sp2.time_stamp){
                res = 1;
            }else if(sp1.time_stamp < sp2.time_stamp){
                res = -1;
            }
            
            if(!isASC){
                res = -res;
            }
            return res;
            /*if(sp1.time_stamp.after(sp2.time_stamp)){
                return 1;
            }else if (sp1.time_stamp.before(sp2.time_stamp)){
                return -1;
            }else{
                return 0;
            }*/
        //}
        
        //return -1;
    }

}
