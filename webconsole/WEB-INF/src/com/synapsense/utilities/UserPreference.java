package com.synapsense.utilities;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;

public class UserPreference {
    
    public static final String TIME_ZONE = "TimeZone"; 

    private static final Log LOG = LogFactory.getLog(UserPreference.class);
    
    //prefName->setting name in standalone.xml
    private static Map<String, String> defaultPrefs = new HashMap<String, String>();
    private static List<String> notStringTypePrefs = new ArrayList<String>(5);
    
    static {
        defaultPrefs.put("SavedLanguage", "com.synapsense.defaultLanguage");
        defaultPrefs.put("UnitsSystem", "com.synapsense.defaultUnitSystem");
        defaultPrefs.put("DateFormat", "com.synapsense.defaultDateFormat");
        defaultPrefs.put("DecimalSeparator", "com.synapsense.defaultDecimalSeparator");
        defaultPrefs.put("DecimalFormat", "com.synapsense.defaultDecimalFormat");
        
        notStringTypePrefs.add("DecimalFormat");
    }


    public static void set(TO<?> user, String prefName, String prefValue, UserManagementService userDAO) {
        try {
            
            userDAO.setUserPreferenceValue(user, prefName, prefValue);
        } catch (UserManagementException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    public static void removePreferenceWithValue(String prefName, String prefValue, UserManagementService userDAO) {
        removePreference(prefName,prefValue,userDAO);
    }

    public static void removePreference(String prefName, UserManagementService userDAO) {
        removePreference(prefName,null,userDAO);
    }

    
    private static void removePreference(String prefName, String prefValue, UserManagementService userDAO) {
        Collection<TO<?>> users = userDAO.getAllUsers();
        String qPrefValue = "\"" + prefValue + "\"";
        for (TO<?> user : users) {
            Map<String, Object> prefs = userDAO.getUserPreferences(user);
            for (Map.Entry<String, Object> pref : prefs.entrySet()) {
                Object currValue = pref.getValue();
                currValue = currValue != null ? currValue.toString() : "";
                if (pref.getKey().equals(prefName)
                        &&  ((prefValue == null) || ((prefValue != null) && (prefValue.equals(currValue) || qPrefValue.equals(currValue))))) {
                    try {
                        userDAO.removeUserPreference(user, prefName);
                    } catch (UserManagementException e) {
                        LOG.error(e.getLocalizedMessage(), e);
                    }
                }
            }
        }
    }

    public static String get(TO<?> user, String prefName, UserManagementService userDAO) {
        Map<String, Object> prefs = userDAO.getUserPreferences(user);
        for (Map.Entry<String, Object> pref : prefs.entrySet()) {
            if (pref.getKey().equals(prefName)) {
                Object objValue = pref.getValue();
                return objValue != null ? objValue.toString() : null;
            }
        }
        return null;
    }
    
    public static void removeAll(TO<?> user, UserManagementService userDAO) {
        try {
            Map<String, Object> prefs = userDAO.getUserPreferences(user);
            for (String pref : prefs.keySet()) {
                userDAO.removeUserPreference(user, pref);
            }
        } catch (UserManagementException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        updateDefaultPrefs(user, userDAO);
    }
    
    /**
     * Since we store json objects in the user preferences all strings are quoted.
     * This method returns unquoted string.  
     * 
     * @param userName
     * @param prefName
     * @param userDAO
     * @return
     */
    public static String getString(TO<?> user, String prefName, UserManagementService userDAO) {
        String str = UserPreference.get(user, prefName, userDAO);
        
        if (str != null && str.startsWith("\"") && str.endsWith("\"") && str.length() > 1) {
            str = str.substring(1, str.length() - 1);
        }
        
        return str;
    }
    
    public static void updateDefaultPrefs(TO<?> user, UserManagementService userDAO) {
        for (Map.Entry<String, String> entry : defaultPrefs.entrySet()) {
            String prefName = entry.getKey();
            String prefValue = UserPreference.get(user, prefName, userDAO);
            if (prefValue == null) {
                //Default values are defined in standalone.xml and are loaded 
                //into system properties by Registrar service
                prefValue = System.getProperty(entry.getValue());
                if (prefValue != null) {
                    if(!notStringTypePrefs.contains(prefName)){
                        prefValue = "\"" + prefValue + "\"";
                    }
                    UserPreference.set(user, prefName, prefValue, userDAO);
                }
            }
            
        }
    }
    
    public static Document getPrefsDocument(TO<?> user, UserManagementService userDAO) {
        Document doc = null;
        
        try {
            doc = XMLHelper.createNewXmlDocument(UserManagementService.USER_PREFERENCES);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return doc;
        }

        Map<String, Object> prefs = userDAO.getUserPreferences(user);

        Element root = doc.getDocumentElement();
        
        for (Map.Entry<String, Object> pref : prefs.entrySet()) {
            String name = pref.getKey();
            Object value = pref.getValue();
            value = value != null ? value.toString() : "";
            Node prefNode = XMLHelper.createXmlTextNode(doc, "pref", (String) value);
            ((Element)prefNode).setAttribute("name", name);
            root.appendChild(prefNode);
        }
        
        return doc;
        
    }

}
