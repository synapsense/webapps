package com.synapsense.utilities;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Collection;
import java.util.StringTokenizer;

public class StringUtils {
    public static String join(Collection<String> strings, String delimiter) {
        if (strings.isEmpty()) {
            return "";
        }
        
        StringBuffer sb = new StringBuffer();
        for (String string : strings) {
            sb.append(string).append(delimiter);
        }
        return sb.substring(0, sb.length() - delimiter.length());
    }

    public static String HTMLEntityEncode( String s ) {
        StringBuffer buf = new StringBuffer();
        int len = (s == null ? -1 : s.length());
        for ( int i = 0; i < len; i++ ) {
            char c = s.charAt( i );
            if ( c>='a' && c<='z' || c>='A' && c<='Z' || c>='0' && c<='9' ) {
                buf.append( c );
            } else {
                buf.append( "&#" + (int)c + ";" );
            }
        }
        return buf.toString();
    }

    public static  String quoteTokens(String str) {
        StringTokenizer st = new StringTokenizer(str, ",");
        StringBuffer sb = new StringBuffer();
        while (st.hasMoreTokens()) {
            sb.append("\"" + st.nextToken() + "\",");
        }
    
        return sb.substring(0, sb.length() - 1);
    }

    public static String readFileAsString(String filePath)
            throws java.io.IOException {
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            fileData.append(buf, 0, numRead);
        }
        reader.close();
        return fileData.toString();
    }
}
