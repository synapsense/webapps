/**
 * 
 */
package com.synapsense.utilities;

/**
 * @author dinniss
 *
 */
public class ErrorMessages {
    public static final String SUCCESS						= "1|message/SUCCESS";
    public static final String UNABLE_TO_COMPLETE			= "0|message/UNABLE_TO_COMPLETE";
    public static final String OBJECT_NOT_FOUND				= "-1|message/OBJECT_NOT_FOUND";
    public static final String NO_ANIMATION_FOR_ALERT		= "-1|message/NO_ANIMATION_FOR_ALERT";
    public static final String SESSION_EXPIRED				= "-2|message/SESSION_EXPIRED";
    public static final String OBJECT_ASSIGNED				= "1|message/OBJECT_ASSIGNED";
    public static final String CANT_DELETE_CURRENT_USER     = "-3|message/CANT_DELETE_CURRENT_USER";
    public static final String CANT_DELETE_CURRENT_GROUP_STILL_USED     = "-3|message/CANT_DELETE_CURRENT_GROUP_STILL_USED";
    public static final String CANT_DELETE_LAST_ADMIN	    = "-3|message/CANT_DELETE_LAST_ADMINISTRATOR";
    public static final String CANT_REMOVE_LAST_ADMIN_FROM_ROOT_GROUP	= "-3|message/CANT_REMOVE_LAST_ADMIN_FROM_ROOT_GROUP";
    public static final String CANT_DELETE_SEMS_USERS_AND_LAST_ADMIN    = "-3|message/CANT_DELETE_SEMS_USERS_AND_LAST_ADMINISTRATOR";
    public static final String DUPLICATE_CAROUSEL_NAME 		= "-2|message/DUPLICATE_CAROUSEL_NAME";
    public static final String DUPLICATE_DASHBOARD_NAME 	= "-2|message/DUPLICATE_DASHBOARD_NAME";
    public static final String DUPLICATE_NAME 				= "-2|message/DUPLICATE_NAME";
    public static final String DUPLICATE_TEMPLATE_NAME 		= "-2|message/DUPLICATE_TEMPLATE_NAME";
    public static final String DUPLICATE_GROUP_NAME 		= "-2|message/DUPLICATE_GROUP_NAME";
    public static final String DUPLICATE_USER_NAME 			= "-2|message/DUPLICATE_USER_NAME";
    public static final String RESERVED_USER_NAME           = "-2|message/RESERVED_USER_NAME";

    public static final String DUPLICATE_CUSTOM_QUERY_NAME 	= "-2|message/DUPLICATE_CUSTOM_QUERY_NAME";
    public static final String DUPLICATE_ALERT_NAME 		= "-2|message/DUPLICATE_ALERT_NAME";
    public static final String ALERT_TYPE_ACTIVATED			= "1|message/ALERT_TYPE_ACTIVATED";
    public static final String ALERT_TYPE_DEACTIVATED		= "1|message/ALERT_TYPE_DEACTIVATED";
    public static final String USER_DELETED					= "1|message/USER_DELETED";
    public static final String USER_ADDED					= "1|message/USER_ADDED";
    public static final String USER_UPDATED					= "1|message/USER_UPDATED";
    public static final String UPDATE_ALERT_PRIORITY_INTERVAL_FAIL		= "0|message/UPDATE_ALERT_PRIORITY_INTERVAL_FAIL";
    public static final String UPDATED_ALERT_PRIORITY_INTERVAL_SUCCESS	= "1|message/UPDATED_ALERT_PRIORITY_INTERVAL_SUCCESS";
    public static final String UNKNOWN_REQUEST				= "-1|message/UNKNOWN_REQUEST";
    public static final String LI_NO_IMAGE_TO_DISPLAY_ERROR_CODE = "0|message/LI_NO_IMAGE_TO_DISPLAY_ERROR_CODE";
    public static final String LI_SERVER_NOT_AVAILABLE_CODE = "-1|message/LI_SERVER_NOT_AVAILABLE_CODE";
    public static final String LI_GENERATION_FAILED_ERROR_CODE = "-2|message/LI_GENERATION_FAILED_ERROR_CODE";
    public static final String UNABLE_TO_SET_PROPERTY       = "-1|message/UNABLE_TO_SET_PROPERTY";
    public static final String LEGENDS_UPDATED				= "1|message/LEGENDS_UPDATED";
    public static final String UPDATE_LEGENDS_FAILED        = "0|message/UPDATE_LEGENDS_FAILED";
    public static final String UNABLE_CHANGE_LAST_ADMIN        = "0|message/UNABLE_CHANGE_LAST_ADMIN";
    public static final String ERROR_OCCURRED_WHILE_SENDING	= "0|ESConfigurator/ID_ERROR_OCCURRED_WHILE_SENDING_MESSAGE";
    public static final String ERROR_OCCURRED_WHILE_SENDING_TRAP	= "0|ESConfigurator/ID_ERROR_OCCURRED_WHILE_SENDING_TRAP_MESSAGE";
    public static final String TRAP_WAS_SENT_TO				= "1|ESConfigurator/ID_TRAP_WAS_SENT_TO_MESSAGE";
    public static final String WRONG_PARAMETER				= "-1|message/WRONG_REQUEST_PARAMETER";
    public static final String WRONG_REMOTE_IP_ADDRESS		= "-1|ESConfigurator/ID_WRONG_REMOTE_IP_ADDRESS_MESSAGE";
    public static final String SOME_RACKS_CAN_NOT_BE_CONFIGURED		= "-4|message/SOME_RACKS_CAN_NOT_BE_CONFIGURED";
    public static final String SUCESS_WITH_RESULT			= "-5|message/SUCCESS|";
    public static final String ALERT_4E_TEST_FAIL			= "1|ESConfigurator/ID_ALERT_4E_TEST_FAIL_MESSAGE";
    public static final String ALERT_4E_TEST_SUCCESS	    = "0|ESConfigurator/ID_ALERT_4E_TEST_SUCCESS_MESSAGE";
    public static final String SOME_CHANGES_NOT_APPLIED		= "0|message/SOME_CHANGES_NOT_APPLIED";
    public static final String SOME_COINS_COULD_NOT_BE_ENABLED	= "0|message/SOME_COINS_COULD_NOT_BE_ENABLED";
    public static final String UNABLE_TO_SAVE_DASHBOARD 	= "-2|message/UNABLE_TO_SAVE_DASHBOARD";
    public static final String CONTROL_OBJECT_CANNOT_BE_DISABLED = "-2|message/CONTROL_OBJECT_CANNOT_BE_DISABLED";
    
    public static String getText(String errorMessage){
        return errorMessage.substring(errorMessage.indexOf("|") + 1, errorMessage.length());
    }
}
