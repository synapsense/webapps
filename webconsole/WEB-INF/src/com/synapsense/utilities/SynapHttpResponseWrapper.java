package com.synapsense.utilities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;

/**
 * @author aklushin
 * */
public class SynapHttpResponseWrapper extends HttpServletResponseWrapper {
    protected ByteArrayOutputStream out = new ByteArrayOutputStream();
    protected PrintWriter writer = new PrintWriter(out);

    public SynapHttpResponseWrapper (HttpServletResponse response) {
        super(response);
    }

    /*
     * @Override public void setContentType(String type) {
     * super.setContentType(type); }
     */

    @Override
    public PrintWriter getWriter() throws IOException {
        return writer = new PrintWriter(new OutputStreamWriter(out,
                getCharacterEncoding()));
    }

    @Override
    public ServletOutputStream getOutputStream() {
        return new ServletOutputStream() {
            @Override
            public void write(int arg0) throws IOException {
                out.write(arg0);
            }
        };
    }

    @Override
    public void setBufferSize(int size) {
        // Actually do nothing/
    }

    @Override
    public int getBufferSize() {
        return out.size();
    }

    @Override
    public void flushBuffer() throws java.io.IOException {
        writer.flush();
        out.flush();
    }

    @Override
    public void resetBuffer() {
        out.reset();
    }

    @Override
    public void reset() {
        super.reset();
        resetBuffer();
    }
}