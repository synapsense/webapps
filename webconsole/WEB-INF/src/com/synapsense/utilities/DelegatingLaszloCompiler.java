package com.synapsense.utilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DelegatingLaszloCompiler extends HttpServlet {	
    private static final long serialVersionUID = -8177915340910562827L;
    private static final Log LOG = LogFactory.getLog(DelegatingLaszloCompiler.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.trace(this.getServletName() + " is started ...");
        
        String path = req.getRequestURI();
        this.getServletContext().getContext("/openlaszlo").getRequestDispatcher(path).forward(req, resp);
        
        LOG.trace(this.getServletName() + " is finished ...");
    }		
}
