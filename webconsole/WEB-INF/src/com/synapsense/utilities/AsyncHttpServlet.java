package com.synapsense.utilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author aklushin
 * */
public class AsyncHttpServlet extends BaseHttpServlet {
    private static final long serialVersionUID = -650247104424233091L;

    protected ThreadLocal<Boolean> asyncRequest = new ThreadLocal<Boolean>();
    
    @Override
    public void sendResponse(HttpServletResponse response, String str)
            throws IOException, ServletException {
        if (str != null) {
            prepareResponse(response);
            if (asyncRequest.get()) {
                ProgressTracker pt = ProgressTracker.getInstance();
                pt.updateProgress(pt.getPercent(), pt.getMessage(), str);
                pt.stop(this);
                ProgressTracker.setInstance(null);
            }
            super.sendResponse(response, str);
        }
    }

    @Override
    public void sendResponse(HttpServletResponse response, byte[] bytes)
            throws IOException, ServletException {
        if (asyncRequest.get()) {
            prepareResponse(response);
            ProgressTracker pt = ProgressTracker.getInstance();
            pt.updateProgress(pt.getPercent(), pt.getMessage(), new String(bytes));
            pt.stop(this);
            ProgressTracker.setInstance(null);
        }
        super.sendResponse(response, bytes);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doPost(request, response);
    }

    /**
     * This method must be called first in subclass's doPost method.
     * */
    @Override
    protected synchronized void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession(false);
        synchronized (session) {
            boolean asyncR = false;
            String asyncRStr = request.getParameter(RequestVariableNames.ASYNC_REQUEST);
            asyncR = asyncRStr != null && request.getParameter(RequestVariableNames.ASYNC_REQUEST).equals("true");
            String requestID = request.getParameter(RequestVariableNames.REQUEST_ID);
            asyncRequest.set(asyncR);
            if (!asyncR) {
                return;
            }
            ProgressTracker pt = ProgressTracker.createNew();
            ProgressTracker.setInstance(pt);
            
            session.setAttribute(requestID, pt);
            
            pt.start(this);
        }
    }
}
