package com.synapsense.utilities.matcher;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.synapsense.utilities.StringUtils;

/**
 * Simple wrapper of regular expressions for console wildcards using.
 * @author ahorosh
 *
 */
public class ConsoleMatcher {

    public ConsoleMatcher(){	
    }
    
    /**
     * Checks pattern matching.
     * @param str - string which will be checked
     * @param consolePattern - console pattern
     * @return
     */
    public static boolean patternMatches(String str, String consolePattern){
        boolean result = false;		
        String regExpPattern = convertToRegExpPattern(consolePattern);		
        Pattern p = Pattern.compile(regExpPattern, Pattern.CASE_INSENSITIVE);		
        Matcher m = p.matcher(str);		
        result = m.matches();		
        return result;
    }
    
    /**
     * Converts console pattern (pattern with console wildcards) to the regular
     * expression pattern. Returns converted pattern.
     * 
     * This method is close of synapgridfilter.preprocessRegExp() defined in synapgridfilter.lzx
     * 
     * @param consolePattern
     * @return 
     */
    private static String convertToRegExpPattern(String consolePattern){
        String res = consolePattern;
        //escape all meta-charactes except "*" and "?" with "\", presevre "\*" and "\?"
        String[] metaCharacters = {"*", "?", "\\", "^", "$", ".", "+", "(",  ")", "[", "]", "{", "}", "|"};
        String[] t1 = res.split(Pattern.quote("\\*"));
        boolean hasQuestion = false;
        for (int j = 0; j < t1.length; j++) {
            String[] t2 = t1[j].split(Pattern.quote("\\?"));
            for (int k = 0; k < t2.length; k++){
                for (int i = 2; i < metaCharacters.length; i++) {
                    String mChar = metaCharacters[i];
                    t2[k] = t2[k].replaceAll(Pattern.quote(mChar), "\\" + mChar);
                                 
                }
                if (t2[k].indexOf("?") != -1) {
                    hasQuestion = true;
                }
                t2[k] = t2[k].replaceAll(Pattern.quote("*"), ".*").replaceAll(Pattern.quote("?"), ".");
            }
            t1[j] = StringUtils.join(Arrays.asList(t2),"\\?");
        }
        res = StringUtils.join(Arrays.asList(t1),"\\*");
        
        if (hasQuestion) {
            //use strict match
            res = "^" + res + "$";
        } else {
            res = ".*" + res + ".*";
        }
        
        return res;
    }
}
