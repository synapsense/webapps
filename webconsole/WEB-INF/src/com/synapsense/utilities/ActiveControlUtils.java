package com.synapsense.utilities;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.synapsense.utilities.es.EnvironmentTypes;

public class ActiveControlUtils {
    private static final Set<String> coinEnvironmentParentTypes = new HashSet<String>();

    static {
        coinEnvironmentParentTypes.add(EnvironmentTypes.TYPE_RACK);
        coinEnvironmentParentTypes.add(EnvironmentTypes.TYPE_PRESSURE);
        coinEnvironmentParentTypes.add(EnvironmentTypes.TYPE_GENERICTEMPERATURE);
        coinEnvironmentParentTypes.add(EnvironmentTypes.TYPE_VERTICALTEMPERATURE);
    }
    
    public static Set<String> getCoinEnvParentTypes() {
        return Collections.unmodifiableSet(coinEnvironmentParentTypes);
    }
}
