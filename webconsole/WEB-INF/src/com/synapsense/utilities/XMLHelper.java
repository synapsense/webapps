package com.synapsense.utilities;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.ProcessingInstruction;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLHelper {
    private static XPathFactory xpathFactory;
    private static XPath xpathObj;
    private static TransformerFactory transformerFactory;
    private static StringWriter stringWriter;
    private static StreamResult streamResult;
    private static DOMSource domSource;

    static {
        xpathFactory = XPathFactory.newInstance();
        xpathObj = xpathFactory.newXPath();
        transformerFactory = TransformerFactory.newInstance();
        stringWriter = new StringWriter();
        streamResult = new StreamResult(stringWriter);
        domSource = new DOMSource();
    }
    
    public static void disableEscapingForNodeWithText(Node nodeWithTextChild) {
        ArrayList<Node> nodes = new ArrayList<Node>();
        NodeList nodeList = nodeWithTextChild.getChildNodes();
        for(int i = 0; i < nodeList.getLength(); i++) {
            nodes.add(nodeList.item(i));
        }
        disableEscapingForTextNodes(nodes);
    }
    
    public static void disableEscapingForTextNodes(List<Node> textTypeNodes) {
        Document doc = null;
        for(Node node : textTypeNodes) {
            doc = node.getOwnerDocument();
            if(node.getNodeType() == Node.TEXT_NODE && node.getNodeValue().indexOf("\n") != -1) {
                String value = node.getNodeValue();
                Node parent = node.getParentNode();
                parent.removeChild(node);
                ProcessingInstruction pi = doc.createProcessingInstruction(Result.PI_DISABLE_OUTPUT_ESCAPING, "");
                parent.appendChild(pi);
                parent.appendChild(doc.createCDATASection(value));
                pi = doc.createProcessingInstruction(Result.PI_ENABLE_OUTPUT_ESCAPING, "");
                parent.appendChild(pi);
            } else if(node.getNodeType() == Node.CDATA_SECTION_NODE && node.getNodeValue().indexOf("\n") != -1) {				
                node.getParentNode().insertBefore(doc.createProcessingInstruction(Result.PI_DISABLE_OUTPUT_ESCAPING, ""), node);
                node.getParentNode().appendChild(doc.createProcessingInstruction(Result.PI_ENABLE_OUTPUT_ESCAPING, ""));
            }
        }
    }
    
    public static Node appendNodeWithText(Node parentNode, String childName, String childText) {
        childText = childText == null ? "" : childText;
        Document doc = parentNode.getOwnerDocument();
        Text textNode = doc.createTextNode(childText);
        Node newNode = doc.createElement(childName);
        newNode.appendChild(textNode);
        parentNode.appendChild(newNode);
        disableEscapingForNodeWithText(newNode);
        return newNode;
    }
    
    public static void setNodeText(Node node, String text) {
        try {
            NodeList nodeList = node.getChildNodes();
            for(int i = 0; i < nodeList.getLength(); i++) {
                Node itemnode = nodeList.item(i);
                if(itemnode.getNodeType() == Element.TEXT_NODE) {
                    itemnode.setNodeValue(text);
                    disableEscapingForNodeWithText(itemnode.getParentNode());
                    return;
                }
            }
            Text textNode = (Text)node.getOwnerDocument().createTextNode(text);
            node.appendChild(textNode);
            disableEscapingForNodeWithText(node);
        } catch(Exception e){
            CommonFunctions.log.error("setNodeText()", e);
        }
    }
    
    public static List<Node> parseAndAppend(Node parentNode, String xmlString) {
        List<Node> parcedNodes = new ArrayList<Node>();
        xmlString = "<append>" + xmlString + "</append>";
        Document srcDoc = getDocument(xmlString);
        Document destDoc = parentNode.getOwnerDocument();
        List<Node> srcNodes = xpathQueryNodeList(srcDoc, "/append/*");
        for(Node srcNode : srcNodes) {
            Node inode = destDoc.importNode(srcNode, true);
            parentNode.appendChild(inode);
            parcedNodes.add(inode);
        }
        disableEscapingForTextNodes(xpathQueryNodeList(parentNode, "//*/text()"));
        return parcedNodes;
    }
    
    public static void removeNodeList(List<Node> nodes) {
        for(Node node : nodes) {
            removeNode(node);
        }
    }
    
    public static void removeNode(Node node) {
        node.getParentNode().removeChild(node);
    }
    
    public static void setNodeAttribute(Node node, String attrName, String attrValue) {
        try{			
            Element elem = (Element)node;
            elem.setAttribute(attrName, attrValue);
        } catch(Exception e){
            CommonFunctions.log.error("setNodeAttribute()", e);
        }
    }
    
    public static String nodeToString(Node node, int indent) {
        String xmlString = "";
        try {
            transformerFactory.setAttribute("indent-number", new Integer(indent));
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty(OutputKeys.METHOD, "xml");       
            domSource.setNode(node);
            stringWriter.getBuffer().setLength(0);
            transformer.transform(domSource, streamResult);
            xmlString = stringWriter.toString();
        } catch(Exception e){
            CommonFunctions.log.error("nodeToString()", e);
        }
        return xmlString;
    }
    
    public static List<String> xpathQueryMultipleNodeValues(Node node, String xpath) {
        List<String> values = new ArrayList<String>();
        List<Node> nodeList = xpathQueryNodeList(node, xpath);
        for(Node nodeitem : nodeList) {
            values.add(nodeitem.getNodeValue());
        }
        return values;
    }
    
    public static String xpathQuerySingleNodeValue(Node node, String xpath, String defaultValue) {
        String value = xpathQuerySingleNodeValue(node, xpath);
        return value == null ? defaultValue : value;
    }
    
    public static String xpathQuerySingleNodeValue(Node node, String xpath) {
        String value = null;
        Node nodeitem = xpathQuerySingleNode(node, xpath);
        if(nodeitem != null) {
            return nodeitem.getNodeValue(); 
        }
        return value;
    }
    
    public static Node xpathQuerySingleNode(Node node, String xpath) {
        try {
            return (Node)xpathQueryObject(node, xpath, XPathConstants.NODE);
        } catch(Exception e){
            CommonFunctions.log.error("xpathQuerySingleNode()", e);
        }
        return null;
    }
    
    public static String xpathQueryString(Node node, String xpath) {
        return (String)xpathQueryObject(node, xpath, XPathConstants.STRING);
    }
    
    public static List<Node> xpathQueryNodeList(Node node, String xpath) {
        ArrayList<Node> nodes = new ArrayList<Node>();
        try {
            NodeList nodeList = (NodeList)xpathQueryObject(node, xpath, XPathConstants.NODESET);
            if(nodeList != null) {
                for(int i = 0; i < nodeList.getLength(); i++) {
                    nodes.add(nodeList.item(i));
                }
            }
        } catch(Exception e){
            CommonFunctions.log.error("xpathQueryNodeList()", e);
        }
        return nodes;
    }
    
    public static Object xpathQueryObject(Node node, String xpath, QName returnType) {
        Object result = null;
        try {
            if(node == null || xpath == null) {
                return result;
            }
            XPathExpression expr = xpathObj.compile(xpath);
            result = expr.evaluate(node, returnType);
        } catch(Exception e){
            CommonFunctions.log.error("xpathQueryObject()", e);
        }
        return result;
    }
    
    /**
     * Retrieves the string version of an XML document
     * @param xmlDoc
     * @return
     */
    public static String getDocumentXml(Document xmlDoc){
        String result = null;
        StringWriter sw = new StringWriter();
        try{
            //create string from xml tree
            writeXml(xmlDoc, sw);
            result = sw.toString();
        }catch(TransformerConfigurationException tce){
            CommonFunctions.log.error("getDocumentXml()", tce);
        }catch(TransformerException te){
            CommonFunctions.log.error("getDocumentXml()", te);
        }
        
        return result;
    }
    
    /**
     * Writes XML document directly to the writer
     * @param xmlDoc
     * @param writer
     * @throws TransformerException
     */
    public static void writeXml(Document xmlDoc, Writer writer) throws TransformerException{
        TransformerFactory transfac = TransformerFactory.newInstance();
        Transformer trans = transfac.newTransformer();
        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        trans.setOutputProperty(OutputKeys.INDENT, "yes");	        

        StreamResult result = new StreamResult(writer);
        DOMSource source = new DOMSource(xmlDoc);
                   
        if (source == null || result==null || trans == null){
            CommonFunctions.log.warn("PROBLEM FOOL...");
        }
            
        trans.transform(source, result);
    }

    public static String transformXmlToHtml(String xslUrl, Document xmlDoc, HttpServletResponse response){
        String trxOutput = null;
        
        try {
    
            // JAXP reads data using the Source interface
            String xmlString = getDocumentXml(xmlDoc);
            Source xmlSource = new StreamSource(new StringReader(xmlString));
            Source xsltSource = new StreamSource(new File(xslUrl));
            
            // Adding html headers to disable caching of pages
            response.setHeader("CACHE-CONTROL", "NO-CACHE");
            response.setHeader("EXPIRES", "-1");
            response.setHeader("PRAGMA", "NO-CACHE");
    
            
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer(xsltSource);
            
            transformer.transform(xmlSource, new StreamResult(response.getOutputStream()));		      
        }catch (Exception e) {
            CommonFunctions.log.error("TransformXmlToHtml()", e);
        }
          
    
        
        return trxOutput;
        
    }

    public static Document createNewXmlDocument(String rootNodeName)throws ParserConfigurationException{
        
        //Create the XML Document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document xmlDoc = docBuilder.newDocument();
        
        Node rootNode = xmlDoc.createElement(rootNodeName);
        xmlDoc.appendChild(rootNode);
        
        return xmlDoc;
    }

    public static Document createNewXmlDocumentFromNode(Node fromNode)throws ParserConfigurationException{
        
        //Create the XML Document
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        Document xmlDoc = docBuilder.newDocument();
        
        Node rootNode = xmlDoc.importNode(fromNode, true);
        xmlDoc.appendChild(rootNode);
        
        return xmlDoc;
    }

    public static Node selectSingleXmlNode(Document xmlDoc, String nodeName){
        NodeList nodeList = xmlDoc.getElementsByTagName(nodeName);
        Node returnedNode = null;
        if (nodeList != null){
            if (nodeList.getLength() != 0){
                returnedNode = nodeList.item(0);
            }
        }
        return  returnedNode;
    }

    public static Node selectSingleXmlNode(Node node, String nodeName){
        Element nodeElem = (Element)node;		
        NodeList nodeList = nodeElem.getElementsByTagName(nodeName);
        Node returnedNode = null;
        if (nodeList != null){
            if (nodeList.getLength() != 0){
                returnedNode = nodeList.item(0);
            }
        }
        return  returnedNode;
    }

    public static String getXmlNodeTextValue(Node node){
        
        String returnedVal = null;
        if (node.getNodeType() == Node.TEXT_NODE){
            Node textNode = node.getFirstChild(); 
            returnedVal = textNode.getNodeValue();
        }
        
        return returnedVal;
    }

    public static Node createXmlNode(Document doc,String nodeName,HashMap<?,?> map)
    {
        Node root = doc.createElement(nodeName);
        
        Set<?> entries = map.entrySet();
        Iterator<?> it = entries.iterator();
        
        while (it.hasNext()) 
        {
            Map.Entry<?,?> entry = (Map.Entry<?,?>)it.next();
            
            Node nameNode = doc.createElement((String)(entry.getKey()));
            Node valueNode = null;
            Object value = entry.getValue();
            
            if (value instanceof HashMap)
            {
                root.appendChild(createXmlNode(doc,"entity",(HashMap<?,?>)value));
                continue;
            }
            else if (value instanceof String)
            {
                valueNode = doc.createTextNode((String)(entry.getValue()));
            }
            else
            {
                CommonFunctions.log.warn((String)entry.getKey());
                CommonFunctions.log.warn(entry.getValue());
                CommonFunctions.log.warn("NOT YET IMPLEMENTED@");
            }
            
            nameNode.appendChild(valueNode);
            root.appendChild(nameNode);
        }
        return root;
    }

    /**
     * Return org.w3c.dom.Node object.
     * If value is null - it is considered as empty string ("") 
     * to avoid exceptions while XML transformations. 
     * @param doc
     * @param name
     * @param value
     * @return
     */
    
    public static Node createXmlTextNode(Document doc,String name,String value)
    {
        Node root = doc.createElement(name);
        if(value == null){
            value = "";
        }
        
        if(value.indexOf("\n") == -1){
            root.appendChild(doc.createTextNode(value));
        }else{			
            //workaround: wrap text as CDATA; otherwise "\n" will became "\r\n" after 'XML document->string' transformation
            addTextValueAsCDATA(doc, root, value);			
        }

        return root;
    }
    
    private static void addTextValueAsCDATA(Document doc, Node xmlNode, String value){
        //disable output escaping
        ProcessingInstruction pi = doc.createProcessingInstruction(Result.PI_DISABLE_OUTPUT_ESCAPING, "");
        xmlNode.appendChild(pi);
        
        //add CDATA section
        xmlNode.appendChild(doc.createCDATASection(value));
        
        //enable output escaping
        pi = doc.createProcessingInstruction(Result.PI_ENABLE_OUTPUT_ESCAPING, "");
        xmlNode.appendChild(pi);
    }	

    public static Element createXmlNodeWithValue(Document doc,String name,String value)
    {
        Element root = doc.createElement(name);
        root.setAttribute("v", (value != null) ? value : "");
        return root;
    }

    public static Document loadXmlFromFile(String fileAbsolutePath) throws IOException{
        Document xmlDoc = null;
        try{
          DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
          DocumentBuilder builder = factory.newDocumentBuilder();
          xmlDoc = builder.parse(new FileInputStream(fileAbsolutePath));
        }catch (ParserConfigurationException pcE){
            CommonFunctions.log.error(pcE.getMessage());
        }catch (SAXException sE){
            CommonFunctions.log.error(sE.getMessage());
        }
          
         return xmlDoc;
    }

    public static Document loadXmlFromByteArray(byte[] buf) throws IOException{
        Document xmlDoc = null;
        try{
          DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
          DocumentBuilder builder = factory.newDocumentBuilder();
          xmlDoc = builder.parse(new ByteArrayInputStream(buf));
        }catch (ParserConfigurationException pcE){
            CommonFunctions.log.error(pcE.getMessage());
        }catch (SAXException sE){
            CommonFunctions.log.error(sE.getMessage());
        }
          
         return xmlDoc;
    }

    public static String getErrorXML(int code, String error) {
        String errorXML = "<error>";
        errorXML += "<code>" + code + "</code>";
        errorXML += "<info>" + error + "</info>";
        errorXML += "</error>";
        return errorXML;
    }
    
    public static Document getDocument (String xmlString) {
        Document document = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory
            .newInstance();
            factory.setValidating(false);
            
            DocumentBuilder builder = null;
        
            builder = factory.newDocumentBuilder();
        
            builder.setEntityResolver(new EntityResolver() {
        
                @Override
                public InputSource resolveEntity(String publicId, String systemId)
                        throws SAXException, IOException {
                    return new InputSource(new StringReader(""));
                }
            });
            document = builder.parse(new ByteArrayInputStream(xmlString.getBytes("UTF-8")));
            return document;
        } catch (IOException e) {
            CommonFunctions.log.error(e.getMessage(),e);
        } catch (SAXException e) {
            CommonFunctions.log.error(e.getMessage(),e);
        } catch (ParserConfigurationException e) {
            CommonFunctions.log.error(e.getMessage(),e);
        }
        return null;
    }

    /**
     * Gets standard document builder
     * @return
     * @throws ParserConfigurationException
     */
    public static DocumentBuilder getStandardDocBuilder() throws ParserConfigurationException{
        //Get XML-configuration document				
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setCoalescing(true); // Convert CDATA to Text nodes
        factory.setNamespaceAware(false); // No namespaces: this is default
        factory.setValidating(false); // Don't validate DTD: also default
        DocumentBuilder docBuilder = factory.newDocumentBuilder();	    
        return docBuilder;
    }
}
