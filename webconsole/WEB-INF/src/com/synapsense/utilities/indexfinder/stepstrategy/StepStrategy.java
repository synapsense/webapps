package com.synapsense.utilities.indexfinder.stepstrategy;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

abstract public class StepStrategy {
    abstract public int calculateStep(int startEdge, int endEdge, List<SensorPoint>array, long searchingValue);
}
