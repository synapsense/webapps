package com.synapsense.utilities.indexfinder.stepstrategy;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public class DichotomyStrategy extends StepStrategy {
    
    @Override
    public int calculateStep(int startEdge, int endEdge, List<SensorPoint>array, long searchingValue) {
        return (Math.abs(endEdge - startEdge) / 2);
    }

}
