package com.synapsense.utilities.indexfinder.stepstrategy;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public class InterpolationStrategy extends StepStrategy {
    
    @Override
    public int calculateStep(int startEdge, int endEdge, List<SensorPoint>array, long searchingValue) {
        long startTime = array.get(startEdge).time_stamp;
        long endTime = array.get(endEdge).time_stamp;
        
        if(searchingValue < startTime){
            searchingValue = startTime;
        }
        
        if(searchingValue > endTime){
            searchingValue = endTime;
        }
        
        if(endTime == startTime){
            return 0;
        }
        
        Long step =  (Math.abs(endEdge - startEdge) * (searchingValue - startTime)) / (endTime - startTime);
        return step.intValue();
    }

}
