package com.synapsense.utilities.indexfinder;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.indexfinder.stepstrategy.DichotomyStrategy;
import com.synapsense.utilities.indexfinder.stepstrategy.InterpolationStrategy;
import com.synapsense.utilities.indexfinder.stepstrategy.StepStrategy;
import com.synapsense.utilities.sensors.SensorPoint;

public class IndexFinder {
    private static final Log LOG = LogFactory.getLog(IndexFinder.class);
    
    protected boolean is_ASC = false;
    protected StepStrategy stepStrategy = null;     
    protected StepStrategy interStrategy = null;
    protected StepStrategy dichStrategy = null;
    
    //TEST
    private static ArrayList<SensorPoint> arr = new ArrayList<SensorPoint>();    
    public static void fillArray(){
        arr.clear();        
        long step = 2;//0.5;
        long xLimit = 11000;
        
        //for(long i = xLimit; i > 0; i -= step){
        for(long i = 0; i < xLimit; i += step){
            arr.add(new SensorPoint(Math.random(), i));
        }
    }    
    public static void main(String[] args) {
        fillArray();
        
        //IndexFinder iFinder = new IndexFinder(false);
        IndexFinder iFinder = new IndexFinder(true);
        int index = iFinder.findIndex(arr, 21);
        
        LOG.info("Index was found: " + index);
    }
    //END TEST
    
    public IndexFinder(boolean isAsc){
        is_ASC = isAsc;
        interStrategy = new InterpolationStrategy();
        dichStrategy = new DichotomyStrategy();
    }
    
    public int findIndex(List<SensorPoint> array, long searchingValue){
        int n = 0; //getIndexInAscArray(0, array.size());//0;
        int m = array.size() - 1; //getIndexInAscArray(array.size() - 1, array.size());//array.size() - 1;
        
        int count = 0;
        int step;
        int nextIndex = -1;
        long K;
        
        stepStrategy = interStrategy;
        
        Date bTime = new Date();
        
        while(m > n){
            count++;            
            
            //LOG.info("count " + count);
            //LOG.info("n, m " + n + ", " + m);
            
            //step = stepStrategy.calculateStep(n, m, array, searchingValue); //parseInt((m - n) / 2);
            step = stepStrategy.calculateStep(getIndexInAscArray(n, array.size()), getIndexInAscArray(m, array.size()), array, searchingValue);
            
            //LOG.info("step " + step);
            
            nextIndex = n + step;
                    
            //K = array.get(nextIndex).time_stamp.getTime();
            K = array.get(getIndexInAscArray(nextIndex, array.size())).time_stamp;
        
            if(searchingValue < K){
                m = nextIndex - 1;
            }else if(searchingValue > K){
                n = nextIndex + 1;
            }else{
                //we found it!
                //LOG.info("equals index was found: " + nextIndex + ". Operations number: " + count);
                LOG.info("equals index was found.");
                break;
            }
            
            if(count == 3){
                LOG.info("Strategy was changed.");
                stepStrategy = dichStrategy;
            }
        }
        
        LOG.info("Index is: " + nextIndex + ". Operations number: " + count);
                
        Date eTime = new Date();
        
        LOG.info("Total searching time: " + (eTime.getTime() - bTime.getTime()) + " ms");
        //return nextIndex;
        return getIndexInAscArray(nextIndex, array.size());
    }
    
    protected int getIndexInAscArray(int indexInArray, int arraySize){
        int result = indexInArray;        
        if(!is_ASC){
            result = arraySize - 1 - indexInArray;
        }        
        return result;
    }
}
