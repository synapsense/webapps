package com.synapsense.utilities;

import java.util.ArrayList;
import java.util.Collection;

import com.synapsense.elementmodel.basic.ModelElement;

public class DataRestrictor {
    
    public static void addRestrictionInfoToModel(ModelElement rootElement, int existingNumber, int shownNumber){
        rootElement.addAttribute("existingNumber", existingNumber);
        rootElement.addAttribute("shownNumber", shownNumber);
    }
    
    public static <T> Collection<T> decreaseCollectionToAllowedSize(Collection<T> objects, int maxNumber){
        Collection<T> decreasedCollection = objects;
        if(objects.size() > maxNumber){
            decreasedCollection = new ArrayList<T>(maxNumber);
            for(T obj : objects){
                decreasedCollection.add(obj);
                if(decreasedCollection.size() >= maxNumber){
                    break;
                }
            }
        }
        return decreasedCollection;
    }
}
