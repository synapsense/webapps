package com.synapsense.utilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.session.ApplicationWatch;

/**
 * 
 * @author aklushin
 * 
 */
public class TestServlet extends AsyncHttpServlet {
    private static final long serialVersionUID = -6725255528458668352L;
    private static final Log LOG = LogFactory.getLog(TestServlet.class);

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }

    @Override
    protected void doPost (HttpServletRequest request,HttpServletResponse response) 
            throws IOException, ServletException {
        super.doPost(request, response);
        Document typesDoc = XMLHelper.loadXmlFromFile(ApplicationWatch.getPath() + "conf/types.xml");
        clearTypesDoc(typesDoc, request);
        for (int i=0;i<(Integer.MAX_VALUE/3);i++) {
            ProgressTracker.getInstance().updateProgress((byte)(i),"III "+i);
        }
        response.setHeader("Pragma", "no-cache");
        sendResponse(response, typesDoc);
    }
    
    private void clearTypesDoc (Document document, HttpServletRequest request) {
        Element typesEl = document.getDocumentElement();
        Environment env = Env.getEnv(request);
        NodeList typeList = typesEl.getElementsByTagName("type");
        for (int i=0;i<typeList.getLength();i++) {
            Element typeEl = (Element)typeList.item(i);
            String typeName = typeEl.getAttribute("name");
            NodeList propertiesList = typeEl.getElementsByTagName("property");
            for (int j=0;j<propertiesList.getLength();j++) {
                Element propertyEl = (Element)propertiesList.item(j);
                String propertyName = propertyEl.getAttribute("name");
                if ("false".equals(propertyEl.getAttribute("visible")) ||
                        propertyName.isEmpty()) {
                    typeEl.removeChild(propertyEl);
                    j--;
                    continue;
                }				
                try {
                    PropertyDescr pdescr = env.getPropertyDescriptor(typeName, propertyName);
                    Class<?> propClass = pdescr.getType();
                    if (Number.class.isAssignableFrom(propClass)) {
                        //do nothing
                    } else if (TO.class.isAssignableFrom(propClass)) {
                        //test is it sensor. If so check lastValue property?
                    } else {
                        typeEl.removeChild(propertyEl);
                        j--;
                    }
                } catch (PropertyNotFoundException e) {
                    typeEl.removeChild(propertyEl);
                    j--;
                    LOG.info(e.getLocalizedMessage());
                } catch (ObjectNotFoundException e) {
                    typesEl.removeChild(typeEl);
                    i--;
                    LOG.info(e.getLocalizedMessage());
                    break;
                }
            }
            if (propertiesList.getLength()==0) {
                typesEl.removeChild(typeEl);
                i--;
                continue;
            }
        }
    }
}