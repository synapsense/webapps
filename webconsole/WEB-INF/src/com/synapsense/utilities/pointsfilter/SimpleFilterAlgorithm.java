package com.synapsense.utilities.pointsfilter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.synapsense.utilities.sensors.PointComparator;
import com.synapsense.utilities.sensors.SensorPoint;
import com.synapsense.utilities.sensors.TimestampPointComparator;

public class SimpleFilterAlgorithm extends FilterAlgorithm{	
        
    public SimpleFilterAlgorithm(){		
    }
    
    public List<SensorPoint> filter(List<SensorPoint> allPoints, int filteredNumber){
        return pickByStep(allPoints, calculateStep(allPoints.size(), filteredNumber));
    }
    
    /**
     * Get points from sub list. Currently get max and min points
     * @param glass
     * @return
     */
    private List<SensorPoint> getFilteredPoints(List<SensorPoint> subList){		
        SensorPoint maxPoint = null;
        SensorPoint minPoint = null;
        List<SensorPoint> arr = new ArrayList<SensorPoint>();
        
        int size = subList.size();
        if(size > 0){
            maxPoint = subList.get(0);
        }
        if(size > 1){
            minPoint = subList.get(1);
            }
            
        if(size < 3){
            if(maxPoint != null){
                arr.add(maxPoint);
            }
            if(minPoint != null){
                arr.add(minPoint);
            }
            return arr;
        }
        
        for(int i = 0; i < subList.size(); i++){
            SensorPoint current = subList.get(i);
            if(maxPoint.value < current.value){
                maxPoint = current;
            }			
            if(minPoint.value > current.value){
                minPoint = current;
            }
        }
        
        arr.add(maxPoint);
        arr.add(minPoint);
        
        return arr;
    }
    
    private int calculateStep(int pointsNumber, int numberAfterFilter){
        int step = pointsNumber;
        if(numberAfterFilter > 0){
            step = (pointsNumber / numberAfterFilter) * 2; 
        }	    
        
        return step;
    }
    
    public List<SensorPoint> pickByStep(List<SensorPoint> allPoints, int step){
        List<SensorPoint> filteredPoints = new ArrayList<SensorPoint>();
        PointComparator timeComp = new TimestampPointComparator(false);
        //PointComparator valueComp = new ValuePointComparator();       
        
        //sort points collection by time stamp              
        Collections.sort(allPoints, timeComp);
        
        List<SensorPoint> glass = new ArrayList<SensorPoint>();
        int lastIndex = 0;
        int pointsArraySize = allPoints.size();
        //int step = calculateStep(pointsArraySize, filteredNumber);
        
        for(int i = 0; i < pointsArraySize; i += step){
            lastIndex = (i + step < pointsArraySize) ? i + step : pointsArraySize - 1; 
            glass = allPoints.subList(i, lastIndex);
            
            //get max and min points from sublist 
            List<SensorPoint> arr = getFilteredPoints(glass);
            
            for(int j = 0; j < arr.size(); j++){
                filteredPoints.add(arr.get(j));
            }           
        }
        
        return filteredPoints;	    
    }
}
