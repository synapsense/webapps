package com.synapsense.utilities.pointsfilter;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.sensors.SensorPoint;

public class TabularFilter extends DummyFilter{
    private static final Log LOG = LogFactory.getLog(TabularFilter.class);
    
    /** Allowed rows number in Laszlo table */
    private static Integer allowedPointsNumber = null;    
    
    /** If this flag is null or false, passed points will be returned as is, without filtering */
    private static Boolean isFilterTurnedOn = null;

    protected FilterAlgorithm algorithm = new SimpleFilterAlgorithm();
    //private PointsFilter filter = new PointsFilter();
    
    public TabularFilter(){
        try{
            readConfiguration();            
        }catch(Exception e){
            isFilterTurnedOn = false;            
            LOG.error("Filter for Tabular data is not turned on. Exception occured: ", e);
        }
    }
    
    /**
     * Read filter configuration from config file
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private void readConfiguration() throws SAXException, IOException, ParserConfigurationException{
        if(isFilterTurnedOn == null){
            isFilterTurnedOn = false;
            
            // Get XML-configuration document
            File confFile = new File(CommonFunctions.getStandardConfigFileName());      
            Document confDoc = null;
            
            confDoc = XMLHelper.getStandardDocBuilder().parse(confFile);
            
            try{
                //Compile xPath expressions
                XPathFactory  factoryX = XPathFactory.newInstance();
                XPath xPath = factoryX.newXPath();
                XPathExpression xPathMaxRows = xPath.compile("config/feature[@name='DataAnalysis']/setting[@name='maxRows']/@value");
                String maxRows =  xPathMaxRows.evaluate(confDoc);
                if(maxRows != null){
                    allowedPointsNumber = Integer.parseInt(maxRows);
                    isFilterTurnedOn = true;
                }
            }catch(Exception e){
                isFilterTurnedOn = false;                
                LOG.error("Filter for Tabular data is not turned on. Exception occured: ", e);
            }
        }
    }
    
    @Override
    /**
     * Filters passed points.
     * @param allPoints - points to filter
     * @param totalRequests - count of items (properties or sensors) in tabular requests 
     */
    public List<SensorPoint> filter(List<SensorPoint> allPoints, int totalRequests){        
        
        //if filter is not turned on, return points as is
        if((isFilterTurnedOn != null && !isFilterTurnedOn) || 
                allowedPointsNumber == null){
            return allPoints;
        }
        
        int filteredPointsNumber = allowedPointsNumber / totalRequests;
        
        List<SensorPoint> result = allPoints;
        if(allPoints.size() > filteredPointsNumber){
            result =  algorithm.filter(allPoints, filteredPointsNumber);
            originPointsCount += allPoints.size();
            returnedPointsCount += result.size();
        }        
        
        return result;
    }
    
}
