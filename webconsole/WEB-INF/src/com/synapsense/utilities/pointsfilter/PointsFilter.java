package com.synapsense.utilities.pointsfilter;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public class PointsFilter {
    protected FilterAlgorithm algorithm = new SimpleFilterAlgorithm();
    
    public PointsFilter(){		
    }
    
    public PointsFilter(FilterAlgorithm filter_algorithm){
        algorithm = filter_algorithm;
    }
    
    public void setFilterAlgorithm(FilterAlgorithm filter_algorithm){
        algorithm = filter_algorithm;
    }
    
    public FilterAlgorithm getFilterAlgorithm(){
        return algorithm;
    }
    
    public List<SensorPoint> filter(List<SensorPoint> allPoints, int filteredNumber){
        return algorithm.filter(allPoints, filteredNumber);
    }
    
    public List<SensorPoint> pickByStep(List<SensorPoint> allPoints, int step){
        return algorithm.pickByStep(allPoints, step);
    }
}
