package com.synapsense.utilities.pointsfilter;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public class DummyFilter{
    
    /**
     * Total count of points that were passed for filtering
     */
    protected int originPointsCount = 0;
    /**
     * Total count of points that were returned from filtering
     */
    protected int returnedPointsCount = 0;
    
    public int getTotalOriginPointsCount(){
        return originPointsCount;
    }
    
    public void resetCounters(){
        returnedPointsCount = 0;
        originPointsCount = 0;
    }
    
    public List<SensorPoint> filter(List<SensorPoint> allPoints, int totalRequests){
        return allPoints;
    }
}
