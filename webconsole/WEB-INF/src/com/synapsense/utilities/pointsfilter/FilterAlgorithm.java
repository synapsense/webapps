package com.synapsense.utilities.pointsfilter;

import java.util.List;

import com.synapsense.utilities.sensors.SensorPoint;

public abstract class FilterAlgorithm {
    
    abstract public List<SensorPoint> filter(List<SensorPoint> allPoints, int filteredNumber);
    abstract public List<SensorPoint> pickByStep(List<SensorPoint> allPoints, int step);

}
