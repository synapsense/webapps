package com.synapsense.utilities;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.utilities.ProgressTracker;
import com.synapsense.utilities.XMLHelper;

public class GetProgress extends BaseHttpServlet {	
    private static final long serialVersionUID = -8177915340910562827L;
    private static final Log LOG = LogFactory.getLog(GetProgress.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
              throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        ProgressTracker pt = null;
        synchronized (session) {
            LOG.trace(this.getServletName() + " is started ...");
            String id = request.getParameter(RequestVariableNames.REQUEST_ID);
            if (session != null) {
                pt = (ProgressTracker)session.getAttribute(id);
            }
        }
        if (pt == null) {
            throw new ServletException("ProgressTracker can't be null");
        }
                
        Document doc = null;
        try {
            doc = XMLHelper.createNewXmlDocument("progress");
        } catch (ParserConfigurationException e) {
            throw new ServletException(e.getMessage(), e);
        }
        Element root = doc.getDocumentElement();
        
        root.appendChild(XMLHelper.createXmlTextNode(doc, "percent", Byte.toString(pt.getPercent())));
        root.appendChild(XMLHelper.createXmlTextNode(doc, "message", pt.getMessage()));
        root.appendChild(XMLHelper.createXmlTextNode(doc, "data", pt.getResultString()));
        root.appendChild(XMLHelper.createXmlTextNode(doc, "iscompleted", Boolean.toString(pt.isCompleted())));
        
        sendResponse(response, doc);
        if (pt.isCompleted()) {
            reset(request);
        }
        LOG.trace(this.getServletName() + " is finished ...");
    }
    
    private void reset (HttpServletRequest request) {
        String id = request.getParameter(RequestVariableNames.REQUEST_ID);
        request.getSession(false).setAttribute(id,null);
    }
}
