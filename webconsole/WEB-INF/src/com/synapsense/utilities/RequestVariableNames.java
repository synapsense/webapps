/**
 * 
 */
package com.synapsense.utilities;

/**
 * @author dinniss
 */
public class RequestVariableNames
{

    // Request variable Names
    public static final String	ACTION								= "action";
    public static final String	MODE								= "mode";
    public static final String	SEARCH_BY							= "searchBy";
    public static final String	SEARCH_CRITERIA						= "searchCriteria";
    public static final String	USER_NAME							= "user";
    public static final String	OLD_USER_NAME						= "oldUser";
    public static final String	PASSWORD							= "pword";
    public static final String	NEWPASSWORD							= "newpasswd";	
    public static final String  ID                                  = "id";
    public static final String	TYPE								= "type";
    public static final String	LAST_X_MEASURES						= "lastX";
    public static final String	AGED								= "aged";
    public static final String	HISTORY_BETWEEN_DATES				= "betweenDates";
    public static final String	HISTORY_BETWEEN_DATES_AND_TIME		= "betweenDatesAndTime";
    public static final String	BEGIN_HOUR                  		= "beginHour";
    public static final String	END_HOUR                  		    = "endHour";
    public static final String	BEGIN_MINUTE                  		= "beginMinute";
    public static final String	END_MINUTE                  		= "endMinute";
    public static final String	BEGIN								= "begin";
    public static final String	END									= "end";
    public static final String	HISTORY_BY							= "histBy";
    public static final String	NAME								= "name";
    public static final String	DISPLAY_NAME						= "displayName";
    public static final String	DESCRIPTION							= "desc";
    public static final String	MESSAGE_HEADER						= "msgHdr";
    public static final String	MESSAGE_BODY						= "msgBody";
    public static final String	PRIORITY							= "priority";
    public static final String	AUTODISMISS							= "autodismiss";
    public static final String	FIRST_NAME							= "fName";
    public static final String	LAST_NAME							= "lName";
    public static final String	MIDDLE_INITIAL						= "middleInitial";
    public static final String	TITLE								= "title";
    public static final String	PRIMARY_PHONE						= "primaryPhone";
    public static final String	SECONDARY_PHONE						= "secondaryPhone";
    public static final String	PRIMARY_EMAIL						= "primaryEmail";
    public static final String	SECONDARY_EMAIL						= "secondaryEmail";
    public static final String	SMS_ADDRESS							= "smsAddress";
    public static final String	PREFERENCES							= "preferences";
    public static final String	CHANGE_PASSWORD_FLAG				= "changePword";
    public static final String  CHILDREN							= "children";
    public static final String  ENTRYINFO							= "entryInfo";
    public static final String  ROOM								= "room";
    public static final String  LOCATION							= "location";

    public static final String	COMMAND								= "command";

    public static final String	ACTION_CHECK_AUTHENTICATION			= "checkauthentication";
    public static final String	ACTION_EMBEDED_AUTHENTICATE			= "embededauth";
    public static final String	ACTION_AUTHENTICATE					= "authenticate";
    public static final String	ACTION_UPDATEPWD					= "updatepwd";
    public static final String	ACTION_CLOSE_USER_SESSION			= "logoff";
    public static final String	ACTION_SAVE_USER_PREF				= "savePref";

    public static final String	USER_PREF_NAME						= "prefName";
    public static final String	USER_PREF_VALUE						= "prefVAlue";

    public static final String	MODE_EXCEL							= "xls";
    
    public static final String  AGED_BY                             = "agedBy";
    public static final String  AGED_BY_DAY                         = "day";
    public static final String  AGED_BY_WEEK                        = "week";
    public static final String  AGED_BY_MONTH                       = "month";
    public static final String  AGED_BY_HOUR                        = "hour";

    public static final String	SEARCH_BY_FIRSTNAME					= "byfirstname";
    public static final String	SEARCH_BY_LASTNAME					= "bylastname";
    public static final String	SEARCH_BY_PHONE						= "byphone";
    public static final String	SEARCH_BY_EMAIL						= "byemail";
    public static final String	SEARCH_BY_USERNAME					= "byusername";
    public static final String	SEARCH_BY_USERTYPE					= "byusertype";
    
    public static final String	FILTER_BY_ACTION_AND_TIME			= "byactionandtime";
    public static final String	FILTER_BY_MODULE_AND_TIME			= "bymoduleandtime";
    public static final String	FILTER_BY_USER_AND_TIME				= "byusernameandtime";
    public static final String	FILTER_BY_DATE_ONLY					= "bydateonly";

    public static final String	CUSTOM_QUERY_ID						= "queryId";
    public static final String 	REWRITE_CUSTOM_QUERY				= "rewriteCustomQuery";
    public static final String 	CUSTOM_QUERY_NAME_IS_CHANGED		= "nameIsChanged";
    public static final String	IS_NEW_CUSTOM_QUERY					= "isNewCustomQuery";
    public static final String	CUSTOM_QUERY_NAME					= "customQuery";
    public static final String	CUSTOM_DATA_REQUEST_IDS				= "associatedSensors";
    public static final String	IS_PRIVATE							= "privateCheck";
    public static final String	CUSTOM_QUERY_DESCRIPTION			= "queryDesc";
    public static final String	CUSTOM_QUERY_USERID			        = "userId";
    public static final String	CUSTOM_QUERY_LINES_DESCR            = "lines";
    public static final String	CUSTOM_QUERY_CHART_DESCR            = "chartDescr";
    
    public static final String	ANNOTATION_NOTE			            = "note";
    public static final String	ANNOTATION_STATUS		            = "status";
    public static final String	ANNOTATION_DATETIME		            = "datetime";
    public static final String	ANNOTATION_USERNAME		            = "user";
    public static final String	ANNOTATION_DC			            = "dc";
    
    public static final String PARAM_TYPE = "param";
    
    public static final int LAYER_VALUE_OFF = 0;
    public static final int LAYER_VALUE_CONTROLLED = 128;
    public static final int LAYER_VALUE_SUBFLOOR = 256;
    public static final int LAYER_VALUE_FLOOR = 2048;
    public static final int LAYER_VALUE_MIDDLE = 16384;
    public static final int LAYER_VALUE_TOP = 131072;
    public static final int LAYER_VALUE_CEILING = 1048576;
    
    public static final String TABLE_HTML = "html";	
    public static final String TABLE_KEY = "key";
    public static final String DATA_FILE_NAME = "dataFileName";
    
    public static final String REQUEST_ON_LOAD_DATA = "load";	
    public static final String REQUEST_ON_SAVE_DATA = "save";
    
    public static final String CSRF_NONCE = "CSRF_NONCE";
    public static final String GETRESPONSEHEADERS = "GETRESPONSEHEADERS";
    
    public static final String ASYNC_REQUEST = "ASYNC_REQUEST";
    public static final String REQUEST_ID = "REQUEST_ID";
}
