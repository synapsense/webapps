package com.synapsense.utilities.lang;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStreamReader;

import java.io.OutputStreamWriter;
import java.util.Collections;
import java.util.Enumeration;
import java.util.InvalidPropertiesFormatException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.synapsense.dataAccess.Env;
import com.synapsense.session.ApplicationWatch;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.XMLHelper;

public class ResBundleConverter {
    static Log log = LogFactory.getLog(ResBundleConverter.class);
    static private String REAL_PATH = "./";
    static private final String LANG_FILE_NAME = "LangData.xml";
    static private final String LANG_FILE_PRENAME = "strings_";
    static private final String LANG_DIRECTORY_NAME = "resources";
    static private final String ENCODING = "utf-8";
    static private final String CHARSET = "iso8859-1";
    static private final String TYPE_PREFIX = "object_type_";
    static private final String PROPERTY_PREFIX = "object_property_";
    static private final String UNIT_PREFIX = "unit_";

    private ResBundleConverter() {
    }

    public static void main(String[] args) throws Exception {
        readPropsFromXml(ResBundleConverter.REAL_PATH + "/" +
                 ResBundleConverter.LANG_FILE_NAME, ENCODING);
    }
    
    public static void init() {
        ResBundleConverter.REAL_PATH = ApplicationWatch.getPath() + "/" + ResBundleConverter.LANG_DIRECTORY_NAME;
        try {

            /*
             * readPropsFromXml(XmlConverter.REAL_PATH + "/" +
             * ResBundleConverter.LANG_FILE_NAME, ENCODING);
             */
            writePropsToXml(ResBundleConverter.REAL_PATH);
        } catch (InvalidPropertiesFormatException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (IOException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            log.error(e.getLocalizedMessage(), e);
        } catch (TransformerException e) {
            log.error(e.getLocalizedMessage(), e);
        }
    }

    private static void writePropsToXml(String directory) throws IOException, ParserConfigurationException,
            TransformerException {
        File f = new File(directory);
        String[] list = {};
        if (f.isDirectory()) {
            list = f.list(new FilenameFilter() {

                @Override
                public boolean accept(File dir, String name) {
                    String f = new File(name).getName();
                    return f.endsWith(".properties");
                }

            });
        }

        deleteExistingFile(directory + "/" + ResBundleConverter.LANG_FILE_NAME);

        Document doc = XMLHelper.createNewXmlDocument("locales");

        for (int i = 0; i < list.length; i++) {
            generateDoc(doc, list[i], CHARSET, ENCODING);
        }

        File file = new File(ResBundleConverter.REAL_PATH + "/" + ResBundleConverter.LANG_FILE_NAME);
        FileOutputStream fos = new FileOutputStream(file);
        XMLHelper.writeXml(doc, new OutputStreamWriter(fos, ENCODING));
        fos.close();
    }

    private static boolean deleteExistingFile(String file) {
        File f = new File(file);
        if (f.exists()) {
            return f.delete();
        }
        return false;
    }

    private static void generateDoc(Document doc, String fName, String charset, String encoding) throws IOException {
        Element locales = doc.getDocumentElement();
        Element locale = (Element) locales.appendChild(doc.createElement("locale"));
        String fPath = ResBundleConverter.REAL_PATH + "/" + fName;

        Properties props = new Properties();
        props.load(new InputStreamReader(new FileInputStream(fPath), ENCODING));

        Locale loc = getLocale(fName);
        locale.setAttribute("id", loc.toString());
        locale.setAttribute("codeISO3", loc.getISO3Country());
        locale.setAttribute("lang", loc.getDisplayLanguage(loc));
        locale.setAttribute("chars", props.getProperty("chars_value"));
        if(Env.getLocalizationService().getDefaultLocale().equals(loc)) {
            locale.setAttribute("isDefault", "true");
        }

        locales.appendChild(locale);
        Set<Object> keys = props.keySet();
        Iterator<Object> i = keys.iterator();
        while (i.hasNext()) {
            String key = (String) i.next();
            locale.appendChild(XMLHelper.createXmlTextNode(doc, key, props.getProperty(key)));
        }

        // Add type and property names from
        Properties strings = Env.getLocalizationService().getStrings().get(loc);
        if (strings != null) {
            for (String key : strings.stringPropertyNames()) {
                if (key.startsWith(TYPE_PREFIX) || key.startsWith(PROPERTY_PREFIX) || key.startsWith(UNIT_PREFIX)) {
                    locale.appendChild(XMLHelper.createXmlTextNode(doc, key, strings.getProperty(key)));
                }
            }
        }
    }

    private static Locale getLocale(String fileName) throws IOException {
        String localeId = fileName.substring(fileName.indexOf("_") + 1, fileName.indexOf("."));
        Locale locale = LocalizationUtils.getLocale(localeId);
        return locale;
    }

    public static void readPropsFromXml(String file, String encoding) throws IOException,
            InvalidPropertiesFormatException, FileNotFoundException {

        class SortedProperties extends Properties {
            private static final long serialVersionUID = 9126878878618065532L;

            @SuppressWarnings({ "rawtypes", "unchecked" })
            public Enumeration keys() {
                 Enumeration<Object> keysEnum = super.keys();
                 Vector<String> keyList = new Vector<String>();
                 while(keysEnum.hasMoreElements()){
                   keyList.add((String)keysEnum.nextElement());
                 }
                 Collections.sort(keyList);
                 return keyList.elements();
              }
              
            }

        Document doc = null;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            factory.setIgnoringComments(true);
            doc = builder.parse(file);
        } catch (SAXException saxe) {
            throw new InvalidPropertiesFormatException(saxe);
        } catch (ParserConfigurationException x) {
            throw new Error(x);
        }

        Element propertiesElement = (Element) doc.getChildNodes().item(0);

        NodeList nl = propertiesElement.getElementsByTagName("locale");
        for (int i = 0; i < nl.getLength(); i++) {
            Properties prop = new SortedProperties();

            importProperties(prop, (Element) nl.item(i));
            prop.put("chars_value", ((Element) nl.item(i)).getAttribute("chars"));

            String fileName = ResBundleConverter.LANG_FILE_PRENAME + ((Element) nl.item(i)).getAttribute("id")
                    + ".properties";

            FileOutputStream fos = new FileOutputStream(new File(ResBundleConverter.REAL_PATH + "/" + fileName));
            OutputStreamWriter osw = new OutputStreamWriter(fos, ENCODING);
            prop.store(osw, null);

            osw.close();
            fos.close();
        }

    }

    private static void importProperties(Properties props, Element propertiesElement) {
        NodeList entries = propertiesElement.getChildNodes();
        int numEntries = entries.getLength();

        for (int i = 0; i < numEntries; i++) {
            String key = "";

            if (entries.item(i).getNodeType() != 1) {
                continue;
            }
            getElementData(props, key, (Element) entries.item(i));
        }
    }

    private static Element getElementData(Properties props, String key, Element el) {
        Element rel = el;
        if (el == null || el.getNodeType() == 3)
            return rel;

        key += ("".equals(key)) ? rel.getNodeName() : "_" + rel.getNodeName();
        NodeList ell = rel.getChildNodes();
        for (int i = 0; i < ell.getLength(); i++) {
            if (!ell.item(i).hasChildNodes() && ell.item(i).getNodeValue() != null
                    && !"".equals(ell.item(i).getNodeValue().trim())) {
                props.setProperty(key, ell.item(i).getNodeValue().trim());
            }
            if (ell.item(i).getNodeType() == 1 && ell.item(i).hasChildNodes()) {
                rel = getElementData(props, key, (Element) ell.item(i));
            }
        }

        return rel;
    }
}
