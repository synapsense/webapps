package com.synapsense.utilities.lang;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.XMLHelper;

public class LangManager {
    static private volatile LangManager instance;
    static private final String LANG_FILE_NAME = "resources/LangData.xml";

    private Map<String, Map<String, String>> langMap;

    private LangManager(ServletContext context) {
        try {
            String path = context.getRealPath("/") + "/" + LangManager.LANG_FILE_NAME;
            Document doc = XMLHelper.loadXmlFromFile(path);

            XPathFactory factoryX = XPathFactory.newInstance();
            XPath xPath = factoryX.newXPath();
            XPathExpression langsExps = xPath.compile("locales/locale");
            NodeList locales = (NodeList) langsExps.evaluate(doc, XPathConstants.NODESET);

            langMap = new HashMap<String, Map<String, String>>();

            for (int i = 0; i < locales.getLength(); i++) {
                Element locale = (Element) locales.item(i);
                String localeId = locale.getAttribute("id");

                NodeList prompts = locale.getElementsByTagName("*");
                
                Map<String, String> promptsMap = new HashMap<String, String>();

                for (int j = 0; j < prompts.getLength(); j++) {
                    Element prompt = (Element) prompts.item(j);

                    String value = getElementText(prompt);
                    
                    if (value != null) {
                        String promptId = getPromptId(prompt);
                        promptsMap.put(promptId, value.trim());
                        
                    }
                }

                langMap.put(localeId, promptsMap);
            }

        } catch (Exception e) {
            throw new LangManagerInitializationException(e.getMessage(), e);
        }
    }

    private String getElementText(Element e) {
        NodeList children = e.getChildNodes();
        
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeType() == Node.TEXT_NODE || children.item(i).getNodeType() == Node.CDATA_SECTION_NODE) {
                return children.item(i).getTextContent();
            } 
        }
        return null;
    }

    private String getPromptId(Element e) {
        StringBuffer promptId = new StringBuffer();
        Element e1 = e;

        while (!e1.getNodeName().equals("locale")) {
            promptId.insert(0, e1.getNodeName()).insert(0, "/");
            e1 = (Element) e1.getParentNode();
        }

        return promptId.substring(1, promptId.length());
    }

    synchronized static public void init(ServletContext context) throws LangManagerInitializationException {
        if (instance == null) {
            instance = new LangManager(context);
        }
    }

    static public LangManager getInstance() {
        if (instance == null) {
            throw new NullPointerException("LangManager is not initialized");
        }

        return instance;
    }

    public String getLocaleString(String langID, String promptId) {
        String result = null;

        String repPromptId = promptId;
        if (promptId != null && !"".equals(promptId) && promptId.contains("/")) {
            repPromptId = promptId.replace("/", "_");
        }
        Map<String, String> promptsMap = langMap.get(langID);

        if (promptsMap == null) {
            result = repPromptId;
        } else {
            result = promptsMap.get(repPromptId);
        }

        if (result == null) {
            result = repPromptId;
        } else if (result.equals("''")) {
            result = "";
        }

        return result;
    }
    
    public String getLocaleString(String langID, String promptId, String... args) {
        String result = null;

        String repPromptId = promptId;
        if (promptId != null && !"".equals(promptId) && promptId.contains("/")) {
            repPromptId = promptId.replace("/", "_");
        }
        Map<String, String> promptsMap = langMap.get(langID);

        if (promptsMap == null) {
            result = repPromptId;
        } else {
            result = promptsMap.get(repPromptId);
            if(result != null && result.indexOf("$") != -1) {
                for (int i = 0; i < args.length; i++) {
                    String arg = args[i] != null ? args[i] : "null";
                    result = result.replaceAll("\\$"+i, arg);
                }
            }
        }

        if (result == null) {
            result = repPromptId;
        } else if (result.equals("''")) {
            result = "";
        }

        return result;
    }

    public String getLocaleString(Locale locale, String promptId) {
        return getLocaleString(locale.toString(), promptId);
    }

    public static Locale getCurrentLocale(HttpServletRequest request) {
        String userName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        Locale locale = null;

        if (userName != null) {
            String lang = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_LANGUAGE);
            if(lang == null){
                TO<?> user = Env.getUserDAO().getUser(userName);
                lang = UserPreference.getString(user, "SavedLanguage", Env.getUserDAO());
                request.getSession().setAttribute(SessionVariableNames.SESSION_LANGUAGE, lang);
            }

            if (lang != null) {
                locale = LocalizationUtils.getLocale(lang);
            }
        }

        if (locale == null) {
            locale = Env.getLocalizationService().getDefaultLocale();
        }

        return locale;
    }

    public static String getCurrentLocaleId(HttpServletRequest request) {
        return LangManager.getCurrentLocale(request).toString();
    }

    public boolean isPromptExists(String langID, String promptId) {
        boolean result = false;
        String localeStr = null;
        if (promptId != null && !"".equals(promptId) && promptId.contains("/")) {
            promptId = promptId.replace("/", "_");
        }
        
        Map<String, String> promptsMap = langMap.get(langID);

        if (promptsMap != null) {
            localeStr = promptsMap.get(promptId);
            if (localeStr != null) {
                result = true;
            }
        }

        return result;
    }
}
