package com.synapsense.utilities;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class DocumentHelper {
    
    /**
     * Get parent xPath for given child xPath 
     * @param xPath XPath to get child xPath from.
     */	
    public static String getParentXPath(String xPath)
    {
        String xpParentXPath = xPath.substring(0, xPath
                .lastIndexOf("/"));
        return xpParentXPath;
    }

    /**
     * Remove DOM node by given xPath 
     * @param xPath XPath node to remove.
     */	
    public static void removeNodeByXPath(Document document,String xPath) throws XPathExpressionException
    {
        Node node = getNode(document, xPath);
        if(node != null)
            node.getParentNode().removeChild(node);
    }
    
    /**
     * Create and attach Node to DOM document by given xPath 
     * @param xPath XPath to create node from.
     */	
    public static void createNodeByXPath(Document document,String xPath)
    throws XPathExpressionException
    {
        String buildPath;
        String [] xArray = xPath.replaceAll("//", "").split("/");
        Stack<String> stack = new Stack<String>();
        
        for(int i = xArray.length; i != 0; i--)
            stack.push(xArray[i - 1]);
        
        Node rootNode = document.getFirstChild();

        if(rootNode == null)
        {
            document.appendChild(document.createElement(stack.lastElement()));
        }
        
        buildPath = "//" + stack.pop();

        
        while(!stack.empty())
        {
            String xPathElement = stack.pop();
            buildPath += "/" + xPathElement;
            Node node = getNode(document,buildPath);
            if(node != null)
            {
                rootNode = node;
                continue;
            }
            node = createElement(document, rootNode, xPathElement);
            if(node != null)
            {
                rootNode.appendChild(node);
                rootNode = node;
            }
        }
    }
    /**
     * Get single DOM Node by given xPath
     * @param xpathstr XPath to get node from.
     * @return DOM Node
     */
    public static Node getNode(Document document,String xpathstr)
    throws XPathExpressionException
    {
        XPathFactory xpFactory = XPathFactory.newInstance();
        XPath xpath = xpFactory.newXPath();
        XPathExpression expr = null;
        try {
            expr = xpath.compile(xpathstr);
        } catch (XPathExpressionException e) {
            CommonFunctions.log.error(e.getLocalizedMessage());
        }
        
        Object result = null;
        
        result = expr.evaluate(document, XPathConstants.NODE);
        
        Node node = (Node) result;
        return node;
    }
    /**
     * Creates single DOM node by given single xPath element.
     * For xPath elements such as attribute or text(), return result will be null and
     * given rootNode will be updated with text or new attribute.
     * @param rootNode DOM node to update.
     * @param xPathElement Single xPath element
     * @return DOM Node
     */
    public static Node createElement(Document document,Node rootNode, String xPathElement)
    {
        Pattern pattern = Pattern.compile("(.*)\\[.*@(.*)=.*'(.*)'.*\\]");
        Matcher matcher = pattern.matcher(xPathElement);
        if (matcher.find())
        {
            String nodeName = matcher.group(1).trim();
            String attrName = matcher.group(2).trim();
            String attrValue = matcher.group(3).trim();
            Node node = document.createElement(nodeName);
            node.getAttributes().setNamedItem(document.createAttribute(attrName));
            node.getAttributes().getNamedItem(attrName).setNodeValue(attrValue);
            return node;
        }
        
        pattern = Pattern.compile("@(.*)");
        matcher = pattern.matcher(xPathElement);
        if (matcher.find())
        {
            String attrName = matcher.group(1).trim();
            rootNode.getAttributes().setNamedItem(document.createAttribute(attrName));
            rootNode.getAttributes().getNamedItem(attrName).setNodeValue("");
            return null;
        }
        
        pattern = Pattern.compile("text()");
        matcher = pattern.matcher(xPathElement);
        if (matcher.find())
        {
            rootNode.appendChild(document.createTextNode(""));
            return null;
        }
        
        pattern = Pattern.compile("(.*)");
        matcher = pattern.matcher(xPathElement);
        if (matcher.find())
        {
            String nodeName = matcher.group(1).trim();
            Node node = document.createElement(nodeName);
            return node;
        }
        
        return null;
    }
    /**
     * Serialize DOM document to output xml file, using LSSerializer if document support features "Core","3.0" and "LS","3.0",
     * otherwise JAXP Transformer will be used for serialization.
     */
    /*public static void serialize(Document document,String configurationFolder,String configurationFile)
    throws SerializeException
    {
        FileOutputStream outStream = null;
        try {
            //testing the support for DOM Load and Save
            if(document.getFeature("Core","3.0") != null && document.getFeature("LS","3.0") != null)
            {
                Logger.getLogger("ES").log(Level.INFO, "Starting serialization " + configurationFile + " with LSSerializer");
                DOMImplementationLS lsImplementationDOM = (DOMImplementationLS)(document.getImplementation()).getFeature("LS","3.0");
                
                //get a LSSerializer object
                LSSerializer serializer = lsImplementationDOM.createLSSerializer();
                DOMConfiguration config = serializer.getDomConfig();
                try {
                    config.setParameter("format-pretty-print", Boolean.TRUE);
                } catch (Exception e) {
                    //Unfortunately this feature not supported
                }
                //get a LSOutput object
                outStream = new FileOutputStream(new File(configurationFolder, configurationFile));
                LSOutput lsOutput = lsImplementationDOM.createLSOutput();       
                lsOutput.setByteStream((OutputStream)outStream);
                //do the serialization
                serializer.write(document,lsOutput);
            }
            else
            {
                Logger.getLogger("ES").log(Level.INFO, "Starting serialization " + configurationFile + " with JAXP Transformer");
                Transformer transformer = TransformerFactory.newInstance().newTransformer();
                
                DocumentType docType = document.getDoctype();           
                String publicID = null;
                String systemID = null;
                if(docType != null)
                {
                    publicID = document.getDoctype().getPublicId();
                    systemID = document.getDoctype().getSystemId();
                    
                    if(publicID != null)
                        transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, publicID);
                    if(systemID != null)
                        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, systemID);
                }
    
                transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                transformer.setOutputProperty(OutputKeys.MEDIA_TYPE, "text/xml");
                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
                
                outStream = new FileOutputStream(new File(configurationFolder, configurationFile));
                StreamResult result = new StreamResult(outStream);
                transformer.transform(new DOMSource(document), result);
            }
            outStream.flush();
            outStream.close();
        } catch (Exception e) {
            try {
                outStream.flush();
                outStream.close();
            } catch (Exception e1) {}
            throw (new DocumentHelper()).new SerializeException();
        }
    }
    public class SerializeException extends Exception {
        private static final long serialVersionUID = -5811283100715494661L;
    }*/
}