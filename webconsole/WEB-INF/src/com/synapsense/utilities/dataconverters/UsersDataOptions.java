package com.synapsense.utilities.dataconverters;

import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.UserPreference;

public class UsersDataOptions {	
    
    private static Log log = LogFactory.getLog(UsersDataOptions.class);
    
    /**
     * Get digits from database
     * @param request
     * @return int
     */
    public static int getDecimalDigits(HttpServletRequest servletRequest) {
        
        String currentUser = (String) servletRequest.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUser == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        int decimalFormat = 2;
        try{
            TO<?> user = Env.getUserDAO().getUser(currentUser);
            String numDigits = UserPreference.getString(user, "DecimalFormat", Env.getUserDAO());
            if(numDigits != null) {
                decimalFormat = Integer.parseInt(numDigits);
            }
        }catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }
        
        return decimalFormat;
    }	

    public static char getDecimalSeparator(HttpServletRequest servletRequest) {
        
        String currentUser = (String) servletRequest.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUser == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        String decimalSeparator = null;
        try{
            TO<?> user = Env.getUserDAO().getUser(currentUser);
            decimalSeparator = UserPreference.getString(user, "DecimalSeparator", Env.getUserDAO());
        }catch (Exception e) {
            log.error(e.getLocalizedMessage(), e);
        }

        if(decimalSeparator == null || decimalSeparator.isEmpty()) {
            decimalSeparator = ".";
        }
        
        return decimalSeparator.charAt(0);
    }	
        
    public static String getTzIdFromRequest (HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        String TZ_ID = TimeZone.getDefault().getID();
        if(cookies != null) {
            for (Cookie cook:cookies) {
                if ("LOCALE_TIME_ZONE".equalsIgnoreCase(cook.getName())) {
                    TZ_ID = cook.getValue();
                    break;
                }
            }
        }
        return TZ_ID;
    }
    
    /**
     * Get date format from database
     * @param request
     * @return int
     */
    public static String getDateFormat(HttpServletRequest servletRequest) {
        String currentUser = (String) servletRequest.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUser == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        TO<?> user = Env.getUserDAO().getUser(currentUser);
        String curDateFormat = UserPreference.getString(user, "DateFormat", Env.getUserDAO());
        
        if(curDateFormat == null) {
            curDateFormat = "yyyy-MM-dd HH:mm:ss";
        }
        return curDateFormat;
    }

    /**
     * Get date format for charts: remove year and second form current date format.
     * @param servletRequest
     * @return
     */
    public static String getDateFormatForCharts(HttpServletRequest servletRequest) {
        String currentUser = (String) servletRequest.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUser == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        TO<?> user = Env.getUserDAO().getUser(currentUser);
        String curDateFormat = UserPreference.getString(user, "DateFormat", Env.getUserDAO());
        
        if(curDateFormat == null) {
            curDateFormat = "yyyy-MM-dd HH:mm:ss";
        }
        
        //try to remove year and seconds. If fail, use current date format
        try{
            curDateFormat = removeYearAndSecondsFromFormatString(curDateFormat);
        }catch(Exception e){
            log.error(e.getLocalizedMessage(), e);
        }		
        
        return curDateFormat;
    }
    
    /**
     * Removes year and seconds from format string 
     * @param formatStr
     * @return
     */
    private static String removeYearAndSecondsFromFormatString(String formatStr){
        String result = "";
        
        result = removeSymbolSequence(formatStr, "y");
        result = removeSymbolSequence(result, "s");		
                
        return result;
    }
    
    /**
     * Removes symbol sequence and one separator character. 
     * If one character exists before symbol, delete it. Else delete character after symbol.
     * @param formatStr
     * @param symbol
     * @return
     */
    private static String removeSymbolSequence(String formatStr, String symbol){
        String result = "";
        
        result = formatStr.replace(symbol.toUpperCase(), symbol);
        result = formatStr.replace(symbol.toLowerCase(), symbol);
        
        int fIndex = result.indexOf(symbol);
        int lIndex = result.lastIndexOf(symbol);
        
        
        //delete one character after/before symbol (expected separator(".", "/", "-", and so on..))		
        if(fIndex != -1){
            if(fIndex > 0){
                //remove symbol before				
                result = result.substring(0, fIndex - 1) + result.substring(lIndex + 1, result.length());
            }else{
                //remove symbol after
                result = result.substring(lIndex + 2, result.length());
            }
        }
        
        return result;
    }

}
