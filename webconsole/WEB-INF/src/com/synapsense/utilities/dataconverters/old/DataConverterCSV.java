package com.synapsense.utilities.dataconverters.old;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DataConverterCSV extends DataConverter {
    
    public DataConverterCSV (HttpServletResponse servletResponse, HttpServletRequest servletRequest){
        super(servletResponse, servletRequest);
    }
            
    protected String convertElement(String element){				
        String mappedElement = mapElement(element);
        mappedElement = '"' + mappedElement.replaceAll("\"", "\"\"") + '"';
        return mappedElement + ",";
    }		
    
    public void addElementToConstantRowPart(String element){			
        constantPart = constantPart.concat(convertElement(element));
    }
    
    public void addElementToHeader(String element){
        header = header.concat(convertElement(element));
    }	
    
    public void addLastElementToHeader(String element){
        header = header.concat(convertElement(element));
    }
    
    @Override
    public void beginBuild() throws IOException{
        //Add UTF-8 BOM
        response.getOutputStream().write(0xEF);
        response.getOutputStream().write(0xBB);
        response.getOutputStream().write(0xBF);
        response.setContentType("APPLICATION/OCTET-STREAM; charset=utf-8");
        String disHeader = "Attachment; Filename=\"data.csv\"";
        response.setHeader("Content-Disposition", disHeader);
        
        addHeader();
        endRow();
    }
    
    @Override
    protected void beginRow() throws IOException{				
        addElementToResponse(constantPart);			
    }
    
    @Override
    protected void convertAndAddRowElementToResponse(String element) throws IOException{		    
        addElementToResponse(convertElement(element));    		    	
    }
    
    @Override
    protected void endRow() throws IOException{
            response.getOutputStream().println("");    		    		
        }
}
