package com.synapsense.utilities.dataconverters.old;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.synapsense.session.SessionVariableNames;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.sensors.SensorPoint;

public class DataConverter {
    protected String constantPart = ""; 
    protected String header = "";	
    protected HashMap <String, String> elementMap = new HashMap <String, String>(); 
    protected boolean isEmpty = true;
    
    boolean isTimestampFirst = false;
    
    protected HttpServletResponse response = null;
    //OutputStream response;
    
    protected NumberFormat numberFormatter;
    protected SimpleDateFormat sdf;
        
    /**
     * Set timestamp order (first or not) in converter result
     * @param isFirst
     */
    public void setIsTimestampFieldFirst(boolean isFirst){
        isTimestampFirst = isFirst;
    }
    
    public DataConverter(HttpServletResponse servletResponse, HttpServletRequest servletRequest){
        response = servletResponse;

        String userName = (String) servletRequest.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(userName == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        
        numberFormatter = CommonFunctions.getNumberFormat(servletRequest);
        sdf = new SynapDateFormat(UsersDataOptions.getDateFormat(servletRequest),UsersDataOptions.getTzIdFromRequest(servletRequest));	
    }
    
    public void setDecimalDigits(int decimalFormat){
        numberFormatter.setMaximumFractionDigits(decimalFormat);
    }
        
    public void clearElementMap(){
        elementMap.clear();
    }
    
    public void addToElementMap(String element, String mappedValue){
        elementMap.put(element, mappedValue);
    }
    
    protected String mapElement(String element){
        String result = elementMap.get(element);		
        return (result == null ? element : result);				
    }
        
    public void addElementToHeader(String element){		
    }
    
    public void addLastElementToHeader(String element){		
    }
    
    protected void addHeader() throws IOException{
        addElementToResponse(header);
    }	
    
    public void addElementToConstantRowPart(String element){		
    }
    
    public void clearConstantRowPart(){
        constantPart = "";
    }
    
    public void beginBuild() throws IOException{
    }
    
    public void endBuild() throws IOException{		
    }
    
    protected void addElementToResponse(String element) throws IOException{
        response.getOutputStream().write(element.getBytes("UTF-8"));    					
    }
    protected String convertElement(String element){
        return element;
    }
    protected void convertAndAddRowElementToResponse(String element) throws IOException{		
    }
    
    protected void beginRow() throws IOException{		
    }
    
    protected void endRow() throws IOException{		
    }
        
    public void buildDataPart(List<SensorPoint> pointArray) throws IOException{
            for (int i = 0; i < pointArray.size(); i++){
                
                beginRow();				
                if(isTimestampFirst){
                    convertAndAddRowElementToResponse(sdf.format(pointArray.get(i).time_stamp));
                    convertAndAddRowElementToResponse(String.valueOf(numberFormatter.format(pointArray.get(i).value)));
                }else{
                    convertAndAddRowElementToResponse(String.valueOf(numberFormatter.format(pointArray.get(i).value)));
                    convertAndAddRowElementToResponse(sdf.format(pointArray.get(i).time_stamp));
                }				
                endRow();
                
                //Mark that we have any data
                isEmpty = false;
            }						
    }

}
