package com.synapsense.utilities.dataconverters.old;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DataConverterHtmlTable extends DataConverter {
    

    public DataConverterHtmlTable (HttpServletResponse servletResponse, HttpServletRequest servletRequest){
        super(servletResponse, servletRequest);
    }
    
    @Override
    protected String convertElement(String element){				
        String mappedElement = mapElement(element);
        return "<td>" + mappedElement + "</td>";
    }		
    
    @Override
    public void addElementToConstantRowPart(String element){	
        
        constantPart = constantPart.concat(convertElement(element));
    }
    
    @Override
    public void addElementToHeader(String element){		
        header = header.concat("<th>" + element + "</th>");
    }	
    
    @Override
    public void addLastElementToHeader(String element){
        addElementToHeader(element);
    }
    
    @Override
    public void beginBuild() throws IOException{		
        addElementToResponse("<table>");		
        addElementToResponse("<tr>");
        addHeader();
        addElementToResponse("</tr>");
    }
    
    @Override
    public void endBuild() throws IOException{
        addElementToResponse("</table>");
    }
    
    @Override
    protected void beginRow() throws IOException{
        addElementToResponse("<tr>");
        addElementToResponse(constantPart);			
    }
    
    @Override
    protected void convertAndAddRowElementToResponse(String element) throws IOException{		    
        addElementToResponse(convertElement(element));    		    	
    }	
    
    @Override
    protected void endRow() throws IOException{
        addElementToResponse("</tr>");   		
            response.getOutputStream().println("");    		    		
        }
}
