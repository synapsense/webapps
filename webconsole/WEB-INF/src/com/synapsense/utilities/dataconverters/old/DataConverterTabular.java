package com.synapsense.utilities.dataconverters.old;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DataConverterTabular extends DataConverterHtmlTable {
        
    protected HttpServletRequest request = null;
    protected boolean isNoDataForNode = false;
    
    public DataConverterTabular (HttpServletResponse servletResponse, HttpServletRequest servletRequest){
        super(servletResponse,servletRequest);
        request = servletRequest;
        response.setContentType("text/html; charset=utf-8");
    }		
    
    public void setEmptyRequest(){
        isNoDataForNode = true;
    }
    
    @Override
    public void addElementToHeader(String element){		
        header = header.concat("<th class=\"blueTitles\" align=\"left\" Filter=\"ALL\">" + element + "</th>");
    }
    
    @Override
    protected void beginRow() throws IOException{
        addElementToResponse("<tr class=\"fieldLabel\">");
        addElementToResponse(constantPart);			
    }
        
    protected void addHiddenInputToRespone(String parameterName, String inputId, String inputName) throws IOException{		
        String parameter = request.getParameter(parameterName);		
        addHiddenInput(parameter, inputId, inputName);
    }
    
    protected void addHiddenInput(String parameter, String inputId, String inputName) throws IOException{
        addElementToResponse("<input type=\"hidden\" id=\"" + inputId + "\" name=\"" + inputName +"\" ");
        addElementToResponse("value=\"" + parameter + "\">");
        addElementToResponse("</input>");
    }
    
    protected void addHiddenInputDateToResponse() throws IOException{
        
        String beginDateStr = request.getParameter("begin");
        String endDateStr = request.getParameter("end");
        
        if (beginDateStr != null && endDateStr != null){
            String[] beginStrs = beginDateStr.split("-");				
            String[] endStrs = endDateStr.split("-");
            
            addHiddenInput(beginStrs[0], "beginYear", "beginYear");
            addHiddenInput(endStrs[0], "endYear", "endYear");
            
            addHiddenInput(beginStrs[1], "beginMonth", "beginMonth");
            addHiddenInput(endStrs[1], "endMonth", "endMonth");
            
            addHiddenInput(beginStrs[2], "beginDay", "beginDay");
            addHiddenInput(endStrs[2], "endDay", "endDay");
        }				
    }
    
    @Override
    public void beginBuild() throws IOException{			
        addHiddenInputToRespone("selectedNode", "selectedNodeId", "selectedNodeName");				
        addHiddenInputToRespone("selectedGroupId", "selectedGroupId", "selectedGroupId");				
        
        addHiddenInputToRespone("visiblePoints", "visiblePoints", "visiblePoints");
        addHiddenInputToRespone("selectedZoomFactor", "selectedZoomFactor", "selectedZoomFactor");
        addHiddenInputToRespone("dataPoints", "dataPoints", "dataPoints");
        addHiddenInputToRespone("histdataType", "histdataType", "histdataType");
        addHiddenInputToRespone("agedDays", "agedDays", "agedDays");
        addHiddenInputToRespone("agedHours", "agedHours", "agedHours");
        addHiddenInputToRespone("agedMins", "agedMins", "agedMins");
        addHiddenInputToRespone("agedSecs", "agedSecs", "agedSecs");		
        addHiddenInputDateToResponse();
        addHiddenInputToRespone("mode", "mode", "mode");
        addHiddenInputToRespone("fromDialog", "fromDialog", "fromDialog");
        addHiddenInputToRespone("selectedDataRequests", "selectedDataRequests", "selectedDataRequests");						
        
        addElementToResponse("<table style=\"width:100%;\" border=\"1\"> <tr> <td style=\"width:100%;height:400px\" valign=\"top\" colspan=\"5\">");
        addElementToResponse("<div id=\"tableContainer\" style=\"overflow:auto;height:400px;\"> <table border=\"1\" style=\"border:1 solid #b0b0b0;width:100%;\" class=\"scrollTable\" id=\"sensorNodesTable\">");		
        addElementToResponse("<thead class=\"fixedHeader\" id=\"headerRow\"> <tr class=\"fieldLabel\">");								
        addHeader();
        addElementToResponse("</tr></thead><tbody id=\"sensorNodeDataNodes\" class=\"scrollContent\">");				
    }
    
    @Override
    public void endBuild() throws IOException{	
        //Set the label if we need
        if (isNoDataForNode == true){
            addElementToResponse("<tr><td colspan=\"5\"><label class=\"fieldLabel\"><i>No data met the specified criteria for this node</i></label></td></tr>");
        }else if (isEmpty == true){			
            addElementToResponse("<tr><td colspan=\"5\"><label class=\"fieldLabel\"><i>No data matched the specified criteria</i></label></td></tr>");
        }
        
        addElementToResponse("</tbody> </table> </div> </td> </tr>");		
        addElementToResponse("</table>");
    }

}
