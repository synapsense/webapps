package com.synapsense.utilities.dataconverters.old;


import java.io.IOException;

import java.util.List;
import java.util.Vector;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.sensors.SensorPoint;

public class XmlDataConverter extends DataConverter {
    private static Log LOG = LogFactory.getLog(XmlDataConverter.class);

    Document result = null;
    int index = 0;
    Vector<String>	header = new Vector<String>();
    Node constantRow = null;
    Node dataRow = null;
    
    public XmlDataConverter(HttpServletResponse servletResponse, HttpServletRequest servletRequest) {
        super(servletResponse, servletRequest);
        DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = null;
        try {
            documentBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        result =
            documentBuilder.newDocument();
        
        Element lines = result.createElement("lines");
        result.appendChild(lines);
    }

    public void clearConstantRowPart(){
        constantRow = null;
        index = 0;
    }

    public void addElementToHeader(String element){
        header.add(element.replace(" ", "_"));
    }
    public void addLastElementToHeader(String element){		
        header.add(element.replace(" ", "_"));
    }
    
    public void addElementToConstantRowPart(String element){
        if (constantRow == null)
            constantRow = result.createElement("line");
        Node node = null;
        node = constantRow.appendChild(result.createElement(header.elementAt(index%header.size())));
        index++;
        if (index>=header.size())
            index=0;
        //node.setTextContent(element);
        ((Element)node).setAttribute("v", element);
    }
    protected void convertAndAddRowElementToResponse(String element){
        if(dataRow == null)
            dataRow = result.createElement("data");
        Node node = dataRow.appendChild(result.createElement(header.elementAt(index)));
        index++;
        if (index>=header.size())
            index = constantRow.getChildNodes().getLength();
        //node.setTextContent(mapElement(element));
        ((Element)node).setAttribute("v", mapElement(element));
    }

    public void beginBuild() throws IOException{
        response.setContentType("text/xml; charset=utf-8");
    }
    
    protected void endRow(){
        Node toAdd = constantRow.cloneNode(true);
        for (int j=0;j<dataRow.getChildNodes().getLength();)
        {
            toAdd.appendChild(dataRow.getChildNodes().item(j));
        }
        result.getDocumentElement().appendChild(toAdd);
    }

    public void endBuild(){
        try {
            response.getWriter().print(XMLHelper.getDocumentXml(result));
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }
        
    public Document getXmlDocument(){
        return this.result;
    }
    
    public void buildDataPart(List<SensorPoint> pointArray){			
        
        try{								
            for (int i = 0; i < pointArray.size(); i++){
                
                beginRow();				
                if(isTimestampFirst){
                    convertAndAddRowElementToResponse(sdf.format(pointArray.get(i).time_stamp));
                    convertAndAddRowElementToResponse(String.valueOf(numberFormatter.format(pointArray.get(i).value)));
                    //hack: we should add time in long to the xmlConverter..
                    convertAndAddRowElementToResponse(String.valueOf(pointArray.get(i).time_stamp));
                }else{
                    convertAndAddRowElementToResponse(String.valueOf(numberFormatter.format(pointArray.get(i).value)));
                    convertAndAddRowElementToResponse(sdf.format(pointArray.get(i).time_stamp));
                    //hack: we should add time in long to the xmlConverter..
                    convertAndAddRowElementToResponse(String.valueOf(pointArray.get(i).time_stamp));
                }				
                endRow();
                
                //Mark that we have any data
                isEmpty = false;
            }			
        }catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
        }
                
    }
}
