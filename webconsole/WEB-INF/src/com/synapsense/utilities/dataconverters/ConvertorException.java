package com.synapsense.utilities.dataconverters;

public class ConvertorException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = -4896250956350393455L;

    ConvertorException(String message, Exception e){
        super(message, e);
    }		
}
