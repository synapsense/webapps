package com.synapsense.utilities.dataconverters;

public class HtmlTableConvertor extends Convertor {
    
    protected void addHeader() throws ConvertorException{
        addElementToResponse("<tr>");
        for(int i = 0; i < m_header.length; i++){
            addElementToResponse( "<th>" + m_header[i] + "</th>");
        }
        addElementToResponse("</tr>");
    }
    
    protected void beginBuild() throws ConvertorException{
        addElementToResponse("<table>");
        addHeader();
    }
    
    protected void endBuild() throws ConvertorException{
        addElementToResponse("</table>");
    }				
    
    protected String convertElement(String element){
        return "<td>" + element + "</td>";
    }
    
    protected String convertLastRowElement(String element){
        return convertElement(element);
    }
    
    protected void writeRowBegin() throws ConvertorException{
        addElementToResponse("<tr>");
    }
    
    protected void writeRowEnd() throws ConvertorException{
        addElementToResponse("</tr>");
    }
}
