package com.synapsense.utilities.dataconverters;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public abstract class Convertor {
    private HashMap<String, String> m_buildingRow = new HashMap<String, String>();
    private HashMap<String, XPathExpression> m_expressionMap = new HashMap<String, XPathExpression>();
            
    protected String[] m_header = null;	
    protected HttpServletResponse m_response = null;
    protected String m_objectName = "CRAC";		
    
    public void setHeader(String[] header){ m_header = header; }	
    public void setResponse(HttpServletResponse response){ m_response = response; }	
    public void setObjectName(String objName){ m_objectName = objName; }		
    
    //---------- Protected methods. Should be implemented in each sub-class
    abstract protected void addHeader() throws ConvertorException;	
    abstract protected void beginBuild() throws ConvertorException;	
    abstract protected void endBuild() throws ConvertorException;	
    abstract protected String convertElement(String element) throws ConvertorException;
    
    abstract protected String convertLastRowElement(String element) throws ConvertorException;
    
    abstract protected void writeRowBegin() throws ConvertorException;	
    abstract protected void writeRowEnd() throws ConvertorException;
    
    //------------- Final methods (Base logic) -------------------
    public final void convertXMLDoc(Document doc) throws XPathExpressionException, ConvertorException{										
        
        NodeList objects = doc.getElementsByTagName(m_objectName);
        
        beginBuild();
        
        //for each object (for ex, CRAC)
        for(int  i = 0; i < objects.getLength(); i++){
                                                            
            Set<String> keys = m_expressionMap.keySet(); //column names
            Iterator<String> it = keys.iterator();
            
            beginRow();			
            while(it.hasNext()){
                String name = it.next();// column name
                XPathExpression  xPathExpression = m_expressionMap.get(name);		
                String value = xPathExpression.evaluate(objects.item(i));
                
                addElementToRow(name, value);
            }			
            endRow();						
        }				
        
        endBuild();
    }
    
    public final void setNamesMap(HashMap<String, String> map) throws XPathExpressionException{
        m_expressionMap.clear();
        
        Set<String> keys = map.keySet(); //column names
        Iterator<String> it = keys.iterator();
        
        //-- xPath
        XPathFactory  factory = XPathFactory.newInstance();
        XPath xPath = factory.newXPath();						
        
        while(it.hasNext()){
            String name = it.next();
            XPathExpression  xPathExpression = xPath.compile(map.get(name));
            m_expressionMap.put(name, xPathExpression);			
        }
    }
    
    protected final void addElementToResponse(String element) throws ConvertorException{
        try
        {    		
            m_response.getOutputStream().print(element);    					
        }
        catch (Exception e)
        {    		
            throw new ConvertorException("Can't print into servlet response", e);    	
        }		
    }

    private void beginRow() throws ConvertorException{
        //clear building row
        m_buildingRow.clear();
        
        //write row beginning
        writeRowBegin();		
    }
    
    private void endRow() throws ConvertorException{				
        //write building row to output stream in header order
        for(int i = 0; i < m_header.length - 1; i++){			
            String value = m_buildingRow.get(m_header[i]);
            addElementToResponse(convertElement(value));
        }
        
        //add last row element
        String value = m_buildingRow.get(m_header[m_header.length - 1]);
        addElementToResponse(convertLastRowElement(value));
        
        //write row end
        writeRowEnd();
    }
    
    private void addElementToRow(String headName, String value){
        m_buildingRow.put(headName, value);
    }						
    
    //------------ Static methods --------------------
    public static final void setDataFromXmlConfig(Convertor convertor, String objectName, String fileName) throws XPathExpressionException, ParserConfigurationException, SAXException, IOException{
        //Get XML-configuration document				
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setIgnoringComments(true);
        factory.setCoalescing(true); // Convert CDATA to Text nodes
        factory.setNamespaceAware(false); // No namespaces: this is default
        factory.setValidating(false); // Don't validate DTD: also default
        DocumentBuilder docBuilder = factory.newDocumentBuilder();
        
        File confFile = new File(fileName);
        Document confDoc = docBuilder.parse(confFile);	                                                    
        
        //Compile xPath expressions
        XPathFactory  factoryX = XPathFactory.newInstance();
        XPath xPath = factoryX.newXPath();		
        XPathExpression  xPathName = xPath.compile("name");
        XPathExpression  xPathPath = xPath.compile("xpath");				
                
        //Get list of object configuration				
        XPathExpression  xPathColumn = xPath.compile("config/tabular/" + objectName + "/column");						
        NodeList objectsList = (NodeList)xPathColumn.evaluate(confDoc, XPathConstants.NODESET);			
        
        //Gather data
        HashMap<String, String> namesMap = new HashMap<String, String>();
        String []header = new String[objectsList.getLength()];
        
        for(int i = 0; i < objectsList.getLength(); i++){
            String name = xPathName.evaluate(objectsList.item(i));
            String xPathStr = xPathPath.evaluate(objectsList.item(i));
            
            header[i] = name;
            namesMap.put(name, xPathStr);
        }				                														
        
        //Apply data to convertor
        convertor.setNamesMap(namesMap);		
        convertor.setObjectName(objectName);		
        convertor.setHeader(header);
    }		
    
}
