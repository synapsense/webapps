package com.synapsense.utilities.dataconverters;

public class CSVConvertor extends Convertor {

    @Override
    protected void addHeader() throws ConvertorException {		
        for(int i = 0; i < m_header.length - 1; i++){
            addElementToResponse(convertElement(m_header[i]));
        }
        addElementToResponse(m_header[m_header.length - 1]);
        
        writeRowEnd();
    }

    @Override
    protected void beginBuild() throws ConvertorException {
        m_response.setContentType("APPLICATION/OCTET-STREAM");
        String disHeader = "Attachment; Filename=\"data.csv\"";
        m_response.setHeader("Content-Disposition", disHeader);
                        
        addHeader();
    }

    @Override
    protected String convertElement(String element) throws ConvertorException {		
        return element + ",";
    }
    
    @Override
    protected String convertLastRowElement(String element) throws ConvertorException{
        return element;
    }

    @Override
    protected void endBuild() throws ConvertorException {
        //Nothing to do
    }

    @Override
    protected void writeRowBegin() throws ConvertorException {
        //Nothing to do
    }

    @Override
    protected void writeRowEnd() throws ConvertorException {
        try
        {    		
            m_response.getOutputStream().println("");    		    		
        }
        catch (Exception e)
        {
            throw new ConvertorException("Can't print into servlet response", e);
        }
    }

}
