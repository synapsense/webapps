package com.synapsense.ESConfigurator.Mailer;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;

public class Mailer {
    // private String filePath = null;
    private String host;
    private int smtp_port = 25;
    private String body = "ESConfigurator: default message";
    private String subject = "ESConfigurator test mail.";
    private String user_name = "DefaultUserName";
    private String user_password = "DefaultPassword";
    private String from;
    private String to;
    private boolean auth = false;
    private boolean encr = false;

    private MimeMessage message;

    private static String ENCODING = "UTF-8";

    public void setConnectionData(String host, int port) {
        this.host = host;
        smtp_port = port;
    }

    public void defineMessage(String from, String to, String subject, String body) throws MailerException {
        this.from = from;
        this.to = to;
        this.subject = subject;
        this.body = body;
    }

    public void setAuthenticationData(boolean authenticationRequired, String userName, String userPassword) {
        auth = authenticationRequired;
        user_name = userName;
        user_password = userPassword;
    }

    public void setEncryptionRequired(boolean encryptionRequired, String filePath) {
        // this.filePath = filePath;
        encr = encryptionRequired;
    }

    public void setEncryptionRequired(boolean encryptionRequired) {
        encr = encryptionRequired;
    }

    private void applyConfig() throws MailerException {

        Session session = null;

        Properties properties = new Properties();
        properties.setProperty("mail.smtp.connectiontimeout", "10000");
        // Connection data
        properties.setProperty("mail.smtp.host", host);
        properties.setProperty("mail.smtp.port", new Integer(smtp_port).toString());

        if (encr) {
            properties.setProperty("mail.smtp.ssl.enable", Boolean.TRUE.toString());
            // JBoss 7 doesn't support tls
            // properties.setProperty("mail.smtp.starttls.enable",Boolean.TRUE.toString());
            // properties.setProperty("mail.smtp.starttls.required",Boolean.TRUE.toString());
            properties.setProperty("mail.smtp.socketFactory.fallback", Boolean.FALSE.toString());
            // properties.put("javax.net.ssl.trustStore", filePath);
            properties.setProperty("mail.smtp.ssl.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.setProperty("mail.smtp.ssl.socketFactory.port", new Integer(smtp_port).toString());
        }

        // Authentication
        properties.setProperty("mail.smtp.auth", new Boolean(auth).toString());
        if (auth) {
            session = Session.getInstance(properties, new Authenticator());
        } else {
            session = Session.getInstance(properties);
        }
        message = new MimeMessage(session);

        // Define message
        try {
            message.setRecipient(RecipientType.TO, new InternetAddress(to));
            message.setFrom(new InternetAddress(from));
            message.setSubject(subject, ENCODING);
            message.setContent(body, "text/plain; charset=" + ENCODING);
        } catch (MessagingException e) {
            throw new MailerException("Can't define new message", e);
        }
    }

    public void send() throws MailerException {
        try {
            applyConfig();
            Transport.send(message);
        } catch (MessagingException e) {
            throw new MailerException("Can't send message", e);
        }
    }

    private class Authenticator extends javax.mail.Authenticator {
        protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(user_name, user_password);
        }
    }

    public class MailerException extends Exception {

        private static final long serialVersionUID = 3165548082456429712L;

        public MailerException(String message) {
            super(message);
        }

        public MailerException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
