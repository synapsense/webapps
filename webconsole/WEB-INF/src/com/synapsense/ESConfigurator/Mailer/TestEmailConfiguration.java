package com.synapsense.ESConfigurator.Mailer;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.ESConfigurator.Mailer.Mailer.MailerException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.lang.LangManager;

public class TestEmailConfiguration extends BaseHttpServlet {

    private static final long serialVersionUID = 5895676074646630675L;

    private static final Log LOG = LogFactory.getLog(TestEmailConfiguration.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String result = "";
        try {
            String host = request.getParameter("smtpHost");
            request.getParameter("smtpPort");
            int port = 25;
            try {
                port = Integer.parseInt(request.getParameter("smtpPort"), 10);
            } catch (NumberFormatException e) {
                LOG.warn("Property smtpPort not set. Using default: 25");
            }

            String from = request.getParameter("mailFrom");
            String to = new String(from);// request.getParameter("xpMailTo");
            String user = request.getParameter("smtpUser");
            String password = request.getParameter("smtpPassword");
            String autStr = request.getParameter("authRequired");
            boolean authenticationRequired = false;
            if (autStr != null && !autStr.equals("") && !"false".equals(autStr)) {
                authenticationRequired = true;
            }
            
            boolean encryptedConn = "true".equals(request.getParameter("encryptedConn"));

            Mailer mailer = new Mailer();
            mailer.setConnectionData(host, port);
            mailer.defineMessage(
                    from,
                    to,
                    LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request),
                            "text_test_email_subject"),
                    LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request),
                            "text_test_email_body")
                            + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime()));

            mailer.setAuthenticationData(authenticationRequired, user, password);
            mailer.setEncryptionRequired(encryptedConn);
            mailer.send();
            result = ErrorMessages.SUCCESS;
        } catch (MailerException e) {			
            LOG.error(e.getMessage(), e);
            result = ErrorMessages.ERROR_OCCURRED_WHILE_SENDING;
        }
        sendResponse(response, "<result>" + result + "</result>");
    }
}