package com.synapsense.ESConfigurator.utils;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.BaseHttpServlet;

import java.sql.*;

public class TestDBConnection extends BaseHttpServlet {
    private static final long serialVersionUID = -7559216554820668930L;
    private static final Log LOG = LogFactory.getLog(TestDBConnection.class);

    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        LOG.info("In get");
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {

        LOG.info("Test DB connection...");
        try
        {
            String url = request.getParameter("url");
            String user = request.getParameter("user");
            String pass = request.getParameter("pass");

//            Class.forName("com.mysql.jdbc.Driver");
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

            try (Connection conn = DriverManager.getConnection(url, user, pass)) {
                sendResponse(response, "<success>true</success>");
            }
        } catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
            sendResponse(response, "<success>false</success>");
        }
    }
}
