package com.synapsense.ESConfigurator.utils;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.liveimaging.LIConfig;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class EscFormSetup extends BaseHttpServlet {
    private static final long serialVersionUID = 9030484444437066806L;
    private static final Log LOG = LogFactory.getLog(EscFormSetup.class);
    
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
    
    protected void doPost(HttpServletRequest request,
        HttpServletResponse response) throws ServletException, IOException {
        try
        {
            Document resultDoc = XMLHelper.createNewXmlDocument("escformsetup");
            Node rootNode = resultDoc.getFirstChild();
            Node visibilityNode  = XMLHelper.createXmlTextNode(resultDoc, "visibility", null);
            rootNode.appendChild(visibilityNode);
            Node successNode = XMLHelper.createXmlTextNode(resultDoc, "success", "true");
            rootNode.appendChild(successNode);
            
            
            //Setup forms
            setupPowerImagingConfigForm(visibilityNode, request);
            setupLiveImagingConfigForm(visibilityNode, request);
    
            sendResponse(response, resultDoc);
        } catch(Exception e){
            LOG.error(e.getLocalizedMessage(), e);
            sendResponse(response, "<escformsetup><success>false</success></<escformsetup>");
        }
    }
    
    protected void setTabVisibility(String tabID, boolean visible, Node visibilityNode) {
        Node tabVisibleNode = XMLHelper.createXmlTextNode(visibilityNode.getOwnerDocument(), tabID,  visible ? "true" : "false");
        visibilityNode.appendChild(tabVisibleNode);
    }
    
    // Setup forms functions
    protected void setupPowerImagingConfigForm(Node visibilityNode, HttpServletRequest request) {        
        setTabVisibility("powerimagingconfigform", !Env.getEnv(request).getObjectsByType(EnvironmentTypes.TYPE_POWER_RACK).isEmpty(), visibilityNode);
    }
    
    protected void setupLiveImagingConfigForm(Node visibilityNode, HttpServletRequest request) {
        boolean visible = LIConfig.isLiConfigExists(request, null);
        setTabVisibility("liconfigform", visible, visibilityNode);
    }
}
