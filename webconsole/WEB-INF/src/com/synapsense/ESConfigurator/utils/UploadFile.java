package com.synapsense.ESConfigurator.utils;

import org.apache.commons.fileupload.*;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.BaseHttpServlet;

/**
 * Servlet implementation class UploadFile
 */

public class UploadFile extends BaseHttpServlet {
    private static final long serialVersionUID = -1401896186259853169L;
    private static final Log LOG = LogFactory.getLog(UploadFile.class);
    
    private static Map<String, byte[]> uploadedFilesMap = new HashMap<String, byte[]>();

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        LOG.info("In get");
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        LOG.info("Start file(s) upload");
/*
        User u = null;
        StringBuilder msg = new StringBuilder();
        u = Authenticator.authenticateUser(request, msg);

        if (u == null) {
            LOG.info(msg.toString());
            return;
        }
*/
        String fileId = request.getParameter("fileId");
        String fileContent = request.getParameter("fileContent");
        
        if (fileContent == null) {
            try {
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);
                if (isMultipart) {
                    // Create a factory for disk-based file items
                    FileItemFactory factory = new DiskFileItemFactory();

                    // Create a new file upload handler
                    ServletFileUpload upload = new ServletFileUpload(factory);

                    // Parse the request
                    @SuppressWarnings("unchecked")
                    List<FileItem> items = (List<FileItem>) upload.parseRequest(request);

                    Iterator<FileItem> iter = items.iterator();

                    while (iter.hasNext()) {
                        FileItem item = (FileItem) iter.next();
                        String fileName = item.getName();
                        String contentType = item.getContentType();
                        long sizeInBytes = item.getSize();

                        if (fileName != null && !fileName.equals("")
                                && sizeInBytes != 0) {
                            LOG.info("File name :" + " " + fileName + "  "
                                    + "Content Type :" + contentType + " "
                                    + "Size in KB :" + sizeInBytes / 1024);
                            byte[] array = item.get();

                            uploadedFilesMap.put(fileId, array);
                            LOG.info("Upload of file " + fileName
                                    + "was completed successfully");
                        }
                    }
                }
            } catch (FileUploadException e) {
                LOG.error(e.getMessage(), e);
            }
        } else {
            try {
                String[] byteStrings = fileContent.split(",");
                byte[] data = new byte[byteStrings.length];

                for (int i = 0; i < byteStrings.length; i++) {
                    data[i] = Byte.parseByte(byteStrings[i]);
                }

                uploadedFilesMap.put(fileId, data);
                sendResponse(response, "<success>true</success>");
                LOG.info("Keystore file uploaded via doRequest successfully.");
            } catch (Exception e) {
                sendResponse(response, "<success>false</success>");
                LOG.error(e.getMessage(), e);
            }          
        }
    }

    public static byte[] getUploadedFile(String fileId){
        return uploadedFilesMap.get(fileId);
    }
    
    public static void deleteUploadedFile(String fileId){
        uploadedFilesMap.remove(fileId);
    }
    
    public static Collection<byte[]> getUploadedFiles () {
        return uploadedFilesMap.values();
    }
    
    public static void resetUploadedFiles () {
        uploadedFilesMap.clear();
    }
}