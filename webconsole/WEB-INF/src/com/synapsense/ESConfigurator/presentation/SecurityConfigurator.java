package com.synapsense.ESConfigurator.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.ESConfigurator.utils.UploadFile;
import com.synapsense.dataAccess.Env;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ConsoleConfigurationData;
import com.synapsense.utilities.JbossManagementAPI;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.activityLog.ESConfiguratorActivityLog;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.ComponentsNameConstants;

public class SecurityConfigurator extends BaseHttpServlet {

    final String SECURE_MODE_NO_ENCRYPTION = "0";
    final String SECURE_MODE_AUTH_ONLY = "1";
    final String SECURE_MODE_ALL = "2";

    private static final long serialVersionUID = -6756723268442446778L;

    private final String keystoreFileName = "server.keystore";
    private static final Log LOG = LogFactory.getLog(SmtpConfigurator.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestType = request.getParameter("TYPE").toLowerCase();

        if (requestType.equals("load")) {
            load(request, response, false, false);
        } else if (requestType.equals("save")) {
            save(request, response);
        }
    }

    protected void load(HttpServletRequest request, HttpServletResponse response, boolean httpConnectorReloaded, boolean httpsConnectorReloaded) {
        JbossManagementAPI jbossUtil = null;
        try {
            Document resultDoc = XMLHelper.createNewXmlDocument("config");
            Element root = resultDoc.getDocumentElement();
            jbossUtil = JbossManagementAPI.createFromRequest(request);

            String httpEnabled = jbossUtil.getAttribute(JbossManagementAPI.HTTP_CONNECTOR_ENABLED);
            String httpsEnabled = jbossUtil.getAttribute(JbossManagementAPI.HTTPS_CONNECTOR_ENABLED);
            String secureMode = SECURE_MODE_NO_ENCRYPTION;
            if ("true".equals(httpEnabled) && "true".equals(httpsEnabled)) {
                secureMode = SECURE_MODE_AUTH_ONLY;
            } else if (!"true".equals(httpEnabled) && "true".equals(httpsEnabled)) {
                secureMode = SECURE_MODE_ALL;
            }
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "secureMode", secureMode));

            String securePort = jbossUtil.getAttribute(JbossManagementAPI.HTTPS_PORT);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "securePort", securePort));

            String defaultHttpPort = jbossUtil.getAttribute(JbossManagementAPI.HTTP_PORT);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "defaultHttpPort", defaultHttpPort));

            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "keystoreFileName", keystoreFileName));

            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "keystorePassword",
                    jbossUtil.getAttribute(JbossManagementAPI.KEYSTORE_PASSWORD)));

            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "httpConnectorReloaded", Boolean.toString(httpConnectorReloaded)));
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "httpsConnectorReloaded", Boolean.toString(httpsConnectorReloaded)));
            
            sendResponse(response, resultDoc);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        } finally {
            if (jbossUtil != null) {
                try {
                    jbossUtil.close();
                } catch (IOException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    protected void save(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        JbossManagementAPI jbossUtil = null;
        try {
            String secureMode = request.getParameter("secureMode");
            // String keystoreUploaded =
            // request.getParameter("keystoreUploaded");

            jbossUtil = JbossManagementAPI.createFromRequest(request);

            boolean httpEnabled;
            boolean httpsEnabled;

            if (SECURE_MODE_AUTH_ONLY.equals(secureMode)) {
                httpEnabled = true;
                httpsEnabled = true;
            } else if (SECURE_MODE_ALL.equals(secureMode)) {
                httpEnabled = false;
                httpsEnabled = true;
            } else {
                httpEnabled = true;
                httpsEnabled = false;
            }
            
            boolean httpWasEnabled = Boolean.parseBoolean(jbossUtil.getAttribute(JbossManagementAPI.HTTP_CONNECTOR_ENABLED));
            boolean httpsWasEnabled = Boolean.parseBoolean(jbossUtil.getAttribute(JbossManagementAPI.HTTPS_CONNECTOR_ENABLED));
            
            boolean reloadHttp = false;
            boolean reloadHttps = false;
            
            if(httpEnabled != httpWasEnabled){
                reloadHttp = true;
            }
            
            if(httpsEnabled != httpsWasEnabled
                    || (!request.getParameter("securePort").equals(jbossUtil.getAttribute(JbossManagementAPI.HTTPS_PORT)))
                    || "true".equals(request.getParameter("keystoreUploaded"))){				
                reloadHttps = true;
            }

            jbossUtil.setAttribute(JbossManagementAPI.HTTP_CONNECTOR_ENABLED, Boolean.toString(httpEnabled));
            jbossUtil.setAttribute(JbossManagementAPI.HTTPS_CONNECTOR_ENABLED, Boolean.toString(httpsEnabled));
            // jbossUtil.setSystemProperty(this.propertyPrefix + "secureMode",
            // secureMode);
            if (httpsEnabled) {
                String securePort = request.getParameter("securePort");
                String keystorePassword = request.getParameter("keystorePassword");
                jbossUtil.setAttribute(JbossManagementAPI.HTTPS_PORT, securePort);
                jbossUtil.setAttribute(JbossManagementAPI.KEYSTORE_PASSWORD, keystorePassword);
            }

            updateKeystoreFile(request);

            // ACTIVITY LOG
            ESConfiguratorActivityLog.logRecord(request, ComponentsNameConstants.COMPONENT_SECURITY,
                    ActivityLogConstans.MODULE_SECURITY,
                    "activity_log_control_" + ActivityLogConstans.MODULE_SECURITY.toLowerCase()
                            + "_configuration_was_changed");
            
            jbossUtil.reloadConnectors(reloadHttp, reloadHttps);
            
            ConsoleConfigurationData.update(Env.getEnv(request)); // update console URL in configuration data
            
            if((httpEnabled && reloadHttp) || (httpsEnabled && reloadHttps)){
                // If connector should be created just wait sometime to let it start (since I don't want logic for LifeCycle Listener)
                Thread.sleep(10000);
            }
            
            load(request, response, reloadHttp, reloadHttps);
            
            jbossUtil.close();
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        } finally {
            if (jbossUtil != null) {
                try {
                    jbossUtil.close();
                } catch (IOException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    private boolean updateKeystoreFile(final HttpServletRequest request) {
        String keystoreFileId = request.getParameter("keystoreFileId");
        LOG.info("keystoreFileId: " + keystoreFileId);

        byte[] keystoreFile = UploadFile.getUploadedFile(keystoreFileId);
        if (keystoreFile == null) {
            return false;
        }
        FileWriter fileWR = Env.getFileWritter();
        try {
            fileWR.writeFileBytes(keystoreFileName, keystoreFile, WriteMode.REWRITE);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return true;
    }
}