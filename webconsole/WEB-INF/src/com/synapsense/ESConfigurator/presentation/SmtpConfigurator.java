package com.synapsense.ESConfigurator.presentation;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.JbossManagementAPI;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.activityLog.ESConfiguratorActivityLog;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.ComponentsNameConstants;

public class SmtpConfigurator extends BaseHttpServlet {

    private static final long serialVersionUID = 1L;

    private static final Log LOG = LogFactory.getLog(SmtpConfigurator.class);

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        doGet(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        String requestType = request.getParameter("TYPE").toLowerCase();

        if (requestType.equals("load")) {
            load(request, response);
        } else if (requestType.equals("save")) {
            save(request, response);
        }
    }

    protected void load(HttpServletRequest request, HttpServletResponse response) {
        JbossManagementAPI jbossUtil = null;
        try {
            Document resultDoc = XMLHelper.createNewXmlDocument("config");
            Element root = resultDoc.getDocumentElement();
            jbossUtil = JbossManagementAPI.createFromRequest(request);

            String smtpHost = jbossUtil.getAttribute(JbossManagementAPI.SMTP_HOST);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "smtpHost", smtpHost));

            String smtpPort = jbossUtil.getAttribute(JbossManagementAPI.SMTP_PORT);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "smtpPort", smtpPort));

            String mailFrom = jbossUtil.getAttribute(JbossManagementAPI.MAIL_FROM);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "mailFrom", mailFrom));

            String encryptedConn = jbossUtil.getAttribute(JbossManagementAPI.SMTP_SSL);
            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "encryptedConn", encryptedConn));

            String authRequired;

            String user = jbossUtil.getAttribute(JbossManagementAPI.SMTP_USERNAME);
            if (user != null) {
                root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "smtpUser", user));

                String password = jbossUtil.getAttribute(JbossManagementAPI.SMTP_PASSWORD);
                root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "smtpPassword", password));

                authRequired = "true";
            } else {
                authRequired = "false";
            }

            root.appendChild(XMLHelper.createXmlTextNode(resultDoc, "authRequired", authRequired));

            sendResponse(response, resultDoc);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        } finally {
            if (jbossUtil != null) {
                try {
                    jbossUtil.close();
                } catch (IOException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
        }
    }

    protected void save(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        JbossManagementAPI jbossUtil = null;
        try {

            String encryptedConn = request.getParameter("encryptedConn");
            String smtpHost = request.getParameter("smtpHost");
            String smtpPort = request.getParameter("smtpPort");
            String mailFrom = request.getParameter("mailFrom");
            String authRequired = request.getParameter("authRequired");

            jbossUtil = JbossManagementAPI.createFromRequest(request);

            jbossUtil.setAttribute(JbossManagementAPI.SMTP_SSL, encryptedConn);
            jbossUtil.setAttribute(JbossManagementAPI.SMTP_HOST, smtpHost);
            jbossUtil.setAttribute(JbossManagementAPI.SMTP_PORT, smtpPort);
            jbossUtil.setAttribute(JbossManagementAPI.MAIL_FROM, mailFrom);

            if ("true".equals(authRequired)) {
                String smtpPassword = request.getParameter("smtpPassword");
                String smtpUser = request.getParameter("smtpUser");
                jbossUtil.setAttribute(JbossManagementAPI.SMTP_PASSWORD, smtpPassword);
                jbossUtil.setAttribute(JbossManagementAPI.SMTP_USERNAME, smtpUser);
            } else {
                jbossUtil.undefineAttribute(JbossManagementAPI.SMTP_PASSWORD);
                jbossUtil.undefineAttribute(JbossManagementAPI.SMTP_USERNAME);
            }

        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        } finally {
            if (jbossUtil != null) {
                try {
                    jbossUtil.close();
                } catch (IOException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
        }

        // ACTIVITY LOG
        ESConfiguratorActivityLog
                .logRecord(request, ComponentsNameConstants.COMPONENT_EMAIL, ActivityLogConstans.MODULE_EMAIL,
                        "activity_log_control_" + ActivityLogConstans.MODULE_EMAIL.toLowerCase()
                                + "_configuration_was_changed");
        load(request, response);
        // jbossUtil.reloadServer();
    }
}