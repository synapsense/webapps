package com.synapsense.ESConfigurator.presentation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.ESConfigurator.mappers.AuditMapper;
import com.synapsense.ESConfigurator.mappers.AuditMapper.AuditMapperException;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.ComponentsNameConstants;

public class AuditConfigurationMapper extends ConfigurationMapper {
    public AuditConfigurationMapper() {
        super(ComponentsNameConstants.COMPONENT_AUDITORS,ActivityLogConstans.MODULE_AUDITORS);
        // TODO Auto-generated constructor stub
    }
    
    private static final long serialVersionUID = 5895676074646630675L;
    
    private final String tabID = "ESConfigurator/ID_AUDITOR_TAB_TITLE";
    private String docType = null;
    private final String auditPrefix = "./";
    private final String auditBackupFilePrefix = "./";
    private final String auditFileName = "audit.xml";
    
    private final String xpConfigComments = "//configuration/comment()";
    
    private final String[][][] auditParamMapping = {
            //{"xpHAHost","//configuration/HostAuditor/@hostName"},
            //{"xpHATimeout","//configuration/HostAuditor/@timeout"},
            
            {
                {"xpRHost","//configuration/RmiServiceAuditor/@host"},
                {"xpRPort","//configuration/RmiServiceAuditor/@port"},
                {"xpRRefName","//configuration/RmiServiceAuditor/@refName"},
            },
            
            {
                {"xpWSHost","//configuration/WebServerAuditor/@webServer"},
                {"xpWSTimeout","//configuration/WebServerAuditor/@timeout"},
                {"xpWSPort","//configuration/WebServerAuditor/@port"},
                {"xpWSPage","//configuration/WebServerAuditor/@page"},
                {"xpWSMsg","//configuration/WebServerAuditor/@msg"},
            },
            
            {
                {"xpSVHost","//configuration/LiveImagingAuditor/@webServer"},
                {"xpSVTimeout","//configuration/LiveImagingAuditor/@timeout"},
                {"xpSVPort","//configuration/LiveImagingAuditor/@port"},
                {"xpSVPage","//configuration/LiveImagingAuditor/@page"},
                {"xpSVMsg","//configuration/LiveImagingAuditor/@msg"},
            },
            
            {
                {"xpDSName","//configuration/MySQLAuditor/@datasourceName"}
            }
    };
            
    @Override
    public void init()
        throws ServletException {
        super.LOG = LogFactory.getLog(AuditConfigurationMapper.class);
        super.fileName = this.auditFileName;
        super.prefix = this.auditPrefix;
        super.backupFilePrefix = this.auditBackupFilePrefix;
        super.tabID = this.tabID;
    }
    
    @Override
    protected Document parseDoc (Document document,HttpServletRequest request) 
        throws ParseLoadingDocumentException {
        try {
            Document resultDoc = XMLHelper.createNewXmlDocument("config");
            Node rootNode = resultDoc.getFirstChild();
            
            AuditMapper auditMapper = new AuditMapper();
            for (int i=0;i<auditParamMapping.length;i++) {
                String[] result = auditMapper.getValue(document, auditParamMapping[i],xpConfigComments);
                String xpAudit = auditParamMapping[i][0][1].substring(0, auditParamMapping[i][0][1].lastIndexOf("/"));
                String auditName = xpAudit.substring(xpAudit.lastIndexOf("/")+1, xpAudit.length());
                Node paramNode = XMLHelper.createXmlTextNode(resultDoc, auditName, result[0]);
                rootNode.appendChild(paramNode);
                for (int j=0;j<auditParamMapping[i].length;j++) {
                    paramNode = XMLHelper.createXmlTextNode(resultDoc, auditParamMapping[i][j][0], result[j+1]);
                    rootNode.appendChild(paramNode);
                }
            }
            return resultDoc;
        } catch (ParserConfigurationException e) {
            throw new ParseLoadingDocumentException("Error occured while parsing document",e);
        } catch (AuditMapperException e) {
            throw new ParseLoadingDocumentException("Error occured while getting value from loading document",e);
        }
    }
    
    @Override
    protected Boolean saveDoc (Document document,Document returnDoc,HttpServletRequest req) 
        throws SaveDocumentException, ServletException {
        Node rootNode = returnDoc.getFirstChild();
        
        String auditName = null;
        try {
            boolean docChanged = false;
            AuditMapper auditMapper = new AuditMapper();
            for (int i=0;i<auditParamMapping.length;i++) {
                String xpAudit = auditParamMapping[i][0][1].substring(0, auditParamMapping[i][0][1].lastIndexOf("/"));
                auditName = xpAudit.substring(xpAudit.lastIndexOf("/")+1, xpAudit.length());
                
                com.synapsense.ESConfigurator.mappers.AuditMapper.Result result = auditMapper.setValue(document, req, auditParamMapping[i],xpConfigComments);
                
                Node paramNode = XMLHelper.createXmlTextNode(returnDoc, auditName, result.getData()[0]);
                rootNode.appendChild(paramNode);
                if (result.isChanged()) {
                    docChanged = true;
                }
                for (int j=0;j<auditParamMapping[i].length;j++) {
                    paramNode = XMLHelper.createXmlTextNode(returnDoc, auditParamMapping[i][j][0], result.getData()[j+1]);
                    rootNode.appendChild(paramNode);
                }
            }
            return docChanged;
        } catch (AuditMapperException e) {
            throw new SaveDocumentException("Can't set value for " + auditName,e);
        }
    }
    
    @Override
    protected boolean loadConfigurationFile (HttpServletRequest request,String backupFile,StringBuilder originFileBuffer) 
    throws ConfigurationFileLoadingException {
        StringBuilder file = new StringBuilder();
        boolean result = super.loadConfigurationFile(request, backupFile, file);
        String confFile = file.toString();
        /*
        //Get xml declaration if exist.
        //Just for haven't of problems later.
        final String xmlDeclrBegin = "<?xml";
        final String xmlDeclrEnd = "?>";
        int startInd = confFile.indexOf(xmlDeclrBegin);
        int endInd = confFile.indexOf(xmlDeclrEnd,startInd+xmlDeclrBegin.length());
        if (this.xmlDeclaration == null && startInd != -1 && endInd != -1) {
            this.xmlDeclaration = confFile.substring(startInd, endInd+xmlDeclrEnd.length());
            confFile = confFile.substring(endInd+xmlDeclrEnd.length());
        }*/
        
        //Get document type.
        final String docTypeConstBegin = "<!DOCTYPE";
        final String docTypeConstEnd = "]>";
        int startInd = confFile.indexOf(docTypeConstBegin);
        int endInd = confFile.indexOf(docTypeConstEnd,startInd+docTypeConstBegin.length());
        if (this.docType == null && startInd != -1 && endInd != -1) {
            this.docType = confFile.substring(startInd, endInd+docTypeConstEnd.length());
        } if (startInd != -1 && endInd != -1) {
            confFile = confFile.substring(endInd+docTypeConstEnd.length());
        }
        
        originFileBuffer.append(confFile);
        return result;
    }
    
    @Override
    protected String retrieveNewFileContent (Document document) {
        return retrieveNewFile(super.retrieveNewFileContent(document));
    }
    
    @Override
    protected String retrieveNewFile (String docPath) {
        if (this.docType != null && !"".equals(this.docType)) {
            final String xmlDeclrBegin = "<?xml";
            final String xmlDeclrEnd = "?>";
            int startInd = docPath.indexOf(xmlDeclrBegin);
            int endInd = docPath.indexOf(xmlDeclrEnd,startInd+xmlDeclrBegin.length());
            String xmlDeclaration = "";
            if (startInd != -1 && endInd != -1) {
                xmlDeclaration = docPath.substring(startInd, endInd+xmlDeclrEnd.length()) + "\r\n";
                docPath = docPath.substring(endInd+xmlDeclrEnd.length());
            }
            return xmlDeclaration+this.docType+docPath;
        } else {
            return docPath;
        }
    }
}