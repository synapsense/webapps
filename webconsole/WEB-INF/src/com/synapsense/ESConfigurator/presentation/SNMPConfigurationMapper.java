package com.synapsense.ESConfigurator.presentation;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;

import com.synapsense.ESConfigurator.mappers.DestinationsMapper;
import com.synapsense.ESConfigurator.mappers.DestinationsMapper.ManagerMapperException;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.ComponentsNameConstants;

public class SNMPConfigurationMapper extends ConfigurationMapper {
    
    public SNMPConfigurationMapper() {
        super(ComponentsNameConstants.COMPONENT_SNMP, ActivityLogConstans.MODULE_SNMP);
        // TODO Auto-generated constructor stub
    }

    private static final long serialVersionUID = 5895676074646630675L;
    
    private final String tabID = "ESConfigurator/ID_SNMP_TAB_TITLE";
    private final String snmpBackupFilePrefix = "./";
    private final String snmpPrefix = "snmp/";
    private final String snmpFileName = "snmpconf.xml";
    
    private final String xptraps = "//performer/destinations/trap";
    private final String xpAVDevices = "//performer/destinations/avdevice";
    
    private final String[][] snmpParamMapping = {
            //{"xpGeneralATO","//performer/general/@alert-text-oid"},
            //{"xpGeneralEO","//performer/general/@enterprise-oid"},
            //{"xpGeneralET","//performer/general/@enterprise-trap-id"},
            {"xpGeneralLI","//performer/general/@local-ip"}
    };
    
    @Override
    public void init()
        throws ServletException {
        super.LOG = LogFactory.getLog(SNMPConfigurationMapper.class);        
        super.paramMapping = this.snmpParamMapping;
        super.fileName = this.snmpFileName;
        super.prefix = this.snmpPrefix;
        super.backupFilePrefix = this.snmpBackupFilePrefix;
        super.tabID = this.tabID;
    }
    
    @Override
    protected Document parseDoc (Document document,HttpServletRequest request) 
        throws ParseLoadingDocumentException {
        Document resultDoc = super.parseDoc(document,request);
            
        try {
            DestinationsMapper destMapper = new DestinationsMapper();
            destMapper.getValue(document, resultDoc,xptraps);
            destMapper.getValue(document, resultDoc,xpAVDevices);
            return resultDoc;
        } catch (ManagerMapperException e) {
            throw new ParseLoadingDocumentException ("Error occured while getting value from loading document",e);
        }
    }
    
    @Override
    protected Boolean saveDoc (Document document,Document returnDoc,HttpServletRequest req) 
        throws SaveDocumentException, ServletException {
        boolean docChanged = super.saveDoc(document,returnDoc,req);
        
        try {
            DestinationsMapper destMapper = new DestinationsMapper();
            String traps = req.getParameter("xpTraps");
            boolean result = destMapper.setValue(document, returnDoc, xptraps, traps);
            if (result) {
                docChanged = true;
            }
            String devices = req.getParameter("xpAVDevices");
            result = destMapper.setValue(document, returnDoc, xpAVDevices, devices);
            if (result) {
                docChanged = true;
            }
            return docChanged;
        } catch (ManagerMapperException e) {
            throw new SaveDocumentException("Can't set new value",e);
        }
    }
}