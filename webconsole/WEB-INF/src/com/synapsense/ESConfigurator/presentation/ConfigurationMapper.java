package com.synapsense.ESConfigurator.presentation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;

import javax.management.ObjectName;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;

import com.synapsense.ESConfigurator.esException.ESConfiguratorException;
import com.synapsense.ESConfigurator.mappers.SimpleMapper;
import com.synapsense.ESConfigurator.mappers.SimpleMapper.Result;
import com.synapsense.ESConfigurator.mappers.SimpleMapper.SimpleMapperException;
import com.synapsense.dataAccess.Env;
import com.synapsense.service.FileWriter;
import com.synapsense.service.FileWriter.WriteMode;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.activityLog.ESConfiguratorActivityLog;
import com.synapsense.utilities.lang.LangManager;

public class ConfigurationMapper extends BaseHttpServlet {
    
    protected class ConfigurationFileLoadingException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = -3115131199764979253L;
        public ConfigurationFileLoadingException (String message,Throwable cause) {
            super(message,cause);
        }
        public ConfigurationFileLoadingException (String message) {
            super(message);
        }
    }
    
    protected class RestartServiceException extends ESConfiguratorException {
        private static final long serialVersionUID = 5700332811862765690L;

        public RestartServiceException (String message,Throwable cause) {
            super(message,cause);
        }
        public RestartServiceException (String message) {
            super(message);
        }
    }
    
    protected class ParseLoadingDocumentException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = 8971152051521304863L;
        public ParseLoadingDocumentException (String message,Throwable cause) {
            super(message,cause);
        }
        public ParseLoadingDocumentException (String message) {
            super(message);
        }
    }
    
    protected class SaveDocumentException extends ESConfiguratorException {
        
        /**
         * 
         */
        private static final long serialVersionUID = 2005088132010142253L;
        public SaveDocumentException (String message,Throwable cause) {
            super(message,cause);
        }
        public SaveDocumentException (String message) {
            super(message);
        }
    }
    
    protected class WriteFileException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = 4963111020014949927L;
        public WriteFileException (String message,Throwable cause) {
            super(message,cause);
        }
        public WriteFileException (String message) {
            super(message);
        }
    }
    
    protected class SwapBackupFilesException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = 5891850932447223413L;
        public SwapBackupFilesException (String message,Throwable cause) {
            super(message,cause);
        }
        public SwapBackupFilesException (String message) {
            super(message);
        }
    }
    
    protected class SerializeDocumentException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = -2191531721782787426L;
        public SerializeDocumentException (String message,Throwable cause) {
            super(message,cause);
        }
        public SerializeDocumentException (String message) {
            super(message);
        }
    }
    
    private static final long serialVersionUID = -3516860642763829070L;
    private final short backupCount = 3;
    private final String backupFileExtension = "backupfile";
    private String componentName = null;
    private String moduleName = null;
    
    protected Log LOG = null;
    
    protected String backupFilePrefix = null;
    protected String prefix = null;
    protected String fileName = "qwe.qwe";
    protected String[][] paramMapping = null;
    protected String tabID = "ESConfigurator/ID_MAINWND_TITLE";
    
    protected ConfigurationMapper(String cn, String mn) {
        // TODO Auto-generated constructor stub
        this.componentName = cn;
        this.moduleName = mn;
    }
    
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) 
        throws IOException, ServletException {
        
        ArrayList<String> backupFilesArray = new ArrayList<String>(backupCount);
        short backupPoint = loadBackupFiles(request, backupFilesArray);
        
        boolean originFileIsAbsent = false;
        StringBuilder buffer = new StringBuilder();
        String fileContent = null;
        Document loadedDoc = null;
        
        String requestType = request.getParameter("TYPE").toLowerCase();
        if (requestType.equals("restore_config"))
        {
            String restore_type = request.getParameter("restore_type").toLowerCase();
            if(restore_type.equals("template"))
            {
                restoreTemplate(request, response);
            }
            else if(restore_type.equals("backup"))
            {
                restoreBackup(request, response);
            }
            else
            {
                writeRestoreConfigResponse(request, response, "failed");
            }
            return;
        }

        
        try {
            originFileIsAbsent = loadConfigurationFile(request, backupPoint == -1?
                                        null:backupFilesArray.get(backupPoint), buffer);
                        
            fileContent = buffer.toString();
            loadedDoc = getSettingsFileAsDocument(fileContent);
             
            if(loadedDoc == null) {
                throw new ConfigurationFileLoadingException("Error while parsing file " + fileName);
            }
            
        } catch (ConfigurationFileLoadingException e) {
            LOG.error(e.getMessage(), e);
            ArrayList<String> files = getExistingBackupIndices(request);
            String errorDescription = getErrorDescription(request, e.getMessage(), "ID_ERROR_LOAD_FILE", files);
            sendResponse(response,errorDescription);
            return;
        }

        if (requestType.equals("load"))
        {
            load(request, response, loadedDoc);
        }
        else if (requestType.equals("save"))
        {
            save(request, response, loadedDoc, originFileIsAbsent, backupFilesArray, fileContent);
        }
    }
    
    protected Document getSettingsFileAsDocument(String fileContent) {
        return XMLHelper.getDocument(fileContent);
    }
    
    private void restoreBackup(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException
    {
        String result = "success";
        try {
            int lastDot = fileName.lastIndexOf(".");
            String backupName = fileName.substring(0,lastDot) + request.getParameter("backup_index") + "~." + backupFileExtension;
            String backupFileContents = read(request, backupFilePrefix + backupName);
            
            write(request, prefix + fileName, backupFileContents, WriteMode.REWRITE);
            restartService(request);
            
        } catch (Exception e) {
            result = "failed";
            LOG.error("Failed to restore backup" + e.getMessage(), e);
        }
        writeRestoreConfigResponse(request, response, result);
    }
    
    private void restoreTemplate(HttpServletRequest request,HttpServletResponse response) throws IOException,ServletException
    {
        String result = "success";
        try {
            
            String fileSeparator = System.getProperty("file.separator");
            String templFileName = request.getSession().getServletContext().getRealPath(fileSeparator) +
                               "conf" + fileSeparator + "ESConfigurator" + 
                               fileSeparator + "templ" + fileSeparator + fileName;

            String templFileContents = readFileAsString(templFileName);
            write(request, prefix + fileName, templFileContents, WriteMode.REWRITE);
            restartService(request);
            
        } catch (Exception e) {
            result = "failed";
            LOG.error("Failed to restore template" + e.getMessage(), e);
        }
        writeRestoreConfigResponse(request, response, result);
    }
            
    private void writeRestoreConfigResponse(HttpServletRequest request, HttpServletResponse response, String result) 
        throws IOException,ServletException
    {
        String responseStr = "<config><restore_config><result>"+result+"</result></restore_config></config>";
        sendResponse(response, responseStr);
    }
    
    protected void load (HttpServletRequest request,HttpServletResponse response,Document loadedDoc) 
        throws IOException,ServletException {
        Document document = null;
        try {
            document = parseDoc(loadedDoc,request);
        } catch (ParseLoadingDocumentException e) {
            LOG.error(e.getMessage(), e);
            String errorDescription = getErrorDescription(request, e.getMessage(), "ID_ERROR_PARSE_FILE", getExistingBackupIndices(request));
            sendResponse(response,errorDescription);
            return;
        }
        sendResponse(response,document);
    }
    
    protected void save (HttpServletRequest request,HttpServletResponse response,Document loadedDoc,
                        boolean originFileIsAbsent,ArrayList<String> backupFilesArray,String fileContent)
        throws IOException,ServletException {
        
        Document document = null;
        try {
            document = XMLHelper.createNewXmlDocument("config");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getMessage(),e);
            ArrayList<String> files = getExistingBackupIndices(request);
            String errorDescription = getErrorDescription(request, e.getMessage(), "ID_ERROR_CREATE_RESULT", files);
            sendResponse(response,errorDescription);
            return;      
        }
        
        try {
            boolean docChanged = saveDoc(loadedDoc,document,request);
            if (docChanged) {
                String newFileVersion = retrieveNewFileContent(loadedDoc);
                if (newFileVersion == null || newFileVersion.equals("")) {
                    throw new SaveDocumentException("New file version is empty");
                }
                try {
                    write(request,prefix + fileName, 
                            newFileVersion,WriteMode.REWRITE);
                    restartService(request);
                    swapBackupFiles(request,newFileVersion,backupFilesArray);
                } catch (WriteFileException e) {
                    originFileIsAbsent = true;
                    throw new SaveDocumentException(e.getMessage(), e);
                } catch (SwapBackupFilesException e) {
                    throw new SaveDocumentException(e.getMessage(), e);
                } catch (RestartServiceException e) {
                    originFileIsAbsent = true;
                    throw new SaveDocumentException(e.getMessage(), e);
                }
                ESConfiguratorActivityLog.logRecord(request, this.componentName, this.moduleName, "activity_log_control_" + this.moduleName.toLowerCase() + "_configuration_was_changed");
            } else {
                try {
                    updateBackupFiles(request, retrieveNewFile(fileContent), backupFilesArray);
                    if (originFileIsAbsent) {
                        write(request,prefix + fileName, 
                                retrieveNewFile(fileContent),WriteMode.REWRITE);
                        restartService(request);
                    }
                } catch (RestartServiceException e) {
                    throw new SaveDocumentException (e.getMessage(), e);
                } catch (WriteFileException e) {
                    throw new SaveDocumentException (e.getMessage(), e);
                }
            }
        } catch (SaveDocumentException e) {
            String error = e.getMessage();
            
            try {
                updateBackupFiles(request, retrieveNewFile(fileContent), backupFilesArray);
                if (originFileIsAbsent) {
                    write(request,prefix + fileName, 
                            retrieveNewFile(fileContent),WriteMode.REWRITE);
                    try {
                        restartService(request);
                    } catch (Exception e1) {
                        LOG.error(e1.getLocalizedMessage());
                    }
                }
            } catch (SaveDocumentException e1) {/*We can nothing*/
                LOG.error(e1.getLocalizedMessage());
            } catch (WriteFileException e1) {/*We can nothing*/
                String errorDescription = getErrorDescription(request, error, "ID_ERROR_WRITE_FILE", null);
                sendResponse(response,errorDescription);
                return;
            }
            try {
                document = parseDoc(getSettingsFileAsDocument(fileContent),request);
            } catch (ParseLoadingDocumentException e1) {
                LOG.error(e1.getMessage(),e1);
                error += "\n" + prefix + fileName + getLocale("ESConfigurator/ID_ERROR_REVERTED_RESULT", request) + prefix + fileName + "\r\n";
                String errorDescription = getErrorDescription(request, error, "ID_ERROR_WRITE_FILE", null);
                sendResponse(response,errorDescription);
                return;
            }
            document = getErrorDocument(error, document);
        }
        sendResponse(response,document);
    }
    
    private Document getErrorDocument (String errorMessage,Document document) {
        Node root = document.getFirstChild();
        root.appendChild(XMLHelper.createXmlTextNode(document, "errorDescription", errorMessage));
        return document;
    }
    
    private short loadBackupFiles (HttpServletRequest request,ArrayList<String> buffer) {
        
        int lastDot = fileName.lastIndexOf(".");
        
        buffer.clear();
        //First element of the array is newest version of file.
        //It is equivalent of used file. But, other elements of the array can follow in arbitrary sequence.
        //This array must consist of nulls or correct files.
        short backupPoint = -1;
        for (short i=0;i<backupCount;i++) {
            String backupName = fileName.substring(0,lastDot) + i + "~." + backupFileExtension;
            String backupFile = read(request, backupFilePrefix + backupName);
            if (backupFile == null || backupFile.equals("")) {
                buffer.add(i, null);
            } else {
                buffer.add(i, backupFile);
                if (backupPoint == -1) {
                    backupPoint = i;
                }
            }
        }
        return backupPoint;
    }
    
    protected boolean loadConfigurationFile (HttpServletRequest request,String backupFile,StringBuilder originFileBuffer) 
        throws ConfigurationFileLoadingException {
        boolean originFileIsAbsent = false;
        String fileSeparator = System.getProperty("file.separator");
        
        String file = read(request,prefix + fileName);
        
        if (file == null || file.equals("")) {
            originFileIsAbsent = true;
            if (backupFile == null || backupFile.equals("")) {
                
                String templFileName = request.getSession().getServletContext().getRealPath(fileSeparator) + "conf" + fileSeparator + "ESConfigurator"
                                    + fileSeparator + "templ" + fileSeparator + fileName;
                
                String templFile = "";
                try {
                    FileInputStream is = new FileInputStream(templFileName);
                    int b;
                    while ((b = is.read()) != -1) {templFile += (char)b;}
                    is.close();
                } catch (IOException e) {
                    throw new ConfigurationFileLoadingException("There is no any one configuration file",e);
                }
                if (templFile.equals("")) {
                    throw new ConfigurationFileLoadingException("There is no any one configuration file");
                } else {
                    file = new String(templFile);
                }
            } else {
                file = new String(backupFile);
            }
        }
        originFileBuffer.append(file);
        return originFileIsAbsent;
    }
    
    protected String retrieveNewFileContent (Document document) {
        
        DOMImplementation domImplementation = document.getImplementation();    	
        if (domImplementation.hasFeature("LS", "3.0") && domImplementation.hasFeature("Core", "2.0")) {
            DOMImplementationLS domImplementationLS = (DOMImplementationLS) domImplementation.getFeature("LS", "3.0");
            LSSerializer lsSerializer = domImplementationLS.createLSSerializer();
            DOMConfiguration domConfiguration = lsSerializer.getDomConfig();
            
            if (domConfiguration.canSetParameter("format-pretty-print", Boolean.TRUE))
                lsSerializer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
            else    			
                LOG.warn("DOMConfiguration 'format-pretty-print' parameter isn't settable.");
                
            LSOutput lsOutput = domImplementationLS.createLSOutput();
            lsOutput.setEncoding("UTF-8");
            Writer stringWriter = new StringWriter();
            lsOutput.setCharacterStream(stringWriter);
            lsSerializer.write(document, lsOutput);
            return stringWriter.toString();
            
        } else {
            throw new RuntimeException("DOM 3.0 LS and/or DOM 2.0 Core not supported.");
        }
    }
    
    protected String retrieveNewFile (String docPath) {
        return docPath;
    }
    
    private void swapBackupFiles (HttpServletRequest request,String newFileVersion,ArrayList<String> oldBackupFiles) 
        throws SwapBackupFilesException {
        
        int lastDot = fileName.lastIndexOf(".");
        
        if (newFileVersion == null || newFileVersion.equals("")) {
            throw new SwapBackupFilesException("Can't swap backup version of " + prefix + fileName);
        }
        
        try {
            String backupName = fileName.substring(0,lastDot) + 0 + "~." + backupFileExtension;
            write(request, backupFilePrefix + backupName,newFileVersion,WriteMode.REWRITE);
            for (short i=0;i<backupCount-1;i++) {
                backupName = fileName.substring(0,lastDot) + (i+1) + "~." + backupFileExtension;
                String buffer = oldBackupFiles.get(i);
                if (buffer == null || buffer.equals("")) {
                    buffer = newFileVersion;
                }
                write(request, backupFilePrefix + backupName,buffer,WriteMode.REWRITE);
            }
        } catch (WriteFileException e) {
            throw new SwapBackupFilesException ("Can't swap backup versions of " + prefix + fileName,e);
        }
    }
    
    private void updateBackupFiles (HttpServletRequest request,String updatedFileVersion,ArrayList<String> oldBackupFiles) 
        throws SaveDocumentException,WriteFileException {
        
        int lastDot = fileName.lastIndexOf(".");
        
        if (updatedFileVersion == null || updatedFileVersion.equals("")) {
            throw new SaveDocumentException("Can't update backup version of " + fileName);
        }
        
        for (short i=0;i<backupCount;i++) {
            String backupName = fileName.substring(0,lastDot) + i + "~." + backupFileExtension;
            String buffer = oldBackupFiles.get(i);
            if (buffer == null || buffer.equals("")) {
                buffer = updatedFileVersion;
                write(request, backupFilePrefix + backupName,buffer,WriteMode.REWRITE);
            }
        }
        
    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        doGet(req, resp);
    }
    
    private String read (HttpServletRequest req,String fileName) {
        FileWriter fileWritter = Env.getFileWritter();
        String file = null;
        try {
            file = fileWritter.readFile(fileName);
        } catch (IOException e) {
            LOG.info(e.getMessage());
        }
        return file;
    }
    
    private void write (HttpServletRequest req,String fileName,String file,WriteMode WM) 
        throws WriteFileException {
        try {
            FileWriter fileWritter = Env.getFileWritter();
            fileWritter.writeFileBytes(fileName, file.getBytes("utf-8"), WM);
        } catch (IOException e) {
            throw new WriteFileException ("Can't write file: " + fileName + file);
        }
    }
    
    protected Document parseDoc (Document document,HttpServletRequest request) 
        throws ParseLoadingDocumentException {
        try {
            Document resultDoc = XMLHelper.createNewXmlDocument("config");
            Node rootNode = resultDoc.getFirstChild();
            
            SimpleMapper simpleMapper = new SimpleMapper();
            for (int i=0;i<paramMapping.length;i++) {
                String result = simpleMapper.getValue(document, paramMapping[i][1]);
                Node paramNode = XMLHelper.createXmlTextNode(resultDoc, paramMapping[i][0], result);
                rootNode.appendChild(paramNode);
            }
            return resultDoc;
        } catch (ParserConfigurationException e) {
            throw new ParseLoadingDocumentException("Error occured while parsing document",e);
        } catch (SimpleMapperException e) {
            throw new ParseLoadingDocumentException("Error occured while getting value from loading document",e);
        }
    }
    
    protected Boolean saveDoc (Document document,Document returnDoc,HttpServletRequest req) 
        throws SaveDocumentException, ServletException {
        Node rootNode = returnDoc.getFirstChild();
        
        try {
            boolean docChanged = false;
            SimpleMapper simpleMapper = new SimpleMapper();
            for (int i=0;i<paramMapping.length;i++) {
                String value = req.getParameter(paramMapping[i][0]);
                Result result = simpleMapper.setValue(document, paramMapping[i][1],value);
                if (result.isChanged()) {
                    docChanged = true;
                }
                Node paramNode = XMLHelper.createXmlTextNode(returnDoc, paramMapping[i][0], result.getData());
                rootNode.appendChild(paramNode);
            }
            return docChanged;
        } catch (SimpleMapperException e) {
            throw new SaveDocumentException("Can't set new value",e);
        }
    }
    
    private void restartService(HttpServletRequest req) throws RestartServiceException {
        String objectName = getServiceName(fileName);
        if (objectName == null) {
            return;
        }
        
        try {
            String[] methodSignature = {};
            final ObjectName name = new ObjectName (objectName);

            ManagementFactory.getPlatformMBeanServer().invoke(name, "stop", null, methodSignature);
            ManagementFactory.getPlatformMBeanServer().invoke(name, "destroy", null, methodSignature);
            ManagementFactory.getPlatformMBeanServer().invoke(name, "create", null, methodSignature);
            // "start" method is not called, since both "AuditorService" and "SNMPTrapPerformer" services call "start" in the "create" method
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            throw new RestartServiceException("Failed restart service " + objectName, e);
        }
    }

    private String getServiceName (String fileName) {
        if (fileName.contains("audit")) {
            return "com.synapsense:type=AuditorService";
        } else if (fileName.contains("snmpconf")) {
            return "com.synapsense:type=SNMPTrapPerformer";
        }
        return null;
    }
        
    private String getLocale(String messageID, HttpServletRequest request)
    {
        String langID = LangManager.getCurrentLocaleId(request);
        return LangManager.getInstance().getLocaleString(langID, messageID);    	
    }
    
    private String getErrorDescription(HttpServletRequest request, String error, String errorType, ArrayList<String> backupIndices)
    {
        StringBuilder errorDescription = new StringBuilder();
        
        errorDescription.append("<config>");
        errorDescription.append("<config_error>");
        
        errorDescription.append("<errorDescription>");
        errorDescription.append(error);    	
        errorDescription.append("</errorDescription>");
            
        if(errorType == null || errorType == "")
        {
            errorType = "ID_ERROR_UNKNOWN";
        }
        errorDescription.append("<errorType>");
        errorDescription.append(errorType);    	
        errorDescription.append("</errorType>");
        
        if(backupIndices != null && backupIndices.size() != 0)
        {        	
            errorDescription.append("<backupIndices>");
            for(String index : backupIndices)
            {
                errorDescription.append("<index>");    	
                errorDescription.append(index);    	
                errorDescription.append("</index>");    	
            }
            errorDescription.append("</backupIndices>");            	
        }
        
        errorDescription.append("<tabName>");
        errorDescription.append(getLocale(tabID, request));        
        errorDescription.append("</tabName>");
      
        errorDescription.append("<fileName>");
        errorDescription.append(fileName);        
        errorDescription.append("</fileName>");
      
        errorDescription.append("</config_error>");
        errorDescription.append("</config>");
        
        return errorDescription.toString();
    }
    
    private ArrayList<String> getExistingBackupIndices(HttpServletRequest request)
    {
        ArrayList<String> indices = new ArrayList<String>();
        int lastDot = fileName.lastIndexOf(".");
        
        FileWriter fileWritter = Env.getFileWritter();
        String file = null;

        for (int i = 0; i < backupCount; i++)
        {
            String backupFileName = backupFilePrefix + fileName.substring(0, lastDot) + i + "~." + backupFileExtension;
            try
            {
                file = fileWritter.readFile(backupFileName);
                if(file != null)
                {
                    indices.add(new Integer(i).toString());
                }
            } catch (IOException e) {}
        }
        return indices;
    }
    
    private String readFileAsString(String filePath) throws java.io.IOException
    {
        byte[] buffer = new byte[(int) new File(filePath).length()];
        try (FileInputStream f = new FileInputStream(filePath)) {
            f.read(buffer);
        } catch (IOException e) {
        }
        return new String(buffer);
    }
}