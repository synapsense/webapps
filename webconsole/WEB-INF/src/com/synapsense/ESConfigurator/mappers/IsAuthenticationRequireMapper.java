package com.synapsense.ESConfigurator.mappers;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.ESConfigurator.esException.ESConfiguratorException;
import com.synapsense.utilities.DocumentHelper;

public class IsAuthenticationRequireMapper {
    
    public class IsAuthenticationRequireMapperException extends ESConfiguratorException {
        
        /**
         * 
         */
        private static final long serialVersionUID = -8861741410410852990L;

        public IsAuthenticationRequireMapperException(String message, Throwable cause) {
            super(message, cause);
        }

        public IsAuthenticationRequireMapperException(String message) {
            super(message);
        }
    }
    
    public class Result {
        private String[] data = null;
        private boolean isChanged = false;

        public String[] getData() {
            return data;
        }

        public boolean isChanged() {
            return isChanged;
        }
    }

    /**
     * 
     * @param document
     * @param xpUser
     * @param userValue
     * @param xpPassword
     * @param passwordValue
     * @return result data: String[0] - check box value String[1] - user field
     *         value String[2] - password field value
     * @throws XPathExpressionException
     */
    public Result setValue(Document document, final String xpAuth, final String xpUser, String userValue,
            final String xpPassword, String passwordValue)
            throws IsAuthenticationRequireMapperException {
        Result result = new Result();
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            
            XPathExpression expr = xpath.compile(xpUser);
            Node nodeUser = (Node) expr.evaluate(document, XPathConstants.NODE);
            expr = xpath.compile(xpPassword);
            Node nodePassword = (Node) expr.evaluate(document, XPathConstants.NODE);
            
            //Create node for authentication parameter 
            expr = xpath.compile(xpAuth);
            Node nodeAuth = (Node)expr.evaluate(document, XPathConstants.NODE);
            if (nodeAuth == null) {
                DocumentHelper.createNodeByXPath(document, xpAuth);
                nodeAuth = DocumentHelper.getNode(document, xpAuth);
            }
            
            result.data = new String[3];
            if (userValue == null || userValue.equals("") || passwordValue == null
                    || passwordValue.equals("")) {
                if (!Boolean.FALSE.toString().equalsIgnoreCase(nodeAuth.getNodeValue())) {
                    nodeAuth.setNodeValue(Boolean.FALSE.toString());
                    result.isChanged = true;
                }
                
                if (nodeUser != null) {
                    String xpParentXPath = xpUser.substring(0, xpUser
                            .lastIndexOf("/"));
                    Node parent = DocumentHelper.getNode(document, xpParentXPath);
                    parent.getParentNode().removeChild(parent);
                    result.isChanged = true;
                }
                if (nodePassword != null) {
                    String xpParentXPath = xpPassword.substring(0, xpPassword
                            .lastIndexOf("/"));
                    Node parent = DocumentHelper.getNode(document, xpParentXPath);
                    parent.getParentNode().removeChild(parent);
                    result.isChanged = true;
                }
                
                result.data[0] = "";
                result.data[1] = "";
                result.data[2] = "";
                return result;
            } else {
                result.data[0] = "1";
                if (nodeUser == null) {
                    DocumentHelper.createNodeByXPath(document, xpUser);
                    nodeUser = DocumentHelper.getNode(document, xpUser);
                }
                if (!userValue.equals(nodeUser.getNodeValue())) {
                    nodeUser.setNodeValue(userValue);
                    result.isChanged = true;
                }
                result.data[1] = userValue;
                if (nodePassword == null) {
                    DocumentHelper.createNodeByXPath(document, xpPassword);
                    nodePassword = DocumentHelper.getNode(document, xpPassword);
                }
                if (!passwordValue.equals(nodePassword.getNodeValue())) {
                    nodePassword.setNodeValue(passwordValue);
                    result.isChanged = true;
                }
                result.data[2] = passwordValue;
                
                if (!Boolean.TRUE.toString().equalsIgnoreCase(nodeAuth.getNodeValue())) {
                    nodeAuth.setNodeValue(Boolean.TRUE.toString());
                    result.isChanged = true;
                }
                return result;
            }
        } catch (XPathExpressionException e) {
            throw new IsAuthenticationRequireMapperException(
                    "Failed set value-" + userValue + " " + passwordValue + " by given xPath-" 
                    + xpUser + " " + xpPassword,e);
        }
    }

    public String[] getValue(Document document, final String xpUser, final String xpPassword)
            throws IsAuthenticationRequireMapperException {
        String[] result = null;
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile(xpUser);
            Node nodeUser = (Node) expr.evaluate(document, XPathConstants.NODE);
            expr = xpath.compile(xpPassword);
            Node nodePassword = (Node) expr.evaluate(document, XPathConstants.NODE);
            result = new String[3];
            if (nodeUser != null && nodePassword != null) {
                result[0] = "1";
                result[1] = nodeUser.getNodeValue();
                result[2] = nodePassword.getNodeValue();
            } else {
                if (nodeUser != null) {
                    String xpParentXPath = xpUser.substring(0, xpUser
                            .lastIndexOf("/"));
                    Node parent = DocumentHelper.getNode(document, xpParentXPath);
                    parent.getParentNode().removeChild(parent);
                }
                if (nodePassword != null) {
                    String xpParentXPath = xpPassword.substring(0, xpPassword
                            .lastIndexOf("/"));
                    Node parent = DocumentHelper.getNode(document, xpParentXPath);
                    parent.getParentNode().removeChild(parent);
                }
                result[0] = "";
                result[1] = "";
                result[2] = "";
            }
        } catch (XPathExpressionException e) {
            throw new IsAuthenticationRequireMapperException("Failed set user and password value");
        }
        return result;
    }
}