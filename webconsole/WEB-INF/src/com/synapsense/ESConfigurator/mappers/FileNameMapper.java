package com.synapsense.ESConfigurator.mappers;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.utilities.JbossManagementAPI;

public class FileNameMapper {
    
    /* TODO - Needs to uncommented when below is uncommented */
    /*
    private static final Log LOG = LogFactory.getLog(FileNameMapper.class);
    
    private static final String securePortKey = "com.synapsense.securePort";
    private static final String filePropertyName = "keystoreFile";
    private static final String passwordPropertyName = "keystorePass";
    */
    public static String[] getValue(final HttpServletRequest request) {
        String[] result = {"",""};
        // TODO Configuration is different under JBoss 7. The code below
        // is to be reworked to reflect this
        /*
        try {
            
            final String objectName = "jboss.web:serviceName=jboss.web,type=Service";
            MBeanServerConnection server = Env.getMBeanServerConnection();
            ObjectName name = new ObjectName(objectName);
            String[] signature = {};
            Connector[] list = (Connector[])server.invoke(name, "findConnectors", new Object[]{}, signature);
            
            String sPort = System.getProperty(securePortKey);
            if (sPort != null && sPort != "") {
                int port = Integer.parseInt(sPort);
                for (Connector connector : list) {
                    if (connector.getPort() == port) {
                        String keystoreFile = (String) connector
                                .getProperty(filePropertyName);
                        String keystorePass = (String) connector
                                .getProperty(passwordPropertyName);
                        result[1] = keystorePass;
                        File file = new File(keystoreFile);
                        int leftEng = keystoreFile.lastIndexOf("\\");
                        int rightEng = keystoreFile.lastIndexOf("/");
                        int delimiter = leftEng > rightEng ? leftEng : rightEng;
                        if (file.exists()) {
                            result[0] = keystoreFile.substring(delimiter + 1,
                                    keystoreFile.length());
                        }
                        break;
                    }
                }
            }
        } catch (NullPointerException e) {
            LOG.error(e.getMessage(),e);
        } catch (SecurityException e) {
            LOG.error(e.getMessage(),e);
        } catch (MalformedObjectNameException e) {
            LOG.error(e.getMessage(),e);
        } catch (IOException e) {
            LOG.error(e.getMessage(),e);
        } catch (InstanceNotFoundException e) {
            LOG.error(e.getMessage(),e);
        } catch (MBeanException e) {
            LOG.error(e.getMessage(),e);
        } catch (ReflectionException e) {
            LOG.error(e.getMessage(),e);
        } catch (NumberFormatException e) {
            LOG.error(e.getMessage(),e);
        }
        */
        
        try {
            JbossManagementAPI util = JbossManagementAPI.createFromRequest(request);
            //TODO:
            result[0] = "server.keystore";
            result[1] = "pass";
            util.close();
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return result;
        //throw new UnsupportedOperationException("needs to be reimplemented");
    }

}