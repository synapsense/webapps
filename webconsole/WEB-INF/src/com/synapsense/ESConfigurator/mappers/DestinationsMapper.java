package com.synapsense.ESConfigurator.mappers;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.ESConfigurator.esException.ESConfiguratorException;
import com.synapsense.utilities.DocumentHelper;
import com.synapsense.utilities.XMLHelper;

public class DestinationsMapper {

    public class ManagerMapperException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = -3898941576250561782L;

        public ManagerMapperException(String message, Throwable cause) {
            super(message, cause);
        }

        public ManagerMapperException(String message) {
            super(message);
        }
    }

    public boolean setValue(Document document, Document returnDoc, final String xPath, String managers)
            throws ManagerMapperException {
        final String snmp_ns = "snmp";
        
        boolean isChanged = true;
        try {
            
            int index = xPath.lastIndexOf("/");
            String itemName = xPath.substring(index+1);
            String localRootName = xPath.substring(0, index).
                                        substring(xPath.substring(0, index).lastIndexOf("/")+1);
            
            Node root = returnDoc.getFirstChild();
            Node rootNode = XMLHelper.createXmlNode(returnDoc, localRootName, new HashMap<String, String>());
            root.appendChild(rootNode);
            
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile(xPath);
            NodeList nodeList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);
            
            for (int i = 0; i < nodeList.getLength(); i++)
                nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
            
            Document managersDoc = XMLHelper.loadXmlFromByteArray(managers.getBytes());
            NodeList managerList = managersDoc.getElementsByTagName(itemName);
            
            String xpParentXPath = xPath.substring(0, xPath.lastIndexOf("/"));
            for (int i = 0; i < managerList.getLength(); i++) {
                Node managerNode = managerList.item(i);

                DocumentHelper.createNodeByXPath(document, xpParentXPath
                        + "/" + snmp_ns + ":" + itemName);
                expr = xpath.compile(xpParentXPath + "/" + itemName + "[" + (i + 1)
                        + "]");
                
                Node itemNode = XMLHelper.createXmlNode(returnDoc, itemName, new HashMap<String, String>());
                rootNode.appendChild(itemNode);
                
                Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
                NodeList childNodes = managerNode.getChildNodes();
                for (int j = 0; j < childNodes.getLength(); j++) {
                    Node childNode = childNodes.item(j);
                    String key = childNode.getNodeName();
                    String value = childNode.getTextContent();
                    
                    itemNode.appendChild(XMLHelper.createXmlTextNode(returnDoc, key, value));
                    
                    node.getAttributes().setNamedItem(
                            document.createAttribute(key));
                    node.getAttributes().getNamedItem(key).setNodeValue(
                            value);
                }
            }
        } catch (XPathExpressionException e) {
            throw new ManagerMapperException("Failed set value-" + managers + " by xPath-" + xPath, e);
        } catch (IOException e) {
            throw new ManagerMapperException("Can't read xml", e);
        }
        return isChanged;
    }

    public void getValue(Document document, Document returnDoc, final String xPath)
            throws ManagerMapperException {
        try {
            int index = xPath.lastIndexOf("/");
            String itemName = xPath.substring(index+1);
            String localRootName = xPath.substring(0, index).
                                        substring(xPath.substring(0, index).lastIndexOf("/")+1);
            
            Node root = returnDoc.getFirstChild();
            Node rootNode = XMLHelper.createXmlNode(returnDoc, localRootName, new HashMap<String, String>());
            root.appendChild(rootNode);
            
            //Get all manager nodes
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile(xPath);
            NodeList nodeList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);
            
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node itemNode = XMLHelper.createXmlNode(returnDoc, itemName, new HashMap<String, String>());
                rootNode.appendChild(itemNode);
                
                NamedNodeMap map = nodeList.item(i).getAttributes();
                String managerStr = "";
                for (int j = 0; j < map.getLength(); j++) {
                    String nodeName = map.item(j).getNodeName();
                    String nodeValue = map.item(j).getNodeValue();
                    managerStr += nodeName + "=" + nodeValue + " ";
                    itemNode.appendChild(XMLHelper.createXmlTextNode(returnDoc, nodeName, nodeValue));
                }
                managerStr = managerStr.substring(0, managerStr.length() - 1);
            }
        } catch (XPathExpressionException e) {
            throw new ManagerMapperException("Failed get value by xPath-" + xPath,e);
        }
    }
}