package com.synapsense.ESConfigurator.mappers;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.ESConfigurator.esException.ESConfiguratorException;
import com.synapsense.utilities.DocumentHelper;

public class EncryptMapper {
    
    public class EncryptedMapperException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = 3181569206823545507L;
        
        public EncryptedMapperException (String message,Throwable cause) {
            super(message,cause);
        }
        public EncryptedMapperException (String message) {
            super(message);
        }
    }
    
    public class Result {
        private String data = null;
        private boolean isChanged = false;

        public String getData() {
            return data;
        }

        public boolean isChanged() {
            return isChanged;
        }
    }

    public Result setValue(Document document, final String[][] depends, String value)
            throws EncryptedMapperException {
        Result result = new Result();
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = null;
            ArrayList<Node> nodeList = new ArrayList<Node>();
            for (String[] str : depends) {
                expr = xpath.compile(str[1]);
                Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
                nodeList.add(node);
            }
    
            if (value == null || value.equals("")) {
                for (int i = 0; i < nodeList.size(); i++) {
                    if (nodeList.get(i) != null) {
                        String xpParentXPath = depends[i][1].substring(0,
                                depends[i][1].lastIndexOf("/"));
                        Node parent = DocumentHelper.getNode(document,
                                xpParentXPath);
                        parent.getParentNode().removeChild(parent);
                        result.isChanged = true;
                    }
                }
                result.data = "";
                return result;
            } else {
                result.data = "1";
                for (int i = 0; i < nodeList.size(); i++) {
                    Node node = nodeList.get(i);
                    if (node == null) {
                        DocumentHelper.createNodeByXPath(document, depends[i][1]);
                        node = DocumentHelper.getNode(document, depends[i][1]);
                    }
                    if (!depends[i][0].equals(node.getNodeValue())) {
                        node.setNodeValue(depends[i][0]);
                        result.isChanged = true;
                    }
                }
                return result;
            }
        } catch (XPathExpressionException e) {
            throw new EncryptedMapperException("Failed set configuration for encrypted connection",e);
        }
    }

    public String getValue(Document document, final String[][] depends)
            throws EncryptedMapperException {
        String result = null;
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = null;
    
            ArrayList<Node> nodeList = new ArrayList<Node>();
            for (String[] str : depends) {
                expr = xpath.compile(str[1]);
                Node node = (Node) expr.evaluate(document, XPathConstants.NODE);
                nodeList.add(node);
            }
            boolean exist = true;
            for (int i = 0; i < nodeList.size(); i++) {
                if (nodeList.get(i) != null) {
                    if (!nodeList.get(i).getNodeValue().equalsIgnoreCase(
                            depends[i][0])) {
                        exist = false;
                        break;
                    }
                } else {
                    exist = false;
                    break;
                }
            }
            if (exist) {
                result = "1";
            } else {
                for (int i = 0; i < nodeList.size(); i++) {
                    if (nodeList.get(i) != null) {
                        String xpParentXPath = depends[i][1].substring(0,
                                depends[i][1].lastIndexOf("/"));
                        Node parent = DocumentHelper.getNode(document,
                                xpParentXPath);
                        parent.getParentNode().removeChild(parent);
                    }
                }
                result = "";
            }
        } catch (XPathExpressionException e) {
            throw new EncryptedMapperException("Failed get connection type",e);
        }
        return result;
    }
}