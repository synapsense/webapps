package com.synapsense.ESConfigurator.mappers;

import javax.servlet.http.HttpServletRequest;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.ESConfigurator.mappers.SimpleMapper.SimpleMapperException;

public class AuditMapper {
    
    private final String auditClassPrefix = "com.synapsense.impl.auditsvc.auditor.";
    private final String xpAuditClass = "//configuration/auditor/@class";
    
    public class AuditMapperException extends Exception {
        
        private static final long serialVersionUID = -1876475009903946923L;
        
        public AuditMapperException (String message,Throwable cause) {
            super(message,cause);
        }
        public AuditMapperException (String message) {
            super(message);
        } 
    }
    
    public class Result {
        private String[] data = null;
        private boolean isChanged = false;

        public String[] getData() {
            return data;
        }

        public boolean isChanged() {
            return isChanged;
        }
    }
    
    public String[] getValue (Document document, String[][] auditMapping, String xpConfigurationComments)
        throws AuditMapperException {
        String[] result = new String[auditMapping.length+1];
        try {
            String xpAudit = auditMapping[0][1].substring(0, auditMapping[0][1].lastIndexOf("/"));
            String auditName = xpAudit.substring(xpAudit.lastIndexOf("/")+1, xpAudit.length());
            
            XPath xpath = XPathFactory.newInstance().newXPath();
            
            //Search current service in comments
            XPathExpression expr = xpath.compile(xpConfigurationComments);
            NodeList commentList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);
            
            int auditIsCommented = -1;
            for (int i=0;i<commentList.getLength();i++) {
                Node commentNode = commentList.item(i);
                String tagName = commentNode.getNodeValue().trim().substring(0, auditName.length());
                if (tagName.equals(auditName)) {
                    auditIsCommented = i;
                    break;
                }
            }
            
            expr = xpath.compile(xpAudit);
            NodeList auditNode = (NodeList)expr.evaluate(document, XPathConstants.NODESET);
            if (auditNode.getLength() > 1) {
                throw new AuditMapperException("There is more than one node by " + xpAudit);
            } else if (auditNode.getLength() != 0 && auditIsCommented != -1) {
                throw new AuditMapperException("There is both commeted and real node by " + xpAudit);
            } else if (auditIsCommented != -1) {
                result[0] = "";
                String data = commentList.item(auditIsCommented).getNodeValue();
                for (int i=0;i<auditMapping.length;i++) {
                    String attrName = auditMapping[i][1].substring(auditMapping[i][1].lastIndexOf("@")+1,auditMapping[i][1].length());
                    int startInd = data.indexOf("\"", data.indexOf(attrName)+attrName.length());
                    int endInd = data.indexOf("\"",startInd+1);
                    String value = data.substring(startInd+1, endInd);
                    result[i+1] = value;
                }
            } else {
                result[0] = "1";
                SimpleMapper simpleMapper = new SimpleMapper();
                for (int i=0;i<auditMapping.length;i++) {
                    result[i+1] = simpleMapper.getValue(document, auditMapping[i][1]);
                }
            }
        } catch (XPathExpressionException e) {
            throw new AuditMapperException("Failed",e);
        } catch (SimpleMapperException e) {
            throw new AuditMapperException("Failed",e);
        }
        return result;
    }
    
    public Result setValue (Document document,HttpServletRequest request,String[][] auditMapping, String xpConfigurationComments) 
        throws AuditMapperException {
        Result result = new Result();
        result.data = new String[auditMapping.length+1];
        try {
            String xpAudit = auditMapping[0][1].substring(0, auditMapping[0][1].lastIndexOf("/"));
            String auditName = xpAudit.substring(xpAudit.lastIndexOf("/")+1, xpAudit.length());
            
            XPath xpath = XPathFactory.newInstance().newXPath();
            
            //Search current service in comments
            XPathExpression expr = xpath.compile(xpConfigurationComments);
            NodeList commentList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);
            
            int auditIsCommented = -1;
            for (int i=0;i<commentList.getLength();i++) {
                Node commentNode = commentList.item(i);
                String tagName = commentNode.getNodeValue().trim().substring(0, auditName.length());
                if (tagName.equals(auditName)) {
                    auditIsCommented = i;
                    break;
                }
            }
            
            String auditEnable = request.getParameter(auditName);
            
            expr = xpath.compile(xpAudit);
            NodeList auditNode = (NodeList)expr.evaluate(document, XPathConstants.NODESET);
            if (auditNode.getLength() > 1) {
                throw new AuditMapperException("There is more than one node by " + xpAudit);
            } else if (auditNode.getLength() != 0 && auditIsCommented != -1) {
                throw new AuditMapperException("There is both commeted and real node by " + xpAudit);
            } else if (auditIsCommented != -1) {
                if (auditEnable == null || auditEnable.equals("")) {
                    result.data[0] = "";
                    String data = commentList.item(auditIsCommented).getNodeValue();
                    for (int i=0;i<auditMapping.length;i++) {
                        String attrName = auditMapping[i][1].substring(auditMapping[i][1].lastIndexOf("@")+1,auditMapping[i][1].length());
                        int startInd = data.indexOf("\"", data.indexOf(attrName)+attrName.length());
                        int endInd = data.indexOf("\"",startInd+1);
                        String value = data.substring(startInd+1, endInd);
                        String newValue = request.getParameter(auditMapping[i][0]);
                        if(newValue == null) {
                            newValue = value;
                        }
                        if (!value.equals(newValue)) {
                            result.isChanged = true;
                            data = data.substring(0,startInd+1) + newValue + data.substring(endInd, data.length());
                        }
                        result.data[i+1] = newValue;
                    }
                    commentList.item(auditIsCommented).setNodeValue(data);
                } else {
                    commentList.item(auditIsCommented).getParentNode().removeChild(commentList.item(auditIsCommented));
                    result.data[0] = "1";
                    
                    if (checkAuditClass(document,auditName,xpConfigurationComments.substring(0,xpConfigurationComments.lastIndexOf("/")))) {
                        result.isChanged = true;
                    }
                    
                    SimpleMapper simpleMapper = new SimpleMapper();
                    for (int i=0;i<auditMapping.length;i++) {
                        com.synapsense.ESConfigurator.mappers.SimpleMapper.Result simpleResult = 
                            simpleMapper.setValue(document, auditMapping[i][1], request.getParameter(auditMapping[i][0]));
                        if (simpleResult.isChanged()) {
                            result.isChanged = true;
                        }
                        result.data[i+1] = simpleResult.getData();
                    }
                }
            } else {
                if (auditEnable != null && auditEnable.equals("")) {
                    auditNode.item(0).getParentNode().removeChild(auditNode.item(0));
                    result.data[0] = "";
                    result.isChanged = true;
                    String data = auditName;
                    for (int i=0;i<auditMapping.length;i++) {
                        String attrName = auditMapping[i][1].substring(auditMapping[i][1].lastIndexOf("@")+1,auditMapping[i][1].length());
                        String attrValue = request.getParameter(auditMapping[i][0]);
                        data += " " + attrName + "=\"" + attrValue + "\"";
                        result.data[i+1] = attrValue;
                    }
                    data += "></" + auditName;
                    Node comment = document.createComment(data);
                    String xpConfigNode = xpConfigurationComments.substring(0,xpConfigurationComments.lastIndexOf("/"));
                    expr = xpath.compile(xpConfigNode);
                    Node confNode = (Node)expr.evaluate(document, XPathConstants.NODE);
                    confNode.appendChild(comment);
                } else {
                    if (checkAuditClass(document,auditName,xpConfigurationComments.substring(0,xpConfigurationComments.lastIndexOf("/")))) {
                        result.isChanged = true;
                    }
                    
                    result.data[0] = "1";
                    SimpleMapper simpleMapper = new SimpleMapper();
                    for (int i=0;i<auditMapping.length;i++) {
                        String value = request.getParameter(auditMapping[i][0]);
                        if(value == null) {
                            continue;
                        }
                        com.synapsense.ESConfigurator.mappers.SimpleMapper.Result simpleResult = 
                            simpleMapper.setValue(document, auditMapping[i][1], value);
                        if (simpleResult.isChanged()) {
                            result.isChanged = true;
                        }
                        result.data[i+1] = simpleResult.getData();
                    }
                }
            }
        } catch (XPathExpressionException e) {
            throw new AuditMapperException("Failed",e);
        } catch (SimpleMapperException e) {
            throw new AuditMapperException("Failed",e);
        }
        return result;
    }
    
    private boolean checkAuditClass (Document document,String auditName,String xpConfigNode) 
        throws XPathExpressionException {
        
        XPath xpath = XPathFactory.newInstance().newXPath();
        XPathExpression expr = xpath.compile(xpConfigNode);
        Node confNode = (Node)expr.evaluate(document, XPathConstants.NODE);
        
        //Get list of audit class
        expr = xpath.compile(xpAuditClass);
        NodeList auditNodes = (NodeList)expr.evaluate(document, XPathConstants.NODESET);
        
        boolean isAuditClass = false;
        for (int j=0;j<auditNodes.getLength();j++) {
            if (auditNodes.item(j).getNodeValue().equals(auditClassPrefix+auditName)) {
                isAuditClass = true;
                break;
            }
        }
        if (!isAuditClass) {
            String nodeName = "auditor";
            String attrName = "class";
            String attrValue = auditClassPrefix+auditName;
            Node node = document.createElement(nodeName);
            node.getAttributes().setNamedItem(document.createAttribute(attrName));
            node.getAttributes().getNamedItem(attrName).setNodeValue(attrValue);
            confNode.appendChild(node);
        }
        return !isAuditClass;
    }
}