package com.synapsense.ESConfigurator.mappers;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.ESConfigurator.esException.ESConfiguratorException;
import com.synapsense.utilities.DocumentHelper;

//When we have one node by given XPath
public class SimpleMapper {

    public class SimpleMapperException extends ESConfiguratorException {
        /**
         * 
         */
        private static final long serialVersionUID = 6363025610445108867L;

        public SimpleMapperException(String message, Throwable cause) {
            super(message, cause);
        }

        public SimpleMapperException(String message) {
            super(message);
        }
    }

    public class Result {
        private String data = null;
        private boolean isChanged = false;

        public String getData() {
            return data;
        }

        public boolean isChanged() {
            return isChanged;
        }
    }

    public Result setValue(Document document, final String xPath, String value)
            throws SimpleMapperException {
        Result result = new Result();
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile(xPath);
            NodeList nodeList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);

            if (nodeList.getLength() == 0) {
                if (value != null && !value.equalsIgnoreCase("")) {
                    result.data = value;// XMLHelper.createXmlTextNode(document,
                    // xpName,value);
                    DocumentHelper.createNodeByXPath(document, xPath);
                    Node node = DocumentHelper.getNode(document, xPath);
                    if (node != null) {
                        result.isChanged = true;
                        node.setNodeValue(value);
                    }
                } else {
                    result.data = "";// =
                    // XMLHelper.createXmlTextNode(document,
                    // xpName,"");
                }
            } else if (nodeList.getLength() == 1) {
                if (value != null && !value.equalsIgnoreCase("")) {
                    result.data = value;// =
                    // XMLHelper.createXmlTextNode(document,
                    // xpName,value);
                    if (!value.equals(nodeList.item(0).getNodeValue())) {
                        result.isChanged = true;
                        nodeList.item(0).setNodeValue(value);
                    }
                } else {
                    String xpParentXPath = xPath.substring(0, xPath
                            .lastIndexOf("/"));
                    Node parent = DocumentHelper.getNode(document, xpParentXPath);
                    parent.getParentNode().removeChild(parent);
                    result.data = "";
                    result.isChanged = true;
                    // paramNode = XMLHelper.createXmlTextNode(document,
                    // xpName,nodeList.item(0).getNodeValue());
                }
            } else {
                throw new SimpleMapperException(
                        "SimpleMapper can't set multinode xPath expression: " + xPath);
            }
        } catch (XPathExpressionException e) {
            throw new SimpleMapperException("Failed set value-" + value + " by xPath-" + xPath, e);
        }
        return result;
    }

    public String getValue(Document document, final String xPath)
            throws SimpleMapperException {
        String result = null;
        try {
            XPath xpath = XPathFactory.newInstance().newXPath();
            XPathExpression expr = xpath.compile(xPath);
            NodeList nodeList = (NodeList) expr.evaluate(document,
                    XPathConstants.NODESET);
            if (nodeList.getLength() == 0) {
                result = "";
                // paramNode = XMLHelper.createXmlTextNode(document, xpName,"");
            } else if (nodeList.getLength() == 1) {
                result = nodeList.item(0).getNodeValue();
                // paramNode = XMLHelper.createXmlTextNode(document, xpName,
                // nodeList.item(0).getNodeValue());
            } else {
                throw new SimpleMapperException(
                        "SimpleMapper can't get multinode xPath expression");
            }
        } catch (XPathExpressionException e) {
            throw new SimpleMapperException("Failed get value by xPath-" + xPath,e);
        }
        return result;
    }
}