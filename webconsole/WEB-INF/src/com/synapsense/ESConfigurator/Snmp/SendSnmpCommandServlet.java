package com.synapsense.ESConfigurator.Snmp;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.IpAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;

public class SendSnmpCommandServlet extends BaseHttpServlet {

    private static final long serialVersionUID = -5501185074122784203L;

    private static final Log LOG = LogFactory.getLog(SendSnmpCommandServlet.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String type = req.getParameter("type");
        if ("trap".equals(type)) {
            sendTrap(req, resp);
        } else if ("avdevice".equals(type)) {
            sendAVDeviceTestRequest(req, resp);
        }
    }

    private void sendTrap(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String result = "";
        String localIP = request.getParameter("remote-ip");// request.getParameter("xpGeneralLI");
        try {
            int trapID = 0;// Integer.parseInt(request.getParameter("xpGeneralET"),
                           // 10);
            String enterpriseOID = request.getParameter("xpGeneralEO");// "1.3.6.1.4.1.29078";
            String alertOID = request.getParameter("xpGeneralATO");// "1.3.6.1.4.1.29078.1.2";
            String snmpPort = request.getParameter("remote-port");// request.getParameter("xpPort");
            String writeCommunity = request.getParameter("write-community");

            PDUv1 pdu = new PDUv1();
            pdu.setType(PDU.V1TRAP);
            pdu.setGenericTrap(trapID);
            pdu.setEnterprise(new OID(enterpriseOID));
            pdu.setAgentAddress(new IpAddress(InetAddress.getLocalHost()));
            pdu.addOID(new VariableBinding(new OID(alertOID)));

            CommunityTarget target = new CommunityTarget();
            target.setCommunity(new OctetString(writeCommunity));
            target.setAddress(new UdpAddress(InetAddress.getByName(localIP), new Integer(snmpPort)));
            target.setVersion(SnmpConstants.version1);

            Snmp snmp = null;
            snmp = new Snmp(new DefaultUdpTransportMapping());
            snmp.send(pdu, target);
            result = ErrorMessages.TRAP_WAS_SENT_TO + "|" + localIP;
        } catch (NumberFormatException e) {
            LOG.error(e.getMessage(), e);
            result = ErrorMessages.ERROR_OCCURRED_WHILE_SENDING_TRAP;
        } catch (UnknownHostException e) {
            LOG.error(e.getMessage(), e);
            result = ErrorMessages.WRONG_REMOTE_IP_ADDRESS + "|" + localIP;
        }
        sendResponse(response, "<result>" + result + "</result>");
    }

    private void sendAVDeviceTestRequest(HttpServletRequest request, HttpServletResponse response) throws IOException,
            ServletException {
        String result = "";
        String remoteIp = request.getParameter("remote-ip");
        String remotePort = request.getParameter("remote-port");
        String writeCommunity = request.getParameter("write-community");
        try {
            if (sendAVDeviceTestRequest(remoteIp, remotePort, writeCommunity)) {
                result = ErrorMessages.ALERT_4E_TEST_SUCCESS + "|" + remoteIp;
            } else {
                result = ErrorMessages.ALERT_4E_TEST_FAIL + "|" + remoteIp;
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
            result = ErrorMessages.ALERT_4E_TEST_FAIL + "|" + remoteIp;
        }
        sendResponse(response, "<result>" + result + "</result>");
    }

    private boolean sendAVDeviceTestRequest(String devAddress, String devPort, String writeCommunity) throws Exception {
        boolean result = true;
        TransportMapping transport = new DefaultUdpTransportMapping();
        transport.listen();
        CommunityTarget comtarget = new CommunityTarget();
        comtarget.setCommunity(new OctetString(writeCommunity));
        comtarget.setAddress(new UdpAddress(devAddress + "/" + devPort));
        Snmp snmp = new Snmp(transport);

        PDU pdu = createAudioVideoPDU(1, 0, 0, 0);		
        ResponseEvent response = snmp.set(pdu, comtarget);

        if (response.getResponse() == null) {
            result = false;
        }

        if (result) {
            Thread.sleep(10000);
            
            pdu = createAudioVideoPDU(0, 0, 0, 0);
            response = snmp.set(pdu, comtarget);
        }

        if (response.getResponse() == null) {
            result = false;
        }

        snmp.close();
        return result;
    }
    
    private PDU createAudioVideoPDU(int green, int red, int yellow, int audio){
        final String GREEN_OID = ".1.3.6.1.4.1.20916.1.6.2.3.0";
        final String YELLOW_OID = ".1.3.6.1.4.1.20916.1.6.2.2.0";
        final String RED_OID = ".1.3.6.1.4.1.20916.1.6.2.1.0";
        final String AUDIO_OID = ".1.3.6.1.4.1.20916.1.6.2.6.0";
        
        PDU pdu = new PDU();
        VariableBinding varGreen = new VariableBinding(new OID(GREEN_OID), new Integer32(green));
        VariableBinding varRed = new VariableBinding(new OID(RED_OID), new Integer32(red));
        VariableBinding varYellow = new VariableBinding(new OID(YELLOW_OID), new Integer32(yellow));
        VariableBinding varAudio = new VariableBinding(new OID(AUDIO_OID), new Integer32(audio));

        pdu.add(varGreen);
        pdu.add(varRed);
        pdu.add(varYellow);
        pdu.add(varAudio);

        pdu.setType(PDU.SET);
        
        return pdu;
    }
}