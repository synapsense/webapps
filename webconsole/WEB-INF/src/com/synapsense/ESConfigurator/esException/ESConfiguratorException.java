package com.synapsense.ESConfigurator.esException;

public class ESConfiguratorException extends Exception {
    /**
     * 
     */
    private static final long serialVersionUID = -6762367191203924901L;
    
    public ESConfiguratorException (String message,Throwable cause) {
        super(message,cause);
    }
    public ESConfiguratorException (String message) {
        super(message);
    }
}