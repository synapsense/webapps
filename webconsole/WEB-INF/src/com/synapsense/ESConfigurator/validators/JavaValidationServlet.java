package com.synapsense.ESConfigurator.validators;

import java.io.IOException;
import java.lang.reflect.Method;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.utilities.BaseHttpServlet;

public class JavaValidationServlet extends BaseHttpServlet {

    private static final long serialVersionUID = 7640280589076258181L;
    
    private static final Log LOG = LogFactory.getLog(JavaValidationServlet.class);
    private final String packageName = JavaValidationServlet.class.getPackage().getName(); 
    private final String validatorMethodName = "validate";
    
    @Override
    protected void doGet (HttpServletRequest request, HttpServletResponse response) 
        throws IOException, ServletException {    	
        ValidationResult result = new ValidationResult();
        result.success = false;
        result.error = "message/UNABLE_TO_COMPLETE";
        
        try {
            
            String jvalidatorclass = request.getParameter("jvalidatorclass");
            Class<?> validatorClass = Class.forName(packageName + "." + jvalidatorclass);
            Method validate = validatorClass.getDeclaredMethod(validatorMethodName, new Class[] {HttpServletRequest.class});
            result = (ValidationResult)validate.invoke(null, new Object[] {request});
            
        } catch (Exception e) {
            result.deverror = e.getLocalizedMessage();
            LOG.error(e.getMessage(),e);
        }
        sendResponse(response, result.toString());
    }
    
    @Override
    protected void doPost (HttpServletRequest request, HttpServletResponse response) 
        throws IOException, ServletException {
        this.doGet(request, response);
    }
    
}