package com.synapsense.ESConfigurator.validators;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.utilities.XMLHelper;

public class ValidationResult {
    private static final Log LOG = LogFactory.getLog(ValidationResult.class);
    
    public Boolean success = false;
    public String error = "unknown_error";
    public String warning = "";
    public String deverror = "unknown_dev_error";
    public String objname = "";
    
    public String toString(){
        
        try {
            
            Document doc = XMLHelper.createNewXmlDocument("validationresult");
            Node root = doc.getFirstChild();
            root.appendChild(XMLHelper.createXmlTextNode(doc, "success", success.toString()));
            root.appendChild(XMLHelper.createXmlTextNode(doc, "error", error));
            root.appendChild(XMLHelper.createXmlTextNode(doc, "warning", warning));
            root.appendChild(XMLHelper.createXmlTextNode(doc, "deverror", deverror));
            root.appendChild(XMLHelper.createXmlTextNode(doc, "objname", objname));
            return XMLHelper.getDocumentXml(doc);
            
        } catch (ParserConfigurationException e) {
            LOG.error(e.getMessage(),e);
            StringBuffer result = new StringBuffer();
            result.append("<validationresult>");        
                result.append("<success>false</success>");
                result.append("<error>ParserConfigurationException</error>");
                result.append("<warning>ParserConfigurationException</warning>");
                result.append("<deverror>ParserConfigurationException</deverror>");
                result.append("<objname></objname>");
            result.append("/validationresult>");            
            return result.toString();
        }        
    }
}
