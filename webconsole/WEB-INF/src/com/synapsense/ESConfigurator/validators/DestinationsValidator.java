package com.synapsense.ESConfigurator.validators;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageType;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.service.FileWriter;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.lang.LangManager;

public class DestinationsValidator {
    // private static final long serialVersionUID = -3996473516177343612L;
    private static final Log LOG = LogFactory
            .getLog(DestinationsValidator.class);

    private final static String snmpPrefix = "snmp/";
    private final static String snmpFileName = "snmpconf.xml";
    private final static String trapConfTag = "snmp:trap";
    private final static String avdeviceConfTag = "snmp:avdevice";
    private final static String trapTag = "trap";
    private final static String avdeviceTag = "avdevice";
    private final static String trapsParamName = "xpTraps";
    private final static String avdeviceParamName = "xpAVDevices";

    public static ValidationResult validate(HttpServletRequest request) {
        ValidationResult result = new ValidationResult();
        result.success = true;
        try {
            if (!validateOfDestinationsHasAlerts(result, request, trapsParamName, trapTag, trapConfTag, MessageType.SNMP)) {
                return result;
            }
            if (!validateOfDestinationsHasAlerts(result, request, avdeviceParamName, avdeviceTag, avdeviceConfTag, MessageType.AUDIO_VISUAL)) {
                return result;
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
            result.error = "text/error";
            result.success = false;
        } catch (NoSuchAlertTypeException e) {
            LOG.error(e.getMessage(), e);
            result.error = "text/error";
            result.success = false;
        }

        return result;
    }

    private static boolean validateOfDestinationsHasAlerts(
            ValidationResult result, HttpServletRequest request, String paramName, String tagName, String confTagName, MessageType messageType)
            throws IOException, NoSuchAlertTypeException {
        Set<String> avDest = getListAvailableDestination(request, confTagName);
        Set<String> chDest = getListChangedDestination(request, paramName, tagName);
        List<String> destinations = getChangedDestination(chDest, avDest);
        HashMap<String, Collection<AlertType>> alertTypesMap = new HashMap<String, Collection<AlertType>>();

        for (String destination : destinations) {
            addAlertTypesByDestination(request, destination, alertTypesMap, messageType);
        }

        if (alertTypesMap.isEmpty()) {
            result.objname = paramName;
            result.success = true;
            result.warning = "";
        } else {
            result.objname = paramName;
            result.success = false;
            String errmessage = LangManager.getInstance().getLocaleString(LangManager.getCurrentLocaleId(request), "message_are_you_sure_to_update_SNMP_configuration_");
            result.warning = errmessage + "\n" +prepareMessage(alertTypesMap);
        }
        return result.success;
    }

    private static Set<String> getListChangedDestination(
            HttpServletRequest request, String paramName, String tagName) {
        String destination = request.getParameter(paramName);
        Document resultDoc = XMLHelper.getDocument(destination);
        NodeList list = resultDoc.getElementsByTagName(tagName);

        Set<String> dest = new HashSet<String>();
        for (int i = 0; i < list.getLength(); i++) {
            String ip = ((Element) list.item(i))
                    .getElementsByTagName("remote-ip").item(0).getTextContent();
            String port = ((Element) list.item(i))
                    .getElementsByTagName("remote-port").item(0)
                    .getTextContent();
            if (!"add".equals(((Element) list.item(i)).getAttribute("action"))) {
                dest.add(ip + ":" + port);
            }
        }
        return dest;
    }

    private static Set<String> getListAvailableDestination(
            HttpServletRequest request, String destTag) throws IOException {
        Document resultDoc = null;

        String absoluteFN = DestinationsValidator.snmpPrefix
                + DestinationsValidator.snmpFileName;
        FileWriter fw = Env.getFileWritter();
        resultDoc = XMLHelper.getDocument(fw.readFile(absoluteFN));

        Element rootEl = resultDoc.getDocumentElement();
        NodeList list = rootEl.getElementsByTagName(destTag);

        Set<String> dest = new HashSet<String>();
        for (int i = 0; i < list.getLength(); i++) {
            String ip = ((Element) list.item(i)).getAttribute("remote-ip");
            String port = ((Element) list.item(i)).getAttribute("remote-port");
            dest.add(ip + ":" + port);
        }
        return dest;
    }

    private static List<String> getChangedDestination(Set<String> changedList,
            Set<String> availableList) {
        List<String> resList = new ArrayList<String>();
        for (String d : availableList) {
            if (!changedList.contains(d)) {
                resList.add(d);
            }
        }
        return resList;
    }

    private static void addAlertTypesByDestination(HttpServletRequest request, String dest,
            HashMap<String, Collection<AlertType>> alertTypesDestinationMap, MessageType messageType)
            throws NoSuchAlertTypeException {
        Collection<AlertType> cat = Env.getAlertingServ(request).getAlertTypesByDestination(dest, messageType);
        if (!cat.isEmpty()) {
            alertTypesDestinationMap.put(dest, cat);
        }

    }

    private static String prepareMessage(
            HashMap<String, Collection<AlertType>> alertTypesMap) {
        String res = "";
        int rowCount = 0;
        for (String k : alertTypesMap.keySet()) {
            if (rowCount == 7) {
                res += "...";
                break;
            }
            String alertTypeNames = "";
            Collection<AlertType> ats = alertTypesMap.get(k);

            for (AlertType at : ats) {
                String alertName = at.getName();
                if (alertName.startsWith(Constants.CONSOLE_ALERT_PREFIX)) {
                    alertName = alertName
                            .substring(Constants.CONSOLE_ALERT_PREFIX.length());
                }
                alertTypeNames += alertName + ", ";
            }
            alertTypeNames = alertTypeNames.substring(0,
                    alertTypeNames.lastIndexOf(","));

            int maxRowLength = 200;
            if (alertTypeNames.length() > maxRowLength) {
                alertTypeNames = alertTypeNames.substring(0, maxRowLength)
                        + "...";
            }

            res += k + " - " + alertTypeNames + "\n";
            rowCount++;
        }
        return res;
    }

}
