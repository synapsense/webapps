package com.synapsense.ESConfigurator.validators;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class IPValidator {
    
    private static final Log LOG = LogFactory.getLog(IPValidator.class);
    
    public static boolean validate (String ipAddress,StringBuffer error,HttpServletRequest request) {
        boolean result = true;
        try {
            InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            LOG.error(e.getMessage(),e);
            error.append("Ip address " + ipAddress + " isn't exists");
            result = false;
        }
        return result;
    }
}