package com.synapsense.ESConfigurator.validators;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.security.KeyStore;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.ESConfigurator.utils.UploadFile;
import com.synapsense.dataAccess.Env;
import com.synapsense.utilities.JbossManagementAPI;

public class SecurityConfigValidator {
    private static final Log LOG = LogFactory.getLog(SecurityConfigValidator.class);
    private static final String KEYSTORE_FILE_NAME = "server.keystore";
    
    public static ValidationResult validate(HttpServletRequest request) {
        ValidationResult result = new ValidationResult();
        result.success = true;

        if (!validateKeystorePassword(result, request)) {
            return result;
        }

        if (!validateSecurePort(result, request)) {
            return result;
        }

        return result;
    }

    private static boolean validateKeystorePassword(ValidationResult result, HttpServletRequest request) {
        java.io.ByteArrayInputStream inputStream = null;
        String password = request.getParameter("keystorePassword");
        String mode = request.getParameter("secureMode");
        String keystoreWasUploadedStr = request.getParameter("keystoreUploaded");
        String keystoreFileId = request.getParameter("keystoreFileId");
        //String fileName = request.getParameter("xpFile");

        boolean keystoreWasUploaded = false;
        if (keystoreWasUploadedStr != null && keystoreWasUploadedStr.equals("true")) {
            keystoreWasUploaded = true;
        }
        
        result.objname = "keystorePassword";
        result.success = true;

        if (mode.equals("1") || mode.equals("2")) {
            try {
                char[] passChars = password.toCharArray();
                KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());

                byte[] keystoreFile = null;
                if (keystoreWasUploaded) {
                    keystoreFile = UploadFile.getUploadedFile(keystoreFileId);
                //} else if (fileName != null && !fileName.equals("")) {
                } else {
                    try {
                        keystoreFile = Env.getFileWritter().readFileBytes(KEYSTORE_FILE_NAME);
                    } catch (IOException e) {
                        LOG.error(e.getLocalizedMessage(), e);
                    }
                }
                if (keystoreFile == null) {
                    result.error = "message/keystore_not_uploaded";
                    result.success = false;
                    LOG.info("Keystore file is not uploaded.");
                } else {
                    inputStream = new ByteArrayInputStream(keystoreFile);
                    keyStore.load(inputStream, passChars);
                }
            } catch (Exception e) {
                result.error = "message/invalid_keystore_password";
                result.success = false;
                LOG.error(e.getLocalizedMessage(), e);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                    }
                }
            }
        }
        return result.success;
    }

    private static boolean validateSecurePort(ValidationResult result, HttpServletRequest request) {
        int securePort = Integer.parseInt(request.getParameter("securePort"));
        String mode = request.getParameter("secureMode");
        String changedItems = request.getParameter("changedItems");
        result.objname = "securePort";
        result.success = true;
        if (!mode.equals("0") && changedItems.contains("securePort")) {
            JbossManagementAPI jbossUtil = null;
            try {
                jbossUtil = JbossManagementAPI.createFromRequest(request);
                boolean isPortAvailable = jbossUtil.isPortAvailableForSecuredConnector(securePort);
                
                if (!isPortAvailable) {
                    result.error = "message/port_already_in_use";
                    result.success = false;
                    LOG.info("Port already in use: connectors.");
                }

                // Check ports used by system
                ServerSocket serverSocket = null;
                if (result.success == true && isPortAvailable) {
                    try {
                        serverSocket = new ServerSocket(securePort);
                    } catch (Exception e) {
                        result.error = "message/port_already_in_use";
                        result.success = false;
                        LOG.info("Port already in use: system.");
                    } finally {
                        if (serverSocket != null && !serverSocket.isClosed()) {
                            serverSocket.close();
                        }
                    }
                }
            } catch (Exception e) {
                result.error = "message/UNABLE_TO_COMPLETE";
                result.deverror = e.getMessage();
                result.success = false;
                LOG.error(e.getMessage(), e);
            } finally {
                if (jbossUtil != null) {
                    try {
                        jbossUtil.close();
                    } catch (IOException e) {
                        LOG.error(e.getMessage(), e);
                    }
                }
            }
        }

        return true;
    }
}
