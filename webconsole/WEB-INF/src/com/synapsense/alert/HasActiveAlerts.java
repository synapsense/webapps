package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class HasActiveAlerts extends BaseHttpServlet {
    private static final long serialVersionUID = -2694215900764220041L;
    private static final Log LOG = LogFactory.getLog(HasActiveAlerts.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        sendResponse(resp, getAlertTypesDoc(req));
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private Document getAlertTypesDoc(HttpServletRequest req) {
        
        Collection<TO<?>> roots = Env.getEnv(req).getObjectsByType(EnvironmentTypes.TYPE_ROOT); 
        Collection<Alert> alerts = Env.getAlertingServ(req).getActiveAlerts(TOFactory.EMPTY_TO, true);

        Document doc = null;

        try {
            Integer numAlerts = 0;
            for(TO<?> root : roots) {		
                numAlerts += Env.getEnv(req).getPropertyValue(root, "numAlerts", Integer.class);
            }
            
            doc = XMLHelper.createNewXmlDocument("active_alert");

            Element root = doc.getDocumentElement();

            if (!alerts.isEmpty() || (numAlerts != null && numAlerts > 0) ) {
                root.setAttribute("v", "true");
                return doc;
            }
        } catch (EnvException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return doc;
    }
    
}
