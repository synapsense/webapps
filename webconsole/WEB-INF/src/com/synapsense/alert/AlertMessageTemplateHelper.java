package com.synapsense.alert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageTemplateType;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.utilities.CommonFunctions;
import com.synapsense.utilities.es.CollectionTOSorter;
import com.synapsense.utilities.es.EnvironmentTypes;

public class AlertMessageTemplateHelper {
    private static final org.apache.commons.logging.Log LOG = LogFactory.getLog(AlertMessageTemplateHelper.class);
    
    private static final HashMap<String, String> macrosUiNameToRuleTokenName = new HashMap<String, String>()
    {
        private static final long serialVersionUID = 7992659173206299742L;

    {
        put("$DCNAME$", "$parent(DC).name$");
        put("$ROOMNAME$", "$parent(ROOM).name$");
        put("$OBJECTNAME$", "$name$");
        put("$LOCATION$", "$location$");
        put("$ALERT_LINK$", "$console_url$?actionType=handleAlert&alertId=$alert_id$");
    }};
    
    private HttpServletRequest request;
    
    public AlertMessageTemplateHelper(HttpServletRequest request) {
        this.request = request;
    }
    
    public static String messageBodyToUIBody(String templateBody) {
        Set<Entry<String, String>> set = macrosUiNameToRuleTokenName.entrySet();
        for(Entry<String, String> entry : set) {
            CharSequence replaceWhat = entry.getValue();
            CharSequence replaceWith = entry.getKey();
            templateBody = templateBody.replace(replaceWhat, replaceWith);
        }
        return templateBody;
    }
    
    public static String uiBodyToTemplateBody(String uiBody) {
        Set<Entry<String, String>> set = macrosUiNameToRuleTokenName.entrySet();
        for(Entry<String, String> entry : set) {
            CharSequence replaceWhat = entry.getKey();
            CharSequence replaceWith = entry.getValue();
            uiBody = uiBody.replace(replaceWhat, replaceWith);
        }
        return uiBody;
    }
    
    public MessageType getTemplateMessageType(TO<?> templateId) {
        MessageType messageType = null;
        if(templateId != null) {
            String messageTypeStr = getTemplatePropertyValue(templateId, "messagetype");
            return numberStringToMessageType(messageTypeStr);
        }
        return messageType;
    }
    
    public MessageType numberStringToMessageType(String strNum) {
        MessageType [] messageTypes = MessageType.values();
        for(MessageType messageType : messageTypes) {
            String code = Integer.toString(messageType.code());
            if(code.equals(strNum)) {
                return messageType;
            }
        }
        return null;
    }
        
    public MessageTemplate getMessageTemplate(AlertType alertType, MessageType messageType) {
        Set<MessageTemplate> smt = alertType.getMessageTemplates();
        for(MessageTemplate messageTemplate : smt) {
            if(messageTemplate.getMessageType() == messageType) {
                return messageTemplate;
            }
        }
        return null;
    }
    
    public Collection<AlertType> getAlertTypesUsesThisTemplate(TO<?> templateId) {
        Collection<AlertType> list = new ArrayList<AlertType>();
        try {
            Collection<AlertType> atcol = Env.getAlertingServ(request).getAllAlertTypes();				
            for(AlertType alertType : atcol) {
                Set<MessageTemplate> mts = alertType.getMessageTemplates();
                for(MessageTemplate messageTemplate : mts) {
                    if(messageTemplate.getRef().equals(templateId)) {
                        if(!list.contains(alertType)) {
                            list.add(alertType);
                        }
                    }
                }
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return list;
    }
    
    public boolean isTemplateUsedByAlertType(TO<?> templateId) {
        return !getAlertTypesUsesThisTemplate(templateId).isEmpty();
    }
    
    public void deleteAllMessageTemplates() {
        try {
            Environment env = Env.getEnv(request);
            for (TO<?> to : env.getObjectsByType(EnvironmentTypes.TYPE_ALERT_MESSAGE_TEMPLATE)) {
                env.deleteObject(to);
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    public void deleteMessageTemplate(TO<?> templateId) {
        try {
            Env.getEnv(request).deleteObject(templateId);
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    public TO<?> getMessageTemplate(String name) {
        Collection<TO<?>> tos = Env.getEnv(request).getObjects(EnvironmentTypes.TYPE_ALERT_MESSAGE_TEMPLATE, new ValueTO[] {new ValueTO("name", name)});
        if (tos.isEmpty()) {
            return null;
        } else {
            return tos.iterator().next();
        }
    }
    
    public boolean isMessageTemplateNameCustom(TO<?> templateId) {
        Integer type;
        try {
            type = Env.getEnv(request).getPropertyValue(templateId, "type", Integer.class);
            return type != null && type == MessageTemplateType.CUSTOM.code();
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return false;
    }
    
    public TO<?> createMessageTemplate(String name, String messageType, 
            String description, String header, String body, MessageTemplateType type) {
        try {
            Environment env = Env.getEnv(request);
            ValueTO [] ValueArr = {
                    new ValueTO("name", name),
                    new ValueTO("messagetype", messageType),
                    new ValueTO("description", description),
                    new ValueTO("header", header),
                    new ValueTO("body", body),
                    new ValueTO("type", type.code())
                    };
            return env.createObject(EnvironmentTypes.TYPE_ALERT_MESSAGE_TEMPLATE, ValueArr);
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
    }
    
    public void setMessageTemplate(TO<?> templateId, String newTemplateName, String messageType, 
            String description, String header, String body) {
        try {
            Environment env = Env.getEnv(request);
            if(templateId != null) {
                ValueTO [] ValueArr = {
                        new ValueTO("name", newTemplateName),
                        new ValueTO("messagetype", messageType),
                        new ValueTO("description", description),
                        new ValueTO("header", header),
                        new ValueTO("body", body)
                        };
                env.setAllPropertiesValues(templateId, ValueArr);
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    public Collection<TO<?>> geMessageTemplatesSameTypeAs(TO<?> templateId) {
        String messageType = getTemplatePropertyValue(templateId, "messagetype");
        return getMessageTemplatesByType(messageType);
    }
    
    public String setTemplatePropertyValue(TO<?> templateId, String property, String value) {
        Environment env = Env.getEnv(request);
        try {
            env.setPropertyValue(templateId, property, value);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return value;
    }
    
    public String getTemplatePropertyValue(TO<?> template, String property) {
        String value = "unknown";
        Environment env = Env.getEnv(request);
        try {
            value = env.getPropertyValue(template, property, String.class);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return value;
    }
    
    public Collection<TO<?>> getMessageTemplatesByType(String messageType) {
        Environment env = Env.getEnv(request);
        Collection<TO<?>> cto = null;
        try {
            ValueTO [] vtoa = {
                    new ValueTO("messagetype", messageType),
                    };
            cto = env.getObjects(EnvironmentTypes.TYPE_ALERT_MESSAGE_TEMPLATE, vtoa);
            if(!cto.isEmpty()) {
                Collection<TO<?>> ctofilter = new ArrayList<TO<?>>();
                for(TO<?> to : cto) {
                    if(!isMessageTemplateNameCustom(to)) {
                        ctofilter.add(to);
                    }
                }
                cto = ctofilter;
                
                if(!cto.isEmpty()) {
                    Collection<CollectionTO> ccTO = env.getPropertyValue(cto, new String[] {"name"});
                    List<CollectionTO> lcTO = CollectionTOSorter.sortObjects(ccTO, "name");
                    cto = new ArrayList<TO<?>>();
                    for(CollectionTO cTO : lcTO) {
                        cto.add(cTO.getObjId());
                    }
                }
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return cto;
    }

    public TO<?> getDefaultMessageTemplate(String messageType) {
        Environment env = Env.getEnv(request);
        try {
            ValueTO [] vtoa = {
                    new ValueTO("messagetype", messageType),
//					new ValueTO("type", AlertNotificationType.DEFAULT.code())
                    };
            Collection<TO<?>> cto = env.getObjects(EnvironmentTypes.TYPE_ALERT_MESSAGE_TEMPLATE, vtoa);
            for (TO<?> to : cto) {
                Integer type = env.getPropertyValue(to, "type", Integer.class);
                if (type != null && MessageTemplateType.DEFAULT.code() == type) {
                    return to;
                }
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return null;
}

    //TODO: remove
    public String getMessageTemplateAsXmlString(TO<?> templateId) {
        return extractTemplateXmlStringFromTO(templateId);
    }
    
    private String extractTemplateXmlStringFromTO(TO<?> to) {
        CollectionTO cto = Env.getEnv(request).getAllPropertiesValues(Arrays.asList(new TO<?>[]{to})).iterator().next();
        StringBuilder sb = new StringBuilder();
        String name = (String)cto.getSinglePropValue("name").getValue();
        if (name == null) {name = "";}
        String messagetype = (String)cto.getSinglePropValue("messagetype").getValue();
        Integer type = (Integer)cto.getSinglePropValue("type").getValue();
        String description = (String)cto.getSinglePropValue("description").getValue();
        if(description == null) {description = "";}
        String header = AlertMessageTemplateHelper.messageBodyToUIBody((String)cto.getSinglePropValue("header").getValue());
        String messageBody = (String)cto.getSinglePropValue("body").getValue();
        String body = AlertMessageTemplateHelper.messageBodyToUIBody(messageBody);
        sb.append("<template name='" + CommonFunctions.escapeXML(name) + "' id='" + TOFactory.getInstance().saveTO(to) + "' messagetype='" + messagetype + "' type='" + type  + "'>");
            sb.append("<description><![CDATA[" + description + "]]></description>");
            sb.append("<header><![CDATA[" + header + "]]></header>");
            sb.append("<body><![CDATA[" + body + "]]></body>");
        sb.append("</template>");
        return sb.toString();
    }
    
    public String getMessageTemplatesAsXmlStringByType(String messageType, boolean includecustom) {
        StringBuilder sb = new StringBuilder();
        Collection<TO<?>> cto = getMessageTemplatesByType(messageType);
        for(TO<?> to : cto) {
            if (isMessageTemplateNameCustom(to) && !includecustom) {
                continue;
            }
            sb.append(extractTemplateXmlStringFromTO(to));
        }
        return sb.toString();
    }

    public Set<MessageTemplate> getTemplatesFromJsonArr(String alertTypeName, String templatesJsonArr) {
        Set<MessageTemplate> smt = new HashSet<MessageTemplate>();
        try {
            JSONArray jarr = new JSONArray(templatesJsonArr);	        
            for(int i = 0; i < jarr.length(); i++) {
                JSONObject jobj = jarr.getJSONObject(i);
                TO<?> templateId = null;
                String templateIdStr = null;
                if (jobj.has("id")) {
                    templateIdStr = jobj.getString("id");
                }
                if (templateIdStr != null) {
                    templateId = TOFactory.getInstance().loadTO(templateIdStr);
                }
                String messageType = jobj.getString("messagetype");
                int type = jobj.getInt("type");
                
                if(type == MessageTemplateType.CUSTOM.code()) {
                    String header = jobj.getString("header");
                    header = uiBodyToTemplateBody(header);
                    String body = jobj.getString("body");
                    body = uiBodyToTemplateBody(body);
                    if(templateId == null) {
                        templateId = createMessageTemplate(null, messageType, "", header, body, MessageTemplateType.CUSTOM);
                    } else {
                        setMessageTemplate(templateId, null, messageType, "", header, body);
                    }
                }
                MessageTemplate mt = new MessageTemplateRef(numberStringToMessageType(messageType), templateId);
                smt.add(mt);
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return smt;
    }
}
