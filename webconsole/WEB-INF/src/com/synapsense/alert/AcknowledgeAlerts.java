package com.synapsense.alert;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.tasktracker.StatesEnum;
import com.synapsense.tasktracker.TaskData;
import com.synapsense.tasktracker.TaskDescr;
import com.synapsense.tasktracker.TaskID;
import com.synapsense.tasktracker.TaskState;
import com.synapsense.tasktracker.TaskStorage;
import com.synapsense.tasktracker.TaskUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;

public class AcknowledgeAlerts extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 5102805084873996921L;
    private static final Log LOG = LogFactory.getLog(AcknowledgeAlerts.class);
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        String status = ErrorMessages.UNABLE_TO_COMPLETE;
        StatesEnum state = StatesEnum.ERROR;
         
        TaskID taskId = TaskUtils.getTaskIDByRequest(req);

        TaskState taskState = new TaskState(StatesEnum.IN_PROGRESS, "Execution in progress");
        TaskStorage.getInstance().addTask(taskId, new TaskData(taskState, new TaskDescr(req.getParameter("taskName"))), req); 
        
        try {
            performOperation(taskId, req);
                state = StatesEnum.DONE;
                status = ErrorMessages.SUCCESS;
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
            throw new ServletException("Object is removed", e);
        } catch (NamingException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (NumberFormatException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
         
        taskState.updateState(state, ErrorMessages.getText(status)); 
                
        String str = "<result>" + status + "</result>";
        //String str = "<result>" + TaskStorage.getInstance().getTaskData(taskId, req).getTaskState().packToStr() + "</result>";
        
        sendResponse(resp, str);
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    protected void performOperation(TaskID taskId, HttpServletRequest request) throws NamingException, NumberFormatException, ObjectNotFoundException {    	
        
        String mode = request.getParameter("mode");
        mode = (mode == null)? "" : mode;                
        
        if("acknowledgeSelected".equals(mode)){
            String arrAlertIds = request.getParameter("arrAlertIds");
            if (arrAlertIds != null) {
                for(String alertId : arrAlertIds.split(",")) {
                    AlertUtils.acknowledgeAlert(Integer.parseInt(alertId), request);
                }
            }
        } else if("resolveSelected".equals(mode)){
            String arrAlertIds = request.getParameter("arrAlertIds");
            if (arrAlertIds != null) {
                for(String alertId : arrAlertIds.split(",")) {
                    AlertUtils.resolveAlert(Integer.parseInt(alertId), request);
                }
            }
        } else if("dismissSelected".equals(mode)){
            String arrAlertIds = request.getParameter("arrAlertIds");
            if (arrAlertIds != null) {
                for(String alertId : arrAlertIds.split(",")) {
                    AlertUtils.dismissAlert(Integer.parseInt(alertId), request);
                }
            }
        } else if("dismissAll".equals(mode)){
            String [] objIds = request.getParameter("id").split(",");            
            String deepPar = request.getParameter("deep");
            boolean deep = false;
            
            if(deepPar != null && "true".equalsIgnoreCase(deepPar)){
                deep = true;
            }
            
            for(String objId : objIds) {
                AlertUtils.dismissAllAlerts(objId, deep, request);
            }
        }
    }

}
