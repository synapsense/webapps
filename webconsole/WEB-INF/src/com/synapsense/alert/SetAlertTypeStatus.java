package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;

public class SetAlertTypeStatus extends BaseHttpServlet {

    private static final long serialVersionUID = -2694215900764220041L;
    private static final Log LOG = LogFactory.getLog(SetAlertTypeStatus.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        String result = ErrorMessages.UNABLE_TO_COMPLETE;

        String name = req.getParameter("name");
        String status = req.getParameter("status");
        if(status == null) {
            sendResponse(resp, "<result>" + result + "</result>");
            return;
        }
        
        boolean alertStatus = Boolean.parseBoolean(status);
                
        try {
            Collection<AlertType> alertTypes = Env.getAlertingServ(req).getAlertTypes(new String[] {name});
            if(alertTypes.isEmpty()) {
                throw new ServletException("Object [" + name + "] is removed.");
            }
            for (AlertType alertType: alertTypes) {
                if(LOG.isDebugEnabled()) {
                    LOG.debug("Set alert status to '" + alertStatus + "'");
                }
                alertType.setIsActive(alertStatus);
                if(LOG.isDebugEnabled()) {
                    LOG.debug("Update '" + name + "' alert");
                }
                Env.getAlertingServ(req).updateAlertType(name, alertType);
            }
            result = (alertStatus) ? ErrorMessages.ALERT_TYPE_ACTIVATED : ErrorMessages.ALERT_TYPE_DEACTIVATED;
        
        } catch (NoSuchAlertTypeException e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        sendResponse(resp, "<result>" + result + "</result>");
        LOG.info(this.getServletName() + " is finished ...");
    }
}
