package com.synapsense.alert;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.rule.Expression;
import com.synapsense.alert.rule.Rule;
import com.synapsense.alert.rule.RuleUtils;
import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.DREException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.util.TranslatingAlertService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import org.json.*;

public class UpdateAlertType extends BaseHttpServlet {

    private static final long serialVersionUID = -8766692067775248363L;
    private static final Log LOG = LogFactory.getLog(UpdateAlertType.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");

        req.setCharacterEncoding("utf-8");
        
        String resultMessage = null;
        try {
            resultMessage = updateAlert(req);
        } catch (JSONException e) {
            resultMessage = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        if (resultMessage != null) {
            sendResponse(resp, "<result>" + resultMessage + "</result>");
        }
        LOG.info(this.getServletName() + " is finished ...");
    }

    private String updateAlert(HttpServletRequest request) throws JSONException {
        String result = null;

        try {

            boolean isSystem = Boolean.valueOf(request.getParameter("system"));
            String alertDisplayName = request.getParameter(RequestVariableNames.DISPLAY_NAME);
            String alertTypeName = request.getParameter(RequestVariableNames.NAME);
            
            String alertDesc = request.getParameter(RequestVariableNames.DESCRIPTION);
            String alertPriorityNumberString = request.getParameter(RequestVariableNames.PRIORITY);
            String alertAutodismissStr = request.getParameter(RequestVariableNames.AUTODISMISS);
            String alertMsgHeader = request.getParameter(RequestVariableNames.MESSAGE_HEADER);
            String alertMsgBody = request.getParameter(RequestVariableNames.MESSAGE_BODY);
            String autoAcknowledge = request.getParameter("autoack");
            
            if (alertTypeName == null || alertDisplayName == null) {
                return ErrorMessages.UNABLE_TO_COMPLETE;
            }
            
            
            //TODO: do we need this check now?
//			if(!alertTypeName.equals(oldAlertName)){//if name is changed we should check new name
//				if(AlertUtils.isAlertExists(alertTypeName, request)){
//					LOG.error("Rule or alert type for '" + alertTypeName + "' already exists");
//					return ErrorMessages.DUPLICATE_ALERT_NAME;
//				}
//			}
            
            if(LOG.isDebugEnabled()) {
                LOG.debug("Get alert type for '" + alertTypeName + "' alert");
            }
            
            //call original AlertService to get non-translated alert type,
            //we don't want to save translate properties
            AlertType originalAlertType = ((TranslatingAlertService)Env.getAlertingServ(request)).getAlertService().getAlertType(alertTypeName);
            AlertType alertType = Env.getAlertingServ(request).getAlertType(alertTypeName);
            if(LOG.isDebugEnabled()) {
                LOG.debug("Get rule type <TO> for '" + alertTypeName + "' alert");
            }
            
            DRuleTypeService<Object> ruleTypeDAO = Env.getDRuleTypeDAO();
            TO<?> ruleTypeTo = ruleTypeDAO.getRuleTypeByName(alertTypeName);

            if ((ruleTypeTo == null && !isSystem) || alertType == null) {
                LOG.error("Rule type <TO> for '" + alertTypeName + "' alert is null or alertType is empty");
                return ErrorMessages.UNABLE_TO_COMPLETE;
            }
            
            AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
            Set<MessageTemplate> msgTemplates = helper.getTemplatesFromJsonArr(alertTypeName, request.getParameter("notificationsjson"));
            
            boolean alertDisplayNameChanged = false;
            if (alertDisplayName != null && !alertDisplayName.equals(alertType.getDisplayName())) {
                originalAlertType.setDisplayName(alertDisplayName);
                alertDisplayNameChanged = true;
            }
            if (alertDesc != null && !alertDesc.equals(alertType.getDescription())) {
                originalAlertType.setDescription(alertDesc);
            }
            String oldPriority = originalAlertType.getPriority();
            originalAlertType.setPriority(AlertUtils.numberStringToPriorityName(alertPriorityNumberString));
            originalAlertType.setAutoDismiss(Boolean.parseBoolean(alertAutodismissStr));
            originalAlertType.setAutoAcknowledge(Boolean.parseBoolean(autoAcknowledge));
            if (alertMsgHeader != null && !alertMsgHeader.equals(alertType.getDefaultHeader())) {
                originalAlertType.setDefaultHeader(alertMsgHeader);
            }
            if (alertMsgBody != null && !alertMsgBody.equals(alertType.getDefaultBody())) {
                originalAlertType.setDefaultBody(alertMsgBody);
            }

            //update alert type if priority is not 'Informational' for work with tier 2 or tier 3
            if(!AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(AlertUtils.numberStringToPriorityName(alertPriorityNumberString))) {
                Env.getAlertingServ(request).updateAlertType(alertTypeName, originalAlertType);
            }

            if("true".equals(request.getParameter("recipientsChanged"))) {
                String destinationsJsonArr = request.getParameter("destinations");
                JSONArray jarr = new JSONArray(destinationsJsonArr);
                for(int i = 0; i < jarr.length(); i++) {
                    JSONObject jobj = jarr.getJSONObject(i);
                    String type = jobj.getString("type");
                    String recipient = jobj.getString("recipient");
                    int count = 3;
                    if(AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(AlertUtils.numberStringToPriorityName(alertPriorityNumberString)) && oldPriority.equals(AlertUtils.numberStringToPriorityName(alertPriorityNumberString))){
                        count = 1;
                    }
                    for(int j = 0; j < count; j++) {
                        String tierName = "TIER" + (j + 1);
                        String tierValue = jobj.getString(tierName.toLowerCase());
                        
                        if("user".equals(type)) {
                            Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, MessageType.SMTP, recipient);
                            Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, MessageType.SMS, recipient);
                            addUserActionDestination(alertTypeName, tierName, tierValue, recipient, request);
                        } else {
                            if("true".equals(tierValue)) {
                                Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, "snmp".equals(type) ? MessageType.SNMP : MessageType.AUDIO_VISUAL, recipient);
                            } else {
                                Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, "snmp".equals(type) ? MessageType.SNMP : MessageType.AUDIO_VISUAL, recipient);
                            }
                        }
                    }
                }
            }

            //update alert type if priority is 'Informational' after removing of tier 2 or tier 3
            if(AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(AlertUtils.numberStringToPriorityName(alertPriorityNumberString))) {
                Env.getAlertingServ(request).updateAlertType(alertTypeName, originalAlertType);
            }
            
            for(MessageTemplate messageTemplate : msgTemplates) {
                Env.getAlertingServ(request).setTemplate(alertTypeName, messageTemplate);
            }
            
            if (("true".equals(request.getParameter("ruleChanged")) || alertDisplayNameChanged) && ruleTypeTo != null) {
                String duration = request.getParameter("duration");
                String ruleName = alertTypeName;

                //remove old alert rules from DRE
                removeAllRulesByAlertName(alertTypeName, request);
                
                //create new rules in DAO
                String newRuleSource;
                if (!isSystem) {
                    List<Expression> exprs = RuleUtils.getExpressionsFromRequest(request);
                    Rule r = new Rule(ruleName, originalAlertType.getDisplayName(), exprs, duration);
                    newRuleSource = r.toString();
                } else {
                    newRuleSource = Rule.setDurationInSystemRuleSource(duration, ruleTypeDAO.getSource(ruleTypeTo.getID()));
                }
                
                ruleTypeDAO.setSource(ruleTypeTo.getID(), newRuleSource);			

                //add new rules to DRE
                LOG.info("Loading rule '" + ruleName + "' to DRE");
                Env.getDRE().addRulesFromDAO(Integer.valueOf(ruleTypeTo.getID().toString()));
            }
            
            result = ErrorMessages.SUCCESS;
        } catch (EnvException e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        } catch (NumberFormatException e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        } catch (DREException e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        } catch (NoSuchAlertTypeException e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        }
        
        return result;

    }
    
    private void addUserActionDestination(String alertTypeName, String tierName, String tierValue, String recipient, HttpServletRequest request) throws NoSuchAlertTypeException {
        if(AlertUtils.ALERT_USER_DESTINATION_SMTP.equals(tierValue) || AlertUtils.ALERT_USER_DESTINATION_BOTH.equals(tierValue)) {
            Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, MessageType.SMTP, recipient);
        }
        if(AlertUtils.ALERT_USER_DESTINATION_SMS.equals(tierValue) || AlertUtils.ALERT_USER_DESTINATION_BOTH.equals(tierValue)) {
            Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, MessageType.SMS, recipient);
        }
    }
    
    private void removeAllRulesByAlertName(String alertName, HttpServletRequest request){        
        RuleType ruleType = Env.getDRuleTypeDAO().getRuleTypeDTOByName(alertName);
        if (ruleType == null) {
            return;
        }
        String ruleSrc = ruleType.getSource();
        Rule rule = new Rule(ruleSrc);

        List<String> ruleNames = rule.getNames();
        LOG.info("Unload rules '" + ruleNames + "' from DRE");
        for(String subRuleName : ruleNames){
            try {
                Env.getDRE().removeRule(Constants.ALERT_RULES_PACKAGE, subRuleName);
            } catch (IllegalArgumentException e) {
                //previous load failed?
                LOG.error(e.getLocalizedMessage(), e);
            }
        }    
    }
}
