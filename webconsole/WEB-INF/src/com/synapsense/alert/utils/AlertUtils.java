package com.synapsense.alert.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.AlertMessageTemplateHelper;
import com.synapsense.alert.rule.Rule;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilterBuilder;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.dto.ValueTO;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.elementmodel.basic.WrapperModelElement;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.DataRetrievingUtils;
import com.synapsense.utilities.es.EnvXMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.lang.LangManager;

/**
 * Utility class for alerts.
 * 
 * @author ahorosh
 * 
 */
public class AlertUtils {
    private static final Log LOG = LogFactory.getLog(AlertUtils.class);
    /** request to get remote environment */
    protected HttpServletRequest req = null;

    /** Flag to get alerts for object or for its children */
    protected boolean isForChildren = true;

    public static final String ALERT_PRIORITY_NAME_CRITICAL = "CRITICAL";
    public static final String ALERT_PRIORITY_NAME_MAJOR = "MAJOR";
    public static final String ALERT_PRIORITY_NAME_MINOR = "MINOR";
    public static final String ALERT_PRIORITY_NAME_INFORMATIONAL = "INFORMATIONAL";

    public static final String ALERT_STATUS_OPENED = "OPENED";
    public static final String ALERT_STATUS_ACKNOWLEDGED = "ACKNOWLEDGED";
    public static final String ALERT_STATUS_RESOLVED = "RESOLVED";
    public static final String ALERT_STATUS_DISMISSED = "DISMISSED";

    public static final String ALERT_PRIORITY_TIER1_NAME = "TIER1";
    public static final String ALERT_PRIORITY_TIER2_NAME = "TIER2";
    public static final String ALERT_PRIORITY_TIER3_NAME = "TIER3";

    public static final String ALERT_USER_DESTINATION_NONE = "0";
    public static final String ALERT_USER_DESTINATION_SMTP = "1";
    public static final String ALERT_USER_DESTINATION_SMS = "2";
    public static final String ALERT_USER_DESTINATION_BOTH = "3";

    /**
     * If <code>forChildren</code> is false, class operations will be performed
     * under environment object. If <code>forChildren</code> is true, class
     * operations will be performed under environment object children.
     * 
     * @param context
     * @param forChildren
     */
    public AlertUtils(HttpServletRequest request, boolean forChildren) {
        req = request;
        isForChildren = forChildren;
    }

    // get remote services (environment, alertService, etc..)
    public void init() {

    }

    public static boolean isAlertExists(String alertName, HttpServletRequest request) {
        boolean result = false;

        Collection<AlertType> alertTypes = Env.getAlertingServ(request).getAlertTypes(new String[] { alertName });
        TO<?> ruleTypeTo = Env.getDRuleTypeDAO().getRuleTypeByName(alertName);
        if (ruleTypeTo != null || alertTypes.size() != 0) {
            result = true;
        }

        return result;
    }

    /**
     * Gets active alerts for environment object or its children (depend on
     * <code>forChildren</code> class object member)
     * 
     * @param objType
     *            - object type
     * @param objId
     *            - object id
     * @return
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public Collection<Alert> getActiveAlerts(String objId, boolean deep) throws NamingException,
            ObjectNotFoundException {
        TO<?> obj = TOFactory.getInstance().loadTO(objId);

        return getActiveAlerts(obj, deep);
    }

    /**
     * Gets all active alerts in system
     * 
     * @return
     */
    public Collection<Alert> getAllActiveAlerts() {

        AlertService alertService = Env.getAlertingServ(req);
        Collection<TO<?>> roots = Env.getEnv(req).getObjectsByType(EnvironmentTypes.TYPE_ROOT);
        Collection<Alert> alertCollection = new ArrayList<Alert>();

        if ((roots != null) && (!roots.isEmpty())) {
            // get all active alerts of objects
            for (TO<?> r : roots) {
                alertCollection.addAll(alertService.getActiveAlerts(r, true));
            }
        }

        // get all active alerts without objects (system alert)
        alertCollection.addAll(alertService.getActiveAlerts(TOFactory.EMPTY_TO, true));

        return alertCollection;
    }

    /**
     * Gets active alerts for environment object or its children (depend on
     * <code>forChildren</code> class object member)
     * 
     * @param obj
     *            - environment object
     * @return
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public Collection<Alert> getActiveAlerts(TO<?> obj, boolean deep) throws NamingException, ObjectNotFoundException {
        Collection<Alert> alerts = null;

        AlertService alertService = Env.getAlertingServ(req);

        alerts = alertService.getActiveAlerts(obj, deep);

        return alerts;
    }

    public Collection<Alert> getAlertsByStatus(String objId, String statuses, Date beginDate, Date endDate, String mode)
            throws NamingException, ObjectNotFoundException {
        Collection<Alert> alerts = new ArrayList<Alert>();
        AlertService alertService = Env.getAlertingServ(req);
        List<String> statusList = Arrays.asList(statuses.split(","));
        boolean allalerts = "allalerts".equals(mode);

        AlertFilterBuilder alertFilter = new AlertFilterBuilder().propagate(true);
        if (allalerts) {
            alertFilter.system();
            alertFilter.onHosts(Env.getEnv(req).getObjectsByType(EnvironmentTypes.TYPE_ROOT));
        } else {
            alertFilter.onHost(TOFactory.getInstance().loadTO(objId));
        }
        for (String status : statusList) {
            if (AlertStatus.OPENED.name().equals(status)) {
                alertFilter.opened();
            } else if (AlertStatus.ACKNOWLEDGED.name().equals(status)) {
                alertFilter.acknowledged();
            } else if (AlertStatus.DISMISSED.name().equals(status)) {
                alertFilter.dismissed();
            } else if (AlertStatus.RESOLVED.name().equals(status)) {
                alertFilter.resolved();
            }
        }

        if (beginDate != null && endDate != null) {
            alertFilter.between(beginDate, endDate);
        }

        alerts = alertService.getAlerts(alertFilter.build());

        return alerts;
    }

    /**
     * Gets active alerts for collection of environment objects
     * 
     * @param objects
     * @return
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public Collection<Alert> getActiveAlerts(Collection<TO<?>> objects, boolean deep) throws NamingException,
            ObjectNotFoundException {

        /*
         * If operation should be performed under children of each object in
         * collection
         */
        if (isForChildren) {
            throw new UnsupportedOperationException(
                    "Operation is not supported for children of object collection. Set 'forChildren' as false.");
        }

        AlertService alertService = Env.getAlertingServ(req);
        Collection<TO<?>> allObjects = new ArrayList<TO<?>>();

        allObjects.addAll(objects);

        return alertService.getActiveAlerts(allObjects, deep);
    }

    /**
     * Gets environment objects collection which should be checked on alert
     * existing. If <code>m_forChildren</code> is true - returns object
     * first-level children and all its deep children If
     * <code>m_forChildren</code> is false - returns object deep children and
     * object itself
     * 
     * @param obj
     * @return
     * @throws ObjectNotFoundException
     * @throws NamingException
     */
    protected Collection<TO<?>> getCheckingObjects(TO<?> obj) throws ObjectNotFoundException, NamingException {
        Collection<TO<?>> result = new ArrayList<TO<?>>();

        Environment env = Env.getEnv(req);

        if (isForChildren) {
            Collection<TO<?>> children = env.getChildren(obj);

            // get deep children
            // CommonFunctions.getDeepChildren(children, result, env);

            // add first-level children to result
            result.addAll(children);
        } else {
            // get deep children
            // CommonFunctions.getDeepChildren(obj, result, env);

            // add object to its children collection
            result.add(obj);
        }

        return result;
    }

    /**
     * Gets alerts history for environment object or its children (depend on
     * <code>forChildren</code> class object member)
     * 
     * @param objType
     * @param objId
     * @param beginDate
     * @param endDate
     * @return
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public Collection<Alert> getAlertsHistory(String objId, Date beginDate, Date endDate, boolean deep)
            throws NamingException, ObjectNotFoundException {
        Collection<Alert> alerts = null;
        AlertService alertService = Env.getAlertingServ(req);
        TO<?> obj = TOFactory.getInstance().loadTO(objId);

        alerts = alertService.getAlertsHistory(obj, beginDate, endDate, deep);

        return alerts;
    }

    /**
     * Get history of all alerts in system
     * 
     * @param beginDate
     * @param endDate
     * @return
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public Collection<Alert> getAllAlertsHistory(Date beginDate, Date endDate) throws NamingException,
            ObjectNotFoundException {
        AlertService alertService = Env.getAlertingServ(req);
        Collection<Alert> alerts = new ArrayList<Alert>();
        Collection<TO<?>> roots = Env.getEnv(req).getObjectsByType(EnvironmentTypes.TYPE_ROOT);

        if ((roots != null) && (!roots.isEmpty())) {
            alerts.addAll(alertService.getAlertsHistory(roots.iterator().next(), beginDate, endDate, true));
        }
        alerts.addAll(alertService.getAlertsHistory(TOFactory.EMPTY_TO, beginDate, endDate, true));

        return alerts;
    }

    /**
     * Checks if object has alert
     * 
     * @param object
     * @return
     */
    public boolean hasAlerts(TO<?> object, boolean deep) {
        AlertService alertService = Env.getAlertingServ(req);
        return alertService.hasAlerts(object, deep);
    }

    public ModelElement createObjectModelByAlerts(TO<?> objId, Collection<Alert> alerts, HttpServletRequest request)
            throws NamingException, ParserConfigurationException, ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        ModelElement objectsElement = new ModelElement("object");

        Integer permissionsIndependentAlertsNum = getNumAlertsIndependentlyOfPermissions(objId, request);
        objectsElement.addAttribute("permissionsIndependentAlertsNum", permissionsIndependentAlertsNum.toString());

        Environment env = Env.getEnv(request);

        String name;
        String loc = "";
        String type = "";
        if (objId != null) {
            name = env.getPropertyValue(objId, "name", String.class);
            type = objId.getTypeName();
            if (EnvironmentTypes.TYPE_ROOM.equals(objId.getTypeName())) {
                loc = env.getPropertyValue(objId, "descr", String.class);
            } else {
                try {
                    loc = env.getPropertyValue(objId, "location", String.class);
                } catch (PropertyNotFoundException e) {
                    // ingore, no location
                }
            }
            objectsElement.addAttribute("id", TOFactory.getInstance().saveTO(objId));
        } else {
            name = "text_system";
        }
        objectsElement.addAttribute("name", name);
        objectsElement.addAttribute("type", type);
        objectsElement.addAttribute("location", loc);

        if (alerts == null || alerts.isEmpty()) {
            return objectsElement;
        }

        // hashMap <objects, alert collection >
        HashMap<TO<?>, Collection<Alert>> objectAlerts = new HashMap<TO<?>, Collection<Alert>>();

        // array list of alerts with emptyTO
        Collection<Alert> alertsWithEmptyTO = new ArrayList<Alert>();

        for (Alert alert : alerts) {
            TO<?> currObject = alert.getHostTO();

            if (currObject.equals(TOFactory.EMPTY_TO)) {
                alertsWithEmptyTO.add(alert);
                continue;
            }

            Collection<Alert> a = objectAlerts.get(currObject);

            if (a == null) {
                a = new ArrayList<Alert>();
                objectAlerts.put(currObject, a);
            }

            if (currObject != null) {
                a.add(alert);
            }
        }

        // get properties for all objects
        ArrayList<TO<?>> arList = new ArrayList<TO<?>>();
        arList.addAll(objectAlerts.keySet());
        String[] propNames = { "name", "location" };
        Collection<CollectionTO> objectValuesArray = DataRetrievingUtils.getPropertyValue(arList, propNames, env);

        if (objectValuesArray == null) {
            return objectsElement;
        }

        // add object alerts
        for (CollectionTO p : objectValuesArray) {
            // get object
            TO<?> currentObject = p.getObjId();

            ModelElement newelem = new ModelElement(currentObject.getTypeName());
            objectsElement.addChild(newelem);

            newelem.addAttribute("id", TOFactory.getInstance().saveTO(currentObject));
            newelem.addAttribute("type", currentObject.getTypeName());

            // add object properties
            for (ValueTO pVal : p.getPropValues()) {
                String valueStr = EnvXMLHelper.convertPropertyValueToStr(pVal, LangManager.getInstance()
                        .getLocaleString(LangManager.getCurrentLocale(req), "text/n_a"));

                ModelElement node = new ModelElement(pVal.getPropertyName());
                node.addAttribute("v", valueStr);
                newelem.addChild(node);
            }

            // add alerts
            ModelElement alertsDoc = convertAlertsToModel(objectAlerts.get(currentObject));
            if (alertsDoc != null) {
                newelem.addChild(alertsDoc);
            }
        }

        // add alerts with emptyTO
        ModelElement newelem = new ModelElement("System");
        objectsElement.addChild(newelem);
        ModelElement node = new ModelElement("name");
        node.addAttribute("v", "text_system_alert");
        newelem.addChild(node);

        ModelElement alertsDoc = convertAlertsToModel(alertsWithEmptyTO);
        if (alertsDoc != null) {
            newelem.addChild(alertsDoc);
        }

        return objectsElement;
    }

    public ModelElement createModelByAllSystemAlerts(TO<?> objId, Collection<Alert> alerts, HttpServletRequest request)
            throws NamingException, ParserConfigurationException, ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        ModelElement objectsElement = new ModelElement("object");
        Integer permissionsIndependentAlertsNum = getNumAlertsIndependentlyOfPermissions(objId, request);
        objectsElement.addAttribute("permissionsIndependentAlertsNum", permissionsIndependentAlertsNum.toString());

        String name;
        if (objId != null) {
            name = Env.getEnv(request).getPropertyValue(objId, "name", String.class);
        } else {
            name = "text_system";
        }
        objectsElement.addAttribute("name", name);

        ModelElement systemElement = new ModelElement("system");
        objectsElement.addChild(systemElement);

        ModelElement nameElement = new ModelElement("name");
        nameElement.addAttribute("v", "System");
        systemElement.addChild(nameElement);

        // add alerts
        ModelElement alertsDoc = convertAlertsToModel(alerts);
        if (alertsDoc != null) {
            systemElement.addChild(alertsDoc);
        }

        return objectsElement;
    }

    public ModelElement getModelByAlert(Alert alert) throws ParserConfigurationException, ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException, NamingException {
        ModelElement objectsElement = new ModelElement("data");

        Collection<Alert> alerts = new ArrayList<Alert>();
        if (alert != null) {
            alerts.add(alert);
        }

        ModelElement alertsElem = convertAlertsToModel(alerts);
        if (alertsElem != null) {
            objectsElement.addChild(alertsElem);
        }

        return objectsElement;
    }

    public static ModelElement convertAlertsToModel(Collection<Alert> alerts) {
        ModelElement root = new ModelElement("alerts");
        if (alerts.isEmpty()) {
            return root;
        }
        root.addChild(new WrapperModelElement(alerts));
        return root;
    }

    /**
     * 
     * @param id
     * @param request
     * @return
     */
    public Alert getAlertById(Integer id, HttpServletRequest request) {
        Alert alert = null;
        AlertService alertService = Env.getAlertingServ(req);
        try {
            alert = alertService.getAlert(id);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return alert;
    }

    private int getNumAlertsIndependentlyOfPermissions(TO<?> objId, HttpServletRequest request)
            throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        int result = 0;
        Environment env = Env.getEnv(req);
        if (objId != null) {
            result = env.getPropertyValue(objId, "numAlerts", Integer.class);
        } else {
            Collection<TO<?>> roots = env.getObjectsByType(EnvironmentTypes.TYPE_ROOT);
            if (!roots.isEmpty()) {
                for (TO<?> root : roots)
                    result += env.getPropertyValue(root, "numAlerts", Integer.class);
            }
            int systemAlerts = Env.getAlertingServ(req).getActiveAlerts(TOFactory.EMPTY_TO, true).size();
            result += systemAlerts;
        }
        return result;
    }

    public static String numberStringToPriorityName(String numberString) {
        String result = AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL;
        if ("4".equals(numberString)) {
            result = AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL;
        } else if ("3".equals(numberString)) {
            result = AlertUtils.ALERT_PRIORITY_NAME_MINOR;
        } else if ("2".equals(numberString)) {
            result = AlertUtils.ALERT_PRIORITY_NAME_MAJOR;
        } else if ("1".equals(numberString)) {
            result = AlertUtils.ALERT_PRIORITY_NAME_CRITICAL;
        }
        return result;
    }

    public static int priorityNameToInt(String priority) {
        int result = 4;
        if (AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(priority)) {
            result = 4;
        } else if (AlertUtils.ALERT_PRIORITY_NAME_MINOR.equals(priority)) {
            result = 3;
        } else if (AlertUtils.ALERT_PRIORITY_NAME_MAJOR.equals(priority)) {
            result = 2;
        } else if (AlertUtils.ALERT_PRIORITY_NAME_CRITICAL.equals(priority)) {
            result = 1;
        }
        return result;
    }

    /**
     * Gets sub collection of alerts by object form some alerts collection
     * 
     * @param obj
     *            - environment object
     * @param alerts
     *            - alerts collection
     * @return
     */
    public static Collection<Alert> getObjectAlerts(TO<?> obj, Collection<Alert> alerts) {
        Collection<Alert> result = new ArrayList<Alert>();

        for (Alert alert : alerts) {
            TO<?> host = alert.getHostTO();
            if (host != null && host.equals(obj)) {
                result.add(alert);
            }
        }

        return result;
    }

    /**
     * Dismiss alert
     * 
     * @param alert
     * @param context
     * @throws NamingException
     */
    public static void dismissAlert(Alert alert, HttpServletRequest request) throws NamingException {
        AlertService alertService = Env.getAlertingServ(request);
        String acknowledgement = request.getParameter("acknowledgement");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Dismiss '" + alert.getName() + "' alert");
        }
        alertService.dismissAlert(alert, acknowledgement);
        return;
    }

    /**
     * Dismiss alert
     * 
     * @param alertId
     * @param context
     * @throws NamingException
     */
    public static void dismissAlert(Integer alertId, HttpServletRequest request) throws NamingException {
        AlertService alertService = Env.getAlertingServ(request);
        String acknowledgement = request.getParameter("acknowledgement");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Dismiss '" + alertId + "' alert");
        }
        alertService.dismissAlert(alertId, acknowledgement);
        return;
    }

    public static void acknowledgeAlert(Integer alertId, HttpServletRequest request) throws NamingException {
        AlertService alertService = Env.getAlertingServ(request);
        String acknowledgement = request.getParameter("acknowledgement");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Acknowledge '" + alertId + "' alert");
        }
        alertService.acknowledgeAlert(alertId, acknowledgement);
    }

    public static void resolveAlert(Integer alertId, HttpServletRequest request) throws NamingException {
        AlertService alertService = Env.getAlertingServ(request);
        String acknowledgement = request.getParameter("acknowledgement");
        if (LOG.isDebugEnabled()) {
            LOG.debug("Resolve '" + alertId + "' alert");
        }
        alertService.resolveAlert(alertId, acknowledgement);
        return;
    }

    /**
     * Dismiss all object alerts.
     * 
     * @param objType
     * @param objId
     * @param deep
     *            - if true, when all alerts of child objects will be dismissed
     *            too
     * @param context
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public static void dismissAllAlerts(String objId, boolean deep, HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {

        AlertService alertService = Env.getAlertingServ(request);
        String acknowledgement = request.getParameter("acknowledgement");
        TO<?> object = TOFactory.getInstance().loadTO(objId);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Dismiss alerts for '" + objId + "' object");
        }
        alertService.dismissAlerts(object, deep, acknowledgement);

        return;
    }

    public static void dismissAllAlerts(HttpServletRequest request) throws NamingException, ObjectNotFoundException {
        dismissAllAlertsInSystem(request);
        return;
    }

    /**
     * Dismisses all alerts in system
     * 
     * @param request
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public static void dismissAllAlertsInSystem(HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {
        AlertUtils util = new AlertUtils(request, false);
        Collection<Alert> alerts = util.getAllActiveAlerts();

        if (LOG.isDebugEnabled()) {
            LOG.debug("Dismiss all alerts in system");
        }
        for (Alert alert : alerts) {
            dismissAlert(alert, request);
        }

        return;
    }

    public static void deleteAllAlertsHistory(HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {
        AlertService alertService = Env.getAlertingServ(request);
        alertService.deleteAllAlerts();

        String userName = (String) request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        Env.getActivityLogServ(request).addRecord(
                new ActivityRecord(userName, ActivityLogConstans.MODULE_ALERT_MANAGEMENT,
                        ActivityLogConstans.ACTION_DELETE_ALL_ALERTS, LocalizationUtils
                                .getLocalizedString("activity_log_all_alerts_were_deleted")));

        return;
    }

    @SuppressWarnings("deprecation")
    public static void deleteAlertsHistoryOlderThan(HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {
        AlertService alertService = Env.getAlertingServ(request);
        alertService.deleteAlertsHistoryOlderThan(new Date(Long.parseLong(request.getParameter("startDate"))));
        return;
    }

    /**
     * Delete all object alerts
     * 
     * @param objType
     * @param objId
     * @param deep
     *            - if true, when all alerts of child objects will be deleted
     *            too
     * @param context
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    public static void deleteAllAlerts(String objId, boolean deep, HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {

        TO<?> object = TOFactory.getInstance().loadTO(objId);
        deleteAllAlerts(object, deep, request);
        return;
    }

    /**
     * Delete all object alerts
     * 
     * @param object
     * @param deep
     *            - if true, when all alerts of child objects will be deleted
     *            too
     * @param context
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    @SuppressWarnings("deprecation")
    public static void deleteAllAlerts(TO<?> object, boolean deep, HttpServletRequest request) throws NamingException,
            ObjectNotFoundException {

        AlertService alertService = Env.getAlertingServ(request);
        if (LOG.isDebugEnabled()) {
            LOG.debug("Delete alerts for '" + TOFactory.getInstance().saveTO(object) + "' object");
        }
        alertService.deleteAlerts(object, deep);
        return;
    }

    /**
     * 
     * @param request
     * @throws NamingException
     * @throws ObjectNotFoundException
     */
    @SuppressWarnings("deprecation")
    public static void deleteAllAlerts(HttpServletRequest request) throws NamingException, ObjectNotFoundException {
        AlertService alertService = Env.getAlertingServ(request);

        // delete all alerts for "ROOT"
        Collection<TO<?>> roots = Env.getEnv(request).getObjectsByType(EnvironmentTypes.TYPE_ROOT);
        LOG.debug("Delete alerts for roots");
        for (TO<?> root : roots) {
            alertService.deleteAlerts(root, true);
        }
        LOG.debug("Delete all system alerts");
        // delete all system alerts
        alertService.deleteAlerts(TOFactory.EMPTY_TO, true);

        // delete all console alert types and rules
        Collection<AlertType> alertTypes = alertService.getAllAlertTypes();

        LOG.debug("Delete all console alert types and rules");
        for (AlertType at : alertTypes) {
            String typeName = at.getName();

            deleteAlertTypeAndRules(request, typeName);
        }
    }

    /**
     * Currently delete Console alerts only
     * 
     * @param request
     * @param typeName
     */
    public static void deleteAlertTypeAndRules(HttpServletRequest request, String typeName) {
        AlertService alertService = Env.getAlertingServ(request);

        // if alert type is Console alert type
        if (typeName.contains(Constants.CONSOLE_ALERT_PREFIX)) {
            DRuleTypeService<Object> ruleTypeDAO = Env.getDRuleTypeDAO();
            DRuleTypeService<Object> serv = Env.getDRuleTypeDAO();
            TO<?> ruleTypeTo = serv.getRuleTypeByName(typeName);

            if (ruleTypeTo != null) {
                try {
                    String ruleSrc = ruleTypeDAO.getSource(ruleTypeTo.getID());

                    List<String> ruleNames = new Rule(ruleSrc).getNames();

                    for (String ruleName : ruleNames) {

                        if (LOG.isDebugEnabled()) {
                            LOG.debug("Unload rule '" + ruleName + "' from DRE");
                        }
                        try {
                            Env.getDRE().removeRule(Constants.ALERT_RULES_PACKAGE, ruleName);
                        } catch (IllegalArgumentException e) {
                            //previous load failed?
                            LOG.info(e.getLocalizedMessage(), e);
                        }
                    }
                    // delete rule type
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("Remove rule '" + typeName + "'");
                    }
                    serv.delete(ruleTypeTo.getID());
                } catch (ObjectNotFoundException e1) {
                    LOG.warn("Rule doesn't exist:  '" + typeName + "'");
                }
            }

            Set<MessageTemplate> messsageTemplates = null;
            Collection<AlertType> alTypes = alertService.getAlertTypes(new String[] { typeName });
            if (alTypes != null && !alTypes.isEmpty()) {
                messsageTemplates = alTypes.iterator().next().getMessageTemplates();
            }

            if (LOG.isDebugEnabled()) {
                LOG.debug("Remove alert '" + typeName + "'");
            }
            alertService.deleteAlertType(typeName);

            // delete custom templates
            if (messsageTemplates != null) {
                Environment env = Env.getEnv(request);
                AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
                for (MessageTemplate template : messsageTemplates) {
                    TO<?> templateId = template.getRef();

                    if (!helper.isTemplateUsedByAlertType(templateId) && helper.isMessageTemplateNameCustom(templateId)) {
                        try {
                            env.deleteObject(templateId); // if none alert types
                                                          // use this
                                                          // template, delete
                                                          // it
                        } catch (ObjectNotFoundException e) {
                            LOG.error(e.getLocalizedMessage(), e); // just skip
                                                                   // exception
                                                                   // and
                                                                   // continue
                                                                   // to delete
                                                                   // unused
                                                                   // templates
                        }
                    }
                }
            }
        }
    }
}
