package com.synapsense.alert;

import java.io.IOException;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.tasktracker.StatesEnum;
import com.synapsense.tasktracker.TaskData;
import com.synapsense.tasktracker.TaskDescr;
import com.synapsense.tasktracker.TaskID;
import com.synapsense.tasktracker.TaskState;
import com.synapsense.tasktracker.TaskStorage;
import com.synapsense.tasktracker.TaskUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;

public class SecureManageAlerts extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = 6346227251139141483L;
    private static final Log LOG = LogFactory.getLog(SecureManageAlerts.class);
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        String status = ErrorMessages.UNABLE_TO_COMPLETE;
        StatesEnum state = StatesEnum.ERROR;
         
        TaskID taskId = TaskUtils.getTaskIDByRequest(req);

        TaskState taskState = new TaskState(StatesEnum.IN_PROGRESS, "Execution in progress");
        TaskStorage.getInstance().addTask(taskId, new TaskData(taskState, new TaskDescr(req.getParameter("taskName"))), req); 
        
        try {
            performOperation(taskId, req);
                state = StatesEnum.DONE;
                status = ErrorMessages.SUCCESS;
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
            throw new ServletException("Object is removed", e);
        } catch (NamingException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (NumberFormatException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
         
        taskState.updateState(state, ErrorMessages.getText(status)); 
        //String str = "<result>" + TaskStorage.getInstance().getTaskData(taskId, req).getTaskState().packToStr() + "</result>";
        sendResponse(resp, "<result>" + status + "</result>");
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    protected void performOperation(TaskID taskId, HttpServletRequest request) throws NamingException, NumberFormatException, ObjectNotFoundException {    	
        
        String mode = request.getParameter("mode");
        mode = (mode == null)? "" : mode;                
        
        if("deleteAllAlertsHistory".equals(mode)){
            AlertUtils.deleteAllAlertsHistory(request);
        }else if("deleteAlertsHistoryOlderThan".equals(mode)) {
            AlertUtils.deleteAlertsHistoryOlderThan(request);
        }
    }

}
