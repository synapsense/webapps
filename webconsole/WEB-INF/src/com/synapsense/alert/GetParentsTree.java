package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvXMLHelper;

public class GetParentsTree extends BaseHttpServlet {
    private static final long serialVersionUID = -8177915340910562827L;
    private static final Log LOG = LogFactory.getLog(GetParentsTree.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        try {
            sendResponse(resp, getParentsDocument(req));
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private Document getParentsDocument(HttpServletRequest req) throws ParserConfigurationException {
        Document objectsDoc = XMLHelper.createNewXmlDocument("objects");
        Element objectDocRoot = objectsDoc.getDocumentElement();
        
        String[] objIds = null;
        String objIdStr = req.getParameter("id");
        String objIdsJSON = req.getParameter("ids");
        try {
            if (objIdStr != null && !objIdStr.isEmpty()) {
                objIds = new String[]{objIdStr};
            } else if (objIdsJSON != null && !objIdsJSON.isEmpty()) {
                JSONArray jArr = new JSONArray(objIdsJSON);
                objIds = new String[jArr.length()];
                for (int i=0;i<jArr.length();i++) {
                    objIds[i] = jArr.getJSONObject(i).getString("id");
                }
                
            }
            for (String id:objIds) {
                if(id != null && !id.isEmpty()) {
                    Element objectEl = objectsDoc.createElement("object");
                    TO<?> object = TOFactory.getInstance().loadTO(id);
                    Collection<TO<?>> parents = Env.getEnv(req).getParents(object);
                    EnvXMLHelper.writeIDs(object, objectEl);
                    getParents(req, parents, objectEl);
                    objectDocRoot.appendChild(objectEl);
                }
            }
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (PropertyNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (UnableToConvertPropertyException e) {
            LOG.error(e.getLocalizedMessage(), e);
        } catch (JSONException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return objectsDoc;
    }

    private void getParents(HttpServletRequest req, Collection<TO<?>> parent, Element objectEl) throws ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException {
        for(TO<?> obj : parent) {
            /*if(EnvironmentTypes.TYPE_DC.equals(obj.getTypeName()) || EnvironmentTypes.TYPE_ROOM.equals(obj.getTypeName()) || EnvironmentTypes.TYPE_ROOT.equals(obj.getTypeName()) || EnvironmentTypes.TYPE_NETWORK.equals(obj.getTypeName())) {
                continue;
            }*/
            Element newelem = objectEl.getOwnerDocument().createElement("parent");
            objectEl.appendChild((Node)newelem);
            EnvXMLHelper.writeIDs(obj, newelem);
            getParents(req, Env.getEnv(req).getParents(obj), newelem);
        }
    }
    
}
