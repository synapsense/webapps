package com.synapsense.alert.rule;

import java.util.List;

import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.FieldConstraintDescr;
import org.drools.lang.descr.GlobalDescr;
import org.drools.lang.descr.ImportDescr;
import org.drools.lang.descr.LiteralRestrictionDescr;
import org.drools.lang.descr.NotDescr;
import org.drools.lang.descr.PackageDescr;
import org.drools.lang.descr.PatternDescr;
import org.drools.lang.descr.RuleDescr;

import com.synapsense.utilities.es.EnvironmentTypes;

/**
 * TODO Fix comments
 * <code>DurationBasedPackageDescrGenerator</code> can be used when rule's
 * duration is bigger when 0. In this case an alert is generated if the rule is
 * truthful for a period of time. We can't just set duration attribute for
 * generated in the package rules to get delayed alert generation because of the
 * Drools engine behavior. If used in the rule fact's update interval is less
 * when the duration, then the rule will not be fired even if it is still true
 * after the fact update. See bug #3060 for details. To get desirable behavior a
 * set of three rules instead of one is generated. This set is represented by
 * the main rule itself, the so called "anti-rule" and the rule with duration,
 * which performs alert generation. The main rule in it's RHS creates a trigger
 * which activates the rule with duration. The "anti-rule" is also activated by
 * the trigger and the condition which is opposite to the main rule's one. This
 * rule is needed to cancel alert generation, if the main rule stops being
 * truthful.
 * 
 * @author stikhon
 * 
 */
public class DurationBasedPackageDescrGenerator implements
        PackageDescrGenerator {
    /**
     * The consequence pattern for trigger insertion. It has two place holders
     * which should be filled with the unique <b>trigger id</b> and the
     * <b>object references array</b>.
     */
    public static final String TRIGGER_INSERTION_CONSEQUENCE_PATTERN = "Trigger t = new Trigger(\"%1$s\");\n"
            + "%2$s"
            + "if(triggers.putIfAbsent(t.getCompositeId(), t) == null){ \n"
            + "insert(t);\n" 
            + "DRulesEngineService.sendAlert(\"%3$s\","
            + "\"%4$s\",%5$s,null,$obj%6$d.getNativeTO(), t, %7$s);\n" 
            + "}\n";
    /**
     * The trigger add object reference pattern. It has two place holders for
     * the <b>object's index in array</b> and the <b>object</b> to add.
     */
    public static final String TRIGGER_ADD_OBJECT_REFERENCE_PATTERN = "t.addObject(%1$d,%2$s);\n";
    /**
     * The trigger add sensor reference pattern. It has two place holders for
     * the <b>sensor's index in array</b> and the <b>sensor</b> to add.
     */
    public static final String TRIGGER_ADD_SENSOR_REFERENCE_PATTERN = "t.addSensor(%1$d,%2$s);\n";

    /**
     * The consequence pattern for trigger retraction. The string doesn't
     * actually have any place holders and named "pattern" for consistency.
     */
    public static final String TRIGGER_RETRACTION_CONSEQUENCE_PATTERN = "Trigger t = (Trigger)triggers.remove($t.getCompositeId());\n"
        + "if(t != null) retract(t);\n";

    private static final String SENSOR_PROPERTY_NAME = "lastValue";

    private static final String TRIGGER_TYPE = "Trigger";
    private static final String TRIGGER_IDENTIFIER = "$t";
    private static final String TRIGGER_FIELD_NAME = "ruleId";
    private static final String TRIGGER_EVALUATOR = "==";

    private PackageDescr packageDescr = new PackageDescr(
            PACKAGE_DESCRIPTION_NAME);

    private String duration;

    /**
     * Default constructor which sets up the PackageDescr header and the rule
     * duration.
     * 
     * @param duration
     *            The duration attribute of the rule.
     */
    public DurationBasedPackageDescrGenerator(final String duration) {
        this.duration = duration;
        packageDescr.addGlobal(new GlobalDescr(TRIGGER_GLOBAL_ID,
                TRIGGER_GLOBAL_TYPE));
        packageDescr.addImport(new ImportDescr(DOMAIN_OBJECTS_IMPORT));
        packageDescr.addImport(new ImportDescr(DRULES_SERVICE_IMPORT));
        packageDescr.addImport(new ImportDescr(TRIGGER_IMPORT));
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * com.synapsense.alert.rule.PackageDescrGenerator#generatePackageDescr(
     * java.lang.String, java.lang.String, java.util.List,
     * javax.servlet.http.HttpServletRequest, java.lang.String)
     */
    @Override
    public final PackageDescr generatePackageDescr(final String ruleName,
            final String alertName, final List<Expression> expressions) {
        // Split ruleExpressionsList on several lists.
        List<List<Expression>> ruleExpressionsList = RuleUtils
                .splitExpressions(expressions);
        int i = 0;
        int j = 0;
        // A rule needs to be created for each list of expressions.
        for (List<Expression> ruleExpressions : ruleExpressionsList) {
            int k = i;
            // rule name is used as trigger id
            String ruleId = ruleName + j++;
            RuleDescr mainRule = new RuleDescr(ruleId);
            RuleDescr antiRule = new RuleDescr(ANTI_RULE_NAME_PREFIX + ruleId);

            AndDescr lhs = new AndDescr();
            AndDescr antiRuleLhs = new AndDescr();
            StringBuilder alertMessage = new StringBuilder();
            StringBuilder mainRuleObjectsForTrigger = new StringBuilder();
            for (int exprIdx = 0; exprIdx < ruleExpressions.size(); exprIdx++) {
                // get suitable LHSDumper to parse expression and get AndDescr
                // for LHS
                Expression expression = ruleExpressions.get(exprIdx);
                LHSDumperFactory.LHSDumper lhsDumper = LHSDumperFactory.getLHS(
                        expression, i);
                AntiLHSDumperFactory.LHSDumper antiLhsDumper = AntiLHSDumperFactory
                        .getLHS(expression, i, exprIdx);
                AndDescr lhsDescrs = lhsDumper.feed();
                lhs.addOrMerge(lhsDescrs);
                AndDescr antiLhsDescrs = antiLhsDumper.feed();
                antiRuleLhs.addOrMerge(antiLhsDescrs);
                // Generate alert message
                String objectType = expression.getObjType();
                String propertyName = expression.getPropertyName();

                if (EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(objectType)
                        || LHSDumperTypesEnum.NODE.equals(lhsDumper.getType())) {
                    propertyName = SENSOR_PROPERTY_NAME;
                }
                String messagePattern = AlertMessagesEnum
                        .getAlertMessageByType(lhsDumper.getType());

                String dimension = expression.getDimension();
                if (dimension == null) {
                    dimension = "";
                } else if (!dimension.isEmpty()) {
                    dimension = "," + dimension;
                }
                
                alertMessage.append(String.format(messagePattern, objectType,
                        propertyName, RuleUtils.makeGetterName(propertyName), i, dimension));
                
                String objectReference = "$obj" + i;

                mainRuleObjectsForTrigger.append(String.format(
                        TRIGGER_ADD_OBJECT_REFERENCE_PATTERN, exprIdx,
                        objectReference));
                
                if (LHSDumperTypesEnum.NODE.equals(lhsDumper.getType())) {
                    String sensorReference = "$sens" + i;
                    mainRuleObjectsForTrigger.append(String.format(
                            TRIGGER_ADD_SENSOR_REFERENCE_PATTERN, exprIdx,
                            sensorReference));
                }
                i++;
            }

            mainRule.setLhs(lhs);

            mainRule.setConsequence(String.format(
                    TRIGGER_INSERTION_CONSEQUENCE_PATTERN, ruleId,
                    mainRuleObjectsForTrigger.toString(),  alertName, ruleName,
                            alertMessage.substring(1), k, duration));

            // generate LHS for anti-rule
            AndDescr antiLhs = new AndDescr();
            // first of all generate trigger pattern
            PatternDescr triggerPattern = new PatternDescr(TRIGGER_TYPE);
            triggerPattern.setIdentifier(TRIGGER_IDENTIFIER);
            FieldConstraintDescr triggerConstraint = new FieldConstraintDescr(
                    TRIGGER_FIELD_NAME);
            triggerConstraint.addRestriction(new LiteralRestrictionDescr(
                    TRIGGER_EVALUATOR, ruleId));
            triggerPattern.addConstraint(triggerConstraint);

            antiLhs.addDescr(triggerPattern);
            NotDescr reverseCondition = new NotDescr(antiRuleLhs);
            antiLhs.addOrMerge(reverseCondition);
            antiRule.setLhs(antiLhs);
            antiRule.setConsequence(String
                    .format(TRIGGER_RETRACTION_CONSEQUENCE_PATTERN));

            packageDescr.addRule(mainRule);
            packageDescr.addRule(antiRule);

        }
        return packageDescr;
    }
}
