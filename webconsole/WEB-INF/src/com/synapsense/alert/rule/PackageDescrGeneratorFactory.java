package com.synapsense.alert.rule;

/**
 * <code>PackageDescrGeneratorFactory</code> is responsible for
 * <code>PackageDescrGenerator</code> instantiation basing on the provided rule
 * duration.
 * 
 * @author stikhon
 */
public final class PackageDescrGeneratorFactory {

    private static final PackageDescrGeneratorFactory INSTANCE = new PackageDescrGeneratorFactory();

    private PackageDescrGeneratorFactory() {
    }

    /**
     * Gets the single instance of PackageDescrGeneratorFactory.
     * 
     * @return Single instance of PackageDescrGeneratorFactory.
     */
    public static PackageDescrGeneratorFactory getInstance() {
        return PackageDescrGeneratorFactory.INSTANCE;
    }

    /**
     * Creates <code>SimplePackageDescrGenerator</code> by default (duration is
     * 0).
     * 
     * @return The created <code>PackageDescrGenerator</code>.
     */
    public PackageDescrGenerator newPackageDescrGenerator() {
        return newPackageDescrGenerator("0");
    }

    /**
     * Creates <code>PackageDescrGenerator</code> which is suitable for the
     * provided duration.
     * 
     * @param duration
     *            The duration attribute of the rule.
     * 
     * @return The created <code>PackageDescrGenerator</code>.
     */
    public PackageDescrGenerator newPackageDescrGenerator(final String duration) {
        long durationNum;
        try {
            durationNum = Long.parseLong(duration);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                    "The duration parameter has incorrect value: " + duration
                            + ". Number is expected", e);
        }
        if (durationNum > 0) {
            return new DurationBasedPackageDescrGenerator(duration);
        }
        return new BasicPackageDescrGenerator();
    }
}
