package com.synapsense.alert.rule;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.Env;
import com.synapsense.dataAccess.UnitConverter;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.Environment;
import com.synapsense.util.ConvertingEnvironment;
import com.synapsense.utilities.es.EnvTypeUtil;
import com.synapsense.utilities.es.EnvironmentTypes;

public class RuleUtils {

    public static List<Expression> getExpressionsFromRequest(
            HttpServletRequest request) throws PropertyNotFoundException, ObjectNotFoundException {
        List<Expression> expressions = new ArrayList<Expression>();
        String expressionsStr = request.getParameter("expr");
        String expressionArray[] = expressionsStr.split(";");
        Environment env = Env.getEnv(request);

        for (String expression : expressionArray) {
            String[] tokens = expression.split(",");
            String[] objects = expression.substring(
                    expression.lastIndexOf('[') + 1, expression.length() - 1)
                    .split(",");
            String condition = tokens[0];
            String objType = tokens[1];
            String properyName = tokens[2];
            String operation = tokens[3];
            String threshold = tokens[4];
            boolean sensorLink = false;
            if (!EnvironmentTypes.TYPE_NODE.equals(objType) &&
                !EnvironmentTypes.TYPE_SENSOR.equals(objType)) {
                sensorLink = (TO.class.equals(EnvTypeUtil.getPropertyClass(properyName, objType, env)));
            }
            Expression e = new Expression(condition, objType, properyName, operation,
                    threshold, objects, sensorLink);
            RuleUtils.preprocessExpression(e, Env.getEnv(request).getTargetSystem());
            expressions.add(e);
        }

        return expressions;
    }

    /**
     * Splits expressions on lists, starting a new list when "or" expression is
     * processed. Therefore each list in the result contains only "and"
     * expressions.
     * 
     * @param expressions
     *            The list of expressions to split.
     * @return List of split on separate lists expressions.
     */

    public static List<List<Expression>> splitExpressions(
            List<Expression> expressions) {

        int i = 0;
        List<Expression> newList = new ArrayList<Expression>();
        List<List<Expression>> result = new ArrayList<List<Expression>>();

        for (Expression expression : expressions) {
            if ("or".equals(expression.getCondition())) {
                if (!newList.isEmpty()) {
                    newList.get(0).setCondition("and");
                    result.add(i++, newList);
                    newList = new ArrayList<Expression>();
                }
            }
            newList.add(expression);
        }

        if (!newList.isEmpty()) {
            newList.get(0).setCondition("and");
        }
        result.add(i++, newList);
        return result;
    }

    /**
     * Creates getter-style method name based on the provided property name.
     * 
     * @param propName
     *            The name of the property for which method name is generated.
     * @return The generated method name.
     */
    public static String makeGetterName(String propName) {
        return "get" + propName.substring(0, 1).toUpperCase()
                + propName.substring(1);
    }
    
    public static void preprocessExpression(Expression e, String currentUnitSystem) {
        String dimension = TypeUtil.getInstance().getPropertyDataclass(
                e.getObjType(), e.getPropertyName());
        
        //TODO: get it from UI?
        e.setDimension(dimension);
        
        //In ui for sensors we show dataclass instead of property
        if (e.getObjType().equalsIgnoreCase(EnvironmentTypes.TYPE_SENSOR)) {
            e.setPropertyName("lastValue");
        }
        
        if (!Env.DEFAULT_UNIT_SYSTEM.equals(currentUnitSystem) && currentUnitSystem != null) {
            // convert threshold to Server unit system
            String threshold = e.getThreshold();
            
            threshold = UnitConverter.convertValue(threshold, dimension, currentUnitSystem, Env.DEFAULT_UNIT_SYSTEM);
            e.setThreshold(threshold);
        }
        
    }
    
    public static void postprocessExpression(Expression expression, ConvertingEnvironment env) {
        //In ui for sensors we show dataclass instead of property
        if(EnvironmentTypes.TYPE_SENSOR.equals(expression.getObjType())) {
            TO<?> to = TOFactory.getInstance().loadTO(expression.getObjects()[0]);
            try {
                expression.setPropertyName(env.getPropertyValue(to, "dataclass", Integer.class).toString());
            } catch (PropertyNotFoundException e) {
                //
            } catch (UnableToConvertPropertyException e) {
                //
            } catch (ObjectNotFoundException e) {
                //
            }
        }
        
        String currentUnitSystem = env.getTargetSystem();
        
        String dimension = TypeUtil.getInstance().getPropertyDataclass(expression.getObjType(), expression.getPropertyName());
        //TODO: actually we don't need it, can get it from systems.xml in UI
        expression.setDimension(dimension);
        if (!Env.DEFAULT_UNIT_SYSTEM.equals(currentUnitSystem) && currentUnitSystem != null) {
            // convert threshold to Server unit system
            expression.setThreshold(UnitConverter.convertValue(expression.getThreshold(), dimension, Env.DEFAULT_UNIT_SYSTEM, currentUnitSystem));
        }
    }
}
