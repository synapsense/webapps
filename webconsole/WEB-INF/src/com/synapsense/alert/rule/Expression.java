package com.synapsense.alert.rule;

import java.util.Arrays;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.utilities.StringUtils;
import com.synapsense.utilities.XMLHelper;

public class Expression {
    
    /**
     * "and" or "or" 
     */
    private String condition;

    /**
     * Type of the environment object
     */
    private String objType;
    
    /**
     * For NODE type it stores dataclass of child sensors, 
     */
    private String propertyName;

    /**
     * ">", "<", ">=", "<=", "==" or "!=" 
     */
    private String operation;
    
    /**
     * Alert threshold
     */
    private String threshold;
    
    /**
     * Alert threshold dimension
     */
    private String dimension;
    
    /**
     * Array of object ids associated with alert
     */
    private String[] objects;
    
    /**
     * true, if property type is TO.class
     */
    private boolean sensorLink;

    public Expression() {
        super();
    }

    public Expression(String condition, String objType, String properyName,
            String operation, String threshold, String[] objects, boolean sensorLink) {
        this.condition = condition;
        this.objType = objType;
        this.propertyName = properyName;
        this.operation = operation;
        this.threshold = String.valueOf(Double.parseDouble(threshold));
        this.objects = objects;
        this.sensorLink = sensorLink;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getObjType() {
        return objType;
    }

    public void setObjType(String objType) {
        this.objType = objType;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String properyName) {
        this.propertyName = properyName;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = String.valueOf(Double.parseDouble(threshold));
    }

    public String getDimension() {
        return dimension;
    }

    public void setDimension(String dimension) {
        this.dimension = dimension;
    }

    public String[] getObjects() {
        return objects;
    }

    public void setObjects(String[] objects) {
        this.objects = objects;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public boolean isSensorLink() {
        return sensorLink;
    }

    public void setSensorLink(boolean sensorLink) {
        this.sensorLink = sensorLink;
    }
    
    public void toNode (Document doc, Node node) {
        Node conditionNode = XMLHelper.createXmlNode(doc,
                "condition", new HashMap<String, String>());
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "c", condition));
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "type", objType));
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "prop", propertyName));
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "op", operation));
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "threshold", threshold));
        String idsStr = StringUtils.join(Arrays.asList(objects), ","); 
        conditionNode.appendChild(XMLHelper.createXmlTextNode(doc, "ids", idsStr));
        node.appendChild(conditionNode);
    }

}
