package com.synapsense.alert.rule;

import java.util.List;

import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.ImportDescr;
import org.drools.lang.descr.PackageDescr;
import org.drools.lang.descr.RuleDescr;

import com.synapsense.utilities.es.EnvironmentTypes;

/**
 * The <code>BasicPackageDescrGenerator</code> can be used when duration
 * attribute of a rule is 0. In this case no extra rules are generated.
 * 
 * @author stikhon
 * 
 */
public class BasicPackageDescrGenerator implements PackageDescrGenerator {

    private PackageDescr packageDescr = new PackageDescr(
            PACKAGE_DESCRIPTION_NAME);

    /**
     * Default constructor which sets up the PackageDescr header.
     */
    public BasicPackageDescrGenerator() {
        packageDescr.addImport(new ImportDescr(DOMAIN_OBJECTS_IMPORT));
        packageDescr.addImport(new ImportDescr(DRULES_SERVICE_IMPORT));
    }

    /*
     * (non-Javadoc)
     * 
     * @see com.synapsense.alert.rule.PackageDescriptionGenerator#
     * generatePackageDescription(java.lang.String, java.lang.String,
     * java.util.List, javax.servlet.http.HttpServletRequest, java.lang.String)
     */
    @Override
    public final PackageDescr generatePackageDescr(final String ruleName,
            final String alertName, final List<Expression> expressions) {
        // Split ruleExpressionsList on several lists.
        List<List<Expression>> ruleExpressionsList = RuleUtils
                .splitExpressions(expressions);
        int i = 0;
        int j = 0;
        // A rule needs to be created for each list of expressions.
        for (List<Expression> ruleExpressions : ruleExpressionsList) {
            int k = i;

            RuleDescr ruleDesc = new RuleDescr(ruleName + (j++));
            AndDescr lhs = new AndDescr();
            StringBuilder alertMessage = new StringBuilder();
            for (Expression expression : ruleExpressions) {
                // get suitable LHSDumper, to parse expression and get AndDescr
                // for LHS
                LHSDumperFactory.LHSDumper lhsDumper = LHSDumperFactory.getLHS(
                        expression, i);
                AndDescr lhsDescrs = lhsDumper.feed();
                lhs.addOrMerge(lhsDescrs);

                String objectType = expression.getObjType();
                String propertyName = expression.getPropertyName();
                if (EnvironmentTypes.TYPE_SENSOR.equalsIgnoreCase(objectType)) {
                    propertyName = "lastValue";
                }
                String messagePattern = AlertMessagesEnum
                        .getAlertMessageByType(lhsDumper.getType());

                String dimension = expression.getDimension();
                if (dimension == null) {
                    dimension = "";
                } else if (!dimension.isEmpty()) {
                    dimension = "," + dimension;
                }
                
                //TODO: add comma
                alertMessage.append(String.format(messagePattern, objectType,
                        propertyName, RuleUtils.makeGetterName(propertyName), i, dimension));

                i++;
            }

            ruleDesc.setLhs(lhs);

            // Generate the consequence part of the rule (i.e. RHS). The first
            // symbol in the message is "+" and it needs to be left off.
            String consequence = String.format(
                    ALERT_GENERATION_CONSEQUENCE_PATTERN, alertName, ruleName,
                    alertMessage.substring(1), k);
            ruleDesc.setConsequence(consequence);

            packageDescr.addRule(ruleDesc);

        }
        return packageDescr;
    }

}
