package com.synapsense.alert.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.BaseDescr;
import org.drools.lang.descr.EvalDescr;
import org.drools.lang.descr.FieldBindingDescr;
import org.drools.lang.descr.FieldConstraintDescr;
import org.drools.lang.descr.LiteralRestrictionDescr;
import org.drools.lang.descr.OrDescr;
import org.drools.lang.descr.PatternDescr;
import org.drools.lang.descr.RestrictionConnectiveDescr;
import org.drools.lang.descr.RestrictionDescr;

import com.synapsense.utilities.es.EnvironmentTypes;

public class LHSParserFactory {
    private BaseList<Object> _arrDescr;
    private boolean isLegacyRule;
    
    public LHSParserFactory(AndDescr lhs, boolean isXml) {
        this.isLegacyRule = isXml;
        this._arrDescr = preParse(lhs);
    }
    
    public LHSParser getLHSParser () 
        throws LHSParserException {
        return parseArr(_arrDescr);
    }
    @SuppressWarnings("unchecked")
    private LHSParser parseArr(BaseList<Object> arr) 
        throws LHSParserException {
        //create new copy of the list to avoid concurrent modification
        BaseList<Object> arr1 = new BaseList<Object>(arr.getMyType());
        arr1.addAll(arr);
        for (Object obj:arr1) {
            if (obj instanceof BaseList) {
                BaseList<Object> buffArr = (BaseList<Object>)obj;
                if (buffArr.size() != 0) {
                    LHSParser p = parseArr(buffArr);
                    if (p != null) {
                        return p;
                    } else {
                        arr.remove(0);
                        arr.resetParent();
                        continue;
                    }
                } else {
                    arr.remove(0);
                    continue;
                }
            } else if (obj instanceof BaseDescr) {
                ListType conditionType;
                String condition;
                if ((conditionType = arr.getParentType()).equals(ListType.DUMMY)) {
                    condition = arr.getMyType().toString().toLowerCase();
                } else {
                    condition = conditionType.toString().toLowerCase();
                    arr.resetParent();
                }
                Object base = arr.remove(0);
                if (!(base instanceof PatternDescr)) {
                    throw new LHSParserException();
                }
                PatternDescr pattDescr = (PatternDescr)base;
                if (pattDescr.getObjectType().equalsIgnoreCase(EnvironmentTypes.TYPE_NODE) && arr.size() != 0
                    && arr.get(0) instanceof PatternDescr  && ((PatternDescr)arr.get(0)).getObjectType().equalsIgnoreCase(EnvironmentTypes.TYPE_SENSOR)){ //if arr.size is 0, it means that expression is for NODE property (not for SENSOR) i.e. we should process NODE as usual object and its property
                    //remove WSNSENSOR pattern
                    base = arr.remove(0);
                    if (!(base instanceof PatternDescr)) {
                        throw new LHSParserException();
                    }
                    PatternDescr sensorPattern = (PatternDescr)(base);
                    if (arr.size() != 0 && (arr.get(0) instanceof EvalDescr)) {
                        arr.remove(0); //it can be present or absent in different releases. For example, in the Mars we are removing "isParentOf..." evaluation.
                    }
                    return new LHSParser_Node(condition,pattDescr,sensorPattern);
                } else if (arr.size() != 0 && (arr.get(0) instanceof EvalDescr)) {
                    base = arr.remove(0);
                    if (!(base instanceof EvalDescr)) {
                        throw new LHSParserException();
                    }
                    EvalDescr evalDescr = (EvalDescr)(base);
                    return new LHSParser_SensorLink(condition,pattDescr,evalDescr);
                } else {
                    return new LHSParser_Object(condition,pattDescr);
                }
            } else {
                return null;
            }
        }
        return null;
    }
    @SuppressWarnings("unchecked")
    private BaseList<Object> preParse(AndDescr andDescr) {
        BaseList<Object> result = new BaseList<Object>(ListType.AND);
        List<BaseDescr> descrList = (List<BaseDescr>)andDescr.getDescrs();
        for (BaseDescr base:descrList) {
            if (base instanceof OrDescr) {
                OrDescr or = (OrDescr)base;
                BaseList<Object> list = preParse(or);
                result.add(list);
            } else if (base instanceof AndDescr) {
                AndDescr and = (AndDescr)base;
                BaseList<Object> list = preParse(and);
                if (isLegacyRule) {
                    result.add(list);
                } else {
                    //workaround for dlr parse issues with "&&"
                    result.addAll(list);
                }
            } else {
                result.add(base);
            }
        }
        return result;
    }
    
    @SuppressWarnings("unchecked")
    private BaseList<Object> preParse(OrDescr orDescr) {
        BaseList<Object> result = new BaseList<Object>(ListType.OR);
        List<BaseDescr> descrList = (List<BaseDescr>)orDescr.getDescrs();
        for (BaseDescr base:descrList) {
            if (base instanceof OrDescr) {
                OrDescr or = (OrDescr)base;
                BaseList<Object> list = preParse(or);
                result.add(list);
            } else if (base instanceof AndDescr) {
                AndDescr and = (AndDescr)base;
                BaseList<Object> list = preParse(and);
                if (isLegacyRule) {
                    result.add(list);
                } else {
                    //workaround for dlr parse issues with "&&"
                    result.addAll(list);
                }
            } else {
                result.add(base);
            }
        }
        return result;
    }
    
    public enum ListType{
        OR,
        AND,
        DUMMY
    }
    
    public class BaseList<E> extends ArrayList<E> {
        /**
         * 
         */
        private static final long serialVersionUID = 7707714676592394343L;
        protected ListType parent = ListType.DUMMY;
        protected ListType iam = ListType.DUMMY;
        
        public BaseList(ListType type) {
            super();
            iam = type;
        }

        @Override
        public boolean add(E e) {
            if (e instanceof BaseList<?>) {
                ((BaseList<?>)e).parent = iam;
            }
            return super.add(e);
        }
        public ListType getParentType() {
            return parent;
        }
        public ListType getMyType() {
            return iam;
        }
        public void resetParent() {
            parent = ListType.DUMMY;
        }
    }
    
    public abstract class LHSParser {
        protected Expression expr = new Expression();
//        protected String expression = "";
//        protected String condition = "";
//        protected String objType = "";
//        protected String propName = "";
//        protected String evaluator = "";
//        protected String threshold = "";
//        protected ArrayList<String> ids = new ArrayList<String>();
//        public String parse () {return expression;};
//        public String getCondition() {return condition;}
//        public String getObjType() {return objType;}
//        public String getPropName() {return propName;}
//        public String getEvaluator() {return evaluator;}
//        public String getThreshold() {return threshold;}
//        public ArrayList<String> getIds() {return ids;}
        
        public Expression getExpression() {
            return expr;
        }
    }
    private class LHSParser_SensorLink extends LHSParser {
        @SuppressWarnings("unchecked")
        public LHSParser_SensorLink (String condition, PatternDescr pattDescr, EvalDescr evalDescr) {
            expr.setCondition(condition);
            expr.setObjType(pattDescr.getObjectType());
            String content = (String)evalDescr.getContent();
            Pattern exprPattern = Pattern.compile("(get\\w+[^\\(]).+(>=|>|==|!=|<=|<).*?(-?\\d+\\.?\\d*+)");
            Matcher exprMatcher = exprPattern.matcher(content);
            exprMatcher.find();
            expr.setPropertyName(this.getPropName(exprMatcher.group(1)));
            expr.setOperation(exprMatcher.group(2));
            expr.setThreshold(exprMatcher.group(3));
            expr.setSensorLink(true);
            AndDescr andDescr = (AndDescr)pattDescr.getConstraint();
            FieldConstraintDescr fieldConstrDescr = (FieldConstraintDescr)andDescr.getDescrs().get(0);
            RestrictionConnectiveDescr restrConnDescr1 = fieldConstrDescr.getRestriction();
            List<LiteralRestrictionDescr> list = null;
            RestrictionDescr restrDescr2 = (RestrictionDescr)restrConnDescr1.getRestrictions().get(0);
            if (restrDescr2 instanceof RestrictionConnectiveDescr) {
                list = (List<LiteralRestrictionDescr>)((RestrictionConnectiveDescr)restrDescr2).getRestrictions();
            } else if (restrDescr2 instanceof LiteralRestrictionDescr) {
                list = (List<LiteralRestrictionDescr>)restrConnDescr1.getRestrictions();
            }

            List<String> ids = new ArrayList<String>();
            for (LiteralRestrictionDescr liter: list) {
                String id = liter.getText();
                ids.add(id);
            }
            String[] idsArr = new String[ids.size()];
            ids.toArray(idsArr);
            expr.setObjects(idsArr);
        }

        private String getPropName(String funcName) {
            String buff = funcName.substring(3,funcName.length());
            return buff.substring(0,1).toLowerCase() + buff.substring(1, buff.length());
        }
    }

    private class LHSParser_Node extends LHSParser {
        @SuppressWarnings("unchecked")
        public LHSParser_Node (String condition, PatternDescr pattDescr1, PatternDescr pattDescr2) {
            expr.setCondition(condition);
            expr.setObjType(pattDescr1.getObjectType());
            AndDescr andConstr = (AndDescr)pattDescr2.getConstraint();
            List<BaseDescr> descrList = andConstr.getDescrs();
            BaseDescr bd = descrList.get(2);
            FieldConstraintDescr field4;
            //in previous release dataclass was 3rd constraint, 2nd was eval
            //now it is 2nd
            if (bd instanceof FieldConstraintDescr) {
                field4 =  (FieldConstraintDescr)bd;
            } else {
                field4 = (FieldConstraintDescr)descrList.get(3);
            }
            RestrictionConnectiveDescr conn1 = field4.getRestriction();
            LiteralRestrictionDescr literProp = (LiteralRestrictionDescr)conn1.getRestrictions().get(0);
            expr.setPropertyName(literProp.getText());
            FieldConstraintDescr field2 = (FieldConstraintDescr)descrList.get(1);
            conn1 = field2.getRestriction();
            literProp = (LiteralRestrictionDescr)conn1.getRestrictions().get(0);
            expr.setOperation(literProp.getEvaluator());
            expr.setThreshold(literProp.getText());
            
            AndDescr andForIds = (AndDescr)pattDescr1.getConstraint();
            FieldConstraintDescr field = (FieldConstraintDescr)andForIds.getDescrs().get(0);
            RestrictionConnectiveDescr restrConnDescr1 = field.getRestriction();
            List<LiteralRestrictionDescr> idsList = null;
            RestrictionDescr restrDescr2 = (RestrictionDescr)restrConnDescr1.getRestrictions().get(0);
            if (restrDescr2 instanceof RestrictionConnectiveDescr) {
                idsList = (List<LiteralRestrictionDescr>)((RestrictionConnectiveDescr)restrDescr2).getRestrictions();
            } else if (restrDescr2 instanceof LiteralRestrictionDescr) {
                idsList = (List<LiteralRestrictionDescr>)restrConnDescr1.getRestrictions();
            }

            List<String> ids = new ArrayList<String>();
            for (LiteralRestrictionDescr liter: idsList) {
                String id = liter.getText();
                ids.add(id);
            }
            String[] idsArr = new String[ids.size()];
            ids.toArray(idsArr);
            expr.setObjects(idsArr);
        }
    }
    private class LHSParser_Object extends LHSParser {
        @SuppressWarnings("unchecked")
        public LHSParser_Object (String condition, PatternDescr pattDescr)
            throws LHSParserException {
            expr.setCondition(condition);
            expr.setObjType(pattDescr.getObjectType());
            AndDescr andConstr = (AndDescr)pattDescr.getConstraint();
            FieldConstraintDescr field = (FieldConstraintDescr)andConstr.getDescrs().get(1);
            boolean isHomerRule = false;
            if (!"serializedTO".equals(field.getFieldName())) {//for homer rules
                isHomerRule = true;
                field = (FieldConstraintDescr)andConstr.getDescrs().get(2);
            }

            RestrictionConnectiveDescr restrConnDescr1 = field.getRestriction();
            List<LiteralRestrictionDescr> idsList = null;
            RestrictionDescr restrDescr2 = (RestrictionDescr)restrConnDescr1.getRestrictions().get(0);
            if (restrDescr2 instanceof RestrictionConnectiveDescr) {
                idsList = (List<LiteralRestrictionDescr>)((RestrictionConnectiveDescr)restrDescr2).getRestrictions();
            } else if (restrDescr2 instanceof LiteralRestrictionDescr) {
                idsList = (List<LiteralRestrictionDescr>)restrConnDescr1.getRestrictions();
            }
            
            String propName = ((FieldBindingDescr)andConstr.getDescrs().get(0)).getFieldName();
            //hack, alway decapitalize first letter
            propName = propName.substring(0, 1).toLowerCase() + propName.substring(1);
            expr.setPropertyName(propName);

            LiteralRestrictionDescr literResctForThres;
            if (isHomerRule) {//for homer rules
                literResctForThres = (LiteralRestrictionDescr)((FieldConstraintDescr)(andConstr.getDescrs().get(1))).getRestriction().getRestrictions().get(0);
            } else {
                literResctForThres = (LiteralRestrictionDescr)((FieldConstraintDescr)(andConstr.getDescrs().get(2))).getRestriction().getRestrictions().get(0);
            }
            expr.setOperation(literResctForThres.getEvaluator());
            expr.setThreshold(literResctForThres.getText());

            List<String> ids = new ArrayList<String>();
            for (LiteralRestrictionDescr liter: idsList) {
                String id = liter.getText();
                ids.add(id);
            }
            String[] idsArr = new String[ids.size()];
            ids.toArray(idsArr);
            expr.setObjects(idsArr);
        }
    }
    public class LHSParserException extends Exception {
        private static final long serialVersionUID = 1L;
    }
}