package com.synapsense.alert.rule;

import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.EvalDescr;
import org.drools.lang.descr.FieldBindingDescr;
import org.drools.lang.descr.FieldConstraintDescr;
import org.drools.lang.descr.LiteralRestrictionDescr;
import org.drools.lang.descr.PatternDescr;
import org.drools.lang.descr.RestrictionConnectiveDescr;

import com.synapsense.utilities.es.EnvironmentTypes;

public class LHSDumperFactory {
    private LHSDumperFactory() {
    }

    public static LHSDumper getLHS(Expression expression,
            int __j) {
        LHSDumperFactory fac = new LHSDumperFactory();

        boolean hasDataClass = true;
        try {
            Integer.parseInt(expression.getPropertyName());
        } catch (NumberFormatException e) {
            hasDataClass = false;
        }

        if (expression.getObjType().equals(EnvironmentTypes.TYPE_NODE)
                && hasDataClass) {
            return fac.new LHSDumper_Node(expression, __j);
        } else if (expression.isSensorLink()) {
            return fac.new LHSDumper_SensorLink(expression, __j);
        } else {
            return fac.new LHSDumper_Object(expression, __j);
        }
    }

    public abstract class LHSDumper {
        protected int j;
        protected Expression expression;

        protected LHSDumper(Expression expression,
                int __j) {
            this.expression = expression;
            j = __j;
        }

        public abstract AndDescr feed();

        public abstract LHSDumperTypesEnum getType();
    }

    private class LHSDumper_SensorLink extends LHSDumper {
        protected LHSDumper_SensorLink(Expression expression,
                int __j) {
            super(expression, __j);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.SENSOR_LINK;
        }

        public AndDescr feed() {
            String cond = expression.getCondition();
            String objType = expression.getObjType();
            String dataClass = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            String propName = dataClass;

            AndDescr base = new AndDescr();

            EvalDescr labelDescr = new EvalDescr();
            if ("and".equals(cond)) {
                labelDescr.setContent(new String("\"and\".equals(\"and\")"));
            } else {
                labelDescr.setContent(new String("\"or\".equals(\"or\")"));
            }
            // base.addDescr(labelDescr);

            PatternDescr patternDescr = new PatternDescr(objType, "$obj" + j);
            FieldConstraintDescr fieldConstr1 = new FieldConstraintDescr(
                    "serializedTO");
            RestrictionConnectiveDescr entryRestriction1 = new RestrictionConnectiveDescr(
                    1);
            String[] arrObjIds = expression.getObjects();
            for (String id : arrObjIds) {
                LiteralRestrictionDescr litRestr = new LiteralRestrictionDescr(
                        "==", id, LiteralRestrictionDescr.TYPE_STRING);
                entryRestriction1.addRestriction(litRestr);
            }
            fieldConstr1.addRestriction(entryRestriction1);
            patternDescr.addConstraint(fieldConstr1);
            AndDescr patternAnd = new AndDescr();
            patternAnd.addDescr(patternDescr);
            base.addDescr(patternAnd);

            EvalDescr eval = new EvalDescr();
            eval.setContent(new String("DRulesEngineService.readDouble("
                    + "$obj" + j + "." + RuleUtils.makeGetterName(propName)
                    + "(),\"lastValue\")" + op + threshold));
            
            AndDescr evalAnd = new AndDescr();
            evalAnd.addDescr(eval); 
            base.addDescr(evalAnd);

            /*
             * BaseDescr[] arrDescr = new BaseDescr[3]; arrDescr[0] =
             * labelDescr; arrDescr[1] = patternDescr; arrDescr[2] = eval;
             * return arrDescr;
             */
            return base;
        }

    }

    private class LHSDumper_Object extends LHSDumper {

        protected LHSDumper_Object(Expression expression,
                int __j) {
            super(expression, __j);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.OBJECT;
        }

        public AndDescr feed() {
            String cond = expression.getCondition();
            String objType = expression.getObjType();
            String propName = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            // hack, make property name java beans spec compliant
            propName = fixPropertyName(propName);

            PatternDescr pattDescr = new PatternDescr();
            pattDescr.setIdentifier("$obj" + j);
            pattDescr.setObjectType(objType);

            pattDescr
                    .addConstraint(new FieldBindingDescr(propName, "$prop" + j));

            FieldConstraintDescr fieldConstrDescr1 = new FieldConstraintDescr(
                    propName);
            fieldConstrDescr1.addRestriction(new LiteralRestrictionDescr(op,
                    threshold, LiteralRestrictionDescr.TYPE_NUMBER));
            fieldConstrDescr1.addRestriction(new LiteralRestrictionDescr(">",
                    "-1000", LiteralRestrictionDescr.TYPE_NUMBER));

            FieldConstraintDescr fieldConstrDescr2 = new FieldConstraintDescr(
                    "serializedTO");
            RestrictionConnectiveDescr entryRestrict2 = new RestrictionConnectiveDescr(
                    1);
            String[] arrObjIds = expression.getObjects();
            for (String id : arrObjIds) {
                LiteralRestrictionDescr litRestr = new LiteralRestrictionDescr(
                        "==", id, LiteralRestrictionDescr.TYPE_STRING);
                entryRestrict2.addRestriction(litRestr);
            }
            fieldConstrDescr2.addRestriction(entryRestrict2);
            // bug #2562, check serializedTO first
            pattDescr.addConstraint(fieldConstrDescr2);
            pattDescr.addConstraint(fieldConstrDescr1);

            EvalDescr labelDescr = new EvalDescr();
            if ("and".equals(cond)) {
                labelDescr.setContent(new String("\"and\".equals(\"and\")"));
            } else {
                labelDescr.setContent(new String("\"or\".equals(\"or\")"));
            }

            /*
             * BaseDescr[] arrDescr = new BaseDescr[2]; arrDescr[0] =
             * labelDescr; arrDescr[1] = pattDescr; return arrDescr;
             */
            AndDescr result = new AndDescr();
            // result.addDescr(labelDescr);
            AndDescr patern = new AndDescr();
            patern.addDescr(pattDescr);
            result.addDescr(patern);
            return result;

        }

        private String fixPropertyName(String propName) {
            if (propName == null || propName.length() < 2) {
                return null;
            }
            char a = propName.charAt(0);
            char b = propName.charAt(1);

            if (Character.isLowerCase(a) && Character.isUpperCase(b)) {
                return propName.substring(0, 1).toUpperCase()
                        + propName.substring(1);
            } else {
                return propName;
            }
        }
    }

    private class LHSDumper_Node extends LHSDumper {

        protected LHSDumper_Node(Expression expression,
                int __j) {
            super(expression, __j);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.NODE;
        }

        public AndDescr feed() {
            PatternDescr pattDescr1 = this.path1();
            PatternDescr pattDescr2 = this.path2();
            EvalDescr evalDescr = new EvalDescr("DRulesEngineService.isParentOf($obj" + j + ".getNativeTO(),$sens" + j + ".getNativeTO())");

            AndDescr result = new AndDescr();

            AndDescr andPatt1 = new AndDescr();
            andPatt1.addDescr(pattDescr1);
            result.addDescr(andPatt1);

            AndDescr andPatt2 = new AndDescr();
            andPatt2.addDescr(pattDescr2);
            result.addDescr(andPatt2);

            AndDescr andPatt3 = new AndDescr();
            andPatt3.addDescr(evalDescr);
            result.addDescr(andPatt3);
            return result;
        }

        private PatternDescr path1() {
            PatternDescr pattDescr = new PatternDescr();
            pattDescr.setObjectType(EnvironmentTypes.TYPE_NODE);
            pattDescr.setIdentifier("$obj" + j);

            String fieldName = "serializedTO";
            FieldConstraintDescr fieldConstr = new FieldConstraintDescr(
                    fieldName);
            RestrictionConnectiveDescr entryConnectDescr = new RestrictionConnectiveDescr(
                    1);
            String[] arrObjIds = expression.getObjects();
            for (String id : arrObjIds) {
                LiteralRestrictionDescr litRestr = new LiteralRestrictionDescr(
                        "==", id, LiteralRestrictionDescr.TYPE_STRING);
                entryConnectDescr.addRestriction(litRestr);
            }
            fieldConstr.addRestriction(entryConnectDescr);
            pattDescr.addConstraint(fieldConstr);
            return pattDescr;
        }

        private PatternDescr path2() {
            // String cond = token[0];
            String dataClass = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            String propName = "lastValue";

            PatternDescr pattDescr = new PatternDescr();
            pattDescr.setIdentifier("$sens" + j);
            pattDescr.setObjectType(EnvironmentTypes.TYPE_SENSOR);

            FieldBindingDescr fieldBindDescr = new FieldBindingDescr(propName,
                    "$prop" + j);
            pattDescr.addConstraint(fieldBindDescr);

            FieldConstraintDescr fieldConstr1 = new FieldConstraintDescr(
                    propName);
            fieldConstr1.addRestriction(new LiteralRestrictionDescr(op,
                    threshold, LiteralRestrictionDescr.TYPE_NUMBER));
            fieldConstr1.addRestriction(new LiteralRestrictionDescr(">",
                    "-1000", LiteralRestrictionDescr.TYPE_NUMBER));
            pattDescr.addConstraint(fieldConstr1);

            FieldConstraintDescr fieldConstr3 = new FieldConstraintDescr(
                    "dataclass");
            fieldConstr3.addRestriction(new LiteralRestrictionDescr("==",
                    dataClass, LiteralRestrictionDescr.TYPE_NUMBER));
            pattDescr.addConstraint(fieldConstr3);

            return pattDescr;
        }
    }
}