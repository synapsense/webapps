package com.synapsense.alert.rule;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.drools.compiler.DrlParser;
import org.drools.compiler.DroolsParserException;
import org.drools.lang.DrlDumper;
import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.PackageDescr;
import org.drools.lang.descr.PackageDescrDumper;
import org.drools.lang.descr.RuleDescr;
import org.drools.xml.XmlPackageReader;

import com.synapsense.alert.rule.LHSParserFactory.LHSParser;
import com.synapsense.alert.rule.LHSParserFactory.LHSParserException;

public class Rule {
    private static final Log LOG = LogFactory.getLog(Rule.class);
    private PackageDescr packDesc;
    private boolean isLegacyRule = false;

    public Rule(String ruleSrc) {
        super();
        DrlParser parser = new DrlParser();
        try {
            packDesc = parser.parse(ruleSrc);
            isLegacyRule = ruleSrc.indexOf("serializedTO in") != -1;
        } catch (DroolsParserException e1) {
            LOG.error(e1.getLocalizedMessage(), e1);
        }
        if (packDesc.getName() == "") {
            XmlPackageReader xmlParser = new XmlPackageReader();
            try {
                packDesc = xmlParser.read(new StringReader(ruleSrc));
                isLegacyRule = true;
            } catch (Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
            }
        }
    }

    public Rule(String ruleName, String alertDisplayName,
            List<Expression> expressions, String duration) {
        packDesc = PackageDescrGeneratorFactory.getInstance()
                .newPackageDescrGenerator(duration).generatePackageDescr(
                        ruleName, alertDisplayName, expressions);
    }

    public String toString() {
        PackageDescrDumper dumper;
        // if (isLegacyRule) {
        // dumper = new XmlDumper();
        // } else {
        dumper = new DrlDumper();
        // }
        return dumper.dump(packDesc);
    }

    @SuppressWarnings("unchecked")
    public List<Expression> getExpressions() {
        List<Expression> result = new ArrayList<Expression>();

        List<RuleDescr> rules = (List<RuleDescr>) packDesc.getRules();
        for (int i = 0; i < rules.size(); i++) {
            RuleDescr rule = rules.get(i);
            if (!rule.getName().startsWith(
                    PackageDescrGenerator.ANTI_RULE_NAME_PREFIX)) {
                AndDescr lhsAnd = rule.getLhs();
                LHSParserFactory fac = new LHSParserFactory(lhsAnd,
                        isLegacyRule);
                try {
                    LHSParser parser = fac.getLHSParser();
                    int j = 0;
                    while (parser != null) {
                        Expression expr = parser.getExpression();
                        if (i > 0 && j == 0) {
                            expr.setCondition("or");
                        }
                        result.add(expr);
                        parser = fac.getLHSParser();
                        j++;
                    }
                } catch (LHSParserException e) {
                }
            }
        }

        return result;
    }

    public String getDuration() {
        String duration = "0";
        RuleDescr rule = (RuleDescr) packDesc.getRules().get(0);
        if (rule != null) {
            String consequence = (String) rule.getConsequence();
            int start = consequence.lastIndexOf("getNativeTO(), t, ");
            int end = consequence.lastIndexOf(");");
            if (start != -1 && end != -1 && start < end) {
                String result = consequence.substring(start + 18, end);
                if (result != null) {
                    duration = result;
                }
            }
        }

        return duration;
    }

    public String getDuration(String ruleName) {
        String duration = "0";

        RuleDescr rule = getRuleDescrByName(ruleName);
        if (rule != null) {
            String consequence = (String) rule.getConsequence();

            int start = consequence.lastIndexOf("getNativeTO(), t, ");
            int end = consequence.lastIndexOf(");");
            if (start != -1 && end != -1 && start < end) {
                String result = consequence.substring(start + 18, end);
                if (result != null) {
                    duration = result;
                }
            }
        }
        return duration;

    }
    
    /**
     * Warning: this function is for special SynapSense system rules only!
     * Sets duration in rule source, returns updated rule source.
     * This function was written because of bug#6034, since DrlDumper sometimes generates rules that can't be compiled. 
     * @param duration
     * @param ruleSource
     * @return
     */
    public static String setDurationInSystemRuleSource(String duration, String ruleSource){
        try {
            Long.parseLong(duration);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                    "Duration doesn't represent a number", e);
        }
        final String searchString = "getNativeTO(), t, ";
        String result = ruleSource;
        int start = ruleSource.lastIndexOf(searchString);
        if(start != -1){
            int end = ruleSource.indexOf(");", start);
            if(end != -1){
                String part1 = ruleSource.substring(0, start + searchString.length());
                String part2 = ruleSource.substring(end, ruleSource.length());
                result = part1 + duration + part2;
            }
        }
        return result;
    }

    /*
    public void setDuration(String duration, String ruleName) {
        try {
            Long.parseLong(duration);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                    "Duration doesn't represent a number", e);
        }
        RuleDescr rule = getRuleDescrByName(ruleName);
        String consequence = (String) rule.getConsequence();
        int start = consequence.lastIndexOf("getNativeTO(), t, ");
        int end = consequence.lastIndexOf(");");
        if (start != -1 && end != -1 && start < end) {
            String part1 = consequence.substring(0, start + 18);
            String part2 = consequence.substring(end, consequence.length());
            consequence = part1 + duration + part2;
            rule.setConsequence(consequence);
        }
    }

    public void setDuration(String duration) {
        setDuration(duration, 0);
    }

    @SuppressWarnings("unchecked")
    private void setDuration(String duration, int indx) {
        try {
            Long.parseLong(duration);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(
                    "Duration doesn't represent a number", e);
        }

        AttributeDescr durationAttr = getAttributeDescr("duration", indx);
        if (durationAttr == null) {
            RuleDescr rule = (RuleDescr) packDesc.getRules().get(indx);
            List<AttributeDescr> attrList = rule.getAttributes();
            List<AttributeDescr> newAttrList = new ArrayList<AttributeDescr>(
                    attrList);
            durationAttr = new AttributeDescr("duration", duration);
            newAttrList.add(durationAttr);
            rule.setAttributes(newAttrList);

        }
        durationAttr.setValue(duration);
    }

    @SuppressWarnings("unchecked")
    private AttributeDescr getAttributeDescr(String attrName, int indx) {
        if (attrName == null) {
            return null;
        }

        RuleDescr rule = (RuleDescr) packDesc.getRules().get(indx);

        List<AttributeDescr> attrList = rule.getAttributes();
        for (AttributeDescr attr : attrList) {
            if (attrName.equals(attr.getName())) {
                return attr;
            }
        }

        return null;
    }*/
    
    @SuppressWarnings("unchecked")
    public List<String> getNames() {
        List<String> result = new ArrayList<String>();

        List<RuleDescr> rules = (List<RuleDescr>) packDesc.getRules();
        for (int i = 0; i < rules.size(); i++) {
            RuleDescr rule = rules.get(i);
            result.add(rule.getName());
        }

        return result;

    }

//    private AttributeDescr getAttributeDescr(String attrName, String ruleName) {
//        if (attrName == null) {
//            return null;
//        }
//        AttributeDescr attr = null;
//
//        RuleDescr rule = getRuleDescrByName(ruleName);
//        if (rule != null) {
//            List<AttributeDescr> attrList = rule.getAttributes();
//            for (AttributeDescr attrs : attrList) {
//                if (attrName.equals(attrs.getName())) {
//                    attr = attrs;
//                    break;
//                }
//            }
//        }
//        return attr;
//    }

    @SuppressWarnings("unchecked")
    private RuleDescr getRuleDescrByName(String ruleName) {
        RuleDescr resRule = null;
        List<RuleDescr> rules = packDesc.getRules();
        for (RuleDescr rule : rules) {
            if (rule.getName().equals(ruleName)) {
                resRule = rule;
                break;
            }
        }
        return resRule;
    }
}