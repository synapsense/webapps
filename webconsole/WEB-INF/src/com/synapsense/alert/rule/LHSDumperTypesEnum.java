package com.synapsense.alert.rule;

/**
 * The <code>LHSDumperTypesEnum</code> enumeration contains types of the
 * LHSDumpers.
 */
enum LHSDumperTypesEnum {
    SENSOR_LINK, OBJECT, NODE;
}