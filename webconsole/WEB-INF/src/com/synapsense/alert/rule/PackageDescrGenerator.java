package com.synapsense.alert.rule;

import java.util.List;

import org.drools.lang.descr.PackageDescr;

/**
 * <code>PackageDescrGenerator</code> provides interface to generate rule
 * package based on the given parameters. The generated set of rules depends on
 * the duration parameter of the rule and given rule conditions (i.e.
 * expressions). All expressions are to be separated on lists with only "and"
 * expressions in every list. Separate rule to be generated for each list.
 * 
 * @author stikhon
 */

public interface PackageDescrGenerator {
    /**
     * The package description name to be used for all console generated rule
     * packages.
     */
    String PACKAGE_DESCRIPTION_NAME = "ConditionalAlerts";

    /**
     * The consequence pattern for alert generation. It has three place holders.
     * The <b>alert name</b> should be passed for the first one, the <b>alert
     * message</b> for the second and the <b>object index</b> number for the
     * third.
     */
    String ALERT_GENERATION_CONSEQUENCE_PATTERN = "DRulesEngineService.sendAlert(\"%1$s\","
            + "\"%2$s\",%3$s,null,$obj%4$d.getNativeTO());\n";
    /**
     * Anti-rule name prefix.
     */
    String ANTI_RULE_NAME_PREFIX = "anti_";
    
    /**
     * Duration rule name prefix.
     */
    String DURATION_RULE_NAME_PREFIX = "duration_";
    // imports and global variables
    String DURATION_ATTRIBUTE_ID = "duration";
    String TRIGGER_GLOBAL_ID = "triggers";
    String TRIGGER_GLOBAL_TYPE = "java.util.concurrent.ConcurrentHashMap";
    String DOMAIN_OBJECTS_IMPORT = "com.synapsense.dre.domainobjects.*";
    String DRULES_SERVICE_IMPORT = "com.synapsense.dre.service.DRulesEngineService";
    String TRIGGER_IMPORT = "com.synapsense.dre.service.Trigger";

    /**
     * Generates <code>PackageDescr</code> based on the given parameters.
     * 
     * @param ruleName
     *            The name of the rule. This name with rule index is assigned to
     *            each rule in the package.
     * @param alertName
     *            The alert name parameter is used in the RHS part of the rule
     *            to generate an alert with correct name.
     * @param expressions
     *            The list of expressions, which should be handled by
     *            <code>LHSDumper</code> to get <code>AndDescr</code> for the
     *            LHS part of the rule.
     * @param unitSystem
     *            Unit System in that thresholds are store in expression<code>LHSDumper</code>.
     * 
     * @return The generated <code>PackageDescr</code>.
     */
    PackageDescr generatePackageDescr(String ruleName, String alertName,
            List<Expression> expressions);

}
