package com.synapsense.alert.rule;

import org.drools.lang.descr.AndDescr;
import org.drools.lang.descr.EvalDescr;
import org.drools.lang.descr.FieldBindingDescr;
import org.drools.lang.descr.FieldConstraintDescr;
import org.drools.lang.descr.LiteralRestrictionDescr;
import org.drools.lang.descr.PatternDescr;

import com.synapsense.utilities.es.EnvironmentTypes;

public class AntiLHSDumperFactory {
    private AntiLHSDumperFactory() {
    }

    public static LHSDumper getLHS(Expression expression,
            int __j, int objIndex) {
        AntiLHSDumperFactory fac = new AntiLHSDumperFactory();

        boolean hasDataClass = true;
        try {
            Integer.parseInt(expression.getPropertyName());
        } catch (NumberFormatException e) {
            hasDataClass = false;
        }

        if (expression.getObjType().equals(EnvironmentTypes.TYPE_NODE)
                && hasDataClass) {
            return fac.new LHSDumper_Node(expression, __j,  objIndex);
        } else if (expression.isSensorLink()) {
            return fac.new LHSDumper_SensorLink(expression, __j,  objIndex);
        } else {
            return fac.new LHSDumper_Object(expression, __j,  objIndex);
        }
    }

    public abstract class LHSDumper {
        protected int j;
        protected Expression expression;
        protected  int objIndex;

        protected LHSDumper(Expression expression,
                int __j, int objIndex) {
            this.expression = expression;
            this.j = __j;
            this.objIndex = objIndex;
            
        }

        public abstract AndDescr feed();

        public abstract LHSDumperTypesEnum getType();
    }

    private class LHSDumper_SensorLink extends LHSDumper {
      

        protected LHSDumper_SensorLink(Expression expression,
                int __j, int objIndex) {
            super(expression, __j, objIndex);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.SENSOR_LINK;
        }

        public AndDescr feed() {
            String cond = expression.getCondition();
            String objType = expression.getObjType();
            String dataClass = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            String propName = dataClass;

            AndDescr base = new AndDescr();

            EvalDescr labelDescr = new EvalDescr();
            if ("and".equals(cond)) {
                labelDescr.setContent(new String("\"and\".equals(\"and\")"));
            } else {
                labelDescr.setContent(new String("\"or\".equals(\"or\")"));
            }
            // base.addDescr(labelDescr);

            PatternDescr patternDescr = new PatternDescr(objType, "$obj" + j);
            EvalDescr objectRestriction = new EvalDescr();
            objectRestriction.setContent(new String("serializedTO  == $t.getObject(" + objIndex + ")"));
            patternDescr.addConstraint(objectRestriction);
            AndDescr patternAnd = new AndDescr();
            patternAnd.addDescr(patternDescr);
            base.addDescr(patternAnd);

            EvalDescr eval = new EvalDescr();
            eval.setContent(new String("DRulesEngineService.readDouble("
                    + "$obj" + j + "." + RuleUtils.makeGetterName(propName)
                    + "(),\"lastValue\")" + op + threshold));
            
            AndDescr evalAnd = new AndDescr();
            evalAnd.addDescr(eval); 
            base.addDescr(evalAnd);

            return base;
        }

    }

    private class LHSDumper_Object extends LHSDumper {

        protected LHSDumper_Object(Expression expression,
                int __j, int objIndex) {
            super(expression, __j, objIndex);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.OBJECT;
        }

        public AndDescr feed() {
            String cond = expression.getCondition();
            String objType = expression.getObjType();
            String dataClass = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            String propName = null;
            if (objType.equalsIgnoreCase(EnvironmentTypes.TYPE_SENSOR)) {
                propName = "lastValue";
            } else {
                propName = dataClass;
                // hack, make property name java beans spec compliant
                propName = fixPropertyName(propName);
            }
            
            PatternDescr pattDescr = new PatternDescr(objType, "$obj" + j);
            EvalDescr objectRestriction = new EvalDescr();
            objectRestriction.setContent(new String("serializedTO  == $t.getObject(" + objIndex + ")"));
            pattDescr.addConstraint(objectRestriction);
            pattDescr
                    .addConstraint(new FieldBindingDescr(propName, "$prop" + j));

            FieldConstraintDescr fieldConstrDescr1 = new FieldConstraintDescr(
                    propName);
            fieldConstrDescr1.addRestriction(new LiteralRestrictionDescr(op,
                    threshold, LiteralRestrictionDescr.TYPE_NUMBER));
            fieldConstrDescr1.addRestriction(new LiteralRestrictionDescr(">",
                    "-1000", LiteralRestrictionDescr.TYPE_NUMBER));

            pattDescr.addConstraint(fieldConstrDescr1);

            EvalDescr labelDescr = new EvalDescr();
            if ("and".equals(cond)) {
                labelDescr.setContent(new String("\"and\".equals(\"and\")"));
            } else {
                labelDescr.setContent(new String("\"or\".equals(\"or\")"));
            }

            AndDescr result = new AndDescr();
            AndDescr patern = new AndDescr();
            patern.addDescr(pattDescr);
            result.addDescr(patern);
            return result;

        }

        private String fixPropertyName(String propName) {
            if (propName == null || propName.length() < 2) {
                return null;
            }
            char a = propName.charAt(0);
            char b = propName.charAt(1);

            if (Character.isLowerCase(a) && Character.isUpperCase(b)) {
                return propName.substring(0, 1).toUpperCase()
                        + propName.substring(1);
            } else {
                return propName;
            }
        }
    }

    private class LHSDumper_Node extends LHSDumper {

        protected LHSDumper_Node(Expression expression,
                int __j, int objIndex) {
            super(expression, __j, objIndex);
        }

        public LHSDumperTypesEnum getType() {
            return LHSDumperTypesEnum.NODE;
        }

        public AndDescr feed() {
            PatternDescr pattDescr1 = this.path1();
            PatternDescr pattDescr2 = this.path2();
            EvalDescr evalDescr = new EvalDescr("DRulesEngineService.isParentOf($obj" + j + ".getNativeTO(),$sens" + j + ".getNativeTO())");

            AndDescr result = new AndDescr();

            AndDescr andPatt1 = new AndDescr();
            andPatt1.addDescr(pattDescr1);
            result.addDescr(andPatt1);

            AndDescr andPatt2 = new AndDescr();
            andPatt2.addDescr(pattDescr2);
            result.addDescr(andPatt2);

            AndDescr andPatt3 = new AndDescr();
            andPatt3.addDescr(evalDescr);
            result.addDescr(andPatt3);

            return result;
        }

        private PatternDescr path1() {
            PatternDescr pattDescr = new PatternDescr(EnvironmentTypes.TYPE_NODE, "$obj" + j);
            EvalDescr objectRestriction = new EvalDescr();
            objectRestriction.setContent(new String("serializedTO  == $t.getObject(" + objIndex + ")"));
            pattDescr.addConstraint(objectRestriction);
            return pattDescr;
        }

        private PatternDescr path2() {
            String dataClass = expression.getPropertyName();
            String op = expression.getOperation();
            String threshold = expression.getThreshold();
            String propName = "lastValue";

            PatternDescr pattDescr = new PatternDescr();
            pattDescr.setIdentifier("$sens" + j);
            pattDescr.setObjectType(EnvironmentTypes.TYPE_SENSOR);

            FieldBindingDescr fieldBindDescr = new FieldBindingDescr(propName,
                    "$prop" + j);
            pattDescr.addConstraint(fieldBindDescr);

            FieldConstraintDescr fieldConstr1 = new FieldConstraintDescr(
                    propName);
            fieldConstr1.addRestriction(new LiteralRestrictionDescr(op,
                    threshold, LiteralRestrictionDescr.TYPE_NUMBER));
            fieldConstr1.addRestriction(new LiteralRestrictionDescr(">",
                    "-1000", LiteralRestrictionDescr.TYPE_NUMBER));
            pattDescr.addConstraint(fieldConstr1);

            FieldConstraintDescr fieldConstr3 = new FieldConstraintDescr(
                    "dataclass");
            fieldConstr3.addRestriction(new LiteralRestrictionDescr("==",
                    dataClass, LiteralRestrictionDescr.TYPE_NUMBER));
            pattDescr.addConstraint(fieldConstr3);

            return pattDescr;
        }
    }
}