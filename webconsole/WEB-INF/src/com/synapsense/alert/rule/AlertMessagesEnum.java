package com.synapsense.alert.rule;

/**
 * The <code>AlertMessagesEnum</code> contains Alert Message patterns to build
 * alert message text. The pattern has four place holders. The <b>object
 * type</b> should be passed for the first one, the <b>property name</b> for the
 * second,the <b>getter for the property name</b> for the third and
 * <b>expression index</b> number for the fourth.
 */
public enum AlertMessagesEnum {
    //Drools rule should generate strings like "$data(RACK:2464.cTop.lastValue,75.434,200)$"
    //Last param in $data$ macro (dimension) is optional 
    NODE(LHSDumperTypesEnum.NODE, "+\"$data(\" + $sens%4$d.getNativeTO() + "
            +"\".lastValue,\" + $sens%4$d.getLastValue() + \"%5$s)$\""), 
    OBJECT(LHSDumperTypesEnum.OBJECT, "+\"$data(\" + $obj%4$d.getNativeTO() + "
            +"\".%2$s,\" + $obj%4$d.%3$s() + \"%5$s)$\""), 
    SENSOR_LINK(LHSDumperTypesEnum.SENSOR_LINK, "+\"$data(\" + $obj%4$d.getNativeTO() + "
            +"\".%2$s.lastValue,\" + "
            + "DRulesEngineService.readDouble($obj%4$d.%3$s(),"
            + " \"lastValue\") + \"%5$s)$\"");

    private LHSDumperTypesEnum lhsDumperType;

    private String alertMessagePattern;

    private AlertMessagesEnum(final LHSDumperTypesEnum lhsDumperType,
            final String alertMessagePattern) {
        this.lhsDumperType = lhsDumperType;
        this.alertMessagePattern = alertMessagePattern;
    }

    /**
     * Gets appropriate alert message pattern for the given
     * <code>LHSDumperTypesEnum</code>.
     * 
     * @param lhsDumperType
     *            The <code>LHSDumperTypesEnum</code> to get alert message
     *            pattern for.
     * 
     * @return Alert message pattern, if found. Empty string otherwise.
     */
    static public String getAlertMessageByType(LHSDumperTypesEnum lhsDumperType) {
        String alertMessageToReturn = "";
        for (AlertMessagesEnum alertMessage : AlertMessagesEnum.values()) {
            if (alertMessage.getLhsDumperType().equals(lhsDumperType)) {
                alertMessageToReturn = alertMessage.getAlertMessagePattern();
            }
        }
        return alertMessageToReturn;
    }

    /**
     * Gets the LHS dumper type for this <code>AlertMessagesEnum</code> entry.
     * 
     * @return The LHS dumper type.
     */
    public LHSDumperTypesEnum getLhsDumperType() {
        return lhsDumperType;
    }

    /**
     * Gets the alert message pattern this <code>AlertMessagesEnum</code> entry.
     * 
     * @return The alert message pattern.
     */
    public String getAlertMessagePattern() {
        return alertMessagePattern;
    }
}