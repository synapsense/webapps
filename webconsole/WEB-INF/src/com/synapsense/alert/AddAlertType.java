package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.synapsense.alert.rule.Expression;
import com.synapsense.alert.rule.Rule;
import com.synapsense.alert.rule.RuleUtils;
import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;

public class AddAlertType extends BaseHttpServlet {

    private static final long serialVersionUID = -8766692067775248363L;
    private static final Log LOG = LogFactory.getLog(AddAlertType.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");

        req.setCharacterEncoding("utf-8");

        String resultMessage = createAlert(req);
        if (resultMessage != null){
            sendResponse(resp, "<result>" + resultMessage + "</result>");	
        }
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private void addUserActionDestination(String alertTypeName, String tierName, String tierValue, String recipient, HttpServletRequest request) throws NoSuchAlertTypeException {
        if(AlertUtils.ALERT_USER_DESTINATION_SMTP.equals(tierValue) || AlertUtils.ALERT_USER_DESTINATION_BOTH.equals(tierValue)) {
            Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, MessageType.SMTP, recipient);
        }
        if(AlertUtils.ALERT_USER_DESTINATION_SMS.equals(tierValue) || AlertUtils.ALERT_USER_DESTINATION_BOTH.equals(tierValue)) {
            Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, MessageType.SMS, recipient);
        }
    }
    
    private String createAlert(HttpServletRequest request){
        String result = null; 		
        boolean alertTypeCreated = false;
        String alertTypeName = "";
        boolean ruleCreated =false;
        TO<?> ruleTypeTo = null;
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        
        try {
            String alertDisplayName = request.getParameter(RequestVariableNames.DISPLAY_NAME);
            String alertDesc = request.getParameter(RequestVariableNames.DESCRIPTION);
            String alertPriorityStr = request.getParameter(RequestVariableNames.PRIORITY);
            String alertAutodismissStr = request.getParameter(RequestVariableNames.AUTODISMISS);
            String alertMsgHeader = request.getParameter(RequestVariableNames.MESSAGE_HEADER);
            String alertMsgBody = request.getParameter(RequestVariableNames.MESSAGE_BODY);

            if (alertDisplayName == null) {
                LOG.error("Alert name is null");
                return ErrorMessages.UNABLE_TO_COMPLETE;
            }
            alertTypeName = Constants.CONSOLE_ALERT_PREFIX + alertDisplayName;
            if(LOG.isDebugEnabled()) {
                LOG.debug("Get alert types for '" + alertTypeName + "' alert");
            }
            
            if(AlertUtils.isAlertExists(alertTypeName, request)){
                LOG.error("Rule or alert type for '" + alertTypeName + "' already exists");
                return ErrorMessages.DUPLICATE_ALERT_NAME;
            }
            
            Set<MessageTemplate> msgTemplates = helper.getTemplatesFromJsonArr(alertTypeName, request.getParameter("notificationsjson"));
            boolean isActive = true;
            boolean autoDismiss = Boolean.parseBoolean(alertAutodismissStr);
            boolean autoAcknowledge = Boolean.parseBoolean(request.getParameter("autoack"));			
            String priorityName = AlertUtils.numberStringToPriorityName(alertPriorityStr);			
            
            AlertType alertConfig = new AlertType(alertTypeName, alertDisplayName, alertDesc, priorityName,
                    alertMsgHeader, alertMsgBody, msgTemplates, isActive, autoDismiss, autoAcknowledge, true);
            
            
            if(LOG.isDebugEnabled()) {
                LOG.debug("Create '" + alertConfig.getName() + "' alert type for '"+ alertTypeName + "' alert");
            }
            Env.getAlertingServ(request).createAlertType(alertConfig);			
                        
            String destinationsJsonArr = request.getParameter("destinations");
            JSONArray jarr = new JSONArray(destinationsJsonArr);
            for(int i = 0; i < jarr.length(); i++) {
                JSONObject jobj = jarr.getJSONObject(i);
                String type = jobj.getString("type");
                String recipient = jobj.getString("recipient");
                
                for(int j = 0; j < 3; j++) {
                    String tierName = "TIER" + (j + 1);
                    String tierValue = jobj.getString(tierName.toLowerCase());
                    if("user".equals(type)) {
                        addUserActionDestination(alertTypeName, tierName, tierValue, recipient, request);
                    } else if("true".equals(tierValue)) {
                        Env.getAlertingServ(request).addActionDestination(alertTypeName, tierName, "snmp".equals(type) ? MessageType.SNMP : MessageType.AUDIO_VISUAL, recipient);
                    }
                }
            }
            alertTypeCreated = true;
                        
            List<Expression> expressions = RuleUtils.getExpressionsFromRequest(request);
            DRuleTypeService<?> ruleType = Env.getDRuleTypeDAO();

            String ruleName = alertTypeName;
            Rule r = new Rule(ruleName, alertDisplayName, expressions, request.getParameter("duration"));
            String rStr = r.toString();
            RuleType ruleConfig = new RuleType(ruleName, rStr, "drools", false);
            if(LOG.isDebugEnabled()) {
                LOG.debug("Create rule '" + ruleName + "'");
            }
            ruleTypeTo = ruleType.create(ruleConfig);
            ruleCreated = true;
            
            if(LOG.isDebugEnabled()) {
                LOG.debug("Loading rule '" + ruleName + "' to DRE");
            }
            Env.getDRE().addRulesFromDAO(Integer.valueOf(ruleTypeTo.getID().toString()));
            
            result = ErrorMessages.SUCCESS;
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
            
            if (alertTypeCreated) {
                try {
                    for(int i = 0; i < 3; i++) {
                        String tierName = "TIER" + (i + 1);
                        Collection<String> destinations = Env.getAlertingServ(request).getActionDestinations(alertTypeName, tierName, MessageType.SMTP);
                        for(String destination : destinations) {
                            Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, MessageType.SMTP, destination);
                        }
                        destinations = Env.getAlertingServ(request).getActionDestinations(alertTypeName, tierName, MessageType.SMS);
                        for(String destination : destinations) {
                            Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, MessageType.SMS, destination);
                        }
                        destinations = Env.getAlertingServ(request).getActionDestinations(alertTypeName, tierName, MessageType.SNMP);
                        for(String destination : destinations) {
                            Env.getAlertingServ(request).removeActionDestination(alertTypeName, tierName, MessageType.SNMP, destination);
                        }
                    }
                } catch (NoSuchAlertTypeException e1) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
                Env.getAlertingServ(request).deleteAlertType(alertTypeName);
            }
            
            if (ruleCreated) {
                    if(LOG.isDebugEnabled()) {
                        LOG.debug("Delete rule '" + alertTypeName + "' from database");
                    }
                    Env.getDRuleTypeDAO().delete(ruleTypeTo.getID());
            }
            
        }
        return result;
        
    }
}
