package com.synapsense.alert;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.synapsense.alert.rule.Expression;
import com.synapsense.alert.rule.Rule;
import com.synapsense.alert.rule.RuleUtils;
import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.config.UIConfig;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.RuleType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.AlertService;
import com.synapsense.service.DRuleTypeService;
import com.synapsense.service.FileWriter;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetAlertType extends BaseHttpServlet {

    /**
     * 
     */
    private static final long serialVersionUID = -2694215900764220041L;
    private static final Log LOG = LogFactory.getLog(GetAlertType.class);
    private static Map<String, String> destinationTypesMap = new HashMap<String, String>();
    
    static {
        destinationTypesMap.put("snmp:trap", "trap");
        destinationTypesMap.put("snmp:avdevice", "avdevice");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
    throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        try {
            sendResponse(resp, getAlertTypesDoc(req));
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        LOG.info(this.getServletName() + " is finished ...");
    }

    private Document getEmptyAlertTypesDoc(HttpServletRequest request) throws ServletException, IOException, ParserConfigurationException {
        Document doc = null;
        doc = XMLHelper.createNewXmlDocument("alert_types");
        Node alert_typesNode = doc.getDocumentElement();
        Node alert_typeNode = doc.createElement("alert_type");
        alert_typesNode.appendChild(alert_typeNode);
        
        XMLHelper.appendNodeWithText(alert_typeNode, "system", "false");
        XMLHelper.appendNodeWithText(alert_typeNode, "name", "");
        XMLHelper.appendNodeWithText(alert_typeNode, "description", "");
        XMLHelper.appendNodeWithText(alert_typeNode, "header", "");
        XMLHelper.appendNodeWithText(alert_typeNode, "body", "");
        XMLHelper.appendNodeWithText(alert_typeNode, "priority", ""+AlertUtils.priorityNameToInt(AlertUtils.ALERT_PRIORITY_NAME_MINOR));
        XMLHelper.appendNodeWithText(alert_typeNode, "autodismiss", "1");
        XMLHelper.appendNodeWithText(alert_typeNode, "autodismissallowed", "1");
        XMLHelper.appendNodeWithText(alert_typeNode, "autoack", "0");
        XMLHelper.appendNodeWithText(alert_typeNode, "duration", "0");
        
        Node conditionNode = doc.createElement("condition");
        XMLHelper.appendNodeWithText(conditionNode, "c", "and");
        XMLHelper.appendNodeWithText(conditionNode, "type", "");
        XMLHelper.appendNodeWithText(conditionNode, "prop", "");
        XMLHelper.appendNodeWithText(conditionNode, "op", ">");
        XMLHelper.appendNodeWithText(conditionNode, "threshold", "");
        XMLHelper.appendNodeWithText(conditionNode, "ids", "");
        alert_typeNode.appendChild(conditionNode);
        
        Node alertDestNode = XMLHelper.createXmlTextNode(doc, "destinations", null);
        includeUserDestinations("NEWALERT", alertDestNode, true, request);
        includeSNMPDestinations("NEWALERT", alertDestNode, true, request);
        alert_typeNode.appendChild(alertDestNode);
        includeAlertableTypes(alert_typeNode, request);
        
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        Node templatesNode = doc.createElement("templates");
        String templatesXmlStr = "";
        for (MessageType messageType : MessageType.values()) {
            TO<?> to = helper.getDefaultMessageTemplate("" + messageType.code());
            if (to != null) {
                templatesXmlStr += helper.getMessageTemplateAsXmlString(to);
            }
        }
        XMLHelper.parseAndAppend(templatesNode, templatesXmlStr);
        alert_typeNode.appendChild(templatesNode);
        return doc;
    }

    private Document getAlertTypesDoc(HttpServletRequest req) throws ServletException, IOException, ParserConfigurationException {
        Document doc = null;
        
        boolean associatedObjectsInfoAppended = false;
        boolean isSystem = Boolean.valueOf(req.getParameter("system"));
        String name = req.getParameter("name");
        
        if(name == null) {
            //Create empty alert
            return getEmptyAlertTypesDoc(req);
        }

        Collection<AlertType> alertTypes = Env.getAlertingServ(req).getAlertTypes(new String[] {name});
        
        if(alertTypes.isEmpty()) {
            throw new ServletException("Object [" + name + "] is removed.");
        }

        try {
            doc = XMLHelper.createNewXmlDocument("alert_types");

            Element root = doc.getDocumentElement();

            for (AlertType alertType : alertTypes) {
                String alertTypeName = alertType.getName();
                
                Node alertTypeNode = doc.createElement("alert_type");
                XMLHelper.appendNodeWithText(alertTypeNode, "system", String.valueOf(isSystem));
                XMLHelper.appendNodeWithText(alertTypeNode, "name", req.getParameter("name"));
                XMLHelper.appendNodeWithText(alertTypeNode, "displayName", alertType.getDisplayName());
                XMLHelper.appendNodeWithText(alertTypeNode, "description", alertType.getDescription());
                XMLHelper.appendNodeWithText(alertTypeNode, "header", alertType.getDefaultHeader());
                XMLHelper.appendNodeWithText(alertTypeNode, "body", alertType.getDefaultBody());
                XMLHelper.appendNodeWithText(alertTypeNode, "priority", "" + AlertUtils.priorityNameToInt(alertType.getPriority()));
                XMLHelper.appendNodeWithText(alertTypeNode,	"autodismiss", (alertType.isAutoDismiss()) ? "1" : "0");
                XMLHelper.appendNodeWithText(alertTypeNode,	"autodismissallowed", (alertType.isAutoDismissAllowed()) ? "1" : "0");
                XMLHelper.appendNodeWithText(alertTypeNode,	"autoack", (alertType.isAutoAcknowledge()) ? "1" : "0");
                
                Node alertDestNode = doc.createElement("destinations");					
                includeUserDestinations(alertTypeName, alertDestNode, false, req);
                includeSNMPDestinations(alertTypeName, alertDestNode, false, req);
                alertTypeNode.appendChild(alertDestNode);

                DRuleTypeService<Object> ruleTypeDAO = Env.getDRuleTypeDAO();
                if (!isSystem) {
                    RuleType ruleType = ruleTypeDAO.getRuleTypeDTOByName(alertTypeName);
                    
                    String ruleSrc = ruleType.getSource();
                    Rule rule = new Rule(ruleSrc);
                    List<Expression> expressions = rule.getExpressions();

                    for (Expression expression : expressions) {
                        RuleUtils.postprocessExpression(expression, Env.getEnv(req));
                    }

                    String duration = rule.getDuration();
                    XMLHelper.appendNodeWithText(alertTypeNode, "duration", duration);
                    
                    if(!associatedObjectsInfoAppended) {
                        appendAssociatedObjectsInfo(expressions, req, alertTypeNode);						
                        associatedObjectsInfoAppended = true;
                    }
                    
                    for (Expression expression : expressions) {
                        expression.toNode(doc, alertTypeNode);
                    }
                } else {
                    TO<?> ruleTypeTo = ruleTypeDAO.getRuleTypeByName(alertTypeName);
                    String duration = "-1";
                    if (ruleTypeTo != null) {
                        String ruleSrc = ruleTypeDAO.getSource(ruleTypeTo.getID());
                        Rule rule = new Rule(ruleSrc);
                        duration = rule.getDuration();
                    }
                    XMLHelper.appendNodeWithText(alertTypeNode, "duration", duration);
                }
                includeAlertableTypes(alertTypeNode, req);
                root.appendChild(alertTypeNode);
                
                //Include templates data
                try {
                    AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(req);
                    String templatesXmlStr = "";
                    Set<MessageTemplate> mts = alertType.getMessageTemplates();
                    Node templatesNode = doc.createElement("templates");
                    alertTypeNode.appendChild(templatesNode);
                    for(MessageTemplate mt : mts) {
                        templatesXmlStr += helper.getMessageTemplateAsXmlString(mt.getRef());
                    }
                    XMLHelper.parseAndAppend(templatesNode, templatesXmlStr);
                } catch(Exception e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            }
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);			
        } catch (ObjectNotFoundException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return doc;
    }
    
    private void appendAssociatedObjectsInfo(List<Expression> expressions, HttpServletRequest req, Node alertTypeNode)
    {
        List<String> existingObjects = new ArrayList<String>();
        List<String> removedObjects = new ArrayList<String>();
        getExistingAndRemovedObjectsIDs(existingObjects, removedObjects, expressions, req);
        
        if(removedObjects.size() != 0) {
            appendRemovedObjectsInfo(removedObjects, alertTypeNode);
        }
    }
    
    private void getExistingAndRemovedObjectsIDs(List<String> outExistingObjects, List<String> outRemovedObjects, List<Expression> expressions, HttpServletRequest req)
    {
        for(Expression expression : expressions) {
            String [] objects = expression.getObjects();
            for(String object : objects) {
                TO<?> obj = TOFactory.getInstance().loadTO(object);
                
                if(Env.getEnv(req).exists(obj)) {
                    if(!outExistingObjects.contains(object)) {
                        outExistingObjects.add(object);
                    }
                } else {
                    if(!outRemovedObjects.contains(object))	{
                        outRemovedObjects.add(object);
                    }			    	
                }
            }
        }
    }
    
    private void appendRemovedObjectsInfo(List<String> removedObjects, Node alertTypeNode)
    {
        Document doc = alertTypeNode.getOwnerDocument();
        
        Node removedObjectsNode = doc.createElement("removed_objects");
        alertTypeNode.appendChild(removedObjectsNode);
            
        for(String object : removedObjects) {
            Node removedObjectNode = XMLHelper.createXmlTextNode(doc, "removed_object", object);
            removedObjectsNode.appendChild(removedObjectNode);
        }		
    }
    
    private void includeUserDestinations(String alertTypeName, Node destinationsNode, boolean isNewAlertType, HttpServletRequest request) {
        try {
            Document doc = destinationsNode.getOwnerDocument();
            Collection<TO<?>> users = Env.getUserDAO().getAllUsers();
            Iterator<TO<?>> iusers = users.iterator();
            Node usersNode = doc.createElement("users");
            destinationsNode.appendChild(usersNode);
            
            iusers = users.iterator();
            while(iusers.hasNext()) {
                TO<?> user = iusers.next();
                String userName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_NAME);
                String firstName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_FIRST_NAME);
                String lastName = (String) Env.getUserDAO().getProperty(user, UserManagementService.USER_LAST_NAME);
                
                Node userNode = XMLHelper.createXmlTextNode(doc, "user", null);
                XMLHelper.setNodeAttribute(userNode, "username", userName);
                XMLHelper.setNodeAttribute(userNode, "name", lastName + " " + firstName + " (" + userName + ")");
                if(!isNewAlertType) {
                    XMLHelper.setNodeAttribute(userNode, "tier1", getUserActionDestinationType(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER1_NAME, userName, request));
                    XMLHelper.setNodeAttribute(userNode, "tier2", getUserActionDestinationType(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER2_NAME, userName, request));
                    XMLHelper.setNodeAttribute(userNode, "tier3", getUserActionDestinationType(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER3_NAME, userName, request));
                } else {
                    XMLHelper.setNodeAttribute(userNode, "tier1", AlertUtils.ALERT_USER_DESTINATION_NONE);
                    XMLHelper.setNodeAttribute(userNode, "tier2", AlertUtils.ALERT_USER_DESTINATION_NONE);
                    XMLHelper.setNodeAttribute(userNode, "tier3", AlertUtils.ALERT_USER_DESTINATION_NONE);					
                }
                
                usersNode.appendChild(userNode);
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }

    private String getUserActionDestinationType(String alertTypeName, String tierName, String userName, HttpServletRequest request) {
        String destType = AlertUtils.ALERT_USER_DESTINATION_NONE;
        try {
            String destination = Env.getAlertingServ(request).getActionDestination(alertTypeName, tierName, MessageType.SMTP, userName);

            if(destination != null) {
                destType = AlertUtils.ALERT_USER_DESTINATION_SMTP;
            }
            destination = Env.getAlertingServ(request).getActionDestination(alertTypeName, tierName, MessageType.SMS, userName);
            if(destination != null) {
                destType = AlertUtils.ALERT_USER_DESTINATION_NONE.equals(destType) ? AlertUtils.ALERT_USER_DESTINATION_SMS : AlertUtils.ALERT_USER_DESTINATION_BOTH;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return destType;
    }
    
    
    private void includeSNMPDestinations(String alertTypeName, Node destinationsNode, boolean isNewAlertType, HttpServletRequest request) throws IOException {
        try {
            Document srcdoc = getDocument(request, "snmp/snmpconf.xml", "traps");
            
            Document doc = destinationsNode.getOwnerDocument();			
            Node traps = doc.createElement("snmp"); 
            destinationsNode.appendChild(traps);
            
            for(String destinationTag: destinationTypesMap.keySet()){
                addSNMPDestinations(alertTypeName, traps, isNewAlertType, srcdoc, destinationTag, request);
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }		
    }
    
    private void addSNMPDestinations(String alertTypeName, Node destinationsNode, boolean isNewAlertType, Document srcdoc, String destinationTag, HttpServletRequest request) throws IOException, NoSuchAlertTypeException {		
        String elementName =  destinationTypesMap.get(destinationTag);
        MessageType messageType = "trap".equals(elementName) ? MessageType.SNMP : MessageType.AUDIO_VISUAL;
        
        Document doc = destinationsNode.getOwnerDocument();
        Node traps = destinationsNode;
        
        Collection<String> dest_tier1;
        Collection<String> dest_tier2;
        Collection<String> dest_tier3;
        if(!isNewAlertType){
            AlertService alertService = Env.getAlertingServ(request);
            dest_tier1 = new HashSet<String>(alertService.getActionDestinations(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER1_NAME, messageType));
            dest_tier2 = new HashSet<String>(alertService.getActionDestinations(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER2_NAME, messageType));
            dest_tier3 = new HashSet<String>(alertService.getActionDestinations(alertTypeName, AlertUtils.ALERT_PRIORITY_TIER3_NAME, messageType));
        } else {
            dest_tier1 = Collections.emptyList();
            dest_tier2 = Collections.emptyList();
            dest_tier3 = Collections.emptyList();
        }
        Collection<String> existingAddrs = new ArrayList<String>();
        
        //add destinations which are in the 'snmpconf.xml' list
        NodeList destNodes = srcdoc.getElementsByTagName(destinationTag);
        for(int i = 0; i < destNodes.getLength(); i++) {
            Node destNode = destNodes.item(i);
            String ip = destNode.getAttributes().getNamedItem("remote-ip").getNodeValue();
            String port = destNode.getAttributes().getNamedItem("remote-port").getNodeValue();
            String addr = ip + ":" + port;
            existingAddrs.add(addr);
            Node destination = doc.createElement(elementName);
            XMLHelper.setNodeAttribute(destination, "ip", ip);
            XMLHelper.setNodeAttribute(destination, "port", port);
            if(!isNewAlertType) {
                XMLHelper.setNodeAttribute(destination, "tier1", dest_tier1.contains(addr) ? "true" : "false");
                XMLHelper.setNodeAttribute(destination, "tier2", dest_tier2.contains(addr) ? "true" : "false");
                XMLHelper.setNodeAttribute(destination, "tier3", dest_tier3.contains(addr) ? "true" : "false");
            } else {
                XMLHelper.setNodeAttribute(destination, "tier1", "false");
                XMLHelper.setNodeAttribute(destination, "tier2", "false");
                XMLHelper.setNodeAttribute(destination, "tier3", "false");					
            }
            traps.appendChild(destination);
        }
        
        //add destinations which are in the alert type only (not in the 'snmpconf.xml')  
        if(!isNewAlertType) {				
            Set<String> addrs = new HashSet<String>();
            addrs.addAll(dest_tier1);
            addrs.addAll(dest_tier2);
            addrs.addAll(dest_tier3);
            addrs.removeAll(existingAddrs);
            
            for(String addr : addrs){
                String[] arr = addr.split(":");
                String ip = arr[0];
                String port = arr[1];
                Node trap = doc.createElement(elementName);
                XMLHelper.setNodeAttribute(trap, "ip", ip);
                XMLHelper.setNodeAttribute(trap, "port", port);
                
                XMLHelper.setNodeAttribute(trap, "tier1", dest_tier1.contains(addr) ? "true" : "false");
                XMLHelper.setNodeAttribute(trap, "tier2", dest_tier2.contains(addr) ? "true" : "false");
                XMLHelper.setNodeAttribute(trap, "tier3", dest_tier3.contains(addr) ? "true" : "false");
                traps.appendChild(trap);
            }
        }
    }
    
    private void includeAlertableTypes(Node alertTypeNode, HttpServletRequest request) throws ServletException {
        if(request.getParameter("includealertabletypes") == null) {
            return;
        }
        String dc_id = request.getParameter("dc_id");
        LOG.info("Getting available, allowed types of existing children for DC with id = " + dc_id);
        
        Document doc = alertTypeNode.getOwnerDocument();
        
        Collection<TO<?>> related_objs = null;
        
        try {
            Node alertableTypesNode = doc.createElement("alertabletypes");
            XMLHelper.setNodeAttribute(alertableTypesNode, "dc", dc_id);

            List<String> types = UIConfig.getAlertableObjectTypes();
            TO<?> dc_obj = TOFactory.getInstance().loadTO(dc_id);
            for(String nameType : types) {
                related_objs = Env.getEnv(request).getRelatedObjects(dc_obj, nameType, true);
                
                if(related_objs != null && !related_objs.isEmpty()) {
                    Node alertableType = doc.createElement("type");
                    XMLHelper.setNodeAttribute(alertableType, "name", nameType);
                    alertableTypesNode.appendChild(alertableType);
                }
            }

            // add DC type to alertable types
            Node alertableType = doc.createElement("type");
            XMLHelper.setNodeAttribute(alertableType, "name", EnvironmentTypes.TYPE_DC);
            alertableTypesNode.appendChild(alertableType);

            alertTypeNode.appendChild(alertableTypesNode);
            LOG.info("Types recieved");
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
    }
    
    private static Document getDocument(HttpServletRequest request, String fileName, String contentNodeName) throws IOException, ParserConfigurationException {
        Document doc = null;
        String strxml;
        
        try {
            FileWriter fileWritter = Env.getFileWritter();
            strxml = fileWritter.readFile(fileName);
            doc = XMLHelper.getDocument(strxml);
        } catch (FileNotFoundException e) {
            doc = XMLHelper.createNewXmlDocument("root");
            Node root = doc.getFirstChild();
            root.appendChild(doc.createElement(contentNodeName));
        }
        return doc;
    }
}
