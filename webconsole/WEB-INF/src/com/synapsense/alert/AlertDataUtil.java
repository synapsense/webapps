package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageTemplateRef;
import com.synapsense.dto.MessageTemplateType;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.session.exceptions.SessionExpiredException;
import com.synapsense.utilities.BaseHttpServlet;

public class AlertDataUtil extends BaseHttpServlet {
    private static final long serialVersionUID = 4476363094825424001L;
    private static final Log LOG = LogFactory.getLog(AlertDataUtil.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
        LOG.info("Start");
        String result = "true";
        String type = "unknown";
        String error = "-1";
        
        try {
            type = request.getParameter("type");			
            
            if(type.equals("deletealerttemplate")) {
                TO<?> templateId = TOFactory.getInstance().loadTO(request.getParameter("id"));
                error = deleteAlertTemplate(request, templateId, "true".equals(request.getParameter("forcedelete")));
                if(!error.equals("-1")) {
                    result = "false";
                }
            } else if(type.equals("savealerttemplate")) {
                error = saveAlertTemplate(request);
                if(!error.equals("-1")) {
                    result = "false";
                }
            }
        } catch(Exception e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }
        sendResponse(response, "<root><type>" + type + "</type><success>" + result + "</success><error>" + error + "</error></root>");
        LOG.info("Success type=" + type);
    }
    
    private String saveAlertTemplate(HttpServletRequest request) throws IOException, ParserConfigurationException, XPathExpressionException, EnvException, NoSuchAlertTypeException {
        String error = "-1";//No error
        
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        
        String name = request.getParameter("name");
        String messagetype = request.getParameter("notificationtype");
        String oldnotificationtype = request.getParameter("oldnotificationtype");
        String description = request.getParameter("description");
        String header = AlertMessageTemplateHelper.uiBodyToTemplateBody(request.getParameter("subject"));
        String body = AlertMessageTemplateHelper.uiBodyToTemplateBody(request.getParameter("body"));
        boolean isnewtemplate = "true".equals(request.getParameter("isnewtemplate")) ? true : false;
        boolean typechanged = !oldnotificationtype.equals(messagetype);
        boolean updatealerts = "true".equals(request.getParameter("updatealerts"));
        TO<?> templateId = null;
        if (!isnewtemplate) {
            templateId = TOFactory.getInstance().loadTO(request.getParameter("id"));
        }
        
        String currentUserName = (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
        if(currentUserName == null) {
            throw new SessionExpiredException("Session was destroyed");
        }
        
        TO<?> t = helper.getMessageTemplate(name); 
        if(t != null && !t.equals(templateId)) {
            return "1";//duplicate name
        }
        
        if(!updatealerts && !isnewtemplate && typechanged && helper.isTemplateUsedByAlertType(templateId)) {
            //If alerts uses this template, ask user about update to custom template
            return "2";
        }
                
        if(isnewtemplate) {
            templateId = helper.createMessageTemplate(name, messagetype, description, header, body, MessageTemplateType.USER_DEFINED);
        } else {
            if(typechanged) {
                Collection<AlertType> cat = helper.getAlertTypesUsesThisTemplate(templateId);
                for(AlertType alertType : cat) {
                    MessageType oldMessageType = helper.numberStringToMessageType(oldnotificationtype);
                    String srcHeader = helper.getTemplatePropertyValue(templateId, "header");
                    String srcBody = helper.getTemplatePropertyValue(templateId, "body");
                    String srcDescription = helper.getTemplatePropertyValue(templateId, "description");
                    TO<?> newCustomTemplateId = helper.createMessageTemplate(null, oldnotificationtype, srcDescription, srcHeader, srcBody, MessageTemplateType.CUSTOM);
//TODO: setTemplate removes previous one?
//					Env.getAlertingServ(request).removeTemplate(alertType.getName(), oldMessageType);
                    MessageTemplate newMessageTemplate = new MessageTemplateRef(oldMessageType, newCustomTemplateId);
                    Env.getAlertingServ(request).setTemplate(alertType.getName(), newMessageTemplate);
                }
            }
            helper.setMessageTemplate(templateId, name, messagetype, description, header, body);				
        }
        return error;
    }
    
    public static String deleteAlertTemplate(HttpServletRequest request, TO<?> templateId, boolean forcedelete) throws IOException, ParserConfigurationException, XPathExpressionException, ObjectNotFoundException, NoSuchAlertTypeException {
        String error = "-1";
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        boolean templateinuse = helper.isTemplateUsedByAlertType(templateId);
        if(!forcedelete && templateinuse) {			
            return "1";
        }
        if(templateinuse) {
            Collection<AlertType> cat = helper.getAlertTypesUsesThisTemplate(templateId);
            String messageTypeStr = helper.getTemplatePropertyValue(templateId, "messagetype");
            for(AlertType alertType : cat) {
                String srcHeader = helper.getTemplatePropertyValue(templateId, "header");
                String srcBody = helper.getTemplatePropertyValue(templateId, "body");
                String srcDescription = helper.getTemplatePropertyValue(templateId, "description");
                TO<?> newCustomTemplateId = helper.createMessageTemplate(null, messageTypeStr, srcDescription, srcHeader, srcBody, MessageTemplateType.CUSTOM);
                MessageTemplate newMessageTemplate = new MessageTemplateRef(helper.numberStringToMessageType(messageTypeStr), newCustomTemplateId);
                Env.getAlertingServ(request).setTemplate(alertType.getName(), newMessageTemplate);
            }			
        }
        helper.deleteMessageTemplate(templateId);
        return error;
    }
}
