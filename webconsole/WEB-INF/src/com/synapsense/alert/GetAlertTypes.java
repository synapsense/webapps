package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertType;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Constants;
import com.synapsense.utilities.XMLHelper;

public class GetAlertTypes extends BaseHttpServlet {

    private static final long serialVersionUID = -2694215900764220041L;
    private static final Log LOG = LogFactory.getLog(GetAlertTypes.class);

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        sendResponse(resp, getAlertTypesDoc(req));
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private Document getAlertTypesDoc(HttpServletRequest req) {
        String status = req.getParameter("status");
        int alertStatus = -1;
        if(status != null) {
            alertStatus = Integer.parseInt(status);
        }
        Collection<AlertType> alertTypes = Env.getAlertingServ(req).getAllAlertTypes();

        Document doc = null;
        
        try {
            doc = XMLHelper.createNewXmlDocument("alert_types");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return null;
        }

        Element root = doc.getDocumentElement();
        
        for (AlertType alertType: alertTypes) {
            
            String alertName= alertType.getName();
            String alertDisplayName= alertType.getDisplayName();
            if((alertStatus == Constants.ALERT_TYPE_STATUS_ACTIVE && alertType.isActive()) 
                    || (alertStatus == Constants.ALERT_TYPE_STATUS_DEACTIVE && !alertType.isActive()) 
                    || alertStatus == Constants.ALERT_TYPE_STATUS_ALL) {
                Node alertTypeNode = XMLHelper.createXmlNode(doc, "alert_type", new HashMap<String, String>());
                
                boolean isSystem = true;
                if (alertName.startsWith(Constants.CONSOLE_ALERT_PREFIX )) {
                    isSystem = false;
                }
                Node systemNode = XMLHelper.createXmlTextNode(doc, "system", String.valueOf(isSystem));
                alertTypeNode.appendChild(systemNode);

                Node nameNode = XMLHelper.createXmlTextNode(doc, "name", alertName);
                alertTypeNode.appendChild(nameNode);

                Node displayNameNode = XMLHelper.createXmlTextNode(doc, "displayName", alertDisplayName);
                alertTypeNode.appendChild(displayNameNode);
                    
                Node statusNode = XMLHelper.createXmlTextNode(doc, "status", (alertType.isActive()) ? String.valueOf(Constants.ALERT_TYPE_STATUS_ACTIVE) : String.valueOf(Constants.ALERT_TYPE_STATUS_DEACTIVE));
                alertTypeNode.appendChild(statusNode);
    
                Node descrNode = XMLHelper.createXmlTextNode(doc, "description", alertType.getDescription());
                alertTypeNode.appendChild(descrNode);
    
                Node headerNode = XMLHelper.createXmlTextNode(doc, "header", alertType.getDefaultHeader());
                alertTypeNode.appendChild(headerNode);
    
                Node bodyNode = XMLHelper.createXmlTextNode(doc, "body", alertType.getDefaultBody());
                alertTypeNode.appendChild(bodyNode);
    
                Node priorityNode = XMLHelper.createXmlTextNode(doc, "priority", "" + AlertUtils.priorityNameToInt(alertType.getPriority()));
                alertTypeNode.appendChild(priorityNode);
                    
                
                Node priorityNameNode = XMLHelper.createXmlTextNode(doc, "alert_priority_name", alertType.getPriority());
                alertTypeNode.appendChild(priorityNameNode);

                root.appendChild(alertTypeNode);
            }
        }
        
        return doc;
    }
}
