package com.synapsense.alert;

import java.io.IOException;
import java.util.Collection;
import java.util.Date;

import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.elementmodel.basic.ModelElement;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.DataRestrictor;

/**
 * Class for alert data retrieving. 
 * Request parameters:
 * id - environment object id
 * type -  environment object type
 * alertType - {"active" || "historical" }. If "active" or null, active alerts will be retrieved.
 *             If "historical", alerts betweenDates will be retrieved *             
 * UNUSED, will be deleted soon: mode - {"children" || "object"}. If "children", operations will be performed under environment object children.
 * @author ahorosh
 *
 */
public class GetAlerts extends BaseHttpServlet {	
    private static final long serialVersionUID = -8177915340910562827L;
    private static final Log LOG = LogFactory.getLog(GetAlerts.class);
    private static int maxAlertsNumberToShow = 10000;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        
        try {
            sendResponse(req, resp, getAlertsModel(req));
        } catch (Exception e) {
            throw new ServletException(e.getMessage(), e);
        }
        
        LOG.info(this.getServletName() + " is finished ...");
    }        
    
    private ModelElement getAlertsModel(HttpServletRequest request) throws ParserConfigurationException, ObjectNotFoundException, PropertyNotFoundException, UnableToConvertPropertyException, NamingException{
        String id = request.getParameter("id");        
        String alertType = request.getParameter("alertType"); //history, all, active, allAlertsHistory        
        String mode = request.getParameter("mode"); //children or object
        
        TO<?> obj = null;
        if ("history".equals(alertType) || "active".equals(alertType)) {
            if(LOG.isDebugEnabled()) {
                LOG.debug("Load <TO> for " + id);
            }
            obj = TOFactory.getInstance().loadTO(id);
        }
        
        mode = (mode == null) ? "" : mode;
        alertType = (alertType == null) ? "active" : alertType;

        Collection<Alert> alerts = null;
        
        AlertUtils utilObject = new AlertUtils(request, false);
        
        if("history".equals(alertType) || "allAlertsHistory".equals(alertType)){
            Date beginDate = new Date(Long.parseLong(request.getParameter("begin")));
            Date endDate = new Date(Long.parseLong(request.getParameter("end")));        	
            
            if("history".equals(alertType)){
                alerts = utilObject.getAlertsHistory(id, beginDate, endDate, true);
            }else if("allAlertsHistory".equals(alertType)){
                alerts = utilObject.getAllAlertsHistory(beginDate, endDate);
            }
            
        }else if("active".equals(alertType)){
            alerts = utilObject.getActiveAlerts(id, true);
        }else if("allActiveAlertsInSystem".equals(alertType)){
            alerts = utilObject.getAllActiveAlerts();
        } else if("status".equals(alertType)){        	
            String statuses = request.getParameter("statuses");
            Date beginDate = null;
            Date endDate = null;
            
            try {
                beginDate = new Date(Long.parseLong(request.getParameter("begin")));
                endDate = new Date(Long.parseLong(request.getParameter("end")));
            } catch (Exception e) {
                beginDate = null;
                endDate = null;
            }
            
            alerts = utilObject.getAlertsByStatus(id, statuses, beginDate, endDate, mode);
        }else if("alertById".equals(alertType)){
            Integer alertId = Integer.valueOf(request.getParameter("alertId"));        	
            return utilObject.getModelByAlert(utilObject.getAlertById(alertId, request));
        }
        
        int existingAlertsNum = alerts.size();
        alerts = DataRestrictor.decreaseCollectionToAllowedSize(alerts, maxAlertsNumberToShow);        
        ModelElement model;
        if("allActiveAlertsInSystem".equals(alertType)){
            model = utilObject.createModelByAllSystemAlerts(obj, alerts, request);
        }else{
            model = utilObject.createObjectModelByAlerts(obj, alerts, request);
        }        
        DataRestrictor.addRestrictionInfoToModel(model, existingAlertsNum, alerts.size());

        return model;
    }
}
