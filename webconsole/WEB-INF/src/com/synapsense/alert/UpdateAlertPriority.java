package com.synapsense.alert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.alert.utils.AlertUtils;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.PriorityTier;
import com.synapsense.service.AlertService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.activityLog.ESConfiguratorActivityLog;
import com.synapsense.utilities.es.ActivityLogConstans;
import com.synapsense.utilities.es.ComponentsNameConstants;

public class UpdateAlertPriority extends BaseHttpServlet {
    private static final long serialVersionUID = -6992417968424086927L;	
    private static final Log LOG = LogFactory.getLog(UpdateAlertPriority.class);
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
         doGet(req, resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
              throws ServletException, IOException {
        LOG.info(this.getServletName() + " is started ...");
        String reqStr = req.getParameter("TYPE");
        
        try {
            if(reqStr == null) {
                sendResponse(resp, "<result>" + ErrorMessages.UNKNOWN_REQUEST + "<\result>");
            } else if(reqStr.equals(RequestVariableNames.REQUEST_ON_LOAD_DATA)) {
                sendResponse(resp, getAlertIntervals(req));
            } else if(reqStr.equals(RequestVariableNames.REQUEST_ON_SAVE_DATA)) {
                sendResponse(resp, changeAlertIntervals(req));
            } else {
                sendResponse(resp, "<result>" + ErrorMessages.UNKNOWN_REQUEST + "<\result>");
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        } 
        LOG.info(this.getServletName() + " is finished ...");
    }
    
    private Document getAlertIntervals(HttpServletRequest request) {
        Document doc = null;
        AlertPriority alertPriority = null;
        Node rootNode = null;
        Node items = null;
        
        try {
            doc = XMLHelper.createNewXmlDocument("root");
            rootNode = doc.getDocumentElement();
            items = doc.createElement("formitems");
            rootNode.appendChild(items);
            
            Collection<String> priorityNames = Env.getAlertingServ(request).getAlertPrioritiesNames();	    	
            for(String priorityName : priorityNames) {
                alertPriority = Env.getAlertingServ(request).getAlertPriority(priorityName);
                List<PriorityTier> priorityTierList = alertPriority.getTiers();
                
                for(PriorityTier priorityTier : priorityTierList) {
                    String prefix = (alertPriority.getName() + "." + priorityTier.getName() + ".").toLowerCase();
                    
                    appendItemNode(items, prefix, "notificationAttempts", priorityTier.getNotificationAttempts());
                    appendItemNode(items, prefix, "notificationInterval", priorityTier.getInterval() / (1000*60));
                    appendItemNode(items, prefix, "sendEscalationNotification", priorityTier.isSendEscalation());
                    appendItemNode(items, prefix, "sendAcknowledgeNotification", priorityTier.isSendAck());
                    appendItemNode(items, prefix, "sendResolveNotification", priorityTier.isSendResolve());
                }
                String autoDismissInterval = getAutoDismiss(request, priorityName);
                appendItemNode(items, priorityName.toLowerCase() + ".", "autoDismissInterval", autoDismissInterval);
                
                appendItemNode(items, priorityName.toLowerCase() + ".", "red", alertPriority.isRedSignal());
                appendItemNode(items, priorityName.toLowerCase() + ".", "yellow", alertPriority.isYellowSignal());
                appendItemNode(items, priorityName.toLowerCase() + ".", "green", alertPriority.isGreenSignal());
                appendItemNode(items, priorityName.toLowerCase() + ".", "sound", alertPriority.isAudioSignal());
                appendItemNode(items, priorityName.toLowerCase() + ".", "audioDuration", alertPriority.getAudioDuration());
                appendItemNode(items, priorityName.toLowerCase() + ".", "lightDuration", alertPriority.getLightDuration());
                appendItemNode(items, priorityName.toLowerCase() + ".", "ntLog", alertPriority.isNtLog());
            }
            
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        return doc;
    }
    
    private void appendItemNode(Node formitemsNode, String itemNamePrefix, String itemName, Object itemValue) {
        Node itemNode = XMLHelper.appendNodeWithText(formitemsNode, "item", "" + itemValue.toString());
        XMLHelper.setNodeAttribute(itemNode, "submitname", itemNamePrefix + itemName);
    }
    
    private String changeAlertIntervals(HttpServletRequest request) {
        String result = "";
        
        try {
            Collection<String> priorityNames = Env.getAlertingServ(request).getAlertPrioritiesNames();	    	
            for(String priorityName : priorityNames) {
                List<PriorityTier> tiers = getPriorityTierListFromRequest(
                        priorityName, request);
                AlertPriority alertPriority = new AlertPriority(priorityName, Long.parseLong(request
                        .getParameter(priorityName.toLowerCase() + ".autoDismissInterval")) * 1000 * 60, tiers,
                        Boolean.parseBoolean(request.getParameter(priorityName.toLowerCase() + ".red")),
                        Boolean.parseBoolean(request.getParameter(priorityName.toLowerCase() + ".green")),
                        Boolean.parseBoolean(request.getParameter(priorityName.toLowerCase() + ".yellow")),
                        Long.parseLong(request.getParameter(priorityName.toLowerCase() + ".lightDuration")),
                        Boolean.parseBoolean(request.getParameter(priorityName.toLowerCase() + ".sound")),
                        Long.parseLong(request.getParameter(priorityName.toLowerCase() + ".audioDuration")),
                        Boolean.parseBoolean(request.getParameter(priorityName.toLowerCase() + ".ntLog")));
                Env.getAlertingServ(request).updateAlertPriority(priorityName, alertPriority);
            }

            result = ErrorMessages.UPDATED_ALERT_PRIORITY_INTERVAL_SUCCESS;
            ESConfiguratorActivityLog.logRecord(request,ComponentsNameConstants.COMPONENT_ALERT_MANAGEMENT, ActivityLogConstans.MODULE_ALERT_MANAGEMENT, "activity_log_control_alertmanagement_configuration_was_changed");
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UPDATE_ALERT_PRIORITY_INTERVAL_FAIL;
        }
        return "<result>" + result + "</result>";
    }
    
    private List<PriorityTier> getPriorityTierListFromRequest(String priorityName, HttpServletRequest request) {
        List<PriorityTier> result = new ArrayList<PriorityTier>();		
        Map<String, String> tier = getPriorityTierMapFromRequest(priorityName, AlertUtils.ALERT_PRIORITY_TIER1_NAME, request);
        
        PriorityTier priorityTier = new PriorityTier(
                                    AlertUtils.ALERT_PRIORITY_TIER1_NAME,
                                    Long.parseLong(tier.get("notificationInterval")) * (60 * 1000),
                                    Integer.parseInt(tier.get("notificationAttempts")),
                                    AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(priorityName) ? false : Boolean.parseBoolean(tier.get("sendEscalationNotification")),
                                    Boolean.parseBoolean(tier.get("sendAcknowledgeNotification")),
                                    Boolean.parseBoolean(tier.get("sendResolveNotification"))
                                    );
        result.add(priorityTier);
        
        if(!AlertUtils.ALERT_PRIORITY_NAME_INFORMATIONAL.equals(priorityName)) {
            tier = getPriorityTierMapFromRequest(priorityName, AlertUtils.ALERT_PRIORITY_TIER2_NAME, request);
            priorityTier = new PriorityTier(
                    AlertUtils.ALERT_PRIORITY_TIER2_NAME,
                    Long.parseLong(tier.get("notificationInterval")) * (60 * 1000),
                    Integer.parseInt(tier.get("notificationAttempts")),
                    Boolean.parseBoolean(tier.get("sendEscalationNotification")),
                    Boolean.parseBoolean(tier.get("sendAcknowledgeNotification")),
                    Boolean.parseBoolean(tier.get("sendResolveNotification"))
                    );
            result.add(priorityTier);
            
            tier = getPriorityTierMapFromRequest(priorityName, AlertUtils.ALERT_PRIORITY_TIER3_NAME, request);
            priorityTier = new PriorityTier(
                    AlertUtils.ALERT_PRIORITY_TIER3_NAME,
                    Long.parseLong(tier.get("notificationInterval")) * (60 * 1000),
                    Integer.parseInt(tier.get("notificationAttempts")),
                    false,
                    Boolean.parseBoolean(tier.get("sendAcknowledgeNotification")),
                    Boolean.parseBoolean(tier.get("sendResolveNotification"))
                    );
            result.add(priorityTier);
        }
        return result;
    }
    
    private Map<String, String> getPriorityTierMapFromRequest(String priorityName, String tierName, HttpServletRequest request) {
        Map<String, String> result = new HashMap<String, String>();
        
        Enumeration<String> enumeration = request.getParameterNames();
        while(enumeration.hasMoreElements()) {
            String paramName = enumeration.nextElement();
            String [] arr3part = paramName.split("\\.");
            if(arr3part.length == 3 && arr3part[0].equalsIgnoreCase(priorityName) && arr3part[1].equalsIgnoreCase(tierName)) {
                result.put(arr3part[2], request.getParameter(paramName));
            }
        }
        return result;
    }
    
    private String getAutoDismiss(HttpServletRequest request, String priorityName) {
        Long duration = new Long(43200);
        AlertService alertService = Env.getAlertingServ(request);
        AlertPriority alertPriority = alertService.getAlertPriority(priorityName);
        if (alertPriority != null) {
            duration = alertPriority.getDismissInterval() / 1000 / 60;
        }
        return duration.toString();
    }
}
