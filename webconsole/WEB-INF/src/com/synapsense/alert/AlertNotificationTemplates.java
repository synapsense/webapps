package com.synapsense.alert;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.utilities.BaseHttpServlet;

public class AlertNotificationTemplates extends BaseHttpServlet {
    private static final long serialVersionUID = 6052260083122480943L;
    private static final Log LOG = LogFactory.getLog(AlertNotificationTemplates.class);
    
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        LOG.info("Start");
        String result = "true";
        String type = "unknown";
        String responseString = null;
        
        try {
            type = request.getParameter("type");
            if(type.equalsIgnoreCase("loadall")) {
                boolean includecustom = "true".equals(request.getParameter("includecustom"));
                String filterbytype = request.getParameter("filterbytype");
                String alerTypeName = request.getParameter("alerttypename");
                boolean issystem = "true".equals(request.getParameter("issystem"));
                responseString = loadAllTemplates(request, filterbytype, includecustom, alerTypeName, issystem);				
            } else if(type.equalsIgnoreCase("loadtemplate")) {
                responseString = loadTemplateById(request, TOFactory.getInstance().loadTO(request.getParameter("id")));
            }
        } catch(Exception e) {
            result = "false";
            LOG.error("Failed type=" + type, e);
            sendResponse(response, "<root><type>" + type + "</type><success>" + result + "</success></root>");
            return;
        }

        try {
            if(responseString != null) {
                String responseInfo = "<type>" + type + "</type><success>" + result + "</success>";
                responseString = responseString.replaceFirst("<responseinfonode/>", responseInfo);		
                sendResponse(response, responseString);
            } else {
                sendResponse(response, "<root><type>" + type + "</type><success>" + result + "</success></root>");
            }
            LOG.info("Success type=" + type);
        } catch (Exception e) {
            LOG.error("Failed to send response.");
            e.printStackTrace();
        }
    }

    public static String loadAllTemplates(HttpServletRequest request, String filterbytype, boolean includecustom, String alertTypeName, boolean issystem) {
        StringBuilder sb = new StringBuilder();
        sb.append(getOpeningClosingString(true));
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        
        try {
            if(filterbytype != null) {
                sb.append(helper.getMessageTemplatesAsXmlStringByType(filterbytype, includecustom));
            } else {
                sb.append(helper.getMessageTemplatesAsXmlStringByType("" + MessageType.SMTP.code(), includecustom));
                sb.append(helper.getMessageTemplatesAsXmlStringByType("" + MessageType.SMS.code(), includecustom));
                sb.append(helper.getMessageTemplatesAsXmlStringByType("" + MessageType.SNMP.code(), includecustom));				
                sb.append(helper.getMessageTemplatesAsXmlStringByType("" + MessageType.CONSOLE.code(), includecustom));				
                sb.append(helper.getMessageTemplatesAsXmlStringByType("" + MessageType.NT_LOG.code(), includecustom));				
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        sb.append(getOpeningClosingString(false));
        return sb.toString();
    }
    
    public static String loadTemplateById(HttpServletRequest request, TO<?> templateId) {
        StringBuilder sb = new StringBuilder();
        sb.append(getOpeningClosingString(true));
        AlertMessageTemplateHelper helper = new AlertMessageTemplateHelper(request);
        
        sb.append(helper.getMessageTemplateAsXmlString(templateId));
        
        sb.append(getOpeningClosingString(false));		
        return sb.toString();
    }
    
    private static String getOpeningClosingString(boolean opening) {
        if(opening) {
            return "<root><templates>";
        } else {
            return "</templates><responseinfonode/></root>";
        }
    }
}
