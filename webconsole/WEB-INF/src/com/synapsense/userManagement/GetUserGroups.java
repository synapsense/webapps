package com.synapsense.userManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;

public final class GetUserGroups extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(AddUser.class);
    public static final List<String> WEB_CONSOLE_DEFAULT_PRIVS = new ArrayList<String>();
    static {
        WEB_CONSOLE_DEFAULT_PRIVS.add("ACCESS_WEBCONSOLE");
    };

    private UserManagementService ums = Env.getUserDAO();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.info(this.getServletName() + " is started ...");
        String actionParam = (String) request.getParameter(RequestVariableNames.ACTION);
        String result = "<result>" + ErrorMessages.UNABLE_TO_COMPLETE + "</result>";
        String res = "";
        if (actionParam.equals("getGroupsList")) {
            result = XMLHelper.getDocumentXml(getAllGroups(request));
        } else if (actionParam.equals("addGroup")) {
            res = createGroup(request);
            result = "<result>" + res + "</result>";
        } else if (actionParam.equals("editGroup")) {
            res = updateGroup(request);
            result = "<result>" + res + "</result>";
        } else if (actionParam.equals("getGroupData")) {
            result = XMLHelper.getDocumentXml(getGroup(request));
        } else if (actionParam.equals("deleteGroup")) {
            res = deleteGroup(request);
            result = "<result>" + res + "</result>";
        }

        sendResponse(response, result);

        LOG.info(this.getServletName() + " is finished ...");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    private Document getAllGroups(HttpServletRequest request) {
        Document doc = null;
        Collection<TO<?>> userGroups = ums.getAllGroups();

        try {
            doc = XMLHelper.createNewXmlDocument(UserManagementService.USER_GROUPS);
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        if (doc != null) {
            Node root = doc.getDocumentElement();
            if (!userGroups.isEmpty()) {
                Iterator<TO<?>> userItr = userGroups.iterator();
                List<TO<?>> groups = new ArrayList<>();
                while (userItr.hasNext()) {
                    TO<?> group = userItr.next();
                    groups.add(group);
                }
                
                Collections.sort(groups, new Comparator<TO<?>>() {
                    @Override public int compare(final TO<?> o1, final TO<?> o2) {
                        String name1 = (String) ums.getProperty(o1, UserManagementService.GROUP_NAME);
                        String name2 = (String) ums.getProperty(o2, UserManagementService.GROUP_NAME);
                    	
                        return name1.compareTo(name2);
                    }
                });

                for (TO<?> group : groups) {
                    Node groupNode = doc.createElement("group");
                    if (UserUtils.isRootGroup(group)) {
                        XMLHelper.setNodeAttribute(groupNode, "isRoot", "true");
                    }
                    if (group != null) {
                        String groupName = (String) ums.getProperty(group, UserManagementService.GROUP_NAME);
                        String groupDesc = (String) ums.getProperty(group, UserManagementService.GROUP_DESCRIPTION);
    
                        groupNode.appendChild(XMLHelper.createXmlTextNode(doc, UserManagementService.GROUP_NAME, groupName));
                        groupNode.appendChild(XMLHelper.createXmlTextNode(doc, UserManagementService.GROUP_DESCRIPTION, groupDesc));
                    }
                    root.appendChild(groupNode);
                }
            }
        }
        
        return doc;
    }

    private Document getGroup(HttpServletRequest request) throws ServletException {
        Document doc = null;
        String groupName = request.getParameter("groupName");
        String appName = this.getServletContext().getInitParameter("APP_NAME");

    	TO<?> userGroup = (!"".equals(groupName)) ? ums.getGroup(groupName) : null;

        try {
            doc = XMLHelper.createNewXmlDocument("group");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        if (doc != null) {
            Node root = doc.getDocumentElement();
            String name = "";
            String desc = "";
            if (userGroup != null) {
                name = (String) ums.getProperty(userGroup, UserManagementService.GROUP_NAME);
                desc = (String) ums.getProperty(userGroup, UserManagementService.GROUP_DESCRIPTION);
            }

            root.appendChild(XMLHelper.createXmlTextNode(doc, UserManagementService.GROUP_NAME, name));
            root.appendChild(XMLHelper.createXmlTextNode(doc, UserManagementService.GROUP_DESCRIPTION, desc));

            Collection<TO<?>> groupPrivileges = Collections.emptyList();
            if (userGroup != null) {
                try {
                    groupPrivileges = ums.getGroupPrivileges(userGroup);
                } catch (UserManagementException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
            } else {
                // add privileges that should be checked by default
                groupPrivileges = getDefaultPrivileges();
            }

            Map<String, Collection<TO<?>>> privGroupMap = new LinkedHashMap<String, Collection<TO<?>>>();

            List<TO<?>> privileges = new ArrayList<TO<?>>();
            privileges.addAll(ums.getAllPrivileges());

            // sort privileges by sortOrder
            Collections.sort(privileges, new Comparator<TO<?>>() {
                @Override
                public int compare(TO<?> p1, TO<?> p2) {
                    Object order1 = ums.getProperty(p1, UserManagementService.PRIVILEGE_SORT_ORDER);
                    Object order2 = ums.getProperty(p2, UserManagementService.PRIVILEGE_SORT_ORDER);
                    if (order1 == null && order2 == null) {
                        return 0;
                    } else if (order1 == null) {
                        return 1;
                    } else if (order2 == null) {
                        return -1;
                    } else {
                        if (order1 instanceof String) {
                            if (((String) order1).isEmpty()) {
                                return 1;
                            }
                            order1 = Integer.valueOf((String) order1, 10);
                        }

                        if (order2 instanceof String) {
                            if (((String) order2).isEmpty()) {
                                return -1;
                            }
                            order2 = Integer.valueOf((String) order2, 10);
                        }
                        
                        return ((Integer) order1).compareTo((Integer) order2);
                    }
                }
            });

            // separate privileges by categories
            List<String> groups = new ArrayList<>();
            for (TO<?> p : privileges) {
                String privGroupName = (String) ums.getProperty(p, UserManagementService.PRIVILEGE_TAG);
                if (privGroupName == null || privGroupName.isEmpty()) {
                    continue;
                }

                if (!privGroupMap.containsKey(privGroupName)) {
                    groups.add(privGroupName);
                    privGroupMap.put(privGroupName, new ArrayList<TO<?>>());
                }
                privGroupMap.get(privGroupName).add(p);
            }

            // Sort the privilege groups
            Collections.sort(groups, new Comparator<String>() {
                @Override public int compare(final String o1, final String o2) {
                    return o1.compareTo(o2);
                }
            });

            Node privilegesNode = doc.createElement("priv_groups");
            root.appendChild(privilegesNode);

            // write privileges into document
            for (String privGroupName : groups) {
                Element privilegeGroupNode = doc.createElement("PrivilegeGroup");
                privilegeGroupNode.setAttribute("name", privGroupName);
                privilegesNode.appendChild(privilegeGroupNode);

                Collection<TO<?>> privs = privGroupMap.get(privGroupName);
                for (TO<?> privilege : privs) {
                    Element privilegeNode = doc.createElement("privilege");
                    String pName = (String) ums.getProperty(privilege, UserManagementService.PRIVILEGE_NAME);
                    privilegeNode.setAttribute(UserManagementService.PRIVILEGE_NAME, pName);
                    Boolean allowed = groupPrivileges.contains(privilege);
                    privilegeNode.setAttribute("allowed", allowed.toString());
                    privilegeGroupNode.appendChild(privilegeNode);
                }
            }

        }
        
        return doc;
    }

    private List<TO<?>> getDefaultPrivileges() {
        List<TO<?>> defaultPrivileges = new ArrayList<>();

        Collection<TO<?>> allPrivileges = ums.getAllPrivileges();
        List<String> defaultPrivIds = WEB_CONSOLE_DEFAULT_PRIVS;

        for (TO<?> p : allPrivileges) {
            String pName = (String) ums.getProperty(p, UserManagementService.PRIVILEGE_NAME);
            if (defaultPrivIds.contains(pName)) {
                defaultPrivileges.add(p);
            }
        }

        return defaultPrivileges;
    }

    private String createGroup(HttpServletRequest request) throws ServletException {
        String status = ErrorMessages.SUCCESS;
        ArrayList<String> privilegesList = new ArrayList<String>();
        String groupName = request.getParameter("groupName");
        String groupDescr = request.getParameter("groupDescr");

        try {
            JSONObject privileges = new JSONObject(request.getParameter("privileges"));
            JSONArray objects = null;

            if (!privileges.isNull("privileges")) {
                objects = privileges.getJSONArray("privileges");
                for (int i = 0; i < objects.length(); i++) {
                    JSONObject object = objects.getJSONObject(i);
                    String privilegeName = object.getString("id");
                    boolean allowed = "true".equals(object.getString("allowed"));

                    if (allowed) {
                        privilegesList.add(privilegeName);
                    }
                }
            }
        } catch (JSONException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }

        try {
            TO<?> newGroup = ums.createGroup(groupName, groupDescr);

            if (!privilegesList.isEmpty()) {
                ums.setGroupPrivileges(newGroup, privilegesList);
            }
        } catch (UserManagementException e) {
            throw new ServletException(ErrorMessages.getText(ErrorMessages.DUPLICATE_GROUP_NAME), e);
        }

        return status;
    }

    private String updateGroup(HttpServletRequest request) throws ServletException {
        String status = ErrorMessages.SUCCESS;

        String oldGroupName = request.getParameter("oldGroupName");
        String groupName = request.getParameter("groupName");
        String groupDescr = request.getParameter("groupDescr");

        TO<?> group = ums.getGroup(oldGroupName);

        Map<String, Object> groupProps = CollectionUtils.newMap();
        groupProps.put(UserManagementService.GROUP_NAME, groupName);
        groupProps.put(UserManagementService.GROUP_DESCRIPTION, groupDescr);

        Set<String> privilegesList = new HashSet<String>();
        try {
            JSONObject privileges = new JSONObject(request.getParameter("privileges"));
            JSONArray objects = null;

            if (!privileges.isNull("privileges")) {
                objects = privileges.getJSONArray("privileges");
                try {
                    privilegesList = new HashSet<String>(Env.getUserDAO().getGroupPrivilegeIds(group));
                } catch (UserManagementException e) {
                    LOG.error(e.getLocalizedMessage(), e);
                }
                for (int i = 0; i < objects.length(); i++) {
                    JSONObject object = objects.getJSONObject(i);
                    String privilegeName = object.getString("id");
                    boolean allowed = "true".equals(object.getString("allowed"));

                    if (allowed) {
                        privilegesList.add(privilegeName);
                    } else {
                        privilegesList.remove(privilegeName);
                    }
                }
            }
            
        } catch (JSONException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }

        try {
            if (!privilegesList.isEmpty()) {
                ums.setGroupPrivileges(group, privilegesList);
            }

            ums.updateGroup(group, groupProps);
            
        } catch (UserManagementException e) {
            throw new ServletException(e.getLocalizedMessage(), e);
        }

        return status;
    }

    private String deleteGroup(HttpServletRequest request) throws ServletException {
        String status = ErrorMessages.SUCCESS;
        String groupName = request.getParameter("groupName");
        TO<?> group = ums.getGroup(groupName);
        if (group != null) {
            try {
                UserUtils.actionsOnGroupDeleting(group, request);
                ums.deleteGroup(group);
            } catch (UserManagementException e) {
                throw new ServletException(e.getLocalizedMessage(), e);
            }
        }
        return status;
    }
}