/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.lang.LangManager;

/**
 * @author David R. Inniss
 * 
 */
public final class DeleteUsers extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(DeleteUsers.class);

    /**
     * Respond to a GET request for the content produced by this servlet.
     * 
     * @param request
     *            The servlet request we are processing
     * @param response
     *            The servlet response we are producing
     * 
     * @exception IOException
     *                if an input/output error occurs
     * @exception ServletException
     *                if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        HttpSession session = request.getSession();
        String resultMessage = null;
        StringBuffer userNamesStrBuf = new StringBuffer();
        
        if (session == null) {
            resultMessage = ErrorMessages.SESSION_EXPIRED;
        } else {
            try {
                String currentUser = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
                
                if (currentUser == null) {
                    resultMessage = ErrorMessages.SESSION_EXPIRED;
                } else {
                    String userNamesStr = request
                            .getParameter(RequestVariableNames.USER_NAME);

                    String userNameArray[] = userNamesStr.split(",");
                    for (int i = 0; i < userNameArray.length; i++) {
                        if (userNameArray[i].equals(currentUser)) {
                            LOG.warn("Can not delete current user: " + currentUser);
                            sendResponse(response, "<result>"
                                            + ErrorMessages.CANT_DELETE_CURRENT_USER
                                            + "</result>");
                            return;
                        }
                    }

                    String lastAdmin = "";
                    for (int i = 0; i < userNameArray.length; i++) {
                        TO<?> userInformation = Env.getUserDAO().getUser(userNameArray[i]);

                        if(UserUtils.isLastAdmin(userInformation)) {
                            lastAdmin = userNameArray[i];
                            continue;
                        }
                        
                        userNamesStrBuf.append(Env.getUserDAO().getProperty(userInformation, UserManagementService.USER_NAME))
                                    .append(" [")
                                    .append(Env.getUserDAO().getProperty(userInformation, UserManagementService.USER_FIRST_NAME))
                                    .append(" ")
                                    .append(Env.getUserDAO().getProperty(userInformation, UserManagementService.USER_LAST_NAME))
                                    .append("] (")
                                    .append(userInformation.getID())
                                    .append(")");
                        
                        UserUtils.actionsOnUserDeleting(userInformation, request);
                        Env.getUserDAO().deleteUser(userInformation);
                    }

                    if ("".equals(lastAdmin)){
                        resultMessage = ErrorMessages.USER_DELETED;
                    } else {
                        resultMessage = ErrorMessages.CANT_DELETE_LAST_ADMIN;
                    }
                }
            } catch (Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
                resultMessage = ErrorMessages.UNABLE_TO_COMPLETE;
            }
        }

        if (resultMessage != null) {
            sendResponse(response, "<result>" + resultMessage + "</result>");
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }
    
    private String getWarnMessage(HttpServletRequest request, List<String> semsUsersList){
        String langID = LangManager.getCurrentLocaleId(request);
        return "-2|" + semsUsersList.toString() + " " + LangManager.getInstance().getLocaleString(langID, "message/USERS_CAN_NOT_BE_DELETED");
    }

}
