/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.CollectionUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;


/**
 * @author Alex Pakhunov
 *
 */
public final class EditUser extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(EditUser.class);
    
    
    /**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String resultMessage = editUserInfo(request);
        if (resultMessage != null){	
            sendResponse(response, "<result>" + resultMessage + "</result>");
            LOG.debug("GetUserInfoByUsername.Main : UserInfo\n___________\n" + resultMessage);
        } else {    		
            LOG.warn("GetUserInfoByUsername.Main : UserInfo\n___________\n NO USER INFO RETURNED...");
        }
    }    
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
        
        
    private String editUserInfo(HttpServletRequest request) {
        String result = null;
        
        String userName = request.getParameter(RequestVariableNames.USER_NAME);
        String oldUserName = request.getParameter(RequestVariableNames.OLD_USER_NAME);
        String pWord = request.getParameter(RequestVariableNames.PASSWORD);

        String changePwdStr = request.getParameter(RequestVariableNames.CHANGE_PASSWORD_FLAG);
        boolean changePwd = (changePwdStr != null) ? ("true".equals(changePwdStr)) : false;
        boolean changeUserName = !oldUserName.equals(userName);
        
        try {
            // should contain one more characters in first, middle and last name
            HttpSession session = request.getSession();
            String currentUser = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
            
            if (!oldUserName.equals(userName) && null != Env.getUserDAO().getUser(userName)){
                return ErrorMessages.DUPLICATE_USER_NAME;
            } else if(Env.getDefaultUser().equals(userName)) {
                return ErrorMessages.RESERVED_USER_NAME;
            } else {
                TO<?> user = Env.getUserDAO().getUser(oldUserName);
                Map<String, Object> userProps = CollectionUtils.newMap();
                userProps.put(UserManagementService.USER_NAME, request.getParameter(RequestVariableNames.USER_NAME));
                userProps.put(UserManagementService.USER_FIRST_NAME, request.getParameter(RequestVariableNames.FIRST_NAME));
                userProps.put(UserManagementService.USER_LAST_NAME, request.getParameter(RequestVariableNames.LAST_NAME));
                String middleI = request.getParameter(RequestVariableNames.MIDDLE_INITIAL);
                middleI = (middleI != null) ? middleI.toUpperCase() : "";
                userProps.put(UserManagementService.USER_MIDDLE_INITIAL, middleI);
                userProps.put(UserManagementService.USER_TITLE, request.getParameter(RequestVariableNames.TITLE));
                userProps.put(UserManagementService.USER_PRIMARY_PHONE, request.getParameter(RequestVariableNames.PRIMARY_PHONE));
                userProps.put(UserManagementService.USER_SECONDARY_PHONE, request.getParameter(RequestVariableNames.SECONDARY_PHONE));
                userProps.put(UserManagementService.USER_PRIMARY_EMAIL, request.getParameter(RequestVariableNames.PRIMARY_EMAIL));
                userProps.put(UserManagementService.USER_SECONDARY_EMAIL, request.getParameter(RequestVariableNames.SECONDARY_EMAIL));
                userProps.put(UserManagementService.USER_SMS_ADDRESS, request.getParameter(RequestVariableNames.SMS_ADDRESS));
                
                Env.getUserDAO().updateUser(user, userProps);
                
                if (changePwd || changeUserName){//bug #2811 and bug #4999 - update password&username in session for current user
                    // if user changes its own password
                    if (currentUser.equalsIgnoreCase(oldUserName)) {
                        if(changeUserName){
                            session.setAttribute(SessionVariableNames.SESSION_USER, userName);
                        }
                        if(changePwd) {
                            session.setAttribute(SessionVariableNames.SESSION_PASSWORD, pWord);
                            Env.getUserDAO().setPassword(user, pWord);

                            request.logout(); // logout
                            request.login(userName, pWord); // re-login
                        }
                    }  else { // if user changes other user password no need to re-login the current user but we still have to update password
                        if(changePwd) {
                            Env.getUserDAO().setPassword(user, pWord);
                        }
                    }
                }
                
                String groupsStr = request.getParameter("groups");
                if(groupsStr != null && !"".equals(groupsStr)) {
                    JSONObject groups = new JSONObject(groupsStr);
                    JSONArray objects = null;
                    if(!groups.isNull("groups")) {
                        objects = groups.getJSONArray("groups");
                        ArrayList<String> groupsList = new ArrayList<String>();
                        for(int i = 0; i < objects.length(); i++) {
                            JSONObject object = objects.getJSONObject(i);
                            String groupName = object.getString("id");
                            boolean ingroup = "true".equals(object.getString("ingroup"));
                            boolean isRoot = "true".equals(object.getString("isroot"));
                            if(isRoot && UserUtils.isLastAdmin(user) && !ingroup) {
                                return ErrorMessages.CANT_REMOVE_LAST_ADMIN_FROM_ROOT_GROUP;
                            }
                            if(ingroup) {
                                groupsList.add(groupName);
                            }					
                        }
                        Env.getUserDAO().setUserGroups(user, groupsList);
                    }
                }
                
                String preferencesStr = request.getParameter(RequestVariableNames.PREFERENCES);
                if(preferencesStr != null && !"".equals(preferencesStr)) {
                    JSONObject preferences = new JSONObject(preferencesStr);
                    JSONArray objects = null;
                    
                    if(!preferences.isNull("preferences")) {
                        objects = preferences.getJSONArray("preferences");
                        for(int i = 0; i < objects.length(); i++) {
                            JSONObject object = objects.getJSONObject(i);
                            String prefName = object.getString("name");
                            Object value = object.get("value");
                            //to make it consistent with laszlo json parser we have to quote string
                            String prefValue;
                            if (value instanceof String) {
                                prefValue = "\"" + value.toString() + "\"";
                            } else {
                                prefValue = value.toString();
                            }
                            UserPreference.set(user, prefName, prefValue, Env.getUserDAO());
                        }
                    }
                }
                
                result = ErrorMessages.USER_UPDATED;
            }
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        }

        return result;
    }         
 }
