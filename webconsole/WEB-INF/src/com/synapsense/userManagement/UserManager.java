
/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.es.ActivityLogConstans;


/**
 * @author David R. Inniss
 *
 */
public final class UserManager extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(UserManager.class);

    /**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        String actionParam = (String) request.getParameter(RequestVariableNames.ACTION);
        
        response.setHeader("Pragma", "no-cache");
        response.setHeader("CACHE-CONTROL", "NO-CACHE");
        
        boolean result  = false;
        
        if (RequestVariableNames.ACTION_CLOSE_USER_SESSION.equals(actionParam)){
            result = closeUserSession(request);
        } else if (RequestVariableNames.ACTION_UPDATEPWD.equals(actionParam)){
            result = updateUserPassword(request);
        } else if (RequestVariableNames.ACTION_SAVE_USER_PREF.equals(actionParam)) {
            result = saveUserPreference(request);
        }
        sendResponse(response, "<result>"+ result + "</result>");	 	
    }	
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    private boolean updateUserPassword(HttpServletRequest request) {		
        HttpSession session = request.getSession();
        String currentUserName = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> currentUser = Env.getUserDAO().getUser(currentUserName);
        String userName = (String)request.getParameter(RequestVariableNames.USER_NAME);
        TO<?> user = Env.getUserDAO().getUser(userName);
        
        if(currentUser == null) { // current user was deleted!
            return closeUserSession(request);
        }
        
        if (UserUtils.hasPrivilege(currentUser, "CREATE_EDIT_USERS") || (currentUserName.equalsIgnoreCase(userName))) {
            String pword = (String)request.getParameter(RequestVariableNames.NEWPASSWORD);
                
            try {
                
                Env.getUserDAO().setPassword(user, pword);
                if (currentUserName.equalsIgnoreCase(userName)) {
                    session.setAttribute(SessionVariableNames.SESSION_PASSWORD, pword);

                    request.logout(); // logout
                    Thread.sleep(2000);
                    request.login(userName, pword); // re-login
                }
                
                return true;
            } catch(UserManagementException e) {
                LOG.error(e.getLocalizedMessage(), e);
                return false;
            } catch(Exception e) {
                LOG.error(e.getLocalizedMessage(), e);
                return false;
            } 
        } else {
            return false;
        }
    }
  
    private boolean closeUserSession(HttpServletRequest request){		
        boolean closeSessionSuccess = true;
        HttpSession session = request.getSession();
        
        try{
            String username = (String)request.getSession().getAttribute(SessionVariableNames.SESSION_USER);
            if (username != null) {
                Env.getActivityLogServ(request).addRecord(new ActivityRecord(username, ActivityLogConstans.MODULE_USER_MANAGEMENT, ActivityLogConstans.ACTION_USER_LOGOUT, LocalizationUtils.getLocalizedString("activity_log_control_user_logged_off")));
            }
            session.invalidate();
        } catch(Exception ex){
            closeSessionSuccess = false;
            LOG.error(ex.getMessage());
        }
                
        LOG.info("UserManager.CloseUserSession : Session closed and discarded");
        return closeSessionSuccess;		
    }
    
    private boolean saveUserPreference(HttpServletRequest request) {
        boolean result = true;
        HttpSession session = request.getSession();
        String currentUser = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        String prefName = request.getParameter(RequestVariableNames.USER_PREF_NAME);
        String prefValue = request.getParameter(RequestVariableNames.USER_PREF_VALUE);

        TO<?> user = Env.getUserDAO().getUser(currentUser);
        UserPreference.set(user, prefName, prefValue, Env.getUserDAO());
        
        return result;
    }
}

