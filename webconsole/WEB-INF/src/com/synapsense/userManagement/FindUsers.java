/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.UserManagementService;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.matcher.ConsoleMatcher;


/**
 * @author Alex Pakhunov
 *
 */
public final class FindUsers extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(FindUsers.class);

    /**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        
        Document usersDoc = getUsersDoc(request);
        if (usersDoc != null){	
            sendResponse(response, usersDoc);
        } else {    		
            LOG.warn("Find Users: NO USERS RETURNED...");
        }    	
        
    }    
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
    
    /**
     * Retrieve all users
     * @param request
     * @return
     */
    private Document getUsersDoc(HttpServletRequest request){
        Document doc = null;
        try {
            doc = XMLHelper.createNewXmlDocument("users");
            Node root = doc.getDocumentElement();
            UserManagementService ums = Env.getUserDAO();
            Collection<TO<?>> users = ums.getAllUsers();
            String searchBy = request.getParameter(RequestVariableNames.SEARCH_BY);
            String searchCriteria = request.getParameter(RequestVariableNames.SEARCH_CRITERIA);
            

            for (TO<?> user: users) {
                String propName = "";
                String filterStr = "";
                if (RequestVariableNames.SEARCH_BY_USERNAME.equals(searchBy)){
                    propName = UserManagementService.USER_NAME;
                } else if (RequestVariableNames.SEARCH_BY_LASTNAME.equals(searchBy)){
                    propName = UserManagementService.USER_LAST_NAME;
                } else if (RequestVariableNames.SEARCH_BY_FIRSTNAME.equals(searchBy)){
                    propName = UserManagementService.USER_FIRST_NAME;
                } else if (RequestVariableNames.SEARCH_BY_PHONE.equals(searchBy)){
                    propName = UserManagementService.USER_PRIMARY_PHONE;
                } else if (RequestVariableNames.SEARCH_BY_EMAIL.equals(searchBy)){
                    propName = UserManagementService.USER_PRIMARY_EMAIL;
                }				

                if (!propName.isEmpty()) {
                    filterStr = (String) ums.getProperty(user,  propName);
                    //make searching case insensitive
                    filterStr = filterStr.toLowerCase();
                }

                searchCriteria = searchCriteria == null ? null : searchCriteria.toLowerCase();
                
                if (searchCriteria != null && !ConsoleMatcher.patternMatches(filterStr, searchCriteria)) {
                    continue;
                }

                Node userNode = doc.createElement("user");
                Map<String, Object> userProps = ums.getUserInformation(user);
                for (Map.Entry<String, Object> prop : userProps.entrySet()) {
                    String key = prop.getKey();
                    if (!UserManagementService.USER_GROUPS.equals(key)) {
                        Object val = prop.getValue();
                        val = val != null ? val.toString() : "";
                        userNode.appendChild(XMLHelper.createXmlTextNode(doc, key, (String) val));                
                    }
                }
                
                userNode.appendChild(XMLHelper.createXmlTextNode(doc, "name", userProps.get(UserManagementService.USER_LAST_NAME) + ", " + userProps.get(UserManagementService.USER_FIRST_NAME)));
                userNode.appendChild(XMLHelper.createXmlTextNode(doc, UserManagementService.USER_GROUPS, getUserGroups(user, ums)));

                root.appendChild(userNode);
            }
            
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return doc;
    }
    
    private String getUserGroups(TO<?> user, UserManagementService ums) {
        String res = "";
        try {
            Collection<String> groupsList = ums.getUserGroupIds(user);
            res = groupsList.toString();
        } catch (UserManagementException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        return res.substring(1, res.length()-1);
    }
}
