/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.util.CollectionUtils;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;

/**
 * @author Alex Pakhunov
 *
 */
public final class AddUser extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(AddUser.class);
    
    /**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }    
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String resultMessage = addNewUser(request);
        sendResponse(response, "<result>" + resultMessage + "</result>");
    }
    
    private String addNewUser(HttpServletRequest request) {
        
        String result = null;

        Map<String, Object> userProps = CollectionUtils.newMap();
        userProps.put(UserManagementService.USER_NAME, request.getParameter(RequestVariableNames.USER_NAME));
        userProps.put(UserManagementService.USER_FIRST_NAME, request.getParameter(RequestVariableNames.FIRST_NAME));
        userProps.put(UserManagementService.USER_LAST_NAME, request.getParameter(RequestVariableNames.LAST_NAME));
        String middleI = request.getParameter(RequestVariableNames.MIDDLE_INITIAL);
        middleI = (middleI != null) ? middleI.toUpperCase() : "";
        userProps.put(UserManagementService.USER_MIDDLE_INITIAL, middleI);
        userProps.put(UserManagementService.USER_TITLE, request.getParameter(RequestVariableNames.TITLE));
        userProps.put(UserManagementService.USER_PRIMARY_PHONE, request.getParameter(RequestVariableNames.PRIMARY_PHONE));
        userProps.put(UserManagementService.USER_SECONDARY_PHONE, request.getParameter(RequestVariableNames.SECONDARY_PHONE));
        userProps.put(UserManagementService.USER_PRIMARY_EMAIL, request.getParameter(RequestVariableNames.PRIMARY_EMAIL));
        userProps.put(UserManagementService.USER_SECONDARY_EMAIL, request.getParameter(RequestVariableNames.SECONDARY_EMAIL));
        userProps.put(UserManagementService.USER_SMS_ADDRESS, request.getParameter(RequestVariableNames.SMS_ADDRESS));
        
        String userName = request.getParameter(RequestVariableNames.USER_NAME);
        String pword = request.getParameter(RequestVariableNames.PASSWORD);

        try {
            if(Env.getDefaultUser().equals(userName)) {
                return ErrorMessages.RESERVED_USER_NAME;
            }
            
            if(null != Env.getUserDAO().getUser(userName)) {
                return ErrorMessages.DUPLICATE_USER_NAME;
            }
            
            String groupsStr = request.getParameter("groups");
            ArrayList<String> groupsList = new ArrayList<String>();
            if(groupsStr != null && !"".equals(groupsStr)) {
                JSONObject groups = new JSONObject(groupsStr);
                JSONArray objects = null;
                
                if(!groups.isNull("groups")) {
                    objects = groups.getJSONArray("groups");
                    for(int i = 0; i < objects.length(); i++) {
                        JSONObject object = objects.getJSONObject(i);
                        String groupName = object.getString("id");
                        boolean ingroup = "true".equals(object.getString("ingroup"));
                        
                        if(ingroup) {
                            groupsList.add(groupName);
                        }					
                    }
                }
            }

            TO<?> newUser = Env.getUserDAO().createUser(userName, pword, groupsList);
            
            if (newUser != null) {
                Env.getUserDAO().updateUser(newUser, userProps);
                
                String preferencesStr = request.getParameter(RequestVariableNames.PREFERENCES);
                if(preferencesStr != null && !"".equals(preferencesStr)) {
                    JSONObject preferences = new JSONObject(preferencesStr);
                    JSONArray objects = null;
                    
                    if(!preferences.isNull("preferences")) {
                        objects = preferences.getJSONArray("preferences");
                        for(int i = 0; i < objects.length(); i++) {
                            JSONObject object = objects.getJSONObject(i);
                            String prefName = object.getString("name");
                            Object value = object.get("value");
                            //to make it consistent with laszlo json parser we have to quote string
                            String prefValue;
                            if (value instanceof String) {
                                prefValue = "\"" + value.toString() + "\"";
                            } else {
                                prefValue = value.toString();
                            }
                            UserPreference.set(newUser, prefName, prefValue, Env.getUserDAO());
                        }
                    }
                }
                
                result = ErrorMessages.USER_ADDED;
            } else {
                LOG.error("Unable to create user.");
            }
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return result;
    }				
}
