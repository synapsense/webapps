package com.synapsense.userManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UserManagementException;
import com.synapsense.service.Environment;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.Configurator;
import com.synapsense.utilities.MobileDeviceUtils;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.es.EnvironmentTypes;
import com.synapsense.utilities.lang.LangManager;

public class UserAuthenticator extends BaseHttpServlet {

    private static final long serialVersionUID = 6917802155232434793L;
    private static final Log LOG = LogFactory.getLog(UserAuthenticator.class);

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String actionParam = (String) request
                .getParameter(RequestVariableNames.ACTION);

        response.setHeader("Pragma", "no-cache");
        response.setHeader("CACHE-CONTROL", "NO-CACHE");

        if (RequestVariableNames.ACTION_EMBEDED_AUTHENTICATE
                .equals(actionParam)) {

            HttpSession session = request.getSession();
            session.setAttribute(SessionVariableNames.SESSION_USER,
                    (String) request
                            .getParameter(RequestVariableNames.USER_NAME));
            session.setAttribute(SessionVariableNames.SESSION_PASSWORD,
                    (String) request
                            .getParameter(RequestVariableNames.PASSWORD));
            session.setAttribute(SessionVariableNames.SESSION_AUTOLOGIN, "true");

            String redUrl = "console.html?jsessionid=" + java.net.URLEncoder.encode(session.getId(), "UTF-8");
            response.sendRedirect(redUrl);
        } else if (RequestVariableNames.ACTION_AUTHENTICATE.equals(actionParam)) {
            TO<?> u = null;
            StringBuilder msgBuilder = new StringBuilder();
            u = Authenticator.authenticateUser(request, msgBuilder);
            String msg = msgBuilder.toString();

            HttpSession session = request.getSession();
            if (u != null) {
                if(!UserUtils.hasPrivilege(u, "ACCESS_WEBCONSOLE")) {
                    Document failDoc = null;
                    try {
                        failDoc = XMLHelper.createNewXmlDocument("authentication");
                        Element root = failDoc.getDocumentElement();
                        root.appendChild(XMLHelper.createXmlTextNode(failDoc, "result", "false"));
                        root.appendChild(XMLHelper.createXmlTextNode(failDoc, "reason", "message/authentication_failed"));
                        if (failDoc != null) {
                            session.invalidate();
                            sendResponse(response, failDoc);
                        }
                    } catch (ParserConfigurationException e) {
                        LOG.error(e.getLocalizedMessage(), e);
                    }
                    return;
                }
                
                String userName = (String) Env.getUserDAO().getProperty(u, UserManagementService.USER_NAME);
                session.setAttribute(SessionVariableNames.SESSION_USER, userName);

                String timeZone = UsersDataOptions.getTzIdFromRequest(request);
                if (timeZone != null) {
                    UserPreference.set(u,
                            UserPreference.TIME_ZONE,
                            "\"" + timeZone + "\"", Env.getUserDAO());
                }
                
                UserPreference.updateDefaultPrefs(u, Env.getUserDAO());

                Document doc = getUserDoc(u, request);
//				String JSESSIONID = request.getSession(false).getId();
//
//				String newCokies = "JSESSIONID=" + JSESSIONID;
//				response.setHeader("Set-Cookie", newCokies);
                
                try{
                    if(UserUtils.hasRootPrivilege(u)){
                        Configurator.updateConfiguration(request);
                    }
                }catch(Exception e){//just keep exception and go
                    LOG.error(e.getLocalizedMessage(), e);
                }
                
                if (doc != null) {
                    sendResponse(response, doc);
                }
            } else {
                String us = (String) session
                        .getAttribute(SessionVariableNames.SESSION_USER);
                String p = (String) session
                        .getAttribute(SessionVariableNames.SESSION_USER);
                String autoLogin = (String) session
                        .getAttribute(SessionVariableNames.SESSION_AUTOLOGIN);
                // invalidate the session. user is not authorized
                session.invalidate();
                
                String result = "<authentication><result>false</result><login>" + 
                us + "</login><password>" + p + "</password><auto_login>" + 
                    "true".equals(autoLogin) + "</auto_login><reason>" + msg + "</reason></authentication>";
                sendResponse(response, result);
            }
            return;
        }
        sendResponse(response, "<result>" + false + "</result>");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        doGet(request, response);
    }
    
    private Document getUserDoc(TO<?> user, HttpServletRequest req) {
        Document doc = null;
        try {
            doc = XMLHelper.createNewXmlDocument("authentication");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return doc;
        }
        
        Element root = doc.getDocumentElement();
        
        Node resNode = XMLHelper.createXmlTextNode(doc, "result", "true");
        root.appendChild(resNode);
        
        Node userNameNode = XMLHelper.createXmlTextNode(doc, "user", (String)req.getSession().getAttribute(SessionVariableNames.SESSION_USER));
        root.appendChild(userNameNode);
        
        Node userPswdNode = XMLHelper.createXmlTextNode(doc, "pass", (String)req.getSession().getAttribute(SessionVariableNames.SESSION_PASSWORD));
        root.appendChild(userPswdNode);
        
        Node userIdNode = XMLHelper.createXmlTextNode(doc, "userId", user.getID().toString());
        root.appendChild(userIdNode);
        
        Node servNameNode = XMLHelper.createXmlTextNode(doc, "serverName", System.getProperty("jboss.server.name"));
        root.appendChild(servNameNode);
        
        Node jsessionidNode = XMLHelper.createXmlTextNode(doc, "jsessionid", req.getSession().getId());
        root.appendChild(jsessionidNode);
        
        addVersionInfo(doc, req);
        
        try {
            Node mobDevNameNode = XMLHelper.createXmlTextNode(doc, "showMobileManagement",
                    String.valueOf(MobileDeviceUtils.isMobileDevicesPresent(req)));
            root.appendChild(mobDevNameNode);
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }
        
        Document prefsNode = UserPreference.getPrefsDocument(user, Env.getUserDAO());
        root.appendChild(doc.importNode(prefsNode.getDocumentElement(), true));
        
        Document privsNode = getPrivilegesDocument(user, Env.getUserDAO());
        root.appendChild(doc.importNode(privsNode.getDocumentElement(), true));
                
        return doc;
    }
    
    private void addVersionInfo(Document doc, HttpServletRequest req){
        Element rootElem = doc.getDocumentElement();
        String isVersionCompatible = "false";
        String message = "";
        String debugInfo = "";
        String dbVersionStr = "N/A";

        try {			
            Environment env = Env.getLocalEnv(req);
            TO<?> root = (env.getObjectsByType(EnvironmentTypes.TYPE_ROOT)).iterator().next();
            
            try {
                dbVersionStr = env.getPropertyValue(root, "version", String.class);
            } catch (Exception e1) {
                LOG.error(e1.getLocalizedMessage(), e1);
            }
            
            Collection<TO<?>> invalidDcs = new ArrayList<TO<?>>(1);
            Collection<TO<?>> validDcs = new ArrayList<TO<?>>(1);
            getInvalidAndValidDCs(invalidDcs, validDcs, req);
            
            if (!Env.dbVersion.equals(dbVersionStr) || (validDcs.isEmpty() && !invalidDcs.isEmpty())) {				
                LangManager lm = LangManager.getInstance();
                String locale = LangManager.getCurrentLocaleId(req);				

                String invalidDCsStr = lm.getLocaleString(locale, "message_there_is_no_any_incompatible_dc");
                if (!invalidDcs.isEmpty()) {
                    invalidDCsStr = getInvalidDCsInfoStr(invalidDcs, req);
                }
                
                message = lm.getLocaleString(locale, "message_versions_are_not_compatible", Env.dbVersion, Env.minModelVersion, Env.maxModelVersion, dbVersionStr, invalidDCsStr);
            } else {
                isVersionCompatible = "true";
                if (!invalidDcs.isEmpty()) {
                    LangManager lm = LangManager.getInstance();
                    String locale = LangManager.getCurrentLocaleId(req);
                    
                    message = lm.getLocaleString(locale, "message_some_dcs_are_not_compatible", getInvalidDCsInfoStr(invalidDcs, req));
                }
            }
            
        } catch (Exception e) {
            // something unexpected happened.
            isVersionCompatible = "false";
            message = "System configuration is not complete. Please complete all required steps or contact your system administrator."; // default message, just in case
            LOG.error(e.getLocalizedMessage(), e);
            
            try {
                LangManager lm = LangManager.getInstance();
                String locale = LangManager.getCurrentLocaleId(req);
                message = lm.getLocaleString(locale, "message_versions_check_failed", dbVersionStr, "N/A");
            } catch (Exception e1) {
                LOG.error(e1.getLocalizedMessage(), e1);
            }
        }
        
        Node versionNode = XMLHelper.createXmlTextNode(doc, "isVersionCompatible", isVersionCompatible);
        XMLHelper.setNodeAttribute(versionNode, "message", message);
        XMLHelper.setNodeAttribute(versionNode, "debugInfo", debugInfo);
        rootElem.appendChild(versionNode);
    }
    
    private static Collection<TO<?>> getInvalidAndValidDCs (Collection<TO<?>> invalidDcs, Collection<TO<?>> validDcs, HttpServletRequest req) {
        Environment env = Env.getLocalEnv(req);
        TO<?> root = (env.getObjectsByType(EnvironmentTypes.TYPE_ROOT)).iterator().next();

        Collection<TO<?>> dcs = env.getRelatedObjects(root, "DC", true);				
                        
        for (TO<?> dc : dcs) {
            Double dcVersion = null;
            try {
                String dcVersionStr = env.getPropertyValue(dc, "modelVersion", String.class);
                dcVersion = Double.valueOf(dcVersionStr);
            } catch (Exception e) {
                // something wrong with version number, just leave dc version as NULL
            }
            
            if (dcVersion == null ||
                !(Double.parseDouble(Env.minModelVersion) <= dcVersion && dcVersion <= Double.parseDouble(Env.maxModelVersion))) {
                invalidDcs.add(dc);
            } else {
                validDcs.add(dc);
            }
        }
        
        return invalidDcs;
    }
    
    private static String getInvalidDCsInfoStr (Collection<TO<?>> invalidDcs, HttpServletRequest req) throws ObjectNotFoundException, UnableToConvertPropertyException, PropertyNotFoundException {
        Environment env = Env.getLocalEnv(req);
        String invalidDCsStr = "";
        
        for (TO<?> dc : invalidDcs) {
            String dcVersionStr = "N/A";
            try {
                dcVersionStr = env.getPropertyValue(dc, "modelVersion", String.class);
            } catch (PropertyNotFoundException e) {
                // something wrong with version, just leave dc version as N/A
            }
            String dcName = env.getPropertyValue(dc, "name", String.class);
            
            if (!invalidDCsStr.isEmpty()) {
                invalidDCsStr += ", ";
            }
            invalidDCsStr += dcName + " (" + dcVersionStr + ")";
        }
                
        return invalidDCsStr;
    }
    
    private static Document getPrivilegesDocument(TO<?> user, UserManagementService userDAO) {
        Document doc = null;
        try {
            doc = XMLHelper.createNewXmlDocument("privileges");
        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return doc;
        }

        Collection<String> privs;
        try {
            privs = userDAO.getUserPrivilegeIds(user);

            Element root = doc.getDocumentElement();
            
            for (String priv : privs) {
                Node privNode = doc.createElement("privilege");
                ((Element)privNode).setAttribute("name", priv.toUpperCase());
                root.appendChild(privNode);
            }
        } catch (UserManagementException e) {
            LOG.error(e.getLocalizedMessage(), e);
            return doc;
        }	

        return doc;
        
    }
}