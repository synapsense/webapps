
/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.UserPreference;


public final class SaveUserPreference extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    
    /**
     * Respond to a GET request for the content produced by
     * this servlet.
     *
     * @param request The servlet request we are processing
     * @param response The servlet response we are producing
     *
     * @exception IOException if an input/output error occurs
     * @exception ServletException if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String reset = request.getParameter("reset");
        if (reset != null && reset.equals("true")) {
            HttpSession session = request.getSession(false);
            String currentUser = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
            TO<?> user = Env.getUserDAO().getUser(currentUser);
            UserPreference.removeAll(user, Env.getUserDAO());
            Env.setLanguage(request,
                    UserPreference.getString(user, "SavedLanguage", Env.getUserDAO()));
            Env.setUnitsSystem(
                    UserPreference.getString(user, "UnitsSystem", Env.getUserDAO()),
                    request);

            sendResponse(response,
                    UserPreference.getPrefsDocument(user, Env.getUserDAO()));
        } else {
            sendResponse(response, "<result>" + saveUserPreference(request) + "</result>");
        }
    }	
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
        
    private boolean saveUserPreference(HttpServletRequest request) throws ServletException {
        boolean result = true;
        HttpSession session = request.getSession(false);
        String currentUser = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        TO<?> user = Env.getUserDAO().getUser(currentUser);
        
        String params =  request.getParameter("params");
        
        try {
            JSONArray jsonArray = new JSONArray(params);
            for(int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                String prefName = obj.getString("name");
                Object value = obj.get("value");
                //to make it consistent with laszlo json parser we have to quote string
                String prefValue;
                if (value instanceof String) {
                    prefValue = "\"" + value.toString() + "\"";
                } else {
                    prefValue = value.toString();
                }
                UserPreference.set(user, prefName, prefValue, Env.getUserDAO());
            }
            
        } catch (JSONException e) {
            throw new ServletException(e.getMessage(), e);
        }
        
        return result;
    }
}

