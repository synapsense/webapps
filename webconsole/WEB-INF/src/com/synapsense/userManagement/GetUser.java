/**
 * 
 */
package com.synapsense.userManagement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserPreference;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.XMLHelper;


/**
 * @author Alex Pakhunov
 * 
 */
public final class GetUser extends BaseHttpServlet {

    public static final long serialVersionUID = 0;
    private static final Log LOG = LogFactory.getLog(GetUser.class);
    /**
     * Respond to a GET request for the content produced by this servlet.
     * 
     * @param request
     *            The servlet request we are processing
     * @param response
     *            The servlet response we are producing
     * 
     * @exception IOException
     *                if an input/output error occurs
     * @exception ServletException
     *                if a servlet error occurs
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        sendResponse(response, retUserInfo(request));
    }    
       
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }
        
        
    private Document retUserInfo(HttpServletRequest request) {
        
        Document doc = null;
        
        UserManagementService ums = Env.getUserDAO();
        try {
            HttpSession session = request.getSession();
            String currentUserName = (String) session.getAttribute(SessionVariableNames.SESSION_USER);
            TO<?> currentUser = ums.getUser(currentUserName);
            
            String userName = (String)request.getParameter(RequestVariableNames.USER_NAME);
            doc = XMLHelper.createNewXmlDocument("users");
            Node root = doc.getDocumentElement();
            Node userNode = doc.createElement("user");
            
            Node groupsNode = null;
            List<TO<?>> allGroups = new ArrayList<>();
            Collection<TO<?>> userGroups = new ArrayList<>();
            if(userName!=""){
                TO<?> user = ums.getUser(userName);		
                if(user != null) {
                    Map<String, Object> userProps = ums.getUserInformation(user);
                    for (Map.Entry<String, Object> prop : userProps.entrySet()) {
                        String key = prop.getKey();
                        if (!UserManagementService.USER_GROUPS.equals(key)) {
                            Object val = prop.getValue();
                            val = val != null ? val.toString() : "";
                            userNode.appendChild(XMLHelper.createXmlTextNode(doc, key, (String) val));                
                        }
                    }
                    userNode.appendChild(XMLHelper.createXmlTextNode(doc, "name", userProps.get(UserManagementService.USER_LAST_NAME) + ", " + userProps.get(UserManagementService.USER_FIRST_NAME)));
                    
                    Document prefsNode = UserPreference.getPrefsDocument(user, Env.getUserDAO());
                    userNode.appendChild(doc.importNode(prefsNode.getDocumentElement(), true));
                    
                    groupsNode = doc.createElement(UserManagementService.USER_GROUPS);
                    
                    userGroups = ums.getUserGroups(user);
                    
                    if (!UserUtils.hasRootPrivilege(currentUser)) {
                        allGroups.addAll(userGroups);
                        allGroups.addAll(ums.getUserGroups(currentUser));
                    } else {
                        allGroups.addAll(ums.getAllGroups());
                    }
                }
            } else {
                groupsNode = doc.createElement(UserManagementService.USER_GROUPS);
                
                if (!UserUtils.hasRootPrivilege(currentUser)) {
                    allGroups.addAll(ums.getUserGroups(currentUser));
                } else {
                    allGroups.addAll(ums.getAllGroups());
                }
            }
            
            if (groupsNode != null) {
                Collections.sort(allGroups, new Comparator<TO<?>>() {
                    @Override public int compare(final TO<?> o1, final TO<?> o2) {
                        String name1 = (String) ums.getProperty(o1, UserManagementService.GROUP_NAME);
                        String name2 = (String) ums.getProperty(o2, UserManagementService.GROUP_NAME);
                    	
                        return name1.compareTo(name2);
                    }
                });

                for(TO<?> group : allGroups){
                    addGroupNode(doc, groupsNode, group, ums, userGroups.contains(group));
                }
                
                userNode.appendChild(groupsNode);
            }

            root.appendChild(userNode);
        
        } catch(Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return doc;
    }
    
    private static final void addGroupNode(Document doc, Node groupsNode, 
            TO<?> group, UserManagementService ums, Boolean isUserInGroup) {
        Node groupNode = doc.createElement("group");
        String groupName = (String) ums.getProperty(group, UserManagementService.GROUP_NAME);
        String groupDesc = (String) ums.getProperty(group, UserManagementService.GROUP_DESCRIPTION);

        XMLHelper.setNodeAttribute(groupNode, UserManagementService.GROUP_NAME, groupName);
        XMLHelper.setNodeAttribute(groupNode, UserManagementService.GROUP_DESCRIPTION, groupDesc);
        XMLHelper.setNodeAttribute(groupNode, "isUserInGroup", isUserInGroup.toString());
        if(UserUtils.isRootGroup(group)) {
            XMLHelper.setNodeAttribute(groupNode, "isRoot", "true");
        }

        groupsNode.appendChild(groupNode);
    }
}
