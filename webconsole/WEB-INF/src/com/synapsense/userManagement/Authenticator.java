package com.synapsense.userManagement;

import javax.ejb.EJBAccessException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.service.UserManagementService;
import com.synapsense.service.impl.activitylog.ActivityRecord;
import com.synapsense.session.ApplicationWatch;
import com.synapsense.session.SessionVariableNames;
import com.synapsense.util.LocalizationUtils;
import com.synapsense.utilities.RequestVariableNames;
import com.synapsense.utilities.UserUtils;
import com.synapsense.utilities.es.ActivityLogConstans;

public class Authenticator {
    
    private static final Log LOG = LogFactory.getLog(Authenticator.class);
    
    public static TO<?> authenticateUser(HttpServletRequest request, StringBuilder msg){
        
        HttpSession session = request.getSession();
        String autologin = (String) session.getAttribute(SessionVariableNames.SESSION_AUTOLOGIN);
        String userName = (autologin == null) ? (String)request.getParameter(RequestVariableNames.USER_NAME) : (String) session.getAttribute(SessionVariableNames.SESSION_USER);
        String pword = (autologin == null) ? (String)request.getParameter(RequestVariableNames.PASSWORD) : (String) session.getAttribute(SessionVariableNames.SESSION_PASSWORD);
        
        session.invalidate();

        if(userName == null){
            return null;
        }
        
        request.getSession().setAttribute(SessionVariableNames.SESSION_USER, userName);
        request.getSession().setAttribute(SessionVariableNames.SESSION_PASSWORD, pword);

        LOG.info("UserManagement.AuthenticateUser : Authentication of " + userName + " in progress...");
        
        if (ApplicationWatch.isConfiguring()) {
            msg.append("message/maintenance_please_try_to_login_later");
            return null;
        }

        if(!Env.login(request)) {
            if(autologin != null && !autologin.isEmpty()) {
                msg.append("message/authentication_failed");
            }
            return null;
        }
        
        UserManagementService userDao = Env.getUserDAO();
        if (userDao == null) {
            msg.append("message/environment_server_is_not_accessible");
            LOG.info("UserManagement.AuthenticateUser : Environment server is inaccessible");
            return null;
        }
        
        try {
            //for the case when previous session was not closed
            request.logout();
        } catch (ServletException e1) {
            // ignore
        }

        TO<?> u;
        String errCode = "0";
        try {
            request.login(userName, pword);
            u = userDao.getUser(userName);
            if (u == null) {
                errCode = "2";
            }
        } catch (EJBAccessException e) {
            // user has not privileges
            errCode = "1";
            u = null;
        } catch (Exception e) {
            errCode = "2";
            u = null;
        }
        
        if (errCode == "0"){
            Env.getActivityLogServ().addRecord(new ActivityRecord(userName, ActivityLogConstans.MODULE_USER_MANAGEMENT, ActivityLogConstans.ACTION_USER_LOGIN, LocalizationUtils.getLocalizedString("activity_log_control_user_authenticated_into_the_application")));
            LOG.info("UserManager.AuthenticateUser : User [" + userName + "] successful");
            
            // Cleanup the permissions
            UserUtils.actionsOnUserLogon(request);
        } else if (errCode == "1"){
            Env.loginAsSynapUser();
            Env.getActivityLogServ().addRecord(new ActivityRecord(userName, ActivityLogConstans.MODULE_USER_MANAGEMENT, ActivityLogConstans.ACTION_USER_LOGIN_FAILED, LocalizationUtils.getLocalizedString("activity_log_control_user_authentication_refused", userName)));
            LOG.warn("UserManager.AuthenticateUser : User [" + userName + "] have not permission for log in...");
            msg.append("message/user_does_not_have_permission_to_access_");
        } else {
            Env.loginAsSynapUser();
            Env.getActivityLogServ().addRecord(new ActivityRecord(userName, ActivityLogConstans.MODULE_USER_MANAGEMENT, ActivityLogConstans.ACTION_USER_LOGIN_FAILED, LocalizationUtils.getLocalizedString("activity_log_control_user_authentication_refused", userName)));
            LOG.warn("UserManager.AuthenticateUser : User [" + userName + "] failed...");
            msg.append("message/authentication_failed");
        }
            
        return u;
    }
}
