package com.synapsense.liveimaging;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.ValueTO;
import com.synapsense.service.Environment;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.es.EnvironmentTypes;

public class LIConfig {

    public static final String LI_CONFIG_OBJECT_NAME = "LiveImaging";
    
    public static boolean isLiConfigExists(HttpServletRequest request, Integer esId) {
        return getLiConfigTO(request, esId) != null;
    }
    
    public static TO<?> getLiConfigTO(HttpServletRequest request, Integer esId) {
        TO<?> liConfigTO = null;
        
        try {
            Environment env = Env.getEnv(request);
            ValueTO [] vals = {new ValueTO("name", LI_CONFIG_OBJECT_NAME)};            
            Collection<TO<?>> ctos = env.getObjects(EnvironmentTypes.TYPE_CONFIGURATION_DATA,  vals);
            for (TO<?> to : ctos) {
                if ((to.getEsID() == null && esId == null) || (esId != null && esId.equals(to.getEsID()))) {
                    liConfigTO = to;
                    break;
            }            
            }
            
        } catch(Exception e) {
            LogFactory.getLog(LIConfig.class).error(e.getLocalizedMessage(), e);
            
        }
        return liConfigTO;
    }
    
    private static String getLiConfigString(HttpServletRequest request, Integer esId) {
        String liConfigString = null;
        TO<?> liConfigTO = null;
        
        try {
            liConfigTO = getLiConfigTO(request, esId);
            Environment env = Env.getEnv(request);
            if(liConfigTO != null) {
                liConfigString = env.getPropertyValue(liConfigTO, "config", String.class);
            }
        } catch(Exception e) {
            LogFactory.getLog(LIConfig.class).error(e.getLocalizedMessage(), e);            
        }

        return liConfigString;
    }
    
    public static String getHostFromConfig(HttpServletRequest request, Integer esId) {
        Document confDoc = null;
        
        String res = null;
        String config = getLiConfigString(request, esId);
        try {
            if(config != null && !config.isEmpty()) {
                confDoc = XMLHelper.getDocument(config);
                NodeList hostsList = confDoc.getElementsByTagName("li:webserver-ip");
                String host = (hostsList.getLength()>0) ? hostsList.item(0).getTextContent() : "localhost";
                NodeList portsList = confDoc.getElementsByTagName("li:webserver-port");
                String port = (portsList.getLength()>0) ? portsList.item(0).getTextContent() : "9091";
                res = "http://" + host + ":" + port + "/";
            } else {
                res = "http://localhost:9091/";
            }
        } catch(Exception e) {
            LogFactory.getLog(LIConfig.class).error(e.getLocalizedMessage(), e);
        }
        return res;
    }
}
