package com.synapsense.liveimaging;

public class ImageAttributes {
    private int dataclass;
    private int layer;

    public ImageAttributes(int dataclass, int layer) {
        super();
        this.dataclass = dataclass;
        this.layer = layer;
    }

    public int getDataclass() {
        return dataclass;
    }

    public int getLayer() {
        return layer;
    }
    
    @Override
    public String toString(){
        return packToString();
    }
    
    public String packToString(){
        return dataclass + "_" + layer;
    }
    
    public static ImageAttributes unpackFromString(String str){
        String [] arr = str.split("_");
        return new ImageAttributes(Integer.parseInt(arr[0]), Integer.parseInt(arr[1]));
    }
}
