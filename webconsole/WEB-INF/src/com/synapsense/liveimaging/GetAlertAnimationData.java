package com.synapsense.liveimaging;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.synapsense.config.UIConfig;
import com.synapsense.dataAccess.Env;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;
import com.synapsense.utilities.BaseHttpServlet;
import com.synapsense.utilities.ErrorMessages;
import com.synapsense.utilities.SynapDateFormat;
import com.synapsense.utilities.XMLHelper;
import com.synapsense.utilities.dataconverters.UsersDataOptions;
import com.synapsense.utilities.es.EnvironmentTypes;

public class GetAlertAnimationData extends BaseHttpServlet {
    public static final long serialVersionUID = 0;
    private static Log LOG = LogFactory.getLog(GetAlertAnimationData.class);

    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doGet(req, resp);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {
        LOG.info(this.getServletName() + " is started ...");
        String result = getDocumentForAlertAnimation(request);
        String[] resArr = result.split("\\|");

        if ("-1".equals(resArr[0]) || "0".equals(resArr[0])) {
            result = "<result>" + result + "</result>";
        }
        sendResponse(response, result);
        LOG.info(this.getServletName() + " is finished ...");
    }

    private Document prepareDocumentForAlertAnimation(String groupId,
            String type, String startTime, String endTime) {
        Document aniDoc = null;

        try {
            // make node for first animation cut
            aniDoc = XMLHelper.createNewXmlDocument("alert");
            Node groupDocRootNode = aniDoc.getFirstChild();

            Node groupIdNode = XMLHelper.createXmlTextNode(aniDoc, "groupId",
                    groupId);
            groupDocRootNode.appendChild(groupIdNode);

            Node typeNode = XMLHelper.createXmlTextNode(aniDoc, "type", type);
            groupDocRootNode.appendChild(typeNode);

            Node startTimeNode = XMLHelper.createXmlTextNode(aniDoc,
                    "startTime", startTime);
            groupDocRootNode.appendChild(startTimeNode);

            Node endTimeNode = XMLHelper.createXmlTextNode(aniDoc, "endTime",
                    endTime);
            groupDocRootNode.appendChild(endTimeNode);

        } catch (ParserConfigurationException e) {
            LOG.error(e.getLocalizedMessage(), e);
        }

        return aniDoc;
    }

    private String getDocumentForAlertAnimation(HttpServletRequest request) {
        String result = "";

        try {
            
            // NOTE: if alert is not OPEN, we can't retrieve it by its ID form AlertingService (using "getAlert" method).
            // So to be able to animate DISMISSED alerts, we pass here all necessary information, since alert_id is not useful
            // See Bug 9337 for more details
            String alertTimeStr = request.getParameter("time_stamp");
            String hostTOStr = request.getParameter("host"); //alert.getHostTO()
            String alertMessage = request.getParameter("alert_message"); //alert.getMessage()
            
            if (hostTOStr == null) {
                return ErrorMessages.NO_ANIMATION_FOR_ALERT;
            }
            
            Long alertTime = Long.parseLong(alertTimeStr);
            TO<?> hostTO = TOFactory.getInstance().loadTO(hostTOStr);
            TO<?> to = null;
            Environment env = Env.getEnv(request);
            HashMap<String, String> alertMsgTos = parseDataFormAlertMessage(alertMessage);
            switch(hostTO.getTypeName()) {
                case EnvironmentTypes.TYPE_SENSOR:
                case EnvironmentTypes.TYPE_IPMISENSOR:
                    // In case when the alert owner is a sensor (wsn or ipmi), we animate using this owner directly
                    to = hostTO;
                    break;
                case EnvironmentTypes.TYPE_NODE:
                    // In case when the alert owner is a wsnnode, we expect the proper wsnsensor link in alert message body
                    // TODO: (commented by skapral) Seems like a workaround case and a subject for refactoring. Link to monitored TO.property should be placed instead of the resolved WSNSENSORs, like in any other cases.
                    for (Entry<String, String> toEntry : alertMsgTos.entrySet()) {
                        TO<?> cto = TOFactory.getInstance().loadTO(toEntry.getKey());
                        // WSNNODE can refer only to WSNSENSORS, so we don't expect IPMI here...
                        if(EnvironmentTypes.TYPE_SENSOR.equals(cto.getTypeName())) {
                            Collection<TO<?>> parentsList = env.getParents(TOFactory.getInstance().loadTO(toEntry.getKey()),EnvironmentTypes.TYPE_NODE);
                            if (!parentsList.contains(hostTO)) {
                                // We should check that the listed WSNSENSOR belongs to the alert owner, to prevent the invalid animation behavior
                                continue;
                            }
                            
                            to = cto;
                            break;
                        }
                    }
                    break;
                default:
                    // In any other cases, we expect the link to a proper sensor (WSN or IPMI) in alert message.
                    // If there aren't any proper links, the alert is non-animable
                    for (Entry<String, String> toEntry : alertMsgTos.entrySet()) {
                        TO<?> cto = TOFactory.getInstance().loadTO(toEntry.getKey());
                        
                        if (!"".equals(toEntry.getValue())) {
                            try {
                                TO<?> resolvedLink = env.getPropertyValue(cto, toEntry.getValue(), TO.class);
                                if (resolvedLink.getTypeName().equals(EnvironmentTypes.TYPE_SENSOR)
                                 || resolvedLink.getTypeName().equals(EnvironmentTypes.TYPE_IPMISENSOR)) {
                                    to = resolvedLink;
                                    break;
                                }
                            } catch(ObjectNotFoundException ex) {
                                continue;
                            }
                        }
                    }
            }
            
            String type = "";
            String startTime = convertDateToString(alertTime - 3600000, request);;
            String endTime = convertDateToString(alertTime + 3600000, request);

            if (to != null) {
                Integer dataclass = env.getPropertyValue(to, "dataclass", Integer.class);
                Integer layer = env.getPropertyValue(to, "z", Integer.class);

                ArrayList<Integer> availLayers = UIConfig.getLayersValueByClassId(dataclass);
                if (layer > 0 && availLayers.contains(layer)) {
                    type = (new ImageAttributes(dataclass, layer)).packToString();
                }
            }

            if ("".equals(type)) {
                return ErrorMessages.NO_ANIMATION_FOR_ALERT;
            }
            if ("".equals(startTime) || "".equals(endTime)) {
                return ErrorMessages.UNABLE_TO_COMPLETE;
            }
                
            Collection<TO<?>> parent = env.getRelatedObjects(hostTO, EnvironmentTypes.TYPE_ROOM, false);
            String groupId = TOFactory.getInstance().saveTO(parent.iterator().next());

            if ("".equals(groupId)) {
                return ErrorMessages.UNABLE_TO_COMPLETE;
            }

            result = XMLHelper.getDocumentXml(prepareDocumentForAlertAnimation(groupId, type, startTime, endTime));
        } catch (Exception e) {
            LOG.error(e.getLocalizedMessage(), e);
            result = ErrorMessages.UNABLE_TO_COMPLETE;
        }

        return result;
    }

    private String convertDateToString(long date, HttpServletRequest request) {
        SimpleDateFormat formatter = new SynapDateFormat("yyyy-MM-dd HH:mm:ss",
                UsersDataOptions.getTzIdFromRequest(request));
        return formatter.format(new Date(date));
    }

    private HashMap<String, String> parseDataFormAlertMessage(String mess) {
        HashMap<String, String> res = new HashMap<String, String>();
        if (mess == null || mess.isEmpty()) {
            return res;
        }

        String alrDataStr = getDataFormAlertMessage(mess);
        if (alrDataStr.isEmpty())
            return res;
        String[] dataArr = alrDataStr.split(",");
        for (int i = 0; i < dataArr.length; i++) {
            String[] dd = dataArr[i].split("\\.");
            if (dd.length > 0) {
                res.put(dd[0], (dd.length > 2) ? dd[1] : "");
            }
        }
        return res;
    }

    public String getDataFormAlertMessage(String input) {
        if (input == null)
            return "Macro.DEFAULT_EXPANSION";
        // current data value skips message's text
        // but expands data macro into comma-separated
        // any-env-property-value macros
        Pattern p = Pattern
                .compile("\\$(?i)data\\((\\w+:\\d+\\.(?:\\w+\\.?){1,}),(-?\\d+(?:\\.\\d+)?)(?:,([ \\w]+))?\\)\\$");
        StringBuilder sb = new StringBuilder();
        Matcher m = p.matcher(input);
        boolean firstMatch = true;
        while (m.find()) {
            if (!firstMatch) {
                sb.append(", ");
            } else {
                firstMatch = false;
            }

            sb.append(m.group(1));
        }
        return sb.toString();
    }

}
