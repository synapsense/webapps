LPS 4.0.7 Core README

Here's what you'll find:

    LICENSE.txt            - The End User License Agreement for the LPS.
    README.txt             - This file.
    3rdPartyCredits        - Acknowledgements for 3rd party contributions 
                             to the LPS.
    lps-4.0.7        - Expanded directory containing the 
                             LPS 4.0.7 web app.
    release-notes.html     - Release notes.
    changelog.html         - Change log.

Deploy the lps-4.0.7 web application and surf to 

       http://yourhost:yourport/lps-4.0.7/index.html

where yourhost:yourport is the host and port where your servlet
container is running.  This should bring up Laszlo Quick Index page
with pointers to tutorials, examples, sample applications and more.

For detailed installation instructions, please see 
http://www.laszlosystems.com/developers/learn/documentation/ 

--

* T_LZ_COPYRIGHT_BEGIN ******************************************************
* Copyright 2001-2006 Laszlo Systems, Inc.  All Rights Reserved.            *
* Use is subject to license terms.                                          *
* T_LZ_COPYRIGHT_END ********************************************************
