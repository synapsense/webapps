package com.synapsense.dataAccess.calc;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dataAccess.util.EnvironmentMock;
import com.synapsense.dataAccess.util.MockTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;



/**
 * 
 * Tests related to calculation of properties defined
 * types.xml. 
 * 
 * @author pahunov
 *
 */
public class EnvPropertyCalculatorTest {
    private static EnvironmentMock env;
    private static PropertyCalculator calculator;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception{

        env = new EnvironmentMock();
        
        TO<?> root = new MockTO("ROOT:1");
        TO<?> rack = new MockTO("RACK:1");
        TO<?> cTop = new MockTO("SENSOR:1");
        TO<?> cMid = new MockTO("SENSOR:2");
        TO<?> cBot = new MockTO("SENSOR:3");
        
        env.setRelation(root, rack);
        env.setRelation(root, cTop);
        env.setRelation(root, cMid);
        env.setRelation(root, cBot);

        env.setPropertyValue(rack, "cTop", cTop);
        env.setPropertyValue(rack, "cMid", cMid);
        env.setPropertyValue(rack, "cBot", cBot);
        env.setPropertyValue(rack, "width", 4d);
        env.setPropertyValue(rack, "depth", 5d);

        
        env.setPropertyValue(cTop, "lastValue", 15d);
        env.setPropertyValue(cMid, "lastValue", 18d);
        env.setPropertyValue(cBot, "lastValue", 19d);
        
        calculator =  new EnvPropertyCalculator(env);
        
        TypeUtil.CONFIG_FILE_NAME = "test/src/com/synapsense/dataAccess/calc/types.xml";
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception{
    }
    
    
    @Test
    public void calculatePropertyValue1() throws ObjectNotFoundException, FormulaConfigurationException {
        TO<?> rack = new MockTO("RACK:1");
        Number area = calculator.calculateProperty(rack, "area");
        
        assertEquals((Double) (4d * 5d), area);
    }
    
    @Test
    public void calculatePropertyValue2() throws ObjectNotFoundException, FormulaConfigurationException {
        TO<?> rack = new MockTO("RACK:1");
        Number avgColdTemp = calculator.calculateProperty(rack, "avgColdTemp");

        assertEquals((Double) ((15d + 18d + 19d) / 3), avgColdTemp);
    }
    
}