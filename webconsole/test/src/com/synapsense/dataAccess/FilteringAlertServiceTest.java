package com.synapsense.dataAccess;


import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import com.synapsense.dataAccess.util.AlertServiceMock;
import com.synapsense.dataAccess.util.EnvironmentMock;
import com.synapsense.dataAccess.util.MockTO;
import com.synapsense.dto.Alert;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.service.AlertService;
import com.synapsense.service.Environment;


/**
 * 
 * Tests related to filtering of related objects 
 * based on "permissions" properties 
 * 
 * @author pahunov
 *
 */
public class FilteringAlertServiceTest {
    private AlertService fas;
    private AlertService as;
    
    private void setRelation(Environment env, String o1, String o2) throws Exception {
        env.setRelation(new MockTO(o1), new MockTO(o2));
    }

    private void raiseAlert(AlertService as, String o) throws Exception {
        String name = o + " Alert";
        as.raiseAlert(new Alert(name, name, new Date(), name, new MockTO(o)));
    }
    
    private void checkAlerts(Collection<Alert> alerts, String[] ids) {
        assertEquals(alerts.size(), ids.length);
        List<String> idsList = Arrays.asList(ids);
        for (Alert alert : alerts) {
            assertEquals(true, idsList.contains(alert.getHostTO().toString()));
        }
    }
    
    @Before
    public void setUp() throws Exception {
        
        /*
         * Original model
         * 
         * ROOT:1
         *   DC:1 (permissions=null)
         *     SET:1
         *       CONTROLOUT_CRAH:1
         *       CONTROLOUT_CRAH:2
         *     ROOM:1(permissions="__everyone")
         *       RACK:1
         *       RACK:2
         *     ROOM:2(permissions="")
         *       RACK:3
         *       RACK:4
         *   DC:2(permissions="")
         *     SET:2
         *       CONTROLOUT_CRAH:3
         *       CONTROLOUT_CRAH:4
         *     ROOM:3(permissions="__everyone")
         *       RACK:5
         *       RACK:6
         *     ROOM:4(permissions=null)
         *       RACK:7
         *       RACK:8
         *   DC:3(permissions="user")
         *     ROOM:5(permissions="user")
         *       RACK:9
         *       RACK:10
         *     ROOM:6(permissions=null)
         *       RACK:11
         *       RACK:12
         *       
         *       
         * Filtered model
         * 
         * ROOT:1
         *   DC:1 (permissions=null)
         *     SET:1
         *       CONTROLOUT_CRAH:1
         *       CONTROLOUT_CRAH:2
         *     ROOM:1(permissions="__everyone")
         *       RACK:1
         *       RACK:2
         *   DC:3(permissions="user")
         *     ROOM:5(permissions="user")
         *       RACK:9
         *       RACK:10
         *     ROOM:6(permissions=null)
         *       RACK:11
         *       RACK:12
         */

        EnvironmentMock env = new EnvironmentMock();
        
        setRelation(env, "ROOT:1", "DC:1");
        setRelation(env, "DC:1", "SET:1");
        setRelation(env, "SET:1", "CONTROLOUT_CRAH:1");
        setRelation(env, "SET:1", "CONTROLOUT_CRAH:2");
        setRelation(env, "DC:1", "ROOM:1");
        setRelation(env, "ROOM:1", "RACK:1");
        setRelation(env, "ROOM:1", "RACK:2");
        setRelation(env, "DC:1", "ROOM:2");
        setRelation(env, "ROOM:2", "RACK:3");
        setRelation(env, "ROOM:2", "RACK:4");

        setRelation(env, "ROOT:1", "DC:2");
        setRelation(env, "DC:2", "SET:2");
        setRelation(env, "SET:2", "CONTROLOUT_CRAH:3");
        setRelation(env, "SET:2", "CONTROLOUT_CRAH:4");
        setRelation(env, "DC:2", "ROOM:3");
        setRelation(env, "ROOM:3", "RACK:5");
        setRelation(env, "ROOM:3", "RACK:6");
        setRelation(env, "DC:2", "ROOM:4");
        setRelation(env, "ROOM:4", "RACK:7");
        setRelation(env, "ROOM:4", "RACK:8");

        setRelation(env, "ROOT:1", "DC:3");
        setRelation(env, "DC:3", "ROOM:5");
        setRelation(env, "ROOM:5", "RACK:9");
        setRelation(env, "ROOM:5", "RACK:10");
        setRelation(env, "DC:3", "ROOM:6");
        setRelation(env, "ROOM:6", "RACK:11");
        setRelation(env, "ROOM:6", "RACK:12");

        as = new AlertServiceMock(env);
        as.raiseAlert(new Alert("System Alert", "System Alert", new Date(), "System Alert", TOFactory.EMPTY_TO));
        for (TO<?> obj : env.getObjects()) {
            raiseAlert(as, obj.toString());
        }
        
        
        EnvironmentMock fEnv = new EnvironmentMock();
        
        setRelation(fEnv, "ROOT:1", "DC:1");
        setRelation(fEnv, "DC:1", "SET:1");
        setRelation(fEnv, "SET:1", "CONTROLOUT_CRAH:1");
        setRelation(fEnv, "SET:1", "CONTROLOUT_CRAH:2");
        setRelation(fEnv, "DC:1", "ROOM:1");
        setRelation(fEnv, "ROOM:1", "RACK:1");
        setRelation(fEnv, "ROOM:1", "RACK:2");

        setRelation(fEnv, "ROOT:1", "DC:3");
        setRelation(fEnv, "DC:3", "ROOM:5");
        setRelation(fEnv, "ROOM:5", "RACK:9");
        setRelation(fEnv, "ROOM:5", "RACK:10");
        setRelation(fEnv, "DC:3", "ROOM:6");
        setRelation(fEnv, "ROOM:6", "RACK:11");
        setRelation(fEnv, "ROOM:6", "RACK:12");
        
        fas = new FilteringAlertService(as, env, fEnv).getProxy();
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception{
    }
    
//	for (Alert a : alerts) {
//		System.out.println(a.getName());
//	}
//	System.out.println( alerts.size());
    
    
    @Test
    public void getActiveAlerts1() {
        Collection<Alert> alerts = fas.getActiveAlerts(new MockTO("ROOT:1"), true);
        assertEquals(15, alerts.size());
    }

    @Test
    public void getActiveAlerts2() {
        Collection<Alert> alerts = fas.getActiveAlerts(new MockTO("DC:1"), true);
        assertEquals(7, alerts.size());
    }

    @Test
    public void getActiveAlerts3() {
        Collection<Alert> alerts = fas.getActiveAlerts(new MockTO("DC:3"), true);
        assertEquals(7, alerts.size());
    }
    
    @Test
    public void getAlertsHistory1() {
        Collection<Alert> alerts = fas.getAlertsHistory(new MockTO("ROOT:1"), new Date(System.currentTimeMillis() - 60000), new Date(), true);
        assertEquals(15, alerts.size());
    }

    @Test
    public void getAlertsHistory2() {
        Collection<Alert> alerts = fas.getAlertsHistory(new MockTO("DC:1"), new Date(System.currentTimeMillis() - 60000), new Date(), true);
        assertEquals(7, alerts.size());
    }

    @Test
    public void getAlertsHistory3() {
        Collection<Alert> alerts = fas.getAlertsHistory(new MockTO("DC:3"), new Date(System.currentTimeMillis() - 60000), new Date(), true);
        assertEquals(7, alerts.size());
    }

    @Test
    public void dismissAlerts1() {
        TO<?> obj = new MockTO("ROOT:1");
        fas.dismissAlerts(obj, true, "dummy: dismissAlerts1");
        Collection<Alert> alerts = as.getActiveAlerts(obj, true);
        checkAlerts(alerts, new String[] {"ROOM:2", "RACK:3", "RACK:4",  "DC:2", "SET:2", "CONTROLOUT_CRAH:3", "CONTROLOUT_CRAH:4", "ROOM:3", "RACK:5", "RACK:6", "ROOM:4", "RACK:7", "RACK:8"});
    }
    
    @Test
    public void dismissAlerts2() {
        TO<?> obj = new MockTO("DC:1");
        fas.dismissAlerts(obj, true, "dummy: dismissAlerts2");
        Collection<Alert> alerts = as.getActiveAlerts(obj, true);
        checkAlerts(alerts, new String[] {"ROOM:2", "RACK:3", "RACK:4"});
    }
    
    @Test
    public void dismissAlerts3() {
        TO<?> obj = new MockTO("DC:13");
        fas.dismissAlerts(obj, true, "dummy: dismissAlerts");
        Collection<Alert> alerts = as.getActiveAlerts(obj, true);
        checkAlerts(alerts, new String[] {});
    }
    
    @Test
    public void getNumActiveAlerts1() {
        TO<?> obj = new MockTO("ROOT:1");
        int numAlerts = fas.getNumActiveAlerts(obj, true);
        assertEquals(numAlerts, 15);
    }
    
    @Test
    public void getNumActiveAlerts2() {
        TO<?> obj = new MockTO("DC:1");
        int numAlerts = fas.getNumActiveAlerts(obj, true);
        assertEquals(numAlerts, 7);
    }
    
    @Test
    public void getNumActiveAlerts3() {
        TO<?> obj = new MockTO("ROOM:1");
        int numAlerts = fas.getNumActiveAlerts(obj, true);
        assertEquals(numAlerts, 3);
    }

}
