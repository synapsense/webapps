package com.synapsense.dataAccess;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.dataAccess.FilteringEnvironment;
import com.synapsense.dataAccess.util.EnvironmentMock;
import com.synapsense.dataAccess.util.MockTO;
import com.synapsense.dto.TO;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.service.Environment;


/**
 * 
 * Tests related to filtering of related objects 
 * based on "permissions" properties 
 * 
 * @author pahunov
 *
 */
public class FilteringEnvironmentTest {
    private static EnvironmentMock env;
    private static Environment fEnv;
    
    private static void setRelation(String o1, String o2) throws Exception {
        env.setRelation(new MockTO(o1), new MockTO(o2));
    }

    private static void setPropertyValue(String o, String prop, Object value) throws Exception {
        env.setPropertyValue(new MockTO(o), prop, value);
    }
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        
        /*
         * ROOT:1
         *   DC:1 (permissions=null)
         *     SET:1
         *       CONTROLOUT_CRAH:1
         *       CONTROLOUT_CRAH:2
         *     ROOM:1(permissions="__everyone")
         *       RACK:1
         *       RACK:2
         *     ROOM:2(permissions="")
         *       RACK:3
         *       RACK:4
         *   DC:2(permissions="")
         *     SET:2
         *       CONTROLOUT_CRAH:3
         *       CONTROLOUT_CRAH:4
         *     ROOM:3(permissions="__everyone")
         *       RACK:5
         *       RACK:6
         *     ROOM:4(permissions=null)
         *       RACK:7
         *       RACK:8
         *   DC:3(permissions="user")
         *     ROOM:5(permissions="user")
         *       RACK:9
         *       RACK:10
         *     ROOM:6(permissions=null)
         *       RACK:11
         *       RACK:12
         */

        env = new EnvironmentMock();
        
        setRelation("ROOT:1", "DC:1");
        setRelation("DC:1", "SET:1");
        setRelation("SET:1", "CONTROLOUT_CRAH:1");
        setRelation("SET:1", "CONTROLOUT_CRAH:2");
        setRelation("DC:1", "ROOM:1");
        setRelation("ROOM:1", "RACK:1");
        setRelation("ROOM:1", "RACK:2");
        setRelation("DC:1", "ROOM:2");
        setRelation("ROOM:2", "RACK:3");
        setRelation("ROOM:2", "RACK:4");

        setRelation("ROOT:1", "DC:2");
        setRelation("DC:2", "SET:2");
        setRelation("SET:2", "CONTROLOUT_CRAH:3");
        setRelation("SET:2", "CONTROLOUT_CRAH:4");
        setRelation("DC:2", "ROOM:3");
        setRelation("ROOM:3", "RACK:5");
        setRelation("ROOM:3", "RACK:6");
        setRelation("DC:2", "ROOM:4");
        setRelation("ROOM:4", "RACK:7");
        setRelation("ROOM:4", "RACK:8");

        setRelation("ROOT:1", "DC:3");
        setRelation("DC:3", "ROOM:5");
        setRelation("ROOM:5", "RACK:9");
        setRelation("ROOM:5", "RACK:10");
        setRelation("DC:3", "ROOM:6");
        setRelation("ROOM:6", "RACK:11");
        setRelation("ROOM:6", "RACK:12");

        setPropertyValue("ROOM:1", "permissions", "__everyone");
        setPropertyValue("ROOM:2", "permissions", "");
        setPropertyValue("DC:2", "permissions", "");
        setPropertyValue("ROOM:3", "permissions", "__everyone");
        setPropertyValue("DC:3", "permissions", "user");
        setPropertyValue("ROOM:5", "permissions", "user");
        
        fEnv = new FilteringEnvironment(env, "user", Collections.<String> emptyList(), false).getProxy();
    }
    
    @AfterClass
    public static void tearDownAfterClass() throws Exception{
    }
    
    
    @Test
    public void getObjectsByType1() {
        Collection<TO<?>> objs = fEnv.getObjectsByType("DC");

        assertTrue(objs.contains(new MockTO("DC:1")));
        assertTrue(objs.contains(new MockTO("DC:3")));
        
        assertTrue(!objs.contains(new MockTO("DC:2")));
    }
    
    @Test
    public void getObjectsByType2() {
        Collection<TO<?>> objs = fEnv.getObjectsByType("ROOM");

        assertTrue(objs.contains(new MockTO("ROOM:1")));
        assertTrue(objs.contains(new MockTO("ROOM:5")));
        assertTrue(objs.contains(new MockTO("ROOM:6")));
        
        assertTrue(!objs.contains(new MockTO("ROOM:2")));
        assertTrue(!objs.contains(new MockTO("ROOM:3")));
        assertTrue(!objs.contains(new MockTO("ROOM:4")));
    }
    
    @Test
    public void getObjectsByType3() {
        Collection<TO<?>> objs = fEnv.getObjectsByType("RACK");

        assertTrue(objs.contains(new MockTO("RACK:1")));
        assertTrue(objs.contains(new MockTO("RACK:2")));
        assertTrue(objs.contains(new MockTO("RACK:9")));
        assertTrue(objs.contains(new MockTO("RACK:10")));
        assertTrue(objs.contains(new MockTO("RACK:11")));
        assertTrue(objs.contains(new MockTO("RACK:12")));
        
        assertTrue(!objs.contains(new MockTO("RACK:3")));
        assertTrue(!objs.contains(new MockTO("RACK:4")));
        assertTrue(!objs.contains(new MockTO("RACK:5")));
        assertTrue(!objs.contains(new MockTO("RACK:6")));
        assertTrue(!objs.contains(new MockTO("RACK:7")));
        assertTrue(!objs.contains(new MockTO("RACK:8")));
    }

    @Test
    public void getChildren1() throws ObjectNotFoundException{
        Collection<TO<?>> objs = fEnv.getChildren(new MockTO("ROOT:1"));

        assertTrue(objs.contains(new MockTO("DC:1")));
        assertTrue(objs.contains(new MockTO("DC:3")));
        
        assertTrue(!objs.contains(new MockTO("DC:2")));
    }

    @Test
    public void getChildren2() throws ObjectNotFoundException{
        Collection<TO<?>> objs = fEnv.getChildren(new MockTO("DC:1"));

        assertTrue(objs.contains(new MockTO("ROOM:1")));
        
        assertTrue(!objs.contains(new MockTO("ROOM:2")));
    }

    @Test
    public void getChildren3() throws ObjectNotFoundException{
        Collection<TO<?>> objs = fEnv.getChildren(new MockTO("DC:3"));

        assertTrue(objs.contains(new MockTO("ROOM:5")));
        assertTrue(objs.contains(new MockTO("ROOM:6")));
    }
    
    
    @Test
    public void getRelatedObjects1() {
        Collection<TO<?>> objs = fEnv.getRelatedObjects(new MockTO("ROOT:1"), "DC", true);

        assertTrue(objs.contains(new MockTO("DC:1")));
        assertTrue(objs.contains(new MockTO("DC:3")));
        
        assertTrue(!objs.contains(new MockTO("DC:2")));
    }
    
    @Test
    public void getRelatedObjects2() {
        Collection<TO<?>> objs = fEnv.getRelatedObjects(new MockTO("ROOT:1"), "ROOM", true);

        assertTrue(objs.contains(new MockTO("ROOM:1")));
        assertTrue(objs.contains(new MockTO("ROOM:5")));
        assertTrue(objs.contains(new MockTO("ROOM:6")));
        
        assertTrue(!objs.contains(new MockTO("ROOM:2")));
        assertTrue(!objs.contains(new MockTO("ROOM:3")));
        assertTrue(!objs.contains(new MockTO("ROOM:4")));
    }
    
    @Test
    public void getRelatedObjects3() {
        Collection<TO<?>> objs = fEnv.getRelatedObjects(new MockTO("ROOT:1"), "RACK", true);

        assertTrue(objs.contains(new MockTO("RACK:1")));
        assertTrue(objs.contains(new MockTO("RACK:2")));
        assertTrue(objs.contains(new MockTO("RACK:9")));
        assertTrue(objs.contains(new MockTO("RACK:10")));
        assertTrue(objs.contains(new MockTO("RACK:11")));
        assertTrue(objs.contains(new MockTO("RACK:12")));
        
        assertTrue(!objs.contains(new MockTO("RACK:3")));
        assertTrue(!objs.contains(new MockTO("RACK:4")));
        assertTrue(!objs.contains(new MockTO("RACK:5")));
        assertTrue(!objs.contains(new MockTO("RACK:6")));
        assertTrue(!objs.contains(new MockTO("RACK:7")));
        assertTrue(!objs.contains(new MockTO("RACK:8")));
    }
    
    @Test
    public void getRelatedObjects4() {
        Collection<TO<?>> objs = fEnv.getRelatedObjects(new MockTO("DC:1"), "RACK", true);

        assertTrue(objs.contains(new MockTO("RACK:1")));
        assertTrue(objs.contains(new MockTO("RACK:2")));
        
        assertTrue(!objs.contains(new MockTO("RACK:3")));
        assertTrue(!objs.contains(new MockTO("RACK:4")));
    }
    
}
