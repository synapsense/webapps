package com.synapsense.dataAccess;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.BeforeClass;
import org.junit.Test;

import com.synapsense.config.types.TypeUtil;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.service.Environment;
import com.synapsense.service.impl.dao.to.EnvObjTypeTO;


/**
 * 
 * Tests related to filtering of properties that have
 * visible="false" in types.xml and additions of virtual
 * properties. 
 * 
 * @author pahunov
 *
 */
public class FilteringEnvironmentTest1 {

    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception{

        TypeUtil.CONFIG_FILE_NAME = "test/src/com/synapsense/dataAccess/types.xml";
    }
    
    @Test
    public void getObjectTypeTest() throws Exception {
        final ObjectType ot = new MyEnvObjTypeTO("RACK", "");
        PropertyDescr widthDescr = new PropertyDescr("width", Integer.class);
        PropertyDescr depthDescr = new PropertyDescr("depth", Integer.class);

        //should be thrown away by FilteringEnvironment
        PropertyDescr invisibleDescr = new PropertyDescr("invisible", Integer.class);

        //should be added by FilteringEnvironment
        PropertyDescr areaDescr = new PropertyDescr("area", Integer.class);
        PropertyDescr perimeterDescr = new PropertyDescr("perimeter", Integer.class);

        ot.getPropertyDescriptors().add(widthDescr);
        ot.getPropertyDescriptors().add(depthDescr);
        ot.getPropertyDescriptors().add(invisibleDescr);
        
        Mockery context = new Mockery();
        final Environment envMock = context.mock(Environment.class);
        
        context.checking(new Expectations() {
            {
                exactly(1).of(envMock).getObjectType("RACK");
                will(returnValue(ot));
            }
        });
        
        Environment fEnv = new FilteringEnvironment(envMock, "", Collections.<String> emptyList(), false).getProxy();
        
        Collection<PropertyDescr> descrs = fEnv.getObjectType("RACK").getPropertyDescriptors();
        
        assertTrue(descrs.contains(widthDescr));
        assertTrue(descrs.contains(depthDescr));
        assertTrue(descrs.contains(areaDescr));
        assertTrue(descrs.contains(perimeterDescr));
        assertTrue(!descrs.contains(invisibleDescr));
        
        context.assertIsSatisfied();
        
    }
    
    @Test
    public void getObjectTypesTest() throws Exception {
        final ObjectType ot = new MyEnvObjTypeTO("RACK", "");
        PropertyDescr widthDescr = new PropertyDescr("width", Integer.class);
        PropertyDescr depthDescr = new PropertyDescr("depth", Integer.class);

        //should be thrown away by FilteringEnvironment
        PropertyDescr invisibleDescr = new PropertyDescr("invisible", Integer.class);

        //should be added by FilteringEnvironment
        PropertyDescr areaDescr = new PropertyDescr("area", Integer.class);
        PropertyDescr perimeterDescr = new PropertyDescr("perimeter", Integer.class);

        ot.getPropertyDescriptors().add(widthDescr);
        ot.getPropertyDescriptors().add(depthDescr);
        ot.getPropertyDescriptors().add(invisibleDescr);
        
        Mockery context = new Mockery();
        final Environment envMock = context.mock(Environment.class);
        
        context.checking(new Expectations() {
            {
                exactly(1).of(envMock).getObjectTypes(new String[]{"RACK"});
                will(returnValue(new ObjectType[]{ot}));
            }
        });
        
        Environment fEnv = new FilteringEnvironment(envMock, "", Collections.<String> emptyList(), false).getProxy();

        ObjectType[] types = fEnv.getObjectTypes(new String[]{"RACK"});
        assertTrue(types.length == 1);
        
        Collection<PropertyDescr> descrs = types[0].getPropertyDescriptors();
        
        assertTrue(descrs.contains(widthDescr));
        assertTrue(descrs.contains(depthDescr));
        assertTrue(descrs.contains(areaDescr));
        assertTrue(descrs.contains(perimeterDescr));
        assertTrue(!descrs.contains(invisibleDescr));
        
        context.assertIsSatisfied();
        
    }
    
    @Test
    public void updateObjectTypeTest() throws Exception {
        final ObjectType ot = new MyEnvObjTypeTO("RACK", "");
        PropertyDescr widthDescr = new PropertyDescr("width", Integer.class);
        PropertyDescr depthDescr = new PropertyDescr("depth", Integer.class);
        PropertyDescr newDescr = new PropertyDescr("new", Integer.class);

        //should be thrown away by FilteringEnvironment
        PropertyDescr invisibleDescr = new PropertyDescr("invisible", Integer.class);

        //should be added by FilteringEnvironment
        PropertyDescr areaDescr = new PropertyDescr("area", Integer.class);
        PropertyDescr perimeterDescr = new PropertyDescr("perimeter", Integer.class);

        ot.getPropertyDescriptors().add(widthDescr);
        ot.getPropertyDescriptors().add(depthDescr);
        ot.getPropertyDescriptors().add(invisibleDescr);

        final ObjectType clientOt = new MyEnvObjTypeTO("RACK", "");
        clientOt.getPropertyDescriptors().add(widthDescr);
        clientOt.getPropertyDescriptors().add(depthDescr);
        clientOt.getPropertyDescriptors().add(areaDescr);
        clientOt.getPropertyDescriptors().add(perimeterDescr);
        clientOt.getPropertyDescriptors().add(newDescr);

        final ObjectType newOt = new MyEnvObjTypeTO("RACK", "");
        newOt.getPropertyDescriptors().add(widthDescr);
        newOt.getPropertyDescriptors().add(depthDescr);
        newOt.getPropertyDescriptors().add(invisibleDescr);
        newOt.getPropertyDescriptors().add(newDescr);
        
        Mockery context = new Mockery();
        final Environment envMock = context.mock(Environment.class);
        
        context.checking(new Expectations() {
            {
                atLeast(1).of(envMock).getObjectType("RACK");
                will(returnValue(ot));
                exactly(1).of(envMock).updateObjectType(newOt);
            }
        });
        
        Environment fEnv = new FilteringEnvironment(envMock, "", Collections.<String> emptyList(), false).getProxy();
        
        fEnv.updateObjectType(clientOt);

        context.assertIsSatisfied();
        
    }
    
    private class  MyEnvObjTypeTO extends EnvObjTypeTO {

        private static final long serialVersionUID = 1L;

        public MyEnvObjTypeTO(String name, String description) {
            super(name, description);
        }

        //equals is not override in EnvObjTypeTO, need it to check expectations
        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            MyEnvObjTypeTO other = (MyEnvObjTypeTO) obj;
            if (!getName().equals(other.getName()))
                return false;
            
            
            
            if (!getPropertyDescriptors().equals(other.getPropertyDescriptors()))
                return false;
            return true;
        }
        
    }
}
