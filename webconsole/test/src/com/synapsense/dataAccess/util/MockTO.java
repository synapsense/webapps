package com.synapsense.dataAccess.util;

import com.synapsense.dto.TO;

/**
 * @author pahunov
 *
 */
public class MockTO implements TO<String>{
    private String id;
    
    public MockTO(String id) {
        if (id == null) {
            throw new NullPointerException();
        }
        if (id.split(":").length != 2) {
            throw new IllegalArgumentException("Id must be in a form of \"TYPE:id\"");
        }
        
        this.id = id;
    }

    @Override
    public String getEsID() {
        return null;
    }
    
    @Override
    public String getID() {
        return id;
    }

    @Override
    public String getTypeName() {
        return id.substring(0, id.indexOf(":"));
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public String toString() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        
        if (!(obj instanceof MockTO)) {
            return false;
        }
        return id.equals(((MockTO) obj).id);
    }
    
    

}
