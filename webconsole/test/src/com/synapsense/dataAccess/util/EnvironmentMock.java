package com.synapsense.dataAccess.util;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.synapsense.dto.CollectionTO;
import com.synapsense.dto.EnvObjectBundle;
import com.synapsense.dto.EnvObjectHistory;
import com.synapsense.dto.ObjectType;
import com.synapsense.dto.ObjectTypeMatchMode;
import com.synapsense.dto.PropertyDescr;
import com.synapsense.dto.Relation;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOBundle;
import com.synapsense.dto.Tag;
import com.synapsense.dto.TagDescriptor;
import com.synapsense.dto.ValueTO;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.IllegalInputParameterException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.ServerConfigurationException;
import com.synapsense.exception.TagNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.exception.UnableToConvertTagException;
import com.synapsense.exception.UnsupportedRelationException;
import com.synapsense.search.QueryDescription;
import com.synapsense.service.Environment;

/**
 * @author pahunov
 *
 */
public class EnvironmentMock implements Environment {

    private Map<TO<?>, Set<TO<?>>> children = new HashMap<TO<?>, Set<TO<?>>>();
    private Map<TO<?>, Set<TO<?>>> parents = new HashMap<TO<?>, Set<TO<?>>>();
    Map<TO<?>, Map<String, Object>> objects = new HashMap<TO<?>, Map<String, Object>>();
    
    
    @Override
    public void setAllTagsValues(TO<?> arg0, Collection<Tag> c){
        throw new UnsupportedOperationException("Not Implemented");
    }
    
    @Override
    public <REQUESTED_TYPE> void addPropertyValue(TO<?> arg0, String arg1,
            REQUESTED_TYPE arg2) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void configurationComplete() throws ServerConfigurationException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void configurationStart() throws ServerConfigurationException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public TO<?> createObject(String arg0) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }
    
    @Override
    public TOBundle createObjects(EnvObjectBundle obj0) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void removeRelation(List<Relation> arg0)
            throws ObjectNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public void setRelation(List<Relation> arg0)
            throws ObjectNotFoundException, UnsupportedRelationException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public TO<?> createObject(String arg0, ValueTO[] arg1) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public ObjectType createObjectType(String arg0) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public void deleteObject(TO<?> arg0) throws ObjectNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");

    }
    
    @Override
    public void deleteObject (TO<?> arg0,int arg1) throws ObjectNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void deleteObjectType(String arg0) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public boolean exists(TO<?> obj) throws IllegalInputParameterException {
        return objects.containsKey(obj);

    }

    @Override
    public Collection<ValueTO> getAllPropertiesValues(TO<?> arg0)
            throws ObjectNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<CollectionTO> getAllPropertiesValues(
            Collection<TO<?>> arg0) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getChildren(TO<?> arg0)
            throws ObjectNotFoundException {
        Collection<TO<?>> res = children.get(arg0);
        return res == null?  new HashSet<TO<?>>() : new HashSet<TO<?>>(res);		
    }

    @Override
    public Collection<CollectionTO> getChildren(Collection<TO<?>> arg0) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getChildren(TO<?> arg0, String arg1)
            throws ObjectNotFoundException {
        Collection<TO<?>> res = new HashSet<TO<?>>();
        Collection<TO<?>> c = children.get(arg0);
        if (c != null) {
            for (TO<?> to : c) {
                if (to.getTypeName().equals(arg1)) {
                    res.add(to);
                }
            }
        }
        return res;
        
    }

    @Override
    public Collection<CollectionTO> getChildren(Collection<TO<?>> arg0,
            String arg1) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> arg0,
            String arg1, Class<RETURN_TYPE> arg2)
            throws ObjectNotFoundException, UnableToConvertPropertyException,
            PropertyNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> arg0,
            String arg1, int arg2, int arg3, Class<RETURN_TYPE> arg4)
            throws ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> arg0,
            String arg1, Date arg2, Date arg3, Class<RETURN_TYPE> arg4)
            throws ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public <RETURN_TYPE> Collection<ValueTO> getHistory(TO<?> arg0,
            String arg1, Date arg2, int arg3, Class<RETURN_TYPE> arg4)
            throws ObjectNotFoundException, PropertyNotFoundException,
            UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Integer getHistorySize(TO<?> arg0, String arg1)
            throws ObjectNotFoundException, PropertyNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public ObjectType getObjectType(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public String[] getObjectTypes() {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public ObjectType[] getObjectTypes(String[] arg0) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getObjects(String arg0, ValueTO[] arg1) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getObjects(String arg0, ObjectTypeMatchMode arg1,
            ValueTO[] arg2) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getObjectsByType(String arg0) {
        Collection<TO<?>> res = new HashSet<TO<?>>();
        for (TO<?> to : objects.keySet()) {
            if (to.getTypeName().equals(arg0)) {
                res.add(to);
            }
        }
        return res;
    }

    @Override
    public Collection<TO<?>> getObjectsByType(String arg0,
            ObjectTypeMatchMode arg1) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getParents(TO<?> arg0)
            throws ObjectNotFoundException {
        Collection<TO<?>> res = parents.get(arg0);
        return res ==  new HashSet<TO<?>>()? res : new HashSet<TO<?>>(res);		
        
    }

    @Override
    public Collection<TO<?>> getParents(TO<?> arg0, String arg1)
            throws ObjectNotFoundException {
        Collection<TO<?>> res = new HashSet<TO<?>>();
        Collection<TO<?>> p = parents.get(arg0);
        if (p != null) {
            for (TO<?> to : p) {
                if (to.getTypeName().equals(arg1)) {
                    res.add(to);
                }
            }
        }
        return res;
        
    }

    @Override
    public PropertyDescr getPropertyDescriptor(String arg0, String arg1)
            throws ObjectNotFoundException, PropertyNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public long getPropertyTimestamp(TO<?> arg0, String arg1)
            throws ObjectNotFoundException, PropertyNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<CollectionTO> getPropertyValue(Collection<TO<?>> arg0,
            String[] arg1) {
        Collection<CollectionTO> res = new HashSet<CollectionTO>(); 
        for (TO<?> to : arg0) {
            CollectionTO colTO = new CollectionTO(to);
            for (String prop : arg1) {
                try {
                    colTO.addValue(new ValueTO(prop, getPropertyValue(to, prop, Object.class)));
                } catch (ObjectNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (PropertyNotFoundException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                } catch (UnableToConvertPropertyException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            res.add(colTO);
        }
        
        return res;
        
    }

    @SuppressWarnings("unchecked")
    @Override
    public <RETURN_TYPE> RETURN_TYPE getPropertyValue(TO<?> arg0, String arg1,
            Class<RETURN_TYPE> arg2) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        Map<String, Object> p = objects.get(arg0);
        if (p == null) {
            return null;
        }
        
        return (RETURN_TYPE) p.get(arg1);
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE getPropertyValue(String arg0, String arg1,
            Class<RETURN_TYPE> arg2) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Collection<TO<?>> getRelatedObjects(TO<?> arg0, String arg1,
            boolean arg2) {
        
        Collection<TO<?>> res = new HashSet<TO<?>>();
        Collection<TO<?>> col = null;
        try {
            if (arg2) {
                col = getChildren(arg0);
            } else {
                col = getParents(arg0);
            }
            if (col != null) {
                for (TO<?> to : col) {
                    if (to.getTypeName().equals(arg1)) {
                        res.add(to);
                    }
                    res.addAll(getRelatedObjects(to, arg1, arg2));
                }
            }
        } catch (ObjectNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        return res;
        
    }

    @Override
    public <REQUESTED_TYPE> void removePropertyValue(TO<?> arg0, String arg1,
            REQUESTED_TYPE arg2) throws ObjectNotFoundException,
            PropertyNotFoundException, UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void removeRelation(TO<?> arg0, TO<?> arg1)
 throws ObjectNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void setAllPropertiesValues(TO<?> arg0, ValueTO[] arg1)
            throws ObjectNotFoundException, PropertyNotFoundException,
 UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void setAllPropertiesValues(TO<?> arg0, ValueTO[] arg1, boolean arg2)
            throws ObjectNotFoundException, PropertyNotFoundException,
 UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(TO<?> arg0, String arg1,
            REQUESTED_TYPE arg2) throws ObjectNotFoundException,
 PropertyNotFoundException, UnableToConvertPropertyException {
        Map<String, Object> p = objects.get(arg0);
        if (p == null) {
            p = new HashMap<String, Object>();
            objects.put(arg0, p);
        }
        
        p.put(arg1, arg2);

    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(String arg0, String arg1,
            REQUESTED_TYPE arg2) throws ObjectNotFoundException,
 PropertyNotFoundException, UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public <REQUESTED_TYPE> void setPropertyValue(TO<?> arg0, String arg1,
            REQUESTED_TYPE arg2, boolean arg3)
            throws PropertyNotFoundException, ObjectNotFoundException,
 UnableToConvertPropertyException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void setRelation(TO<?> parent, TO<?> child)
 throws ObjectNotFoundException, UnsupportedRelationException {

        if (!objects.containsKey(parent)) {
            objects.put(parent, null);
        }

        if (!objects.containsKey(child)) {
            objects.put(child, null);
        }
        
        Set<TO<?>> c = children.get(parent);
        if (c == null) {
            c = new HashSet<TO<?>>();
            children.put(parent, c);
        }
        c.add(child);

        Set<TO<?>> p = parents.get(child);
        if (p == null) {
            p = new HashSet<TO<?>>();
            parents.put(child, p);
        }
        p.add(parent);

    }
    
    @Override
    public void updateObjectType(ObjectType arg0) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");

    }
    
    public void dumpObjects() {
        Collection<TO<?>> objs = objects.keySet();
        System.out.println("Total number of objects: " + objs.size());
        try {
            for (TO<?> to : objs) {
                System.out.println(to);
                    Collection<TO<?>> parents = getParents(to);
                    if (parents != null) {
                        System.out.println("\tparents:");
                        for (TO<?> p : parents) {
                            System.out.println("\t\t" + p);
                        }
                    }
                    Collection<TO<?>> children = getChildren(to);
                    if (children != null) {
                        System.out.println("\tchildren:");
                        for (TO<?> c : children) {
                            System.out.println("\t\t" + c);
                        }
                    }
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    public Collection<TO<?>> getObjects() {
        return objects.keySet();
    }

    @Override
    public Collection<Tag> getAllTags(TO<?> arg0)
            throws ObjectNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map<TO<?>, Collection<Tag>> getAllTags(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<Tag> getAllTags(TO<?> arg0, String arg1)
            throws ObjectNotFoundException, PropertyNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public TagDescriptor getTagDescriptor(String arg0, String arg1, String arg2)
            throws ObjectNotFoundException, PropertyNotFoundException,
            TagNotFoundException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public <RETURN_TYPE> RETURN_TYPE getTagValue(TO<?> arg0, String arg1,
            String arg2, Class<RETURN_TYPE> arg3)
            throws ObjectNotFoundException, PropertyNotFoundException,
            TagNotFoundException, UnableToConvertTagException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Map<TO<?>, Map<String, Collection<Tag>>> getTags(
            Collection<TO<?>> arg0, List<String> arg1, List<String> arg2) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public void setTagValue(TO<?> arg0, String arg1, String arg2, Object arg3)
            throws PropertyNotFoundException, ObjectNotFoundException,
            TagNotFoundException, UnableToConvertTagException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }
    
    @Override
    public void setAllPropertiesValues(TO<?> objId, Collection<ValueTO> values, Collection<Tag> tags) throws EnvException {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<TO<?>> getObjects(QueryDescription queryDescr) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public EnvObjectHistory getHistory(TO<?> objId, Collection<String> propNames, Date start, Date end) {
        throw new UnsupportedOperationException("Not Implemented");
    }

}
