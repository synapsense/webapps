package com.synapsense.dataAccess.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.synapsense.dto.Alert;
import com.synapsense.dto.AlertFilter;
import com.synapsense.dto.AlertPriority;
import com.synapsense.dto.AlertStatus;
import com.synapsense.dto.AlertType;
import com.synapsense.dto.MessageTemplate;
import com.synapsense.dto.MessageType;
import com.synapsense.dto.TO;
import com.synapsense.dto.TOFactory;
import com.synapsense.exception.AlertTypeAlreadyExistsException;
import com.synapsense.exception.EnvException;
import com.synapsense.exception.NoSuchAlertPriorityException;
import com.synapsense.exception.NoSuchAlertTypeException;
import com.synapsense.exception.ObjectNotFoundException;
import com.synapsense.exception.PropertyNotFoundException;
import com.synapsense.exception.UnableToConvertPropertyException;
import com.synapsense.service.AlertService;

/**
 * @author pahunov
 *
 */
public class AlertServiceMock implements AlertService {
    
    EnvironmentMock env;

    public AlertServiceMock(EnvironmentMock env) {
        this.env = env;
    }

    @Override
    public void addActionDestination(String arg0, String arg1, MessageType mt, String arg2)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void createAlertType(AlertType arg0)
            throws AlertTypeAlreadyExistsException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void deleteAlertType(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    public void deleteAlerts(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void deleteAlerts(TO<?> arg0, boolean arg1) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    public void deleteAlerts(Collection<TO<?>> arg0, boolean arg1) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void deleteAlertsHistoryOlderThan(Date arg0) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void deleteAllAlerts() {
        throw new UnsupportedOperationException("Not Implemented");

    }

    public void deleteAllAlertsHistory() {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void dismissAlert(Alert arg0, String arg1) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void dismissAlert(Integer arg0, String arg1) {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void dismissAlerts(TO<?> objs, boolean deep, String arg2) {
        for (Alert alert : getActiveAlerts(objs, deep)) {
            dismissAlert(alert, arg2);
        }

    }

    public void dismissAlerts(Collection<TO<?>> objs, boolean deep, String arg2) {
        for (TO<?> obj : objs) {
            dismissAlerts(obj, deep, arg2);
        }

    }

    @Override
    public Collection<Alert> getActiveAlerts(TO<?> obj, boolean deep) {
        Collection<Alert> res = new ArrayList<Alert>();
        for (Alert alert: getAllAlerts(obj, deep)) {
            if (alert.getStatus() == AlertStatus.OPENED) {
                res.add(alert);
            }
        }
        return res;
    }

    @Override
    public Collection<Alert> getActiveAlerts(Collection<TO<?>> objs,
            boolean deep) {
        Collection<Alert> alerts = new ArrayList<Alert>();
        for (TO<?> obj : objs) {
            alerts.addAll(getActiveAlerts(obj, deep));
        }
        return alerts;
    }

    @Override
    public Collection<String> getAlertPrioritiesNames() {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<AlertType> getAlertTypes(String[] arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<Alert> getAlertsHistory(TO<?> obj, Date arg1, Date arg2,
            boolean deep) {
        return getAllAlerts(obj, deep);
    }

    public Collection<Alert> getAlertsHistory(Collection<TO<?>> objs,
            Date start, Date end, boolean deep) {
        Collection<Alert> alerts = new ArrayList<Alert>();
        for (TO<?> obj : objs) {
            alerts.addAll(getAlertsHistory(obj, start, end, deep));
        }
        return alerts;
    }

    @Override
    public Collection<AlertType> getAllAlertTypes() {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @SuppressWarnings("unchecked")
    public Collection<Alert> getAllAlerts(TO<?> obj, boolean deep) {
        Collection<Alert> alerts;
        try {
            alerts = env.getPropertyValue(obj, "__alerts", Collection.class);
            if (alerts == null) {
                alerts = new ArrayList<Alert>();
            } else {
                alerts = new ArrayList<Alert>(alerts);
            }
            
            if (deep) {
                Collection<TO<?>>children = env.getChildren(obj);
                for (TO<?> child : children) {
                    alerts.addAll(getActiveAlerts(child, deep));
                }
            }
            return alerts;
        } catch (ObjectNotFoundException e) {
            throw new RuntimeException();
        } catch (PropertyNotFoundException e) {
            throw new RuntimeException();
        } catch (UnableToConvertPropertyException e) {
            throw new RuntimeException();
        }
    }

    public Collection<Alert> getAllAlerts(Collection<TO<?>> objs, boolean deep) {
        Collection<Alert> alerts = new ArrayList<Alert>();
        for (TO<?> obj : objs) {
            alerts.addAll(getAllAlerts(obj, deep));
        }
        return alerts;
    }

    public Collection<Alert> getDismissedAlerts(TO<?> obj, boolean deep) {
        Collection<Alert> res = new ArrayList<Alert>();
        for (Alert alert: getAllAlerts(obj, deep)) {
            if (alert.getStatus() == AlertStatus.DISMISSED) {
                res.add(alert);
            }
        }
        return res;
    }

    public Collection<Alert> getDismissedAlerts(Collection<TO<?>> objs,
            boolean deep) {
        Collection<Alert> alerts = new ArrayList<Alert>();
        for (TO<?> obj : objs) {
            alerts.addAll(getDismissedAlerts(obj, deep));
        }
        return alerts;
    }

    @Override
    public boolean hasAlerts(TO<?> arg0, boolean arg1) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<Alert> getAlerts(Integer[] arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @SuppressWarnings("unchecked")
    @Override
    public void raiseAlert(Alert alert) {
        
        TO<?> to =alert.getHostTO();
        if (to == null) {
            if (!env.exists(TOFactory.EMPTY_TO)) {
                env.objects.put(TOFactory.EMPTY_TO, null);
            }
            to = TOFactory.EMPTY_TO;
            
        } 
        
        if (!env.exists(to)) {
            
        } else {
            Collection<Alert> alerts;
            try {
                alerts = env.getPropertyValue(to, "__alerts", Collection.class);
                if (alerts == null) {
                    alerts = new ArrayList<Alert>();
                }
                alert.setStatus(AlertStatus.OPENED);
                alerts.add(alert);
                env.setPropertyValue(to, "__alerts", alerts);
            } catch (EnvException e) {
                throw new RuntimeException();
            }
        }
    }

    public void removeAction(String arg0, String arg1)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void removeActionDestination(String alertTypeName, String tierName, MessageType messageType, String destination)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void updateAlertType(String arg0, AlertType arg1)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");

    }

    @Override
    public void acknowledgeAlert(Integer arg0, String arg1) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public String getActionDestination(String arg0, String arg1,
            MessageType arg2, String arg3) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public AlertPriority getAlertPriority(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<Alert> getAlerts(AlertFilter arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public void removeTemplate(String arg0, MessageType arg1)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public void resolveAlert(Integer arg0, String arg1) {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public void setTemplate(String arg0, MessageTemplate arg1)
            throws NoSuchAlertTypeException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public void updateAlertPriority(String arg0, AlertPriority arg1)
            throws NoSuchAlertPriorityException {
        throw new UnsupportedOperationException("Not Implemented");
        
    }

    @Override
    public Alert getAlert(Integer arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public int getNumActiveAlerts(TO<?> objId, boolean propagate) {
        return getActiveAlerts(objId, propagate).size();
    }

    @Override
    public Collection<AlertType> getAlertTypesByDestination(String arg0,
            MessageType arg1) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public AlertType getAlertType(String arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public Collection<String> getActionDestinations(String arg0, String arg1, MessageType arg2) {
        throw new UnsupportedOperationException("Not Implemented");
    }

    @Override
    public int getNumAlerts(AlertFilter arg0) {
        throw new UnsupportedOperationException("Not Implemented");
    }

}
