package com.synapsense.alert.rule;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.synapsense.utilities.es.EnvironmentTypes;


public class RuleTest {
    
    @Test
    public void parseNodeRuleTest() throws Exception {
        String ruleSource = readRuleSource("com/synapsense/alert/rule/ver60/node_temperature.drools");
        Rule rule = new Rule(ruleSource);
        List<Expression> expressions = rule.getExpressions();
        assertEquals(expressions.size(), 1);
        
        Expression expression = expressions.get(0);
        assertEquals(expression.getObjType(), EnvironmentTypes.TYPE_NODE);
        assertEquals(expression.getPropertyName(), "200");
        assertEquals(Double.valueOf(expression.getThreshold()), (Double) 122d);
        assertEquals(expression.getCondition(), "and");
        assertEquals(expression.getOperation(), ">");
        
    }
    
    @Test
    public void parseSensorRuleTest() throws Exception {
        String ruleSource = readRuleSource("com/synapsense/alert/rule/ver60/sensor_temperature.drools");
        Rule rule = new Rule(ruleSource);
        List<Expression> expressions = rule.getExpressions();
        assertEquals(expressions.size(), 1);
        
        Expression expression = expressions.get(0);
        assertEquals(expression.getObjType(), EnvironmentTypes.TYPE_SENSOR);
        assertEquals(expression.getPropertyName(), "lastValue");
        assertEquals(Double.valueOf(expression.getThreshold()), (Double) 122d);
        assertEquals(expression.getCondition(), "and");
        assertEquals(expression.getOperation(), ">");
        
    }
    
    @Test
    public void parseSensorLinkRuleTest() throws Exception {
        String ruleSource = readRuleSource("com/synapsense/alert/rule/ver60/rack_ctop.drools");
        Rule rule = new Rule(ruleSource);
        List<Expression> expressions = rule.getExpressions();
        assertEquals(expressions.size(), 1);
        
        Expression expression = expressions.get(0);
        assertEquals(expression.getObjType(), EnvironmentTypes.TYPE_RACK);
        assertEquals(expression.getPropertyName(), "cTop");
        assertEquals(Double.valueOf(expression.getThreshold()), (Double) 122d);
        assertEquals(expression.getCondition(), "and");
        assertEquals(expression.getOperation(), ">");
    }
    
    @Test
    public void parseObjectRuleTest() throws Exception {
        String ruleSource = readRuleSource("com/synapsense/alert/rule/ver60/rack_dew_point.drools");
        Rule rule = new Rule(ruleSource);
        List<Expression> expressions = rule.getExpressions();
        assertEquals(expressions.size(), 1);
        
        Expression expression = expressions.get(0);
        assertEquals(expression.getObjType(), EnvironmentTypes.TYPE_RACK);
        assertEquals(expression.getPropertyName(), "coldDp");
        assertEquals(Double.valueOf(expression.getThreshold()), (Double) 122d);
        assertEquals(expression.getCondition(), "and");
        assertEquals(expression.getOperation(), ">");
    }
    
    private String readRuleSource(String path) throws IOException {
        StringBuffer fileData = new StringBuffer(1000);
        InputStream is = this.getClass().getClassLoader().getResourceAsStream(path);
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            fileData.append(buf, 0, numRead);
        }
        reader.close();
        return fileData.toString();
    }
}
