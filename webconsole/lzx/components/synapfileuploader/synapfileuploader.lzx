<library>
    <include href="incubator/uploader/fileupload.lzx"/>
    
    <class name="synapfileuploader" extends="fileupload" uploadenable="true">
        <switch>
            <when property="$as3">
                <passthrough>
                    import flash.net.*;
                </passthrough>
            </when>
        </switch>
        
        <attribute name="overrideUploadMethod" type="boolean" value="false"/>
        <attribute name="uploadingViaHttp" type="boolean" value="false"/>
        <attribute name="fileFilter" value="$once{[new FileFilter('All Files (*.*)', '*.*')]}"/>
        <attribute name="uploadedFileId" type="number" value="0"/>
                           
        <event name="oncomplete"/>
        <event name="oncancel"/>
        <event name="onselect"/>
        <event name="onprogress"/>
        <event name="onopen"/>
        <event name="onsecurityerror"/>
        <event name="onioerror"/>
        <event name="onhttperror"/>
        <event name="onuploadfinish"/>
        
        <basesynapdataset name="UploaderDataset" type="http" request="false">        
            <handler name="ondata">
                classroot.uploadingViaHttp = false;
                var success = this.getPointer().xpathQuery("success/text()") == "true" ? true : false;
                if(success) {
                    classroot.onProgress(classroot.fileref, classroot.fileref.size, classroot.fileref.size);
                    if (classroot["oncomplete"]) {
                        classroot.setAttribute('msg', LangManager.getFormattedLocaleString("ESConfigurator/ID_UPLOAD_COMPLETED_MESSAGE", classroot.getName()));
                        classroot.oncomplete.sendEvent(classroot.fileref);
                    }
                } else {
                    classroot.onHTTPError();                    
                }
                classroot.onuploadfinish.sendEvent();
            </handler>
            
            <handler name="onerror">
                classroot.uploadingViaHttp = false;
                classroot.setAttribute('msg', LangManager.getLocaleString("message/request_failed"));
                if (classroot["onhttperror"]) {
                    classroot.onhttperror.sendEvent();
                }
                classroot.onuploadfinish.sendEvent();
            </handler>
            
            <handler name="ontimeout">            
                classroot.uploadingViaHttp = false;
                classroot.setAttribute('msg', LangManager.getLocaleString("message/request_was_timed_out"));
                if (classroot["onhttperror"]) {
                    classroot.onhttperror.sendEvent();
                }
                classroot.onuploadfinish.sendEvent();
            </handler>
        </basesynapdataset>
        
        <method name="addFileFilter" args="fileType, fileMask">
            fileFilter.push(new FileFilter(fileType, fileMask));
        </method>
        
        <method name="clearFileFilters">
            fileFilter = [];    
        </method>
        
        <method name="browse">
            <![CDATA[
                fileref.browse(fileFilter);
            ]]>
        </method>
                        
        <method name="onComplete" args="fr">
            if(overrideUploadMethod){
                var len = fr.data.length;
                var array = new Array();
                for(var i = 0; i &lt; len; i++){
                    array.push(fr.data.readByte());
                }
                var fileContent = array.join(",");
                uploadingViaHttp = true;

                var params = new LzParam();
                params.addValue("fileContent", fileContent);
                params.addValue("fileId", this.uploadedFileId);               
                UploaderDataset.setQueryString(params);
                UploaderDataset.doRequest();
            }else{
                //Debug.write("onComplete " + fr);
                super.onComplete(fr);
                if (this["oncomplete"]) {
                    this.setAttribute('msg', LangManager.getFormattedLocaleString("ESConfigurator/ID_UPLOAD_COMPLETED_MESSAGE", fr.name));
                    this.oncomplete.sendEvent(fr);
                }
                onuploadfinish.sendEvent();
            }
        </method>
        
        <method name="upload" args="url">
            if(this.fileref.size / 1000 > this.maxsize) {
                this.setAttribute("msg", LangManager.getFormattedLocaleString("ESConfigurator/ID_UPLOADED_FILE_SIZE_EXEEDED_LIMITATION_MESSAGE", Math.round(fileref.size/1000), maxsize/1000000));                
                onuploadfinish.sendEvent();                
            } else {
                if(this.overrideUploadMethod) {
                    Debug.write("Uploading file [" + this.getName() + "] via http doRequest...");
                    this.fileref.load();
                }else{
                    Debug.write("Uploading file [" + this.getName() + "] via flash.net.FileReference...");
                    url += "?fileId=" + this.uploadedFileId;
                    super.upload(url);
                }
            }
        </method>
        
        <method name="onCancel">
            if(uploadingViaHttp &amp;&amp; overrideUploadMethod) {
                uploadingViaHttp = false;
                UploaderDataset.abort();
            }
            //Debug.write("onCancel");
            super.onCancel();
            if (this["oncancel"]) {
               this.oncancel.sendEvent();
            }
        </method>
                
        <method name="onSelect" args="fr">
            //Debug.write("onSelect " + fr);
            super.onSelect(fr);
            if (this["onselect"]) {
               this.onselect.sendEvent(fr);
            }
        </method>
                
        <method name="onProgress" args="fr, bytesLoaded, bytesTotal">
            if(!overrideUploadMethod) {
                //Debug.write("onProgress " + fr + " " + bytesLoaded + " " + bytesTotal);
                super.onProgress(fr,bytesLoaded,bytesTotal);
                if (this["onprogress"]) {
                   this.onprogress.sendEvent([fr,bytesLoaded,bytesTotal]);
                }
            }
        </method>
        
        <method name="onOpen">
            //Debug.write("onOpen");
            super.onOpen();
            if (this["onopen"]) {
               this.onopen.sendEvent();
            }
        </method>
        
        <method name="onSecurityError">
            //Debug.write("onSecurityError")
            super.onSecurityError();
            if (this["onsecurityerror"]) {
                this.setAttribute('msg', LangManager.getLocaleString("ESConfigurator/ID_SECURITY_ERROR_MESSAGE"));
                this.onsecurityerror.sendEvent();
            }
            onuploadfinish.sendEvent();
        </method>
        
        <method name="onIOError">
            //Debug.write("onIOError");
            super.onIOError();
            if (this["onioerror"]) {
                this.setAttribute('msg', LangManager.getLocaleString("ESConfigurator/ID_IO_ERROR_MESSAGE"));
                this.onioerror.sendEvent();
            }
            onuploadfinish.sendEvent();
        </method>
        
        <method name="onHTTPError">
            //Debug.write("onHTTPError");
            super.onHTTPError();
            if (this["onhttperror"]) {
                this.setAttribute('msg', LangManager.getLocaleString("ESConfigurator/ID_HTTP_ERROR_MESSAGE"));
                this.onhttperror.sendEvent();
            }
            onuploadfinish.sendEvent();
        </method>        
    </class>
</library>    