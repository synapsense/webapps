<library>
    <include href="gauge.lzx"/>
    <include href="../utils/unitsutil.lzx"/>
    <include href="../utils/typeutil.lzx"/>
    <include href="../lang/langmanager.lzx"/>
    <include href="../utils/utils.lzx"/>
    
    <!--- Gauge that handles data from the getObject servlet and is configured
         based on uiconfig.xml settings
    -->
    <class name="synapgauge" extends="gauge">
        <attribute name="type" value="" type="string"/>
        <attribute name="ownType" value="" type="string"/>
        <attribute name="typePath" value="" type="string"/>
        <attribute name="sourceType" value="" type="string"/>
        <attribute name="showTime" value="true" type="boolean"/>
        
        <method name="getGaugeTitle">
        <![CDATA[
            var title = "";
            
            if(this.type == "WSNNODE"){
               var sensorId = this.datapath.xpathQuery("dataclass/@v");               
               title = UnitsUtil.getDimensionName(sensorId);
               
               var units =  this.datapath.xpathQuery("lastValue/@u");
               if (units) {
                   title += "(" + units + ")";
               }
               title += " [" + this.datapath.xpathQuery("channel/@v") + "] " + "\n";
               title += this.datapath.xpathQuery("name/@v");                              
            } else if(this.type == "GENERICDEVICE"){
               var sensorId = this.datapath.xpathQuery("dataclass/@v");               
               title = UnitsUtil.getDimensionName(sensorId);
               
               var units =  this.datapath.xpathQuery("lastValue/@u");
               if (units) {
                   title += "(" + units + ")";
               }
            } else {
               var xmlNodeName = this.datapath.xpathQuery("name()");
               var dispName = getMappedPropertyName(this.type, xmlNodeName);                              
               
               dispName = (dispName == null) ? xmlNodeName : dispName;
               
               if (!dispName) {
                   return "";
               }
               
               var units = this.datapath.xpathQuery("@u");
               if (!units) {
                   units = this.datapath.xpathQuery("lastValue/@u");
               }
               var unitsStr = (units == null) ? "" : "(" + units + ")";
               title =  dispName + unitsStr;
            }
            
             return title;
        ]]>
        </method>
        
        <handler name="ondata">
            if(this.typePath != ""){
                this.setAttribute("type", this.datapath.xpathQuery(typePath));            
            }
            this.setAttribute("ownType", this.datapath.xpathQuery("@type"));
            
            if(this.isIonType()){            	
                this.handleModBusData();
            }else
            //Sensor gauge     
            if (this.datapath.getNodeName() == "WSNSENSOR" || this.datapath.xpathQuery("../name()") == "links"
                || this.datapath.xpathQuery("lastValue/@v") != null) {
                this.handleSensorData();
            } else { //historical property gauge
                this.handleHistoricalData();
            }    
            
            if(!this.showTime){
                this.tLabel.setAttribute("text", "");
            }
            
            this.postUpdate();
        </handler>
        
        <method name="handleModBusData">
            var value = NumberFormat.parse(this.datapath.xpathQuery("lastValue/@v"));
            
            if (NumberFormat.isNotANumber(value)) {
                value = -3000;
            }
            this.setAttribute('drawTicks', getFeatureTypeSetting("Gauge", "WSNSENSOR", "all", "drawTicks"));
            this.setAttribute('totalDegree', Number(getFeatureTypeSetting("Gauge", "WSNSENSOR", "all", "totalDegree"))); 
            this.setAttribute('shiftDegree', Number(getFeatureTypeSetting("Gauge", "WSNSENSOR", "all", "shiftDegree")));
            this.setAttribute('scaleColor', Number(getFeatureTypeSetting("Gauge", "WSNSENSOR", "all", "scaleColor")));
            
            var title = this.getGaugeTitle();                                                
            this.tTitle.setAttribute("text", title);
            
            this.setAttributesByXpath("ConfigData:/config/feature[@name='Gauge']/type[@name='MODBUSPROPERTY']/setting[@name='attributesXPath']/*[1]");
            
            if(NumberFormat.isNotANumber(this["min"])) {
                this.setAttribute("min", 0);
            }
            
            if(NumberFormat.isNotANumber(this["max"])) {
                if(value == -3000){
                    this.setAttribute("max", 100);
                }else{                	
                    var valueAsNumber = Number(value);
                    this.setAttribute("max", Math.ceil(valueAsNumber + 0.5 * valueAsNumber));
                }
            }
            if(NumberFormat.isNotANumber(this["aMin"])) {
                this.setAttribute('scaleColor', getNeutralGaugeScaleColor());
            }
            
            if (NumberFormat.isNotANumber(this["aMin"]) || NumberFormat.isNotANumber(this["aMax"])) {
                this["aMin"] = 0;
                this["aMax"] = 0;
            }
            if (NumberFormat.isNotANumber(this["rMin"]) || NumberFormat.isNotANumber(this["rMax"])) {
                this["rMin"] = 0;
                this["rMax"] = 0;
            }
            
            this.setAttribute("value", value);
            
            this.tLabel.setAttribute("text", this.datapath.xpathQuery("lastValue/@t"));
        </method>
        
        <method name="handleSensorData">
            var sensorId = this.datapath.xpathQuery("dataclass/@v");
            var value = NumberFormat.parse(this.datapath.xpathQuery("lastValue/@v"));
            
            if (NumberFormat.isNotANumber(value)) {
                value = -3000;
            }
            
            var label = getValueLabel(sensorId, value);
            if(label) {
                this.setAttribute('currentValueLabel', label); 
            } else {
                this.setAttribute('currentValueLabel', "undefined");
            }
            
            var min = NumberFormat.parse(this.datapath.xpathQuery("min/@v"));
            var max = NumberFormat.parse(this.datapath.xpathQuery("max/@v"));
            
            if(min == null) {
                min = 0;
            }
            if(max == null) {
                max = 100;
            }
           
            var label = getValueLabel(sensorId, min);
            if(label) {
                this.setAttribute('beginLabel', label); 
            } else {
                this.setAttribute('beginLabel', "undefined");
            }
            
            var label = getValueLabel(sensorId, max);
            if(label) {
                this.setAttribute('endLabel', label); 
            } else {
                this.setAttribute('endLabel', "undefined");
            }
            
            this.setSetting('drawTicks', "Boolean", sensorId);
            this.setSetting('totalDegree', "Number", sensorId);
            this.setSetting('shiftDegree', "Number", sensorId);
            this.setSetting('scaleColor', "Number", sensorId);
                       
            var title = this.getGaugeTitle();                                                
            this.tTitle.setAttribute("text", title);
            
            this.setAttributesByXpath("ConfigData:/config/feature[@name='Gauge']/type[@name='WSNSENSOR']/setting[@name='attributesXPath']/*[1]");
            
            if(NumberFormat.isNotANumber(this["aMin"])) {
                this.setAttribute('scaleColor', getNeutralGaugeScaleColor());
            }
            
            if (NumberFormat.isNotANumber(this["aMin"]) || NumberFormat.isNotANumber(this["aMax"])) {
                this["aMin"] = 0;
                this["aMax"] = 0;
            }
            if (NumberFormat.isNotANumber(this["rMin"]) || NumberFormat.isNotANumber(this["rMax"])) {
                this["rMin"] = 0;
                this["rMax"] = 0;
            }
            
            this.setAttribute("value", value);
            
            this.tLabel.setAttribute("text", this.datapath.xpathQuery("lastValue/@t"));
        </method>
        
        <method name="setSetting" args="setting, type, sensorId">
            var s = getFeatureTypeSetting("Gauge", "WSNSENSOR", sensorId, setting);
            if (!s) {
                s = getFeatureTypeSetting("Gauge", "WSNSENSOR", "all", setting);
            }
            if (type == "Number") {
                s = Number(s);
            }
            this.setAttribute(setting, s);
        </method>
        
        <method name="handleHistoricalData">
            var value = NumberFormat.parse(this.datapath.xpathQuery("@v"));
            this.setAttribute('scaleColor', getNeutralGaugeScaleColor());
            this.tTitle.setAttribute("text", getGaugeTitle());                                                                                             

            this.setAttribute("aMin", 0);
            this.setAttribute("aMax", 0);
            this.setAttribute("rMin", 0);
            this.setAttribute("rMax", 0);
            this.tLabel.setAttribute("text", "");
            var max;
            if (value == 0) {
                max = 100;
            } else if (value % 100 == 0) {
                max = value;
            } else {
                max = Math.ceil(value / 100) * 100
            }
            if (isNaN(value)) {
                value = -3000;
                min = 0;
                max = 100;
            }
            
            this.setAttribute("max", max);
            this.setAttribute("min", 0);
            this.setAttribute("value", value);
            this.tLabel.setAttribute("text", this.datapath.xpathQuery("@t"));
        </method>
        
        <method name="isIonType">
            var result = false;
            if (this.sourceType == "MODBUSPROPERTY" || (this.type == "ION_DELTA")
                || (this.type == "ION_WYE") || this.ownType == "MODBUSPROPERTY"){
                result = true;
            }
            return result;
        </method>
        
        <method name="setAttributesByXpath" args="attrXPath">
               var dps  = ConfigData.getPointer();
               dps.setXPath(attrXPath);
                   
                var i = 1;
                do {
                    var name = dps.xpathQuery("@key");
                    var xpath = dps.xpathQuery("@value");
                    var val = this.datapath.xpathQuery(xpath + "/@v");
                    this.setAttribute(name, val == null ? null : NumberFormat.parse(val));
                    i++;
                }while(dps.selectNext());
                
                dps.destroy();                
        </method>
        
        <method name="setRangeColorsByXPath" args="rangeXPath">
            var dp  = ConfigData.getPointer();
            
            dp.setXPath(rangeXPath);
            this.setAttribute("maxColor", Number(dp.xpathQuery("maxColor/text()")));
            this.setAttribute("aMaxColor", Number(dp.xpathQuery("aMaxColor/text()")));
            this.setAttribute("rMaxColor", Number(dp.xpathQuery("rMaxColor/text()")));
            this.setAttribute("rMinColor", Number(dp.xpathQuery("rMinColor/text()")));
            this.setAttribute("aMinColor", Number(dp.xpathQuery("aMinColor/text()")));
            
            dp.destroy();
        </method>
        
     </class>
</library>
