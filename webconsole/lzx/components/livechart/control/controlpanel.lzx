<library>
    <resource name="openhand" src="../resources/cursor/openedhand.png"/>
    <resource name="closedhand" src="../resources/cursor/closedhand.png"/>
    
    <resource name="zoomout" src="../../resources/zoomout.png"/>
    
    <node id="LvChartModes">
        <attribute name="NORMAL" value="normal" type="string"/>
        <attribute name="STANDALONE" value="standalone" type="string"/>
    </node>
    
    <class name="controlpanel" extends="view">
        <attribute name="manipulRef" value="$once{null}"/>
        
        <attribute name="percent" value="0.4" type="number"/>
        <attribute name="stepInMs" value="0" type="number"/>
        
        <attribute name="startBoundMs" value="1199134800000" type="number"/> <!-- 2008 -->
        <attribute name="endBoundMs" value="2556046800000" type="number"/> <!-- 2050 -->
        
        <attribute name="previous_drag_x" value="0" type="number"/>
        
        <attribute name="infoWndRef" value="$once{null}"/>
        <attribute name="infoOkWndRef" value="$once{null}"/>
        
        <attribute name="defaultLegendDescr" value="$once{new lz.legenddescr(this)}"/>
        
        <attribute name="params"/>
        
        <attribute name="isDataOnFirstRequest" type="boolean" value="false"/>
        
        <attribute name="wheelLocked" type="boolean" value="true"/>
        <attribute name="mode" value="normal" type="string"/> <!-- "normal" or "standalone" -->
        
        <attribute name="outOfMemoryMessage" value="Out of memory" type="string"/>
        
        <event name="onstartdate"/>
        <event name="onenddate"/>
        
        <view name="mainView">
            <view name="controlButtons">
            </view>
        </view>
        
        <handler name="onmousewheeldelta" reference="lz.Keys" args="delta">
        <![CDATA[
            var lvChartPlot = this.manipulRef.getControlledChart().getPlot(); 
        
            //if mouse is out of chart plot then return
            if (this.isWheelLocked() ||
                lvChartPlot.getMouse("x") < 0 ||
                lvChartPlot.getMouse("y") < 0 ||
                lvChartPlot.getMouse("x") > lvChartPlot.width ||
                lvChartPlot.getMouse("y") > lvChartPlot.height) {
                return;
            }
            
            if (delta < 0) {
                this.zoom(this.stepInMs); //zoom out
            }else{
                this.zoom(-this.stepInMs); //zoom in
            }
        ]]>
        </handler>
        
        <method name="lockwheel">
            this.setAttribute("wheelLocked", true);
        </method>
        
        <method name="unlockwheel">
            this.setAttribute("wheelLocked", false);
        </method>
        
        <method name="isWheelLocked">
            return this.wheelLocked;
        </method>
        
        <method name="zoomIn">
            this.zoom(-this.stepInMs);        	
        </method>
        
        <method name="zoomOut">
            this.zoom(this.stepInMs);          
        </method>
        
        <method name="plot_onmousedown" args="ignore=null">
            var plot = this.manipulRef.getControlledChart().getPlot();
            
            this.previous_drag_x = plot.getMouse("x");
            
            //simple "cursor" attribute does not work
            //plot.setAttribute("cursor", "closedhand");
            //lz.Cursor.setCursorGlobal("closedhand");
        </method>
        
        <method name="plot_onmouseup" args="ignore=null">
            var plot = this.manipulRef.getControlledChart().getPlot();
            this.plot_onmousedragx();
            
            //simple "cursor" attribute does not work
            //plot.setAttribute("cursor", "openhand");
            //lz.Cursor.setCursorGlobal("openhand");
        </method>
        
        <method name="plot_onmouseover" args="ignore=null">
            this.unlockwheel();
        </method>
            
        <method name="plot_onmouseout" args="ignore=null">
            this.lockwheel();
        </method>
        
        <method name="plot_onmousedragx" args="ignore=null">
            var lvChartPlot = this.manipulRef.getControlledChart().getPlot();
            var currentX = lvChartPlot.getMouse("x");
            var minDelta = 3;
            
            var delta = Math.abs(this.previous_drag_x - currentX);
            var scale = this.manipulRef.getControlledChart().getDomainAxis().scale;
            
            var scrollMs = Math.round((delta / scale));
            
            if(scrollMs &gt; this.stepInMs){
                scrollMs = this.stepInMs;
            }
            
            if(delta &gt; minDelta){            	
                Debug.write("scrollMins, step: " + scrollMs + ", " + this.stepInMs);            	
                if(this.previous_drag_x &lt; currentX){
                    this.scroll(-scrollMs);
                }else{
                    this.scroll(scrollMs);
                }            
                this.previous_drag_x = currentX;
            }
        </method>
        
        <method name="manip_onchartloaded" args="ignore=null">
            this.hideInfoWindow();
            this.isDataOnFirstRequest = this.manipulRef.getControlledChart().isAnyData();
        </method>
        
        <method name="manip_ontimeout" args="ignore=null">
            this.hideInfoWindow();
        </method>
        
        <method name="manip_onserveroutofmem" args="ignore=null">
            Debug.write("!!!!!! OUT OF MEMORY ON SERVER !!!!!!");
            this.setMode(LvChartModes.STANDALONE);
            this.hideInfoWindow();
            
            if(this.infoOkWndRef != null){
                this.infoOkWndRef.show(this.outOfMemoryMessage);
            }
            
            this.manipulRef.getControlledChart().drawChart(); //draw chart
        </method>
        
        <method name="doOnSessionExpired" args="ignore=null">
            Debug.write("!!! Session expired");
            this.hideInfoWindow();
        </method>
        
        <method name="setDefaultCustomization">
            var chartObj = this.manipulRef.getControlledChart();
            chartObj.legendDescr.copyData(this.defaultLegendDescr);
            
            //bg type is background
            this.manipulRef.bgStyle = "color";
            
            //set auto calculation for all axises
            chartObj.setAutoCalcToAllAxises();
        </method>
        
        <method name="setManipulator" args="manipulator">
            this.manipulRef = manipulator;
            this.updatePanel();
        </method>
        
        <method name="getManipulator">
            return this.manipulRef;
        </method>
        
        <method name="setInfoWnd" args="infoWnd">
            this.infoWndRef = infoWnd;
        </method>
        
        <method name="setOutOfMemoryMessage" args="str">
            this.outOfMemoryMessage = str;
        </method>
        
        <method name="setInfoOkWnd" args="info_wnd">
            if(info_wnd["show"] &amp;&amp; (info_wnd.show instanceof Function)){
                this.infoOkWndRef = info_wnd;
            }else{
                Debug.error("lvChartControlPanel: info window should have method 'show'.");
            }
        </method>
        
        <method name="updatePanel">
            //var lvChartPlot = this.manipulRef.getControlledChart().getPlot();
            var lvChartPlot = this.manipulRef.getControlledChart().getPointsPlot();
            
            //lvChartPlot.setAttribute("cursor", "openhand");
            //lz.Cursor.setCursorGlobal("openhand");
            
            //handlers for plot events
            this.plotDownDel = new lz.Delegate(this, "plot_onmousedown");
            this.plotDownDel.register(lvChartPlot, "onmousedown");
            
            this.plotUpDel = new lz.Delegate(this, "plot_onmouseup");
            this.plotUpDel.register(lvChartPlot, "onmouseup");
                        
            this.plotOverDel = new lz.Delegate(this, "plot_onmouseover");
            this.plotOverDel.register(lvChartPlot, "onmouseover");
            
            this.plotOutDel = new lz.Delegate(this, "plot_onmouseout");
            this.plotOutDel.register(lvChartPlot, "onmouseout");
            
            //handler for manipulator events
            this.manipChartLoadDel = new lz.Delegate(this, "manip_onchartloaded");
            this.manipChartLoadDel.register(this.manipulRef, "onchartloaded");
            
            this.manipChartTimeoutDel = new lz.Delegate(this, "manip_ontimeout");
            this.manipChartTimeoutDel.register(this.manipulRef, "ontimeout");
            
            this.manipChartErrorDel = new lz.Delegate(this, "manip_ontimeout");
            this.manipChartErrorDel.register(this.manipulRef, "onerror");
            
            this.manipChartMemDel = new lz.Delegate(this, "manip_onserveroutofmem");
            this.manipChartMemDel.register(this.manipulRef, "onserveroutofmem");
            
            this.manipExpired = new lz.Delegate(this, "doOnSessionExpired");
            this.manipExpired.register(this.manipulRef, "onsessionexpired");
        </method>
        
        <method name="setStartTimeInMs" args="timeInMs">
            this.manipulRef.startTimeInMsSelected = timeInMs;
            this.onstartdate.sendEvent(timeInMs);
        </method>
        
        <method name="setEndTimeInMs" args="timeInMs">
            this.manipulRef.endTimeInMsSelected = timeInMs;
            this.onenddate.sendEvent(timeInMs);
        </method>
        
        <method name="getStartTimeInMs">
            return Number(this.manipulRef.startTimeInMsSelected);
        </method>
        
        <method name="getEndTimeInMs">
            return Number(this.manipulRef.endTimeInMsSelected);
        </method>
        
        <method name="zoom" args="ms">
            var shiftedStart = this.getStartTimeWithShift(-ms);
            var shiftedEnd = this.getEndTimeWithShift(ms);
            
            if((shiftedStart &gt;= shiftedEnd) || 
                (this.getStartTimeInMs() == shiftedStart &amp;&amp; this.getEndTimeInMs() == shiftedEnd)){
                Debug.write("Zoom canceled because of edge limits.");
                return;
            }
            
            if(!this.isDataOnFirstRequest &amp;&amp; !this.isStandAlone()){
                //disguise request - it looks like zoom for user
                this.disguisedRequest(params, shiftedStart, shiftedEnd);
                return;
            }

            if(!this.isStandAlone()){
                this.manipulRef.getControlledChart().freeHilitedPoints();
                this.manipulRef.getControlledChart().setPointsHiliteOnMouseMove(false);
                
                this.manipulRef.zoomChartInMs(shiftedStart, shiftedEnd);
                this.calcStep();
                this.manipulRef.getZoomData(this.getStartTimeInMs(), this.getEndTimeInMs(), this.percent);
                
                this.manipulRef.getControlledChart().setPointsHiliteOnMouseMove(true);
            }
            
            this.manipulRef.getControlledChart().hilitePointsByCursor();
        </method>
        
        <method name="scroll" args="ms">
            var isForward = ms &gt; 0 ? true : false;

            var shiftedStart = this.getStartTimeWithShift(ms);
            var shiftedEnd = this.getEndTimeWithShift(ms);
            
            if((shiftedStart &gt;= shiftedEnd) || (isForward &amp;&amp; this.getEndTimeInMs() == shiftedEnd) || 
               (!isForward &amp;&amp; this.getStartTimeInMs() == shiftedStart)){
                
                Debug.write("Scroll canceled because of edge limits.");
                return;
            }
            
            if(!this.isDataOnFirstRequest &amp;&amp; !this.isStandAlone()){
                //disguise request - it looks like scroll for user
                this.disguisedRequest(params, shiftedStart, shiftedEnd);
                return;
            }
            
            if(!this.isStandAlone()){
                this.manipulRef.getControlledChart().freeHilitedPoints();
                this.manipulRef.getControlledChart().setPointsHiliteOnMouseMove(false);
                
                this.manipulRef.scrollMs(shiftedStart, shiftedEnd);
                this.manipulRef.getExtra(isForward);
                
                this.manipulRef.getControlledChart().setPointsHiliteOnMouseMove(true);
            }
            
            this.manipulRef.getControlledChart().hilitePointsByCursor();
        </method>
        
        <method name="requestDataForChart" args="params">
            this.setMode(LvChartModes.NORMAL);			
            if(this.requestData(params)){
                this.showInfoWindow();
            }
        </method>
        
        <method name="disguisedRequest" args="params, startTime, endTime">
            params.remove("endTime"); params.remove("beginTime");
            params.addValue("endTime", endTime); params.addValue("beginTime", startTime);
            
            this.setEndTimeInMs(endTime);
            this.setStartTimeInMs(startTime);
            
            this.requestData(params);
        </method>
        
        <method name="requestData" args="params">
            this.calcStep();
            this.params = params;
            this.isDataOnFirstRequest = false;
            return manipulRef.requestData(params, this.percent);
        </method>
        
        <method name="calcStep">
            var t = this.manipulRef.endTimeInMsSelected - this.manipulRef.startTimeInMsSelected;
            t = Math.round(t * this.percent / 2);
            t = (t != 0) ? t : 15 * 60 * 1000;
            this.setAttribute("stepInMs", t);
            return t;
        </method>
        
        <method name="showInfoWindow">
            Debug.write("!!showInfoWindow: " + this.manipulRef.getControlledChart().chartID);
            if(this.infoWndRef){
                this.infoWndRef.open();
                this.infoWndRef.bringToFront();
            }
        </method>
        
        <method name="hideInfoWindow">
            Debug.write("!!hideInfoWindow: " + this.manipulRef.getControlledChart().chartID);
            if(this.infoWndRef &amp;&amp; this.infoWndRef.visible){
                this.infoWndRef.close();
            }
        </method>
        
        <method name="getStartTimeWithShift" args="shiftInMs">
            var newStartMs = this.getStartTimeInMs() + shiftInMs;
            if(newStartMs &lt; this.startBoundMs){
                newStartMs = this.startBoundMs;
            }
            return newStartMs;
        </method>
        
        <method name="getEndTimeWithShift" args="shiftInMs">
            var newEndMs = this.getEndTimeInMs() + shiftInMs;
            if(newEndMs &gt; this.endBoundMs){
                newEndMs = this.endBoundMs;
            }
            return newEndMs;
        </method>
        
        <method name="getMode">
            return this.mode;
        </method>
        
        <method name="setMode" args="mode">
            this.setAttribute("mode", mode);
        </method>
        
        <method name="isStandAlone">
            return (this.getMode() == LvChartModes.STANDALONE) ? true : false;
        </method>
        
    </class>
</library>