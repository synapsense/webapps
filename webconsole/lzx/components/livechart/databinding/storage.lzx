<library>
    <include href="../textannotation/textannotation.lzx"/>   
    <include href="../descriptors/seriedescr.lzx"/>
    <include href="../utilclasses/valueranges.lzx"/>
    <include href="../utilclasses/symbolvalue.lzx"/>
    <include href="../../../data/securedataset.lzx" />
    
    <class name="storagedataset" extends="securedataset">
        <handler name="onerror">
            Debug.write("dataset '" + this.name + "': onerror");
            parent.onerror.sendEvent();
        </handler>
        
        <handler name="ontimeout">
            Debug.write("dataset '" + this.name + "': ontimeout");
            parent.ontimeout.sendEvent();
        </handler>
        
        <method name="doReq">
            super.doRequest();
            Debug.write(this.getSrc() + "?" + this.getQueryString());
        </method>
    </class>
    
    <class name="storage" extends="node">
        <storagedataset name="ChartData" querytype="POST" type="http" src="getLivechartsData" request="false" timeout="180000">
            <handler name="ondata">
                //vg.gTime = new Date();
                Debug.write("Storage ChartData ondata");
                classroot.mapDataToSeries(classroot.timeSeries, classroot.valSeries, this, classroot.onseriesretrieved);
            </handler>
        </storagedataset>
        
        <storagedataset name="ExtraData" querytype="POST" type="http" src="getLivechartsData" request="false" timeout="180000">
            <handler name="ondata">
                Debug.write("Storage ExtraData ondata");
                classroot.mapDataToSeries(classroot.timeSeriesExtra, classroot.valSeriesExtra, this, classroot.onextraretrieved);
            </handler>
        </storagedataset>
        
        <storagedataset name="ZoomData" querytype="POST" type="http" src="getLivechartsData" request="false" timeout="180000">
            <handler name="ondata">
                Debug.write("Storage ZoomData ondata");
                classroot.mapDataToSeries(classroot.timeSeriesZoom, classroot.valSeriesZoom, this, classroot.onzoomretrieved);
            </handler>
        </storagedataset>
        
        <attribute name="isForward" value="true"/>
            
        <attribute name="timeSeries" value="$once{new Array()}"/>
        <attribute name="valSeries" value="$once{new Array()}"/>
    
        <attribute name="timeSeriesExtra" value="$once{new Array()}"/>
        <attribute name="valSeriesExtra" value="$once{new Array()}"/>
        
        <attribute name="timeSeriesZoom" value="$once{new Array()}"/>
        <attribute name="valSeriesZoom" value="$once{new Array()}"/>
        
        <attribute name="propIds" value="$once{new Array()}"/>
        <attribute name="serieDescrs" value="$once{new Array()}"/>
        
        <attribute name="textAnnotations" value="$once{new Array()}"/>
        
        <event name="onseriesretrieved"/>
        <event name="onextraretrieved"/>
        <event name="onzoomretrieved"/>
        
        <event name="onreadydata"/>
        <event name="onnodata"/>
        <event name="ontimeout"/>
        
        <event name="onerror"/>
        
        <event name="onserveroutofmem"/>
        <event name="onsessionexpired"/>
        
        <!-- PUBLIC SECTION -->
        <method name="renewStorage" args="params">
            propIds.splice(0, propIds.length); //clear
            for(var i = 0; i &lt; serieDescrs.length; i++){
                this.serieDescrs[i].destroy();
            }
            this.serieDescrs.splice(0, serieDescrs.length); //clear
            this.textAnnotations.splice(0, textAnnotations.length); //clear
            
            requestDataFromServer(this.ChartData, params);
        </method>
        
        <method name="requestExtraData" args="params, is_Forward">
            this.isForward = is_Forward;
            requestDataFromServer(this.ExtraData, params);
        </method>
        
        <method name="requestZoomData" args="params">
            requestDataFromServer(this.ZoomData, params);
        </method>
        
        <method name="getTimeSeries">
            return this.timeSeries;
        </method>
        
        <method name="getValSeries">
            return this.valSeries;
        </method>
        
        <!-- PRIVATE SECTION -->
        <method name="requestDataFromServer" args="data_set, params">
            if(params != null){
                data_set.setQueryString(params);
            }
            data_set.doReq();
        </method>
        
        <method name="mapDataToSeries" args="timeArray, valueArray, data_set, event_done">
            //temp: session expiration handling
            if(data_set.getPointer().xpathQuery("error/code/text()") != null){
                var code = data_set.getPointer().xpathQuery("error/code/text()");
                if(code == 10 || code == 11) {
                    this.onsessionexpired.sendEvent();
                    return;
                }        		
            }

            if(data_set.getPointer().xpathQuery("result/exception/last()") != null){
                //temp: we have only one exeption - outOfMemory
                this.onserveroutofmem.sendEvent();
                return;
            }
            
            retrieveTextAnnotations(data_set);
            
            timeArray.splice(0, timeArray.length); //clear
            valueArray.splice(0, valueArray.length); //clear
            
            var serieCount = parseInt(data_set.getPointer().xpathQuery("result/serie/last()"));
            
            if(serieCount != null){
                retrieveSeries(timeArray, valueArray, data_set, event_done);
            }else{
                Debug.write("no data retrieved.");
                event_done.sendEvent();
            }
        </method>
        
        <method name="retrieveTextAnnotations" args="data_set">
            var anCount = parseInt(data_set.getPointer().xpathQuery("result/annotations/annotation/last()"));
            
            if(isNaN(anCount) || anCount == 0){
                return; //there is no any annotation
            }
            
            var beginTime = Number(data_set.getPointer().xpathQuery("result/annotations/annotation[1]/@time"));
            var endTime = Number(data_set.getPointer().xpathQuery("result/annotations/annotation[" + anCount + "]/@time"));
            
            var length = this.textAnnotations.length;
            
            var storedBeginTime = null;
            var storedEndTime = null;
            
            if(length &gt; 0){
                storedBeginTime = this.textAnnotations[0].getTime();
                storedEndTime = this.textAnnotations[length - 1].getTime();
            }
            
            if(storedBeginTime != null &amp;&amp;
               (storedBeginTime &lt;= beginTime &amp;&amp; storedEndTime &gt;= endTime)){
                //Debug.write("!!! retrieveTextAnnotations, we already have these annotations; RETURN.");
                return;
            }
            
            var preList = [];
            var postList = [];
            
            var dp = data_set.getPointer();        	   	
            if(dp.setXPath("result/annotations/annotation[1]")){
                do{
                    var currTime = Number(dp.xpathQuery("@time"));
                    
                    if(this.textAnnotations.length == 0){
                        postList.push(new TextAnnotation(dp.xpathQuery("@id"), currTime, dp.xpathQuery("descr/text()")));
                    }else if(currTime &lt; storedBeginTime){
                        preList.push(new TextAnnotation(dp.xpathQuery("@id"), currTime, dp.xpathQuery("descr/text()")));
                    }else if(currTime &gt; storedEndTime){
                        postList.push(new TextAnnotation(dp.xpathQuery("@id"), currTime, dp.xpathQuery("descr/text()")));
                    }
                }while(dp.selectNext());
            }
            dp.destroy();
                        
            var newArr = preList.concat(this.textAnnotations);
            newArr = newArr.concat(postList);
            
            this.textAnnotations = newArr;        	
        </method>
        
        <method name="retrieveSeries" args="timeArray, valueArray, data_set, event_done">
            var serieCount = parseInt(data_set.getPointer().xpathQuery("result/serie/last()"));        	
                        
            if(this.propIds.length == 0){
                var cor = serieCount == 1 ? 0 : 1;
                for(var serieIndex = 0; serieIndex &lt; serieCount; serieIndex++){
                    if(data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/v/last()") != null){
                        valueArray.push(data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/v/text()").split(","));
                        parseN(valueArray[valueArray.length - 1]);
                
                        timeArray.push(data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/t/text()").split(","));
                        parseN(timeArray[timeArray.length - 1]);
                    }else{
                        valueArray.push(new Array());
                        timeArray.push(new Array());
                    }

                    var pId = data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/@pid");
                    this.propIds.push(pId);
                    
                    var serieName = data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/@name");
                    var serieUnit = data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/@u");
                    var serieRangesStr = data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/@ranges");                    
                    var serieSymbolValuesStr = data_set.getPointer().xpathQuery("result/serie[" + (serieIndex + cor) + "]/@symbolvalues"); 
                    var symbolVals = null;
                    var serieRanges = null;
                    //MEMORY LEAK!!! SHOULD BE FIXED.
                    if(serieRangesStr != null){
                        serieRanges = new lz.valueranges(this);
                        serieRanges.setDataFromString(serieRangesStr);
                    }
                    if(serieSymbolValuesStr != null){
                        symbolVals = new lz.symbolvalues(this);
                        symbolVals.setDataFromString(serieSymbolValuesStr);
                    }
                    
                    this.serieDescrs.push(new lz.seriedescr(this, {propID: pId, serieName: serieName, axisName: serieUnit, lineDescr: new lz.linedescr(this, {color: 0x00ff00}), ranges: serieRanges, symbolValues: symbolVals}));
                }
            }else{
                for(var serieIndex = 0; serieIndex &lt; serieCount; serieIndex++){
                    var pId = this.propIds[serieIndex];
                    
                    if(data_set.getPointer().xpathQuery("result/serie[@pid='" + pId + "']/v/last()") != null &amp;&amp;
                       data_set.getPointer().xpathQuery("result/serie[@pid='" + pId + "']/t/last()") != null ){
                        valueArray.push(data_set.getPointer().xpathQuery("result/serie[@pid='" + pId + "']/v/text()").split(","));
                        parseN(valueArray[valueArray.length - 1]);
                
                        timeArray.push(data_set.getPointer().xpathQuery("result/serie[@pid='" + pId + "']/t/text()").split(","));
                        parseN(timeArray[timeArray.length - 1]);
                    }else{
                        valueArray.push(new Array());
                        timeArray.push(new Array());
                    }
                }
            }
            if ($debug) {
               Debug.write("Data was retrieved. Event '" + event_done._dbg_eventName +"' was sent.");
            }
            event_done.sendEvent();
        </method>
        
        <method name="parseN" args="array">
            for(var i = 0; i &lt; array.length; i++){
                array[i] = Number(array[i]);
            }
        </method>
        
        <method name="isAnyData" args="timeArray">
            var result = false;        	
            for(var i = 0; i &lt; timeArray.length; i++){
                if(timeArray[i].length != 0){
                    result = true;
                    break;
                }
            }        	
            return result;
        </method>
        
        <handler name="onzoomretrieved">
            Debug.write("storage: onzoomretrieved event came.");
            //if(this.timeSeriesZoom.length == 0 || this.timeSeriesZoom[0].length == 0){
            if(!this.isAnyData(this.timeSeriesZoom)){
                Debug.write("No zoom data was retrieved.");
                this.onnodata.sendEvent();
            }else{
                this.timeSeries = this.timeSeriesZoom.splice(0, this.timeSeriesZoom.length);
                this.valSeries = this.valSeriesZoom.splice(0, this.valSeriesZoom.length);
                this.onreadydata.sendEvent();
            }
        </handler>
        
        <handler name="onextraretrieved">
            Debug.write("storage: onextraretrieved event came.");
        
            //if(this.timeSeriesExtra.length == 0 || this.timeSeriesExtra[0].length == 0){
            if(!this.isAnyData(this.timeSeriesExtra)){
                Debug.write("No extra data was retrieved.");
                this.onnodata.sendEvent();
            }else{
                if(this.isForward){
                    for(var i = 0; i &lt; this.timeSeries.length; i++){
                        this.timeSeries[i] = this.timeSeries[i].concat(this.timeSeriesExtra[i]);
                        this.valSeries[i] = this.valSeries[i].concat(this.valSeriesExtra[i]);
                    }
                }else{
                    var extraPointsCount = 0;
                    for(var i = 0; i &lt; this.timeSeries.length; i++){
                        extraPointsCount = this.timeSeriesExtra[i].length;
                        this.timeSeries[i] = this.timeSeriesExtra[i].concat(this.timeSeries[i]);
                        this.valSeries[i] = this.valSeriesExtra[i].concat(this.valSeries[i]);
                        //to do: recalculate filteredindexes..
                    }
                }
                this.onreadydata.sendEvent();
            }
        </handler>
        
        <handler name="onseriesretrieved">
            Debug.write("storage: onseriesretrieved event came.");
            //if(this.timeSeries.length == 0 || this.timeSeries[0].length == 0){
            if(!this.isAnyData(this.timeSeries)){
                Debug.write("No data was retrieved.");
                this.onnodata.sendEvent();
            }else{
                this.onreadydata.sendEvent();
            }
        </handler>
    
    </class>
    
</library>