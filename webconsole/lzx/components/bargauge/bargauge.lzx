<library>
    <class name="bargauge" extends="view">
        <attribute name="label" type="string" value=""/>
        <attribute name="value" type="number" value="50"/>
        
        <attribute name="min" type="number" value="0"/>
        <attribute name="max" type="number" value="100"/>
        
        <attribute name="rMin" type="number" value="0"/>
        <attribute name="rMax" type="number" value="0"/>
        <attribute name="aMin" type="number" value="0"/>
        <attribute name="aMax" type="number" value="0"/>
        <attribute name="showranges" type="boolean" value="false"/>
        
        <attribute name="aColor" value="0xF3FF3F" />
        <attribute name="rColor" value="0x42973B" />
        <attribute name="outOfRangeColor" value="0xEE3F3E" />
        <attribute name="neutralColor" value="0xC1E1C1" />
        
        <attribute name="bordersize" type="number" value="2"/>
        <attribute name="bordercolor" type="number" value="0x999999"/>
        
        <attribute name="barwidth" type="number" value="100"/>
        
        <attribute name="timestamp" type="string" value=""/>
        <attribute name="objId" type="string" value=""/>
        
        <attribute name="decimalPrecision" value="2"/>
        
        <view name="content">
            <simplelayout axis="y"/>
            
            <view name="dataview">
                <simplelayout axis="x" spacing="3"/>
                
                <view name="barview">
                    <view name="bar" height="${15 + 2 * classroot.bordersize}" width="${classroot.barwidth + 2 * classroot.bordersize}" bgcolor="${classroot.bordercolor}">
                        <view name="rangeViews" clip="true" bgcolor="${classroot.neutralColor}"
                               width="${parent.width - 2 * classroot.bordersize}"
                               height="${parent.height - 2 * classroot.bordersize}"
                               y="${classroot.bordersize}" x="${classroot.bordersize}">
                               
                            <simplelayout axis="x"/>
                            <view name="min_aMin" bgcolor="${classroot.outOfRangeColor}" visible="${classroot.showranges}" height="${parent.height}"/>
                            <view name="aMin_rMin" bgcolor="${classroot.aColor}" visible="${classroot.showranges}" height="${parent.height}"/>
                            <view name="rMin_rMax" bgcolor="${classroot.rColor}" visible="${classroot.showranges}" height="${parent.height}"/>
                            <view name="rMax_aMax" bgcolor="${classroot.aColor}" visible="${classroot.showranges}" height="${parent.height}"/>
                            <view name="aMax_max" bgcolor="${classroot.outOfRangeColor}" visible="${classroot.showranges}" height="${parent.height}"/>
                        </view>
                        
                        <method name="updateRanges">
                            if (classroot.showranges){
                                var pixelsScale = classroot.getPixelsScale();
                                
                                rangeViews.min_aMin.setAttribute("width", (classroot.aMin - classroot.min) * pixelsScale);
                                rangeViews.aMin_rMin.setAttribute("width", (classroot.rMin - classroot.aMin) * pixelsScale);
                                rangeViews.rMin_rMax.setAttribute("width", (classroot.rMax - classroot.rMin) * pixelsScale);
                                rangeViews.rMax_aMax.setAttribute("width", (classroot.aMax - classroot.rMax) * pixelsScale);
                                rangeViews.aMax_max.setAttribute("width", (classroot.max - classroot.aMax) * pixelsScale);
                            }
                        </method>
                    </view>
                    
                    <view name="line" y="${classroot.bordersize + 1}" width="1" height="${15 - 2 * classroot.bordersize - 1}" bgcolor="0x000000"/> <!-- I hope this is temporary -->
                    <view name="upper_arrow" resource="../resources/bargauge/arrow.png" y="15" opacity="0.7" rotation="180"/> <!-- I hope this is temporary -->
                    
                    <view name="arrow" resource="../resources/bargauge/arrow.png" y="4" opacity="0.7"/>
                    
                </view>
                
                <view name="valueview">
                    <text name="value">
                        <handler name="onvalue" reference="classroot">
                            this.setAttribute("text", NumberFormat.format(classroot.value, classroot.decimalPrecision));
                        </handler>
                    </text>
                </view>
            </view>
            
            <view name="tsview">
                <text name="ts" text="${classroot.timestamp}"/>
            </view>
        </view>
        
        <view name="warningView" visible="false" width="${classroot.barwidth}">
            <text name="warningTxt" text="X" align="center" fontsize="16" fontstyle="bold" fgcolor="0xFF0000"/>
        </view>
                
        <method name="update">
        <![CDATA[
            var val = getLimitedValue();
                                
            var pixelsScale = this.getPixelsScale();
            
            var currentValueInPixels = Math.ceil((val - this.min) * pixelsScale);
            var shiftToArrow = Math.ceil(this.getArrow().width / 2) - 1;
            var borderShift = this.bordersize;
            
            this.getArrow().setAttribute("x", currentValueInPixels - shiftToArrow + borderShift);
            
            this.getArrow().parent.upper_arrow.setAttribute("x", this.getArrow().x + this.getArrow().parent.upper_arrow.width); //I hope this is temporary
            this.getArrow().parent.line.setAttribute("x", currentValueInPixels + borderShift); //I hope this is temporary

            if (this.value != null && !isNaN(this.value) && this.value != -2000 && this.value != -3000) {
                this.content.setAttribute("visible", true);
                this.warningView.setAttribute("visible", false);
            } else {
                this.content.setAttribute("visible", false);
                this.warningView.setAttribute("visible", true);
                this.warningView.warningTxt.setAttribute("fgcolor", this.value == -2000 ? "orange" : 0xFF0000);
            }
            
            this.getBar().updateRanges();
        ]]>
        </method>
        
        <method name="getPixelsScale">
            return this.barwidth / Math.abs(this.max - this.min);
        </method>
        
        <method name="getLimitedValue">
        <![CDATA[
            var v = this.value;
            if (this.value > this.max) {
                v = this.max;
            } else if (this.value < this.min) {
                v = this.min;
            }
            return v;
        ]]>
        </method>
        
        <method name="getBar">
            return this.content.dataview.barview.bar;
        </method>
        
        <method name="getArrow">
            return this.content.dataview.barview.arrow;
        </method>
        
    </class>
</library>