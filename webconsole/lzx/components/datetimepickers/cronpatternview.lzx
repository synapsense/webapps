<library>
<include href="../synapmodaldialog.lzx"/>
<include href="../frameview.lzx"/>
<include href="../synapcombobox.lzx"/>
<include href="../synapbutton.lzx"/>
<include href="../synapradiobutton.lzx"/>
<include href="../synapedittext.lzx"/>
<include href="../synaptext.lzx"/>
<include href="../synapcheckbox.lzx"/>
<include href="../synaplist.lzx"/>
<include href="periodpicker.lzx"/>
<include href="../../messages/confirmdialog.lzx"/>
<include href="datetimepicker.lzx"/>
        
    <class name="cronpatternview" extends="view" pixellock="true">
        <attribute name="cronExpression" type="string" value=""/>
        <attribute name="isNew" type="boolean" value="true"/>
        <attribute name="yearlyVisible" type="boolean" value="false"/>
        <attribute name="editorType" type="number" value="0"/>
        <attribute name="lastError" type="string" value=""/>
        <attribute name="privileges" type="string" value=""/>
        
        <event name="oncronerrorfixed"/>
        
        <handler name="oninit">
            getStartDateTimePicker().DateTimeView.DateTimePicker.setAttribute("visible", false);
        </handler>
        
        <dataset name="crondata">            
            <monthlydayorder>
                <item value="1">cron/first</item>
                <item value="2">cron/second</item>
                <item value="3">cron/third</item>
                <item value="4">cron/fourth</item>
                <item value="L">cron/last</item>
            </monthlydayorder>
            
            <dayname>
                <item shortname="SUN" value="1">cron/sunday</item>
                <item shortname="MON" value="2">cron/monday</item>
                <item shortname="TUE" value="3">cron/tuesday</item>
                <item shortname="WED" value="4">cron/wednesday</item>
                <item shortname="THU" value="5">cron/thursday</item>
                <item shortname="FRI" value="6">cron/friday</item>
                <item shortname="SAT" value="7">cron/saturday</item>
            </dayname>
            
            <monthname>
                <item value="JAN" days="31" number="1">cron/january</item>    
                <item value="FEB" days="28" number="2">cron/february</item>    
                <item value="MAR" days="31" number="3">cron/march</item>    
                <item value="APR" days="30" number="4">cron/april</item>    
                <item value="MAY" days="31" number="5">cron/may</item>    
                <item value="JUN" days="30" number="6">cron/june</item>    
                <item value="JUL" days="31" number="7">cron/july</item>    
                <item value="AUG" days="31" number="8">cron/august</item>    
                <item value="SEP" days="30" number="9">cron/september</item>    
                <item value="OCT" days="31" number="10">cron/october</item>    
                <item value="NOV" days="30" number="11">cron/november</item>    
                <item value="DEC" days="31" number="12">cron/december</item>
            </monthname>
        </dataset>


        <method name="setExpression" args="cronExpr">
            if (cronExpr == null) {
                this.cronExpression = "";
                this.reset();
                return;
            }
            
            this.cronExpression = cronExpr == null ? "" : cronExpr;
            getCronTextBox().setAttribute("text", this.cronExpression);
            getEditorTypeRadiogroup().selectItem(0);
            
            var cronArr = this.cronExpression.split(" ");            
            if(this.cronExpression == "" || cronArr.length &lt; 6 || isNaN(cronArr[2]) || isNaN(cronArr[1]) || isNaN(cronArr[0])) {
                Debug.write("CRON.setExpression [" + this.cronExpression + "] manual.");
                getEditorTypeRadiogroup().selectItem(1);
                return;
            }
            
            var date = new Date();
            date.setHours(Number(cronArr[2]), Number(cronArr[1]), Number(cronArr[0]), 0);
            getStartDateTimePicker().setDate(date);

            //isDaily every day
            if(cronArr[5] == "?"  &amp;&amp; cronArr[4] == "*" &amp;&amp; cronArr[3] == "*") {
                Debug.write("CRON.setExpression [" + this.cronExpression + "] every week day.");
                getPatternsContainer().dailyPatternContainer.dailyRadiogroup.selectItem(0);
                getRecurrenceRadiogroup().selectItem(0);
                return;                
            }
            
            //isDaily every workday
            if((cronArr[5] == "2-6" || cronArr[5] == "MON-FRI") &amp;&amp; cronArr[4] == "*" &amp;&amp; cronArr[3] == "?") {
                Debug.write("CRON.setExpression [" + this.cronExpression + "] every work day.");
                getPatternsContainer().dailyPatternContainer.dailyRadiogroup.selectItem(1);
                getRecurrenceRadiogroup().selectItem(0);
                return;
            }

            
            //isWeekly
            if(cronArr[5].indexOf("-") == -1 &amp;&amp; cronArr[5].indexOf("#") == -1 &amp;&amp; cronArr[5].indexOf("L") == -1 &amp;&amp; cronArr[3] == "?" &amp;&amp; cronArr[4] == "*") {
                var dayCheckboxes = getWeeklyDayCheckboxes();
                for(var i = 0; i &lt; dayCheckboxes.length; i++) {
                    dayCheckboxes[i].setAttribute("value", false);
                }
                var days = cronArr[5].split(",");
                for(var i = 0; i &lt; days.length; i++) {
                    var dayName;
                    if(isNaN(days[i])) {
                        dayName = days[i];
                    } else {
                        dayName = crondata.getPointer().xpathQuery("dayname/item[@value='" + days[i] + "']/@shortname");
                    }
                    var cb = getDayCheckboxByName(dayName);
                    if(cb == null) {
                        getEditorTypeRadiogroup().selectItem(1);
                        Debug.write("CRON.setExpression [" + this.cronExpression + "] manual.");                        
                        return;
                    }                    
                    cb.setAttribute("value", true);
                }
                Debug.write("CRON.setExpression [" + this.cronExpression + "] weekly.");                        
                getRecurrenceRadiogroup().selectItem(1);
                return;
            }
            
            //isMonthly
            if(cronArr[4] == "*") {
                if(cronArr[3] == "?") {
                    getPatternsContainer().monthlyPatternContainer.monthlyRadiogroup.selectItemAt(1);
                    
                    var dayOrder;
                    var dayName;
                    if(cronArr[5].indexOf("#") == -1) {
                        dayOrder = "L";
                        dayName = cronArr[5].substring(0, cronArr[5].indexOf("L"));
                    } else {
                        dayName = cronArr[5].split("#")[0];
                        dayOrder = cronArr[5].split("#")[1];
                    }
                    if(isNaN(dayName)) {
                        dayName = crondata.getPointer().xpathQuery("dayname/item[@shortname='" + dayName + "']/@value");
                    }
                    
                    getPatternsContainer().monthlyPatternContainer.theDayView.monthlyDayOrderCombo.selectItem(dayOrder);
                    getPatternsContainer().monthlyPatternContainer.theDayView.dayNameCombo.selectItem(dayName);                    
                } else {
                    getPatternsContainer().monthlyPatternContainer.monthlyRadiogroup.selectItemAt(0);
                    getPatternsContainer().monthlyPatternContainer.dayOfView.dayNumber.setAttribute("text", cronArr[3]);
                }
                Debug.write("CRON.setExpression [" + this.cronExpression + "] mothly.");                        
                getRecurrenceRadiogroup().selectItem(2);
                return;
            }
            
            //isYearly
            if(this.yearlyVisible) {
                getRecurrenceRadiogroup().selectItem(3);
                var month = cronArr[4];
                if(!isNaN(month)) {
                    month = crondata.getPointer().xpathQuery("monthname/item[@number='" + month + "']/@value");                    
                }
                if(cronArr[3] != "?") {
                    getPatternsContainer().yearlyPatternContainer.yearlyRadiogroup.selectItemAt(0);
        
                    getPatternsContainer().yearlyPatternContainer.dayOfView.monthCombo.selectItem(month);
                    getPatternsContainer().yearlyPatternContainer.dayOfView.dayNumber.setAttribute("text", cronArr[3]);
                } else {
                    getPatternsContainer().yearlyPatternContainer.yearlyRadiogroup.selectItemAt(1);
                    var dayOrder;
                    var dayName;
                    if(cronArr[5].indexOf("#") == -1) {
                        dayOrder = "L";
                        dayName = cronArr[5].substring(0, cronArr[5].indexOf("L"));
                    } else {
                        dayName = cronArr[5].split("#")[0];
                        dayOrder = cronArr[5].split("#")[1];
                    }
                    if(isNaN(dayName)) {
                        dayName = crondata.getPointer().xpathQuery("dayname/item[@shortname='" + dayName + "']/@value");
                    }                
                    getPatternsContainer().yearlyPatternContainer.theDayView.dayOrderCombo.selectItem(dayOrder);
                    getPatternsContainer().yearlyPatternContainer.theDayView.dayNameCombo.selectItem(dayName);
                    getPatternsContainer().yearlyPatternContainer.theDayView.monthCombo.selectItem(month);
                }
            }
            Debug.write("CRON.setExpression [" + this.cronExpression + "] manual.");                        
            getEditorTypeRadiogroup().selectItem(1);
        </method>
        
        <method name="isValid" args="showErrorMessage=false">
            if(editorType == 1) {
                return true;
            }
            var patterns = getPatternsContainer();
            switch(getRecurrenceRadiogroup().value) {
                case 1:
                    var dayCheckboxes = getWeeklyDayCheckboxes();
                    var selected = false;
                    for(var i = 0; i &lt; dayCheckboxes.length; i++) {
                        if(dayCheckboxes[i].value == true) {
                            selected = true;
                            break;
                        }
                    }
                    if(!selected) {
                        InfoDialog.show("cron/should_select_day");
                        return false;
                    }
                break;
                
                case 2:
                    if(patterns.monthlyPatternContainer.monthlyRadiogroup.value == 0) {
                        var dayNumber = parseInt(patterns.monthlyPatternContainer.dayOfView.dayNumber.text);
                        if(isNaN(dayNumber)) {
                            var dayNumberText = patterns.monthlyPatternContainer.dayOfView.dayNumber.text;
                            dayNumberText = dayNumberText == null ? "" : dayNumberText;
                            if(dayNumberText.toString().toUpperCase() != "L") {
                                InfoDialog.show("cron/invalid_value");
                                return false;
                            }
                        } else if(dayNumber == 0) {
                            InfoDialog.show("cron/invalid_day_zero");
                            return false;
                        } else if(dayNumber > 28) {
                            ConfirmDialog.show("cron/will_set_last_dayofmonth", this, "monthlyDayOnOk");
                            return false;
                        }
                    }
                break;
                
                case 3:
                    if(patterns.yearlyPatternContainer.yearlyRadiogroup.value == 0) {
                        var date = parseInt(patterns.yearlyPatternContainer.dayOfView.dayNumber.value);
                        if(date == 0) {
                            InfoDialog.show("cron/invalid_day_zero");
                            return false;
                        }
                        var month = patterns.yearlyPatternContainer.dayOfView.monthCombo.value;
                        var daysInMonth = parseInt(crondata.getPointer().xpathQuery("monthname/item[@value='" + month + "']/@days"));
                        if(date > daysInMonth) {
                            InfoDialog.show("cron/invalid_day_for_month");
                            return false;
                        }
                    }
                break;
            }
            return true;
        </method>

        <method name="monthlyDayOnOk" args="ignore=null">
            this.setAttribute("cronExpression", this.getCronExpression(true));
            var patterns = getPatternsContainer();
            patterns.monthlyPatternContainer.dayOfView.dayNumber.setAttribute("text", "L");
            oncronerrorfixed.sendEvent();
        </method>
        
        <method name="getCronExpression" args="replaceMonthDay=false">
            if(editorType == 1) {
                return getCronTextBox().text;
            }
            
            var date = getStartDateTimePicker().getDate();
            var cron = "";
            var patterns = getPatternsContainer();

            cron += date.seconds + " ";
            cron += date.minutes + " ";
            cron += date.hours + " ";

            switch(getRecurrenceRadiogroup().value) {                
                case 0:
                    if(patterns.dailyPatternContainer.dailyRadiogroup.value == 0) {
                        cron += "* * ?"; 
                    } else {
                        cron += "? * 2-6";
                    }
                break;
                
                case 1:
                    var checkboxes = getWeeklyDayCheckboxes();
                    var daysSelected = "";
                    for(var i = 0; i &lt; checkboxes.length; i++) {
                        var pref = daysSelected == "" ? "" : ",";
                        if(checkboxes[i].value == true) {
                            daysSelected += pref + (i + 1);
                        }
                    }
                    cron += "? * " + daysSelected;
                break;
                
                case 2:
                    if(patterns.monthlyPatternContainer.monthlyRadiogroup.value == 0) {
                        if(replaceMonthDay == true) {
                            cron += "L * ?";
                        } else {
                            cron += patterns.monthlyPatternContainer.dayOfView.dayNumber.text.toUpperCase() + " * ?";
                        }
                    } else {
                        var dayOrder = patterns.monthlyPatternContainer.theDayView.monthlyDayOrderCombo.value;
                        var dayName = patterns.monthlyPatternContainer.theDayView.dayNameCombo.value;                        
                        cron += "? * ";
                        cron += dayName;
                        cron += dayOrder == "L" ? "L" : "#" + dayOrder; 
                    }
                break;
                
                case 3:
                    if(patterns.yearlyPatternContainer.yearlyRadiogroup.value == 0) {
                        var month = patterns.yearlyPatternContainer.dayOfView.monthCombo.value;
                        var date = patterns.yearlyPatternContainer.dayOfView.dayNumber.value;
                        cron += date + " " + month + " ? *";
                    } else {
                        var dayOrderValue = patterns.yearlyPatternContainer.theDayView.dayOrderCombo.value;
                        var dayOrder = dayOrderValue == "L" ? "L" : "#" + dayOrderValue;
                        var dayName = patterns.yearlyPatternContainer.theDayView.dayNameCombo.value;
                        var month = patterns.yearlyPatternContainer.theDayView.monthCombo.value;
                        cron += "? " + month + " " + dayName + dayOrder + " *" 
                    }
                break;
            }
            
            return cron;    
        </method>
                        
        <method name="reset">
            getStartDateTimePicker().DateTimeView.DateTimePicker.setAttribute("visible", false);
            this.setAttribute("isNew", true);
            getStartDateTimePicker().reset();
            var patterns = getPatternsContainer();
            
            var currentDate = getStartDateTimePicker().getDate();
            var currentDayNumber =  currentDate.day + 1;
            var currentDayName = crondata.getPointer().xpathQuery("dayname/item[@value='" + currentDayNumber + "']/@shortname");
            
            getEditorTypeRadiogroup().selectItem(0);
            getCronTextBox().setAttribute("text", "");
            
            getRecurrenceRadiogroup().selectItem(0);
            //Daily
            patterns.dailyPatternContainer.dailyRadiogroup.selectItemAt(0);
            
            //Weekly
            var dayCheckboxes = getWeeklyDayCheckboxes();
            for(var i = 0; i &lt; dayCheckboxes.length; i++) {
                dayCheckboxes[i].setValue(dayCheckboxes[i].name == currentDayName ? true : false);
            }
            
            //Monthly
            patterns.monthlyPatternContainer.monthlyRadiogroup.selectItemAt(0);
            patterns.monthlyPatternContainer.dayOfView.dayNumber.setAttribute("text", currentDate.date);
            
            var dayOrder:int = ((currentDate.date - 1) / 7) + 1;
            var dayOrderItem = dayOrder != 5 ? dayOrder.toString() : "L";
            patterns.monthlyPatternContainer.theDayView.monthlyDayOrderCombo.selectItem(dayOrderItem);
            patterns.monthlyPatternContainer.theDayView.dayNameCombo.selectItem(currentDayNumber.toString());
            
            //Yearly
            patterns.yearlyPatternContainer.yearlyRadiogroup.selectItemAt(0);
            patterns.yearlyPatternContainer.dayOfView.monthCombo.selectItemAt(currentDate.month);
            patterns.yearlyPatternContainer.dayOfView.dayNumber.setAttribute("text", currentDate.date);
            
            patterns.yearlyPatternContainer.theDayView.dayOrderCombo.selectItemAt(dayOrderItem);
            patterns.yearlyPatternContainer.theDayView.dayNameCombo.selectItem(currentDayNumber.toString());
            patterns.yearlyPatternContainer.theDayView.monthCombo.selectItemAt(currentDate.month);
        </method>

        <method name="selectEditorType" args="idx">
            this.setAttribute("editorType", idx);
        </method>
        
        <method name="selectRecurrencePattern" args="idx">            
            var patterns = getPatternsContainer();
            patterns.dailyPatternContainer.setAttribute("visible", false);
            patterns.weeklyPatternContainer.setAttribute("visible", false);
            patterns.monthlyPatternContainer.setAttribute("visible", false);
            patterns.yearlyPatternContainer.setAttribute("visible", false);
            
            var pattern;            
            switch(idx) {
                case 0:
                    pattern = patterns.dailyPatternContainer;
                break;
                case 1:
                    pattern = patterns.weeklyPatternContainer;
                break;
                case 2:
                    pattern = patterns.monthlyPatternContainer;
                break;
                case 3:
                    pattern = patterns.yearlyPatternContainer;
                break;
            }
            pattern.setAttribute("visible", true);
        </method>
        
        <method name="getPatternsContainer">
            return simpleEditorView.expressionContainer.patternsContainer;
        </method>

        <method name="getStartDateTimePicker">
            return simpleEditorView.startDateTimePicker;
        </method>

        <method name="getRecurrenceRadiogroup">
            return simpleEditorView.expressionContainer.recurrenceRadiogroup;
        </method>

        <method name="getWeeklyDayCheckboxes">
            var patterns = getPatternsContainer();
            var checkBoxes = [];
            var subv = patterns.weeklyPatternContainer.weekDaysView.subviews;
            for(var i = 0; i &lt; subv.length; i++) {
                if(subv[i] instanceof lz.synapcheckbox) {
                    checkBoxes.push(subv[i]);
                }
            }
            return checkBoxes;
        </method>

        <method name="getDayCheckboxByName" args="dayName">
            var checkboxes = getWeeklyDayCheckboxes();
            for(var i = 0; i &lt; checkboxes.length; i++) {
                if(checkboxes[i].name == dayName) {
                    return checkboxes[i];
                }
            }
            return null;
        </method>
        
        <method name="getCronTextBox">
            return editorSelectView.cronText;
        </method>
        
        <method name="getSimpleEditorView">
            return simpleEditorView;
        </method>
        
        <method name="getEditorTypeRadiogroup">
            return editorSelectView.editorTypeRadiogroup;
        </method>
        
        
        
        
        <simplelayout axis="y" spacing="5"/>
        
        <view name="editorSelectView">
            <simplelayout axis="x" spacing="5"/>            
            <radiogroup name="editorTypeRadiogroup" layout="class:simplelayout;axis:x;spacing:5;" enabled="${UserPrivileges.isAllowed(classroot.privileges)}">
                <synapradiobutton prompt="cron/simple" value="0"/>
                <synapradiobutton prompt="cron/cron_expression" value="1"/>
                
                <handler name="onselect">
                    classroot.selectEditorType(this.value);
                </handler>
            </radiogroup>
            <synapedittext name="cronText" width="120" y="-6" enabled="${classroot.editorType == 1 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
        </view>
        
        <view name="simpleEditorView" width="${immediateparent.width}" pixellock="true">
            <simplelayout axis="y" spacing="5"/>
            <datetimepicker name="startDateTimePicker" showTimeType="1" prevText="text/start_time"
                            showTime="true" settings="${UserPreference}" showNowTime="true"
                            enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
            
            <view name="expressionContainer" width="${immediateparent.width}" pixellock="true">            
                <simplelayout axis="x" spacing="5"/>
                
                <radiogroup name="recurrenceRadiogroup" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                    <synapradiobutton prompt="cron/daily" value="0"/>
                    <synapradiobutton prompt="cron/weekly" value="1"/>
                    <synapradiobutton prompt="cron/monthly" value="2"/>
                    <synapradiobutton prompt="cron/yearly" value="3" visible="${classroot.yearlyVisible}"/>
                    
                    <handler name="onselect">
                        classroot.selectRecurrencePattern(this.value);
                    </handler>
                </radiogroup>
                
                <view width="1" height="${immediateparent.height}" bgcolor="0x0"/>
                
                <view name="patternsContainer" width="${immediateparent.width - this.x}" pixellock="true">
                    
                    <view name="dailyPatternContainer" width="${immediateparent.width}">
                        <radiogroup name="dailyRadiogroup" layout="class: simplelayout; axis: y; spacing:10" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                            <synapradiobutton name="everyDayRB" prompt="cron/every_weekday" value="0"/>
                            <synapradiobutton name="everyWorkDayRB" prompt="cron/every_workday" value="1"/>
                        </radiogroup>
                    </view>
                    
                    <view name="weeklyPatternContainer" width="${classroot.width}">                      
                        <view name="weekDaysView" width="${immediateparent.width}">
                            <wrappinglayout axis="x" spacing="5"/>
                            <synapcheckbox name="SUN" prompt="cron/sunday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>                            
                            <synapcheckbox name="MON" prompt="cron/monday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synapcheckbox name="TUE" prompt="cron/tuesday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synapcheckbox name="WED" prompt="cron/wednesday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synapcheckbox name="THU" prompt="cron/thursday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synapcheckbox name="FRI" prompt="cron/friday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synapcheckbox name="SAT" prompt="cron/saturday" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                        </view>
                    </view>
                    
                    <view name="monthlyPatternContainer" pixellock="true" width="${immediateparent.width}">
                        <radiogroup name="monthlyRadiogroup" layout="class: simplelayout; axis: y; spacing:10" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                            <synapradiobutton name="dayOfRB" prompt="cron/day" value="0"/>
                            <synapradiobutton name="theDayRB" prompt="cron/the" value="1"/>
                        </radiogroup>
                        
                        <view name="dayOfView" x="${parent.monthlyRadiogroup.theDayRB.width +
                                                        parent.monthlyRadiogroup.dayOfRB.x + 5}"
                                                  y="${parent.monthlyRadiogroup.dayOfRB.y - 4}">
                            <simplelayout axis="x" spacing="5"/>
                            <synapedittext name="dayNumber" width="40" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                            <synaptext prompt="cron/of_every_month" y="3"/>
                        </view>
                        <view name="theDayView" x="${parent.monthlyRadiogroup.theDayRB.width +
                                                  parent.monthlyRadiogroup.dayOfRB.x + 5}"
                                                y="${parent.monthlyRadiogroup.theDayRB.y - 3}" pixellock="true">
                            <simplelayout axis="x" spacing="5"/>
                            
                            <synapcombobox name="monthlyDayOrderCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/monthlydayorder/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                            
                            <synapcombobox name="dayNameCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/dayname/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                            
                            <synaptext prompt="cron/of_every_month" y="3"/>
                        </view>
                    </view>
                    
                    <view name="yearlyPatternContainer" pixellock="true" visible="${classroot.yearlyVisible}">
                        <radiogroup name="yearlyRadiogroup" layout="class: simplelayout; axis: y; spacing:10" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                            <synapradiobutton name="everyMonthDayRB" prompt="cron/every" value="0"/>
                            <synapradiobutton name="theDayRB" prompt="cron/the" value="1"/>
                        </radiogroup>
                        
                        <view name="dayOfView" x="${parent.yearlyRadiogroup.everyMonthDayRB.width +
                                                        parent.yearlyRadiogroup.everyMonthDayRB.x + 5}"
                                                  y="${parent.yearlyRadiogroup.everyMonthDayRB.y - 4}" pixellock="true">
                            <simplelayout axis="x" spacing="5"/>
                            
                            <synapcombobox name="monthCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/monthname/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                            
                            <synapedittext name="dayNumber" width="40" pattern="[0-9]*" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}"/>
                        </view>
                        <view name="theDayView" x="${parent.yearlyRadiogroup.everyMonthDayRB.width +
                                                  parent.yearlyRadiogroup.everyMonthDayRB.x + 5}"
                                                y="${parent.yearlyRadiogroup.theDayRB.y - 3}" pixellock="true">
                            
                            <simplelayout axis="x" spacing="5"/>
                            
                            <synapcombobox name="dayOrderCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/monthlydayorder/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                            
                            <synapcombobox name="dayNameCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/dayname/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                            
                            <synaptext prompt="cron/of" y="3"/>
                            
                            <synapcombobox name="monthCombo" enabled="${classroot.editorType == 0 &amp;&amp; UserPrivileges.isAllowed(classroot.privileges)}">
                                <synaptextlistitem datapath="local:classroot.crondata:/monthname/item"
                                                   prompt="$path{'text()'}" value="$path{'@value'}"/>
                            </synapcombobox>
                        </view>
                    </view>                    
                </view>         
            </view>
        </view>
    </class>
    
</library>