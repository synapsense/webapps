<library>
    <include href="../../synapcombobox.lzx"/>
    
    <class name="statcombobox" extends="synapcombobox">
        <attribute name="statisticItemId" type="string"/>
        <attribute name="sourceDataset"/>
        <attribute name="subItemXPath" type="string"/>
        <attribute name="subItemIdAttr" type="string"/>
        <attribute name="subItemNameAttr" type="string"/>
        <attribute name="listItemsReplicatorName" type="string"/>
        <attribute name="syncStatisticOnRefresh" type="boolean" value="false"/>
        <attribute name="sortAttr" type="string" value="sortval"/>
        <attribute name="inTopAttr" type="string" value="inTop"/>
        
        <event name="statisticadded"/>
        
        <handler name="ondata" reference="this.sourceDataset">
            if(this.syncStatisticOnRefresh){
                this.setAttribute("syncStatisticOnRefresh", false);
                this.syncStatistic();
            }
            this.addStatisticToDataset();
        </handler>
        
        <!-- This method can be overriden -->
        <method name="getStatisticItemId">
            return this.statisticItemId;
        </method>
        
        <!-- This method can be overriden -->
        <method name="getAvailableStatisticItemIds">
            return [this.statisticItemId];
        </method>
        
        <method name="addStatisticToDataset">
            var dp = sourceDataset.getPointer();
            var xmlTemplNodes = this.xpathQueryArray(dp, this.subItemXPath);
            
            dp = new LzDatapointer(null);
            var topSubItems = UserStat.getTopFrequentSubItems(this.getStatisticItemId(), 3);
            for (var j = 0; j &lt; xmlTemplNodes.length; j++) {
                dp.p = xmlTemplNodes[j];
                
                var curentSubItemId = xmlTemplNodes[j].attributes[this.subItemIdAttr];
                var inTop = false;
                var idNum = 0;
                for(var i = 0; i &lt; topSubItems.length; i++){
                    if(topSubItems[i].id == curentSubItemId){
                        inTop = true;
                        idNum = topSubItems[i].frequency;
                        break;
                    }
                }
                
                dp.setNodeAttribute(this.sortAttr, idNum);
                if(inTop){
                    dp.setNodeAttribute(this.inTopAttr, "true");
                }else{
                    dp.setNodeAttribute(this.sortAttr, xmlTemplNodes[j].attributes[this.subItemNameAttr]);
                }
            }            
            dp.destroy();
            
            if(this[listItemsReplicatorName] instanceof LzReplicationManager){
                this[listItemsReplicatorName].setOrder("@" + this.sortAttr, this._numericDescAndStringAscSort); //re-sort
            }
            
            this.statisticadded.sendEvent(null);
        </method>
        
        <method name="syncStatistic">
            var dp = sourceDataset.getPointer();
            var subItemIds = this.xpathQueryArray(dp, this.subItemXPath + "/@" + this.subItemIdAttr);            

            var availableStatisticItemIds = this.getAvailableStatisticItemIds();
            for(var i = 0; i &lt; availableStatisticItemIds.length; i++){
                UserStat.syncSubItems(availableStatisticItemIds[i], subItemIds);
            }
        </method>
        
        <method name="xpathQueryArray" args="dp, xpath">
            var result = dp.xpathQuery(xpath);
            if(result == null){
                result = [];
            }else if(!(result instanceof Array)){
                result = [result];
            }
            return result;
        </method>
        
        <method name="_numericDescAndStringAscSort" args="a,b"><![CDATA[
            var result;
            
            var x = Number( a );
            var y = Number( b );

            //if a variable is NaN, then it won't equal itself
            var xisnum = x == x;
            var yisnum = y == y;

            if ( xisnum == yisnum ){
                if ( !xisnum ){
                    x = a;
                    y = b;
                }
                if ( x > y ){
                    result = 1;
                } else if ( x == y ){
                    result = 0;
                } else {
                    result = -1;
                }
                
                if(!xisnum){
                    result = -result;
                }
            } else if ( xisnum ){
                result = 1;
            } else {
                result = -1;
            }
            
            return result;
        ]]></method>
        
    </class>
</library>