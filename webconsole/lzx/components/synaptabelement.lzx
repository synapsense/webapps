<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     Aug 5, 2010 12:22:32 PM                                                        
     aklushin                                                                
     ====================================================================== -->
<library>
  <include href="../lang/promptlistener.lzx"/>
  <include href="lz/tabelement.lzx"/>
  <include href="basesynaptabelement.lzx"/>
  <include href="animateview.lzx"/>
  <include href="synapbutton.lzx"/>
    
    <resource name="synaptabelement_rsc">
        <frame src="resources/synaptabelement/tabelement_dslct_mid.png" />
        <frame src="resources/synaptabelement/tabelement_mo_mid.png" />
        <frame src="resources/synaptabelement/tabelement_slct_mid.png" />
        <frame src="resources/synaptabelement/tabelement_disable_mid.png" />
    </resource>
  
  <!-- CLASS: synaptabelement -->
  <!-- synaptabelements are children of basetabslider. -->
  <class name="synaptabelement" extends="basesynaptabelement" styleable="true">
    <attribute name="prompt" type="string"/>
    <promptListener/>
    <!--- Default placement for views placed in tabelement is 'content'. -->
    <attribute name="defaultplacement" value="content" type="string"/>

    <!--- The top inset for content appearing within a tabelement. Default
          is the containing tabslider's inset_top value. -->
    <attribute name="inset_top" value="${_parentcomponent.inset_top}"/>
    <!--- The right inset for content appearing within a tabelement. Default
          is the containing tabslider's inset_right value. -->
    <attribute name="inset_right" value="${_parentcomponent.inset_right}"/>
    <!--- The bottom inset for content appearing within a tabelement. Default
          is the containing tabslider's inset_bottom value. -->
    <attribute name="inset_bottom" value="${_parentcomponent.inset_bottom}"/>
    <!--- The left inset for content appearing within a tabelement. Default
          is the containing tabslider's inset_left value. -->
    <attribute name="inset_left" value="${_parentcomponent.inset_left}"/>

    <!--- The x position of the text field in the bkgnd area. -->
    <attribute name="text_x" value="4"/>

    <!--- The height of the shadow. -->
    <attribute name="shadowheight" value="4"/>

    <!--- Boolean to set the shadow of this tabelement. -->
    <attribute name="shadowvisible" value="true"/>

    <!--- This is not being used.
          @keywords private -->
    <attribute name="contentopacity" value="0.5"/>

    <!--- The offset to shift bkgnd contents when the mouse is down.
          @keywords private -->
    <attribute name="_mdoffset" value="2"/>

    <!--- Insets the header art on the x-axis. -->
    <attribute name="headerpadding"   value="0" />
    
    <attribute name="useparentmaxheight" type="boolean" value="false"/>
    
    <setter name="height" args="h"> <![CDATA[
        super.setAttribute("height",null);
    ]]> </setter>
    <view name="top" width="100%" height="${parent.headerheight}"
          oninit="this.bringToFront()" bgcolor="${null}">
                 
        <basebutton name="bkgnd" resource="synaptabelement_rsc" styleable="false"
                    x="${classroot.headerpadding}"
                    width="${parent.width - 2*classroot.headerpadding}" 
                    height="100%" stretches="both" >
                    
            <text name="txt"
                      x="${classroot.text_x}"
                      y="${(parent.height-this.height)/2}"
                      fgcolor="${!classroot.selected ? 0xFFFFFF : 0x000000}"
                      fontstyle="bold"
                      text="${classroot.text}" resize="true"/>
            <method name="showUp" args="sd=null"> <![CDATA[
              if (!classroot.selected) {
                  super.showUp();
                  this.txt.setAttribute('yoffset', 0);
              }
            ]]> </method>
            <method name="showOver" args="sd=null">
              if (!classroot.selected) {
                  super.showOver();
              }
            </method>
            <handler name="onmousedown">
              classroot._parentcomponent.select(classroot);
            </handler>
        </basebutton>

        <handler name="onselected" reference="classroot">
          if (classroot.selected) {
            this.bkgnd.showDown();
            this.bkgnd.txt.setAttribute('yoffset', -classroot._mdoffset);
          } else {
            this.bkgnd.showUp();
            this.bkgnd.txt.setAttribute('yoffset', 0);
          }
        </handler>
    </view>
    <view name="container"
          width="${classroot.width}" bgcolor="${null}">
        <animateview name="content"
                     innerbgcolor="${classroot.bgcolor}"
                     bordercolor="0xAAAAAA"
                     width="${parent.width}"
                     bgopacity="1"
                     top_offset="${classroot.inset_top}"
                     bottom_offset="${classroot.inset_bottom}"
                     left_offset="${classroot.inset_left}"
                     right_offset="${classroot.inset_right}"
                     floatduration="${classroot._parentcomponent.slideduration}">
            <attribute name="_updateHeightDel" type="expression" value="${new LzDelegate(this,'_updateHeight')}"/>
            <attribute name="__sizeaxis" value="${classroot.useparentmaxheight?classroot._parentcomponent.availableheight:
                                                    (this.axis == 'y'?
                    (this.content.y+this.content.height+this.bottom_offset):
                    (this.content.x+this.content.width+this.right_offset))}"/>
            <attribute name="visible" value="${this.height==0?false:true}"/>
            <handler name="on__sizeaxis" args="ignore=null"> <![CDATA[
                if (classroot.selected) {
                    lz.Timer.resetTimer(this._updateHeightDel,100);
                }
            ]]> </handler>
            <method name="_updateHeight" args="ignore=null"> <![CDATA[
                //update height
                if (this.opened) {
                    this.opened = false;
                }
                this.open();
            ]]> </method>
        </animateview>
    </view>
    <simplelayout axis="y" spacing="0"/>
    
    <!--- Overrides baselistitem's setHilite.
          @param boolean dohilite: true if you want to hilite-->
    <method name="setHilite" args="dohilite">
        /*top.bkgnd.txt.setAttribute('fgcolor',
            dohilite ? style.texthilitecolor : style.textcolor);
        if (dohilite)
             top.bkgnd.onmouseover.sendEvent(top.bkgnd);
        else
             top.bkgnd.onmouseout.sendEvent(top.bkgnd);*/
        return;
    </method>

    <!--- @keywords private -->
    <method name="_applystyle" args="s">
        if (this.style != null) {
            super._applystyle(s);
            //this.top.bkgnd.txt.setAttribute("fgcolor", s.textcolor);
            if (this.bgcolor == null) {
                this.container.setAttribute("bgcolor", s.bgcolor);
            }
        }
    </method>

    <!--- @keywords private -->
    <method name="doSpaceDown">
        this.top.bkgnd.doSpaceDown();
    </method>

    <!--- @keywords private -->
    <method name="doSpaceUp">
        this.top.bkgnd.doSpaceUp();
    </method>
    
    <method name="setSelected" args="isSelected" >
        //Because in basetabelement.setSelected() parent's methods are called, but 
        //_parentcomponent's methods should be called.
        if ( !_parentcomponent._initcomplete ) return;
        if ( isSelected ) {
            this.container.content.open();
        } else {
            this.container.content.close();
        }
        //Instead of super.setSelected( isSelected );
        this.selected = isSelected;
        if (this.onselect.ready) this.onselect.sendEvent(this);
        if (this.onselected.ready) this.onselected.sendEvent(this);
    </method>
    <!--method name="open" args="h,d,withAnimation" > <![CDATA[
        this.container.content.open();
    ]]> </method>
    <method name="close" args="h,d"> <![CDATA[
        this.container.content.close();
    ]]> </method-->
  </class>
</library>
