<library>
<include href="synaptabs.lzx"/>

    <class name="multitabsbar" extends="synaptabsbar" width="${immediateparent.width}" clip="true">
        <attribute name="wcoeff" type="number" value="0.5"/>
        <attribute name="updateLayoutDel"/>

        <handler name="oninit">
            for(var i = 0; i &lt; layouts.length; i++) {
                layouts[i].releaseLayout();
            }
        </handler>
                
        <method name="requestUpdateLayout">
            if(this.updateLayoutDel == null) {
                this.updateLayoutDel = new LzDelegate(this, 'updateLayout');
            }
            if(this.isinited == true) {
                lz.Timer.resetTimer(this.updateLayoutDel, 100);
            }
        </method>
                
        <method name="updateLayout" args="ignore=null"><![CDATA[
            var tabs = getTabs();
            setRealWidth(tabs);
            alignTabsByLine(tabs);
                                    
            var rows = getRows(tabs);            
            setTabsPositions(rows);
            flipTabsbarRows(tabs);
            updateRowsWidth(tabs);
        ]]></method>

        <method name="setRealWidth" args="tabs">
            for(var i = 0; i &lt; tabs.length; i++) {
                var w:int = tabs[i].tabview.labeltext.getTextWidth() + 2 * tabs[i].padding;
                tabs[i].setAttribute("width", w);
            }     
        </method>

        <method name="setTabsPositions" args="rows"><![CDATA[
            var rowH = subviews[0].height;
            this.setAttribute("height", rows.length * rowH);
            
            for(var r = 0; r < rows.length; r++) {
                var tabrow = rows[r];
                var dy:int = r * rowH;
                var dx:int = 0;
                for(var t = 0; t < tabrow.length; t++) {
                    tabrow[t].setAttribute("x", dx);
                    tabrow[t].setAttribute("y", dy);
                    dx += tabrow[t].width; 
                }                
            }            
        ]]></method>
        
        <method name="getRows" args="tabs"><![CDATA[
            var rows = [];

            var minRowsNum:int = getMinRowsNum(tabs);
            var fullTabsW = getFullTabsWidth(tabs);            
            var rowDivX = fullTabsW / minRowsNum;
            var offsetX = rowDivX;
            var offsetTab = 0;
            
            for(var i = 0; i < minRowsNum; i++) {
                var tabdata = getTabDataForX(tabs, offsetX); 
                offsetX += rowDivX;
                
                var tabrow = [];
                for(var t = offsetTab; t < tabs.length; t++) {
                    if(t == tabdata.index) {
                        if(tabdata.div == 'left') {                            
                            break;
                        } else {
                            tabrow.push(tabs[t]);
                            offsetTab++;
                            break;
                        }
                    }
                    tabrow.push(tabs[t]);
                    offsetTab++;
                }
                rows.push(tabrow);
            }
            return rows;
        ]]></method>

        <method name="alignTabsByLine" args="tabs">
            var dx:int = 0;
            for(var i = 0; i &lt; tabs.length; i++) {
                tabs[i].setAttribute("x", dx);
                dx += tabs[i].width; 
            }            
        </method>
        
        <method name="getMinRowsNum" args="tabs">
            var barW = this.width;
            var widthsum = 0;
            var minRowsN = 1;
            
            for(var i = 0; i &lt; tabs.length; i++) {
                widthsum += tabs[i].width;
                if(widthsum &gt; barW) {
                    minRowsN++;
                    widthsum = tabs[i].width;
                }
            }
            return minRowsN;
        </method>
                
        <method name="getFullTabsWidth" args="tabs">
            var widthsum = 0;
            for(var i = 0; i &lt; tabs.length; i++) {
                widthsum += tabs[i].width;
            }
            return widthsum;            
        </method>
                
        <method name="getTabDataForX" args="tabs,divx"><![CDATA[
            var tabdata = {index:tabs.length-1, div:'right'};
            for(var i = 0; i < tabs.length; i++) {
                if(tabs[i].x <= divx && (tabs[i].x + tabs[i].width) > divx) {
                    tabdata.index = i;
                    tabdata.x = divx;
                    var halftabw = tabs[i].width / 2;
                    if((tabs[i].x + halftabw) > divx) {
                        tabdata.div = 'left';
                    }
                    return tabdata;
                }
            }
            return tabdata;
        ]]></method>
        
        <method name="_showScroller">
            return true;
        </method>
        
        <method name="checkTabVisibility" args="tab" returns="Boolean">
            return true;
        </method>
        
        <method name="scrollto" args="index">
            return true;
        </method>
                        
        <method name="getTabs">
            var tabs = [];
            for(var i = 0; i &lt; subviews.length; i++) {
                if(subviews[i] instanceof lz.tab) {
                    if(subviews[i].visible == true) {
                        tabs.push(subviews[i]);
                    }
                }
            }
            return tabs;
        </method>
        
        <method name="flipTabsbarRows" args="tabs">
            var bottomRowY = -12345;
            
            for(var i = 0; i &lt; tabs.length; i++) {
                if(tabs[i].y &gt; bottomRowY) {
                    bottomRowY = tabs[i].y; 
                }                
            }
            
            var selectedy = value.y;            
            
            if(bottomRowY != selectedy) {
                var selectedRowTabs = [];
                var bottomRowTabs = [];
                
                for(var i = 0; i &lt; tabs.length; i++) {
                    if(tabs[i].y == selectedy) {
                        selectedRowTabs.push(tabs[i]);
                    }
                 
                    if(tabs[i].y == bottomRowY) {
                        bottomRowTabs.push(tabs[i]);
                    }
                }
                
                for(var i = 0; i &lt; selectedRowTabs.length; i++) {
                    selectedRowTabs[i].setAttribute("y", bottomRowY);
                }                
                for(var i = 0; i &lt; bottomRowTabs.length; i++) {
                    bottomRowTabs[i].setAttribute("y", selectedy);
                }                
            }
        </method>
        
        <method name="updateRowsWidth" args="tabs">
            var rows = getCurrentRows(tabs);
            for(var i = 0; i &lt; rows.length; i++) {
                var rcoeff = getRowWidthCoeff(rows[i]);
                //if(rcoeff &gt; wcoeff) {
                    alignTabsByLine(rows[i]);
                    growRowWidth(rows[i]);
                //}
            }
        </method>

        <method name="getCurrentRows" args="tabsorig">
            var rows = [];
            var tabs = [];
            var maxiterations = tabsorig.length;
            
            for(var i = 0; i &lt; tabsorig.length; i++) {
                tabs.push(tabsorig[i]);
            }
            
            while(tabs.length != 0) {
                var minY = getMinY(tabs);
                var toRemove = [];
                for(var i = 0; i &lt; tabs.length; i++) {
                    if(tabs[i].y == minY) {
                        toRemove.push(tabs[i]);
                    }
                }
                rows.push(toRemove);

                for(var i = 0; i &lt; toRemove.length; i++) {
                    var idr = tabs.indexOf(toRemove[i]);
                    if(idr != -1) {
                        tabs.splice(idr, 1);
                    }
                }
                maxiterations--;
                if(maxiterations &lt; 0) {
                    break;
                }
            }
            return rows;            
        </method>
        
        <method name="getMinY" args="tabs">
            var minY = 12345;
            for(var i = 0; i &lt; tabs.length; i++) {
                if(tabs[i].y &lt; minY) {
                    minY = tabs[i].y; 
                }                
            }
            return minY;
        </method>
        
        <method name="getRowWidthCoeff" args="rowtabs">
            var w = 0;            
            for(var i = 0; i &lt; rowtabs.length; i++) {
                w += rowtabs[i].width;
            }
            return w / this.width;
        </method>
        
        <method name="growRowWidth" args="rowtabs">
            var tabsw = 0;
            for(var i = 0; i &lt; rowtabs.length; i++) {
                tabsw += rowtabs[i].width;
            }
            
            var togrow = this.width - tabsw;
            var tabdx = togrow / rowtabs.length;
            
            for(var i = 1; i &lt; rowtabs.length; i++) {
                var dx:int = rowtabs[i].x + tabdx * i;
                rowtabs[i].setAttribute("x", dx);
            } 
            for(var i = 0; i &lt; rowtabs.length; i++) {
                var dw:int = rowtabs[i].width + tabdx;
                rowtabs[i].setAttribute("width", dw);
            }
        </method>
    </class>

    <class name="multitab" extends="synaptab">        
        <handler name="ontext">
            var tabsbar = parent;
            tabsbar.requestUpdateLayout();
        </handler>
        
        <handler name="onvisible">
            var tabsbar = parent;
            tabsbar.requestUpdateLayout();
        </handler>
        
        <method name="setSelected" args="s">
            var store_x = this.x;
            var store_y = this.y;
            super.setSelected(s);
            this.setAttribute("x", store_x);
            this.setAttribute("y", store_y);
                        
            if(s == true) {
                var tabsbar = parent;
                tabsbar.requestUpdateLayout();
            }
        </method> 
    </class>

    <class name="multitabs" extends="synaptabs"
           barclass="multitabsbar" tabclass="multitab"/>
    
</library>