<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     Mar 2, 2009 2:55:51 PM                                                        
     pahunov                                                                
     ====================================================================== -->
<library>
    
    <include href="synapscrollbar.lzx" />
    <include href="../newskin/utils/mousewheellocker.lzx" />
    
    <resource name="zoomin" src="./resources/zoomin.png"/>
    <resource name="zoomout" src="./resources/zoomout.png"/>
    <!--resource name="handcursor" src="./resources/handcursor.png"/-->
    
    <!-- Scales its content using mouse wheel or mouse clicks or 
         programmatically on scale events. Support dragging of the zoomed 
         content. Can be used together with birds eye view.
    -->
    <class name="scalableview" width="100" height="100" clip="true">
        <attribute name="scalable" type="boolean" value="true"/>
        <attribute name="scale" type="number" value="1"/>
        <attribute name="minscale" type="number" value="1"/>
        <attribute name="maxscale" type="number" value="10"/>
        <attribute name="step" type="number" value="1"/>
        <attribute name="wheellock" type="boolean" value="false"/>
        <attribute name="defaultplacement" value="content"/>
        <attribute name="zoommode" type="string" value="none"/>
        <attribute name="allowdrag" type="boolean" value="true"/>
        <attribute name="wheelLocker" value="$once{default_unlocked_mousewheellocker}"/>

        <handler name="onallowdrag">
            if (!allowdrag) {
                this.content.dragger.remove();
            }
        </handler>
        
        <method name="unlockwheel" args="ignore=null">
            this.wheellock = false;
        </method>   
        
        <method name="setScale" args="scale">
        <![CDATA[
            if (!this.scalable || this.scale == scale) {
                return;
            }
                        
            var centerXprop = (-this.content.x + (this.width / 2)) / this.content.width;
            var centerYprop = (-this.content.y + (this.height/ 2)) / this.content.height;
            var centerX = this.width * (scale - 1) * centerXprop;
            var centerY = this.height * (scale - 1) * centerYprop;
            
            if(scale > this.scale) {
                this.scale = scale - this.step;
                this.zoomInToPoint(centerX, centerY);
            } else {
                this.scale = scale + this.step;
                this.zoomOutToPoint(centerX, centerY);                
            }
            this.content.setAttribute("x", -centerX);
            this.content.setAttribute("y", -centerY + this.immediateparent.y*(scale-1));
        ]]>
        </method>
        
        <!--Can be overwriten to change scaling algorithm-->
        <method name="zoomIn">
        <![CDATA[
            if (!this.scalable) {
                return;
            }
            if (this.scale < this.maxscale) {
                this.setAttribute("scale", scale + this.step);
            }
        ]]>
        </method>     
        
        <!--Can be overwriten to change scaling algorithm-->
        <method name="zoomOut">
        <![CDATA[
            if (!this.scalable) {
                return;
            }
            if (this.scale > this.minscale) {
                this.setAttribute("scale", scale - this.step);
            }
        ]]>
        </method>   

        <method name="zoomInToPoint" args="mouseX,mouseY">
        <![CDATA[
            var k = this.scale;
            this.zoomIn();
            var k = this.scale/k;
            this.content.setAttribute("x", Math.round(this.content.x - (k - 1) * mouseX));
            this.content.setAttribute("y", Math.round(this.content.y - (k - 1) * mouseY));
        ]]>
        </method>     

        <method name="zoomOutToPoint" args="mouseX,mouseY">
        <![CDATA[
            var k = this.scale;
            this.zoomOut();
            var k = this.scale/k;
            var x = this.content.x - (k - 1) * mouseX;
            if (x > 0) {
                x = 0;
            } else if (x < this.width - this.content.width) {
                x = this.width - this.content.width;
            }
            
            var y = this.content.y - (k - 1) * mouseY;
            if (y > 0) {
                y = 0;
            } else if (y < this.height - this.content.height) {
                y = this.height - this.content.height;
            }
            
            this.content.setAttribute("x", x);
            this.content.setAttribute("y", y);
        ]]>
        </method>     
        
        <method name="scrollToPoint" args="x, y"> <![CDATA[
            var newX = this.width / 2 - x;
            if (newX > 0) {
                newX = 0;
            }
            if (newX + this.content.width < this.width) {
                newX = this.width - this.content.width;
            }
            this.content.setAttribute("x", newX)
        
            var newY = this.height / 2 - y;
            if (newY > 0) {
                newY = 0;
            }
            if (newY + this.content.height < this.height) {
                newY = this.height - this.content.height;
            }
            this.content.setAttribute("y", newY)
        ]]>
        </method>
        
        <method name="isParentVisible" args="v">
            var p = v.parent
            if (p instanceof lz.canvas) {
                return true;
            }
            if (!p.visible) {
                return false;
            } else {
                return isParentVisible(p);
            }
        </method> 
        
        <handler name="onclick" args="v" reference="content">
            if (this.zoommode == "zoomin") {
                this.zoomInToPoint(v.getMouse("x"), v.getMouse("y"));
            }
            if (this.zoommode == "zoomout") {
                this.zoomOutToPoint(v.getMouse("x"), v.getMouse("y"));
            }
        </handler> 

        <handler name="onmousewheeldelta" reference="lz.Keys" args="delta">
        <![CDATA[
            if (!this.scalable || !this.visible || !this.isParentVisible(this) || this.wheellock ||
                this.wheelLocker.isLocked()){
                return;
            }
        
            //if mouse in out of this then return
            if (this.getMouse("x") < 0 ||
                this.getMouse("y") < 0 ||
                this.getMouse("x") > this.width ||
                this.getMouse("y") > this.height) {
                return;  
            }

            var mouseX = this.content.getMouse("x");
            var mouseY = this.content.getMouse("y");
            
            if (delta < 0) {
                this.zoomOutToPoint(mouseX, mouseY);
            } else {
                this.zoomInToPoint(mouseX, mouseY);
            }
            
            if (!this["wheelunlockDel"]) {
                this["wheelunlockDel"] = new lz.Delegate(this, "unlockwheel");
            }
            
            //As getMouse("x") value updates asynchronously we need this delay to get it updated 
            this.wheellock = true;
            lz.Timer.resetTimer(this["wheelunlockDel"], 200);
        ]]>
        </handler>
        
        <!--view name="content" cursor="${classroot.zoommode=='zoomin'?'zoomin':(classroot.zoommode=='zoomout'?'zoomout':'handcursor')}"-->
        <view name="content"
              width="${parent.width*classroot.scale}"
              height="${parent.height*classroot.scale}">

            <handler name="onmousedown">
            <![CDATA[
                if (classroot.allowdrag && classroot.zoommode == "none" && classroot.scale > 1) {
                    dragger.apply();
                }
            ]]>
            </handler>

            <handler name="onmouseup">
                dragger.remove();
            </handler>

            <dragstate name="dragger" drag_max_x="0" drag_max_y="0"
                drag_min_x="${immediateparent.width - immediateparent.content.width - immediateparent.vsb.width}"
                drag_min_y="${immediateparent.height - immediateparent.content.height - immediateparent.hsb.height}"/>
        </view>

        <synapscrollbar axis="x" scrolldelta="${(classroot.scale > 1) ? parent.vsb.width : 0}" scrolltarget="$once{parent.content}" name="hsb" visible="${this.scrollable}" valign="bottom"/>
        <synapscrollbar axis="y" scrolldelta="${(classroot.scale > 1) ? parent.hsb.height : 0}" scrolltarget="$once{parent.content}" name="vsb" visible="${this.scrollable}" align="right" custom_geometry="true" height="${parent.height - parent.hsb.height}" />
    </class>
</library>
