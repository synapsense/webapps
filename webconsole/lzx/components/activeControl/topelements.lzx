<library>
    <include href="./objecthlink.lzx"/>
    <include href="../../lang/langmanager.lzx"/>
    <include href="../../utils/unitsutil.lzx"/>
    
    <class name="roomobjecthlink" extends="objecthlink">
        <!-- override -->
        <method name="handleFindObjectResult" args="res"><![CDATA[
            RoomConfigDialog.close();
        ]]></method>
    </class>
    
    <class name="topelements">
        <attribute name="inputType" type="string" value="$once{'temperature'}"/>
        <attribute name="showIntakes" type="boolean" value="$once{true}"/>
        <attribute name="showCrahs" type="boolean" value="$once{true}"/>

        <attribute name="crahhighestlabel" type="string" value=""/>
        <attribute name="crahlowestlabel" type="string" value=""/>
        <attribute name="intakehighestlabel" type="string" value=""/>
        <attribute name="intakelowestlabel" type="string" value=""/>
        
        <simplelayout axis="y" spacing="5"/>

        <handler name="ondata" args="d"><![CDATA[
            if (!d) {return;}
            
            this.updateIntakeElements();
            this.updateCrahElements();
        ]]></handler>
        
        <method name="getUnits" args="type"><![CDATA[
            if (type === "crah") {
                return UnitsUtil.getUnitsByDataclass("percent");
            } else if (this.inputType === "temperature") {
                return UnitsUtil.getUnitsByDataclass("200");
            } else if (this.inputType === "pressure") {
                return UnitsUtil.getUnitsByDataclass("202");
            }
            return "";
        ]]></method>

        <method name="getPrecision" args=""><![CDATA[
            if (this.inputType === "temperature") {
                return 1;
            } else if (this.inputType === "pressure") {
                return 3;
            }
            return "";
        ]]></method>

        <method name="getLabel" args="strKey"><![CDATA[
            var t = LangManager.getLocaleString(strKey);
            if (t == undefined) {
                t = strKey;
            }
            return t;
        ]]></method>
            
        <method name="setValue" args="control, val, objIds"><![CDATA[
            if(objIds != null && objIds.length > 0) {
                // For now, just show the first object in the link
                // At some point, might want to show a list, or a count.
                control.link.setAttribute('objectId', objIds[0]);
            } else {
                control.link.setAttribute('objectId', null);
            }
            
            var visible = true;
            var text = "";
            var fgColor = 0xFF0000;

            if (val && val.length != 0) {
                text = val;
                fgColor = this.fgcolor;
            }

            control.setAttribute('visible', visible);
            control.value.setAttribute('text', text);
            control.value.setAttribute('fgcolor', fgColor);
        ]]></method>

        <method name="setIntakeValues" args="control, isMax, variance"><![CDATA[
            var str = null;
            var precision = this.getPrecision();

            if (variance.ids.length > 0) {
                var deviation = Math.abs(parseFloat(variance.value) - parseFloat(variance.target)).toFixed(precision);
                if (isMax) {
                    str = LangManager.getFormattedLocaleString("AC_highest_coin_deviation",
                            deviation,
                            this.getUnits("coin"),
                            parseFloat(variance.target).toFixed(precision));
                } else {
                    str = LangManager.getFormattedLocaleString("AC_lowest_coin_deviation",
                            deviation,
                            this.getUnits("coin"),
                            parseFloat(variance.target).toFixed(precision));
                }
            }

            this.setValue(control, str, variance.ids);
        ]]></method>

        <method name="setCrahValues" args="control, variance"><![CDATA[
            var str = null;

            if (variance.ids.length > 0) {
                if (this.inputType === "pressure") {
                    str = LangManager.getFormattedLocaleString("AC_crah_fanspeed",
                            parseFloat(variance.value).toFixed(0), this.getUnits("crah"));
                } else {
                    str = LangManager.getFormattedLocaleString("AC_crah_capacity",
                            parseFloat(variance.value).toFixed(0), this.getUnits("crah"));
                }
            }

            this.setValue(control, str, variance.ids);
        ]]></method>

        <method name="findVariances" args="paths"><![CDATA[
            var ret = {"lowest": {"value": 10000.0, target: 0, "ids": []},
                       "highest": {"value": -10000.0, target: 0, "ids": []}};
            
            var dp = this.datapath.dupePointer();
            dp.setXPath(paths.elementsPath + '[1]');
            
            do {
                if(dp.getNodeName() != paths.xmlNodeName) {
                    continue;
                }
                
                if(dp.xpathQuery(paths.resourcePath) != inputType) {
                    continue;
                }
                
                var strategyStatus = (paths.strategyStatusPath != null) ? dp.xpathQuery(paths.strategyStatusPath) : null;
                var deviceStatus = (paths.deviceStatusPath != null) ? dp.xpathQuery(paths.deviceStatusPath) : null;
                var validStrategyStatus = (strategyStatus == null) || (strategyStatus === "automatic");
                var validDeviceStatus = (deviceStatus == null) || (deviceStatus === "online");

                if (validStrategyStatus && validDeviceStatus) {
                    var value = parseFloat(dp.xpathQuery(paths.valuePath));
                    if(value != null && !isNaN(value)) {
                        var target = (paths.targetPath != null) ? parseFloat(dp.xpathQuery(paths.targetPath)) : null;
                        var objId = dp.xpathQuery(paths.idPath);

                        if (value > ret.highest.value) {
                            ret.highest.value = value;
                            ret.highest.target = target;
                            ret.highest.ids = [];
                            ret.highest.ids.push(objId);
                        } else if (value == ret.highest.value) {
                            ret.highest.ids.push(objId);
                        }

                        if (value < ret.lowest.value) {
                            ret.lowest.value = value;
                            ret.lowest.target = target;
                            ret.lowest.ids = [];
                            ret.lowest.ids.push(objId);
                        } else if (value == ret.lowest.value) {
                            ret.lowest.ids.push(objId);
                        }
                    }
                }
            } while(dp.selectNext())
            
            return ret;
        ]]></method>
    
        <method name="updateIntakeElements"><![CDATA[
            var paths = {"xmlNodeName": 'inputs_status',
                         "elementsPath": this.inputType + '_balancer_status/inputs_status',
                         "valuePath": 'actual/text()',
                         "targetPath": 'target/text()',
                         "resourcePath": 'resource/text()',
                         "idPath": 'id/text()'
            };
            
            var variances = this.findVariances(paths);

            this.setIntakeValues(this.coin.highest, true, variances.lowest);
            this.setIntakeValues(this.coin.lowest, false, variances.highest);
        ]]></method>
        
        <method name="updateCrahElements"><![CDATA[
            var valuePath = (this.inputType === "pressure") ?
                    'pressure_driver_status/setpoint/text()' :
                    'temperature_driver_status/capacity/text()';
            var paths = {"xmlNodeName": 'crahs_status',
                         "elementsPath": 'crahs_status',
                         "valuePath": valuePath,
                         "resourcePath": this.inputType + '_driver_status/resource/text()',
                         "stategyStatusPath": this.rinputType+ '_strategy_status/status/text()',
                         "deviceStatusPath": this.inputType + '_driver_status/status/text()',
                         "idPath": 'id/text()'
            };
            
            var variances = this.findVariances(paths);

            this.setCrahValues(this.crah.highest, variances.highest);
            this.setCrahValues(this.crah.lowest, variances.lowest);
        ]]></method>
        
        <view name="coin" visible="${classroot.showIntakes}">
            <simplelayout axis="y" spacing="5"/>
            <view name="highest">
                <simplelayout axis="x" spacing="1"/>

                <roomobjecthlink name="link"/>
                <synaptext name="value"/>
            </view>
            <view name="lowest">
                <simplelayout axis="x" spacing="1"/>

                <roomobjecthlink name="link"/>
                <synaptext name="value"/>
            </view>
        </view>
        <view name="crah" visible="${classroot.showCrahs}">
            <simplelayout axis="y" spacing="5"/>
            <view name="highest">
                <simplelayout axis="x" spacing="1"/>

                <roomobjecthlink name="link"/>
                <synaptext name="value"/>
            </view>
            <view name="lowest">
                <simplelayout axis="x" spacing="1"/>

                <roomobjecthlink name="link"/>
                <synaptext name="value"/>
            </view>
        </view>
    </class>
</library>