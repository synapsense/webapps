<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     30.08.2010 15:30:24                                                        
     vlebedev                                                                
     ====================================================================== -->
<library>
    <include href="../data/synapdataset.lzx"/>
    <include href="../components/synaptext.lzx"/>
    <include href="../utils/map.lzx"/>
    <include href="../utils/es/escommands.lzx"/>
    <include href="capacityicon.lzx"/>
    
    <resource name="defaultItemResource" src="../presentation/resources/question.png"/>
    
    <synapdataset name="PowerImagingLegends" type="http" request="false" src="getObject"/>
    
    <node name="powerImagingLegendManager" initstage="defer">
        <attribute name="defaultLegends" type="string" value="25,0x009E0F;50,0x00FFFF;75,0xFFFF00;100,0xFF9900;Above Max Threshold,0xCF2A27|20,0xFF9900;40,0xFFFF00;80,0x00FFFF;100,0x009E0F;Above Max Threshold,0xCF2A27"/>
        <attribute name="currentTypeLegend" type="string" value="usedCapacityLegend"/>
        <attribute name="handleDatasetDel" value="$once{new lz.Delegate(this, 'handleDataset')}"/>
        <attribute name="handleResultDel" value="$once{new lz.Delegate(this, 'handleResult')}"/>
        <attribute name="handleObjCreatedDel" value="$once{new lz.Delegate(this, 'handleObjCreatedResult')}"/>
        <attribute name="legendsMap" value="$once{new Map()}"/>
        <attribute name="isLoaded" type="boolean" value="false"/>
        <attribute name="objId" type="string" value=""/>
        <event name="onloaded"/>
        <event name="onupdated"/>
        <event name="onsaved"/>
        
        <method name="getCapacityColor" args="value, legendType"> <![CDATA[
            var ds = this.legendsMap.get(legendType);
            if (!ds) {
                return null;
            }
            var data = ds.getPointer().xpathQuery("range");
            if (!data) {
                return null;
            }
            
            if (!(data instanceof Array)) {
                data = [data];
            }
            
            for (var i = data.length -1; i >= 0; i--) {
                var from = NumberFormat.parse(data[i].attributes["from"]);
                if (value > from) {
                    return parseInt(data[i].attributes["color"]);
                }
            }
            return null;
        ]]> </method>
        
        <handler name="ondata" reference="PowerImagingLegends">
            var objectId = PowerImagingLegends.getPointer().xpathQuery("objects/CONFIGURATION_DATA/@id");
            this.setAttribute("objId", objectId);
            
            if(this.parseAndPutDS(PowerImagingLegends) == true) {
                this.setAttribute('isLoaded', true);
            }
        </handler>
        
        <method name="handleDataset" args="o">
            this.currentTypeLegend = o.who.ownerDocument.name;
            lz.Idle.callOnIdle(new LzDelegate(this, 'lastedChecking'));
        </method>
        
        <method name="lastedChecking" args="ignore">
            var ds = this.legendsMap.get(this.currentTypeLegend);
            this.checkFrom(ds);
            this.onupdated.sendEvent(ds);
        </method>
        
        <method name="loadLegend" args="typeLegend">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            if(legendsMap.containsKey(typeLegend) &amp;&amp; this.isLoaded) {
                this.onloaded.sendEvent(legendsMap.get(typeLegend));
            } else if(!this.isLoaded) {
                var params =  new LzParam();
                var objectRequest = {type: "CONFIGURATION_DATA",
                                     children: [],
                                     properties: ["name", "config"],
                                     filter: [{name: "name", value: "PowerImagingLegend"}]
                                    };
                params.addValue("objectRequest", JSON.stringify(objectRequest));
                PowerImagingLegends.setQueryString(params);
                PowerImagingLegends.doReq(null);
            }
        </method>
        
        <method name="handleResult" args="res">
            this.handleResultDel.unregisterAll();
            this.onsaved.sendEvent(res);
            if(res) {
                this.setAttribute("defaultLegends", getLegendsAsStr());
            }
        </method>
        
        <method name="handleObjCreatedResult" args="res">
            this.handleObjCreatedDel.unregisterAll();
            this.setAttribute("objId", res);
        </method>
        
        <method name="saveLegends">
            var resStr = this.getLegendsAsStr();
            if (resStr.length) {
                this.handleResultDel.register(EsCommands, "onresult");
            }
            var msg = "ESComponentsConfiguration|ESComponentsConfigurationChange";
            if(this.objId == null) {
                this.handleObjCreatedDel.register(EsCommands, "onobjectcreated");
                var properties = {properties: [{name: "name", value: "PowerImagingLegend"},
                                               {name: "config", value: resStr}]};
                EsCommands.createObject("CONFIGURATION_DATA", "", properties, msg + "|activity_log_control_powerimaging_configuration_was_added");
            } else {
                EsCommands.setPropertyValue(this.objId, "config", resStr, msg + "|activity_log_control_powerimaging_configuration_was_changed");
            }
        </method>
        
        <method name="clear">
            this.handleDatasetDel.unregisterAll();
            
            var keyArr = this.legendsMap.keySet();
            for(var i=0; i &lt; keyArr.length; i++) {
                var ds = this.legendsMap.get(keyArr[i]);
                this[ds.name] = null;
                ds.destroy();
            }
            
            this.legendsMap.clear();
        </method>
        
        <method name="getLegendsAsStr">
        <![CDATA[
            var keyArr = this.legendsMap.keySet();
            var resStr = "";
            var oneDSIsUpdate = false;
            for(var i=0; i < keyArr.length; i++) {
                var ds = this.legendsMap.get(keyArr[i]);
                var nodes = ds.childNodes;
                for(var j=0; j<nodes.length; j++) {
                    resStr += nodes[j].attributes['to'] + ","
                           +  nodes[j].attributes['color'];
                    if(j < nodes.length-1) {
                        resStr += ";"
                    }
                }
                if(i < keyArr.length-1) {
                    resStr += "|";
                }
            }
            return resStr;
        ]]>
        </method>
        
        <method name="parseAndPutDS" args="ds">
        <![CDATA[
            var capacityStr = "";
            if(ds != null) {
                var dp1 = ds.getPointer();
                capacityStr = dp1.xpathQuery("PowerImagingLegends:/objects/CONFIGURATION_DATA/config/@v");
                dp1.destroy();
            }
            
            var capacityStrArr = "";
            if(capacityStr == null || capacityStr == "") {
                capacityStr = this.defaultLegends;
            } else {
                this.setAttribute("defaultLegends", capacityStr);
            }
            var capacityArr = capacityStr.split("|");
            for(var j=0; j<capacityArr.length; j++) {
                var nameDS = ( j == 0 ) ? "usedCapacityLegend" : "availableCapacityLegend";
                var newDS = new lz.synapdataset(this, {name: nameDS});
                newDS.setAttribute("data", []);
                var dp = newDS.getPointer();
                capacityStrArr = capacityArr[j].split(";");
                for (var i = 0; i < capacityStrArr.length; i++) {
                    var rangeArr = capacityStrArr[i].split(",");
                    if(rangeArr || rangeArr.length == 2) {
                        var attrs = {from: "0", to: rangeArr[0], color: rangeArr[1]};
                        if (i == capacityStrArr.length - 1) {
                            attrs["editableTo"] = "false";
                        }
                        dp.addNode("range",null,attrs);
                        dp.selectParent();
                    }
                }
                this.checkFrom(newDS);
                //this.handleDatasetDel.register(newDS, "ondata");
                this.handleDatasetDel.register(newDS, "onDocumentChange");
                this.legendsMap.put(nameDS, newDS);
                this.onloaded.sendEvent(newDS);
                dp.destroy();
            }
            
            return true;
        ]]>
        </method>
        
        <method name="restoreLegends" args="ignore=null"><![CDATA[
            this.clear();
            this.parseAndPutDS(null);
        ]]>
        </method>
        
        <method name="checkFrom" args="ds">
        <![CDATA[
            var nodesArr = ds.childNodes;
            for(var i = 0; i<nodesArr.length; i++) {
                if(i < nodesArr.length-1 && nodesArr[i].attributes['to'] != nodesArr[i+1].attributes['from']) {
                    nodesArr[i+1].setAttr('from', nodesArr[i].attributes['to']);
                }
            }
        ]]>
        </method>
        
    </node>
    
    <class name="powerimaginglegendunit" extends="view">
        
        <simplelayout axis="x" spacing="3"/> 

        <capacityicon name="icon" width="15" height="15" valign="middle" />

        <synaptext name="text" valign="middle" resize="true"/>
        
        <handler name="ondata" args="d">
            if (!d) {
                return;
            }
            this._update();
        </handler>
        
        <handler name="onupdated" reference="powerImagingLegendManager" args="ds">
            if(this['datapath'] &amp;&amp; parent.classroot.typeLegend == ds.name) {
                this._update();
            }
        </handler>
        
        <handler name="langchanged" reference="LangManager">
            if (this["prompt"]) {
                this.text.setAttribute("text", LangManager.getLocaleString(this.prompt));
            }
        </handler>
        
        <method name="_update">
            var color = this.datapath.p.attributes['color'];
            this.icon.setAttribute("scaleColor", color);
            this.icon.draw();
            var txt = "";
            var to = this.datapath.p.attributes['to'];
            if(!isNaN(parseInt(to))) {
                txt = this.datapath.p.attributes['from'] + "-" + to + "%";
            } else {
                txt = "text/above_max";
            }
            
            this.text.setAttribute("prompt", txt);
            //txtObject.setAttribute("align", "center");
        </method>
    </class>
    
    <class name="powerimaginglegend" extends="view" datapath="" width="600">
        <attribute name="typeLegend" type="string" value="usedCapacityLegend"/>
        
        <handler name="oninit">
            powerImagingLegendManager.loadLegend(this.typeLegend);
        </handler>
        
        <handler name="ontypeLegend">
            powerImagingLegendManager.loadLegend(this.typeLegend);
        </handler>
        
        <handler name="onloaded" reference="powerImagingLegendManager" args="ds">
            if(ds &amp;&amp; ds['name'] == this.typeLegend) {
                this.setAttribute(ds.name, ds);
                this.setAttribute('datapath', "local:" + ds.name + ":/");
            }
        </handler>
                
        <view clip="true" width="${classroot.width}">
            <wrappinglayout axis="x" spacing="3"/>
            <view>
                <simplelayout axis="x" spacing="3"/> 
                <capacityicon oninit="draw()" width="15" height="15" valign="middle" value="0">
                    <text text="?" fontsize="12" fontstyle="bold" align="center" valign="middle"/>
                </capacityicon>
                <synaptext valign="middle" prompt="text/unknown"/>
            </view>

            <powerimaginglegendunit datapath="range"/>
        </view>
    </class>
    
</library>
