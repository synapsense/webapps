<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     Oct 8, 2012 7:23:45 PM                                                        
     pahunov                                                                
     ====================================================================== -->
<library>
    <include href="../../utils/permissions/userprivileges.lzx"/>

    <node id="PrivilegesHelper">
        <attribute name="enablingParamsArray"
                   value="$once{[{type: 'enabling', attr: 'enabled', event: 'onenabled', clientAttr: 'clientsEnabled'},
                                 {type: 'visibility', attr: 'visible', event: 'onvisible', clientAttr: 'clientsVisible'}]}"/>
    </node>

    <!--This class can be mixed in component to make it controllable by user privileges.
        It also handles interaction with setting "enabled" and "visible" attributes by client code-->

    <mixin name="securedcomponentmixin">
        <!-- Shows what attribute is controlled by privileges. Can be either enabling" or "visibling"-->
        <attribute name="enablingType" value="enabling" type="string"/>
        <!-- Ids of privileges separated by " or " or " and " -->
        <attribute name="privileges" type="string"/>
        
        <!---intercept and apply our own logic-->
        <!---intercept and apply our own logic-->
        <attribute name="clientsEnabled" type="boolean"/>
        <attribute name="clientsVisible" type="boolean"/>
        <attribute name="isAllowed" value="true" type="boolean"/>
        <attribute name="allowSecureAction" value="false" type="boolean"/>
                                         
        <method name="enablingSetter" args="isEnabled">
            this.setAttribute("clientsEnabled", isEnabled);
            this.setObjectEnable(isEnabled);
        </method>
        
        <setter name="enabled" args="isEnabled">
            this.enablingSetter(isEnabled);
        </setter>
        
        <method name="visiblingSetter" args="visible">
            this.setAttribute("clientsVisible", visible);            
            this.setObjectVisible(visible);
        </method>
        
        <setter name="visible" args="isEnabled">
            this.visiblingSetter(isEnabled);
        </setter>

        <method name="callSuperSetVisible" args="visible">
            super.setAttribute("visible", visible);
        </method>
               
        <handler name="oninit">
            this.applyPrivilege();
        </handler>
        
        <handler name="userLoaded" reference="LoginInfo">
            this.applyPrivilege();
        </handler>
        
        <handler name="onPrivilegeLoad" reference="UserPrivileges">
            this.applyPrivilege();
        </handler>
        
        <method name="applyPrivilege">
            var isAllowed = UserPrivileges.isAllowed(this.privileges);
            this.setAttribute("isAllowed", isAllowed);
            
            var enParams = this.getEnablingParams(this.getEnablingType());
            var attrValue = isAllowed ? this[enParams.clientAttr] : false;
            this.enableObject(attrValue, enParams.attr, enParams.event, null);
        </method>
        
        <method name="enableObject" args="isEnabled, attrName, eventName, enablingType=null">
            var enRes = false;            
            if (!this.isAllowed &amp;&amp; this.getEnablingType() == enablingType){
               if(isEnabled == true){
                  enRes = false;
               }
            }else {
                enRes = isEnabled;
            }
 
            if(this[attrName] != enRes){                
                if(attrName == "visible"){
                    this.callSuperSetVisible(enRes);
                } else {
                    this[attrName] = enRes;
                    if (this[eventName]) {
                        this[eventName].sendEvent(enRes);
                    }
                }
            }
        </method>
        
        <method name="setObjectEnable" args="isEnabled">
            var enType = "enabling";
            var enParams = getEnablingParams(enType);
            this.enableObject(isEnabled, enParams.attr, enParams.event, enType);
        </method>
        
        <method name="setObjectVisible" args="visible">
            var enType = "visibility";
            var enParams = getEnablingParams(enType);
            this.enableObject(visible, enParams.attr, enParams.event, enType);
        </method>
        
        <method name="getEnablingParams" args="enablingType">
            for(var i = 0; i &lt; PrivilegesHelper.enablingParamsArray.length; i++){
                if(PrivilegesHelper.enablingParamsArray[i].type == enablingType){
                    return PrivilegesHelper.enablingParamsArray[i];
                }
            }
        </method>
        
        <method name="getEnablingType">
            return this.enablingType == null ? "enabling" : this.enablingType;
        </method>        
    </mixin>
</library>
