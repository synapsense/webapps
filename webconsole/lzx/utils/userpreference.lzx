<library>
    <include href="../data/securedataset.lzx"/>
    <include href="appsettings.lzx"/>
    <script src="json.js"></script>
    
    <!-- UserPreference object allow to set (name, value) and get(name) 
    setting specific for the currently logged user. Default settings 
    should be defined in the DefaultPreferences global dataset. 
    User preferences are returned from the server together with 
    login data. saveUserPreference servlet is used to store user settings -->
    <node id="UserPreference">
        <securedataset name="SaveUserPreference" type="http" src="saveUserPreference"/>
        <basesynapdataset name="ResetUserPreference" type="http" src="saveUserPreference"/>
        <event name="onResetToDefaults"/>
        <event name="onupdated"/>
        <event name="onPreferenceLoaded"/>
        <attribute name="propList" value="$once{new Array()}"/>
        <attribute name="requestUpdateDel" value="$once{new lz.Delegate(this, 'requestUpdate')}"/>
        <attribute name="buffer" value="$once{new Array()}"/>
        <attribute name="loaded" type="boolean" value="false"/>
        
        <method name="requestUpdate" args="ignore">
            //send request to server
            var params = new LzParam();
            params.addValue("params", JSON.stringify(this.buffer));
            SaveUserPreference.setQueryString(params);
            SaveUserPreference.doRequest();
            this.buffer.splice(0, this.buffer.length);
        </method>
        
        <method name="setProp" args="name, value">
        <![CDATA[
            var flag = false;
            for (var i = 0; i < this.propList.length; i++) {
                if (this.propList[i] == name) {
                    flag = true;
                   break;
                }
            }
            if (!flag) {
                this.propList.push(name);
            }
            this.setAttribute(name, value);
        ]]>
        </method>
        
        <method name="init">
            this.loadDefault();
            this.setAttribute('loaded',false);
            super.init();
        </method>
    
        <method name="resetToDefault">
            for (var i = 0; i &lt; this.propList.length; i++) {
                this.setAttribute(this.propList[i], null);
            }
            this.propList = [];
            this.loadDefault();                    

            var params = new LzParam();
            params.addValue("reset", "true");
            ResetUserPreference.setQueryString(params);
            ResetUserPreference.doRequest();
            
            LangManager.switchLocale(AppSettings.get("defaultLanguage"));
        </method>
        
        <handler name="ondata" reference="ResetUserPreference">
            var prefs = ResetUserPreference.getPointer().xpathQuery("preferences/*");
            if (prefs != null) {
                if (!(prefs instanceof Array)) {
                    prefs = [prefs];
                }
                
                UserPreference.load(prefs);
            } else {
                this.onResetToDefaults.sendEvent();
            }
        </handler>
        
        <method name="loadDefault">
            if (canvas["DefaultPreferences"]) {
                var dp = canvas["DefaultPreferences"].getPointer();
                if (dp.setXPath("preferences/preference[1]")) {
                    do {
                        var name = dp.xpathQuery("@name");
                        var type = dp.xpathQuery("@type");
                        var value = dp.xpathQuery("@value");
                        
                        if (type == "number") {
                            value = Number(value); 
                        } else if (type == "boolean") {
                            if(value == true || value == "true"){
                                value = true;
                            }else{
                                value = false;
                            } 
                        }
                        this.setProp(name, value);
                    } while (dp.selectNext());
                }
                dp.destroy();
            }
        </method>
        
        <method name="load" args="prefs">
        <![CDATA[
            var dp = new LzDatapointer(null);
            for (var i = 0; i < prefs.length; i++) {
                dp.p = prefs[i];
                var name = dp.xpathQuery("@name");
                //var type = dp1.xpathQuery("preferences/preference[@name='" + name + "']/@type");
                var value = dp.xpathQuery("text()");
                
                //if (type == "number") {
                //    value = Number(value); 
                //}
                this.setProp(name, JSON.parse(value));
            }
            dp.destroy();
            this.setAttribute('loaded',true);
            this.onPreferenceLoaded.sendEvent(null);
        ]]>   
        </method>
        
        <method name="get" args="name">
            return this[name];
        </method>
        
        <method name="set" args="name, value">
            //update value in dataset
            //var dp = LoginData.getPointer();
            //if (dp.setXPath("authentication/preferences/" + name)) {
            //   dp.setNodeText(value);
            //} else {
            //    dp.setXPath("LoginData:/authentication/prefs");
            //    dp.addNode(name, value);
           // }
            this.setProp(name, value);
            lz.Timer.resetTimer(this.requestUpdateDel, 100);
            this.buffer.push({name : name, value : value});
            this.onupdated.sendEvent({name : name, value : value});
        </method>
        
        <method name="reset">
        <![CDATA[
            for (var i = 0; i < this.propList.length; i++) {
                this.setAttribute(this.propList[i], null);
            }
            this.init();
        ]]>
        </method>
    </node>
</library>