<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     14.04.2011 15:16:48                                                        
     aklushin                                                                
     ====================================================================== -->
<library>
    <include href="../components/synapcontextmenu.lzx"/>
    
    <!--
        This class was written because of the following problem:
        If 'control' key was realeased, "onkeyup" events for all
        pressed keys will be sent after sending of "onkeyup" for "control"
        key. And more lz.Keys.isKeyDown return false even if key is pressed actually.
        For more details see LPP-7012 and LzKeys.lzs
    -->
    <class name="synapkeys" extends="node">
        <event name="onkeydown"/>
        <event name="onkeyup"/>
        
        <attribute name="downKeysHash" type="expression" value="{}"/>
        <attribute name="downKeysArray" type="expression" value="[]"/>
        
        <attribute name="__ignoredKeyUps" type="expression" value="{}"/>
        
        <handler name="onkeydown" reference="lz.Keys" args="kC"> <![CDATA[
            var dkhash:Object = this.downKeysHash;
            var dkeys:Array = this.downKeysArray;
    
            var firstkeydown = !dkhash[ kC ];
            if (firstkeydown) {
                dkhash[ kC ] = true;
                dkeys.push( kC );
            }
            if (this.onkeydown.ready) {
                this.onkeydown.sendEvent( kC );
            }
        ]]> </handler>
        <handler name="onkeyup" reference="lz.Keys" args="kC"> <![CDATA[
            if (__ignoredKeyUps[kC]) {
                delete __ignoredKeyUps[kC];
                return;
            }
            var dkhash:Object = this.downKeysHash;
            var isDown:Boolean = dkhash[ kC ];
            delete dkhash[ kC ];
            var dkeys:Array = this.downKeysArray;
            // Avoid allocating a new array for each keystroke
            dkeys.length = 0;
            __ignoredKeyUps = {};
            
            for (var k:String in dkhash) {
                if (kC == lz.Keys.keyCodes.control) {
                    lz.Keys.downKeysHash[k] = dkhash[k];
                    lz.Keys.downKeysArray.push( k );
                    __ignoredKeyUps[k] = dkhash[k];
                }
                dkeys.push( k );
            }
    
            // Only send a key-up event if we know the key is already down
            if (isDown && this.onkeyup.ready) this.onkeyup.sendEvent( kC );
        ]]> </handler>
        
        <!--After opening of any context menus we cann't
            receive key's events('down' or 'up').
            So we have to assume management of keys and
            make all keys are up.-->
        <handler name="onmenuopen" reference="GlobalContextMenu" args="a"> <![CDATA[
            allKeysUp();
        ]]> </handler>
        <handler name="onmenuopen" reference="DefaultContextMenu" args="a"> <![CDATA[
            allKeysUp();
        ]]> </handler>
        <method name="allKeysUp"> <![CDATA[
            for (var k in downKeysHash) {
                lz.Keys.gotKeyUp(k);
            }
        ]]> </method>
    </class>
    
    <synapkeys id="SynapKeys"/>
</library>
