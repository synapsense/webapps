<library>
    
    <!-- In Laszlo if child view handles mouse events, these events are 
         not send to parent view. 
    -->
    
    <!-- Utility class that can be added to the view. 
         It rropagates parent's views mouse events to parent's parents
    -->
    <class name="eventpropagator" extends="node">
        <attribute name="eventNames" value="$once{new Array('onmouseover')}"/>
        <attribute name="edgeView" value="$once{null}"/>
        
        <handler name="onmouseover" args="e" reference="parent">
            propagateEvent("onmouseover", e);
        </handler>

        <handler name="onmouseout" args="e" reference="parent">
            propagateEvent("onmouseout", e);
        </handler>

        <handler name="onmousedown" args="e" reference="parent">
            propagateEvent("onmousedown", e);
        </handler>

        <handler name="onmouseup" args="e" reference="parent">
            propagateEvent("onmouseup", e);
        </handler>
        
        <handler name="onclick" args="e" reference="parent">
            propagateEvent("onclick", e);
        </handler>
        
        <handler name="ondblclick" args="e" reference="parent">
            propagateEvent("ondblclick", e);
        </handler>
        
        <method name="propagateEvent" args="eventName, eventArg">
        <![CDATA[
            if(!this.isPropagatable(eventName)){
                return;
            }
            
            var v = parent;
            do {
                v = v.immediateparent;
                if (v instanceof lz.view) {
                    v[eventName].sendEvent(eventArg);
                }
            } while (v != canvas && v != this.edgeView);
        ]]>
        </method>
        
        <method name="isPropagatable" args="eventName">
            var result = false;
            for(var i = 0; i &lt; this.eventNames.length; i++){
                if(this.eventNames[i] == eventName){
                    result = true;
                    break;
                }
            }
            return result;
        </method>    	
    </class>
        
    <!-- Utility class that can be added to the view. 
         It propagate mouse events from all parent's child views to parent 
    -->
    <class name="childreneventpropagator" extends="node">
        <attribute name="events" value="$once{['onmouseover','onmouseout','onmousedown','onmouseup','onclick','ondblclick']}" type="expression"/>
        <attribute name="delegates" value="$once{new Object()}" type="expression"/>
                
        <handler name="oninit">
            this.register(this.parent);
        </handler>
        
        <method name="register" args="v"> <![CDATA[
            for(var i = 0; i < v.subviews.length; i++) {
                var sv = v.subviews[i];
                if (sv.clickable) {
                    for (var j = 0; j < this.events.length; j++) {
                        this.getDelegate(this.events[j]).register(sv, this.events[j]);
                    }
                 }
                 register(sv);
             }
        ]]>
        </method>
        
        <method name="getDelegate" args="event"> <![CDATA[
            if (!delegates[event]) {
                this[event + "Del"] = 
                    function (e) {
                        if (parent[event]) {
                            parent[event].sendEvent(e); 
                        }
                    }
                delegates[event] = new lz.Delegate(this, event + "Del")
            }
            
            return delegates[event];
        ]]>
        </method>
    </class>
    
</library>