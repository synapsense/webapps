<library>

 <!-- ************************************* -->
 <!-- Listitem classes for chart line types -->
 <!-- ************************************* -->
 
 <!-- ** solidlineitem ** -->
    <class name="solidlineitem" extends="listitem" value="'solid'" width="100%" height="20">
      <attribute name="text" value="" type="string"/>       
      <drawview width="10" height="10" x="5" y="5">
          <handler name="oninit">
             this.moveTo(0, 5);
             this.lineTo(15, 5);
             
             this.strokeStyle = 0x000000;
             this.lineWidth = 2;
             this.stroke();             
          </handler>
      </drawview>
      <text x="25" text="${classroot.text}" fgcolor="0x4B4B4B">
      </text>   
    </class>
    
  <!-- ** dashedlineitem ** -->  
    <class name="dashedlineitem" extends="listitem" value="'0,5,5'" width="100%" height="20" >
      <attribute name="text" value="" type="string"/>         
      <drawview width="10" height="10" x="5" y="5">
          <handler name="oninit">
             this.moveTo(0, 5);
             this.lineTo(6, 5);
             this.moveTo(9, 5);
             this.lineTo(15, 5);
             
             this.strokeStyle = 0x000000;
             this.lineWidth = 1;
             this.stroke();             
          </handler>
      </drawview>
      <text x="25" text="${classroot.text}" fgcolor="0x4B4B4B">
      </text>   
    </class>
    
    <!-- ************************************* -->
    <!-- ********* Line Descriptor *********** -->
    <!-- ************************************* -->
    <class name="qLineDescr" extends="node">
        <attribute name="req_id" value="" type="string"/>
        <attribute name="weight" value="1" type="number"/>        
        <attribute name="colorInt" value="0" type="number"/>        
        <attribute name="type" value="solid" type="string"/>
        
        <method name="packDescriptorToString">
           return JSON.stringify([this.weight, this.colorInt]);
        </method>
        <method name="unpackFromArray" args="arr"><![CDATA[
            if(arr && arr instanceof Array){ //just safety validation
                this.weight = parseInt(arr[0]);
                this.colorInt = parseInt(arr[1]);
            }
        ]]></method>
    </class>
    
    <!-- ************************************* -->
    <!-- ******** Chart Descriptor *********** -->
    <!-- ************************************* -->
    <class name="qChartDescr" extends="node">
        <attribute name="title" value="$once{LangManager.getLocaleString('text_data_analysis_chart')}" type="string"/>
        <attribute name="colorInt" value="16777215" type="string"/>
        <attribute name="showLegend" value="true" type="string"/>
        <attribute name="legendPosition" value="bottom" type="string"/>
        <attribute name="avgMetricLineColor" value="-1" type="number"/>
        <attribute name="minMetricLineColor" value="-1" type="number"/>
        <attribute name="maxMetricLineColor" value="-1" type="number"/>
        <attribute name="useThresholds" value="false" type="boolean"/>
        <attribute name="thresholds" value="" type="string"/>

        <method name="reset">
            this.title = LangManager.getLocaleString("text_data_analysis_chart");
            this.colorInt = 16777215;
            this.showLegend = "true";
            this.legendPosition = "bottom";
            this.avgMetricLineColor = -1;
            this.minMetricLineColor = -1;
            this.maxMetricLineColor = -1;
            this.useThresholds = false;
            this.thresholds = "";
        </method>

        <method name="packToString">
           var str = "";
           var bool = "false";
           if (this.useThresholds == true) bool = "true";

           str += this.colorInt + "," + this.showLegend + "," + this.title + "," + this.legendPosition + "," +
                  this.avgMetricLineColor + "," + this.minMetricLineColor + "," + this.maxMetricLineColor + "," +
                  bool + "," + this.thresholds;
           return str;
        </method>
        
        <method name="unpackFromString" args="str">
            var arr = str.split(",");
            this.colorInt = arr[0]; this.showLegend = arr[1]; this.title = arr[2]; this.legendPosition = arr[3];
            if (arr.length > 4){
                this.avgMetricLineColor = arr[4]; this.minMetricLineColor = arr[5]; this.maxMetricLineColor = arr[6];
                if (arr[7] == "true") this.useThresholds = true;
                if (arr[7] == "false") this.useThresholds = false;
                this.thresholds = arr[8];
            }
        </method>
    </class>
        
    <!-- ************************************* -->
    <!-- ** Chart Line Manager Singleton ****** -->
    <!-- ************************************* -->
    <class name="chartLineManager">
        <attribute name="chartDescriptor" value="$once{new Object()}"/>
        <attribute name="lineDescrArray" value="$once{new Object()}"/>
        
        <handler name="oninit">
            this.lineDescrArray = new Array();
            this.chartDescriptor = new lz.qChartDescr(this, {title: LangManager.getLocaleString("text_data_analysis_chart"), colorInt:16777215, showLegend:"true", legendPosition:"bottom" });
        </handler>
        
        <handler name="langDataLoaded" reference="LangManager">
            this.chartDescriptor.title = LangManager.getLocaleString("text_data_analysis_chart");
        </handler>                
        
        <!-- returns line descriptor if it exists, else return default descriptor  -->
        <method name="getLineDescr" args="id">
            var resRow = this.searchLineDescr(id);
            if(resRow == null){
               var newColor = this.calculateColorForNewDescr();
               resRow = new lz.qLineDescr(this, {req_id: id, weight: 1, type : "solid", colorInt:newColor});
               this.addLineDescr(resRow);
            }
            
            return resRow;          
        </method>
        
        <method name="updateLineDescr" args="id,lineDesc">
            var resRow = this.searchLineDescr(id);
            if(resRow == null){
               resRow = new lz.qLineDescr(this, {req_id: id, weight:lineDesc.weight, type:lineDesc.type, colorInt:lineDesc.colorInt});
               this.addLineDescr(resRow);
            }else{
               resRow.setAttribute("weight", lineDesc.weight);
               resRow.setAttribute("type", lineDesc.type);
               resRow.setAttribute("colorInt", lineDesc.colorInt);              
            }
                       
        </method>
        
        <!-- delete all descriptors which are not presented in the necessary_list -->
        <method name="deleteUnnecessary" args="necessary_list">
            var num = necessary_list.getNumItems();
            var newDescrArray = new Array();
            
            for(var i = 0; i &lt; this.lineDescrArray.length; i++){
                
                var isFound = false;
                for(var j = 0; j &lt; necessary_list.getNumItems(); j++){
                    if(necessary_list.getItemAt(j).getValue() == this.lineDescrArray[i].req_id){
                        newDescrArray.push(this.lineDescrArray[i]);
                        isFound = true;
                        break;
                    }
                }
                
                //if descriptor is found in the necessary list it will be reused.
                //else it will be destroed.
                if(!isFound){
                    this.lineDescrArray[i].destroy(); //free memory
               } 
            }
            
            this.lineDescrArray = newDescrArray;          
        </method>
        
        <!-- Init each line descriptor in list if it is not exists. -->
        <method name="initLineDescrsFromList" args="list">
             var num = list.getNumItems();
             for(var i = 0; i &lt; num; i++){                 
                 this.getLineDescr(list.getItemAt(i).getValue());                 
             }
        </method>
        
        <!-- ** private functions ** -->
        <method name="searchLineDescr" args="req_id">
           var resultRow = null;
           for(var i = 0; i &lt; this.lineDescrArray.length; i++){
              var currDescr = this.lineDescrArray[i];
              if(currDescr.req_id == req_id){
                 resultRow = currDescr;
                 break;      
              }              
           }                                 
           return resultRow;           
        </method>
        
        <method name="addLineDescr" args="lineDesc">
           this.lineDescrArray.push(lineDesc);
        </method>
        
        <method name="calculateColorForNewDescr">
           var colorInt = 0;
           var descrCount = this.lineDescrArray.length + 1;
           var colorCount = predefinedChartColors.getPointer().xpathQuery("predefinedChartColors:/color/last()");
           if(descrCount &lt; colorCount){
              var c = predefinedChartColors.getPointer().xpathQuery("predefinedChartColors:/color[" + descrCount + "]/@value");
              colorInt = Number(c);
           }else{              
              colorInt = Math.floor(Math.random() * 0xffffff);
           }
           
           return colorInt;
        </method>
    </class>
    
    <chartLineManager id="lineManagerSingleton"></chartLineManager>
    
    <dataset name="predefinedChartColors">
       <color value="0x000000"/>
       <color value="0x66dddd"/>
       <color value="0xff0000"/>
       <color value="0x00ff00"/>
       <color value="0x0000ff"/>            
       <color value="0x993300"/>
       <color value="0x333399"/>
       <color value="0xFF00FF"/>
       <color value="0x800080"/>     
       <color value="0xFFFF00"/>
    </dataset>       

</library>