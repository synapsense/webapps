<library>
    <include href="../../data/synapdataset.lzx"/>
    <include href="../../data/resultdataset.lzx"/>
    <include href="../../skins/components/skinnedicon.lzx"/>

    <include href="../../components/synaptext.lzx"/>
    <include href="../../components/synaptooltip.lzx"/>
    <include href="../../components/navigationbuttons.lzx"/>
    <include href="../../skins/components/skinnedbasebutton.lzx"/>
    <include href="../alerts/alertlink.lzx"/>
    <include href="rackrearview.lzx"/>
    <include href="rpdusummary/rpdusummaryview.lzx"/>
    <include href="powersummary/powersummaryview.lzx"/>
    <include href="capacitysummary/capacitysummaryview.lzx"/>
    <include href="environmentalssummary/environmentalssummaryview.lzx"/>
    <include href="../../utils/es/to.lzx"/>
    <include href="utils/layouts/wrappinglayout.lzx"/>
    <include href="../../statistics/userstatistics.lzx"/>
    
    <synapdataset name="RackRealtimeData" type="http" src="http:getObject"/>

    <maximizablesynapdatawindow id="RackDetailsWindow" level="1" use_insets="true" initstage="defer" timerUpdate="true">
        <attribute name="objId" value="" type="string"/>
        <attribute name="objType" value="" type="string"/>
        <attribute name="objName" value="" type="string"/>
        <attribute name="totalTitle" type="string" value=""/>
        <attribute name="platform" type="string" value="SC"/>
        
        <onvisiblestatistic statName="Window_RackDetailsWindow"/>
        
        <method name="refresh" args="visibleRefresh=false">
            if (visibleRefresh) {
                RackRealtimeData.doReq();
            } else {
                RackRealtimeData.doRequest();
            }
        </method>
        
        <handler name="alertUpdated" reference="canvas">
            this.refresh();
        </handler>
        
        <method name="setDatapathByType" args="obj,type">
            var path = getXPathForRack(type);
            obj.setAttribute("datapath",path);
            obj.datapath.setAttribute("pooling", true);
        </method>
        
        <method name="getXPathForRack" args="type">
            var path = "RackRealtimeData:/objects/" + type;
            if(type == "POWER_RACK") {
                path += "/links/envRack";
            }
            return path;
        </method>
        
        <method name="setDatapathByType2" args="obj,type">
            var path = getXPathForPowerRack(type);
            obj.setAttribute("datapath", path);
            obj.datapath.setAttribute("pooling", true);
        </method>
        
        <method name="getXPathForPowerRack" args="type">
            var path = "RackRealtimeData:/objects/" + type;
            if(type == "RACK") {
                path += "/links/powerRack";
            }
            return path;
        </method>        

        <method name="create" args="id, ids=null, options=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            var type = TO.fromString(id).getType();
            this.setAttribute("objId", id);
            this.setAttribute("objType", type);
            
            this.navigationButtons.setAttribute("currId", id);
            this.navigationButtons.setAttribute("ids", ids);
            
            this.setDatapathByType(rackEnvironmentals,type);
            this.setDatapathByType2(rackCapacity,type);
            this.setDatapathByType2(rackPower,type);
            this.setDatapathByType2(rackRearView,type);
            this.setDatapathByType2(rpduSummariesView,type);
            
            
            this.mainView.summariesView.summaryText.setDatapath("RackRealtimeData:/objects/*");
            
            var params =  new LzParam();
            params.addValue("objectRequest", JSON.stringify(generateRequestByType(id)));
            RackRealtimeData.setQueryString(params);
            RackRealtimeData.doReq(this);                        
        </method>
        
        <method name="initNominalVoltageUsedWarning"><![CDATA[
            var visible=false;
            if (RackDetailsWindow.platform == 'DUO'){
                var voltageXpath;
                var demandPowerXPath;
                if (RackDetailsWindow.objType =='POWER_RACK'){
                    voltageXpath="objects/POWER_RACK/children/RPDU/children/PHASE/voltage/@v";
                    demandPowerXPath="objects/POWER_RACK/children/RPDU/children/PHASE/demandPower/@v";
                }else{
                    voltageXpath="objects/RACK/links/powerRack/children/RPDU/children/PHASE/voltage/@v";
                    demandPowerXPath="objects/RACK/links/powerRack/children/RPDU/children/PHASE/demandPower/@v";
                }
                var phases=RackRealtimeData.getPointer().xpathQuery(voltageXpath);
                if (phases != null){
                    for (var i=0; i<phases.length; i++){
                        if (phases[i] == null){
                            visible=true;
                            break;
                        }
                    }
                }
                if (!visible){
                    phases=RackRealtimeData.getPointer().xpathQuery(demandPowerXPath);
                    if (phases != null){
                        for (var i=0; i<phases.length; i++){
                            if (phases[i] == null || phases[i] < 0){
                                visible=true;
                                break;
                            }
                        }
                    }
                }
            }
            this.mainView.summariesView.NominalVoltageUsedWarning.setAttribute('visible',visible);
            
        ]]>
        </method>
        
        <method name="generateRequestByType" args="id">
            var type = TO.fromString(id).getType();
            
            var sensorLinks = TypeUtil.getSensorLinkRequestObjects(["lastValue"]);
            TypeUtil.appendPropertiesToLinkRequestObjects(sensorLinks, ["max", "min", "aMin", "rMin", "rMax", "aMax"], "WSNSENSOR");
            TypeUtil.appendPropertiesToLinkRequestObjects(sensorLinks, ["max", "min", "aMin", "rMin", "rMax", "aMax"], "IPMISENSOR");
            var rackRequest = {type: "RACK",
                    properties: ["name", "location", "numAlerts", "cTop", "cMid", "cBot", "hTop", "hMid", "hBot", "chimney", "cTROfChange", "rh", "coldDp"],
                    children: [], 
                    links: sensorLinks,
                    parents:[{type:"ROOM", properties:["id", "name", "roomType", "controlType"]}]
                };
            var powerrackRequest = {type: "POWER_RACK", 
                    properties: ["name","location","numAlerts","uSpaceUsed","height","platform","lastResetTime",
                                                                     "demandPower","apPower","maxApPower","maxHistApPower",
                                                                     "potentialMaxApPower","maxPowerThreshold","percentLoad","nominalVoltage"],
                    children: [{type:"SERVER", properties: ["name", "uLocation", "uHeight", "demandPower", "apPower", "fpDemandPower", "fpApPower"],
                                                children: [{type: "PLUG", properties: []}]},
                                               {type:"RPDU", filter: [{name: "status", value: 1}], properties: ["status", "name", "type", "demandPower", "apPower", "availableApPower", "maxApPower", "maxPowerThreshold"], 
                                                children: [{type: "PHASE", properties: ["name", "demandPower", "avgCurrent", "deltaAvgCurrent", "apPower", "voltage", "maxCurrent", "maxHistCurrent", "potentialMaxCurrent", "maxCurrentThreshold"]}]}],
                    links: [],
                    parents:[{type:"DC", properties:["powerFactor"],requestDirectParentsOnly: false},
                             {type:"ROOM", properties:["id", "name", "roomType", "controlType"]}]
                };
            
            var objectRequest;
            if (type == "POWER_RACK") {
               powerrackRequest.properties.push("envRack");
               powerrackRequest.links.push(rackRequest);
               objectRequest =  powerrackRequest;
            } else {
               rackRequest.properties.push("powerRack");
               rackRequest.links.push(powerrackRequest);
               objectRequest =  rackRequest;
            }
            objectRequest.id = id;
            return objectRequest;
        </method>
        
        <handler name="onwidth">
            this.setAttribute("title", this.totalTitle);
        </handler>
        
        <handler name="ontitle">
            this.title_area.title.setAttribute("text", this.truncatTitle(this.title));
        </handler>
        
        <method name="truncatTitle" args="txt">
            var tt = new lz.trunctext(null, {id : "TitleText", width : (navigationButtons.x != 0) ? (navigationButtons.x-20) : this.width, visible : false, text : txt, options : "ignorelayout"});
            var res_title = tt.text;
            tt.destroy();
            return res_title;
        </method>

        <handler name="ondata" reference="RackRealtimeData">
            var rackName = RackRealtimeData.getPointer().xpathQuery("objects/"+this["objType"]+"/name/@v");
            this.setAttribute("objName", rackName);
            var totalTitle = OptTree.getCurrentLocationString() + ": " + rackName;
            totalTitle = LangManager.getFormattedLocaleString("title_rack_summary", totalTitle);
            this.setAttribute("totalTitle", totalTitle);
            this.setAttribute("title", totalTitle);
            
            var platform = (this["objType"] == "POWER_RACK") ?
                            RackRealtimeData.getPointer().xpathQuery("objects/POWER_RACK/platform/@v") :
                            RackRealtimeData.getPointer().xpathQuery("objects/RACK/links/powerRack/platform/@v");
            this.setAttribute("platform", platform);
            this.initNominalVoltageUsedWarning();
        </handler>
        
        <!-- <handler name="ondata" reference="UpdateSensorNodeAlert">
            this.refresh();
        </handler> -->
        
        <method name="reset" args="ignore=null">
            RackRealtimeData.reset();
        </method>
        
        <method name="resetHistMax">
            var dp = RackRealtimeData.getPointer();
            if (this.objType == "RACK") {
                dp.setXPath("objects/RACK/links/powerRack");
            } else {
                dp.setXPath("objects/POWER_RACK");
            }
            
            if (!this["resetDS"]) {
                this["resetDS"] = new lz.resultdataset(this, {name: "resetDS", request: false, src: "resetHistoricalMaximums"});
            }
            var params =  new LzParam();
            params.addValue("ids", dp.xpathQuery("@id"));
            this["resetDS"].setQueryString(params);
            this["resetDS"].doRequestHandleResult(this, "update");
        </method>
        
        <method name="update" args="ignore">
            this.refresh();
        </method>
        
        <navigationbuttons name="navigationButtons" placement="title_area" x="${immediateparent.width - immediateparent.controls.width - this.width}">
            <!--override-->
            <method name="navigate" args="id">
                RackDetailsWindow.create(id, this.ids);
            </method>
        </navigationbuttons>
        
        <view name="mainView">
            <simplelayout axis="x" spacing="10"/>
            <rackrearview id="rackRearView" name="rear" width="170"  envObjName="${RackDetailsWindow.objName}"
                      visibility="visible" visible="${RackDetailsWindow.platform=='SC'}"/>
            <view name="summariesView" height="${Math.max(580, parent.rear.height-20,parent.immediateparent.height-20)}">
                <resizelayout axis="y"/>
                <view name="summaryText" width="${immediateparent.width}">
                    <handler name="ondata">
                        this.setSummaries(RackDetailsWindow.objId);
                    </handler>
                    <handler name="onwidth" args="w">
                       if (this["summaryBox"]) {
                            this["summaryBox"].setAttribute("width", w);
                       }
                    </handler>
        
                    <method name="setSummaries" args="objId">
                        var type = TO.fromString(objId).getType();
                        var dp  = ConfigData.getPointer();
                        dp.setXPath("config/feature[@name='ObjectDetailsSummary']/type[@name='" + type + "']/*[1]");
        
                        var i = 1;
                        if (this["summaryBox"])
                            this["summaryBox"].destroy();
                        
                        var p = new lz.view(this, {name: "summaryBox", width: this.width, layout:{'class': "wrappinglayout", axis: "x", spacing: 10}});
                        var d=[this, "width"]; 
                        p.applyConstraint("width", function(ignore=null) {p.setAttribute("width", this.width);}, d);
                        //p.setLayout({'class': "wrappinglayout", axis: "x", spacing: 10});
                        
                        do {
                            var name = dp.xpathQuery("@prompt");
                            var xpath = dp.xpathQuery("@xpath");
                            
                            if (!xpath) {
                                continue;
                            }
                            
                            var value = this.datapath.xpathQuery(xpath);
                            
                            //if value is null, set default value
                            if(value == null || value == ""){
                               value = LangManager.getLocaleString(dp.xpathQuery("@defaultvalueprompt"));
                            }
                            if(value == null){
                               value = LangManager.getLocaleString("text/n_a");
                            }
                            if(name != null || name != "") {
                                var o = new lz.view(p, {width: 230, layout: {'class': 'simplelayout', axis: 'x', spacing: 10}});
                                //o.setLayout({'class': 'simplelayout', axis: 'x'});
                                var nameText = new lz.synaptext(o, {prompt: name});
                                if (xpath == "numAlerts/@v") {
                                    name = (type != "RACK") ? "text/num_of_active_power_alerts" : "text/num_of_active_env_alerts";
                                    nameText.setAttribute("prompt", name);
                                    var id = this.datapath.xpathQuery("@id");
                                    var type = this.datapath.xpathQuery("@type");
                                    new lz.alertlink(o, {text: value, 
                                                      fontstyle:"bold", 
                                                      enabled: value != 0, 
                                                      objId: id, objType: type }) ;
                                                      
                                    var prop = (type == "RACK") ? "powerRack" : "envRack";
                                    var otherRack = this.datapath.xpathQuery("links/" + prop);
                                    if (otherRack) {
                                        var numAlert = this.datapath.xpathQuery("links/" + prop + "/numAlerts/@v");
                                        o = new lz.view(p, {width: 300, layout: {'class': 'simplelayout', axis: 'x', spacing: 10}});
                                        name = (type == "RACK") ? "text/num_of_active_power_alerts" : "text/num_of_active_env_alerts"; 
                                        nameText = new lz.synaptext(o, {prompt: name});
                                        id = otherRack.getAttr("id");
                                        type = otherRack.getAttr("type");
                                        new lz.alertlink(o, {text: numAlert, 
                                                          fontstyle:"bold", 
                                                          enabled: numAlert != 0, 
                                                          objId: id, objType: type }) ;
                                    }
                                } else {
                                    new lz.trunctext(o, {text: value + "", 
                                                   fontstyle:"bold", 
                                                   fgcolor:lz.colors.black, width : o.width - nameText.width});
                                    //o.setAttribute("align","right");
                                }
                            }
                            i++;
                        } while (dp.selectNext());
                        dp.destroy();
                    </method>
                </view>
                <view name="rackSummariesView" height="${RackDetailsWindow.platform=='SC'?390:355}">
                    <simplelayout axis="x" spacing="10"/>
                    <view height="${parent.height}">
                        <simplelayout axis="y" spacing="0"/>
                        <capacitysummaryview id="rackCapacity" visible="false" width="400" height="155">
                        </capacitysummaryview>
                        <powersummaryview id="rackPower" visible="false" width="400" height="${RackDetailsWindow.platform=='SC'?235:200}" platform="${RackDetailsWindow.platform}">
                        </powersummaryview>
                    </view>
                    <environmentalssummaryview id="rackEnvironmentals" visible="false">
                    </environmentalssummaryview>
                </view>
                
                <frameview id="rpduSummariesView" width="810" height="180" options="releasetolayout"  prompt="text/rpdu_summary" pixellock="true" topcontentinset="0">
                    <rpdusummaryview name="summary" visible="true" width="${immediateparent.width}" height="${immediateparent.height}" platform="${RackDetailsWindow.platform}">
                        <handler name="ondata" reference="rpduSummariesView">
                            //this.setGridsDataPath("RackRealtimeData:/POWER_RACK");
                            this.setGridsDataPath(rpduSummariesView.datapath.xpath);
                         </handler>
                    </rpdusummaryview>
                </frameview>
                
                <view name="NominalVoltageUsedWarning" visible="false">
                    <simplelayout axis="x" spacing="10"/>
                    <skinnedicon resourceName="ASTERISK"/>
                    <synaptext prompt="text/nominal/voltage/used" valign="middle"/>
                </view>
                
            </view>
            
            
        </view>
        
        <vscrollbar visible="${scrollable}"/>
        <hscrollbar visible="${scrollable}"/>
    </maximizablesynapdatawindow>
</library>
