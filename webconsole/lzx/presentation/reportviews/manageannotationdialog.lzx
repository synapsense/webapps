<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     23.07.2010 10:27:26                                                        
     vlebedev                                                                
     ====================================================================== -->
<library>
    <include href="../../components/secured/securedcheckbox.lzx"/>
    <include href="../../components/synapscrolledittext.lzx"/>
    <include href="../../components/datetimepickers/periodpicker.lzx" />
    <include href="../../messages/messagewindow.lzx"/>
    <include href="annotationmanager.lzx"/>
    
    <managedsynapmodaldialog id="ManageAnnotationDialog" 
                             prompt="${this.isNewAnnotation ? 'title/new_annotation' : 'title/edit_annotation'}"
                             align="center" valign="middle">
        <attribute name="isNewAnnotation" type="boolean" value="true"/>
        <attribute name="annotationId" type="string" value=""/>
        <attribute name="datetime" type="number"/>
        <attribute name="annotation" type="string" value=""/>
        <attribute name="isSystem" type="boolean" value="false"/>
        <attribute name="successCallback"/>
        
        <method name="create" args="options=false">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            
            if(options != null) {
                this.setAttribute("isNewAnnotation", options['isNewAnnotation']);
            }
            
            if(this.isNewAnnotation == false) {
                this.setAttribute("annotationId", options['id'])
                this.setAttribute("datetime", options['datetime']);
                this.setAttribute("annotation", options['annotation']);
                
                AnnotationEdit.setText(this.annotation);
            }
            AnnotationPeriodPicker.setDate((this.isNewAnnotation) ? new Date() : new Date(parseInt(this.datetime)));
            this.setAttribute("isSystem", options['isSystem']);
            this.setAttribute("successCallback", options['successCallback']);
            
            SystemWideCheck.setValue((this.isNewAnnotation &amp;&amp; this.isSystem) ? true : this.isSystem);
            SystemWideCheck.setAttribute('enabled', (this.isNewAnnotation &amp;&amp; this.isSystem) ? false : true);
            
            this.open();
        </method>
        <method name="doOkAndClose">
            if(annotationView.validateInput() == false) {
                return;
            }
            if(ManageAnnotationDialog.isNewAnnotation == true) {
                AnnotationManager.addAnnotation({datetime: AnnotationPeriodPicker.getDateTimeAsLong(), note: AnnotationEdit.getText(), status: SystemWideCheck.value, dc: OptTree.getSelectedDCId(), successCallback: this.successCallback});
            }else {
                var dc = (this.isSystem != SystemWideCheck.value) ? OptTree.getSelectedDCId() : "";
                AnnotationManager.updateAnnotation({id: ManageAnnotationDialog.annotationId, datetime: AnnotationPeriodPicker.getDateTimeAsLong(), note: AnnotationEdit.getText(), status: SystemWideCheck.value, dc: dc, successCallback: this.successCallback});
            }
            
            ManageAnnotationDialog.close();
        </method>
        
        <method name="reset" args="ignore=null">
            this.setAttribute("annotationId", "");
            this.setAttribute("datetime", "");
            this.setAttribute("annotation", "");
            this.setAttribute("isSystem", false);
            AnnotationEdit.setText("");
        </method>
        
        <simplelayout axis="y" spacing="10"/>
        <view name="dateTimeView">
            <datetimepicker id="AnnotationPeriodPicker" settings="${UserPreference}" showTime="true" prevText="text/time_" showTimeType="1" showNowTime="false"/>
        </view>
        <view id="annotationView">
            <simplelayout axis="x" spacing="10"/>
            <synaptext prompt="text/annotation"/>
            <!--scrollrichedittext-->
            <synapscrolledittext id="AnnotationEdit" width="250" height="80"/>    
            <method name="validateInput">
                var annotationText = AnnotationEdit.getText();
                var isValid = true;
                
                if (annotationText.trim().length == 0){
                    isValid = false;
                    InfoDialog.show("message/annotation_is_required", AnnotationEdit, "setFocus");
                }
                
                return isValid;
            </method>
        </view>
        <view name="PrivateCheck">
            <securedcheckbox id="SystemWideCheck" prompt="text/system_wide_annotation" privileges="ADD_SYSTEM_ANNOTATIONS"/>
        </view>
        <view name="ManageButtonView" align="right">
            <attribute name="result" type="string" value=""/>
            <simplelayout axis="x" spacing="10"/>
            <synapbutton name="OkButton" prompt="button/ok" isdefault="true">
                <handler name="onclick">
                    ManageAnnotationDialog.doOkAndClose()
                </handler>
            </synapbutton>
            <synapbutton prompt="button/cancel">
                <handler name="onclick">
                    ManageAnnotationDialog.close();
                </handler>
            </synapbutton>
         </view> 
    </managedsynapmodaldialog>
</library>
