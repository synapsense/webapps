<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     31.03.2010 8:51:53                                                        
     vlebedev                                                                
     ====================================================================== -->
<library>
    <include href="../../../components/synapcombobox.lzx"/>
    
    <include href="../../../components/livechart/lvchart.lzx"/>
    <include href="../../synaplvcharts/autorefresh/refreshchartviewer.lzx"/>
    <include href="../../../utils/es/propertyid.lzx"/>
    
    <class name="historicaldatapane" extends="view">
        
        <attribute name="serieColors" value="$once{new Array(0x000000, 0x237234, 0xff0000, 0x0000ff, 0x808080, 0x00ff00)}"/>
        <attribute name="propIdToDescriptorsMap" value="$once{new Map()}"/>
        <attribute name="descriptorsList" value="$once{new Array()}"/>
        <attribute name="propertyTO" value="$once{new PropertyId('','')}"/>
        
        <handler name="oninit">
            //create serie descriptors
            for(var i = 0; i &lt; this.serieColors.length; i++){
                descriptorsList.push(new lz.seriedescr(this, {lineDescr: new lz.linedescr(this, {color: this.serieColors[i], weight: 1})}));
            }
        </handler>
        
        <method name="init">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            super.init();
        </method>
        
        <method name="create">
            this.reset();
            this.getChartViewer().redrawChart();
        </method>
        
        <method name="reset">
            this.getChartViewer().removeAllSeries();
            this.propIdToDescriptorsMap.clear();
            this.showErrorMessage("message/no_inspectors_selected");
        </method>
        
        <method name="refresh">
            this.getChartViewer().refresh();
        </method>
        
        <method name="getChartViewer">
            return this.ChartView.chartViewer;
        </method>
        
        <method name="isAnySerieToDisplay">
            return (this.propIdToDescriptorsMap.size() > 0) ? true : false;
        </method>
        
        <method name="showChart" args="o">
            this.loadChart(o);
        </method>
        
        <method name="showErrorMessage" args="mess">
            if(this.ChartView.errorText == null) {
                new lz.synaptext(this.ChartView, {name: "errorText", prompt: mess, visible: true, valign: "middle", align: "center"});
            } else {
                this.ChartView.errorText.setAttribute("prompt", mess);
                this.ChartView.errorText.setAttribute("visible", true);
            }
            this.getChartViewer().setAttribute("visible", false);        
        </method>
        
        <method name="hideErrorMessage">
            if(this.ChartView.errorText){
                this.ChartView.errorText.setAttribute("visible", false);
            }
            this.getChartViewer().setAttribute("visible", true);
        </method>
        
        <method name="loadChart" args="event_params">        	
            var toAddCount = 0;
            var toRemoveCount = 0;
            var inspectorDataArray = event_params;
            
            for(var i = 0; i &lt; inspectorDataArray.length; i++){
                if(inspectorDataArray[i].value){
                    toAddCount++;
                    this.addInspector(inspectorDataArray[i].node);
                }else{
                    toRemoveCount++;
                    this.removeInspector(inspectorDataArray[i].node);
                }
            }
            
            if(toAddCount > 0){
                this.getChartViewer().requestDataForAddedSeries();//request data from server (chart will be redrawn after data retrieving)
            }else if(toRemoveCount > 0){
                this.getChartViewer().redrawChart();//just redraw
            }
            
            if(this.isAnySerieToDisplay()){
                this.hideErrorMessage();
            }else{
                this.showErrorMessage("message/no_inspectors_selected");
            }
        </method>
        
        <method name="updateChart" args="event_params">
            this.getChartViewer().setTimeEdges(event_params.nowTime - event_params.shownInterval, event_params.nowTime);
            this.getChartViewer().setShownInterval(event_params.shownInterval);
            
            if(this.isAnySerieToDisplay()){
                this.getChartViewer().requestDataForAllSeries();//request data from server
            }
        </method>
        
        <method name="addInspector" args="inspectorNode">
            var dp = new lz.datapointer();
            dp.setPointer(inspectorNode);
            var name = dp.xpathQuery("name/@v");
            var units = dp.xpathQuery("units/@v");      	
            dp.destroy();
            
            this.propertyTO.setObjectId(inspectorNode.attributes["id"]);
            this.propertyTO.setPropertyName("lastValue");
            
            var inspectorPropertyId = this.propertyTO.toString();
            
            var serieDescr = this.getAvailableDescriptor();
            serieDescr.propID = inspectorPropertyId;
            serieDescr.serieName = (name == null)? "" : name;
            serieDescr.axisName = (units == null) ? "" : units;
            this.propIdToDescriptorsMap.put(inspectorPropertyId, serieDescr);
            
            this.getChartViewer().addSerie(inspectorPropertyId, serieDescr);
            //this.getChartViewer().requestDataForAddedSeries();//request data from server
        </method>
        
        <method name="removeInspector" args="inspectorNode">
            this.propertyTO.setObjectId(inspectorNode.attributes["id"]);
            this.propertyTO.setPropertyName("lastValue");
            var inspectorPropertyId = this.propertyTO.toString();
            this.propIdToDescriptorsMap.remove(inspectorPropertyId);
            
            this.getChartViewer().removeSerie(inspectorPropertyId);
            //this.getChartViewer().redrawChart();
        </method>
        
        <method name="getAvailableDescriptor">
            var result = null;
            var descriptorsInUse = this.propIdToDescriptorsMap.valSet();
            
            for(var i = 0; i &lt; descriptorsList.length; i++){
                var isDescriptorInUse = false;
                for(var j = 0; j &lt; descriptorsInUse.length; j++){
                    if(descriptorsList[i] == descriptorsInUse[j]){
                        isDescriptorInUse = true;
                        break;
                    }
                }
                if(!isDescriptorInUse){//free descriptor was found
                    result = descriptorsList[i];
                    break;
                }
            }
            
            if(result == null){//all descriptors are in use
                result = new lz.seriedescr(this, {lineDescr: new lz.linedescr(this, {color: 0x000000, weight: 1})});
            }
            
            return result;
        </method>
        
        <simplelayout axis="y" spacing="10"/>
        <view name="ChartView" height="${immediateparent.height}" width="${immediateparent.width}">
            <refreshchartviewer name="chartViewer" height="${immediateparent.height}" width="${immediateparent.width}"
                                chartHeight="${height &gt; 0 ? height : 400}" chartWidth="${width &gt; 0 ? width : 400}">
                <handler name="onheight" args="h">
                    this.redrawChart();
                </handler>

            </refreshchartviewer>
        </view>
    </class>
</library>
