<library>
    <include href="templatepane.lzx"/>
    <include href="../../../../components/synapcombobox.lzx"/>
    <include href="../../../../components/synaptext.lzx"/>
    <include href="../../../../components/synapcheckbox.lzx"/>
    <include href="../../../../components/frameview.lzx"/>
    <include href="../../../../data/resultdataset.lzx"/>
    <include href="../utilclasses/contextmenu/menuactiontypes.lzx"/>
    <include href="../utilclasses/viewlink/viewlinktypes.lzx"/>
    <include href="../commoncontextmenu/commoncontextmenuutils.lzx"/>
    <include href="internaldetailsdialog.lzx"/>
    
    <class name="previewpane" extends="templatepane">
        <attribute name="mainObjType" type="string" value=""/>
        <attribute name="titlePrompt" type="string" value="${wizardRef.state != WizardStates.EDIT ? 'tabular_templates/title_new_previewpane' : 'tabular_templates/title_edit_previewpane'}"/>
        
        <resultdataset name="ManageTemplates" type="http" src="http:commonTabular"  request="false"/>
        <synapdataset name="GetTemplates" type="http" src="http:commonTabular"  request="false"/>
        
        <simplelayout axis="y" spacing="5"/>

        <frameview name="previewView" prompt="tabular_templates/text_preview" width="${immediateparent.width}">
            <view name="tableView"/>
        </frameview>
        
        <view name="bottomView" width="${immediateparent.width}">
            <simplelayout axis="x" spacing="15"/>        	
            <frameview name="linkedViewSelection" prompt="tabular_templates/text_links" width="${immediateparent.width}">
                <simplelayout axis="x" spacing="5"/>
                <synaptext prompt="tabular_templates/text_link_to" valign="middle"/>
                <synapcombobox name="LinkedViewCombo" shownitems="5" width="150">
                    <synaptextlistitem prompt="tabular_templates/text_none" value="'None'" selected="true"/>
                    <synaptextlistitem datapath="CommonViewLinkTypesDataSet:/entry">
                        <handler name="ondata">
                            this.setAttribute("prompt", this.datapath.xpathQuery("@prompt"));
                            this.setAttribute("value", this.datapath.xpathQuery("@alias"));                            
                        </handler>
                    </synaptextlistitem>
 
                    <handler name="onselect">
                        var isOtherComboVisible = this.value == CommonViewLinkTypes.OTHER_TEMPLATE_ALIAS ? true : false;
                        parent.OtherTemplateCombo.setAttribute("visible", isOtherComboVisible);
                        if(isOtherComboVisible){
                            classroot.requestTemplates();
                        }
                    </handler>
                </synapcombobox>
                
                <synapcombobox name="OtherTemplateCombo" shownitems="5" width="150" visible="false">
                    <synaptextlistitem datapath="local:parent.parent.parent.parent.GetTemplates:/templates/template">
                        <handler name="ondata">
                            var templateId = this.datapath.xpathQuery("@id");
                            this.setAttribute("prompt", this.datapath.xpathQuery("@name"));
                            this.setAttribute("value", templateId);
                            
                            var tabularTemplate = classroot.wizardRef.getConstructingObject();
                            if(tabularTemplate.templateLinkedView){
                                if(tabularTemplate.templateLinkedView.otherTemplateId == templateId){
                                    this.setAttribute("selected", true);
                                }
                            }
                        </handler>
                    </synaptextlistitem>
                </synapcombobox>
            </frameview>
        </view>
        
        <view visible="false">
            <simplelayout axis="y" inset="5"/>
            <synapbutton name="detailsButton" prompt="tabular_templates/button_internal_details">
                <handler name="onclick">
                    var tabularTemplate = classroot.wizardRef.getConstructingObject();
                    classroot.prepareTemplate(tabularTemplate);
                    InternalDetailsDialog.create({tabularTemplate: tabularTemplate});
                </handler>
            </synapbutton>
            
            <command key="['a', 'alt', 'control']" active="true">
                <handler name="onselect">
                    parent.setAttribute("visible", true);
                </handler>
            </command>
        </view>
        
        <method name="create" args="params=null">
            var tabularTemplate = wizardRef.getConstructingObject();
            this.resetTable(tabularTemplate);
            this.applyTemplateToUIControls(tabularTemplate);
        </method>
        
        <method name="done">
            var tabularTemplate = wizardRef.getConstructingObject();        	
            this.prepareTemplate(tabularTemplate);
            
            this.saveOrUpdateTemplate(tabularTemplate);
        </method>
        
        <method name="prepareTemplate" args="tabularTemplate">
            this.applyUIControlsToTemplate(tabularTemplate);
            this.applyAutoConfigurationToTemplate(tabularTemplate);
        </method>
        
        <method name="saveOrUpdateTemplate" args="tabularTemplate">
            var pObj = wizardRef.getParamsObject();
            var action = wizardRef.state == WizardStates.EDIT ? "updateTemplate" : "addTemplate";
            
            var jsonTempaleString = JSON.stringify(tabularTemplate.createJSONObject());
Debug.write("stringify: " + jsonTempaleString);

            var params =  new LzParam();
            params.addValue("template", escape(jsonTempaleString));            
            params.addValue("requestType", action);
            params.addValue("isPrivate", pObj.isPrivate ? 1 : 0);
            params.addValue("description", escape(pObj.description));
            if(wizardRef.state == WizardStates.EDIT){
               params.addValue("templateId", pObj.templateId);
            }
            
            this.ManageTemplates.setQueryString(params);
            this.ManageTemplates.doRequestHandleResult(this, "onSuccessDone", "onFailDone");
        </method>
        
        <method name="onSuccessDone" args="ignore=null">
            //can be overrided in concrete instance
        </method>
        
        <method name="onFailDone" args="result_arr">
            InfoDialog.show(LangManager.getLocaleString(result_arr[1]));
        </method>
        
        <method name="applyAutoConfigurationToTemplate" args="tabularTemplate">
            this.applyAutoMenuItemsToTemplate(tabularTemplate);
        </method>
        
        <method name="applyAutoMenuItemsToTemplate" args="tabularTemplate">
            var menuItems = CommonContextMenuUtils.getMenuItemsByObjectType(tabularTemplate.objType);
            tabularTemplate.templateMenuItems = menuItems;
        </method>
        
        <method name="applyUIControlsToTemplate" args="tabularTemplate">
            this.applyTablePreviewToTemplate(tabularTemplate);
            this.applyLinkViewToTemplate(tabularTemplate);
        </method>
        
        <method name="applyTemplateToUIControls" args="tabularTemplate">
            this.applyTemplateToLinkView(tabularTemplate);
        </method>
        
        <method name="resetTable" args="tabularTemplate">
            if (this.getTableView()["table"]) {
                this.getTableView()["table"].destroy();
            }
            var tView = this.getTableView();
            var gridIdentifier = "TemplatePreview";        
            var tbl = new lz.commonsynaptable(tView, {name: "table", tabularTemplate: tabularTemplate,
                                              settings: UserPreference,
                                              showcolumnselector: false,
                                              gridIdentifier: gridIdentifier,
                                              width: tView.immediateparent.width,
                                              height: 100});
           
           tbl.requestData(wizardRef.selectedObjectId);
        </method>
        
        <method name="applyTablePreviewToTemplate" args="tabularTemplate"><![CDATA[
            var cTemplates = tabularTemplate.columnTemplates;
            
            var sortedArray = [];
            var table = this.getTableView().table;
            var columns = table.header.hcontent.layouts[0].subviews;
            if(columns){
                for (var i = 0; i < columns.length; i++) {                    
                    var index = findTemplateIndexByTemplate(cTemplates, columns[i].columnTemplate);
                    if(index != -1){//if index is -1 that means child column template was already added
                        var columnTempl = cTemplates[index];
                        cTemplates.splice(index, 1);
                        columnTempl.columnWidth = columns[i].width;
                        sortedArray.push(columnTempl);
                    }
                }
            }
            tabularTemplate.columnTemplates = sortedArray;
        ]]></method>
        
        <method name="applyLinkViewToTemplate" args="tabularTemplate">
            tabularTemplate.templateLinkedView = null;
            var linkedAlias = this.getLinkedViewCombo().getValue();
            if(linkedAlias != "None"){
                tabularTemplate.templateLinkedView = new TemplateLinkedView(linkedAlias);
                if(linkedAlias == CommonViewLinkTypes.OTHER_TEMPLATE_ALIAS){
                    tabularTemplate.templateLinkedView.otherTemplateId = this.getOtherTemplateCombo().getValue();
                }
            }            
        </method>

        <method name="applyTemplateToLinkView" args="tabularTemplate">
            var linkedViewAlias = "None";
            if(tabularTemplate.templateLinkedView){
                linkedViewAlias = tabularTemplate.templateLinkedView.viewLinkAlias;
            }
            this.getLinkedViewCombo().selectItem(linkedViewAlias);
        </method>
        
        <method name="canBeDone"><!-- overriden -->
            var linkedAlias = this.getLinkedViewCombo().getValue();            
            if(linkedAlias == CommonViewLinkTypes.OTHER_TEMPLATE_ALIAS &amp;&amp;
               !this.getOtherTemplateCombo().getValue()){               	
                InfoDialog.show(LangManager.getLocaleString("tabular_templates/text_select_template_type_to_link"));
                return false;
            }
            return true;
        </method>
        
        <method name="requestTemplates">
            var params = new LzParam();
            params.addValue("requestType", "getTemplates");
            this.GetTemplates.setQueryString(params);
            this.GetTemplates.doReq(null);
        </method>
        
        <method name="findTemplateIndexByTemplate" args="cTemplatesArray, cTemplate"><![CDATA[
            var index = -1;
            for(var j = 0; j < cTemplatesArray.length; j++){
                var t = cTemplate;
                var c = cTemplatesArray[j];                      
                if(t.propertyName == c.propertyName && t.propertyObjectType == c.propertyObjectType && t.propertyRelationType == c.propertyRelationType && t.displayMode == c.displayMode){
                    index = j;
                    break;
                }
            }
            return index;
        ]]></method>
        
        <method name="reset"><!-- overriden -->
            this.getLinkedViewCombo().clearSelection();
            this.getOtherTemplateCombo().clearSelection();
        </method>
        
        <method name="getLinkedViewCombo">
            return this.bottomView.linkedViewSelection.LinkedViewCombo;
        </method>
        
        <method name="getOtherTemplateCombo">
            return this.bottomView.linkedViewSelection.OtherTemplateCombo;
        </method>
        
        <method name="getTableView">
            return this.previewView.tableView;
        </method>
    </class>

</library>