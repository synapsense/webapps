<library>
    <include href="displaymodecombobox.lzx"/>
    <include href="../../../../components/synapcheckbox.lzx"/>
    <include href="../../../../components/synapgrid/synapcheckboxcolumn.lzx"/>
    <include href="../../../../components/consolegrid/consolegrid.lzx"/>
    <include href="../../../../utils/typeutil.lzx"/>
    <include href="../utilclasses/commontabularutils.lzx"/>
    <include href="configuratoritem.lzx"/>
    
    <class name="columntemplateconfigurator" extends="view">    	
        <attribute name="itemsArray"/>
        
        <dataset name="itemsDataset"/>
        <dataset name="dummyItemsDataset"/>
        
        <consolegrid name="configGrid"
            datapath="local:parent.itemsDataset:/items"
            contentdatapath="item"
            height="${immediateparent.height}" 
            width="570" showcolumnselector="false"  multiselect="false" selectable="false" _rowclass="lz.synapgridrow" settings="${UserPreference}" restoreScrollPos="false">
            
            <synapcheckboxcolumn width="50"
                            gridDataset="$once{parent.parent.itemsDataset}"
                            checkboxFullXPath="items/item"
                            checkboxXmlAttr="selected"
                            prompt="" sortable="false">
            </synapcheckboxcolumn>
            
            <synapgridcolumn width="200" prompt="columnname/name" sortable="false">
                <text datapath="@propertyName" valign="middle"/>
            </synapgridcolumn>
            
            <synapgridcolumn width="130" prompt="tabular_templates/column_display_mode" sortable="false">
                <displaymodecombobox width="120" valign="middle" align="center">
                    <datapath xpath="." pooling="true"/>
                    
                    <attribute name="firstChange" type="boolean" value="true"/>
                    
                    <handler name="ondata">
                        var dispMode = this.datapath.xpathQuery("@displayMode");
                        if(dispMode != "" &amp;&amp; dispMode != null &amp;&amp; this.value != dispMode){
                            this.selectItem(dispMode);
                        }
                    </handler>
                    
                    <handler name="onvalue">
                        if(this.firstChange){
                            this.firstChange = false;
                            return;
                        }
                        var dispMode = this.datapath.xpathQuery("@displayMode");
                        if(dispMode != null &amp;&amp; dispMode != this.value){
                            this.datapath.setNodeAttribute("displayMode", this.value);
                        }
                    </handler>
                </displaymodecombobox>
            </synapgridcolumn>
            
            <synapgridcolumn width="170" prompt="tabular_templates/column_name" sortable="false">
                <synapcombobox editable="true" width="140" valign="middle" align="center">
                    <datapath xpath="." pooling="true"/>
                    
                    <attribute name="defaultPrompt" type="string" value="tabular_templates/text_default"/>
                    
                    <synaptextlistitem prompt="$once{parent.defaultPrompt}" />
                    
                    <handler name="ondata">
                        var columnTitle = this.datapath.xpathQuery("@title");
                        //Debug.write("ondata, columnTitle: " + columnTitle + ", this.interior.cbtext.text: " + this.interior.cbtext.text);
                        if(columnTitle != "" &amp;&amp; columnTitle != null &amp;&amp; this.interior.cbtext.text != columnTitle){
                            var strToShow = columnTitle;
                            if(LangManager.isPromptExists(columnTitle)){                            	
                                var item = classroot.classroot.getItemByItemId(this.datapath.xpathQuery("@itemid"));
                                var propertyPrompt = getPropertyPrompt(item.columnTemplate.propertyObjectType, item.columnTemplate.propertyName);
                                if(columnTitle == propertyPrompt){
                                    strToShow = LangManager.getLocaleString(this.defaultPrompt);
                                }
                            }
                            this.interior.cbtext.setAttribute("text", strToShow);
                            this.setAttribute("value", columnTitle);
                        }
                    </handler>
                    
                    <handler name="onblur">
                        if(LangManager.getLocaleString(this.defaultPrompt) != this.interior.cbtext.text){
                            this.datapath.setNodeAttribute("title", this.interior.cbtext.text);
                            this.setAttribute("value", this.interior.cbtext.text);
                        }else{
                            var item = classroot.classroot.getItemByItemId(this.datapath.xpathQuery("@itemid"));         	
                            var title = getPropertyPrompt(item.columnTemplate.propertyObjectType, item.columnTemplate.propertyName);
                            
                            this.datapath.setNodeAttribute("title", title);
                            this.setAttribute("value", title);
                        }
                    </handler>
                    
                    <method name="updateSkinResources">//patch... do we really need opacity in edit mode?
                        super.updateSkinResources();
                        this.interior.cbtext.setAttribute('opacity', 1);
                    </method>
                    
                    <handler name="oneditable">
                        //":" symbol is depricated, since it is used as separator in grid user preferences
                        this.interior.cbtext.setAttribute("pattern", "[^[:]]*");
                        //sem max length
                        this.interior.cbtext.setAttribute("maxlength", 100);
                    </handler>
                </synapcombobox>
            </synapgridcolumn>
            
        </consolegrid>
        
        <method name="create" args="configurator_items_array">
            this.configGrid.setAttribute("datapath", "local:parent.dummyItemsDataset:/items"); //detach UI
            
            this.itemsArray = [];
            for(var i = 0; i &lt; configurator_items_array.length; i++){
                itemsArray.push(configurator_items_array[i].clone());
            }
            
            var dp = itemsDataset.getPointer();
            if(dp.setXPath("items")){
                dp.deleteNode();
                dp.destroy();
            }
            dp = itemsDataset.getPointer();
            dp.addNode("items");
            
            for(var i = 0; i &lt; itemsArray.length; i++){
                var currItem = itemsArray[i];
                var isSel = currItem.selected ? "true" : "false";
                dp = itemsDataset.getPointer();
                dp.setXPath("items");
                dp.addNode("item", null, {itemid: i, selected: isSel,
                                    propertyName: this.getDispPropertyName(currItem.columnTemplate),
                                    displayMode: currItem.columnTemplate.displayMode,
                                    title: currItem.columnTemplate.title});
                dp.destroy();
            }
            itemsDataset.ondata.sendEvent();
            
            this.configGrid.setAttribute("datapath", "local:parent.itemsDataset:/items"); //attach UI        	
        </method>
        
        <method name="getSelectedColumnTemplates">
            this.synchItemsAndXml();
            
            var result = [];
            for(var i = 0; i &lt; itemsArray.length; i++){
                if(itemsArray[i].selected){
                    result.push(itemsArray[i].columnTemplate);
                }
            }
            return result;
        </method>
        
        <method name="synchItemsAndXml">
            var dp = itemsDataset.getPointer();
            if(dp.setXPath("items")){
                dp.selectChild();
                do{
                    var sel = dp.getNodeAttribute("selected");
                    sel = sel == "true" || sel == true ? true : false;
                    var dispMode = dp.getNodeAttribute("displayMode");
                    var title = dp.getNodeAttribute("title");
                    
                    var item = getItemByItemId(dp.getNodeAttribute("itemid"));
                    if(item != null){
                        item.selected = sel;
                        var colTempl = item.columnTemplate;
                        colTempl.displayMode = dispMode;
                        colTempl.title = title;
                    }                    
                }while (dp.selectNext());
            }
            dp.destroy();
        </method>
        
        <method name="reset">
            var dp = itemsDataset.getPointer();
            if(dp.setXPath("items")){
                dp.deleteNode();
                dp.destroy();
            }
        </method>
        
        <method name="getItemByItemId" args="item_id">
            return this.itemsArray[Number(item_id)];
        </method>
        
        <method name="getDispPropertyName" args="column_template">
            return CommonTabularUtils.getDispPropertyTemplateName(column_template.propertyObjectType, column_template.propertyName);
        </method>
    </class>
</library>