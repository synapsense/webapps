<library>      
    <include href="../../../data/synapdataset.lzx"/>
    <include href="../../synaplvcharts/lvchartdialog.lzx"/>
    <include href="../../../lang/promptlistener.lzx"/>
    <include href="../../../utils/es/propertyid.lzx"/>
    <include href="../../rowdatadialog.lzx"/>
    
    <include href="metricwindow.lzx"/>
    
    <synapdataset name="PropData" type="http" request="false" src="http:getMetrics"/>      
        
    <class name="metricsview" extends="synaptabview" initstage="defer">
        <attribute name="type" type="string" value="CRAC"/>
        <attribute name="objId" type="string" value="85"/>
        <attribute name="wnd_spacing" value="$once{0}"/>

        <method name="refresh" args="visibleRefresh=false">
            PropData.doRequest();
        </method>

        <method name="selectProperty" args="propertyName, isHistoric">
            //show chart dialog for historical properties only
            if(isHistoric != null &amp;&amp; isHistoric == "true"){
                var objId = OptTree.getSelectedObjectId();
                
                LvChartDialog.create(this.objId, this.type, propertyName, -24);
                LvChartDialog.setAttribute("prompt", "title/historical_data");
            }        	
        </method>
        
        <method name="create" args="windowToOpen=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }            
            
            if (OptTree.getSelectedObjectType() != "") {
                this.update();  
                if (windowToOpen) {
                    windowToOpen.open();
                }              
            }
            else {
                InfoDialog.show("message/select_datacenter_or_room");
            }
            
        </method>                
        
        <method name="update">                                           
            this.updateParent();
                
            var params = new LzParam();              
            params.addValue("id", this.objId);
            params.addValue("type", this.type);                                
                    
            PropData.setQueryString(params);
            PropData.doReq(null);            
        </method>
        
        <method name="updateParent">
             var parentType = OptTree.getSelectedObjectType();                
                         
             this.type = parentType;  
             this.objId = OptTree.getSelectedObjectId();
        </method>        
       
        <metricwindow width="500" height="${immediateparent.height}"        			  
                      contentdatapath="*">
            
            <datapath xpath="PropData:/object/category" pooling="true"/>
            
            <handler name="ondata" args="a">
                this.updateVisualization();
                this.metricsGrid.content.Nodatatext.setAttribute("visible", this.datapath.xpathQuery("./" + this.contentdatapath + "/last()") == null);        		        		
                this.setAttribute("prompt", this.datapath.xpathQuery("@title"));
            </handler>
            
            <handler name="onwidth" reference="classroot"  method="updateVisualization" />
            
            <method name="updateVisualization" args="ignore=null">
                var spacing = classroot.wnd_spacing;
                var wndNumber = Number(this.datapath.xpathQuery("PropData:/object/category/last()"));
                var wndWidth = Math.floor((classroot.width - (wndNumber - 1) * spacing) / wndNumber);
                this.setAttribute("width", wndWidth);
                if(this.clonenumber != null){
                    this.setAttribute("x", (wndWidth + spacing) * this.clonenumber);
                }else{
                    this.setAttribute("x", 0);
                }
            </method>
            
        </metricwindow>
        
        <!--<simplelayout axis="y" spacing="10" inset="10"/>        
        <consolegrid name="NodesGrid" gridtitle="${parent.parent.tab.text}" datapath="PropData:/object" contentdatapath="properties/*"
            height="${immediateparent.height - 20 - (classroot['refreshButton'] ? classroot.refreshButton.totalHeight : 0)}" 
            width = "${immediateparent.width}" showcolumnselector="true" selectable="true" _rowclass="lz.metricgridrow" multiselect="false" 
            nodataprompt="text/there_are_no_metrics_to_display" settings="${UserPreference}">
                        
            <synapgridcolumn width="240" prompt="columnname/metrics_name" sortpath="@dName" movable="false">
                <synaptext datapath="@dName" valign="middle"/>
            </synapgridcolumn>
            <synapgridcolumn width="70" prompt="columnname/value" sortable="false" movable="false" datatype="number">
                <text datapath="@v" valign="middle"/>
            </synapgridcolumn>
            <synapgridtext width="50" prompt="columnname/uom" datapath="@u" sortable="false" movable="false"/>
            <synapgridtext width="150" prompt="columnname/timestamp" datapath="@t" sortpath="@lt" movable="false" datatype="number"/>
       -->
            
        <!--</consolegrid>-->
          
    </class>
   
   <!-- Select Time Interval dialog -->
    <managedsynapmodaldialog id="DateIntervalPickerDialog" prompt="title/select_time_interval" width="550" height="120">
        <attribute name="obj_type" type="string" value=""/>
        <attribute name="obj_id" type="string" value=""/>
        
        <simplelayout axis="y" spacing="6"/>
        
        <method name="create" args="type, id, propName">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            
            this.obj_type = type;
            this.obj_id = id;
            this.props = propName;
            
            this.periodPickerView.dateIntervalPeriodPicker.reset();
            
            this.open();
        </method>
        
        <method name="prepareParams">
            var params =  new LzParam();                        
            params.addValue("type", this.obj_type);                        
            params.addValue("id", this.obj_id);
            params.addValue("props", this.props);
            params.addValue("histBy", "betweenDates");                
            params.addValue("begin", this.periodPickerView.dateIntervalPeriodPicker.getStartDateAsLong());
            params.addValue("end", this.periodPickerView.dateIntervalPeriodPicker.getEndDateAsLong());
               
            params.addValue("beginTime", this.periodPickerView.dateIntervalPeriodPicker.getStartDateAsLong());
            params.addValue("endTime", this.periodPickerView.dateIntervalPeriodPicker.getEndDateAsLong());
            
            var propId = new PropertyId(this.obj_id, this.props);
            params.addValue("propIDs", PropertyId.arrayToString([propId]));
            
            return params;
        </method>
        
        <method name="checkDates">
            return this.periodPickerView.dateIntervalPeriodPicker.validate();
        </method>
        
        <view name="periodPickerView" width="${immediateparent.width}">        	            
            <simplelayout axis="y" spacing="10" inset="10"/>           

            <periodpicker name="dateIntervalPeriodPicker" visible="true" startTimeShift="-23" settings="${UserPreference}">
            </periodpicker>
            
            <view name="buttonsView" align="right">
                <simplelayout axis="x" spacing="3"/>
                <synapbutton prompt="button/show">
                    <handler name="onclick">
                        if(!DateIntervalPickerDialog.checkDates()) {
                            return;
                        }
                        DateIntervalPickerDialog.close();
                        var params = DateIntervalPickerDialog.prepareParams();
                        RowDataDialog.show(params);
                    </handler>
                </synapbutton>
            
                <synapbutton prompt="button/close">
                    <handler name="onclick">
                        DateIntervalPickerDialog.close();
                    </handler>
                </synapbutton>
            </view>
        </view>
    </managedsynapmodaldialog>
    
</library>