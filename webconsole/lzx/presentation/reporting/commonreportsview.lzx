<library>
    <include href="../../utils/utils.lzx"/>
    <include href="../../data/synapdataset.lzx"/>
    <include href="../../components/synaptooltip.lzx"/>
    <include href="../../components/consolegrid/consolegrid.lzx"/>
    
    <class name="generatedreportsgridrow" extends="synapgridrow">
        <handler name="ondblclick">
            var reportId = this.datapath.xpathQuery("@reportId");
            if (this.selected &amp;&amp; reportId) {
                this.ownerGrid.classroot.requestReportView();
                this.ownerGrid.clearSelection();
            }
        </handler>
    </class>
    
    <class name="commonreportsview" extends="view">
        <attribute name="mainSpacing" value="10" type="number"/>
        
        <synapdataset name="ReportData" type="http" src="http:getReportData"  timeout="300000"/>
        <synapdataset name="ReportManagement" type="http" src="http:manageReports"  timeout="300000"/>
        <synapdataset name="ReportViewData" type="http" src="http:getReportData"  timeout="300000"/>
        
        <view name="content" width="${immediateparent.width}" height="${immediateparent.height}">
            <simplelayout axis="y" spacing="$once{classroot.mainSpacing}"/>
            
            <view name="gridView" width="${immediateparent.width}"
                  height="${immediateparent.height - parent.buttonsView.height - classroot.mainSpacing}">
                
                <consolegrid name="CommonReportsGrid"
                             datapath="local:parent.parent.parent.ReportData:/result/reports"
                             contentdatapath="report"
                             _rowclass="lz.generatedreportsgridrow"
                             selectable="true" showcolumnselector="true" multiselect="true"
                             selectiondatapath="@name"
                             width = "${immediateparent.width}"
                             height="${immediateparent.height}"
                             nodataprompt="message/there_are_no_available_reports">
                    
                    <synapgridcolumn width="150" sortpath="@name" prompt="columnname/report_name" >
                        <text datapath="." valign="middle" clickable="true" showhandcursor="false">
                            <synaptooltip name="tt" multiline="true"/>
                            <handler name="ondata">
                                var message = this.datapath.xpathQuery("@name");
                                //var description = this.datapath.xpathQuery("@description");

                                //this.tt.setAttribute("text", description);
                                this.setAttribute("text", removeCommandCharacters(message));
                            </handler>
                        </text>
                    </synapgridcolumn>
                    
                    <synapgridcolumn width="150" prompt="columnname/completed" sortpath="@lt" datatype="number">
                        <text datapath="." valign="middle">
                            <handler name="ondata">
                                this.setAttribute("text", this.datapath.xpathQuery("@timestamp"));
                            </handler>
                        </text>
                    </synapgridcolumn>
                    
                    <synapgridcolumn width="80" sortpath="@statusLabel" prompt="columnname/status">
                        <text datapath="." valign="middle" clickable="true" showhandcursor="false">
                            <synaptooltip name="tt" multiline="true"/>
                            <handler name="ondata">
                                var message = this.datapath.xpathQuery("@statusMessage");
                                var statusLabel = this.datapath.xpathQuery("@statusLabel");
                                
                                this.tt.setAttribute("text", message);
                                this.setAttribute("text", statusLabel);
                            </handler>
                        </text>
                    </synapgridcolumn>
                    
                    <synapgridcolumn width="70" prompt="columnname/progress" sortpath="@progress" datatype="number">
                        <text datapath="." valign="middle">
                            <handler name="ondata">
                                var message = parseInt(this.datapath.xpathQuery("@progress"));
                                this.setAttribute("text", message + "%");
                            </handler>
                        </text>
                    </synapgridcolumn>
                    
                    <handler name="onselect">
                        classroot.updateButtonsVisualization();
                    </handler>
                </consolegrid>
            </view>
            
            <view name="buttonsView" width="${immediateparent.width}" pixellock="true">
                <synapbutton name="refreshButton" prompt="button/refresh">
                    <handler name="onclick">
                        classroot.refresh();
                    </handler>
                </synapbutton>

                <view align="right" name="rightButtons">
                    <simplelayout axis="x" spacing="5"/>
                                    
                    <synapbutton name="stopButton" prompt="Stop" enabled="false" visible="false">
                        <handler name="onclick">
                            var commandName = classroot.getCommonReportsGrid().getSelectedArray()[0];
                            Debug.write("command name: " + commandName);
                            InfoDialog.show("This functionality is not implemented.");
                        </handler>
                    </synapbutton>
                    
                    <securedsynapbutton name="deleteButton" prompt="button/delete" enabled="false" privileges="DELETE_REPORTS">
                        <handler name="onclick">
                            var selArr = classroot.getCommonReportsGrid().getSelection();
                            var confirmMessage = LangManager.getLocaleString("message/are_you_sure_you_want_to_delete_selected_reports");
                            if(selArr.length == 1) {
                                var reportName = selArr[0].xpathQuery("@name");
                                confirmMessage = LangManager.getFormattedLocaleString("message/are_you_sure_you_want_to_delete_report", reportName);
                            }
                            //TODO: i18n
                            ConfirmDialog.show(confirmMessage, classroot, "deleteReport");
                        </handler>
                    </securedsynapbutton>
                    
                    <synapbutton name="viewButton" prompt="button/view" enabled="false">
                        <handler name="onclick">
                            classroot.requestReportView();
                        </handler>
                    </synapbutton>
                </view>
            </view>
        </view>
        
        <handler name="ondata" reference="this.ReportViewData">
            var dp = ReportViewData.getPointer();        	
            var formatsNumber = Number(dp.xpathQuery("result/entry/last()"));        	
            if(formatsNumber == 1){
                var path = dp.xpathQuery("result/entry/@url");
                path = escape(path);
                Debug.write(">> path = " + path);
                SynapURLLoader.loadURL("getReport?url=" + path, '_blank');
            }else{
                ReportFormatPickerDialog.create(ReportViewData);
            }        	
            dp.destroy();
        </handler>
        
        <handler name="ondata" reference="this.ReportManagement">
            this.refresh();
        </handler>
        
        <method name="create" args="windowToOpen=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }            
            this.reset();
            
            var params = new LzParam();              
            params.addValue("action", "getCommon");
            
            ReportData.setQueryString(params);
            ReportData.doReq(windowToOpen);
        </method>
        
        <method name="requestReportView" args="ignore=null">
            var selArr = this.getCommonReportsGrid().getSelection();
            
            if(selArr.length == 1){
                var params = new LzParam();              
                params.addValue("action", "getReportView");
                params.addValue("reportId", selArr[0].xpathQuery("@reportId"));
                
                ReportViewData.setQueryString(params);
                ReportViewData.doReq(null);
            }
        </method>
        
        <method name="deleteReport" args="ignore=null"><![CDATA[
            var reportIds = getSelectedIds("reportId");
            var commandIds = getSelectedIds("commandId");
            
            var params = new LzParam();              
            params.addValue("action", "deleteReports");
            params.addValue("reportIds", reportIds);
            params.addValue("commandIds", commandIds);
            
            ReportManagement.setQueryString(params);
            ReportManagement.doReq(null);
        ]]></method>
        
        <method name="getSelectedIds" args="type"><![CDATA[
            var resArr = [];
            var selArr = this.getCommonReportsGrid().getSelection();
            for(var i=0; i<selArr.length; i++) {
                var val = selArr[i].getNodeAttribute(type);
                if(val) {
                    resArr.push(val);
                }
            }
            return resArr.toString();
        ]]></method>
        
        <method name="refresh">
            ReportData.doReq(null);
        </method>
        
        <method name="getCommonReportsGrid">
            return this.content.gridView.CommonReportsGrid;
        </method>
        
        <method name="getStopButton">
            return this.content.buttonsView.rightButtons.stopButton;
        </method>
        
        <method name="getViewButton">
            return this.content.buttonsView.rightButtons.viewButton;
        </method>
        
        <method name="getDeleteButton">
            return this.content.buttonsView.rightButtons.deleteButton;
        </method>
        
        <method name="reset">
            this.getCommonReportsGrid().clearSelection();
            this.updateButtonsVisualization();
        </method>
        
        <method name="updateButtonsVisualization">
            var selectionDataPointer = this.getCommonReportsGrid().getSelection();
            
            this.getStopButton().setAttribute("enabled", this.checkTasksForStopButton(selectionDataPointer));
            this.getViewButton().setAttribute("enabled", this.checkTasksForViewButton(selectionDataPointer));
            this.getDeleteButton().setAttribute("enabled", this.checkTasksForDeleteButton(selectionDataPointer));
        </method>
        
        <method name="checkTasksForViewButton" args="dpArr">
            if(!dpArr) return false;
            var res = false;
            
            if(!(dpArr instanceof Array)) {
                dpArr = [dpArr];
            }
            
            if(dpArr.length > 1) return res;
            
            for(var i=0; i &lt; dpArr.length; i++) {
                var reportId = dpArr[i].xpathQuery("@reportId");
                if(reportId == null) {
                    return false;
                }
                res = true;
            }
            
            return res;
        </method>
        
        <method name="checkTasksForDeleteButton" args="dpArr">
            if(!dpArr) return false;
            
            var res = false;
            if(!(dpArr instanceof Array)) {
                dpArr = [dpArr];
            }
            
            for(var i=0; i &lt; dpArr.length; i++) {
                var status = dpArr[i].xpathQuery("@status");
                var statusInfo = this.getStatusInfo(status);
                var commandId = dpArr[i].xpathQuery("@commandId");
                if(commandId != null &amp;&amp; status != "FAILED") {
                    return false;
                }
                res = true;
            }
            
            return res;
        </method>
        
        <method name="checkTasksForStopButton" args="dpArr">
            if(!dpArr) return false;
            
            if(!(dpArr instanceof Array)) {
                dpArr = [dpArr];
            }
            var res = false;
            
            for(var i=0; i &lt; dpArr.length; i++) {
                var status = dpArr[i].xpathQuery("@status");
                var statusInfo = this.getStatusInfo(status);
                if(!statusInfo.isFinished) {
                    return false;
                }
                res = true;
            }
            
            return res;
        </method>
        
        <method name="getStatusInfo" args="statusCode">
            var statusInfo = {isFinished: true};
            if("CANCELLED" == statusCode){
                statusInfo.isFinished = true;
            }else if("COMPLETED" == statusCode){
                statusInfo.isFinished = true;
            }else if("FAILED" == statusCode){
                statusInfo.isFinished = true;
            }else if("IN_PROGRESS" == statusCode){
                statusInfo.isFinished = false;
            }else if("NOT_STARTED" == statusCode){
                statusInfo.isFinished = false;
            }else if("SUSPENDED" == statusCode){
                statusInfo.isFinished = false;
            }
            return statusInfo;
        </method>
        
    </class>
    
    <managedsynapmodaldialog id="ReportFormatPickerDialog" width="300" prompt="title/choose_format">
        
        <synapdataset name="dataDS"/>
        
        <view name="dialogContent" width="${immediateparent.width}">
            <simplelayout axis="y" spacing="10"/>
            <synaptext prompt="message/please_choose_format"/>
            <view name="buttonsView" align="center" pixellock="true">
                <simplelayout axis="x" spacing="10"/>
                <synapbutton datapath="local:parent.parent.parent.dataDS:/result/entry">
                    <handler name="ondata">
                        this.setAttribute("prompt", "text/" + this.datapath.xpathQuery("@format"));
                    </handler>
                    
                    <handler name="onclick">
                        var path = this.datapath.xpathQuery("@url");
                        path = escape(path);
                        Debug.write(">> path = " + path);
                        SynapURLLoader.loadURL("getReport?url=" + path, '_blank');
                        ReportFormatPickerDialog.close();
                    </handler>
                </synapbutton>          
            </view>
        </view>
        
        <method name="create" args="dataset">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            
            this.dataDS.reset();
            
            var dp1 = this.dataDS.getPointer();
            var dp2 = dataset.getPointer();
            
            if(dp2.setXPath("result")){
                dp1.addNodeFromPointer(dp2);
            }
            
            this.open();
        </method>
        
    </managedsynapmodaldialog>    
    
</library>