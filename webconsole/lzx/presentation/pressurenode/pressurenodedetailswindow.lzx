<library>
    <include href="../../data/synapdataset.lzx"/>
    <include href="../../skins/components/skinnedicon.lzx"/>
    <include href="../../components/synapgauge.lzx"/>
    <include href="../../components/synaptext.lzx"/>
    <include href="../../components/synaptooltip.lzx"/>
    <include href="../../components/navigationbuttons.lzx"/>
    <include href="../../skins/components/skinnedbasebutton.lzx"/>
    <include href="../../statistics/userstatistics.lzx"/>
    <include href="../alerts/alertlink.lzx"/>
    <include href="../../utils/es/to.lzx"/>
    <include href="targetpressureview.lzx"/>
    
    <synapdataset name="PressureRealtimeData" type="http" src="http:getObject"/>

    <maximizablesynapdatawindow id="PressureNodeDetailsWindow" level="1" use_insets="true" initstage="defer" timerUpdate="true">
        <attribute name="objId" value="" type="string"/>
        <attribute name="objType" value="" type="string"/>
        <attribute name="objName" value="" type="string"/>
        <attribute name="totalTitle" type="string" value=""/>
        
        <dataset name="chartConfig">        	
        </dataset>
        
        <onvisiblestatistic statName="Window_ObjectDetailsWindow"/>
        
        <method name="refresh" args="visibleRefresh=false">
            if (visibleRefresh) {
                PressureRealtimeData.doReq();
            } else {
                PressureRealtimeData.doRequest();
            }
        </method>
        
        <handler name="alertUpdated" reference="canvas">
            this.refresh();
        </handler>
        
        <method name="setDatapathByType" args="obj,type">
            obj.setAttribute("datapath", "PressureRealtimeData:/objects/" + type + "/links/pressure");
            obj.datapath.setAttribute("pooling", true);
        </method>
        
        <method name="create" args="id, ids=null, options=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            var type = TO.fromString(id).getType();
            this.setAttribute("objId", id);
            this.setAttribute("objType", TO.fromString(id).getType());
            
            this.navigationButtons.setAttribute("currId", id);
            this.navigationButtons.setAttribute("ids", ids);
            
            this.setDatapathByType(PressureGaugeContainer.cgauge,type);
            
            PressureNodeSummaryView.setDatapath("PressureRealtimeData:/objects/"+type);
            
            //PressureRealtimeData
            var sensorProps = "name,aMin,aMax,rMin,rMax,max,min,lastValue,channel,dataclass,enabled";
            var params =  new LzParam();
            var links = TypeUtil.getSensorLinkRequestObjects();
            var sensorLinkRequestObject = TypeUtil.getLinkRequestByType(links, "WSNSENSOR");
            var sensorProperties = sensorProps.split(",");
            var sensorFilterObject = [{name: "enabled", value: "1"}];
            sensorLinkRequestObject["properties"] = sensorProperties;
            sensorLinkRequestObject["filter"] = sensorFilterObject;
            var objectRequest = {id: id, 
                                 links: links,
                                 parents : [{type: "ROOM", properties: ["id", "name", "roomType", "controlType"]}],
                                 excludedProperties: TypeUtil.propsToExclude,
                                 children: [{type: "WSNSENSOR", properties: sensorProperties, filter: sensorFilterObject}]
                                };
            params.addValue("objectRequest", JSON.stringify(objectRequest));
            
            PressureRealtimeData.setQueryString(params);
            PressureRealtimeData.doReq(this);
        </method>
        
        <handler name="onwidth">
            this.setAttribute("title", this.totalTitle);
        </handler>
        
        <handler name="ontitle">
            this.title_area.title.setAttribute("text", this.truncatTitle(this.title));
        </handler>
        
        <method name="truncatTitle" args="txt">
            var tt = new lz.trunctext(null, {id : "TitleText", width : (navigationButtons.x != 0) ? (navigationButtons.x-20) : this.width, visible : false, text : txt, options : "ignorelayout"});
            var res_title = tt.text;
            tt.destroy();
            return res_title;
        </method>

        <handler name="ondata" reference="PressureRealtimeData">
            noPressureDataText.setAttribute("visible", !this.isAnyGaugeShown());
            
            var tail_name = PressureRealtimeData.getPointer().xpathQuery("objects/"+this["objType"]+"/name/@v");
            this.setAttribute("objName", tail_name);
            if(tail_name == null){
                tail_name = getMappedObjectTypeName(this["objType"]);
            }
            
            var totalTitle = OptTree.getCurrentLocationString() + ": " + tail_name;
            totalTitle = LangManager.getFormattedLocaleString("title_rack_summary", totalTitle);
            this.setAttribute("totalTitle", totalTitle);
            this.setAttribute("title", totalTitle);            
        </handler>
        
        <method name="reset" args="ignore=null">
            PressureRealtimeData.reset();
        </method>
        
        <method name="isAnyGaugeShown">
            var dp = PressureRealtimeData.getPointer();
            var gaugesNumber = Number(dp.xpathQuery(PressureGaugeContainer.histGauge.datapath.xpath + "/last()")) + Number(dp.xpathQuery(PressureGaugeContainer.cgauge.datapath.xpath + "/last()"));
            return gaugesNumber == 0 ? false : true;
        </method>
        
        <navigationbuttons name="navigationButtons" placement="title_area" x="${immediateparent.width - immediateparent.controls.width - this.width}">
            <!--override-->
            <method name="navigate" args="id">
                PressureNodeDetailsWindow.create(id, this.ids);
            </method>
        </navigationbuttons>
        
        <simplelayout axis="y" spacing="5"/>
        
        <view id="PressureNodeSummaryView" width="${immediateparent.width}">
            <!-- wrappinglayout axis="x" spacing="10"/ -->
            
            <handler name="ondata">
                this.setSummaries(PressureNodeDetailsWindow.objId);
            </handler>

            <method name="setSummaries" args="objId">
                var type = TO.fromString(objId).getType();
                var dp  = ConfigData.getPointer();
                if(!dp.setXPath("config/feature[@name='ObjectDetailsSummary']/type[@name='" + type + "']/*[1]")){
                    this.setAttribute("visible", false);
                    return;
                }else{
                    this.setAttribute("visible", true);
                }

                var i = 1;
                if (this["summaryBox"])
                    this["summaryBox"].destroy();
                
                var p = new lz.view(this, {name: "summaryBox", width: PressureNodeSummaryView.width, layout:{'class': "wrappinglayout", axis: "x", spacing: 10}});
                var d=[PressureNodeSummaryView, "width"]; 
                p.applyConstraint("width", function(ignore=null) {p.setAttribute("width", PressureNodeSummaryView.width);}, d);
                //p.setLayout({'class': "wrappinglayout", axis: "x", spacing: 10});
                
                do {
                    var name = dp.xpathQuery("@prompt");
                    var xpath = dp.xpathQuery("@xpath");
                    var value = this.datapath.xpathQuery(xpath);
                    
                    //if value is null, set default value
                    if(value == null || value == ""){
                        value = LangManager.getLocaleString(dp.xpathQuery("@defaultvalueprompt"));
                    }
                    
                    if(value == null){
                       value = LangManager.getLocaleString("text/n_a");
                    }
                    if(name != null || name != "") {
                        var o = new lz.view(p, {datapath: "PressureRealtimeData:/objects/*/", width: 300, layout: {'class': 'simplelayout', axis: 'x'}});
                        //o.setLayout({'class': 'simplelayout', axis: 'x'});
                        var nameText = new lz.synaptext(o, {prompt: name});
                        if (xpath == "numAlerts/@v"){
                            var id = this.datapath.xpathQuery("@id");
                            var type = this.datapath.xpathQuery("@type");
                            new lz.alertlink(o, {text: value, 
                                              fontstyle:"bold", 
                                              enabled: value != 0, 
                                              objId: id, objType: type }) ;
                        } else {
                            new lz.trunctext(o, {text: value + "", 
                                           fontstyle:"bold", 
                                           fgcolor:lz.colors.black, width : o.width - nameText.width});
                        }
                    }
                    i++;
                } while (dp.selectNext());
                dp.destroy();
            </method>
        </view>

        <view name="mainView" height="${parent.height - PressureNodeSummaryView.height - 5 - parent.inset_top - parent.inset_bottom}" 
              width = "${parent.width - parent.inset_left - parent.inset_right}">
            
            <simplelayout axis="y" spacing="10" />

            <skinnedicon name="PressureChartIcon" resourceName="IMG_CHART_ICON_RESOURCE" options="ignorelayout" y="-10" stretches="both" align="right">
                <handler name="onclick">
                    Debug.write("objId = ", PressureNodeDetailsWindow.objId);
                    Debug.write("objName = ", PressureNodeDetailsWindow.objName);
                    
                    LvCommonChartDialog.create({sourcetype: "uiconfig", objId: PressureNodeDetailsWindow.objId, objType: "PRESSURE", objName: PressureNodeDetailsWindow.objName, placement: "floorplancharts", startTimeShiftMs: 2*60*60*1000, viewPreferenceName: "floorplanChartPref"});
                </handler>                    
                <synaptooltip name="tt" prompt="tooltip/historical_graph"/>
            </skinnedicon>
            
            <synaptext id="noPressureDataText" visible="false" valign="top" align="center" fontsize="15" prompt="text/no_information_to_display"/>
            <view width="${immediateparent.width}" height="${immediateparent.height}">
                <simplelayout axis="y" spacing="10"/>
                <view width="${immediateparent.width - immediateparent.sb.width }">
                    <simplelayout axis="y"/>
                    <view width="${immediateparent.width}" id="PressureGaugeContainer">
                        <wrappinglayout id="pressureGaugeLayout" axis="x" spacing="10"/>
                        
                        <synapgauge name="cgauge" width="150" height="230" decimalPrecision="${UserPreference.DecimalFormat}" typePath="../../name()">
                            <attribute name="group" type="number" value="1"/>
                            <datapath xpath="PressureRealtimeData:/objects/*/links/*" pooling="true"/>
                            <handler name="ondata">
                                this.setAttribute("decimalPrecision",UserPreference.DecimalFormat);//this.datapath.xpathQuery("../../decimalFormat/@v"));
                                 
                                 var sensCount = this.datapath.xpathQuery("last()") - 1;
                                 if (this.clonenumber == sensCount){                                     
                                     pressureGaugeLayout.sortByGroup();
                                     pressureGaugeLayout.update();
                                 }                                 
                            </handler>
                        </synapgauge>
                        
                        <synapgauge name="histGauge" width="150" height="230" decimalPrecision="${UserPreference.DecimalFormat}" typePath="../name()">
                              <attribute name="group" type="number" value="2"/>
                            <datapath xpath="PressureRealtimeData:/objects/*/*[@h='true'][@v]" pooling="true"/>
                            <handler name="ondata">
                                if(UserPreference.DecimalFormat!=null){
                                       this.setAttribute("decimalPrecision",UserPreference.DecimalFormat);
                                }
                            </handler>
                        </synapgauge>
    
                    </view>
                </view>
        
                <targetpressureview id="TargetPressureView" visible="true">
                    <datapath xpath="PressureRealtimeData:/objects/*" pooling="true"/>
                </targetpressureview>

                <synapscrollbar name="sb" axis="y" visible="${scrollable}"/>
            </view>

        </view>
    </maximizablesynapdatawindow>
</library>
