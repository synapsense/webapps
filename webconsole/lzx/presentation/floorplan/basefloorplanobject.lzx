<library>
    <include href="../../utils/eventpropagator/eventpropagator.lzx"/>
    <include href="../../utils/es/escommands.lzx"/>
    <include href="../../statistics/userstatistics.lzx"/>
    
    <class name="basefloorplanobject">
        <attribute name="commoncmcontainer" type="expression" value="null"/>
        <attribute name="floorplanobjectselector" required="true"/>
        <attribute name="layer" type="expression" value="$once{classroot}"/>
        <attribute name="width" value="${obj_width}"/>
        <attribute name="height" value="${obj_height}"/>
        <attribute name="x" value="${this.scale * this.obj_x - Math.ceil(this.width / 2)}"/> <!-- coordinates - are center point coordinates. Icon should be moved. -->
        <attribute name="y" value="${this.scale * this.obj_y - Math.ceil(this.height / 2)}"/> <!-- coordinates - are center point coordinates. Icon should be moved. -->
        <attribute name="obj_x" type="number" value="0"/>
        <attribute name="obj_y" type="number" value="0"/>
        <attribute name="obj_width" type="number" value="${obj_height * icon.unstretchedwidth / icon.unstretchedheight}"/>
        <attribute name="obj_height" type="number" value="20"/>
        <attribute name="scale" type="number" value="1"/>
        <attribute name="type" type="string"/>
        <attribute name="envObjName" type="string" value=""/>
        <attribute name="nameXPath" type="string" value="name/@v"/>
        <attribute name="status" type="string" value=""/>
        <attribute name="statusXPath" type="string" value="status/@v"/>
        <attribute name="envObjId" type="string" value=""/>
        <attribute name="envObjType" type="string" value=""/>
        <attribute name="clickable" value="true"/>
        <attribute name="pixellock" value="true"/>
        <attribute name="replicatedView" value="$once{this}"/>
        <attribute name="isLongMouseOver" value="false"/>

        <attribute name="tooltipclass" value="null" type="string"/>
        
        <ondblclickstatistic statName="${'Floorplan_DoubleClick_'+classroot.envObjType}"/>
        <!--pretent we are skinnedicon, views shouldn't be inserted into view with
            stretched resource, size will be calculated incorrectly, e.g WSNGATEWAY
            icon stresches when "film" gets visible-->
        <attribute name="resourceName" type="string" value=""/>       
        <attribute name="currentResourceNum" type="number" value="1"/>
        
        <attribute name="longMouseOverInterval" type="number" value="500"/>
        <attribute name="longMouseOverDel" value="$once{new lz.Delegate(this, 'sendLongMouseOver')}"/>
        <event name="onlongmouseover"/>
        
        <event name="onenvObjId"/>
        <setter name="envObjId" args="envObjId"> <![CDATA[
            if (this.envObjId != envObjId) {
                if (this.floorplanobjectselector) {
                    if (this.floorplanobjectselector.checkRegistration(this)) {
                        this.floorplanobjectselector.unregisterFloorplanobject(this);
                    }
                }
            }
            this.envObjId = envObjId;
            if (this.onenvObjId.ready) {
                this.onenvObjId.sendEvent(envObjId);
            }
        ]]> </setter>
        
        <attribute name="selected" type="boolean" value="false"/>
        <!---Just to show a selection-->
        <view name="selector" visible="${classroot.selected}" bgcolor="#3366FF"
              opacity="0.5" width="${classroot.width}" height="${classroot.height}"
              clickable="false">
            <handler name="onvisible" args="vis">
                if (vis) {
                    this.sendBehind(classroot.film);
                }
            </handler>
        </view>
        
        <method name="activateForSelection">
            this.film.activate();
        </method>
        <method name="deactivateForSelection">
            this.film.deactivate();
        </method>

       <skinnedicon name="icon" stretches="both" width="${classroot.width}" height="${classroot.height}" resourceName="${classroot.resourceName}" currentResourceNum="${classroot.currentResourceNum}"/>

        <!---Functional view-->
        <view name="film" width="${classroot.width}" height="${classroot.height}" 
              clickable="true" visible="false" pixellock="true">
            <method name="activate">
                this.bringToFront();
                this.setAttribute("visible",true);
            </method>
            <method name="deactivate">
                this.setAttribute("visible",false);
            </method>
            <handler name="onselected" reference="classroot" args="sel"> <![CDATA[
                if (!sel && !classroot.floorplanobjectselector.ctrlselectionactive) {
                    this.setAttribute("visible",false);
                } else if (sel) {
                    activate();
                }
            ]]> </handler>
            <setter name="visible" args="vis"> <![CDATA[
                super.setAttribute("visible",classroot.selected || vis);
            ]]> </setter>
            <eventpropagator edgeView="${classroot}" eventNames="['onmouseover','onmouseout','onclick','ondblclick']">
                <method name="isPropagatable" args="eventName"> <![CDATA[
                    if (classroot.floorplanobjectselector.active && 
                            (eventName == "onmouseover" || eventName == "onmouseout")) {
                        return true;
                    } else if (classroot.floorplanobjectselector.active) {
                        return false;
                    }
                    return super.isPropagatable(eventName);
                ]]> </method>
            </eventpropagator>
            <!--handler name="onmouseover" args="view">
               parent["onmouseover"].sendEvent(view);
            </handler>
            <handler name="onmouseout" args="view">
               parent["onmouseout"].sendEvent(view);
            </handler>
            <handler name="onclick" args="view">
               parent["onclick"].sendEvent(view);
            </handler>
            <handler name="ondblclick" args="view">
               parent["ondblclick"].sendEvent(view);
            </handler-->
            <handler name="onmousedown" args="view"> <![CDATA[
                if (classroot.selected && classroot.floorplanobjectselector.draggingEnabled) {
                    classroot.floorplanobjectselector.startDragging();
                }
            ]]> </handler>
            <handler name="onmouseup" args="view">
                if (!classroot.floorplanobjectselector.dragging) {
                    if (!classroot.floorplanobjectselector.ctrlselectionactive) {return;}
                    if (classroot.selected) {
                        classroot.floorplanobjectselector.unselect(classroot);
                    } else {
                        classroot.floorplanobjectselector.select(classroot, true);
                    }
                } else {
                    classroot.floorplanobjectselector.stopDragging();
                }
            </handler>
        </view>

        <handler name="onmouseover" args="view">
            lz.Timer.addTimer(this.longMouseOverDel, this.longMouseOverInterval);
        </handler>

        <method name="sendLongMouseOver" args="ignore=null">
            this.isLongMouseOver = true;
            this.onlongmouseover.sendEvent(null);
        </method>

        <handler name="onmouseup" args="view"> <![CDATA[
            if (!this.floorplanobjectselector.dragging) {
                this.floorplanobjectselector.clearSelection();
            }
        ]]> </handler>
        
        <handler name="onenvObjId" args="envObjId"> <![CDATA[
            if (this.floorplanobjectselector) {
                this.floorplanobjectselector.registerFloorplanobject(this);
            }
        ]]> </handler>

        <handler name="oninit" args="ignore"> <![CDATA[
            if (this.floorplanobjectselector) {
                this.floorplanobjectselector.registerFloorplanobject(this);
            }
        ]]> </handler>

        <handler name="ondestroy">
            if (this.floorplanobjectselector) {
                this.floorplanobjectselector.unregisterFloorplanobject(this);
            }
        </handler>
        
        <handler name="ondata" args="d"> <![CDATA[
            if (!this.datapath.p) {
                this.setAttribute("visible",false);
                return;
            }

            var id = this.datapath.xpathQuery("@id");
            var type = this.datapath.xpathQuery("@type");
            var name = this.datapath.xpathQuery(nameXPath);
            var status = this.datapath.xpathQuery(statusXPath);

            this.setAttribute("envObjId", id);
            this.setAttribute("envObjType", type);
            this.setAttribute("envObjName", name);
            this.setAttribute("status", status);
            
            //template method to be implemented in subclasses
            handleData(d);
            
            //this.datapath.p can be updated inside handleData(d), 
            //e.g. we don't want to use this class to show ROOMs on Env floorplan
            if (!this.datapath.p) {
                this.setAttribute("visible",false);
                return;
            }

            //When we use datapath with attribute 'pooling' is true or
            //LazyReplicationManager the same data can be set to different clones
            //So we need to relink tooltips
            if (this.envObjId && this.envObjId != "") {
                var tooltip = this.layer.tooltips[this.envObjId];
                if (tooltip) {
                    this["tooltip"] = tooltip;
                    tooltip.setAttribute("parentNode",this);
                    this["tooltip"].requestData();
                }else if (this["tooltip"]){
                    delete this["tooltip"];
                }
            }
 
            this.setAttribute("visible",true);

            if (this.layer.selectedObjects) {
                var index = this.layer.selectedObjects.indexOf(this.envObjId);
                if (this.visible && index != -1) {
                    this.layer.floorplan.highlightObject(this);
                }
                if (index != -1) {
                    this.layer.selectedObjects.splice(index,1);
                }
            }

  
            if(datapath.xpathQuery("./position()") == datapath.xpathQuery("./last()")) {
                lz.Idle.callOnIdle(new lz.Delegate(this, "sendFloorplanObjectsReplicated"));
            }
        ]]> </handler>
             
        <handler name="onlongmouseover" args="v">
            this.handleLongMouseOver(v);
        </handler>
        
        <handler name="onmouseout" args="v"> <![CDATA[
            this.isLongMouseOver = false;
            lz.Timer.removeTimer(this.longMouseOverDel);
            this.removeHint();
            this.handleMouseOut(v);
        ]]> </handler>
        
        <handler name="onclick" args="v">
            this.handleClick(v);
        </handler>
        
        <handler name="ondblclick" args="v"> <![CDATA[
            this.handleDblClick(v);
        ]]> </handler>
        
       
        <method name="handleData" args="d"/>

        <method name="handleLongMouseOver" args="d">
            if(status == GlobalConstants.statusDisabled) {
                this.createHint();
            } else {
                layer.createTooltip(this);
            }
        </method>

        <method name="handleMouseOut" args="d"> <![CDATA[
            if (this["tooltip"] && !this["tooltip"].keep) {
                layer.removeTooltip(this);
            }
        ]]> </method>

        <method name="handleClick" args="d">
            if(status == GlobalConstants.statusDisabled) {
                return;
            }
            if (this["tooltip"]) {
                this["tooltip"].setAttribute("keep", !this["tooltip"].keep);
            }
        </method>
        
        <method name="createHint" args="ignore=null">
            var type = this.datapath.xpathQuery("@type");
            if (isDefined("Hint")) {
                global["Hint"].destroy();
            }
            global["Hint"] = new lz.SynapHint(this.parent.parent, {parentNode : this, id : "Hint", type: type});
        </method>
        
        <method name="removeHint" args="ignore=null">
            if (isDefined("Hint")) {
                global["Hint"].destroy();
            }
        </method>

        <method name="handleDblClick" args="d">
            if (this["tooltip"]) {
                layer.removeTooltip(this);
            }
        </method>

        <method name="sendFloorplanObjectsReplicated" args="ignore=null">
            if (this.floorplanobjectselector) {
                this.floorplanobjectselector.unpdateFloorplanobjectsRegistration();
            }
            this.floorplanobjectselector.layer.floorplan.notifyFloorplanObjectsReplicated();
        </method>
        
        <method name="startDragging"> <![CDATA[
            dragger.setAttribute("applied", true);
        ]]> </method>

        <method name="stopDragging"> <![CDATA[
            dragger.setAttribute("applied", false);
            var x = this.scale * this.obj_x - Math.ceil(this.width / 2);
            var y = this.scale * this.obj_y - Math.ceil(this.height / 2);
            if (this.x != x) {
                var newX = (this.x + Math.ceil(this.width / 2))/this.scale + this.floorplanobjectselector.layer.floorplan.floorplan_x;
                EsCommands.setPropertyValue(this.envObjId, "x", newX);
            }
            if (this.y != y) {
                var newY = (this.y + Math.ceil(this.height / 2))/this.scale + this.floorplanobjectselector.layer.floorplan.floorplan_y;
                EsCommands.setPropertyValue(this.envObjId, "y", newY);
            }
        ]]> </method>

        <handler name="onmenuopen" args="synapcmenu" reference="synapcontextmenu">
            if(!this.selected) {
                this.floorplanobjectselector.select(this, true);
            }
        </handler>
        
        <method name="getOwnerSelection"> <![CDATA[
            return this.floorplanobjectselector.getSelection();;
        ]]> </method>
        
        <method name="getContextMenuItems">         
            //override
        </method>

        <method name="isMouseOver"> <![CDATA[
            return super.isMouseOver() && this.isLongMouseOver;
        ]]> </method>
        
        <dragstate name="dragger" applied="false"/>
        
        <synapcontextmenu name="synapcontextmenu" addParentMenu="false"/>
    </class>
</library>