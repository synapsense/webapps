<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     Feb 4, 2009 10:46:41 AM                                                        
     pahunov                                                                
     ====================================================================== -->
<library>
    <include href="../../utils/baseobjecttoolbar.lzx"/>
    <include href="../../utils/smallsynapgauge.lzx"/>
    <include href="../../../../components/synapgauge.lzx"/>
    <include href="../../../../components/synaptooltip.lzx"/>
    <include href="../../../synaplvcharts/lvcommonchartdialog.lzx"/>
    

    <class name="racktoolbar" extends="baseobjecttoolbar" width="240">

        <method name="reset">
            ThermometersView.setAttribute("visible", false);
            GaugesContainer.setAttribute("visible", false);
            GaugesContainer.ts.setAttribute("text", "");
            super.reset();
        </method>

        <simplelayout axis="y" spacing="1"/>
        <view name="IconContainer" align="center">
            <attribute name="currentGaugeValue" value="" type="string"/>

            <simplelayout axis="x" spacing="5"/>
            <view name="AlertSensorIconContainer">
                <skinnedicon resourceName="IMG_ACTIVE_ALARM_BELL_RESOURCE" datapath="ObjectSensorsData:/objects/*/numAlerts" stretches="both">
                    <handler name="ondata">
                        this.setAttribute("visible", parseInt(this.datapath.xpathQuery("@v")) > 0);
                    </handler>
                    <handler name="onclick">
                        ActiveAlertsDialog.create(this.datapath.xpathQuery("ObjectSensorsData:/objects/*/@id"), classroot["type"]);
                        classroot.close();
                    </handler>
                    <synaptooltip prompt="tooltip/pending_alerts"/>
                </skinnedicon>
            </view>
            
            <view>
                <method name="showSensors" args="sensors,title">
                <![CDATA[
                    var dp = classroot.ThermometersView.thermometerContainer.thermometerData.getPointer();
                    if (dp.selectChild()) {
                        do {
                            dp.deleteNode();
                        } while (dp.p);
                        dp.destroy();
                    }
                    var buffDataSet = new LzDataset();
                    var thermometers = buffDataSet.getPointer().addNode("thermometers");
                    var unit = "(" + UnitsUtil.getUnitsByDataclass("200") + ")";
                    if (classroot.ThermometersView.title != title + unit) {
                        classroot.ThermometersView.ts.setAttribute("text", "")
                        var dp1 = ObjectSensorsData.getPointer();
                        for (var i = 0; i < sensors.length; i++) {
                            if (dp1.setXPath("ObjectSensorsData:/objects/RACK/" + sensors[i])) {
                                thermometers.appendChild(dp1.p.cloneNode(true));
                            } else {
                                dp1.setXPath("ObjectSensorsData:/objects/");
                            }
                        }
                        dp1.destroy();
                        classroot.ThermometersView.thermometerContainer.thermometerData.appendChild(thermometers.cloneNode(true));

                        classroot.GaugesContainer.setAttribute("visible", false);
                        classroot.ThermometersView.setAttribute("visible", true);
                        classroot.ThermometersView.setAttribute("title", title + unit);
                    } else {
                        classroot.ThermometersView.setAttribute("visible", false);
                        classroot.ThermometersView.setAttribute("title", "");
                    }
                    
                    dp.destroy();
                    buffDataSet.destroy();
                ]]>
                </method>
                
                <simplelayout axis="x" spacing="5"/>
                <skinnedicon name="ColdAisleIcon" resourceName="FRAMES_RACK_DATA_RESOURCE" currentResourceNum="1" stretches="both">
                    <attribute name="title" type="string" value="tooltip/cold_aisle_temperature"/>
                    <handler name="onclick">
                        var sensors = ["links/cTop","links/cMid","links/cBot","coldDp"];
                        parent.showSensors(sensors, LangManager.getLocaleString(this.title));
                    </handler>
                    <synaptooltip name="tt" prompt="${parent.title}"/>
                </skinnedicon>
                <skinnedicon name="HotAisleIcon" resourceName="FRAMES_RACK_DATA_RESOURCE" currentResourceNum="4" stretches="both">
                    <attribute name="title" type="string" value="tooltip/hot_aisle_temperature"/>
                    <handler name="onclick">
                        var sensors = ["links/chimney","links/hTop","links/hMid","links/hBot"];
                        parent.showSensors(sensors, LangManager.getLocaleString(this.title));
                    </handler>
                    <synaptooltip name="tt" prompt="${parent.title}"/>
                </skinnedicon>
                <skinnedicon name="OtherIcon" resourceName="IMG_EFFICIENCY_RESOURCE" currentResourceNum="1" stretches="both">
                    <handler name="onclick">
                         classroot.ThermometersView.setAttribute("visible", false);
                        classroot.GaugesContainer.setAttribute("visible", !classroot.GaugesContainer.visible)
                    </handler>
                    <synaptooltip name="tt" prompt="tooltip/other"/>
                </skinnedicon>
            </view>
    
            <view name="ChartIconContainer">
                <skinnedicon name="ChartIcon" resourceName="IMG_CHART_ICON_RESOURCE" stretches="both">
                    <handler name="onclick">
                        var type = classroot["type"];
                        var id = classroot["objId"];
                        var name = classroot["objName"];
                        LvCommonChartDialog.create({sourcetype: "datapointer", objId: id, objType: type, objName: name, sourceDatapointer: classroot.getChartsDp(), startTimeShiftMs: 2*60*60*1000, viewPreferenceName: "floorplanChartPref"});
                        classroot.close();
                    </handler>                    
                    <synaptooltip name="tt" prompt="tooltip/historical_graph"/>
                </skinnedicon>
            </view>
        </view>
        
        <view width="${immediateparent.width}" height="1" bgcolor="${parent.style.basecolor}"
              visible="${classroot.ThermometersView.visible || classroot.GaugesContainer.visible}"/>
        
        <view name="ThermometersView" width="170" align="center" visible="false">
            <attribute name="title" type="string" value=""/>
            
            <handler name="onvisible" args="v">
                if (!v) {
                    this.setAttribute("title", "");
                }
            </handler>
            
            <simplelayout axis="y" spacing="0"/>
            
            <drawview width="${parent.width}" height="20">
                <attribute name="titleColor" value="${classroot.style.textcolor}" />
                <attribute name="titleBackgroundColor" value="${classroot.style.basecolor}" />
                <attribute name="titleBackgroundStrokeColor" value="0xADADB6" />

                <method name="redraw">
                    this.beginPath();
                    this.rect(0, 0, this.width-1, this.height-1, 6);
                    this.strokeStyle = this.titleBackgroundStrokeColor; //0xADADB6;
                    this.stroke()
                    this.fillStyle = this.titleBackgroundColor; //0x2B3F63;
                    this.fill()
                </method>
                
                <handler name="onvisible" reference="parent">
                    if (parent.visible) {
                        //this.redraw();
                    } else {
                        parent.ts.setAttribute("text", "");
                    }
                </handler>
                
                <text text="${parent.parent.title}" fontsize="11" fontstyle="bold" fgcolor="${parent.titleColor}"  width="${parent.width}" multiline="true"/>
            </drawview>
            
            <view name="thermometerContainer" align="center" height="80">
                <dataset name="thermometerData"></dataset>

                <simplelayout axis="x" spacing="5"/>
                <thermometer datapath="local:parent.thermometerData:/thermometers/*" height="80" width="35" showranges="true">
                    <handler name="ondata" args="d">
                    <![CDATA[
                        if (!d) {
                            return;
                        }
                        var value = this.datapath.xpathQuery("lastValue/@v");
                        var ts;
                        var lt;
                        if (value) { //sensor
                            ts = this.datapath.xpathQuery("lastValue/@t");
                            lt = this.datapath.xpathQuery("lastValue/@lt");
                            this.setAttribute("value", value);
                            this.setAttribute("title", getShortPropertyName("RACK", this.datapath.xpathQuery("name()")));
        
                            var max = this.datapath.xpathQuery("max/@v");
                            var min = this.datapath.xpathQuery("min/@v");
             
                            if (!min) {
                                min = 0;
                            }
                            
                            if (!max) {
                                if (value % 100 == 0) {
                                    max = value;
                                } else {
                                    max = Math.ceil(value / 100) * 100
                                }
                            }
                            
                            this.setAttribute("min", min);
                            this.setAttribute("max", max);
                            
                            var aMin = this.datapath.xpathQuery("aMin/@v");
                            var aMax = this.datapath.xpathQuery("aMax/@v");
                            var rMin = this.datapath.xpathQuery("rMin/@v");
                            var rMax = this.datapath.xpathQuery("rMax/@v");
                            
                            if (!aMin || !aMax || !rMin || !rMax) {
                                this.showranges = false;
                            } else {
                                this.setAttribute("aMin", aMin);
                                this.setAttribute("aMax", aMax);
                                this.setAttribute("rMin", rMin);
                                this.setAttribute("rMax", rMax);
                                this.showranges = true;
                            }
                        } else {//hist value
                            ts = this.datapath.xpathQuery("@t");
                            lt = this.datapath.xpathQuery("@lt");
                            value = this.datapath.xpathQuery("@v")
                            this.setAttribute("value", value);
                            this.setAttribute("title", getShortPropertyName("RACK", this.datapath.xpathQuery("name()")));
                            
                            var  min = 0;
                            var max;
                            if (value % 100 == 0) {
                                max = value;
                            } else {
                                max = Math.ceil(value / 100) * 100
                            }
                            this.setAttribute("min", min);
                            this.setAttribute("max", max);
                            
                            this.showranges = false;
                        }
                        
                        this.redraw();
                        
                        if (lt != "0") {
                            var ts1 = classroot.ThermometersView.ts.text;
                            var format = UserPreference.get("DateFormat");
                            if (ts1 == "" || DateFormatter.parse(ts, format) < DateFormatter.parse(ts1, format)) {
                                classroot.ThermometersView.ts.setAttribute("text", ts); 
                            }
                        }
                    ]]>
                    </handler>
                </thermometer>
            </view>
            <text name="ts" text="" align="center"/>
        </view>
        
        <view name="GaugesContainer" align="center" width="211" visible="false">
            <simplelayout axis="y"/>
            
            <view width="${parent.width}">
                <wrappinglayout axis="x"/>
    
                <smallsynapgauge height="70" datapath="ObjectSensorsData:/objects/RACK/links/ref" typePath="../../name()"/>
                <smallsynapgauge height="70" datapath="ObjectSensorsData:/objects/RACK/links/rh" typePath="../../name()"/>
                <smallsynapgauge height="70" datapath="ObjectSensorsData:/objects/RACK/ra[@v]" typePath="../name()"/>
                <smallsynapgauge height="70" datapath="ObjectSensorsData:/objects/RACK/bpa[@v]" typePath="../name()"/>
                <smallsynapgauge height="70" datapath="ObjectSensorsData:/objects/RACK/links/door" typePath="../../name()"/>
            </view>
            
            <text name="ts" text="" align="center"/>
        </view>
    </class>    
</library>
