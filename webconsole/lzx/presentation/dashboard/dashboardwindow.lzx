<library>
    <include href="../../dashboard/dashboarddcview.lzx"/>
    <include href="../fw/maximizablesynapdatawindow.lzx"/>
    
    <include href="dashcontroltabslider.lzx"/>
    
    <dashboardwindow id="DashboardsWindow">
        <dashcontroltabslider placement="controlform" controlRef="${immediateparent.classroot}" name="tabslider">
        </dashcontroltabslider>
        
        <method name="doClose" args="ignore=null">
            super.doClose();
            if(WindowManager.listWindows().length == 0){ // Enh#7528 - open SynapViews if no windows opened 
                SynapViews.create(null);
            }
        </method>
    </dashboardwindow>

    <class name="dashboardwindow" extends="maximizablesynapdatawindow" level="1" use_insets="true" initstage="defer"
            prompt="${dashboardName != null ? dashboardName : 'dash_mainwindow_title'}" timerUpdate="true">
              
        <attribute name="isEditMode" value="false" type="boolean"/>
        <attribute name="dashboardId" value="${this.dcv.dashboardId}"/>
        <attribute name="dashboardName" value="${this.dcv.dashboardName}"/>
        <attribute name="userId" value="${this.dcv.userId}"/>
        <attribute name="isPrivate" value="${this.dcv.isPrivate}"/>
        <attribute name="isFirstOpening" value="true" type="boolean"/>
        <attribute name="useFullWidth" value="true" type="boolean"/>
        
        <view name="dash_custom_controls" placement="custom_controls" width="90">
            <simplelayout axis="x" spacing="1"/>
            
            <basebutton onmouseover="" onmouseout="" focusable="true" doesenter="true">                
                <skinnedicon height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"
                             resourceName="DASH_MANAGE_RESOURCE" currentResourceNum="1"/>
                <handler name="onclick">
                    classroot.callManageDialog();
                </handler>
                <synaptooltip name="tt" prompt="dash_tt_manage_dashboards"/>
            </basebutton>
            
            <basebutton onmouseover="" onmouseout="" focusable="true" doesenter="true">                
                <skinnedicon height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"
                             resourceName="${classroot.isEditMode ? 'DASH_VIEW_RESOURCE' : 'DASH_EDIT_RESOURCE'}" currentResourceNum="1"
                             visible="${((classroot.dashboardId != null)&amp;&amp;(((UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS') &amp;&amp; !classroot.isPrivate) || (UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS') &amp;&amp; classroot.isPrivate)) &amp;&amp; classroot.userId == LoginInfo.userId) || UserPrivileges.isAllowed('CREATE_EDIT_DELETE_ALL_DASHBOARDS')) ? true : false}"/>                             
                <handler name="onclick">
                    classroot.changeMode(!classroot.isEditMode, true);
                </handler>
                <synaptooltip name="tt" prompt="${classroot.isEditMode ? 'dash_tt_view' : 'dash_tt_edit'}"/>
            </basebutton>
            
            <basebutton onmouseover="" onmouseout="" focusable="true" doesenter="true">                
                <skinnedicon height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"
                             resourceName="DASH_SAVE_RESOURCE" currentResourceNum="1"
                             visible="${classroot.isEditMode &amp;&amp; (((classroot.dashboardId != null)&amp;&amp;(((UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS') &amp;&amp; !classroot.isPrivate) || (UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS') &amp;&amp; classroot.isPrivate)) &amp;&amp; classroot.userId == LoginInfo.userId) || UserPrivileges.isAllowed('CREATE_EDIT_DELETE_ALL_DASHBOARDS')) ? true : false)}"/>                             
                <handler name="onclick">
                    classroot.saveDashboardStructure(true);
                </handler>
                <synaptooltip name="tt" prompt="dash_tt_save"/>
            </basebutton>
            
            <basebutton onmouseover="" onmouseout="" focusable="true" doesenter="true">                
                <skinnedicon height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"
                             resourceName="DASH_SETTINGS_RESOURCE" currentResourceNum="1"
                             visible="${classroot.isEditMode &amp;&amp; (((classroot.dashboardId != null)&amp;&amp;(((UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PUBLIC_DASHBOARDS') &amp;&amp; !classroot.isPrivate) || (UserPrivileges.isAllowed('CREATE_EDIT_DELETE_OWN_PRIVATE_DASHBOARDS') &amp;&amp; classroot.isPrivate)) &amp;&amp; classroot.userId == LoginInfo.userId) || UserPrivileges.isAllowed('CREATE_EDIT_DELETE_ALL_DASHBOARDS')) ? true : false)}"/>
                <handler name="onclick">
                    classroot.callSettingsDialog();
                </handler>
                <synaptooltip name="tt" prompt="dash_tt_dash_settings"/>
            </basebutton>
            
            <basebutton onmouseover="" onmouseout="" focusable="true" doesenter="true">                
                <skinnedicon height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"
                             resourceName="DASH_PRINT_RESOURCE" currentResourceNum="1"/>
                <handler name="onclick">
                    SynapPrinter.printObject(classroot.getDashView());
                </handler>
                <synaptooltip name="tt" prompt="dash_tt_print_dashboard"/>
            </basebutton>
            <!--<view name="viewForSpacing" width="10">
            </view>-->
        </view>
        
        <dashboarddcview name="dcv" width="${immediateparent.width}" height="${immediateparent.height}"/>
                
        <method name="create" args="options=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            
            if (!OptTree.getSelectedDCId()) {
                InfoDialog.show("message/system_is_not_configured");
                return;
            }
            
            this.unloadDashboard();
            
            this.open();
            
            var dashIdToOpen = UserPreference.get("lastShownDashboardId");
            if(options != null &amp;&amp; options.dashId != null){
                dashIdToOpen = options.dashId;
            }
            
            if(dashIdToOpen != null &amp;&amp; dashIdToOpen != ""){
                this.loadDashboard(dashIdToOpen);
            }else{
                this.callManageDialog();
            }
            
            MainActionsListener.setInitActionsCompleted();
        </method>
        
        <handler name="alertUpdated" reference="canvas">
            if(this.visible){
                this.refresh(true);
            }
        </handler>
        
        <method name="callManageDialog">
            DashboardListDialog.create({dashObjRef: this});
        </method>
        
        <method name="callSettingsDialog">
            AddDashboardDialog.create({dashboardId: this.dashboardId, dashObjRef: this});
        </method>
        
        <method name="changeMode" args="isChangeToEdit, saveStructure=false">
            if(!isChangeToEdit &amp;&amp; saveStructure){
                this.saveDashboardStructure(true);
            }
            this.setAttribute("isEditMode", isChangeToEdit);
            this.getDashView().changeMode(isChangeToEdit);
        </method>
        
        <method name="saveDashboardStructure" args="visibleSave=false">
            this.getDashView().saveDashboardStructure(visibleSave);
        </method>
        
        <method name="loadDashboard" args="dashboard_id, dashboardJsonStr=null, dashboard_name=null, user_id=null">
            UserPreference.set("lastShownDashboardId", dashboard_id);
            this.changeMode(false); // set "view" mode
            this.getDashView().loadDashboard(dashboard_id, dashboardJsonStr, dashboard_name, user_id);
        </method>
        
        <method name="unloadDashboard">
            this.dcv.unloadDashboard();
            this.changeMode(false);
        </method>
        
        <method name="setDashboardName" args="dash_name">
            this.getDashView().setDashboardName(dash_name);
        </method>
        
        <method name="getParamsForTemplate">
            return this.getDashView().getParamsForTemplate();
        </method>
        
        <method name="refresh" args="visibleRefresh=false">
            dcv.refresh(visibleRefresh);
        </method>
        
        <method name="getDashView">
            return this.dcv;
        </method>
        
        <method name="doClose" args="save=true">
            if (save) {
                this.saveDashboardStructure(true);
            }
            super.close();
        </method>
        
        <!-- override -->
        <method name="close" args="...ignore">
            if(this.isEditMode){
                ConfirmDialog.show("dash_warning_on_closing", this, "doClose");
            }else{
                doClose(false);
            }
        </method>
        
        <!-- override -->
        <method name="gainFocus" args="needRefresh">
            super.gainFocus(needRefresh);
            dcv.getField().gainFocus();
        </method>
        
        <!-- override -->
        <method name="loseFocus">
            dcv.getField().loseFocus();
            super.loseFocus();
        </method>
        
        <!-- override -->
        <method name="_onSize" args="a=null">
            if (!this.useFullWidth) {
                super._onSize();
                return;
            }
            
            if (GlobalWindowState.maximized) {
                this.setAttribute('width', MainWindow.width);
                this.setAttribute('height', MainWindow.height);
                this.setAttribute('x', MainWindow.x);
                this.setAttribute('y', MainWindow.y);
            } else {
                //super._onSize();
                this.setAttribute('width', MainWindow.width);
                this.setAttribute('height', ContainerView.height);
                this.setAttribute('x', TreeContainer.getAttributeRelative('x', canvas));
                this.setAttribute('y', TreeContainer.getAttributeRelative("y", canvas));
            }
        </method>
        
        <!-- override -->
        <method name="updateSkinGeometry">
            super.updateSkinGeometry();
            this.setAttribute("inset_right", 7);
            this.setAttribute("inset_left", 10);
        </method>        
    </class>
</library>