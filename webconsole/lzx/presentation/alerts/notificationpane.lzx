<library>
<include href="../../components/synapedittext.lzx"/>
<include href="../../components/synapbutton.lzx"/>
<include href="../../components/synaplist.lzx"/>
<include href="../../components/synaptext.lzx"/>
<include href="../../utils/validators/datavalidator.lzx"/>

<include href="utils.lzx"/>
<include href="basealertdefinitionpane.lzx"/>
<include href="synapdatacombobox.lzx"/>
<include href="synapscrolledittextex.lzx"/>
<include href="synapedittextex.lzx"/>

    <class name="notificationpane" extends="basealertdefinitionpane" prompt="enhalerting/tabnotification">
        <attribute name="edtxoffset" type="number" value="200"/>
        <attribute name="maxsubjlen" type="number" value="100"/>
        <attribute name="maxbodylen" type="number" value="2000"/>
        <attribute name="subjenabled" type="boolean" value="true"/>
        <attribute name="bodyenabled" type="boolean" value="true"/>
        <attribute name="notificationtemplates"/>
        <attribute name="lastfocusctrl"/>
        <attribute name="addmacroenabled" type="boolean" value="false"/>

        <synapdataset name="AlertNotificationTemplatesData" src="getAlertNotificationTemplates"/>
        
        <handler name="oninit">
            initNotificationTemplates();
        </handler>
        
        <method name="initNotificationTemplates">
            notificationtemplates = [
            
            {type:"1", id: -1, templateType: AlertTempateType.CUSTOM, header:"", body:"", hasheader:true, maxheaderlen:100},
            {type:"2", id: -1, templateType: AlertTempateType.CUSTOM, header:"", body:"", hasheader:true, maxheaderlen:100},
            {type:"3", id: -1, templateType: AlertTempateType.CUSTOM, header:"", body:"", hasheader:false, maxheaderlen:0},
            {type:"4", id: -1, templateType: AlertTempateType.CUSTOM, header:"", body:"", hasheader:false, maxheaderlen:0, skipdestinationcheck:true},
            {type:"6", id: -1, templateType: AlertTempateType.CUSTOM, header:"", body:"", hasheader:false, maxheaderlen:0, skipdestinationcheck:true}
            
                                    ];

            var typeCombo = getTemplateTypeControl();
            while(typeCombo.getNumItems() != 0) {
                typeCombo.removeItemAt(0);
            }            
            for(var i = 0; i &lt; notificationtemplates.length; i++) {
                var template = notificationtemplates[i];
                var prompt = MessageTemplateType.getPrompt(template.type);
                new lz.synaptextlistitem(typeCombo.cblist, {prompt:prompt, value:template.type});
            }
        </method>
                
        <method name="isTemplateEmpty" args="template"> <![CDATA[
            return template.id == -1 && template.header == "" && template.body == "";
        ]]> </method>
                
                
        <method name="trackValueChange">
            var valarr = [];
            for(var i = 0; i &lt; notificationtemplates.length; i++) {
                var template = notificationtemplates[i];
                if (isTemplateEmpty(template)) {
                    continue;
                }
                if(isCustomTemplateType(template.templateType)) {
                    valarr.push(template.id);
                    valarr.push(AlertUtils.translateMacros(template.header));
                    valarr.push(AlertUtils.translateMacros(template.body));              
                } else {
                    valarr.push(template.id);
                }
            }
            setAttribute("value", valarr.join(","));
        </method>
        
        <method name="reset" args="arg=null">
            super.reset(arg);
            initNotificationTemplates();
        </method>
        
        <method name="getNotificationsObjArr"><![CDATA[
            var arr = [];
            for(var i = 0; i < notificationtemplates.length; i++) {
                var template = notificationtemplates[i];
                if (isTemplateEmpty(template)) {
                    continue;
                }
                var obj = {messagetype:template.type, type: parseInt(template.templateType)};
                if(isCustomTemplateType(template.templateType)) {
                    obj.header = AlertUtils.translateMacros(template.header);
                    obj.body = AlertUtils.translateMacros(template.body);
                }
                if (template.id != -1) {
                    obj.id = template.id;
                }
                arr.push(obj);
            }
            return arr;
        ]]> </method>    	
        
        <method name="validate">
            for(var i = 0; i &lt; notificationtemplates.length; i++) {
                var template = notificationtemplates[i];
                if(isCustomTemplateType(template.templateType)) {
                    var hasDestination = template.skipdestinationcheck==null ? refWizard.getRecipientsPane().hasDestinationOfType(template.type) : true;
                    
                    if(!refWizard.issystem &amp;&amp; //notification body can be empty for system alerts (Bug#6329)
                        hasDestination &amp;&amp; template.body.trim().length == 0) {
                        var prompt = MessageTemplateType.getPrompt(template.type);
                        var message = LangManager.getLocaleString(prompt) + " " + LangManager.getLocaleString("enhalerting/templatebodyrequired");
                        return {message:message, pane:this, control:getBodyControl()};
                    }
                }
            }
            return null;
        </method>
                    
        <handler name="onDocumentChange" reference="panedata">
            var ptr = panedata.getPointer();
            if(ptr.xpathQuery("alert_types/alert_type") != null) {
                for(var i = 0; i &lt; notificationtemplates.length; i++) {
                    var template = notificationtemplates[i];
                    var tptr = ptr.dupePointer();
                    if(tptr.setXPath("alert_types/alert_type/templates/template[@messagetype='" + template.type + "']")) {
                        template.id = tptr.xpathQuery("@id");
                        template.header = AlertUtils.translateMacros(tptr.xpathQuery("header/text()"), false);
                        template.body = AlertUtils.translateMacros(tptr.xpathQuery("body/text()"), false);
                        template.templateType = tptr.xpathQuery("@type");
                    } else {
                        template.id = -1;
                        template.header = "";
                        template.body = "";            	
                        template.templateType = AlertTempateType.CUSTOM;
                    }
                }
                setAttribute("ondataselect", true);
                getTemplateTypeControl().selectItem(MessageTemplateType.SMTP);//Email
                trackValueChange();
            }
        </handler>

        <method name="isCustomTemplateType" args="type">
            return parseInt(type) == AlertTempateType.CUSTOM;
        </method>
        
        <method name="handleTemplateTypeSelection">
            var type = getTemplateTypeControl().value;
            requestTemplates(type);
            
            var template = getTemplateByType(type);            
            setAttribute("maxsubjlen", template.maxheaderlen);
            setAttribute("subjenabled", template.hasheader);
        </method>

        <method name="requestTemplates" args="filterbytype=null">
            var params = new LzParam();
            
            if(filterbytype != null) {
                params.addValue("filterbytype", filterbytype);
            } else {
                params.addValue("filterbytype", getTemplateTypeControl().value);
            }
            params.addValue("type", "loadall");
            
            params.addValue("issystem", panedata.getPointer().xpathQuery("alert_types/alert_type/system/text()"));
            params.addValue("includecustom", true);
            
            if(refWizard.alertname != null) {
                params.addValue("alerttypename", escape(refWizard.alertname));
            }
            this.AlertNotificationTemplatesData.setQueryString(params);
            this.AlertNotificationTemplatesData.doRequest();
        </method>

        <handler name="ondata" reference="this.AlertNotificationTemplatesData">
            var cbname = getTemplateNameControl();
            while(cbname.getNumItems() != 0) {
                cbname.removeItemAt(0);
            }
            var type = getTemplateTypeControl().value;                        
            var ptr = this.AlertNotificationTemplatesData.getPointer();            
            var templates = ptr.xpathQuery("root/templates/template");
            
            var id = -1;
            if (isCustomTemplateType(getTemplateByType(type).templateType)) {
                id = getTemplateByType(type).id;
            }
            
            new lz.synaptextlistitem(cbname.cblist, {prompt:"enhalerting/cbcustomitem", value: id});
            
            if(templates != null) {
                if(!(templates instanceof Array)) templates = new Array(templates);            
                for(var i = 0; i &lt; templates.length; i++) {
                    var dp = new LzDatapointer(null);
                    dp.setPointer(templates[i]);
                    var templateType = dp.xpathQuery("@type");
                    if(!isCustomTemplateType(templateType)) {
                        var name = dp.xpathQuery("@name");
                        var id = dp.xpathQuery("@id");
                        new lz.synaptextlistitem(cbname.cblist, {text: name, value: id});
                    }
                }
            }

            var template = getTemplateByType(type);
            if(isCustomTemplateType(template.templateType)) {
                getTemplateNameControl().selectItemAt(0);//Custom name is first in list
            } else {
                getTemplateNameControl().selectItem(template.id);
            }
        </handler>

        <method name="handleTemplateNameSelection">
            var templateId = getTemplateNameControl().value;
            var templateType = getTemplateTypeById(templateId);
            var template = getTemplateByType(getTemplateTypeControl().value);
            template.id = templateId;
            template.templateType = templateType;
            var header = "";
            var body = "";
            setAttribute("subjenabled", isCustomTemplateType(templateType) &amp;&amp; template.hasheader);
            setAttribute("bodyenabled", isCustomTemplateType(templateType));
            getMacroListControl().setAttribute("enabled", isCustomTemplateType(templateType));

            if(isCustomTemplateType(templateType)) {
                header = template.hasheader ?  AlertUtils.translateMacros(template.header, false) : "";
                body = AlertUtils.translateMacros(template.body, false);
            } else {
                var ptr = AlertNotificationTemplatesData.getPointer();
                if(ptr.setXPath("root/templates/template[@id='" + templateId + "']")) {
                    header = template.hasheader ? AlertUtils.translateMacros(ptr.xpathQuery("header/text()"), false) : "";
                    body = AlertUtils.translateMacros(ptr.xpathQuery("body/text()"), false);
                } else {
                    header = "";
                    body = "";
                }
            }
            getSubjectControl().setAttribute("text", header);
            getBodyControl().setAttribute("text", body);
                
            if(isCustomTemplateType(templateType)) {
                handleSubjectText();
                handleBodyText();
            }
            trackValueChange();
        </method>
        
        <method name="getTemplateTypeById" args="id">
            var tType = AlertNotificationTemplatesData.getPointer().xpathQuery("root/templates/template[@id='" + id + "']/@type");
            return tType != null ? tType : AlertTempateType.CUSTOM;
        </method>

        <method name="handleSubjectText">
            if (!this["inited"]){
                return;
            }
            var templateType = getTemplateTypeById(getTemplateNameControl().value);
            if(isCustomTemplateType(templateType)) {
                var template = getTemplateByType(getTemplateTypeControl().value);
                template.header = getSubjectControl().text;                    
                trackValueChange();
            }
        </method>
        
        <method name="handleBodyText">
            if (!this["inited"]){
                return;
            }
            var templateType = getTemplateTypeById(getTemplateNameControl().value);
            if(isCustomTemplateType(templateType)) {
                var template = getTemplateByType(getTemplateTypeControl().value);
                template.body = getBodyControl().text;                    
                trackValueChange();
            }
        </method>
        
        <method name="getTemplateByType" args="type">
            for(var i = 0; i &lt; notificationtemplates.length; i++) {
                if(notificationtemplates[i].type == type) {
                    return notificationtemplates[i];
                }
            }
            return null;
        </method>
        
        <handler name="onfocus" reference="lz.Focus" args="focusview">
            var templateType = getTemplateTypeById(getTemplateNameControl().value);

            if(!isCustomTemplateType(templateType)) {
                setAttribute("addmacroenabled", false);
                return;
            }
            
            if(focusview == getSubjectControl().field || focusview == getBodyControl().editText.field) {
                setAttribute("lastfocusctrl", focusview);
                setAttribute("addmacroenabled", true);
            } else if(focusview != getTemplateTypeControl().interior.cbtext &amp;&amp;
                        focusview != getTemplateNameControl().interior.cbtext &amp;&amp;
                        focusview != getMacroListControl() &amp;&amp;
                        focusview != getSubjectButtonControl()) {
                setAttribute("lastfocusctrl", null);
                setAttribute("addmacroenabled", false);
            }
        </handler>
        
        <method name="handleAddMacroClick">
            if(lastfocusctrl == getBodyControl().editText.field) {
                getBodyControl().insertText(getMacroListControl().getValue());
            } else if(lastfocusctrl == getSubjectControl().field) {
                getSubjectControl().insertText(getMacroListControl().getValue());
            }
        </method>     

        <method name="getTemplateTypeControl">
            return c.cbtempltype;
        </method>
        <method name="getTemplateNameControl">
            return c.cbtemplname;
        </method>
        <method name="getSubjectControl">
            return c.macroView.edtbrdr.edt.subjectView.edtsubject;
        </method>
        <method name="getBodyControl">
            return c.macroView.edtbrdr.edt.bodyView.edtbody;
        </method>
        <method name="getMacroListControl">
            return c.macroView.macrolist;
        </method>
        <method name="getSubjectButtonControl">
            return c.macroView.btnmacrosubj;
        </method>

        <view name="c" height="${parent.height-parent.parent.bar.height}"
            width="${parent.width-(parent.parent.inset_left+parent.parent.inset_right)}">
            
            <resizelayout axis="y" spacing="15"/>
            
            <synapdatacombobox name="cbtempltype" x="${classroot.edtxoffset}" width="120">
                <handler name="onselect">
                    classroot.handleTemplateTypeSelection();
                </handler>
            </synapdatacombobox>
            
            <synapdatacombobox name="cbtemplname" x="${classroot.edtxoffset}" width="120" shownitems="7">
                <handler name="onselect">
                    classroot.handleTemplateNameSelection();
                </handler>
            </synapdatacombobox>
            
            <synaptext prompt="enhalerting/notificationtype" y="${classroot.c.cbtempltype.y}" options="ignorelayout"/>
            <synaptext prompt="enhalerting/usemsgtemplate" y="${classroot.c.cbtemplname.y}" options="ignorelayout"/>
            
            <view height="10"/>
            
            <synaptext name="lblmacro" prompt="enhalerting/lblmacros" options="ignorelayout" y="${parent.macroView.y-this.height}"/>
            <view name="macroView" options="releasetolayout" width="100%">
                <resizelayout axis="x" spacing="20"/>
                <synaplist name="macrolist" width="180" height="${immediateparent.height}" oninit="selectItemAt(0)" ignoreform="true">
                    <synaptextlistitem datapath="EnhAlertMacro:/items/item"
                                       prompt="$path{'@prompt'}">
                        <handler name="oninit">
                            for(var i = 0; i &lt; subnodes.length; i++) {
                                if((subnodes[i] instanceof lz.tooltip) &amp;&amp; subnodes[i]["name"] != "tt") {
                                    subnodes[i].destroy();
                                    break;
                                }
                            }
                        </handler>
                        <synaptooltip name="tt" prompt="$path{'@ttprompt'}"/>
                    </synaptextlistitem>
                </synaplist>
                
                <synapbutton name="btnmacrosubj" text=">" y="${parent.height/2-this.height/2}" width="40"
                             enabled="${classroot.addmacroenabled}">                        		
                    <handler name="onclick">
                        classroot.handleAddMacroClick();
                    </handler>
                </synapbutton>
                            
                <view name="edtbrdr" height="${immediateparent.height}" options="releasetolayout" bgcolor="black">
                    <attribute name="bordersize" type="number" value="1"/>
                    <view name="edt" x="${parent.bordersize}" y="${parent.bordersize}" bgcolor="white"
                        width="${immediateparent.width-parent.bordersize*2}" height="${immediateparent.height-parent.bordersize*2}">
                        <attribute name="winset" type="number" value="8"/>
                        <attribute name="edtxoffset" type="number" value="0"/>
                        <resizelayout axis="y"/>
                        
                        <view name="subjectView" width="${immediateparent.width-parent.winset*2}" x="${parent.winset}">
                            <simplelayout axis="y"/>
                            <synaptext x="${parent.parent.edtxoffset}" prompt="enhalerting/lblsubject"/>
                            <synapedittextex name="edtsubject" x="${parent.parent.edtxoffset}" width="${immediateparent.width-x}"
                                             visibility="visible"
                                             maxlength="${classroot.maxsubjlen}" enabled="${classroot.subjenabled}">
                                <handler name="ontext">
                                    classroot.handleSubjectText();
                                </handler>
                            </synapedittextex>
                        </view>
                        <view height="20"/>
                        
                        <view name="bodyView" width="${immediateparent.width-parent.winset*2}" x="${parent.winset}" options="releasetolayout">
                            <resizelayout axis="y"/>
                            <synaptext x="${parent.parent.edtxoffset}" prompt="enhalerting/lblnotificationbody"/>
                            <synapscrolledittextex name="edtbody" x="${parent.parent.edtxoffset}" width="${immediateparent.width-x}"
                                                   visibility="visible" options="releasetolayout"
                                                   maxlength="${classroot.maxbodylen}" enabled="${classroot.bodyenabled}">
                                <handler name="ontext">
                                    classroot.handleBodyText();
                                </handler>                            
                            </synapscrolledittextex>
                        </view>
                        
                        <view height="${parent.subjectView.edtsubject.y - 3}"/>
                    </view>
                </view>                            
            </view>
        </view>
    </class>
</library>