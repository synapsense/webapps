<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     29.07.2010 12:49:14                                                        
     vlebedev                                                                
     ====================================================================== -->
<library>
    <include href="../../../components/synapgrid/synapresizegrid.lzx"/>
    <include href="../../../components/secured/securedbutton.lzx"/>
    <include href="../../../components/synapradiogroup.lzx"/>
    <include href="../../../components/synapradiobutton.lzx"/>
    <include href="../../../components/secured/securedcombobox.lzx"/>
    
    <class name="utilitycostsgridrow" initstage="normal" extends="synapgridrow">
        <attribute name="focusable" value="false"/>
    </class>

    <class name="utilitycostsform" extends="dctabview">
        <dataset name="TimeOfUseTable"/>
        <attribute name="isLoaded" type="boolean" value="false"/>
        <synapdataset name="GetCurrency" type="http" src="getLocales" request="false">
            <handler name="ondata">
                if(!classroot.isLoaded) {
                    classroot.setAttribute('isLoaded', true);
                }
            </handler>
        </synapdataset>
        
        <synapdataset name="UtilityCostsData"/>
        
        <handler name="onselected" reference="immediateparent.tab">
            if (immediateparent.tab.selected &amp;&amp; !this.isLoaded) {
                var params = new LzParam();
                params.addValue("mode", "currency");
                this.GetCurrency.setQueryString(params);
                this.GetCurrency.doReq(null);
            } else if(immediateparent.tab.selected &amp;&amp; this.isLoaded) {
                this.context.currencyView.curencyCombobox.setCurrency();
            }
        </handler>
        
        <method name="getObjFromLZDE_tu" args="lzDataEl"> <![CDATA[
            var resultObj = [];
            if (!lzDataEl || !lzDataEl.hasChildNodes()) {
                return resultObj;
            }
            var tframes = lzDataEl.getElementsByTagName("timeframe");
            for (var i=0;i<tframes.length;i++) {
                var bmVal = tframes[i].getElementsByTagName("beginmonth")[0].getAttr("value");
                var emVal = tframes[i].getElementsByTagName("endmonth")[0].getAttr("value");
                var tdVal = tframes[i].getElementsByTagName("typeofday")[0].getAttr("value");
                var tlframes = tframes[i].getElementsByTagName("timeline")[0].getElementsByTagName("timeframe");
                var frameObj = {beginmonth:bmVal,endmonth:emVal,typeofday:tdVal,timeline:[]};
                for (var j=0;j<tlframes.length;j++) {
                    frameObj.timeline.push({begin:tlframes[j].getAttr("begin"),
                                            end:tlframes[j].getAttr("end"),
                                            period:tlframes[j].getAttr("period"),
                                            cost:NumberFormat.parse(tlframes[j].getAttr("cost")).toString()});
                }
                resultObj.push(frameObj);
            }
            return resultObj;
        ]]> </method>
        
        <!-- Method to parse JSON object has a look 
                [{beginmonth:(jan,...),endmonth:(dec,...),typeofday:(weekdays,weekends,all),
                  timeline:[{begin:00:00:00(AM,PM),end:00:00:00(AM,PM),period:(on_peak,mid_peak,off_peak),cost:0.00},...]},...]
             to lz.DataElement
             @jsonObj is a String represent a array of JSON objects or itself array.
             Output is xml node 
             <timeofusetable>
                <timeframe>
                    <beginmonth value="..."/>
                    <endmonth value="..."/>
                    <typeofday value="..."/>
                    <timeline><timeframe begin="..." end="..." period="..." cost="..."/>...</timeline>
                </timeframe>
             </timeofusetable>
             -->
        <method name="parseJSONToLzDataElement_tu" args="jsonObj"> <![CDATA[
            //Any errors are occuring in duration of parsing is not normal
            //situation. Actualy it can't be, because we set this value.
            //Mapsense should set correct, valid value for DC property exactly like console should.
            //Messages in debug mode are used for development purposes not for normal
            //users. But again DC property 'timeOfUseTable' have to have a correct, valid value.
            var result = new lz.DataElement("timeofusetable");        	
            if (!jsonObj) {
                return result;
            } else if (jsonObj instanceof String) {
                jsonObj = JSON.parse(jsonObj);
            }
            
            var sort = function(a, b){
                var dp = months.getPointer();
                var one = parseInt(dp.xpathQuery("month[@value='" +  a.beginmonth + "']/@index"));
                var two = parseInt(dp.xpathQuery("month[@value='" +  b.beginmonth + "']/@index"));
                return one-two;
            };
            jsonObj.sort(sort);
            
            for (var i=0;i<jsonObj.length;i++) {
                var timeframe = jsonObj[i];
                if (!timeframe) {
                    Debug.error("Parsing problem: timeframe is empty");
                    return result;
                }        		
                var timeframeLDE = new lz.DataElement("timeframe",null,
                    [new lz.DataElement("beginmonth",{value:timeframe.beginmonth}),
                     new lz.DataElement("endmonth",{value:timeframe.endmonth}),
                     new lz.DataElement("typeofday",{value:timeframe.typeofday})]);
                var timelineLDE = new lz.DataElement("timeline");
                if (!timeframe.timeline) {
                    Debug.error("Parsing problem: timeline of timeframe is empty");
                    return result;
                }
                for (var j=0;j<timeframe.timeline.length;j++) {
                    var hourtimeframe = timeframe.timeline[j];
                    if (!hourtimeframe) {
                        Debug.error("Parsing problem: hourtimeframe is empty");
                        return result;
                    }
                    timelineLDE.appendChild(new lz.DataElement("timeframe",
                        {begin:hourtimeframe.begin,end:hourtimeframe.end,
                         period:hourtimeframe.period,cost: NumberFormat.format(Number(hourtimeframe.cost), 2)}));
                }        		
                timeframeLDE.appendChild(timelineLDE);
                result.appendChild(timeframeLDE);
            }
            return result;
        ]]> </method>
        
        <method name="parseJSONToLzDataElement_tiered" args="jsonObj"> <![CDATA[
        ]]> </method>
        
        <method name="loadData" args="ds"> <![CDATA[
            this.context.currencyView.curencyCombobox.setCurrency();
            
            this.UtilityCostsData.setAttribute("data", []);
            var dp = this.UtilityCostsData.getPointer();
            var dp1 = ds.getPointer();
            //add TimeOfUseNode
            var timeOfUseJSONStr = dp1.xpathQuery("DCProperties:/objects/DC/timeOfUseTable/@v");
            TimeOfUseTable.setAttribute("data",parseJSONToLzDataElement_tu(timeOfUseJSONStr));
            var tierStr = dp1.xpathQuery("DCProperties:/objects/DC/tiersTable/@v");
            
            
            var tierStrArr = "";
            if(tierStr == null || tierStr == "") {
                return;
            }
            tierStrArr = tierStr.split(";");
            for (var i = 0; i < tierStrArr.length; i++) {
                var tierArr = tierStrArr[i].split(",");
                if(tierArr || tierArr.length == 2) {
                    dp.addNode("tier",null,{from: "0", to: tierArr[0], cost: NumberFormat.format(Number(tierArr[1]), 2), firstLoaded: 'true'});
                    dp.selectParent();
                }
            }
            dp.destroy();
            dp1.destroy();
        ]]> </method>
        
        <method name="clear">
            dcpropertiesGrid.setAttribute("badItem", null);
            this.context.currencyView.curencyCombobox.clear();
            this.setAttribute('isLoaded', false);
        </method>
        
        <method name="addRow" args="ignore=null">
            var res = this.validate();
            if(!res.result) {
                InfoDialog.show(res.msg, res.item, "setFocus");
                return;
            }
            
            var dp = this.UtilityCostsData.getPointer();
            if (dp.getNodeCount() &lt; 15) {
                dp.p.appendChild(new lz.DataElement("tier", {from: "0", to: "0", cost: NumberFormat.format(0.0, 2), firstLoaded: 'false'}, null));
            } else {
                InfoDialog.show("message/max_avail_tiers");
            }
            dp.destroy();
            dcpropertiesGrid.clearSelection();
            lz.Idle.callOnIdle(new LzDelegate(this, 'setFocusToAddedCell'));
        </method>
        
        <method name="setFocusToAddedCell" args="ignore">
            var obj = dcpropertiesGrid._getReplicator();
            var cell = null;
            if(obj instanceof LzReplicationManager) {
                var l = dcpropertiesGrid._getReplicator().nodes.length;
                dcpropertiesGrid._selector._ensureItemInViewByIndex(l-1);
                l = (l > 0) ? dcpropertiesGrid._getReplicator().clones.length-1 : 0;
                cell = dcpropertiesGrid._getReplicator().clones[l].subviews[1];
            } else {
                cell = dcpropertiesGrid._getReplicator().subviews[1];
            }
            dcpropertiesGrid.setAttribute("badItem", cell);
            lz.Focus.setFocus(cell);
        </method>
        
        <method name="removeRow" args="grid">
        <![CDATA[
            var selection = grid._selector.getSelection();

            if(selection instanceof Array) {
                if(!selection.length) {
                    InfoDialog.show("message/choose_row_tier_to_remove");
                    return;
                }
                var item;
                while (item = selection.shift()) {
                    item.deleteNode();
                }
            }
            grid.clearSelection();
        ]]> 
        </method>
        
        <method name="validate"> <![CDATA[
            var nodesArr = this.UtilityCostsData.childNodes;
            if(context.paymenttype.value == "tiered") {
                for(var i = 0; i<nodesArr.length; i++) {
                    var error = dcpropertiesGrid.RangeValidator.validate(nodesArr[i].attributes['to']);
                    if(error != null) {
                        dcpropertiesGrid.setAttribute("alreadyChecked", true);
                        context.paymenttype.setAttribute("value","tiered");
                        return {result: false, item: dcpropertiesGrid.badItem, msg: error};
                    }
                }
            } else if (context.paymenttype.value == "timeofuse") {
                var obj = context.jsontut.value;
                if (!obj || !obj.length) {
                    return {result: false, item: null, msg: "message/invalid_conf_12_months"};
                }
                var checksumm = {};
                for (var i=0;i<obj.length;i++) {
                    var dp = months.getPointer();
                    var startIndex = parseInt(dp.xpathQuery("month[@value='" + 
                                        obj[i].beginmonth + "']/@index"));
                    var endIndex = parseInt(dp.xpathQuery("month[@value='" + 
                                        obj[i].endmonth + "']/@index"));
                    do {
                        if (checksumm[startIndex]) {
                            if (checksumm[startIndex][obj[i].typeofday] || 
                                    checksumm[startIndex]["all"]) {
                                //We already have a record for this month index covering
                                //that type of days. 
                                context.paymenttype.setAttribute("value","timeofuse");
                                return {result: false, item: null, msg: "message/invalid_conf_12_months"};
                            }
                            checksumm[startIndex][obj[i].typeofday] = true;
                        } else {
                            checksumm[startIndex] = {};
                            checksumm[startIndex][obj[i].typeofday] = true;
                        }
                        if (startIndex == endIndex) {
                            break;
                        } else if (startIndex >= 11) {
                            startIndex = 0;
                        } else {
                            startIndex++;
                        }
                    } while (true)
                }
                var monthcounter = 0;
                for (var i in checksumm) {
                    monthcounter++;
                    if (checksumm[i]["all"] && 
                            !checksumm[i]["weekdays"] && !checksumm[i]["weekends"]) {
                    } else if (checksumm[i]["weekdays"] && checksumm[i]["weekends"]) {
                    } else {
                        context.paymenttype.setAttribute("value","timeofuse");
                        return {result: false, item: null, msg: "message/invalid_conf_12_months"};
                    }
                }
                if (monthcounter != 12) {
                    context.paymenttype.setAttribute("value","timeofuse");
                    return {result: false, item: null, msg: "message/invalid_conf_12_months"};
                }
            }
            return {result: true, item: null, msg: null};
        ]]> </method>
        
        <method name="checkColumn">
        <![CDATA[
            var nodesArr = this.UtilityCostsData.childNodes;
            for(var i = 0; i<nodesArr.length; i++) {
                nodesArr[i].attributes['firstLoaded'] = 'false';
                if(i < nodesArr.length-1 && (parseInt(nodesArr[i].attributes['to'])+1) != parseInt(nodesArr[i+1].attributes['from'])) {
                    var from = parseInt(nodesArr[i].attributes['to'])+1;
                    nodesArr[i+1].setAttr('from', ""+from);
                }
            }
        ]]>
        </method>
        
        <view name="context" width="${classroot.width-10}">
            <simplelayout axis="y" spacing="5"/>
            <!--Dummy formitem that allows to submit grid data -->
            <baseformitem submitname="tiersTable" submit="${parent.paymenttype.getValue() == 0}" name="tiersTable" visible="false" enabled="true">
                <handler name="onDocumentChange" reference="classroot.UtilityCostsData" args="obj">
                    if((obj['cobj'] &amp;&amp; obj.type == '0' &amp;&amp; obj.cobj.attributes['firstLoaded'] == 'true') || obj['what'] == 'childNodes') {
                        lz.Idle.callOnIdle(new LzDelegate(this, 'updateValIgnoreForm'));
                    } else {
                        lz.Idle.callOnIdle(new LzDelegate(this, 'updateVal'));
                    }
                </handler>
                <method name="updateValIgnoreForm" args="ignore">
                    classroot.checkColumn();
                    
                    var val = dcpropertiesGrid.convertToStr();
                    if(this.value != val) {
                        this.setValue(dcpropertiesGrid.convertToStr(), true);
                    }
                </method>
                <method name="updateVal" args="ignore">
                    classroot.checkColumn();
                    
                    var val = dcpropertiesGrid.convertToStr();
                    if(this.value != val) {
                        this.setValue(dcpropertiesGrid.convertToStr());
                    }
                </method>
                <method name="commit">
                    if(parent.paymenttype.getValue() == 0)
                        super.commit();
                </method>
            </baseformitem>
            <view name="currencyView">
                <simplelayout axis="x" spacing="10"/>
                <synaptext height="26" prompt="dc_properties/ID_CURRENCY_TEXT" valign="middle"/>
                <securedcombobox name="curencyCombobox" submitname="currency" width="100" height="26"
                               editable="false" shownitems="10" dataoption="pooling"
                               privileges="EDIT_DATA_CENTER_PROPERTIES_GENERAL">
                    <attribute name="defaultCurrency" type="string" value=""/>
                    
                    <method name="setDefaultValue" args="ignore">
                        if(!this.getValue() &amp;&amp; this.defaultCurrency != "") {
                            this.setValue(this.defaultCurrency, true);
                        }
                    </method>
                    
                    <method name="clear">
                        this.clearSelection();
                        this.setValue(null, true);
                        this.setAttribute("defaultCurrency", "");
                    </method>
                    
                    <method name="setCurrency">
                        var currency = DCProperties.getPointer().xpathQuery("DCProperties:/objects/DC/currency/@v");
                        
                        if(currency != null &amp;&amp; currency != "") {
                            this.selectItem(currency);
                            this.setValue(currency, true);
                        } else {
                            this.setDefaultValue(null);
                        }
                    </method>
                    
                    <synaptextlistitem datapath="local:parent.parent.classroot.GetCurrency:/currencies/currency">
                        <handler name="ondata">
                            this.setAttribute("text", this.datapath.xpathQuery("name/text()") + " (" + this.datapath.xpathQuery("symbol/text()") + ")");
                            this.setAttribute("value", this.datapath.xpathQuery("name/text()"));
                            var currency = DCProperties.getPointer().xpathQuery("DCProperties:/objects/DC/currency/@v");
                            
                            if(currency != null &amp;&amp; this.value == currency) {
                                parent.selectItem(this.value);
                                parent.setValue(this.value, true);
                            }
                            if(this.datapath.xpathQuery("isCurrent/text()") == "true") {
                                parent.setAttribute("defaultCurrency", this.value);
                                lz.Idle.callOnIdle(new LzDelegate(parent, 'setDefaultValue'));
                            }
                        </handler>
                    </synaptextlistitem>
                </securedcombobox>
            </view>
            <synapradiogroup name="paymenttype" value="'tiered'"
                             layout="class: simplelayout; axis: x; spacing:5" submitname="costType" 
                             visible="true" enabled="${UserPrivileges.isAllowed('EDIT_DATA_CENTER_PROPERTIES_GENERAL')}">
                <attribute name="type" value="$path{'DCProperties:/objects/DC/costType/@v'}"/>
                <handler name="onchanged" args="v">
                    dcpropertiesGrid.clearSelection();
                    resizeGrid.clearSelection();
                </handler>
                <handler name="ontype"> <![CDATA[
                    if (type == "1") {
                        setAttribute("value","timeofuse");
                    } else {
                        setAttribute("value","tiered");
                    }
                ]]> </handler>
                <method name="setChanged" args="changed,skipform=null"><![CDATA[
                    if(parent['tiersTable'] && parent['jsontut']) {
                        parent.tiersTable.setAttribute("ignoreform", this.value == 'timeofuse');
                        parent.jsontut.setAttribute("ignoreform", this.value == 'tiered');
                    }
                    super.setChanged(changed,skipform);
                ]]>
                </method>
                <synapradiobutton prompt="text/tiered" value="'tiered'"/>
                <synapradiobutton prompt="text/time_of_use" value="'timeofuse'"/>
                <method name="getValue"> <![CDATA[
                    if (value=="tiered") {
                        return 0;
                    } else if (value=="timeofuse") {
                        return 1;
                    } else {
                        return 2;
                    }
                ]]> </method>
            </synapradiogroup>
            <view name="tiered" visible="${parent.paymenttype.value=='tiered'}">
                <consolegrid id="dcpropertiesGrid" width="${classroot.width - 10}" height="200" 
                           datapath="local:classroot.UtilityCostsData:/" contentdatapath="tier" selectable="true">
                    <attribute name="_rowclass" value="lz.utilitycostsgridrow" when="once"/>
                    <attribute name="badItem"/>
                    <attribute name="alreadyChecked" type="boolean" value="false"/>
                    
                    <node name="RangeValidator">
                        <method name="validate" args="val">
                            if (!this.textValidator) {
                                this.textValidator = new lz.notemptyvalidator(this);
                            }
                            if (!this.intValidator) {
                                this.intValidator = new lz.integervalidator(this);
                            }
                            
                            var result = this.textValidator.validate(val);
                            if (result) {
                                return result;
                            }
                            
                            result = this.intValidator.validate(val);
                            if (result) {
                                return result;
                            }
                            
                            result = this.compareRowCells(val);
                            if (result) {
                                return result;
                            }
                            
                            return null;
                        </method>
                        
                        <method name="compareRowCells" args="val">
                        <![CDATA[
                            var nodesArr = classroot.UtilityCostsData.childNodes;
                            for(var i = 0; i<nodesArr.length; i++) {
                                if(parseInt(val) == parseInt(nodesArr[i].attributes['to']) && parseInt(val) < parseInt(nodesArr[i].attributes['from'])) {
                                    return "dc_properties/id_value_to_must_be_greater_than_or_equal_value_from_message";
                                }
                            }
                            return null;
                        ]]>
                        </method>
                    </node>
                    
                    <handler name="onvalidationerror" args="e">
                        this.setAttribute("badItem", e.cell);
                        if(!dcpropertiesGrid.alreadyChecked) {
                            InfoDialog.show(e.error, e.cell, "setFocus");
                        }
                        this.setAttribute("alreadyChecked", false);
                    </handler>
                    
                    <handler name="onselect" args="s">
                        var isSelected = (s != null ? true : false);
                        classroot.context.manageButtons.remove.setAttribute('enabled', isSelected);
                    </handler>
                    
                    <method name="setFocus">
                        lz.Focus.setFocus(this);
                    </method>
                            
                    <method name="convertToStr">
                        <![CDATA[ 
                        var resStr = "";
                        var nodes = this.datapath.p.childNodes;
                        
                        for(var i=0; i<nodes.length; i++) {
                            resStr += nodes[i].attributes['to'] + ","
                                   +  NumberFormat.parse(nodes[i].attributes['cost']).toString();
                            if(i < nodes.length-1) {
                                resStr += ";"
                            }
                        }
                        
                        return resStr;
                        ]]>
                    </method>
                    
                    <synapgridtext prompt="dc_properties/ID_FROM_COLUMNNAME" ignoreform="true" width="100" datapath="@from" editable="false"/>
                    <synapgridtext prompt="dc_properties/ID_TO_COLUMNNAME" ignoreform="true" width="100" datapath="@to" maxlength="7"
                                   validator="$once{parent.RangeValidator}" type="Integer"
                                   editable="${UserPrivileges.isAllowed('EDIT_DATA_CENTER_PROPERTIES_GENERAL')}"/>
                    <synapgridtext prompt="dc_properties/ID_COST_COLUMNNAME" ignoreform="true" width="100" datapath="@cost" maxlength="6" 
                                   validator="$once{DoubleValidator}" type="Double"
                                   editable="${UserPrivileges.isAllowed('EDIT_DATA_CENTER_PROPERTIES_GENERAL')}"/>
                    
                </consolegrid>
            </view>
            <baseformitem name="jsontut" submitname="timeOfUseTable" submit="${parent.paymenttype.getValue() == 1}">
                <handler name="onDocumentChange" reference="classroot.TimeOfUseTable">
                    var lzde = classroot.TimeOfUseTable.getPointer().xpathQuery("timeofusetable");
                    if (!lzde) {return;}
                    var obj = classroot.getObjFromLZDE_tu(lzde);
                    if (!this.value || areEqualObjs(obj, this.rollbackvalue)) {
                        this.setValue(obj,true);
                    } else {
                        this.setAttribute("value",obj);
                    }
                </handler>
                <method name="areEqualObjs" args="obj1,obj2"> <![CDATA[
                    if (obj1 == obj2) {return true;}
                    if ((obj1 && !obj2) || (!obj1 && obj2)) {return false;}
                    if (obj1.length != obj2.length) {return false;}
                    for (var i=0;i<obj1.length;i++) {
                        if (obj1[i].beginmonth!=obj2[i].beginmonth ||
                                obj1[i].endmonth!=obj2[i].endmonth ||
                                obj1[i].typeofday!=obj2[i].typeofday ||
                                obj1[i].timeline.length != obj2[i].timeline.length) {
                            return false;
                        }
                        for (var j=0;j<obj1[i].timeline.length;j++) {
                            if (obj1[i].timeline[j].begin!=obj2[i].timeline[j].begin ||
                                    obj1[i].timeline[j].end!=obj2[i].timeline[j].end ||
                                    obj1[i].timeline[j].period!=obj2[i].timeline[j].period ||
                                    obj1[i].timeline[j].cost!=obj2[i].timeline[j].cost) {
                                return false;
                            }
                        }
                    }
                    return true;
                ]]> </method>
                <method name="setValue" args="v,isinitvalue=null"> <![CDATA[
                    var didchange = !areEqualObjs(this.value,v);
                    this.value = v;
                    
                    if (isinitvalue || ! this._initcomplete) {
                        this.rollbackvalue = v;
                    }
                    this.setChanged(didchange && !isinitvalue && !areEqualObjs(this.rollbackvalue,v));
                    if (this['onvalue']) this.onvalue.sendEvent(v);
                ]]> </method>
                <method name="rollback">
                    if (!areEqualObjs(this.rollbackvalue,this['value'])) {
                        this.setAttribute('value', this.rollbackvalue);
                    }
                    this.setAttribute('changed', false);
                </method>
                <method name="commit">
                    if(parent.paymenttype.getValue() == 1)
                        super.commit();
                </method>
            </baseformitem>
            <view name="timeofuse" visible="${parent.paymenttype.value=='timeofuse'}">
                <synapresizegrid id="resizeGrid" width="${classroot.width - 10}" height="200" 
                           datapath="local:classroot.TimeOfUseTable:/" twolineheader="true"
                           contentdatapath="timeofusetable/timeframe" selectable="true">
                    <handler name="onselect" args="s">
                        var isSelected = (s != null ? true : false);
                        classroot.context.manageButtons.remove.setAttribute('enabled', isSelected);
                    </handler>
                    <synapgridcolumn width="100" prompt="dc_properties/ID_DAY_OF_THE_WEEK_COLUMNNAME" allowAutoResize="false">
                        <view width="${immediateparent.width}" height="${immediateparent.height}">
                            <handler name="onheight" reference="content"> <![CDATA[
                                var row = immediateparent.immediateparent;
                                row.setAttribute("minrowheight",content.height);
                            ]]> </handler>
                            <view name="content" align="right" valign="middle">
                                <simplelayout axis="y" spacing="2"/>
                                <view>
                                    <attribute name="beginm" type="string" value="$path{'beginmonth/@value'}"/>
                                    <attribute name="endm" type="string" value="$path{'endmonth/@value'}"/>
                                    <simplelayout axis="x" spacing="2"/>
                                    <synaptext prompt="${'timeunit/'+parent.beginm+'_abbreviation'}"/>
                                    <text text="1"/><text text="-"/>
                                    <synaptext prompt="${'timeunit/'+parent.endm+'_abbreviation'}"/>
                                    <text datapath="${'months:/month[@value=\''+parent.endm +'\']/@days'}"/>
                                </view>
                                <attribute name="typeofday" type="string" value="$path{'typeofday/@value'}"/>
                                <synaptext prompt="${'timeunit/'+parent.typeofday}"/>
                            </view>
                        </view>
                    </synapgridcolumn>
                    <synapgridcolumn width="100" prompt="dc_properties/ID_TIME_COLUMNNAME" allowAutoResize="false">
                        <view width="${immediateparent.width}" visible="true">
                          <view align="right" datapath="timeline" visible="true">
                            <simplelayout axis="y"/>
                            <view visible="${!!this.datapath.p}">
                                <datapath xpath="timeframe" pooling="true"/>
                                <simplelayout axis="x" spacing="2"/>
                                <text text="$path{'@begin'}"/>
                                <text>-</text>
                                <text text="$path{'@end'}"/>
                            </view>
                          </view>
                        </view>
                    </synapgridcolumn>
                    <synapgridcolumn width="90" prompt="dc_properties/ID_TIME_OF_USE_PERIOD_COLUMNNAME" allowAutoResize="false">
                        <view width="${immediateparent.width}" visible="true">
                          <view align="right" datapath="timeline" visible="true">
                            <simplelayout axis="y"/>
                            <view visible="${!!this.datapath.p}">
                                <datapath xpath="timeframe" pooling="true"/>
                                <attribute name="pv" type="string" value="$path{'@period'}"/>
                                <synaptext prompt="${'text/' + parent.pv}"/>
                            </view>
                          </view>
                        </view>
                    </synapgridcolumn>
                    <synapgridcolumn width="110" allowAutoResize="false">
                        <attribute name="currency" value="${classroot.context.currencyView.curencyCombobox.value}"/>
                        <handler name="oncurrency">
                            this.updateColumnTitle();
                        </handler>
                        <handler name="langchanged" args="a" reference="LangManager">
                            lz.Idle.callOnIdle(new LzDelegate(this, "updateColumnTitle"));
                        </handler>
                        
                        <method name="updateColumnTitle" args="ignore=null">
                            this.setAttribute("text", LangManager.getFormattedLocaleString("dc_properties/ID_TIME_OF_USE_PRICE_COLUMNNAME", currency));
                        </method>
                        
                        <view width="${immediateparent.width}">
                          <view align="right">
                            <simplelayout axis="y"/>
                            <text datapath="timeline/timeframe" align="right" text="$path{'@cost'}"/>
                          </view>
                        </view>
                    </synapgridcolumn>
                    <synapgridcolumn width="85" prompt="columnname/actions" filterable="false" allowAutoResize="false">
                        <view width="${immediateparent.width}" height="${immediateparent.height}">
                            <view valign="middle" align="center">
                                <simplelayout axis="x" spacing="5"/>
                                <securedbasebutton valign="middle" datapath="." focusable="true" doesenter="true" privileges="EDIT_DATA_CENTER_PROPERTIES_GENERAL">                    
                                    <synaptooltip prompt="tooltip/edit"/>
                                    <skinnedicon resourceName="FRAMES_EDIT_ICON_RESOURCE" height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" stretches="both"/>
                                    <handler name="onclick"> <![CDATA[
                                        TimeOfUseUtilityCostDlg.create({targetnode:this.datapath.p,actiontype:"edit"});
                                    ]]> </handler>
                                </securedbasebutton>
                                <securedbasebutton valign="middle" datapath="." focusable="true"  doesenter="true"
                                            onclick="this.datapath.deleteNode();" privileges="EDIT_DATA_CENTER_PROPERTIES_GENERAL">
                                    <synaptooltip prompt="tooltip/delete"/>
                                    <skinnedicon resourceName="FRAMES_DELETE_ICON_RESOURCE" 
                                                 height="16" 
                                                 width="${this.height / this.unstretchedheight * this.unstretchedwidth}" 
                                                 stretches="both"/>
                                </securedbasebutton>
                            </view>
                        </view>
                    </synapgridcolumn>
                </synapresizegrid>
            </view>
            <view name="manageButtons" align="right">
                <simplelayout axis="x" spacing="10"/>
                <securedsynapbutton name="addNew" prompt="dc_properties/ID_NEW_BUTTON" enablingType="visibility" privileges="EDIT_DATA_CENTER_PROPERTIES_GENERAL">
                    <handler name="onclick">
                        if (classroot.context.paymenttype.value == "timeofuse") {
                            var tn = classroot.TimeOfUseTable.getPointer().xpathQuery("timeofusetable");
                            TimeOfUseUtilityCostDlg.create({targetnode:tn,actiontype:"add"});
                        } else {classroot.addRow();}
                    </handler>
                </securedsynapbutton>
                <securedsynapbutton name="remove" prompt="dc_properties/ID_REMOVE_BUTTON" enabled="false" enablingType="visibility" privileges="EDIT_DATA_CENTER_PROPERTIES_GENERAL">
                    <handler name="onclick">
                        classroot.removeRow((classroot.context.paymenttype.value == "timeofuse") ? resizeGrid : dcpropertiesGrid);
                    </handler>
                </securedsynapbutton>
            </view>
        </view>
    </class>
</library>
