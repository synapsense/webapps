<?xml version="1.0" encoding="UTF-8"?>
<!-- ====================================================================== 
     16.03.2011 12:19:30                                                        
     aklushin                                                                
     ====================================================================== -->
<library>
    <include href="../../windowmanagement/managedsynapmodaldialog.lzx" />
    <include href="../../components/datetimepickers/synaptimepickerpanel.lzx"/>
    <include href="../../components/datetimepickers/monthpicker.lzx"/>
    <include href="../../components/datetimepickers/typeofdaypicker.lzx"/>
    <include href="../../components/synapform.lzx"/>
    <include href="../../components/datacontainer.lzx"/>
    <include href="../../messages/confirmdialog.lzx"/>
    <include href="../../utils/validators/notemptyvalidator.lzx"/>
    <include href="../../utils/timeparser.lzx"/>
    
    <dataset name="timeofuseperiods">
        <timeofuseperiod value="on_peak" prompt="text/on_peak"/>
        <timeofuseperiod value="mid_peak" prompt="text/mid_peak"/>
        <timeofuseperiod value="off_peak" prompt="text/off_peak"/>
    </dataset>
    
    <class name="timeofuseperiodpicker" extends="synapcombobox">
        <synaptextlistitem datapath="timeofuseperiods:/timeofuseperiod" 
                           value="$path{'@value'}" prompt="$path{'@prompt'}"
                           ondata="if (classroot.value == value) {setAttribute('selected',true);}">
        </synaptextlistitem>
    </class>
    
    <!--We don't know when a value, has been set to
        the parent component, is a value from a dataset.
        Therefore to sync a parent's value with a dataset
        we should send a value back to a dataset every time
        when a value has been changed. But component may have
        a default value or something what can corrupt a dataset.
        So we sync data only after all 'ondata' events or default 
        value settings. This is why we use lz.Timer as a 
        synchronization mechanism.-->
    <class name="__synchronizer" extends="node">
        <attribute name="_syncDel" type="expression" value="$once{new lz.Delegate(this, '_sync')}"/>
        
        <handler name="onvalue" args="v" reference="parent"> <![CDATA[
            if (parent.inited) {
                lz.Timer.resetTimer(_syncDel,1);
            }
        ]]> </handler>
        <method name="_sync" args="ignore=null">
            parent.datapath.updateData();
        </method>
    </class>
    
    <class name="cycle24hrow" extends="synapgridrow">
        <attribute name="__instancesToValidate" type="expression" value="{}" allocation="class"/>
        <attribute name="__nrow" type="number" value="0" allocation="class"/>
        <handler name="onvisible"> <![CDATA[
            if (visible && !lz.cycle24hrow.__instancesToValidate[getUID()]) {
                lz.cycle24hrow.__instancesToValidate[getUID()] = this;
                lz.cycle24hrow.__nrow++;
            } else if (lz.cycle24hrow.__instancesToValidate[getUID()]) {
                delete lz.cycle24hrow.__instancesToValidate[getUID()];
                lz.cycle24hrow.__nrow--;
            }
        ]]> </handler>
        <handler name="oninit"> <![CDATA[
            if (visible && !lz.cycle24hrow.__instancesToValidate[getUID()]) {
                lz.cycle24hrow.__instancesToValidate[getUID()] = this;
                lz.cycle24hrow.__nrow++;
            }
        ]]> </handler>
        <method name="validate" allocation="class"> <![CDATA[
            var result = null;
            var cb = 0;
            var ce = 0; 
            if (lz.cycle24hrow.__nrow == 0) {
                return {result : false, item: null, msg: "message/invalid_24_cycle"};
            }
            for (var i in lz.cycle24hrow.__instancesToValidate) {
                var row = lz.cycle24hrow.__instancesToValidate[i];
                var begintime = row.searchSubviews("name","begintime");
                var endtime = row.searchSubviews("name","endtime");
                if (!(begintime.hours-endtime.hours)&&
                    !(begintime.minutes-endtime.minutes)&&
                    !(begintime.seconds-endtime.seconds)&&
                    lz.cycle24hrow.__nrow!=1) {
                    return {result : false, item: row, msg: "message/invalid_24_cycle"};
                }
                cb += begintime.hours*100
                        +begintime.minutes*10
                        +begintime.seconds;
                ce += endtime.hours*100
                        +endtime.minutes*10
                        +endtime.seconds;
            }
            return {result : !(cb-ce), item: null, msg: "message/invalid_24_cycle"};
        ]]> </method>
    </class>
    
    <class name="timeofuseutilitycostdialog" extends="managedsynapmodaldialog"
        width="500" height="550" prompt="title/time_of_use_utility_costs">
        <attribute name="openDel" type="expression" value="${new LzDelegate(this,'openDlg')}"/>
        <dataset name="TimeOfUseCostTable">
          <timeframe>
            <beginmonth></beginmonth>
            <endmonth></endmonth>
            <typeofday></typeofday>
            <timeline></timeline>
          </timeframe>
        </dataset>
        <view name="context" 
            width="${immediateparent.width}"
            height="${immediateparent.height}">
            <synapform name="mainForm" 
                       width="${immediateparent.width}"
                       height="${immediateparent.height}"
                       datapath="local:classroot.TimeOfUseCostTable:/timeframe">
                <method name="initValidators"> <![CDATA[
                    this.validators = [
                        "beginMonth",new lz.notemptyvalidator(this),
                        "endMonth",new lz.notemptyvalidator(this),
                        "typeOfDay",new lz.notemptyvalidator(this)
                    ];
                ]]> </method>
                
                <basesynapvalidator name="monthrangevalidator" message="message/invalid_conf_begin_end_month">
                    <method name="validate" args="ignore"> <![CDATA[
                        var dp = months.getPointer();
                        var startIndex = parseInt(dp.xpathQuery("month[@value='" + 
                                        parent.beginmcont.mp.value + "']/@index"));
                        var endIndex = parseInt(dp.xpathQuery("month[@value='" + 
                                        parent.endmcont.mp.value + "']/@index"));
                        var result = null;
                        if (startIndex > endIndex) {
                            result = message;
                        }
                        dp.destroy();
                        return result;
                    ]]> </method>
                </basesynapvalidator>
                
                <method name="validate" args="changedOnly=true"> <![CDATA[
                    var result = super.validate(false);
                    if (result.result) {
                        return lz.cycle24hrow.validate();
                    } else {
                        return result;
                    }
                ]]> </method>
                
                <simplelayout axis="y" spacing="1"/>
                
                <datacontainer name="beginmcont" 
                    titleprompt="text/begin_month"
                    titlewidth="150">
                    <!--don't use $path{}...-->
                    <monthpicker name="mp" width="150" datapath="beginmonth" 
                                 oninit="dataBindAttribute('value','@value','string');" submitname="beginMonth"/>
                </datacontainer>
                <datacontainer name="endmcont"
                    titleprompt="text/end_month"
                    titlewidth="150">
                    <!--don't use $path{}...-->
                    <monthpicker name="mp" width="150" datapath="endmonth"
                                 oninit="dataBindAttribute('value','@value','string');" submitname="endMonth"/>
                </datacontainer>
                <datacontainer name="typeofdaycont" yoffset="-5"
                    titleprompt="text/type_of_day"
                    titlewidth="150">
                    <!--don't use $path{}...-->
                    <typeofdaypicker name="dp" width="150" datapath="typeofday"
                                     oninit="dataBindAttribute('value','@value','string');" submitname="typeOfDay"/>
                </datacontainer>
                
                <synapgrid name="costgrid" yoffset="-10" width="${immediateparent.width}" height="300" _rowclass="lz.cycle24hrow"
                           contentdatapath="timeline/timeframe" selectable="true">
                    <synapgridcolumn width="100" prompt="columnname/begin_time">
                        <synaptimepickerpanel name="begintime" usecurrenttimetoinit="false" datapath="." 
                                              align="center" valign="middle" showseconds="false" restrictMinutes="true">
                            <__synchronizer/>
                            <handler name="oninited">
                                dataBindAttribute('value','@begin','string');
                            </handler>
                        </synaptimepickerpanel>
                    </synapgridcolumn>
                    <synapgridcolumn width="100" prompt="columnname/end_time">
                        <synaptimepickerpanel name="endtime" usecurrenttimetoinit="false" datapath="." 
                                              align="center" valign="middle" showseconds="false" restrictMinutes="true">
                            <__synchronizer/>
                            <handler name="oninited">
                                dataBindAttribute('value','@end','string');
                            </handler>
                        </synaptimepickerpanel>
                    </synapgridcolumn>
                    <synapgridcolumn width="150" prompt="columnname/time_of_use_period">
                        <timeofuseperiodpicker width="150" datapath=".">
                            <__synchronizer/>
                            <handler name="oninited">
                                dataBindAttribute('value','@period','string');
                            </handler>
                        </timeofuseperiodpicker>
                    </synapgridcolumn>
                    <synapgridtext prompt="columnname/time_of_use_cost" width="50" maxlength="6" textalign="right" datapath="@cost" 
                                   validator="$once{DoubleValidator}" type="Double"
                                   editable="${UserPrivileges.isAllowed('EDIT_DATA_CENTER_PROPERTIES_GENERAL')}"/>
                    <synapgridcolumn width="50" prompt="columnname/actions" filterable="false">
                        <view width="${immediateparent.width}" height="${immediateparent.height}">
                            <basebutton align="center" valign="middle" 
                                        datapath="." focusable="true"
                                        onclick="this.datapath.deleteNode();">
                                <synaptooltip prompt="tooltip/delete_time_frame"/>
                                <skinnedicon resourceName="FRAMES_DELETE_ICON_RESOURCE" 
                                             height="16" width="${this.height / this.unstretchedheight * this.unstretchedwidth}" 
                                             stretches="both"/>
                            </basebutton>
                        </view>
                    </synapgridcolumn>
                </synapgrid>
                
                <view align="right" yoffset="-20">
                    <simplelayout axis="x" spacing="5"/>
                    <synapbutton prompt="button/add" 
                                 onclick="classroot.addTimeFrameCost('00:00:00','00:00:00','on_peak',NumberFormat.format(0.0,2));"/>
                    <synapbutton prompt="button/clear"
                                 onclick="classroot.clearTimeFrames();"/>
                </view>
                
                <view align="right" yoffset="-40">
                    <simplelayout axis="x" spacing="5"/>
                    <synapbutton prompt="button/ok">
                        <handler name="onclick"> <![CDATA[
                            var result = classroot.context.mainForm.validate();
                            if (!result.result) {
                                if (result.item) {
                                    InfoDialog.show(result.msg,classroot,"setFocusTo",result.item);
                                } else {
                                    InfoDialog.show(result.msg);
                                }
                                return;
                            } else {
                                classroot.context.mainForm.commit();
                                classroot.update();
                                var node = classroot.TimeOfUseCostTable.getPointer().xpathQuery("timeframe").cloneNode(true);
                                if (classroot.actiontype == "edit") {
                                    classroot.targetnode.setAttribute("childNodes",node.childNodes);
                                } else {
                                    classroot.targetnode.appendChild(node);
                                }
                                classroot.close();
                            }
                        ]]> </handler>
                    </synapbutton>
                    <synapbutton prompt="button/cancel">
                        <handler name="onclick"> <![CDATA[
                            if (classroot.context.mainForm.changed) {
                                ConfirmDialog.show("message/are_you_sure_you_wish_to_cancel_and_lose_all_changes",classroot,"closeDlg");
                            } else {
                                classroot.close();
                            }
                        ]]> </handler>
                    </synapbutton>
                </view>
            </synapform>
        </view>
        
        <method name="setFocusTo" args="view"> <![CDATA[
            lz.Focus.setFocus(view);
        ]]> </method>
        
        <method name="update" args="ignore=null">
            var form = this.context.mainForm.datapath.updateData();
        </method>
        <method name="addTimeFrameCost" args="beginTime,endTime,timeOfUsePeriod,timeOfUseCost"> <![CDATA[
            completeInstantiation();
            var dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe/timeline");
            dp.addNode("timeframe", null, {
                        begin : beginTime.toString(),
                        end : endTime.toString(),
                        period : timeOfUsePeriod.toString(),
                        cost : timeOfUseCost.toString()
                    });
            dp.destroy();
        ]]> </method>
        <method name="clearTimeFrames"> <![CDATA[
            completeInstantiation();
            var dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe/timeline");
            dp.deleteNode();
            dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe");
            dp.addNode("timeline");
            dp.destroy();
        ]]> </method>
        <method name="setBeginMonth" args="bMonth"> <![CDATA[
            completeInstantiation();
            var dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe/beginmonth");
            dp.setNodeAttribute("value",bMonth);
            dp.destroy();
        ]]>	</method>
        <method name="setEndMonth" args="eMonth"> <![CDATA[
            completeInstantiation();
            var dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe/endmonth");
            dp.setNodeAttribute("value",eMonth);
            dp.destroy();
        ]]> </method>
        <method name="setTypeOfDay" args="type"> <![CDATA[
            completeInstantiation();
            var dp = TimeOfUseCostTable.getPointer().dupePointer();
            dp.setXPath("timeframe/typeofday");
            dp.setNodeAttribute("value",type);
            dp.destroy();
        ]]> </method>
        <method name="open"> <![CDATA[
            super.open();
            context.mainForm.commit();
        ]]> </method>
        <method name="closeDlg" args="ignore=null">
            close();
        </method>
        <method name="openDlg" args="ignore=null">
            open();
        </method>
        <method name="create" args="arg"> <![CDATA[
            completeInstantiation();
            if (!arg.targetnode || !arg.actiontype) {return;}
            if (arg.actiontype == "edit") {
                TimeOfUseCostTable.setAttribute("data",arg.targetnode.cloneNode(true));
            } else if (arg.actiontype == "add") {
                clearTimeFrames();
                setBeginMonth("jun");
                setEndMonth("jun");
                setTypeOfDay("all");
            } else {return;}
            this["actiontype"] = arg.actiontype;
            this["targetnode"] = arg.targetnode;
            lz.Idle.callOnIdle(openDel);
        ]]> </method>
        
    </class>
    
    <timeofuseutilitycostdialog id="TimeOfUseUtilityCostDlg"/>
</library>
