<library>
    <managedsynapmodaldialog id="AddDashTemplateDialog" prompt="Add Dashboard Template">
        <attribute name="dialogState" value="$once{DashboardDialogStates.NEW}"/>
        <attribute name="options"/>
        
        <escommands name="ESCommander" visibleRefresh="true"/>
        
        <simplelayout axis="y" spacing="5"/>
        
        <view name="topView">
            <simplelayout axis="x" spacing="10"/>
                
            <view name="leftView">
                <simplelayout axis="y" spacing="15" inset="5"/>                
                <synaptext prompt="text/name"/>
                <synaptext prompt="text/description"/>
            </view>
            
            <view name="rightView" pixellock="true">
                <simplelayout axis="y" spacing="10"/>
                <synapedittext name="nameEt" width="250"/>
                <synapedittext name="descrEt" multiline="true" height="80" width="250"/>
            </view>    
        </view>
        
        <view name="buttonsView" pixellock="true" align="right">
            <simplelayout axis="x" spacing="5"/>
            <synapbutton prompt="button/ok">
                <handler name="onclick">
                    AddDashTemplateDialog.doOnOk();
                </handler>
            </synapbutton>
            
            <synapbutton prompt="button/cancel">
                <handler name="onclick">
                    AddDashTemplateDialog.close();
                </handler>
            </synapbutton>
        </view>
        
        <handler name="onresult" args="res" reference="ESCommander">
            if (res) {
                this.close();
            }
        </handler>
        
        <method name="create" args="options=null">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            this.options = options;
            
            this.open();
        </method>
        
        <method name="doOnOk">
            var result = validate(); 
            if(!result.res){
                InfoDialog.show(result.msg);
                return;
            }
            
            var uniquePropNames = ["name"];
            var props = [{name: "name", value: this.getNameEt().text},
                         {name: "isPrivate", value: 0},
                         {name: "dashJsonString", value: result.templateParams.jsonStr},
                         {name: "objIDsToReplace", value: result.templateParams.ids.join(",")},
                         {name: "descr", value: this.getDescrEt().text},
                         {name: "userId", value: LoginInfo.userId}];
            
            this.ESCommander.createObject(
                                "DASHBOARD_TEMPLATE",
                                '', // parent id (no parent)
                                {properties : props},
                                '', // message
                                uniquePropNames);
            
        </method>
        
        <method name="validate"><![CDATA[
            var result = {res: true, msg: "", templateParams: null};
            if(this.getNameEt().text == ""){
                result.res = false;
                result.msg = "Please enter template name";
                return result;
            }
            
            result.templateParams = options.dashObjRef.getParamsForTemplate();
            var ids = result.templateParams.ids;
            var dcsNum = 0;
            var puesNum = 0;
            for (var i = 0; i < ids.length; i++){
                var currType = TO.typeFromString(ids[i]);
                if (currType == "DC"){
                    dcsNum++;
                } else if (currType == "PUE"){
                    puesNum++;
                }
            }
            if(dcsNum > 1 || puesNum > 1){
                result.res = false;
                result.msg = "Template can't be created. Dashboard should have one DC and/or one PUE";
            } else if (dcsNum + puesNum != ids.length) {
                result.res = false;
                result.msg = "Template can't be created. Dashboard should have only DC and/or one PUE";
            }
                       
            return result;
        ]]></method>
        
        <method name="getNameEt">
            return this.topView.rightView.nameEt;
        </method>
        
        <method name="getDescrEt">
            return this.topView.rightView.descrEt;
        </method>
        
    </managedsynapmodaldialog>
</library>