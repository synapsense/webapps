<library>
    <include href="../../components/synapformdialog.lzx"/>
    <include href="../../components/synapform.lzx"/>
    <include href="boundslider.lzx"/>
    
    <synapformdialog id="VisualResizerDialog" prompt="dash_title_set_widget_size" privileges="ACCESS_DASHBOARDS" showConfirmationOnApply="false" showConfirmationOnOk="false">                
        <attribute name="options"/>
         
        <synapform name="mainForm">
            <simplelayout axis="y" spacing="10"/>
            <view name="widthView" pixellock="true">
                <simplelayout axis="x" spacing="2"/>                
                <synaptext prompt="dash_text_width" pixellock="true" valign="middle"/>                
                <boundslider name="widthSlider"/>
            </view>
            <view name="heightView" pixellock="true">
                <simplelayout axis="x" spacing="2"/>                
                <synaptext prompt="dash_text_height" pixellock="true" valign="middle"/>                
                <boundslider name="heightSlider"/>
            </view>
        </synapform>
        
        <method name="create" args="options">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            this.options = options;            
            var content = this.options.content;
            
            setSliderData(this.getWidthSlider(), this.options.maxWidthDelta, content.width);
            setSliderData(this.getHeightSlider(), this.options.maxHeightDelta, content.height);
            
            this.open();
        </method>
        
        <method name="setSliderData" args="slider, maxDelta, currentSizeValue">
            var minValue = currentSizeValue - maxDelta.maxDecrease;
            var maxValue = currentSizeValue + maxDelta.maxIncrease;
            
            slider.setAttribute("maxSliderValue", maxValue);
            slider.setAttribute("minSliderValue", minValue);
            
            slider.setValue(currentSizeValue, true);            
            slider.updateLabelVals(minValue, maxValue);
        </method>
        
        <!--Override-->
        <method name="close" args="...ignore">
            this.doClose(); //close without any confirmation
        </method>
      
        <!--Override-->
        <method name="isChanged">
            return this.getMainForm().changed;
        </method>

        <!--Override-->
        <method name="doApply" args="ignore=null">
            this.getMainForm().commit();
            var content = this.options.content;            
            content.setAttribute("width", this.getWidthSlider().getValue());
            content.setAttribute("height", this.getHeightSlider().getValue());
            this.options.fieldRef.resizeContents(content);
        </method>       
       
        <!--Override-->
        <method name="validate">
            return true; // need no validation
        </method>
        
        <method name="getMainForm">
            return this.mainForm;
        </method>
        
        <method name="getWidthSlider">
            return this.mainForm.widthView.widthSlider;
        </method>
        
        <method name="getHeightSlider">
            return this.mainForm.heightView.heightSlider;
        </method>
    </synapformdialog>
</library>