<library>
    <include href="displayobject.lzx"/>
    <include href="../../presentation/synaplvcharts/autorefresh/refreshchartviewer.lzx"/>
    <include href="../templates/charttemplate.lzx"/>
    <include href="../templates/chartlinetemplate.lzx"/>
    <include href="../dataretrieving/chartpropertyretriever.lzx"/>
    <include href="../utils/chartingutils.lzx"/>
    
    <class name="autorefreshchartdo" extends="displayobject">
        <attribute name="cosmeticData"/>
        <attribute name="shownInterval" value="${2*60*60*1000}" type="number"/>
        
        <attribute name="propertyTO" value="$once{new PropertyId('','')}"/>
        <attribute name="serieDescrArr" value="$once{[]}"/>
        
        <attribute name="configDoneDel" value="$once{new LzDelegate(this, 'onConfigurationDone')}"/>
        <attribute name="redrawDel" value="$once{new LzDelegate(this, 'redraw')}"/>
        
        <chartpropertyretriever name="PropRetriever"/> <!-- it is needed for units and object name retrieving -->
        
        <refreshchartviewer name="chartViewer" 
                           width="${immediateparent.width}"
                           height="${immediateparent.height}"
                           chartWidth="${this.width - 10}"
                           chartHeight="${this.height}"/>
       
       <handler name="oninit">
            this.dispContentRef.setTitle("Chart");
       </handler>
       
       <handler name="onwidth">
            this.chartViewer.setAttribute("width", this.width);
            lz.Timer.resetTimer(this.redrawDel, 100);
        </handler>
        
        <handler name="onheight">
            this.chartViewer.setAttribute("height", this.height);
            lz.Timer.resetTimer(this.redrawDel, 100);
        </handler>
        
        <method name="redraw" args="ignore=null">
            this.chartViewer.redrawChart();
        </method>
        
        <method name="resetChart" args="showPleaseWait=true">
            if(this.cosmeticData == null){
                return;
            }
            this.getChartViewer().removeAllSeries();
            
            var chartTemplate = this.cosmeticData.chartTemplate;            
            this.getChartViewer().setLegendOptions(chartTemplate.showLegend, chartTemplate.legendPosition);
            
            PropRetriever.setAttribute("chartTemplate", chartTemplate);
            PropRetriever.doRetrieve(showPleaseWait);
        </method>
        
        <handler name="ondataretrieved" reference="PropRetriever">
            var nowTime = (new Date()).getTime();
            this.getChartViewer().setTimeEdges(nowTime - shownInterval, nowTime);
            this.getChartViewer().setShownInterval(shownInterval);
            
            this.addSeriesToChartByPropDisplayData();            
            this.getChartViewer().requestDataForAllSeries();
        </handler>

        <method name="addSeriesToChartByPropDisplayData"><![CDATA[
            var chartTemplate = cosmeticData.chartTemplate;
            this.freeSeriesDescrs();
            var propsDispData = PropRetriever.getPropsDisplayData();
            for(var i = 0; i < propsDispData.length; i++){
                var propIdStr = propsDispData[i].propId; 
                var dispData = propsDispData[i].dispData;
                
                var res = ChartingUtils.getSerieDisplayData(chartTemplate, propIdStr, dispData);                
                var propColor = res.propColor;
                var propDispName = res.propDispName;
                
                var serieDescr = new lz.seriedescr(this, {lineDescr: new lz.linedescr(this, {color: propColor, weight: 1})})
                serieDescr.propID = propIdStr;
                serieDescr.serieName = propDispName;
                serieDescr.axisName = dispData.units;
                this.serieDescrArr.push(serieDescr);
                
                this.getChartViewer().addSerie(propIdStr, serieDescr);
            }
        ]]></method>
        
        <method name="freeSeriesDescrs"><![CDATA[
            for(var i = 0; i < serieDescrArr.length; i++){
                serieDescrArr[i].destroy();
            }
            this.serieDescrArr.splice(0, this.serieDescrArr.length);
        ]]></method>
       
       <method name="configure" args="options=null">
            CustomQueryWizardDialog.create({callback: this.configDoneDel, cosmeticData: cosmeticData, shownInterval: this.shownInterval, dispObjRef: this});
        </method>
        
        <method name="onConfigurationDone" args="event_params=null">
            if(event_params != null &amp;&amp; event_params.cosmeticData != null){
                this.cosmeticData = event_params.cosmeticData;

                this.getChartViewer().getChart().applyThresholdsOptions(this.cosmeticData.chartTemplate);
                this.getChartViewer().getChart().applyMetricsOptions(this.cosmeticData.chartTemplate);

                this.shownInterval = event_params.interval;
                this.resetChart();
            }
        </method>
        
        <method name="refresh" args="visibleRefresh=false">
            this.getChartViewer().setAttribute("visibleRefresh", visibleRefresh);
            this.chartViewer.refresh();
        </method>
       
        <method name="update" args="showPleaseWait=true">
            this.getChartViewer().setAttribute("visibleRefresh", showPleaseWait);
            this.resetChart(showPleaseWait);
        </method>
       
        <method name="createJSONObject">
            var jsonObj = super.createJSONObject();
            jsonObj.shownInterval = this.shownInterval;
            
            if(this.cosmeticData != null){
                jsonObj.cosmeticData = this.cosmeticData.createJSONObject();
            }
            return jsonObj;
        </method>
        
        <method name="createByJSONObject" args="jsonObj">
            super.createByJSONObject(jsonObj);
            
            if(jsonObj.cosmeticData){
                this.cosmeticData = FusionChartCosmeticData.createByJSONObject(jsonObj.cosmeticData);
            }            

            this.getChartViewer().getChart().applyThresholdsOptions(this.cosmeticData.chartTemplate);
            this.getChartViewer().getChart().applyMetricsOptions(this.cosmeticData.chartTemplate);

            this.shownInterval = jsonObj.shownInterval;
        </method>
        
        <method name="getDependentIDs">
            if (this.cosmeticData == null){
                return [];              
            }
            return this.cosmeticData.chartTemplate.getObjectIds();
        </method>
        
        <method name="getChartViewer">
            return this.chartViewer;
        </method>
        
    </class>
</library>