<library>
    <include href="displayobject.lzx"/>
    <include href="tabledoconfigdialog.lzx"/>
    <include href="../../presentation/tabularviews/commontabularview/commontableview.lzx"/>
    <include href="../../presentation/tabularviews/commontabularview/tabulartemplate/tabulartemplate.lzx"/>
    
    <class name="tabledo" extends="displayobject" defaultWidth="300">
        <attribute name="templateId"/>
        <attribute name="configDoneDel" value="$once{new LzDelegate(this, 'onConfigurationDone')}"/>
        <attribute name="showPleaseWaitOnUpdate" value="true" type="boolean"/>
        
        <synapdataset name="GetTemplate" type="http" src="http:commonTabular"  request="false"/>
        
        <commontableview name="tableView" width="${immediateparent.width}" height="${immediateparent.height}"/>
        
        <view name="infoView" visible="false" width="${immediateparent.width}" height="${immediateparent.height}">
            <synaptext prompt="dash_previously_stored_template_not_found" valign="middle" align="center"/>
        </view>
        
        <handler name="oninit">
            this.getTableView().getTableFilter().setAttribute("visible", false); //TEMPORARY !!!
        </handler>
        
        <method name="create" args="options">
            if(!this["isinited"]) {
                this.completeInstantiation();
            }
            
            if(options.parentId != null){
                this.setParentId(options.parentId);
            }
            
            if(options.templateId != null){
                this.templateId == options.templateId;
                this.requestTemplate();
            }
        </method>
        
        <method name="setParentId" args="parentId">
            super.setParentId(parentId);
            var parId = this.staticParentId ? this.staticParentId : this.parentId;
            this.getTableView().setParentId(parId);
        </method>
        
        <handler name="ondata" reference="GetTemplate">
            var dp = GetTemplate.getPointer();
            var jsonStr = dp.xpathQuery("templates/template/@jsonStr");
            var templateIsFound = jsonStr != null ? true : false;
            updateInfoTextVisibility(templateIsFound);
            
            if(!templateIsFound){
                Debug.write("Template is not found");
                return;
            }
            var tabularTemplate = TabularTemplate.createByJSONObject(JSON.parse(jsonStr));
            
            this.resetTable(tabularTemplate);
            this.requestData(this.showPleaseWaitOnUpdate);
        </handler>
        
        <method name="resetTable" args="tabularTemplate">
            this.getTableView().resetTable(tabularTemplate);
        </method>
        
        <method name="requestTemplate" args="showPleaseWait=true">
            if(this.templateId == null){
                return;
            }
            
            var params = new LzParam();
            params.addValue("requestType", "getTemplates");
            params.addValue("templateId", this.templateId);
            
            var parId = this.staticParentId ? this.staticParentId : this.parentId;
            this.getTableView().setParentId(parId);
                
            GetTemplate.setQueryString(params);
            GetTemplate.doReq(null, showPleaseWait);
        </method>
        
        <method name="configure" args="options=null">
            var confOpt = {callback: this.configDoneDel, typesStr: "DC,ROOM", dispObjRef: this};
            if(this.staticParentId != null){
                confOpt.objToSelect = {objId: this.staticParentId};
                confOpt.templateId = this.templateId;
            }
            TableDOConfigDialog.create(confOpt);
        </method>
        
        <method name="onConfigurationDone" args="event_params=null">
            if(event_params != null &amp;&amp; event_params.templateId != null){
                if(event_params.staticObjId){
                    this.dispContentRef.setTitleIfNoCustom(event_params.title + " - " + event_params.staticObjName);
                } else {
                    this.dispContentRef.setTitleIfNoCustom(event_params.title);
                }
                this.staticParentId = event_params.staticObjId;
                this.templateId = event_params.templateId;
                requestTemplate();
            }
        </method>
        
        <method name="refresh" args="visibleRefresh=false">
            this.getTableView().refresh(visibleRefresh);
        </method>
        
        <method name="update" args="showPleaseWait=true">
            this.setAttribute("showPleaseWaitOnUpdate", showPleaseWait);
            requestTemplate(showPleaseWait);
        </method>
        
        <method name="createJSONObject">
            var jsonObj = super.createJSONObject();
            if(this.templateId){
                jsonObj.templateId = this.templateId;
            }
            return jsonObj;
        </method>
        
        <method name="createByJSONObject" args="jsonObj">
            super.createByJSONObject(jsonObj);
            this.templateId = jsonObj.templateId;
        </method>
        
        <method name="requestData" args="showPleaseWait=true">
            this.getTableView().requestData(showPleaseWait);
        </method>
        
        <method name="updateInfoTextVisibility" args="isTemplateFound">
            infoView.setAttribute("visible", !isTemplateFound);
            this.getTableView().setAttribute("visible", isTemplateFound);
        </method>
        
        <method name="getTableView">
            return this.tableView;
        </method>
                
    </class>
</library>