<library>
    <include href="displayobject.lzx"/>

    <include href="../../presentation/datarestrictor/datarestrictor.lzx"/>
    <include href="../../presentation/alerts/alertacknowledgementwindow.lzx"/>
    
    <class name="dashactivealertsgridrow" extends="synapgridrow" >
        <handler name="ondblclick">
            if (this.selected) {
                var parentWnd = null;
                    if(this.parent.parent.classroot.parent['id']!=null){
                        parentWnd =  this.parent.parent.classroot.parent;
                    }else if(this.parent.parent.classroot.parent.parent['id']!=null){
                        parentWnd = this.parent.parent.classroot.parent.parent;
                   }
                   if (parentWnd==null){
                        AlertDetailsDialog.create({selRow: this});
                   }else{
                        AlertDetailsDialog.create({selRow: this, parentWind:parentWnd });
                   }
            }
        </handler>
        
        <method name="getContextMenuItems" args="window=null">
            var items = new Array();
            items.push(lz.synapcontextmenuitem.create({prompt: "context_menu/acknowledge_alert",window:'AlertsGrid', delegate: new lz.Delegate(this, 'acknowledgeAlerts')}));
            items.push(lz.synapcontextmenuitem.create({prompt: "context_menu/resolve_alert",window:'AlertsGrid', delegate: new lz.Delegate(this, 'resolveAlerts')}));
            items.push(lz.synapcontextmenuitem.create({prompt: "context_menu/dismiss_alert",window:'AlertsGrid', delegate: new lz.Delegate(this, 'dismissAlerts')}));
            return items.concat(super.getContextMenuItems('AlertsGrid'));
        </method>
        
        <method name="openAcknowledgementWindow" args="mode">
            var params = new LzParam();
            params.addValue("mode", mode);
            params.addValue("arrAlertIds", this.ownerGrid.getSelectedIds().join(","));
            
            var prompt = "";            
            if(mode == "acknowledgeSelected") {
                prompt = "enhalerting/acknowledge_alert";
            } else if(mode == "resolveSelected") {
                prompt = "enhalerting/resolve_alert";
            } else if(mode == "dismissSelected") {
                prompt = "enhalerting/dismiss_alert";
            }
            AlertAcknowledgementWindow.create(params, prompt);            
            this.ownerGrid.clearSelection();           
        </method>
        
        <method name="acknowledgeAlerts" args="ignore">
            openAcknowledgementWindow("acknowledgeSelected");
        </method>
        
        <method name="resolveAlerts" args="ignore">
            openAcknowledgementWindow("resolveSelected");
        </method>
        
        <method name="dismissAlerts" args="ignore">
            openAcknowledgementWindow("dismissSelected");
        </method>
    </class>
    
    <class name="alertgridviewdo" extends="displayobject" defaultWidth="300">
        <attribute name="configDoneDel" value="$once{new LzDelegate(this, 'onConfigurationDone')}"/>
        
        <synapdataset name="ActiveAlertsData" type="http" src="getAlerts">
            <handler name="ondata">
                var permIndAlertsNum = parseInt(this.getPointer().xpathQuery("object/@permissionsIndependentAlertsNum"));
                
                //change "no_data" grid prompt
                classroot.getAlertsGrid().setAttribute("nodataprompt", permIndAlertsNum != 0 ? "text/you_have_no_permissions_to_view_active_alerts" : "text/there_are_no_active_alerts");
                
                //show info if not all alerts were displayed
                var msg = DataRestrictor.getMessageIfDataWasRestricted(this.getPointer(), "datarestrictor/alerts_label");
                if(msg != null){
                    //TODO: show message about restrictions
                }
            </handler>
        </synapdataset>
        
        <consolegrid name="dashboardActiveAlertsGrid" gridtitle="${classroot.title}"
            datapath="local:classroot.ActiveAlertsData:/object"
            contentdatapath="*/alerts/alert"
            selectable="true" multiselect="true" applyselection="true" idxpath="alert_id/text()"
            width="${immediateparent.width}" height="${immediateparent.height}"
            nodataprompt="text/there_are_no_active_alerts" showcolumnselector="true"
            _rowclass="lz.dashactivealertsgridrow"
            settings="${UserPreference}" alwaysRestoreCleanGrid="true">                     
                        
            <synapgridcolumn width="87"  prompt="columnname/status" sortpath="alert_isActive/text()">
                <synaptext datapath="alert_isActive/text()" clickable="true" showhandcursor="false" valign="middle">
                    <handler name="ontext" args="status">
                        this.setAttribute("prompt", getFeatureSetting("alerts", "status", status));
                    </handler>
                </synaptext>
            </synapgridcolumn>            
            
            <synapgridtext prompt="columnname/alert_name" width="100" datapath="alert_name/text()" editable="false" />
                                    
             <synapgridcolumn width="200" prompt="columnname/alert_message" >
                <text datapath="alert_message/text()" clickable="true" showhandcursor="false" valign="middle">
                    <synaptooltip text="${parent.text}" multiline="true"/>
                    <handler name="ontext">
                        this.setAttribute("text", removeCommandCharacters(this.text));
                    </handler>
                </text>
            </synapgridcolumn>
            
            <synapgridcolumn width="70" prompt="columnname/alert_priority" sortpath="alert_priority_name/text()">
                <method name="translate" args="value">
                   return LangManager.getLocaleString(getFeatureSetting("alerts", "prioritynames", value));
                </method>
                <synaptext datapath="alert_priority_name" valign="middle">
                    <handler name="ondata">
                        this.setAttribute("prompt", getFeatureSetting("alerts", "prioritynames", this.datapath.xpathQuery("text()")));
                        this.setAttribute("align", "left");
                    </handler>
                </synaptext>
            </synapgridcolumn>
            <synapgridcolumn width="170" prompt="columnname/object" sortpath="object_name/text()">
                <synaptext datapath="." prompt="$path{'object_name/text()'}" valign="middle"/>
            </synapgridcolumn>
            <synapgridtext width="100" prompt="columnname/timestamp" datapath="time_stamp/text()" 
                           editable="false" sortpath="time_stamp/@lt" datatype="number" filterdatatype="string" filterpath="time_stamp/text()"/>
            
            <!--<method name="saveTableState" args="ignore=null">
                //not supported!
            </method>-->
        </consolegrid>
        
        <method name="requestData" args="visibleRefresh=true">
            var parId = this.staticParentId;
            if(parId == null){
                return;
            }
            
            var params = new LzParam();
            var alertType = "active";
            
            params.addValue("id", parId);
            params.addValue("mode", "object");
            params.addValue("alertType", alertType);
            
            this.ActiveAlertsData.setQueryString(params);
            //this.ActiveAlertsData.doReq();
            if(visibleRefresh){
                 ActiveAlertsData.doReq();
            }else{
                 ActiveAlertsData.doRequest();
            }
        </method>
        
        <method name="configure" args="options=null">
            var confOpt = {callback: this.configDoneDel, dispObjRef: this};
            if(this.staticParentId != null){
                confOpt.objToSelect = {objId: this.staticParentId};
            }
            StaticIdSelectorDialog.create(confOpt);
        </method>
        
        <method name="onConfigurationDone" args="event_params=null">
            if(event_params != null){
                if(event_params.staticObjId){
                    this.dispContentRef.setTitleIfNoCustom(LangManager.getLocaleString("title/active_alerts") + " - " + event_params.staticObjName);
                }
                this.staticParentId = event_params.staticObjId;
            }
            this.requestData();
        </method>
        
        <method name="refresh" args="visibleRefresh=false">
            this.requestData(visibleRefresh);
        </method>
        
        <method name="update" args="showPleaseWait=true">
            this.requestData(showPleaseWait);
        </method>
        
        <method name="createByJSONObject" args="jsonObj">
            super.createByJSONObject(jsonObj);
        </method> 
        
        <method name="getAlertsGrid">
            return this.dashboardActiveAlertsGrid;
        </method>
    </class>
</library>