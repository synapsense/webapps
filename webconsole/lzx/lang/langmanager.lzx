<!-- ====================================================================== 
     Dec 11, 2008 4:55:08 PM                                                        
     apahunov                                                                
     ====================================================================== -->
<library>
    <include href="../utils/stringutils.lzx"/>
    <include href="../utils/userpreference.lzx"/>
     <include href="../data/basesynapdataset.lzx"/>
   
    <dataset name="LangData" request="true" type="http" src="resources/LangData.xml" trimwhitespace="true"/>
    <basesynapdataset name="SetLanguageData" src="setLanguage"/>

    <!-- Ctrl+Alt+L - cycle through available languages -->
    <command key="['l', 'alt', 'control']">
        <handler name="onselect"><![CDATA[
            var langs = LangManager.listLocales();
            
            if (langs.length < 2) {
                return;
            }
            
            var curLang = LangManager.getCurrentLocale();
            var index = -1;
            for (var i = 0; i < langs.length; i++) {
                if (langs[i] == curLang) {
                    index = i;
                    break;
                }
            }
            index = (index + 1) % langs.length;
            UserPreference.set("SavedLanguage", langs[index]);
            LangManager.switchLocale(langs[index]);
        ]]>
        </handler>
    </command>
    
    <!-- Object to provide multi-language support
    -->
    <node name="LangManager">
        <!-- Sent when current language is changed -->
        <event name="langchanged"/>
        <!-- Sent when LandData.xml is loaded -->
        <event name="langDataLoaded"/>
        
        <!-- Current language -->
        <attribute name="curLang" type="string" value="en_US"/>
       
        <handler name="ondata" reference="LangData">
            langchanged.sendEvent(this.curLang);
            langDataLoaded.sendEvent();
        </handler>
        
        <!-- Sets current language -->
        <!-- public -->
        <method name="switchLocale" args="locale">
            if (!locale || this.curLang == locale) {
                return;
            }
            
            this.curLang = locale;
            langchanged.sendEvent(locale);
            var params = new LzParam();
            params.addValue("lang", locale);
            SetLanguageData.setQueryString(params);
            SetLanguageData.doRequest();
        </method>

        <!-- private -->
        <method name="validatePromptId" args="id"> <![CDATA[
            if (!id) {
                return false;
            }
            
            if (!(id instanceof String)) {
                id = String(id);
            }
            var tokens = id.split("/");
            var re = new RegExp("[^a-zA-Z_0-9\\-]");
            for (var i = 0; i < tokens.length; i++) {
                if(re.exec(tokens[i]) != null) {
                    return false;
                }
            }
            return true;
        ]]>
        </method>

        <!-- Translates prompt id into localized string -->
        <!-- public -->
        <method name="getLocaleString" args="id">
            if (!validatePromptId(id)) {
                return id;
            }
            var repId = id.replaceAll("/", "_");
            var result = LangData.getPointer().xpathQuery("locales/locale[@id='" + this.curLang + "']/" + repId + "/text()");
            if(!result){
                result = id;
            } else if (result == "''") {
                result = "";
            }
            return result;
        </method>
        
        <!-- Translates prompt id into localized string.
             Substitutes $n patterns in the result string with the supplied strings-->
        <!-- public -->
        <method name="getFormattedLocaleString" args="id, strings">
            if (!validatePromptId(id)) {
                return id;
            }

            var repId = id.replaceAll("/", "_");
            var result = LangData.getPointer().xpathQuery("locales/locale[@id='" + this.curLang + "']/" + repId + "/text()");
            if(!result){
                result = repId;
            } else if (result == "''") {
                result = "";
            }
            
            if (result) {
                var i = 0;
                while (result.indexOf("$" + i) != -1) {
                    var s = arguments[i + 1];
                    result = replaceAll(result, "$" + i, s);
                    i++;
                } 
            }
            return result;
        </method>
        
        <!-- Checks prompt existing -->
        <!-- public -->
        <method name="isPromptExists" args="id">
            if (!validatePromptId(id)) {
                return false;
            }
            var repId = id.replaceAll("/", "_");
            var result = LangData.getPointer().xpathQuery("locales/locale[@id='" + this.curLang + "']/" + repId + "/text()");
            return result != null ? true : false;
        </method>
        
        <!-- Returns arrays of available languages -->
        <!-- public -->
        <method name="listLocales">
            return LangData.getPointer().xpathQuery("locales/locale/@id");
        </method>
        
        <method name="getDefaultLocale">
            return LangData.getPointer().xpathQuery("locales/locale[@isDefault='true']/@id");
        </method>
        <!-- Returns localized name of the language -->
        <!-- public -->
        <method name="getLangLocale" args="locale">
            return LangData.getPointer().xpathQuery("locales/locale[@id='" + locale + "']/@lang");
        </method>
        
        <!-- Returns first available language -->
        <!-- public -->
        <method name="getFirstLocale">
            return LangData.getPointer().xpathQuery("locales/locale[1]/@id");
        </method>
        
        <!-- Returns current language -->
        <!-- public -->
        <method name="getCurrentLocale">
            return this.curLang;
        </method>
        
        <!-- Returns current codeISO3 -->
        <!-- public -->
        <method name="getCodeISO3CurrentLocale">
            return LangData.getPointer().xpathQuery("locales/locale[@id='" + this.curLang + "']/@codeISO3");
        </method>
        
        <!-- Returns regular expression for the alphabets of all locales -->
        <!-- public -->
        <method name="getValidChars"> <![CDATA[
            var validChars = LangData.getPointer().xpathQuery("locales/locale/@chars");
            if (!validChars) {
                return null;
            }
            if (!(validChars instanceof Array)) {
                validChars = [validChars]
            }
            return validChars.join(",");
        ]]>
        </method>
        
    </node>    
</library>
