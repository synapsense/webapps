<library>
    <include href="lz/scrollbar.lzx"/> <!-- include for native resources -->
    <include href="../../../utils/mouseeventsender.lzx"/>
        
    <!-- This class is a copy of native scrollbar. Just necessary modifications were applied.  -->
    <class name="scrollbarcp" extends="basescrollbar" bgcolor="0x595959">

        <attribute name="disabledbgcolor" value="null"/>
    
        <attribute name="skinned" value="true" type="boolean"/>
        
        <event name="onposchanged"/>
        
        <handler name="oninit" reference="canvas" >
            this._showEnabled();
        </handler>
        
        <handler name="oninit">
            new lz.Delegate(this, '_onGlobalMenuOpen', GlobalContextMenu,
                    'onmenuopen');
            new lz.Delegate(this, '_onGlobalMenuOpen', DefaultContextMenu,
                    'onmenuopen');
        </handler>
        
        <method name="_onGlobalMenuOpen" args="ignore=null">
            //fix for [8762] System Map drop down automatically scrolls to bottom of list
            // mouseup event is sent because onmouseup event of the scrollbar does not happens 
            // and the scrollbar will be scrolled themselves 
            // if click right mouse during scroll the scrollbar
            MouseUpEventSender.sendMouseUpEvent(this);
        </method>
        
        <!--- @keywords private -->
        <method name="_showEnabled">
            <![CDATA[
            if (!_enabled) {
                var newbgcolor = this.disabledbgcolor;
                if (newbgcolor == null) {
                    var p = immediateparent;
                    while (p.bgcolor == null && p != canvas) {
                        p = p.immediateparent;
                    }
                    newbgcolor = p.bgcolor;
                    if (newbgcolor == null) newbgcolor = 0xffffff;
                }
                this.setAttribute('bgcolor', newbgcolor);
            } else {
                this.setAttribute('bgcolor', 0x595959);
            }
            super._showEnabled();
        ]]>
        </method>
    
        <method name="step" args="num">
            super.step(num);
            onposchanged.sendEvent(null);
        </method>
        
        <method name="page" args="num">
            super.page(num);
            onposchanged.sendEvent(null);
        </method>
    
        <state applied="${this.parent.axis == 'y'}">
            <attribute name="width" value="14"/>
    
            <!-- up arrow -->
            <view name="toparrow">
                <basescrollarrow x="1" y="1"
                    resource="lzscrollbar_ybuttontop_rsc"
                    direction="-1"/>
            </view>
            <view name="scrolltrack">
                <!-- top of the scroll track -->
                <basescrolltrack name="top"
                    x="1" stretches="height"
                    overResourceNumber="0"
                    downResourceNumber="2"
                    disabledResourceNumber="3"
                    direction="-1">
                    <attribute name="height" value="${parent.thumb.y}"/>
                    <attribute name="width" value="${parent.width}"/>
                    
                    <handler name="oninit">
                        //resource="lzscrollbar_ytrack_rsc"
                        if(!classroot.skinned){
                            this.setAttribute("resource", "lzscrollbar_ytrack_rsc");
                        }
                    </handler>
                </basescrolltrack>
    
                <!-- thumb -->
                 <basescrollthumb name="thumb" x="1">
                     <view resource="lzscrollbar_ythumbtop_rsc"/>
                     <view resource="lzscrollbar_ythumbmiddle_rsc" stretches="both"/>
                     <view resource="lzscrollbar_ythumbbottom_rsc"/>
                     <stableborderlayout axis="y"/>
                     <!-- note: stableborderlayout only acts on the first three views -->
                     <view resource="lzscrollbar_ythumbgripper_rsc" x="1"
                         height="${Math.min(200, parent.height-16)}" width="11"
                         clip="true" valign="middle"/>
                         
                     <method name="thumbControl" args="mousepos">
                        var p = super.thumbControl(mousepos);
                        classroot.onposchanged.sendEvent(null);
                        return p;
                     </method>
                </basescrollthumb>
    
                <!-- bottom of the scroll track -->
                <basescrolltrack name="bottom"
                    x="1" stretches="height"
                    overResourceNumber="0"
                    downResourceNumber="2"
                    disabledResourceNumber="3">
                    <attribute name="y" value="${parent.thumb.y+parent.thumb.height}"/>
                    <attribute name="height"
                        value="${parent.height - parent.thumb.y - parent.thumb.height}"/>
                    <attribute name="width" value="${parent.width}"/>
                    
                    <handler name="oninit">
                        //resource="lzscrollbar_ytrack_rsc"
                        if(!classroot.skinned){
                            this.setAttribute("resource", "lzscrollbar_ytrack_rsc");
                        }
                    </handler>
                    
                </basescrolltrack>
    
    
            </view>     <!-- scrolltrack -->
    
            <!-- down arrow -->
            <view height="14" name="bottomarrow">
                <basescrollarrow x="1"
                    resource="lzscrollbar_ybuttonbottom_rsc"/>
            </view>
            <stableborderlayout axis="y"/>
         </state>
    
    
        <state applied="${this.parent.axis == 'x'}">
            <attribute name="height" value="14"/>
    
            <!-- left arrow -->
            <view name="leftarrow">
                <basescrollarrow x="1" y="1"
                    resource="lzscrollbar_xbuttonleft_rsc"
                    direction="-1"/>
            </view>
            <view name="scrolltrack" height="12" y="1">
                <!-- top of the scroll track -->
                <basescrolltrack name="top"
                    stretches="width"
                    overResourceNumber="0"
                    downResourceNumber="2"
                    disabledResourceNumber="3"
                    direction="-1">
                    <attribute name="width" value="${parent.thumb.x}"/>
                    <attribute name="height" value="${parent.height}"/>
                    
                    <handler name="oninit">
                        if(!classroot.skinned){
                            this.setAttribute("resource", "lzscrollbar_xtrack_rsc");
                        }
                    </handler>
                    
                </basescrolltrack>
    
                <!-- thumb -->
                 <basescrollthumb name="thumb" y="0">
                     <view resource="lzscrollbar_xthumbleft_rsc"/>
                     <view resource="lzscrollbar_xthumbmiddle_rsc" stretches="both"/>
                     <view resource="lzscrollbar_xthumbright_rsc"/>
                     <stableborderlayout axis="x"/>
                         
                     <method name="thumbControl" args="mousepos">
                        var p = super.thumbControl(mousepos);
                        classroot.onposchanged.sendEvent(null);
                        return p;
                     </method>
                     <!-- note: stableborderlayout only acts on the first three views -->
                     <view resource="lzscrollbar_xthumbgripper_rsc" y="1"
                         width="${Math.min(200, parent.width-16)}"
                         clip="true" align="center"/>
                </basescrollthumb>
    
                <!-- bottom of the scroll track -->
                <basescrolltrack name="bottom"
                    stretches="width"
                    overResourceNumber="0"
                    downResourceNumber="2"
                    disabledResourceNumber="3">
                    <attribute name="x" value="${parent.thumb.x+parent.thumb.width}"/>
                    <attribute name="width"
                        value="${parent.width - parent.thumb.x - parent.thumb.width}"/>
                    <attribute name="height" value="${parent.height}"/>
                    
                    <handler name="oninit">
                        if(!classroot.skinned){
                            this.setAttribute("resource", "lzscrollbar_xtrack_rsc");
                        }
                    </handler>
                </basescrolltrack>
    
    
            </view>     <!-- scrolltrack -->
    
            <!-- right arrow -->
            <view width="14" name="rightarrow">
                <basescrollarrow name="rightarrow" y="1"
                    resource="lzscrollbar_xbuttonright_rsc"/>
            </view>
            <stableborderlayout axis="x"/>
         </state>
    
        <state applied="${this.parent.axis == 'x' &amp;&amp; this.parent.othersb}">
            <view name="patch" x="${parent.width}"
                bgcolor="${(parent._enabled || parent.othersb._enabled)
                    ? 0xbdbdbd : parent.disabledbgcolor}"
                width="${parent.height}" height="${parent.height}"/>
        </state>
    
    </class>

</library>