import System;
import System.IO;
import System.Shell;

function print(text) {
	Console.WriteLine(text);
};


// convert input.png -channel Alpha -evaluate Divide 2 output.png

function crop(fin, param, fout) {
	var im = new ActiveXObject("ImageMagickObject.MagickImage.1");

	print("Input:  " + fin);
	print("Output: " + fout);
	print("Param:  " + param);

	// -page  option
	// -debug option

	var result = im.Convert(fin, '-crop', param+"!", fout); // , '-debug', 'Blob');
	print("Result: " + result);
}


function crop9patch(filein, w, h, pfx, sfx) {
	
	var names = new Array(
    		"top_lft", "top_mid", "top_rt",
    		"mid_lft", "mid_mid", "mid_rt",
    		"bot_lft", "bot_mid", "bot_rt"
	);	

	var x   = 0;
	var y   = 0;
	var dx  = 0;
	var dy  = 0;
	var i, j;

	for (j=0; j<3; j++) {
		dy = h[j];
		x = 0;
		for (i=0; i<3; i++) {
			dx = w[i];

			var rect = dx + "x" + dy + "+" + x + "+" + y;
			var fileout = pfx + names[(j*3)+i] + sfx + ".png";

			crop(filein, rect, fileout);

			x+=dx; 								
		}
		y+=dy;
	}
}

function sliceIntoDir(fname, pfx, sfx, w, h) {
	Directory.CreateDirectory(pfx);

	crop9patch(fname+"[0]", w, h, pfx, sfx);

	File.Copy("slice.html", pfx + "slice.html", true);
} 


/*
 * Get the source file name
 */
var CmdArgs = Environment.GetCommandLineArgs();
if (CmdArgs.length == 1) {
	print("Usage: " + Path.GetFileName(CmdArgs[0]) + " <input.png>");
	Environment.Exit(255);
}

var file_in = CmdArgs[1];

print("Slicing image " + file_in);

var dlg_slct  = "wnd1.png";
var dlg_drag  = "wnd1d.png";
var dlg_dslct = "wnd2.png";
var dat_slct  = "wnd3.png";
var dat_drag  = "wnd3d.png";


/*
 * Crop the source file into multiple files
 */
crop(file_in, "800x288+0+0",     dlg_slct);
crop(file_in, "800x288+800+0",   dlg_drag);
crop(file_in, "800x288+0+288",   dlg_dslct);
// crop(file_in, "800x288+800+288", "wnd2-drag.png"); // NOT USED
crop(file_in, "800x288+0+576",   dat_slct);
crop(file_in, "800x288+800+576", dat_drag);


/*
 * Then create a separate directory for dlg_slct resources and split it as 9-patch image
 */
sliceIntoDir(dlg_slct,  "dlg_slct/",  "_slct",  [7, 786, 7], [27, 249, 12]);
sliceIntoDir(dlg_dslct, "dlg_dslct/", "_dslct", [7, 786, 7], [27, 249, 12]);
sliceIntoDir(dlg_drag,  "dlg_drag/",  "_drag",  [7, 786, 7], [27, 249, 12]);
sliceIntoDir(dlg_dslct, "dlg_dsbl/",  "_dsbl",  [7, 786, 7], [27, 249, 12]);

sliceIntoDir(dat_slct,  "dat_slct/",  "_slct",  [7, 786, 7], [7,  269, 12]);
sliceIntoDir(dat_drag,  "dat_drag/",  "_drag",  [7, 786, 7], [7,  269, 12]);
