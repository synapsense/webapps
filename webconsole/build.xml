<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<!-- ======================================================================

     project    	Synapsense WebConsole build script
     description    Script can be used to build the Web Console
     
     ====================================================================== -->

<project basedir="." default="build" name="synapsoft" xmlns:artifact="antlib:org.apache.maven.artifact.ant">

    <!-- [BC 02/2015] Load local property overrides. The ant build should not rely on JBOSS_HOME. -->
    <property file="build.properties" />

    <!-- [bfischer 11/2015] Where to get maven ant tasks jar. -->
    <property name="maven.ant.jar" value="maven-ant-tasks-2.1.3.jar" />
    <property name="maven.ant.downloadurl" value="http://central.maven.org/maven2/org/apache/maven/maven-ant-tasks/2.1.3" />
    <property name="maven.ant.jar.dir" value="${basedir}/build/ant" />
    
    <property environment="env" />
    <property name="JBossHome" value="${env.JBOSS_HOME}" />
    <!-- [BC 02.2015] Prevent ant from adding its classpath to javac. See:
        http://stackoverflow.com/questions/5103384/ant-warning-includeantruntime-was-not-set -->
    <property name="build.sysclasspath" value="ignore"/>
    <property name="debuglevel" value="source,lines,vars" />
    <property name="target" value="1.8" />
    <property name="source" value="1.8" />
    <property name="out.dir" value="${basedir}/build" />
    <property name="swf.file" value="console.swf" />
    <property name="war.file" value="synapsoft.war" />
    <property name="tar.file" value="synapsoft.tar.gz" />
    <property name="swfRuntime" value="swf10" />
    <property name="laszlo.home" value="${basedir}/../openlaszlo" />
    <property name="laszlo.src.dir" value="${basedir}" />
    <!--Set laszlo.debug="-g1" to turn on debug mode-->
    <property name="laszlo.debug" value="" />
    <property name="laszlo.proxied" value="false" />
    <property name="SERVLET_DEV" value="${laszlo.home}/3rd-party/jars/dev/" />

    <path id="laszlo.lib">
        <pathelement location="${laszlo.home}/WEB-INF/classes" />
        <fileset dir="${laszlo.home}/3rd-party/jars/dev" includes="**/*.jar" />
        <fileset dir="${laszlo.home}/WEB-INF/lib" includes="**/*.jar" />
    </path>

    <union id="war_files">
        <fileset id="swf" dir="${out.dir}">
            <include name="${swf.file}" />
            <include name="resources/*.properties" />
        </fileset>
        <fileset id="webconsole" dir=".">
            <include name="*.xsl" />
            <include name="*.jsp" />
            <include name="*.js" />
            <include name="*.html" />
            <include name="*.pdf" />
            <include name="*.txt" />
            <include name="*.xml" />
            <exclude name="build.xml" />
            <include name="conf/**/*.*" />
            <include name="resources/charts/*.*" />
            <include name="resources/icons/*.*" />
            <include name="resources/images/*.*" />
            <include name="resources/LangData.xml" />
            <include name="WEB-INF/*.xml" />
        </fileset>
        <fileset id="synap" dir="product/synap">
            <include name="**/*.*" />
            <exclude name="aips/*.*" />
            <exclude name="resources/**/*.*" />
        </fileset>
        <fileset id="webinf" dir="${out.dir}">
            <include name="WEB-INF/classes/**/*.*" />
            <include name="WEB-INF/lib/commons-beanutils-core*.jar" />
            <include name="WEB-INF/lib/commons-codec-*.jar" />
            <include name="WEB-INF/lib/commons-fileupload-*.jar" />
            <include name="WEB-INF/lib/commons-io-*.jar" />
            <include name="WEB-INF/lib/commons-jxpath-*.jar" />
            <include name="WEB-INF/lib/json-*.jar" />
            <include name="WEB-INF/lib/quartz-*.jar" />
            <include name="WEB-INF/lib/snmp4j-*.jar" />
            <include name="logs/**/*.*" />
        </fileset>
        <fileset id="lps_includes" dir="${laszlo.home}">
            <include name="lps/includes/**/*.*" />
            <exclude name="lps/includes/lfc/*.*" />
            <include name="3rd-party/**/*.*" />
            <include name="3rdPartyCredits/**/*.*" />
        </fileset>
    </union>
    
    <!-- Copy the antlib jar to a known location before it is used
         to get the dependencies -->
    <target name="-get_antlib" description="Gets Maven Ant Tasks jar from known URL">
        <get 
            src="${maven.ant.downloadurl}/${maven.ant.jar}"
            dest="${maven.ant.jar.dir}/${maven.ant.jar}"
            verbose="on"
            skipexisting="true"/>
    </target>
    
    <!-- Get the jars our JAVA code is dependent upon from the Maven repo.
         The jars the lazslo build is dependent upon are still in the GIT repo,
         since they are old, and no version information is available. -->
    <target name="-get_dependencies" description="Gets dependencies from Maven Repository" depends="-get_antlib">
        <path id="maven-ant-tasks.classpath" path="${maven.ant.jar.dir}/${maven.ant.jar}" />
        <typedef resource="org/apache/maven/artifact/ant/antlib.xml"
               uri="antlib:org.apache.maven.artifact.ant"
               classpathref="maven-ant-tasks.classpath" />

        <artifact:dependencies pathId="java.dependency.classpath" filesetId="java.dependency.fileset" useScope="runtime">
            <remoteRepository id="panduit-public" url="http://op-mnsdb-01p.panduit.com:8081/nexus/content/groups/public/" />
            <remoteRepository id="org.snmp4j" url="https://oosnmp.net/dist/release" />
            <remoteRepository id="org.jboss" url="https://repository.jboss.org/nexus/content/groups/public-jboss" />
            <remoteRepository id="googlecode" url="https://symja.googlecode.com/svn/maven-repository/" />
            
            <dependency groupID="commons-beanutils" artifactId="commons-beanutils-core" version="1.8.3"/>
            <dependency groupID="commons-codec" artifactId="commons-codec" version="1.9"/>
            <dependency groupID="commons-fileupload" artifactId="commons-fileupload" version="1.3.1"/>
            <dependency groupID="commons-io" artifactId="commons-io" version="2.4"/>
            <dependency groupID="commons-jxpath" artifactId="commons-jxpath" version="1.3"/>
            <dependency groupID="org.jboss.as" artifactId="jboss-as-controller-client" version="7.1.1.Final"/>
            <dependency groupID="org.jboss" artifactId="jboss-dmr" version="1.1.1.Final"/>
            <dependency groupID="org.jboss.spec.javax.ejb" artifactId="jboss-ejb-api_3.1_spec" version="1.0.0.Final"/>
            <dependency groupID="org.jboss.spec.javax.servlet" artifactId="jboss-servlet-api_3.0_spec" version="1.0.1.Final"/>
            <dependency groupID="org.json" artifactId="json" version="20141113"/>
            <dependency groupID="org.picketbox" artifactId="picketbox" version="4.0.7.Final"/>
            <dependency groupID="org.quartz-scheduler" artifactId="quartz" version="2.2.1"/>
            
            <dependency groupID="org.snmp4j" artifactId="snmp4j" version="2.3.3"/>
            <dependency groupID="org.drools" artifactId="drools-compiler" version="4.0.6"/>        
            <dependency groupID="org.drools" artifactId="drools-core" version="4.0.6"/>        
            <dependency groupID="org.jboss.web" artifactId="jbossweb" version="7.0.13.Final"/>
            <dependency groupID="com.sun.mail" artifactId="javax.mail" version="1.5.3"/>
            <dependency groupID="org.matheclipse" artifactId="matheclipse-parser" version="1.0.1"/>

            <dependency groupID="com.synapsense" artifactId="es-api" version="7.0.0-SNAPSHOT"/>        
        </artifact:dependencies>
        
    </target>
    
    <target name="clean" description="Delete all build artifacts" >
        <delete dir="${out.dir}" />
        
        <!-- Not used anymore, but left in to clean them up if they exist -->
        <delete>
            <fileset dir="." includes="**/*.exe" />
            <fileset dir="." includes="**/*.war" />
            <fileset dir="." includes="**/*.tar.gz" />
            <fileset dir="." includes="*.swf" excludes="e.swf" />
        </delete>
        <delete dir="WEB-INF/classes" />
        <delete dir="tmp" />
        <delete dir="WAR" />
        <delete dir="TAR" />
    </target>

    <target name="deploy" depends="-set_jboss_home, build_war" if="JBossHome" description="deploy resulting war to the JBOSS_HOME/standalone/deployments/" >
        <copy file="${out.dir}/jboss/${war.file}" todir="${JBossHome}/standalone/deployments/" overwrite="true" />
    </target>

    <target name="standalone_dist" depends="build, build_standalone_war">
        <exec executable="makensis">
            <arg value="/V2" />
            <arg value="synapsoft.nsi" />
        </exec>
        <move file="synapsoft-setup.exe" todir="${out.dir}" />
        <move file="${tar.file}" todir="${out.dir}" />
    </target>

    <target name="build_standalone_war" depends="build" >
        <mkdir dir="${out.dir}/standalone" />
        <mkdir dir="${out.dir}/logs" />
        <jar destfile="${out.dir}/standalone/${war.file}" >
            <resources refid="war_files" />
        </jar>
    </target>

    <target name="build_war" description="Used to build war" depends="build" >
        <mkdir dir="${out.dir}/jboss" />
        <mkdir dir="${out.dir}/logs" />
        
        <copy todir="${out.dir}/WEB-INF/lib">
          <fileset refid="java.dependency.fileset" />
          <!-- This mapper strips off all leading directory information -->
          <mapper type="flatten" />
        </copy>
        
        <jar destfile="${out.dir}/jboss/${war.file}" manifest="META-INF/MANIFEST.MF" >
            <resources refid="war_files" />
        </jar>
    </target>
    
    <target name="build" depends="-check_tomcat_home, -init_build, build_laszlo, build_java" description="Build Web Console, but don't produce war." />

    <target name="build_laszlo" depends="-init_build, -check_for_laszlo_changes" unless="nolaszlochanges" >
        <java classname="org.openlaszlo.compiler.Main" fork="true" newenvironment="true" failonerror="true" dir="${laszlo.src.dir}" classpathref="laszlo.lib">
            <jvmarg value="-DLPS_HOME=${laszlo.home}" />
            <jvmarg value="-Xms1024M" />
            <jvmarg value="-Xmx1024M" />
            <arg line="${laszlo.debug} --dir ${out.dir} --runtime=${swfRuntime} -o ${swf.file} synapsoft.lzx" />
        </java>
        
        <!-- This file gets left around. It is exact copy of ${out.dir}/${swf.file}. -->
        <delete file="synapsoft.lzx.${swfRuntime}.swf" />
    </target>

    <target name="build_java" depends="-init_build, -get_dependencies">
        <echo message="${ant.project.name}: ${ant.file}" />
        <javac debug="true" debuglevel="${debuglevel}" destdir="${out.dir}/WEB-INF/classes" source="${source}" target="${target}">
            <src path="WEB-INF/src" />
            <classpath refid="java.dependency.classpath" />
        </javac>
    </target>

    <target name="deploy_artifacts">
    </target>

    <target name="-check_for_laszlo_changes" >
        <uptodate property="nolaszlochanges" >
            <srcfiles dir="../" includes="**/*.lzx"/>
            <mapper type="merge" to="${out.dir}/${swf.file}" />
        </uptodate>
    </target>
    
    <target name="-check_tomcat_home" unless="SERVLET_DEV">
        <fail message="SERVLET_DEV must be set!" />
    </target>

    <target name="-is_release_build">
        <property name="RELEASE_BUILD" value="${env.git_tag}" />
        <condition property="_RELEASE_" value="">
            <contains string="${RELEASE_BUILD}" substring="RELEASE" casesensitive="false" />
        </condition>
    </target>

    <target name="-init_build" >
        <mkdir dir="${out.dir}" />
        <mkdir dir="${maven.ant.jar.dir}" />

        <copy includeemptydirs="false" todir="${out.dir}/resources" encoding="UTF-8">
            <fileset dir="resources/strings">
                <include name="*.properties" />
            </fileset>
        </copy>

		<copy includeemptydirs="false" todir="${out.dir}/WEB-INF/classes">
			<fileset dir="WEB-INF/src" excludes="**/*.launch, **/*.g, **/*.java" />
		</copy>

        <antcall target="-set_beta_title" />
	</target>
	
	<target name="-set_beta_title" depends="-is_release_build" unless="_RELEASE_" >
		<replace dir="${out.dir}/resources" includes="*.properties">
			<replacefilter token="title_logo=''" value="title_logo=BETA" />
		</replace>
	</target>
		
	<target name="-set_jboss_home" if="${env.JBOSS_HOME}" >
		<property name="JBossHome" value="${env.JBOSS_HOME}" />
	</target>
	
</project>
