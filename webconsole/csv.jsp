<%@page pageEncoding="utf-8" contentType="APPLICATION/OCTET-STREAM; charset=utf-8"%>
<%@page import="com.synapsense.utilities.RequestVariableNames"%>
<%@page import="com.synapsense.session.SessionVariableNames"%>
<%@page import="java.io.OutputStream"%>
<%
    String disHeader = "Attachment; Filename=\"data.csv\"";
    response.setHeader("Content-Disposition", disHeader);

    String key = request.getParameter(RequestVariableNames.TABLE_KEY);
    String csv = (String) request.getSession().getAttribute(SessionVariableNames.getDataVariableName(key));
    if (csv != null) {
        request.getSession().removeAttribute(SessionVariableNames.getDataVariableName(key));

        OutputStream os = response.getOutputStream();
        os.write(0xEF);
        os.write(0xBB);
        os.write(0xBF);
        os.write(csv.getBytes("UTF-8"));
        os.flush();
        os.close();
        out.clear();
        out = pageContext.pushBody(); 
    } else {
        out.println("Data expired. Please export data again.");
    }
%>
