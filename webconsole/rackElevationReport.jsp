<%@page import="com.synapsense.powerimaging.RackSorter"%>
<%@page import="com.synapsense.dto.CollectionTO"%>
<%@page import="java.util.List"%>
<%@page import="com.synapsense.dataAccess.Env"%>
<%@page import="com.synapsense.utilities.es.CollectionTOSorter"%>
<%@page import="com.synapsense.dto.TO"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.LinkedList"%>
<%@page import="com.synapsense.dto.TOFactory"%>
<%@page import="com.synapsense.session.SessionVariableNames"%>
<%@page import="com.synapsense.utilities.RequestVariableNames" contentType="text/html; charset=utf-8"%>
<html>
<title>Rack Elevation Report</title>
<body>
<%
    String key = request.getParameter(RequestVariableNames.TABLE_KEY);
    String data = (String) request.getSession().getAttribute(SessionVariableNames.getDataVariableName(key));
    if (data != null) {
        request.getSession().removeAttribute(SessionVariableNames.getDataVariableName(key));
        
        String[] rackIds = data.split(",");
        Collection<TO<?>> racks = new LinkedList<TO<?>>();
        for (String rackId : rackIds) {
            racks.add(TOFactory.getInstance().loadTO(rackId));
        }
            
//		List<CollectionTO> rackProps = CollectionTOSorter.sortObjects(Env.getEnv(request).getPropertyValue(racks, new String[] {"y"}), "y");
//		for (CollectionTO rackProp : rackProps) {
//			out.println("<img src=\"getPowerRackImage?id=" + rackProp.getObjId() + "\" alt=\"Rack Image\"/>");
//		}

        List<RackSorter.Row> rackRows = RackSorter.sort(Env.getEnv(request).getPropertyValue(racks, new String[] {"y", "x", "rotation"}));

        for (RackSorter.Row row : rackRows) {
            for (CollectionTO rack : row.getRacks()) {
                out.println("<img src=\"" + response.encodeURL("getPowerRackImage") + "?id=" + rack.getObjId() + "\" alt=\"Rack Image\"/>");
            }
            out.println("<br/>");
        }
    } else {
        out.println("Data expired. Please export data again.");
    }
%>
</body>
</html>