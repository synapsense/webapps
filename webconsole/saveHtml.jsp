<%@page import="com.synapsense.utilities.RequestVariableNames" contentType="text/html; charset=utf-8"%>
<%@page import="com.synapsense.session.SessionVariableNames"%>
<html>
<body">
<script type="text/javascript" language="javascript" charset="utf-8">

var timer = setTimeout("timerExpire();", 1000);

function timerExpire() {
    if (navigator.appVersion.indexOf("MSIE") != -1) {
        //works only in IE
        document.execCommand("SaveAs");
    }
}
</script> 
<%
    String disHeader = "Attachment; Filename=\"data.html\"";
    response.setHeader("Content-Disposition", disHeader);

    String key = request.getParameter(RequestVariableNames.TABLE_KEY);
    String html = (String) request.getSession().getAttribute(SessionVariableNames.getDataVariableName(key));
    if (html != null) {
        request.getSession().removeAttribute(SessionVariableNames.getDataVariableName(key));

        out.println(html);
    } else {
        out.println("Data expired. Please export data again.");
    }
%>
</body>
</html>