# Run Tomcat 5 as this user ID (default: tomcat5). Set this to an empty string
# to prevent Tomcat from starting.
export TOMCAT5_USER=root #tomcat5

# The home directory of the Java development kit (JDK). You need at least
# JDK version 1.3, just the Java runtime environment (JRE) will not work
# because a Java compiler is needed to translate JavaServer Pages (JSP).
# If JAVA_HOME is not set, some common directories for the non-free JDKs
# as packaged by the Debian java-package and the directories of 
# java-gcj-compat-dev and kaffe are tried. 
#
# You can also set JSSE_HOME here to enable SSL support
# (this is automatically done for JDK 1.4+, java-gcj-compat-dev and kaffe).
export JAVA_HOME=/usr/lib/jvm/java-6-sun
#JSSE_HOME=/usr/local/jsse

# Directory for per-instance configuration files and webapps. It contain the
# directories conf, logs, webapps, work and temp. See RUNNING.txt for details.
# Default: /var/lib/tomcat5
#CATALINA_BASE=/var/lib/tomcat5

# Arguments to pass to the Java virtual machine (JVM)
# "-Djava.awt.headless=true -Xmx128M" is automatically set if CATALINA_OPTS
# is left empty here
export CATALINA_OPTS="-Djava.awt.headless=true -Xmx384M -server -Djavax.net.ssl.keyStore=conf\keystore -Djavax.net.ssl.keyStorePassword=synap\$ense -Djavax.net.ssl.trustStore=conf\truststore -Djavax.net.ssl.trustStorePassword=synap\$ense"

# Java compiler to use for translating JavaServer Pages (JSPs). You can use all
# compilers that are accepted by Ant's build.compiler property.
#JSP_COMPILER=jikes

# Use the Java security manager? (yes/no, default: yes)
export TOMCAT5_SECURITY=no

# Timeout in seconds for the shutdown procedure (default: 30). The Java
# processes will be killed if tomcat5 has not stopped until then.
#TOMCAT5_SHUTDOWN=30

# Number of days to keep old log files in /var/log/tomcat5 (default: 14)
#LOGFILE_DAYS=30
