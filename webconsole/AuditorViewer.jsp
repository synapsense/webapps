<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %> 
<%@ taglib uri="http://java.sun.com/jstl/xml"  prefix="x" %> 

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Auditor Viewer</title>
</head>
<body>
    <c:import url="getServicesStatus" var="inputData"/>
    <c:import url="AuditorViewerTemplate.xsl" var="templateData"/>
    
    <x:transform xml = "${inputData}" xslt = "${templateData}">
    </x:transform>
    
</body>
</html>