<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="componentList">
	<h1>Auditors:</h1>
	<table cellpadding="5">
		<th><b>Component/Task Name</b></th>
		<th><b>Status</b></th>
		<th><b>Details</b></th>
		<xsl:apply-templates />
	</table>
	</xsl:template>
	
	<xsl:template match="component">
		<xsl:call-template name="processComponent">
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="processComponent">
		<tr>
			<td>
				<xsl:value-of select="name/text()"></xsl:value-of>
			</td>				
			<td>
				<xsl:value-of select="status/text()"></xsl:value-of>
			</td>
			<td>
				<xsl:value-of select="details/text()"></xsl:value-of>
			</td>
		</tr>
	</xsl:template>
</xsl:stylesheet>